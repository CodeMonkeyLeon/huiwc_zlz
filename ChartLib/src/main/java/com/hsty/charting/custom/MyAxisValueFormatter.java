package com.hsty.charting.custom;


import com.hsty.charting.components.AxisBase;
import com.hsty.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyAxisValueFormatter implements IAxisValueFormatter
{

    private final DecimalFormat mFormat;

    public MyAxisValueFormatter() {
//        mFormat = new DecimalFormat("###,###,###,##0.00");
        mFormat = new DecimalFormat("###,###,###,##0.00");

    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
//        return mFormat.format(value) + " w";
        return mFormat.format(value) + "";
    }
}
