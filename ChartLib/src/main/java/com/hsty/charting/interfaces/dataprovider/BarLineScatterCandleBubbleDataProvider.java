package com.hsty.charting.interfaces.dataprovider;


import com.hsty.charting.components.YAxis;
import com.hsty.charting.data.BarLineScatterCandleBubbleData;
import com.hsty.charting.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(YAxis.AxisDependency axis);
    boolean isInverted(YAxis.AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
