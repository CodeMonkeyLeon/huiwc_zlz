package com.hsty.charting.interfaces.dataprovider;


import com.hsty.charting.data.BubbleData;

public interface BubbleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    BubbleData getBubbleData();
}
