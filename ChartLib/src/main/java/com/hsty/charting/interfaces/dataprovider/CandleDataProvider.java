package com.hsty.charting.interfaces.dataprovider;


import com.hsty.charting.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    CandleData getCandleData();
}
