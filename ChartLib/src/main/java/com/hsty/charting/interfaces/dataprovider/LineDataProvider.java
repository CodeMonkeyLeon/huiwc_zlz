package com.hsty.charting.interfaces.dataprovider;


import com.hsty.charting.components.YAxis;
import com.hsty.charting.data.LineData;

public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {

    LineData getLineData();

    YAxis getAxis(YAxis.AxisDependency dependency);
}
