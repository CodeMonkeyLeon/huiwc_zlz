package com.hsty.charting.interfaces.dataprovider;


import com.hsty.charting.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
