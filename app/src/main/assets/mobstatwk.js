var BaiduMobStat = (function() {
    var invokeNatvieMethod = function(action, obj) {
        window.webkit.messageHandlers.bmtj.postMessage({action:action, obj: obj});
    }

    return {
        onPageStart: function(page) {
            var obj = {
                page: page
            };

            invokeNatvieMethod('onPageStart', obj);
        },
        onPageEnd: function(page) {
            var obj = {
                page: page
            };

            invokeNatvieMethod('onPageEnd', obj);
        },
        onEvent: function(id, label) {
            var obj = {
                event_id: id,
                label: label,
            };

            invokeNatvieMethod('onEvent', obj);
        },
        onEventStart: function(id, label) {
            var obj = {
                event_id: id,
                label: label
            };

            invokeNatvieMethod('onEventStart', obj);
        },
        onEventEnd: function(id, label) {
            var obj = {
                event_id: id,
                label: label
            };

            invokeNatvieMethod('onEventEnd', obj);
        },
        onEventDuration: function(id, label, duration) {
            var obj = {
                event_id: id,
                label: label,
                duration: typeof duration === 'number' ? duration : 0
            };

            invokeNatvieMethod('onEventDuration', obj);
        },
        onEventWithAttributes: function(id, label, attributes) {
            var obj = {
                event_id: id,
                label: label,
                attributes: typeof attributes === 'object' ? attributes: {}
            };
                
            invokeNatvieMethod('onEventWithAttributes', obj);
        },
        onEventEndWithAttributes: function(id, label, attributes) {
            var obj = {
                event_id: id,
                label: label,
                attributes: typeof attributes === 'object' ? attributes: {}
            };

            invokeNatvieMethod('onEventEndWithAttributes', obj);
        },           
        onEventDurationWithAttributes: function(id, label, duration, attributes) {
            var obj = {
                event_id: id,
                label: label,
                duration: typeof duration === 'number' ? duration : 0,
                attributes: typeof attributes === 'object' ? attributes: {}
            };

            invokeNatvieMethod('onEventDurationWithAttributes', obj);
        },            
        
    };
}());
