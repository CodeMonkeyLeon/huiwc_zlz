package android.printer;


import android.graphics.Bitmap;
import android.graphics.Color;
import android.printerstatment.PrinterStat;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
/**
 * Created by Administrator on 2017/6/14.
 */

public class PrintTools {

    private static final String TAG = "PrintTools";
    public static SerialPortTools mSerialPortTools;

    public static final byte HT = 0x9; // 水平制表
    public static final byte LF = 0x0A; // 打印并换行
    public static final byte CR = 0x0D; // 打印回车
    public static final byte ESC = 0x1B;
    public static final byte DLE = 0x10;
    public static final byte GS = 0x1D;
    public static final byte FS = 0x1C;
    public static final byte STX = 0x02;
    public static final byte US = 0x1F;
    public static final byte CAN = 0x18;
    public static final byte CLR = 0x0C;
    public static final byte EOT = 0x04;



    public static final byte[] INIT_PRINT_CODE = new byte[]{ESC,'@'};
    public static final byte[] CHECK_STAT_CODE = new byte[] { GS, 'r',
            0x01 };
    /* 默认颜色字体指令 */
    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[] { ESC, 'r',
            0x00 };
    /* 标准大小 */
    public static final byte[] FS_FONT_ALIGN = new byte[] { FS, 0x21, 1, ESC,
            0x21, 1 };
    /* 对齐命令 */
    public static byte[] ESC_ALIGN_CODE = new byte[] { 0x1b, 'a', 0x00 };
    /* 靠左打印命令 */
    public static final byte[] ESC_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
    /* 居中打印命令 */
    public static final byte[] ESC_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };
    /*打印模式选择*/
    public static byte[] ESC_PRINT_MODE = new byte[] { 0x1b, 0x21, 0x00 };
    /*字符间距*/
    public static byte[] ESC_CHAR_SPACE = new byte[] { 0x1b, 0x20, 0x00 };
    /*行间距*/
    public static byte[] ESC_LINE_SPACE = new byte[] { 0x1b, 0x33, 0x00 };
    /* 取消字体加粗 */
    public static final byte[] ESC_CANCEL_BOLD = new byte[] { ESC, 0x45, 0x00 };
    /* 退出汉字编码*/
    public static final byte[] OUT_CN_CODE = new byte[] { 0x1C, 0x2E };
    /* 进入汉字编码*/
    public static final byte[] ENTER_CN_CODE = new byte[] { 0x1C, 0x26 };
    /*设置外文编码页*/
    public static byte[] LANG_PAGE_CODE = new byte[] { 0x1B, 0x74, 0x0D };
    // 打印并走纸
    public static byte[] ESC_ENTER = new byte[] { 0x1B, 0x64, 0x00 };
    //一维码高度
    public static byte[] BAR_CODE_H = new byte[] { GS, 'h', 0x32 };
    //一维码左边距
    public static byte[] BAR_CODE_LEFT = new byte[] { GS, 'x', 0x00 };
    public static byte[] IMAGE_LEFT = new byte[] { ESC ,'P' ,0x00 ,0x00 };
    //设置条码HRI字符打印位置
    public static byte[] BAR_CODE_HRI = new byte[] { GS, 'H', 0x02 };
    //一维码宽度
    public static byte[] BAR_CODE_W = new byte[] { GS, 'w', 0x02 };
    //打印条码命令,默认PRINT_EAN13码
    public static byte[] GS_BAR_CODE = new byte[] { GS, 'k', 0x43 ,0x0C };
    // 自检
    public static final byte[] PRINTE_TEST = new byte[] { 0x1D, 0x28, 0x41 };

    // 测试输出Unicode Pirit Message
    public static final byte[] UNICODE_TEXT = new byte[] {0x00, 0x50, 0x00,
            0x72, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x74, 0x00, 0x20, 0x00, 0x20,
            0x00, 0x20, 0x00, 0x4D, 0x00, 0x65, 0x00, 0x73, 0x00, 0x73, 0x00,
            0x61, 0x00, 0x67, 0x00, 0x65};
    /**
     * 打开打印机
     *
     * @param fd 打印机接入串口位置fd值代表接入ttySXX
     * @return
     */
    public static boolean OpenPrinter(String port,int baudrate)
    {
        try {
            mSerialPortTools = new SerialPortTools(port,baudrate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mSerialPortTools ==null)
            return false;
        else
            return true;
    }

    /**
     * 初始化打印机
     *
     * @return
     */
    public static void InitPrinter(){
        mSerialPortTools.write(INIT_PRINT_CODE);
    }
    /**
     * 设置打印机参数 当前参数 9600 8 1 0
     *
     * @param br             波特率
     * @param data_bits      数据位
     * @param stop_bits      停止位
     * @param check_mode校验模式
     * @return
     */
    /*public static int SetSerial(int br, int data_bits, int stop_bits,
                                int check_mode);*/

    /**
     * 关闭打印机
     *
     * @return
     */
    public static void ClosePrinter()
    {
        try {
            mSerialPortTools.closeSerialPort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 检测打印机状态
     *PRINT_OUT_POWER未上电
     *PRINT_OUT_PAPER 缺纸
     *PRINT_OUT_TEMP 过温
     *PRINT_STAT_NORMAL 正常
     * @return
     */
    public static int CheckPrinterStatus(){
       byte [] b =new byte[1];
        int size = 0;
        int status = 0;
        mSerialPortTools.write(CHECK_STAT_CODE);
        try {
            Thread.sleep(20);
        }catch (InterruptedException e) {
            // TODO Auto-generated catch block
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
        try {
            size = mSerialPortTools.read(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        status =(int)b[0];
        if(PrinterStat.PRINTER_DEBUG)
            Log.d(TAG,"status = " + status + "  size=" + size);
        if(size==0) {
            return PrinterStat.PRINT_OUT_POWER;
        }
        if((status & (1<<2))!=0) {
            return PrinterStat.PRINT_OUT_PAPER;
        }
        if((status & (1<<7)) !=0) {
            return PrinterStat.PRINT_OUT_TEMP;
        }
        return PrinterStat.PRINT_STAT_NORMAL;
    }

    /**
     * 打印数据
     *
     * @param b       要打印数据字节流
     * @param ret_len 打印数据长度
     * @return
     */
    public static void Printer(byte[] b, int ret_len)
    {
        try {
            mSerialPortTools.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 对齐方式 0左对齐 1居中对齐 2右对齐//暂无此功能
     *
     * @param aligned
     * @return
     */
    public static void SetAligned(int aligned)
    {
        if((aligned<=2 &&aligned>=0)||(aligned<=50 &&aligned>=48))
            ESC_ALIGN_CODE[2] = (byte)aligned;

        try {
            mSerialPortTools.write(ESC_ALIGN_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印走纸N行
     *
     * @param len 走纸行数
     * @return
     */
    public static void PrintFeed(int len)
    {
        if(len>=0&&len<=255)
            ESC_ENTER[2] = (byte)len;
        try {
            mSerialPortTools.write(ESC_ENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 打印换行
     *
     * @return
     */
    public static void PrintLF()
    {
        try {
            mSerialPortTools.write(LF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印结束符
     *
     * @return
     */
    //public static int PrintCR();

    /**
     * 打印方式
     * @param mode 0 1 2 3 4 5 6 7
     * 0 字模选择
     * 1 2 6 无定义
     * 3 着重模式
     * 4 倍高
     * 5 倍宽
     * 7 下划线
     * @param n
     * 0 取消
     * 1 设置
     * @return
     */
    public static void PrintMode(int mode ,int n)
    {
        if(n>=0&&n<=255)
            ESC_PRINT_MODE[2] = (byte) (ESC_PRINT_MODE[2]|(byte)(n<<mode));
        try {
            mSerialPortTools.write(ESC_PRINT_MODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 水平放大 0放大 1还原
     *
     * @param n
     * @return
     */
    //public static int TransverseZoomCharacter(int n);

    /**
     * 设置字符间距//暂无此功能
     *
     * @param n 间距大小
     * @return
     */
    public static void SetFontBold(int n)
    {
        if(n>=0&&n<=255)
            ESC_CANCEL_BOLD[2] = (byte)n;

        try {
            mSerialPortTools.write(ESC_CANCEL_BOLD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 设置解除字体加粗
     *
     * @param n 字体大小
     * @return
     */
    public static void SetCharSpacing(int n)
    {
        if(n>=0&&n<=128)
            ESC_CHAR_SPACE[2] = (byte)n;

        try {
            mSerialPortTools.write(ESC_CHAR_SPACE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置行间距
     *
     * @param n 间距大小
     * @return
     */
    public static void SetLineSpacing(int n)
    {
        if(n>=0&&n<=255)
            ESC_LINE_SPACE[2] = (byte)n;
        try {
            mSerialPortTools.write(ESC_LINE_SPACE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 退出中文模式
     * @param
     */
    public static void OutCnGbk() {
        try {
            mSerialPortTools.write(OUT_CN_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *进入中文模式
     * @param
     */
    public static void EnterCnGbk() {
        try {
            mSerialPortTools.write(ENTER_CN_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置外文代码页
     * @param n
     */
    public static void SetLangPage(byte n) {
        try {
            LANG_PAGE_CODE[2] = n;
            mSerialPortTools.write(LANG_PAGE_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 打印GBK编码字符串
     */
    public static void PrintString(String data) {
        byte[] bd = null;
        try {
            bd = data.getBytes("GBK");
            Printer(bd, bd.length);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 打印外文
     */
    public static void PrintCodePage(String data, byte lang) {
        byte[] bd = null;
        String chartype = "GBK";
        switch (lang) {
            case PrinterStat.LANG_PT:
                chartype = "Cp860";
                break;
            case PrinterStat.LANG_RU:
                chartype = "Cp866";
                break;
            default:
                chartype = "GBK";
                break;
        }
        try {
            bd = data.getBytes(chartype);
            Printer(bd, bd.length);
            if (PrinterStat.PRINTER_DEBUG) {
                for (int i = 0; i < bd.length; i++)
                    Log.d(TAG, "Cp860 length = " + bd.length + "Cp860 code =" + Integer.toHexString(bd[i])//+ ","+ Integer.toHexString(bd[1]) + "," + Integer.toHexString(bd[2]) + "," + Integer.toHexString(bd[3]) + "," + Integer.toHexString(bd[4])
                    );
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    /**
     * 设置条码高度
     */
    public static void SetBarCodeH(int n) {
        if(n>=10&&n<=240)
            BAR_CODE_H[2] = (byte)n;
        try {
            mSerialPortTools.write(BAR_CODE_H);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 设置条码宽度
     */
    public static void SetBarCodeW(int n) {
        if(n>=2&&n<=4)
            BAR_CODE_W[2] = (byte)n;
        try {
            mSerialPortTools.write(BAR_CODE_W);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 设置条码左边距
     */
    public static void SetBarCodeLeft(int n) {
        if(n>=0&&n<=255)
            BAR_CODE_LEFT[2] = (byte)n;
        try {
            mSerialPortTools.write(BAR_CODE_LEFT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 设置条码HRI字符位置
     */
    public static void SetBarCodeHri(int n) {
        if((n>=0&&n<=3)||(n>=48&&n<=51))
            BAR_CODE_HRI[2] = (byte)n;
        try {
            mSerialPortTools.write(BAR_CODE_HRI);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 打印条码
     */
    public static boolean PrintBarCode(int type,int n,byte[]b) {
        GS_BAR_CODE[2] = 0x43;
        GS_BAR_CODE[3] = (byte)n;
        switch (type) {
            case PrinterStat.PRINT_EAN13:
                mSerialPortTools.write(GS_BAR_CODE);
                mSerialPortTools.write(b);
                break;
            case PrinterStat.PRINT_EAN8:
                GS_BAR_CODE[2] = PrinterStat.PRINT_EAN8;
                mSerialPortTools.write(GS_BAR_CODE);
                mSerialPortTools.write(b);
                break;
            case PrinterStat.PRINT_EAN39:
                GS_BAR_CODE[2] = PrinterStat.PRINT_EAN39;
                mSerialPortTools.write(GS_BAR_CODE);
                mSerialPortTools.write(b);
                break;
            case PrinterStat.PRINT_EAN93:
                GS_BAR_CODE[2] = PrinterStat.PRINT_EAN93;
                mSerialPortTools.write(GS_BAR_CODE);
                mSerialPortTools.write(b);
                break;
            case PrinterStat.PRINT_EAN128:
                GS_BAR_CODE[2] = PrinterStat.PRINT_EAN128;
                mSerialPortTools.write(GS_BAR_CODE);
                mSerialPortTools.write(b);
                break;
            default:
                if(PrinterStat.PRINTER_DEBUG)
                    Log.d(TAG,"条码类型：" + type + "格式错误");
                return false;
        }
        return true;
    }

    /**
     * 设置条码左边距
     */
    public static void SetImageLeft(int n) {
        if(n>=0&&n<=255)
            IMAGE_LEFT[2] = (byte)n;
        try {
            mSerialPortTools.write(IMAGE_LEFT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印图片
     *
     * @param n
     * @return
     */
    public static int PrintImage(final Bitmap bmp) {
        // 打印速度
        // byte[] speed20 = { 0x1b, 0x6d, 18 };
        if (bmp == null) {
            return -1;
        }
        int mBitmapSize = bmp.getWidth() * bmp.getHeight() * 24;
        ByteBuffer dst = ByteBuffer.allocate(mBitmapSize); // 分配一定的空间,1024 ;
        bmp.copyPixelsToBuffer(dst);
        int height = bmp.getHeight();// 24的倍数
        int weight = bmp.getWidth();

        int mod = 0;
        if (height % 24 != 0) {
            mod = height % 24;
            height = height + (24 - mod);
        }
        if(PrinterStat.PRINTER_DEBUG)
            Log.d(TAG,"PrintImage height = " + height);
        byte[] head = {0x1b, 0x2a, 0x21, (byte) 0x00, 0x00};

        if (weight > 255) {
            head[3] = (byte) (weight - 256);
            head[4] = 1;
        } else {
            head[3] = (byte) weight;
            head[4] = 0;
        }
        int width = weight * 3;

        for (int i = 0; i < height; i += 24) {

            Printer(head, head.length);

            byte[] body = new byte[width];
            for (int j = 0; j < width; j++) {

                for (int k = 0; k < 24; k++) {

                    int color = 0;
                    int r = 0;
                    int g = 0;
                    int b = 0;

                    if (mod != 0) {
                        if (i + k >= bmp.getHeight()) {
                            b = g = r = 1;
                        } else {
                            color = bmp.getPixel(j / 3, i + k);
                            r = Color.red(color);
                            g = Color.green(color);
                            b = Color.blue(color);
                        }
                    } else {
                        color = bmp.getPixel(j / 3, i + k);
                        r = Color.red(color);
                        g = Color.green(color);
                        b = Color.blue(color);
                    }


                    if (k == 8 || k == 16) {
                        j++;
                    }
                    //
                    // if (r == 255 && g == 255 && b == 255) {
                    // body[j] = (byte) (body[j] * 2);
                    // } else {
                    // body[j] = (byte) (body[j] * 2 + 1);
                    // }

                    if (r != 0 && g != 0 && b != 0) {
                        body[j] = (byte) (body[j] * 2);
                    } else {
                        body[j] = (byte) (body[j] * 2 + 1);
                    }
                }

            }
            // 打印图片内容
            Printer(body, body.length);

            try {
                Thread.currentThread().sleep(120);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                Thread.currentThread().interrupt();
                e.printStackTrace();
            }
            byte[] bodyend = {0x1b, 0x4a, 0x00};

            Printer(bodyend, bodyend.length);
            // PrintLF();
        }
        return 0;
    }
}