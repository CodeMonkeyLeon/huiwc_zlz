package android.printer;

/**
 * Created by Administrator on 2017/6/14.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;

import android.printerstatment.PrinterStat;
import android.serialport.SerialPort;
import android.util.Log;

public class SerialPortTools {

    private static final String TAG = "SerialPortTools";
    protected SerialPort mSerialPort;
    protected FileOutputStream mOutputStream;
    private FileInputStream mInputStream;
    private ReadThread mReadThread;

    public SerialPort getSerialPort(String port,int baudrate) {
        //if (mSerialPort == null) {
            SerialPort mgetSerialPort = null;
            if ((port != null) && (baudrate > 0)) {
            try {
                mgetSerialPort = new SerialPort(new File(port), baudrate);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }
        if(PrinterStat.PRINTER_DEBUG)
            Log.d(TAG,"getSerialPort mgetSerialPort = " + mgetSerialPort);
        return mgetSerialPort;
    }
    /**
     * @param port 端口
     * @param baudrate 波特率
     * */
    public SerialPortTools(String port,int baudrate) throws Exception {
            mSerialPort = this.getSerialPort(port,baudrate);
            if(mSerialPort==null)
                throw new Exception();
            else {
                mOutputStream = (FileOutputStream) mSerialPort.getOutputStream();
                mInputStream = (FileInputStream) mSerialPort.getInputStream();
            }
    }

    /*void initp()
    {
        if (mOutputStream != null) {
            try {
                mOutputStream.write(new byte[]{0x1B,'@'});
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
*/
    private class ReadThread extends Thread {
        public void run() {
            super.run();
            while(!isInterrupted()) {
                int size;
                try {
                    byte[] buffer = new byte[64];
                    if (mInputStream == null) return;
                    size = mInputStream.read(buffer);
                    if (size > 0) {
                        if(PrinterStat.PRINTER_DEBUG)
                            Log.d(TAG,"ReadThread size =" + size);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    /** 关闭串口 */
    public void closeSerialPort() {
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    protected void destroy() {
        if (mReadThread != null)
            mReadThread.interrupt();
        this.closeSerialPort();
        mSerialPort = null;
    }

    // [s] 输出
    public void write(String msg)
    {
        try {
            if(allowToWrite())
            {
                if(msg == null)
                    msg = "";
                mOutputStream.write(msg.getBytes("unicode"));
                mOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // [s] 输出
    public void write_unicode(String msg)
    {
        try {
            if(allowToWrite())
            {
                if(msg == null)
                    msg = "";
                mOutputStream.write(msg.getBytes("unicode"));
                mOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // [s] 输出
    public void write_gbk(String msg)
    {
        try {
            if(allowToWrite())
            {
                if(msg == null)
                    msg = "";
                mOutputStream.write(msg.getBytes("GBK"));
                mOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 输出
     * */
    public void write(byte[] b)
    {
        try {
            if(allowToWrite())
            {
                if(b == null)
                    return;
                mOutputStream.write(b);
                mOutputStream.flush(); // 1
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取数据
     * */
    public int read(byte[] buffer) {
        int size =0;
        try {
            if (mInputStream == null) return 0;
            size = mInputStream.read(buffer);
            if(PrinterStat.PRINTER_DEBUG)
                Log.d(TAG,"read size =" + size);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return size;
    }

    public void write(int oneByte)
    {
        try {
            if(allowToWrite())
            {
                mOutputStream.write(oneByte);
                mOutputStream.flush(); // 1
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 是否允许打印
     * */
    public boolean allowToWrite()
    {
        if (mOutputStream == null) {
            if(PrinterStat.PRINTER_DEBUG)
                Log.d(TAG,"mOutputStream不能为空！");
            return false;
        }
        return true;
    }

    // [e]

}
