package android.printer;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.util.Log;

import com.hstypay.enterprise.utils.LogUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class fileUtil {
	//创建文件夹及文件
	public final static  String filenameTemp ="/mnt/sdcard/printzedapp/historyBill" ;
    public static void CreateText(String filename) throws IOException {  
        File file = new File(filenameTemp);
        Log.i("yang","path="+Environment.getDataDirectory());
        if (!file.exists()) {  
            try {
                //按照指定的路径创建文件夹  
                file.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
                // TODO: handle exception  
            }  
        }  
        File dir = new File(file.getAbsolutePath()+"/"+filename);
        if (!dir.exists()) {  
              try {  
                  //在指定的文件夹中创建文件  
                  if (!dir.createNewFile()) {
                      LogUtil.d("Jeremy-创建文件失败");
                  }
            } catch (Exception e) {
                  e.printStackTrace();
            }  
        }  
    }  
      
    //向已创建的文件中写入数据  
    @SuppressLint("SimpleDateFormat")
	public static void writedata(String str,String filename) {
        FileWriter fw = null;  
        BufferedWriter bw = null;  
        String datetime = "";  
        try {  
            SimpleDateFormat tempDate = new SimpleDateFormat("yyyy-MM-dd" + " "  
                    + "hh:mm:ss");  
            datetime = tempDate.format(new java.util.Date()).toString();  
            fw = new FileWriter(filenameTemp+"/"+filename, true);//
            // 创建FileWriter对象，用来写入字符流  
            bw = new BufferedWriter(fw); // 将缓冲对文件的输出  
            String myreadline = datetime + "[]" +"\n"+ str;  
            bw.write(myreadline + "\n"); // 写入文件  
            bw.newLine();  
            bw.flush(); // 刷新该流的缓冲  
            bw.close();  
            fw.close();  
        } catch (IOException e) {  
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    } 
    //数字、字母全角化转半角化
    public static String ToDBC(String input) {
         char[] c = input.toCharArray();
         for (int i = 0; i< c.length; i++) {
        	 if (c[i] == 12288) {
        		 c[i] = (char) 32;
        		 continue;
        	 }if (c[i]> 65280&& c[i]< 65375)
        		 c[i] = (char) (c[i] - 65248);
         }
         return new String(c);
    	}
    // 半角转化为全角的方法
    public String ToSBC(String input) {
     // 半角转全角：
     char[] c = input.toCharArray();
     for (int i = 0; i < c.length; i++) {
      if (c[i] == 32) {
       c[i] = (char) 12288;
       continue;
      }
      if (c[i] < 127 && c[i]>32)
       c[i] = (char) (c[i] + 65248);
     }
     return new String(c);
    }
}
