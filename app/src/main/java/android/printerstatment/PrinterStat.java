package android.printerstatment;

/**
 * Created by Administrator on 2017/6/16.
 */

public class PrinterStat {
    public static boolean PRINTER_DEBUG = false;
    public static final int PRINT_STAT_NORMAL = 0;
    public static final int PRINT_OUT_PAPER = -1;
    public static final int PRINT_OUT_TEMP = -2;
    public static final int PRINT_OUT_POWER =-3;
    public static final int PRINT_EAN13 = 0x43;
    public static final int PRINT_EAN8 = 0x44;
    public static final int PRINT_EAN39 = 0x45;
    public static final int PRINT_EAN93 = 0x48;
    public static final int PRINT_EAN128 = 0x49;
    public static final byte LANG_CN = 0x7F;
    public static final byte LANG_PT = 0x08;
    public static final byte LANG_RU = 0x0D;

}
