package com.hstypay.enterprise.Widget;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.IndustryBean;
import com.hstypay.enterprise.utils.Lists;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by smartTop on 2016/10/19.
 */

public class AddressSelector implements AdapterView.OnItemClickListener {
    private static final int INDEX_TAB_PROVINCE = 0;//省份标志
    private static final int INDEX_TAB_CITY = 1;//城市标志
    private static final int INDEX_TAB_COUNTY = 2;//乡镇标志
    private int tabIndex = INDEX_TAB_PROVINCE; //默认是省份

    private static final int INDEX_INVALID = -1;
    private int provinceIndex = INDEX_INVALID; //省份的下标
    private int cityIndex = INDEX_INVALID;//城市的下标
    private int countyIndex = INDEX_INVALID;//乡镇的下标

    private Context context;
    private final LayoutInflater inflater;
    private View view;

    private View indicator;

    private LinearLayout layout_tab;
    private TextView textViewProvince;
    private TextView textViewCity;
    private TextView textViewCounty;

    private ViewPager viewpager;
    private ProvinceAdapter provinceAdapter;
    private CityAdapter cityAdapter;
    private CountyAdapter countyAdapter;
    private List<IndustryBean.Industry1Entity> provinces;
    private List<IndustryBean.Industry1Entity.Industry2Entity> cities;
    private List<IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity> counties;
    private OnAddressSelectedListener listener;
    private OnDialogCloseListener dialogCloseListener;

    private static final int WHAT_PROVINCES_PROVIDED = 0;
    private static final int WHAT_CITIES_PROVIDED = 1;
    private static final int WHAT_COUNTIES_PROVIDED = 2;
    private ImageView iv_colse;
    private int selectedColor;
    private int unSelectedColor;

    private List<View> viewContainer;
    @SuppressWarnings("unchecked")
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_PROVINCES_PROVIDED: //更新省份列表
                    provinces = (List<IndustryBean.Industry1Entity>) msg.obj;
                    viewContainer.clear();
                    viewContainer.add(mView1);
                    mPagerAdapter.notifyDataSetChanged();
                    provinceAdapter.notifyDataSetChanged();
                    mListView1.setAdapter(provinceAdapter);

                    break;

                case WHAT_CITIES_PROVIDED: //更新城市列表
                    cities = (List<IndustryBean.Industry1Entity.Industry2Entity>) msg.obj;
                    viewContainer.clear();
                    viewContainer.add(mView1);
                    viewContainer.add(mView2);
                    mPagerAdapter.notifyDataSetChanged();
                    cityAdapter.notifyDataSetChanged();
                    if (Lists.notEmpty(cities)) {
                        // 以次级内容更新列表
                        mListView2.setAdapter(cityAdapter);
                        // 更新索引为次级
                        tabIndex = INDEX_TAB_CITY;
                        viewpager.setCurrentItem(tabIndex);
                    } else {
                        // 次级无内容，回调
                        callbackInternal();
                    }

                    break;

                case WHAT_COUNTIES_PROVIDED://更新乡镇列表
                    counties = (List<IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity>) msg.obj;
                    viewContainer.clear();
                    viewContainer.add(mView1);
                    viewContainer.add(mView2);
                    viewContainer.add(mView3);
                    mPagerAdapter.notifyDataSetChanged();
                    countyAdapter.notifyDataSetChanged();
                    if (Lists.notEmpty(counties)) {
                        mListView3.setAdapter(countyAdapter);
                        tabIndex = INDEX_TAB_COUNTY;
                        viewpager.setCurrentItem(tabIndex);
                    } else {
                        callbackInternal();
                    }

                    break;
            }

            updateTabsVisibility();
            updateIndicator();

            return true;
        }
    });
    private IndustryBean mIndustryBean;
    private View mView1, mView2, mView3;
    private ListView mListView1, mListView2, mListView3;
    private SelectPagerAdapter mPagerAdapter;


    public AddressSelector(Context context,IndustryBean industryBean) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mIndustryBean = industryBean;
        initViews();
        initAdapters();
        initData();
    }

    public void initData() {
        retrieveProvinces();
    }


    /**
     * 初始化布局
     */
    private void initViews() {
        viewContainer = new ArrayList<>();
        mView1 = inflater.inflate(R.layout.view_pager, null);
        mView2 = inflater.inflate(R.layout.view_pager, null);
        mView3 = inflater.inflate(R.layout.view_pager, null);
        mListView1 = (ListView) mView1.findViewById(R.id.listView);
        mListView2 = (ListView) mView2.findViewById(R.id.listView);
        mListView3 = (ListView) mView3.findViewById(R.id.listView);
        mListView1.setOnItemClickListener(this);
        mListView2.setOnItemClickListener(this);
        mListView3.setOnItemClickListener(this);
        viewContainer.add(mView1);

        view = inflater.inflate(R.layout.address_selector, null);
        this.iv_colse = (ImageView) view.findViewById(R.id.iv_colse);
        this.indicator = view.findViewById(R.id.indicator); //指示器
        this.layout_tab = (LinearLayout) view.findViewById(R.id.layout_tab);
        this.textViewProvince = (TextView) view.findViewById(R.id.textViewProvince);//省份
        this.textViewCity = (TextView) view.findViewById(R.id.textViewCity);//城市
        this.textViewCounty = (TextView) view.findViewById(R.id.textViewCounty);//区 乡镇

        viewpager = (ViewPager) view.findViewById(R.id.viewpager);
        mPagerAdapter = new SelectPagerAdapter();
        viewpager.setAdapter(mPagerAdapter);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabIndex = position;
                updateTabsVisibility();
                updateIndicator();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        this.textViewProvince.setOnClickListener(new OnProvinceTabClickListener());
        this.textViewCity.setOnClickListener(new OnCityTabClickListener());
        this.textViewCounty.setOnClickListener(new onCountyTabClickListener());

        //this.listView.setOnItemClickListener(this);
        this.iv_colse.setOnClickListener(new onCloseClickListener());

        updateIndicator();
    }

    /**
     * 设置字体选中的颜色
     */
    public void setTextSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
    }

    /**
     * 设置字体没有选中的颜色
     */
    public void setTextUnSelectedColor(int unSelectedColor) {
        this.unSelectedColor = unSelectedColor;
    }

    /**
     * 设置字体的大小
     */
    public void setTextSize(float dp) {
        textViewProvince.setTextSize(dp);
        textViewCity.setTextSize(dp);
        textViewCounty.setTextSize(dp);
    }

    /**
     * 设置字体的背景
     */
    public void setBackgroundColor(int colorId) {
        layout_tab.setBackgroundColor(context.getResources().getColor(colorId));
    }

    /**
     * 设置指示器的背景
     */
    public void setIndicatorBackgroundColor(int colorId) {
        indicator.setBackgroundColor(context.getResources().getColor(colorId));
    }

    /**
     * 设置指示器的背景
     */
    public void setIndicatorBackgroundColor(String color) {
        indicator.setBackgroundColor(Color.parseColor(color));
    }

    /**
     * 初始化adapter
     */
    private void initAdapters() {
        provinceAdapter = new ProvinceAdapter();
        cityAdapter = new CityAdapter();
        countyAdapter = new CountyAdapter();
    }

    /**
     * 更新tab 指示器
     */
    private void updateIndicator() {
        view.post(new Runnable() {
            @Override
            public void run() {
                switch (tabIndex) {
                    case INDEX_TAB_PROVINCE: //省份
                        buildIndicatorAnimatorTowards(textViewProvince).start();
                        break;
                    case INDEX_TAB_CITY: //城市
                        buildIndicatorAnimatorTowards(textViewCity).start();
                        break;
                    case INDEX_TAB_COUNTY: //乡镇
                        buildIndicatorAnimatorTowards(textViewCounty).start();
                        break;
                }
            }
        });
    }

    /**
     * tab 来回切换的动画
     *
     * @param tab
     * @return
     */
    private AnimatorSet buildIndicatorAnimatorTowards(TextView tab) {
        ObjectAnimator xAnimator = ObjectAnimator.ofFloat(indicator, "X", indicator.getX(), tab.getX());

        final ViewGroup.LayoutParams params = indicator.getLayoutParams();
        ValueAnimator widthAnimator = ValueAnimator.ofInt(params.width, tab.getMeasuredWidth());
        widthAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                params.width = (int) animation.getAnimatedValue();
                indicator.setLayoutParams(params);
            }
        });

        AnimatorSet set = new AnimatorSet();
        set.setInterpolator(new FastOutSlowInInterpolator());
        set.playTogether(xAnimator, widthAnimator);

        return set;
    }

    /**
     * 更新tab显示
     */
    private void updateTabsVisibility() {
        textViewProvince.setVisibility(Lists.notEmpty(provinces) ? View.VISIBLE : View.GONE);
        textViewCity.setVisibility(Lists.notEmpty(cities) ? View.VISIBLE : View.GONE);
        textViewCounty.setVisibility(Lists.notEmpty(counties) ? View.VISIBLE : View.GONE);
        //按钮能不能点击 false 不能点击 true 能点击
        textViewProvince.setEnabled(tabIndex != INDEX_TAB_PROVINCE);
        textViewCity.setEnabled(tabIndex != INDEX_TAB_CITY);
        textViewCounty.setEnabled(tabIndex != INDEX_TAB_COUNTY);
        if (selectedColor != 0 && unSelectedColor != 0) {
            updateTabTextColor();
        }
    }

    /**
     * 更新字体的颜色
     */
    private void updateTabTextColor() {
        if (tabIndex != INDEX_TAB_PROVINCE) {
            textViewProvince.setTextColor(context.getResources().getColor(unSelectedColor));
        } else {
            textViewProvince.setTextColor(context.getResources().getColor(selectedColor));
        }
        if (tabIndex != INDEX_TAB_CITY) {
            textViewCity.setTextColor(context.getResources().getColor(unSelectedColor));
        } else {
            textViewCity.setTextColor(context.getResources().getColor(selectedColor));
        }
        if (tabIndex != INDEX_TAB_COUNTY) {
            textViewCounty.setTextColor(context.getResources().getColor(unSelectedColor));
        } else {
            textViewCounty.setTextColor(context.getResources().getColor(selectedColor));
        }

    }

    /**
     * 点击省份的监听
     */
    class OnProvinceTabClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            tabIndex = INDEX_TAB_PROVINCE;
            //mListView1.setAdapter(provinceAdapter);
            viewpager.setCurrentItem(tabIndex);

            if (provinceIndex != INDEX_INVALID) {
                mListView1.setSelection(provinceIndex);
            }

            updateTabsVisibility();
            updateIndicator();
        }
    }

    /**
     * 点击城市的监听
     */
    class OnCityTabClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            tabIndex = INDEX_TAB_CITY;
            //mListView2.setAdapter(cityAdapter);
            viewpager.setCurrentItem(tabIndex);

            if (cityIndex != INDEX_INVALID) {
                mListView2.setSelection(cityIndex);
            }

            updateTabsVisibility();
            updateIndicator();
        }
    }

    /**
     * 点击区 乡镇的监听
     */
    class onCountyTabClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            tabIndex = INDEX_TAB_COUNTY;
            viewpager.setCurrentItem(tabIndex);

            if (countyIndex != INDEX_INVALID) {
                mListView3.setSelection(countyIndex);
            }

            updateTabsVisibility();
            updateIndicator();
        }
    }

    /**
     * 点击右边关闭dialog监听
     */
    class onCloseClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (dialogCloseListener != null) {
                dialogCloseListener.dialogclose();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (tabIndex) {
            case INDEX_TAB_PROVINCE: //省份
                IndustryBean.Industry1Entity province = provinceAdapter.getItem(position);

                // 更新当前级别及子级标签文本
                textViewProvince.setText(province.getIndustryName());
                textViewCity.setText("请选择");
                textViewCounty.setText("请选择");
                //根据省份的id,从数据库中查询城市列表
                List<IndustryBean.Industry1Entity.Industry2Entity> industry2 = province.getList();
                retrieveCitiesWith(industry2);

                // 清空子级数据
                cities = null;
                counties = null;
                cityAdapter.notifyDataSetChanged();
                countyAdapter.notifyDataSetChanged();
                // 更新已选中项
                this.provinceIndex = position;
                this.cityIndex = INDEX_INVALID;
                this.countyIndex = INDEX_INVALID;
                // 更新选中效果
                provinceAdapter.notifyDataSetChanged();
                break;
            case INDEX_TAB_CITY://城市
                IndustryBean.Industry1Entity.Industry2Entity city = cityAdapter.getItem(position);
                textViewCity.setText(city.getIndustryName());
                textViewCounty.setText("请选择");
                //根据城市的id,从数据库中查询城市列表
                List<IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity> industry3 = city.getList();
                retrieveCountiesWith(industry3);
                // 清空子级数据
                counties = null;
                countyAdapter.notifyDataSetChanged();
                // 更新已选中项
                counties = city.getList();
                this.cityIndex = position;
                this.countyIndex = INDEX_INVALID;
                // 更新选中效果
                cityAdapter.notifyDataSetChanged();
                break;
            case INDEX_TAB_COUNTY:
                IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity county = countyAdapter.getItem(position);
                textViewCounty.setText(county.getIndustryName());
                this.countyIndex = position;
                countyAdapter.notifyDataSetChanged();
                callbackInternal();
                break;
        }
    }


    /**
     * 查询省份列表
     */
    private void retrieveProvinces() {
        List<IndustryBean.Industry1Entity> provinceList = mIndustryBean.getData();
        handler.sendMessage(Message.obtain(handler, WHAT_PROVINCES_PROVIDED, provinceList));
    }

    /**
     * 根据省份id查询城市列表
     *
     * @param industry2
     */
    private void retrieveCitiesWith(List<IndustryBean.Industry1Entity.Industry2Entity> industry2) {
        handler.sendMessage(Message.obtain(handler, WHAT_CITIES_PROVIDED, industry2));
    }

    /**
     * 根据城市id查询乡镇列表
     *
     * @param industry3
     */
    private void retrieveCountiesWith(List<IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity> industry3) {
        handler.sendMessage(Message.obtain(handler, WHAT_COUNTIES_PROVIDED, industry3));
    }

    /**
     * 省份 城市 乡镇 都选中完 后的回调
     */
    private void callbackInternal() {
        if (listener != null) {
            IndustryBean.Industry1Entity province = provinces == null || provinceIndex == INDEX_INVALID ? null : provinces.get(provinceIndex);
            IndustryBean.Industry1Entity.Industry2Entity city = cities == null || cityIndex == INDEX_INVALID ? null : cities.get(cityIndex);
            IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity county = counties == null || countyIndex == INDEX_INVALID ? null : counties.get(countyIndex);
            listener.onAddressSelected(province, city, county);
        }
    }


    /**
     * 获得view
     *
     * @return
     */
    public View getView() {
        return view;
    }

    /**
     * 省份的adapter
     */
    class ProvinceAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return provinces == null ? 0 : provinces.size();
        }

        @Override
        public IndustryBean.Industry1Entity getItem(int position) {
            return provinces.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getIndustryId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_industry, parent, false);

                holder = new Holder();
                holder.textView = (TextView) convertView.findViewById(R.id.textView);
                holder.imageViewCheckMark = (ImageView) convertView.findViewById(R.id.imageViewCheckMark);

                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            IndustryBean.Industry1Entity item = getItem(position);
            holder.textView.setText(item.getIndustryName());

            boolean checked = provinceIndex != INDEX_INVALID && provinces.get(provinceIndex).getIndustryId() == item.getIndustryId();
            holder.textView.setEnabled(!checked);
            holder.imageViewCheckMark.setVisibility(checked ? View.VISIBLE : View.GONE);

            return convertView;
        }

        class Holder {
            TextView textView;
            ImageView imageViewCheckMark;
        }
    }

    /**
     * 城市的adaoter
     */
    class CityAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return cities == null ? 0 : cities.size();
        }

        @Override
        public IndustryBean.Industry1Entity.Industry2Entity getItem(int position) {
            return cities.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getIndustryId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_industry, parent, false);

                holder = new Holder();
                holder.textView = (TextView) convertView.findViewById(R.id.textView);
                holder.imageViewCheckMark = (ImageView) convertView.findViewById(R.id.imageViewCheckMark);

                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            IndustryBean.Industry1Entity.Industry2Entity item = getItem(position);
            holder.textView.setText(item.getIndustryName());

            boolean checked = cityIndex != INDEX_INVALID && cities.get(cityIndex).getIndustryId() == item.getIndustryId();
            holder.textView.setEnabled(!checked);
            holder.imageViewCheckMark.setVisibility(checked ? View.VISIBLE : View.GONE);

            return convertView;
        }

        class Holder {
            TextView textView;
            ImageView imageViewCheckMark;
        }
    }

    /**
     * 乡镇的adapter
     */
    class CountyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return counties == null ? 0 : counties.size();
        }

        @Override
        public IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity getItem(int position) {
            return counties.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getIndustryId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_industry, parent, false);

                holder = new Holder();
                holder.textView = (TextView) convertView.findViewById(R.id.textView);
                holder.imageViewCheckMark = (ImageView) convertView.findViewById(R.id.imageViewCheckMark);

                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity item = getItem(position);
            holder.textView.setText(item.getIndustryName());

            boolean checked = countyIndex != INDEX_INVALID && counties.get(countyIndex).getIndustryId() == item.getIndustryId();
            holder.textView.setEnabled(!checked);
            holder.imageViewCheckMark.setVisibility(checked ? View.VISIBLE : View.GONE);

            return convertView;
        }

        class Holder {
            TextView textView;
            ImageView imageViewCheckMark;
        }
    }


    public OnAddressSelectedListener getOnAddressSelectedListener() {
        return listener;
    }

    /**
     * 设置地址监听
     *
     * @param listener
     */
    public void setOnAddressSelectedListener(OnAddressSelectedListener listener) {
        this.listener = listener;
    }

    public interface OnDialogCloseListener {
        void dialogclose();
    }

    /**
     * 设置close监听
     */
    public void setOnDialogCloseListener(OnDialogCloseListener listener) {
        this.dialogCloseListener = listener;
    }

    public interface OnAddressSelectedListener {
        void onAddressSelected(IndustryBean.Industry1Entity province, IndustryBean.Industry1Entity.Industry2Entity city, IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity county);
    }

    class SelectPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return viewContainer.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(viewContainer.get(position));
            return viewContainer.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(viewContainer.get(position));
        }
    }

}
