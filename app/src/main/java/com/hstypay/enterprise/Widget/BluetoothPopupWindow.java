package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BluetoothDeviceBean;
import com.hstypay.enterprise.utils.HandlerManager;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.PreferenceUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.MarketListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by admin on 2017/7/11.
 */

public class BluetoothPopupWindow extends PopupWindow {

    private View mMenuView;
    private BaseActivity activity;
    private OnConnectPrinterListener mOnConnectPrinterListener;
    private MyAdaper myAdaper;
    private ViewPayTypeHolder holder;
    private BluetoothAdapter mBtAdapter;
    private List<BluetoothDeviceBean> deviceList;
    private MarketListView pairedListView;
    boolean connState;//连接状态
    private TextView tv_blue_scaning;
    private ScrollView ly_sv;
    /**
     * 蓝牙UUID
     */
    public static UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    /**
     * 配对成功后的蓝牙套接字
     */
    private BluetoothSocket mBluetoothSocket;
    private int mPrintDeviceType;

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1://连接失败
                    try {
                        getmBluetoothSocket().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    activity.dismissLoading();
                    activity.showCommonNoticeDialog(activity,"",UIUtils.getString(R.string.tx_blue_conn_fail));
                    break;
                case 2://蓝牙可以被搜索
                    break;
                case 3://设备已经接入
                    break;
                case 4://已连接某个设备
                    activity.dismissLoading();
                    /*if (iView != null) {
                        iView.setVisibility(View.VISIBLE);
                    }*/
                    myAdaper.notifyDataSetChanged();
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    LogUtil.i("zhouwei", "连接成功");
                    activity.showCommonNoticeDialog(activity,"",UIUtils.getString(R.string.tx_blue_conn_succ));
                    break;
                case HandlerManager.BLUE_CONNET_STUTS_CLOSED://蓝牙关闭
                    pairedListView.setVisibility(View.GONE);
                    tv_blue_scaning.setVisibility(View.GONE);
                    ly_sv.setVisibility(View.GONE);
                    break;
                case HandlerManager.BLUE_CONNET_STUTS://蓝牙已连接上后
                    if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
                        pairedListView.setVisibility(View.VISIBLE);
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        ly_sv.setVisibility(View.VISIBLE);
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        doDiscovery();
                    }
                    break;
            }
        }
    };

    public interface OnConnectPrinterListener {
        void onConnectPrinter();
    }

    public BluetoothPopupWindow(final BaseActivity context, int printDeviceType, OnConnectPrinterListener onConnectPrinterListener) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_bluetooth, null);
        this.activity = context;
        this.mOnConnectPrinterListener = onConnectPrinterListener;
        this.mPrintDeviceType = printDeviceType;
        TextView tvCancel = mMenuView.findViewById(R.id.tv_cancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.dialog_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
        initView(mMenuView);
        setLister();
        initValue();
        //注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        activity.registerReceiver(mReceiver, filter);
    }

    /**
     * eturn 返回 mBluetoothSocket
     */
    public BluetoothSocket getmBluetoothSocket() {
        return mBluetoothSocket;
    }

    /**
     * @return 返回 mBtAdapter
     */
    public BluetoothAdapter getmBtAdapter() {
        return mBtAdapter;
    }

    private void initValue() {
        MyApplication.setDeviceEnable(mPrintDeviceType, true);
        deviceList = new ArrayList<>();
        myAdaper = new MyAdaper(activity, deviceList);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        pairedListView.setAdapter(myAdaper);
        connState = PreferenceUtil.getBoolean("connState", true);

        if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
            pairedListView.setVisibility(View.VISIBLE);
            ly_sv.setVisibility(View.VISIBLE);
            if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(mPrintDeviceType))) {
                /*if (!mBtAdapter.isEnabled()) { //蓝牙没有打开
                    openBlue(false);
                } else {*/
                doDiscovery();
                //}
            } else {
                //默认去加载收索附件的蓝牙设备
                if (mBtAdapter == null) {//说明手机不带蓝牙模块
                    blueCloseState();
                    activity.showCommonNoticeDialog(activity,"",UIUtils.getString(R.string.tx_blue_no_device));
                    return;
                }
                doDiscovery();
            }
        } else {
            tv_blue_scaning.setVisibility(View.GONE);
            blueCloseState();
        }
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    public void doDiscovery() {
        tv_blue_scaning.setVisibility(View.VISIBLE);
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        deviceList.clear();
        mBtAdapter.startDiscovery();
    }

    void blueCloseState() {
        pairedListView.setVisibility(View.GONE);
        ly_sv.setVisibility(View.GONE);
    }

    /**
     * 尝试配对和连接
     *
     * @param btDev
     */
    public void createBond(BluetoothDevice btDev, Handler handler) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                activity.showNewLoading(true, UIUtils.getString(R.string.tx_blue_conn_deviceings));
            }
        });
        connect(btDev, handler);
    }

    /**
     * 尝试连接一个设备，子线程中完成，因为会线程阻塞
     *
     * @param btDev   蓝牙设备对象
     * @param handler 结果回调事件
     * @return
     */
    private void connect(BluetoothDevice btDev, Handler handler) {

        try {
            //通过和服务器协商的uuid来进行连接
            mBluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);

            if (mBluetoothSocket != null) {
                switch (mPrintDeviceType) {
                    case 1:
                        MyApplication.bluetoothSocket = mBluetoothSocket;
                        break;
                    case 2:
                        MyApplication.bluetoothSocket2 = mBluetoothSocket;
                        break;
                    case 3:
                        MyApplication.bluetoothSocket3 = mBluetoothSocket;
                        break;
                }
            }
            LogUtil.d("blueTooth", "开始连接...");
            //在建立之前调用
            if (getmBtAdapter().isDiscovering()) {
                //停止搜索
                getmBtAdapter().cancelDiscovery();
            }
            //如果当前socket处于非连接状态则调用连接
            if (!getmBluetoothSocket().isConnected()) {
                //你应当确保在调用connect()时设备没有执行搜索设备的操作。
                // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                getmBluetoothSocket().connect();
            }
            LogUtil.i("hehui", "已经链接");
            if (handler == null)
                return;
            Message message = Message.obtain();
            message.what = 4;
            message.obj = btDev;
            handler.sendMessage(message);
            for (int i = 0; i < deviceList.size(); i++) {
                if (deviceList.get(i).getBluetoothDevice().getAddress().equalsIgnoreCase(btDev.getAddress())) {
                    deviceList.get(i).setConnected(true);
                } else {
                    deviceList.get(i).setConnected(false);
                }
            }
            MyApplication.stayPrintDevice(mPrintDeviceType, btDev);
            if (mOnConnectPrinterListener != null) {
                mOnConnectPrinterListener.onConnectPrinter();
            }
        } catch (Exception e) {
            MyApplication.stayPrintDevice(mPrintDeviceType, btDev);
            LogUtil.e("hehui", "...链接失败" + e);
            Message message = Message.obtain();
            message.what = 1;
            handler.sendMessage(message);
            e.printStackTrace();
        }
    }

    /**
     * 按钮设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLister() {
        pairedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, final View view, int position, long arg3) {
                final BluetoothDevice bluetoothDevice = deviceList.get(position).getBluetoothDevice();

                activity.showNewLoading(false, UIUtils.getString(R.string.tx_blue_conn_deviceings));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        createBond(bluetoothDevice, handler);
                    }
                }).start();
            }
        });
    }

    /**
     * 打开蓝牙
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtil.i("zw", "mReceiver actioin-->" + action);
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                try {
                    if (device != null) { //蓝牙打印设备类似
                        for (int i = 0; i < deviceList.size(); i++) {
                            BluetoothDevice info = deviceList.get(i).getBluetoothDevice();
                            if (StringUtils.isEmptyOrNull(device.getName())
                                    || info.getAddress().equalsIgnoreCase(device.getAddress())) {
                                return;
                            }
                        }
                        if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
                            if (!StringUtils.isEmptyOrNull(device.getName())) {
                                BluetoothDeviceBean bluetoothDeviceBean = new BluetoothDeviceBean();
                                bluetoothDeviceBean.setBluetoothDevice(device);
                                String address = MyApplication.getBluetoothAddress(mPrintDeviceType);
                                if (!StringUtils.isEmptyOrNull(address) && address.equalsIgnoreCase(device.getAddress())) {//判断是否连接
                                    bluetoothDeviceBean.setConnected(true);
                                } else {
                                    bluetoothDeviceBean.setConnected(false);
                                }
                                deviceList.add(bluetoothDeviceBean);
                            }
                            myAdaper.notifyDataSetChanged();
                            myAdaper.notifyDataSetInvalidated();
                        }
                    }

                } catch (Exception e) {
                    LogUtil.e("zw", "" + e + ",device.getName()-->" + device.getName());
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                tv_blue_scaning.setVisibility(View.GONE);
                activity.dismissLoading();
            }
        }
    };

    /**
     * 页面参数 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(View view) {
        tv_blue_scaning = (TextView) view.findViewById(R.id.tv_blue_scaning);
        ly_sv = (ScrollView) view.findViewById(R.id.ly_sv);
        pairedListView = (MarketListView) view.findViewById(R.id.paired_devices);
    }

    class MyAdaper extends BaseAdapter {

        private List<BluetoothDeviceBean> list;

        private Context context;

        private MyAdaper(Context context, List<BluetoothDeviceBean> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = View.inflate(context, R.layout.item_pop_bluetooth, null);
                holder = new ViewPayTypeHolder();
                holder.iv_choice = (ImageView) convertView.findViewById(R.id.iv_choice);
                holder.userName = (TextView) convertView.findViewById(R.id.userName);
                convertView.setTag(holder);
            } else {
                holder = (ViewPayTypeHolder) convertView.getTag();
            }
            BluetoothDevice deviceInfo = list.get(position).getBluetoothDevice();
            if (!StringUtils.isEmptyOrNull(deviceInfo.getName())) {
                holder.userName.setText(deviceInfo.getName());
            }
            if (list.get(position).isConnected()) {//判断是否连接
                holder.iv_choice.setImageResource(R.mipmap.ic_radio_selected);
            } else {
                holder.iv_choice.setImageResource(R.mipmap.ic_radio_default);
            }
            return convertView;
        }
    }

    class ViewPayTypeHolder {
        private ImageView iv_choice;
        private TextView userName;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mBtAdapter != null && mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        activity.unregisterReceiver(mReceiver);
    }
}
