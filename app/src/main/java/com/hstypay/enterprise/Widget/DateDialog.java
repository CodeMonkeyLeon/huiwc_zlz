package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.hstypay.enterprise.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DateDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnSubmit;
    private OnClickOkListener mOnClickOkListener;
    private TextView mTvContent;
    private DatePicker mDatePicker;
    private Button mBtnEnsure,mBtnCancel;
    private long minDate;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public DateDialog(Activity context,int style, long minDate) {
        super(context,style);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_select_date, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.mContext = context;
        this.minDate = minDate;

        initView();
        initListener();
    }

    private void initView() {
        mDatePicker = (DatePicker) findViewById(R.id.datePicker);
        mBtnEnsure = (Button) findViewById(R.id.btn_ensure);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);

        mDatePicker.setMinDate(minDate);
    }

    private void initListener() {
        mBtnEnsure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Calendar calendar = Calendar.getInstance();
                calendar.set(mDatePicker.getYear(),
                        mDatePicker.getMonth(),
                        mDatePicker.getDayOfMonth());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = sdf.format(calendar.getTime());
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk(dateStr);
                }
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnClickOkListener {
        void clickOk(String date);
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.mOnClickOkListener = onClickOkListener;
    }

}
