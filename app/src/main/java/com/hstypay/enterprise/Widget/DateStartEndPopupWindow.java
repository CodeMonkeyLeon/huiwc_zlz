package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by admin on 2017/7/11.
 */

public class DateStartEndPopupWindow extends PopupWindow {

    private View mMenuView;
    private HandleTv handleTv;
    private Activity activity;
    private final FrameLayout mFlDateStart;
    private final FrameLayout mFlDateEnd;
    private TimePickerView mPvTime1;
    private TimePickerView mPvTime2;
    private String mStartTime;
    private String mEndTime;

    private final Button mBtnDateEnsure;

    public interface HandleTv {
        void getDate(String startTime, String endTime);
    }

    public DateStartEndPopupWindow(final Activity context, String startTime, String endTime, final HandleTv handleTv) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.popup_select_date, null);
        this.activity = context;
        this.mStartTime = startTime;
        this.mEndTime = endTime;
        this.handleTv = handleTv;
        mFlDateStart = (FrameLayout) mMenuView.findViewById(R.id.fl_date_start);
        mFlDateEnd = (FrameLayout) mMenuView.findViewById(R.id.fl_date_end);
        Button btnDateCancel = (Button) mMenuView.findViewById(R.id.btn_date_cancel);
        mBtnDateEnsure = (Button) mMenuView.findViewById(R.id.btn_date_ensure);

        btnDateCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mBtnDateEnsure.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                mPvTime1.returnData();
//                mPvTime2.returnData();
                if (DateUtil.getTime(mStartTime, "yyyy-MM-dd HH:mm:ss") > DateUtil.getTime(mEndTime, "yyyy-MM-dd HH:mm:ss")) {
                    MyToast.showToastShort("开始时间不能大于结束时间");
                    return;
                }
                handleTv.getDate(mStartTime, mEndTime);
                dismiss();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        initTimePicker();
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectStartCalender = DateUtil.getSelectCalender(mStartTime);
        Calendar selectEndCalender = DateUtil.getSelectCalender(mEndTime);
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -6);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        //时间选择器
        initStartTimePicker(DateUtil.getSelectCalender(mStartTime), startDate, endDate);
        Calendar selectEndEnableDate = DateUtil.getSelectCalender(mStartTime);
        selectEndEnableDate.add(selectEndEnableDate.DAY_OF_MONTH, 30);  //设置区间为31天
        selectEndEnableDate.set(selectEndEnableDate.get(Calendar.YEAR), selectEndEnableDate.get(Calendar.MONTH), selectEndEnableDate.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
        Calendar beforeCalendar = endDate.before(selectEndEnableDate) ? endDate : selectEndEnableDate;
        Calendar endSelectTime;
        if (DateUtil.getSelectCalender(mEndTime).before(DateUtil.getSelectCalender(mStartTime))) {
            endSelectTime = selectEndEnableDate;
        } else {
            if (DateUtil.getSelectCalender(mEndTime).before(selectEndEnableDate)) {
                endSelectTime = DateUtil.getSelectCalender(mEndTime);
            } else {
                endSelectTime = selectEndEnableDate;
            }
        }
        initEndTimePicker(endSelectTime, DateUtil.getSelectCalender(mStartTime), beforeCalendar);
    }

    private void initStartTimePicker(Calendar selectStartCalender, Calendar startDate, Calendar endDate) {
        mPvTime1 = new TimePickerView.Builder(activity, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                mStartTime = DateUtil.getSelectTime(date);
                Calendar endDate = Calendar.getInstance();
                endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
                Calendar selectEndEnableDate = DateUtil.getSelectCalender(mStartTime);
                selectEndEnableDate.add(selectEndEnableDate.DAY_OF_MONTH, 30);  //设置区间为31天
                selectEndEnableDate.set(selectEndEnableDate.get(Calendar.YEAR), selectEndEnableDate.get(Calendar.MONTH), selectEndEnableDate.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
                Calendar beforeCalendar = endDate.before(selectEndEnableDate) ? endDate : selectEndEnableDate;
                Calendar endSelectTime;
                if (DateUtil.getSelectCalender(mEndTime).before(DateUtil.getSelectCalender(mStartTime))) {
                    endSelectTime = endDate.before(selectEndEnableDate) ? endDate : selectEndEnableDate;
                } else {
                    if (DateUtil.getSelectCalender(mEndTime).before(selectEndEnableDate)) {
                        endSelectTime = DateUtil.getSelectCalender(mEndTime);
                    } else {
                        endSelectTime = selectEndEnableDate;
                    }
                }
                initEndTimePicker(endSelectTime, DateUtil.getSelectCalender(mStartTime), beforeCalendar);
            }
        })
                .setLayoutRes(R.layout.pickerview_start_end_time, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        TextView tvDateStart = (TextView) v.findViewById(R.id.tv_date_start);
                        tvDateStart.setVisibility(View.VISIBLE);
                    }
                })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, true, true, true})
                .setLabel("年", "月", "日", "时", "分", "秒") //设置空字符串以隐藏单位提示   hide label.isCenterLabel(false)
                .setDividerColor(UIUtils.getColor(R.color.home_line))
                .setContentSize(14)
                .setDate(selectStartCalender)
                .setRangDate(startDate, endDate)
                .setBackgroundId(0x00000000)
                .setOutSideCancelable(false)
                .setDecorView(mFlDateStart)
                .isCenterLabel(false)
                .setLineSpacingMultiplier(2.5f)
                .build();
        mPvTime1.show(false);
    }

    private void initEndTimePicker(Calendar endSelectTime, Calendar startDate, Calendar endDate) {
        mEndTime = DateUtil.getSelectTime(endSelectTime.getTime());
        mPvTime2 = new TimePickerView.Builder(activity, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                mEndTime = DateUtil.getSelectTime(date);
            }
        })
                .setLayoutRes(R.layout.pickerview_start_end_time, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        TextView tvDateEnd = (TextView) v.findViewById(R.id.tv_date_end);
                        tvDateEnd.setVisibility(View.VISIBLE);
                    }
                })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, true, true, true})
                .setLabel("年", "月", "日", "时", "分", "秒") //设置空字符串以隐藏单位提示   hide label.isCenterLabel(false)
                .setDividerColor(UIUtils.getColor(R.color.home_line))
                .setContentSize(14)
                .setDate(endSelectTime)
                .setRangDate(startDate, endDate)
                .setBackgroundId(0x00000000)
                .setDecorView(mFlDateEnd)
                .setOutSideCancelable(false)
                .isCenterLabel(false)
                .setLineSpacingMultiplier(2.5f)
                .build();
        mPvTime2.show(false);
    }
}
