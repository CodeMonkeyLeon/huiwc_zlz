/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class EditCommonDialog extends Dialog {
    private Context context;
    private TextView mTitle;
    private ViewGroup mRootView;
    private EditCommonDialog.HandleBtn handleBtn;
    private EditCommonDialog.HandleBtnCancel handleBtnCancel;

    private Button mBtnCancel, mBtnOk;
    private EditText mEtInput;
    private ImageView mIvEye;
    private boolean isNumberType;
    private boolean isOpenPwd;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public EditCommonDialog(Context context, String title, String hintString, boolean isPassword, EditCommonDialog.HandleBtn handleBtn, EditCommonDialog.HandleBtnCancel handleBtnCancel) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_edit, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        this.handleBtnCancel = handleBtnCancel;
        initView(title, hintString, isPassword);
        initListener(hintString);

    }

    /**
     * title
     * content 提示内容
     *
     * @param isNumberType 是否数字输入类型
     *                     <默认构造函数>
     */
    public EditCommonDialog(Context context, String title, String hintString, boolean isPassword, boolean isNumberType, EditCommonDialog.HandleBtn handleBtn, EditCommonDialog.HandleBtnCancel handleBtnCancel) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_edit, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        this.handleBtnCancel = handleBtnCancel;
        this.isNumberType = isNumberType;
        initView(title, hintString, isPassword);
        initListener(hintString);

    }


    private void initListener(final String hintString) {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (handleBtnCancel != null)
                    handleBtnCancel.handleCancelBtn();
            }

        });
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String setMoney = mEtInput.getText().toString().trim();
                if (!TextUtils.isEmpty(setMoney)) {
                    handleBtn.handleOkBtn(mEtInput.getText().toString().trim());
                    dismiss();
                } else {
                    MyToast.showToastShort(hintString);
                }
            }
        });
        mIvEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd, mIvEye, mEtInput);
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(String title, String hintString, boolean isPassword) {
        mBtnCancel = findViewById(R.id.btnCancel);
        mBtnOk = findViewById(R.id.btnOk);
        mTitle = findViewById(R.id.title);
        mEtInput = findViewById(R.id.et_input);
        mIvEye = findViewById(R.id.iv_eye_pwd);

        mTitle.setText(title);
        mEtInput.setHint(hintString);
        mEtInput.setSelection(mEtInput.getText().toString().trim().length());
        mEtInput.setInputType(isPassword ? InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);
        if (isNumberType) {
            mEtInput.setInputType(InputType.TYPE_CLASS_NUMBER);
            mEtInput.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }
        if (isPassword) {
            isOpenPwd = false;
            mIvEye.setImageResource(R.mipmap.ic_login_password_hiding);
            mEtInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvEye.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String edit);
    }

    public interface HandleBtnCancel {
        void handleCancelBtn();
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return !isOpen;
    }
}
