/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.ToastHelper;


/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class EditDialog extends Dialog
{
    private Context context;
    private ViewGroup mRootView;
    private EditDialog.HandleBtn handleBtn;

    private Button mBtnCancel,mBtnOk;
    private EditText mEtMoney;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public EditDialog(Context context ,String edit,EditDialog.HandleBtn handleBtn,EditDialog.HandleBtnCancel handleBtnCancel)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.dialog_edit, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        initView(edit);
        setLinster();

    }

    private void setLinster() {
        mBtnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }

        });
        mBtnOk.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String setMoney = mEtMoney.getText().toString().trim();
                if(!TextUtils.isEmpty(setMoney)) {
                    handleBtn.handleOkBtn(mEtMoney.getText().toString().trim());
                    dismiss();
                }else{
                    MyToast.showToastShort("请输入收款备注");
                }
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView(String edit) {
        mBtnCancel = (Button) findViewById(R.id.btnCancel);
        mBtnOk = (Button) findViewById(R.id.btnOk);
        mEtMoney = (EditText) findViewById(R.id.et_input);
        mEtMoney.setText(edit);
        mEtMoney.setSelection(mEtMoney.getText().toString().trim().length());
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn(String edit);
    }

    public interface HandleBtnCancel
    {
        void handleCancelBtn();
    }
}
