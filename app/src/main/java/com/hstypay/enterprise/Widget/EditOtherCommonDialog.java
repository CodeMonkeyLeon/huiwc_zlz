/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;


/**
 * 两行弹出提示框
 * <功能详细描述>
 * 
 * @author  Dean.zeng
 * @version  [版本号, 2020-02-06]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class EditOtherCommonDialog extends Dialog
{
    private Context context;
    private TextView mTitle;
    private ViewGroup mRootView;
    private EditOtherCommonDialog.HandleBtn handleBtn;

    private Button mBtnCancel,mBtnOk;
    private EditText mEtInput,mEtKey;
    private ImageView mIvEye;
    private boolean isOpenPwd;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public EditOtherCommonDialog(Context context , String title, String hintString, boolean isPassword, EditOtherCommonDialog.HandleBtn handleBtn, EditOtherCommonDialog.HandleBtnCancel handleBtnCancel)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.dialog_other_edit, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        initView(title, hintString,isPassword);
        initListener(hintString);

    }

    private void initListener(final String hintString) {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }

        });
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String setMoney = mEtInput.getText().toString().trim();
                String key = mEtKey.getText().toString().trim();
                if(!TextUtils.isEmpty(setMoney)&&!TextUtils.isEmpty(key)) {
                    handleBtn.handleOkBtn(mEtInput.getText().toString().trim(),key);
                    dismiss();
                }else{
                    MyToast.showToastShort(hintString);
                }
            }
        });
        mIvEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd,mIvEye, mEtInput);
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView(String title, String hintString,boolean isPassword) {
        mBtnCancel = findViewById(R.id.btnCancel);
        mBtnOk = findViewById(R.id.btnOk);
        mTitle = findViewById(R.id.title);
        mEtInput = findViewById(R.id.et_input);
        mEtKey = findViewById(R.id.edt_device_key);
        mIvEye = findViewById(R.id.iv_eye_pwd);

        mTitle.setText(title);
        mEtInput.setHint(hintString);
        mEtInput.setSelection(mEtInput.getText().toString().trim().length());
        mEtInput.setInputType(isPassword ? InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);

        if (isPassword) {
            isOpenPwd = false;
            mIvEye.setImageResource(R.mipmap.ic_login_password_hiding);
            mEtInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mIvEye.setVisibility(View.VISIBLE);
        }
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn {
        void handleOkBtn(String edit,String key);
    }

    public interface HandleBtnCancel {
        void handleCancelBtn();
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return !isOpen;
    }
}
