package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.UIUtils;


public class FilterCashierPopupWindow extends PopupWindow {
    private View mMenuView;
    private HandleBtn handleBtn;
    private Activity activity;

    private RelativeLayout mRlCashier;
    private Button mBtnReset, mBtnConfirm;
    private TextView mTvCashier;
    private ImageView mIvCashier;
    private String mUserId = "";
    private String mCashierName = "";

    private OnClickCashier mOnClickCashier;

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String cashierName, String userId);
    }

    public interface OnClickCashier {
        void onClickCashier();
    }

    public void setOnClickCashier(OnClickCashier onClickCashier) {
        mOnClickCashier = onClickCashier;
    }

    public void setCashier(String userId, String cashierName) {
        mUserId = userId;
        mCashierName = cashierName;
        mTvCashier.setText(cashierName);
    }

    public FilterCashierPopupWindow(Activity context, HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_cashier_filter, null);
        this.handleBtn = handleBtn;
        this.activity = context;

        initView(mMenuView);

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.popup_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int top = mMenuView.findViewById(R.id.pop_layout).getTop();
                int bottom = mMenuView.findViewById(R.id.pop_layout).getBottom();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top || y > bottom) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private void initView(View view) {

        mRlCashier = view.findViewById(R.id.rl_cashier);
        mTvCashier = view.findViewById(R.id.tv_cashier);
        mIvCashier = view.findViewById(R.id.iv_cashier);
        mBtnReset = view.findViewById(R.id.btn_reset);
        mBtnConfirm = view.findViewById(R.id.btn_confirm);

        if (MyApplication.getIsCasher() && TextUtils.isEmpty(MyApplication.getOpCodeReport())) {
            mIvCashier.setVisibility(View.INVISIBLE);
        } else {
            mIvCashier.setVisibility(View.VISIBLE);
            mRlCashier.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnClickCashier != null) {
                        mOnClickCashier.onClickCashier();
                    }
                }
            });
        }
        mBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
            }
        });
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                handleBtn.handleOkBtn(mCashierName, mUserId);
            }
        });
    }

    public void reset() {
        if (MyApplication.getIsCasher()) {
            mUserId = MyApplication.getUserId();
            mTvCashier.setText(MyApplication.getRealName());
            mCashierName = MyApplication.getRealName();
        } else {
            mUserId = "";
            mTvCashier.setText(R.string.tv_all_user);
            mCashierName = UIUtils.getString(R.string.tv_all_user);
        }
    }
}
