package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

public class FilterPledgePopupWindow extends PopupWindow {
    private View mMenuView;
    private HandleBtn handleBtn;
    private Activity activity;

    private Button mBtnReset, mBtnConfirm;
    private CheckBox mCbWxPay, mCbAliPay, mCbQqPay, mCbUnionPay;
    private CheckBox mCbNoPay, mCbPaySuccess, mCbPayUnFreeze,mCbPayCancel;
    private TextView mTvType, mTvState;

    private int[] apiProvider;
    private int[] tradeStatus;

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(int[] apiProvider, int[] tradeStatus);
    }

    public FilterPledgePopupWindow(Activity context, int[] apiProvider, int[] tradeStatus, HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_pledge_filter, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.apiProvider = apiProvider;
        this.tradeStatus = tradeStatus;
        initView(mMenuView);

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.popup_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int top = mMenuView.findViewById(R.id.pop_layout).getTop();
                int bottom = mMenuView.findViewById(R.id.pop_layout).getBottom();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top || y > bottom) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private void initView(View view) {
        //支付方式
        mTvType = (TextView) view.findViewById(R.id.tv_pay_type_title);
        mCbWxPay = (CheckBox) view.findViewById(R.id.cb_pay_wx);
        mCbAliPay = (CheckBox) view.findViewById(R.id.cb_pay_ali);
        mCbQqPay = (CheckBox) view.findViewById(R.id.cb_pay_qq);
        mCbUnionPay = (CheckBox) view.findViewById(R.id.cb_pay_union);

        //支付状态
        mTvState = (TextView) view.findViewById(R.id.tv_pay_state_title);
        mCbNoPay = (CheckBox) view.findViewById(R.id.cb_no_pay);
        mCbPaySuccess = (CheckBox) view.findViewById(R.id.cb_pay_success);
        mCbPayUnFreeze = (CheckBox) view.findViewById(R.id.cb_pay_unfreeze);
        mCbPayCancel = (CheckBox) view.findViewById(R.id.cb_pay_cancel);

        mBtnReset = (Button) view.findViewById(R.id.btn_reset);
        mBtnConfirm = (Button) view.findViewById(R.id.btn_confirm);

        mBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
                setView();
            }
        });
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                apiProvider = getApiProvider();
                tradeStatus = getTradeStatus();
                handleBtn.handleOkBtn(apiProvider, tradeStatus);
            }
        });

        showView();//回显
    }

    private void showView() {
        if (apiProvider != null && apiProvider.length > 0) {
            for (int i = 0; i < apiProvider.length; i++) {
                switch (apiProvider[i]) {
                    case 1:
                        mCbWxPay.setChecked(true);
                        break;
                    case 2:
                        mCbAliPay.setChecked(true);
                        break;
                    case 4:
                        mCbQqPay.setChecked(true);
                        break;
                    case 5:
                        mCbUnionPay.setChecked(true);
                        break;
                }
            }
        }
        if (tradeStatus != null && tradeStatus.length > 0) {
            for (int i = 0; i < tradeStatus.length; i++) {
                switch (tradeStatus[i]) {
                    case 1:
                        mCbNoPay.setChecked(true);
                        break;
                    case 2:
                        mCbPaySuccess.setChecked(true);
                        break;
                    case 5:
                        mCbPayUnFreeze.setChecked(true);
                        break;
                    case 9:
                        mCbPayCancel.setChecked(true);
                        break;
                }
            }
        }
    }

    private void setView() {
        mTvType.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));
        mCbWxPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbWxPay.setEnabled(true);
        mCbAliPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbAliPay.setEnabled(true);
        mCbQqPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbQqPay.setEnabled(true);
        mCbUnionPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbUnionPay.setEnabled(true);

        mTvState.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));
        mCbNoPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbNoPay.setEnabled(true);
        mCbPaySuccess.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbPaySuccess.setEnabled(true);
        mCbPayUnFreeze.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbPayUnFreeze.setEnabled(true);
        mCbPayCancel.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        mCbPayCancel.setEnabled(true);
    }


    public void reset() {
        mCbWxPay.setChecked(false);
        mCbAliPay.setChecked(false);
        mCbQqPay.setChecked(false);
        mCbUnionPay.setChecked(false);

        mCbPaySuccess.setChecked(false);
        mCbNoPay.setChecked(false);
        mCbPayUnFreeze.setChecked(false);
        mCbPayCancel.setChecked(false);
    }

    public int[] getApiProvider() {
        List<Integer> list = new ArrayList<>();
        if (mCbWxPay.isChecked()) {
            list.add(1);
        }
        if (mCbAliPay.isChecked()) {
            list.add(2);
        }
        if (mCbQqPay.isChecked()) {
            list.add(4);
        }
        if (mCbUnionPay.isChecked()) {
            list.add(5);
        }
        return listToArray(list);
    }

    public int[] getTradeStatus() {
        List<Integer> list = new ArrayList<>();
        if (mCbPaySuccess.isChecked()) {
            list.add(2);
        }
        if (mCbNoPay.isChecked()) {
            list.add(1);
        }
        if (mCbPayUnFreeze.isChecked()) {
            list.add(5);
        }
        if (mCbPayCancel.isChecked()) {
            list.add(9);
        }
        return listToArray(list);
    }

    public int[] listToArray(List<Integer> list) {
        if (list != null && list.size() > 0) {
            int[] array = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                array[i] = list.get(i);
            }
            return array;
        } else {
            return null;
        }
    }
}
