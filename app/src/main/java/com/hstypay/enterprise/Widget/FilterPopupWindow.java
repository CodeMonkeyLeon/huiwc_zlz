package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.FilterItemAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.CheckBean;
import com.hstypay.enterprise.bean.QueryPayTypeBean;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

public class FilterPopupWindow extends PopupWindow {
    private View mMenuView;
    private HandleBtn handleBtn;
    private Activity activity;

    private RelativeLayout mRlCashier;//收银员
    private Button mBtnReset, mBtnConfirm;//重置按钮  确定按钮
//    private CheckBox mCbWxPay, mCbAliPay, mCbQqPay, mCbUnionPay, mCbCardPay, mCbFukaPay, mCbPosFacePay, mCbWxFacePay;//微信支付,支付宝,QQ钱包,银联支付,刷卡支付,福卡支付,银联刷脸,微信刷脸
    private CheckBox mCbNoPay, mCbPaySuccess, mCbPayClose, mCbRefunding,mCbProcessingRefund,mCbSuccessRefund,mCbFailRefund;//未支付,支付成功,已关闭,转入退款,退款处理中,退款成功,退款失败
    private CheckBox mCbPayCommon, mCbPayScanOrder, mCbPayVlife, mCbPayFcard;//线下支付,扫码点餐,威到家,饭卡
    private CheckBox mCbPayTradeType,mCbVipRechargeTradeType,mCbVipResumeTradeType,mCbFeiqiMianxiTradeType;//支付,会员卡充值,会员卡消费,分期免息
//    private CheckBox mCbRefundSuccess,mCbRefundFailed,mCbReversal,mCbPayCancel;
//    private RadioButton mRbPayGet, mRbPayRefund, mRbVipRecharge, mRbPayVip;//支付收款 ,退款 ,会员卡充值 ,会员卡消费
    private TextView mTvType, mTvState, mTvCashier;//支付方式 交易状态 收银员名称
    private String mUserId = "";
    private String mCashierName = "";
    private LinearLayout mLlBusinessType;//业务类型
    private ImageView mIvCashier;//收银员箭头

    private int[] apiProvider;//当前确定选择的支付方式
    private int[] tradeStatus;//当前确定选择的收款交易状态： 2支付成功 1未支付  3已关闭  4转入退款
    private int[] refundStatus;//当前确定选择的退款交易状态： 1初始化  2退款成功  3退款失败  4未确定   5转入代发   6退款处理中
    private String otherType = "";//当前确定选择的其它类型：退款或收款支付
    private List<Integer> tradeTypeList;//当前确定选择的交易类型： 1会员卡充值  0收款支付  2会员卡消费
    private List<String> sceneList;//当前确定选择的业务类型： OFF_LINE线下支付  PAY_SCO扫码点餐
    private OnClickCashier mOnClickCashier;
    private RecyclerView mRecyclerView;//支付方式列表
    private FilterItemAdapter mAdapter;//支付方式的adapter
    private List<CheckBean> mApiProviderList;//所有的支付方式
    private RadioGroup mRgBillType;//账单类型：收款，退款
    private RadioButton mRbReceiveCashierBill;//收款
    private RadioButton mRbRefundCashierBill;//退款
    private LinearLayout mLlPayStateReceive;//收款时的交易状态
    private LinearLayout mLlPayStateRefund;//退款时的交易状态
    private View mViewPayTypeFilter;

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String cashierName, String userId, int[] apiProvider, int[] tradeStatus,int[] refundStatus, String otherType, List<Integer> tradeTypeList, List<String> sceneList);
    }

    public interface OnClickCashier {
        void onClickCashier();
    }

    public void setOnClickCashier(OnClickCashier onClickCashier) {
        mOnClickCashier = onClickCashier;
    }

    public void setCashier(String userId, String cashierName) {
        mUserId = userId;
        mCashierName = cashierName;
        mTvCashier.setText(cashierName);
    }

    public FilterPopupWindow(Activity context, int[] apiProvider, int[] tradeStatus,int[] refundStatus, String otherType, List<Integer> tradeTypeList, List<String> sceneList, HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_filter, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.apiProvider = apiProvider;
        this.tradeStatus = tradeStatus;
        this.refundStatus = refundStatus;
        this.otherType = otherType;
        this.tradeTypeList = tradeTypeList;
        this.sceneList = sceneList;

        initView(mMenuView);

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.popup_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int top = mMenuView.findViewById(R.id.pop_layout).getTop();
                int bottom = mMenuView.findViewById(R.id.pop_layout).getBottom();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top || y > bottom) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


    /*private void setView(int tradeType) {
        switch (tradeType) {
            case 1://重置
                mTvType.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));

                mTvState.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));
                mCbNoPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbNoPay.setEnabled(true);
                mCbPaySuccess.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPaySuccess.setEnabled(true);
                mCbPayClose.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayClose.setEnabled(true);
                mCbRefunding.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbRefunding.setEnabled(true);

                mCbPayCommon.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayCommon.setEnabled(true);
                mCbPayScanOrder.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayScanOrder.setEnabled(true);
                mCbPayVlife.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayVlife.setEnabled(true);
                mCbPayFcard.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayFcard.setEnabled(true);

                *//*mTvType.setTextColor(UIUtils.getColor(R.color.tv_no_pay));

                mTvState.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbNoPay.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbNoPay.setEnabled(false);
                mCbPaySuccess.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbPaySuccess.setEnabled(false);
                mCbPayClose.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbPayClose.setEnabled(false);
                mCbRefunding.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbRefunding.setEnabled(false);

                mCbPayCommon.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbPayCommon.setEnabled(false);
                mCbPayScanOrder.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbPayScanOrder.setEnabled(false);
                mCbPayVlife.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbPayVlife.setEnabled(false);
                mCbPayFcard.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                mCbPayFcard.setEnabled(false);
                setApiProviderList(false, mApiProviderList);*//*
                break;
            case 2://会员卡充值
                mTvType.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));

                mTvState.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));
                mCbNoPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbNoPay.setEnabled(true);
                mCbPaySuccess.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPaySuccess.setEnabled(true);
                mCbPayClose.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayClose.setEnabled(true);
                mCbRefunding.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbRefunding.setEnabled(true);

                mCbPayCommon.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayCommon.setEnabled(true);
                mCbPayScanOrder.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayScanOrder.setEnabled(true);
                mCbPayVlife.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayVlife.setEnabled(true);
                mCbPayFcard.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayFcard.setEnabled(true);
                setApiProviderList(true, mApiProviderList);
                break;
            case 3://会员卡消费
                mTvType.setTextColor(UIUtils.getColor(R.color.tv_no_pay));

                mTvState.setTextColor(UIUtils.getColor(R.color.home_top_bg_color));
                mCbNoPay.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbNoPay.setEnabled(true);
                mCbPaySuccess.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPaySuccess.setEnabled(true);
                mCbPayClose.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayClose.setEnabled(true);
                mCbRefunding.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbRefunding.setEnabled(true);

                mCbPayCommon.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayCommon.setEnabled(true);
                mCbPayScanOrder.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayScanOrder.setEnabled(true);
                mCbPayVlife.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayVlife.setEnabled(true);
                mCbPayFcard.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
                mCbPayFcard.setEnabled(true);
                setApiProviderList(false, mApiProviderList);
                break;
            default://支付收款，重置可以点击

                break;
        }
        mAdapter.notifyDataSetChanged();
    }*/

    private void initView(View view) {
        //账单类型
        mRgBillType = view.findViewById(R.id.rg_bill_type);
        mRbReceiveCashierBill = view.findViewById(R.id.rb_receive_cashier_bill);//收款即支付
        mRbRefundCashierBill = view.findViewById(R.id.rb_refund_cashier_bill);//退款

        //支付方式
        mTvType = (TextView) view.findViewById(R.id.tv_pay_type_title); //支付方式
        mViewPayTypeFilter = view.findViewById(R.id.view_pay_type_filter);
        mRecyclerView = view.findViewById(R.id.recyclerView); //支付方式列表
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 4);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mApiProviderList = getApiProviderList();
        mAdapter = new FilterItemAdapter(activity, mApiProviderList);
        mAdapter.setOnItemClickListener(new FilterItemAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position, boolean isChecked) {
                mApiProviderList.get(position).setChecked(isChecked);
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        //交易状态
        mTvState = (TextView) view.findViewById(R.id.tv_pay_state_title);//交易状态
        mLlPayStateReceive = view.findViewById(R.id.ll_pay_state_receive);//收款时的交易状态
        mCbNoPay = (CheckBox) view.findViewById(R.id.cb_no_pay);//未支付
        mCbPaySuccess = (CheckBox) view.findViewById(R.id.cb_pay_success);//支付成功
        mCbPayClose = (CheckBox) view.findViewById(R.id.cb_pay_close);//已关闭
        //mCbPayCancel = (CheckBox) view.findViewById(R.id.cb_pay_cancel);
        mCbRefunding = (CheckBox) view.findViewById(R.id.cb_pay_refunding);//转入退款
        /*mCbReversal = (CheckBox) view.findViewById(R.id.cb_pay_reversal);
        mCbRefundSuccess = (CheckBox) view.findViewById(R.id.cb_pay_refund_success);
        mCbRefundFailed = (CheckBox) view.findViewById(R.id.cb_pay_refund_failed);*/
        mLlPayStateRefund = view.findViewById(R.id.ll_pay_state_refund);//退款时的交易状态
        mLlPayStateRefund.setVisibility(View.GONE);
        mCbProcessingRefund = view.findViewById(R.id.cb_processing_refund); //处理中
        mCbSuccessRefund = view.findViewById(R.id.cb_success_refund);//退款成功
        mCbFailRefund = view.findViewById(R.id.cb_fail_refund);//退款失败

        //交易类型
        mCbPayTradeType = view.findViewById(R.id.cb_pay_trade_type);//支付
        mCbVipRechargeTradeType = view.findViewById(R.id.cb_vip_recharge_trade_type);//会员卡充值
        mCbVipResumeTradeType = view.findViewById(R.id.cb_vip_resume_trade_type);//会员卡消费
        mCbFeiqiMianxiTradeType = view.findViewById(R.id.cb_feiqi_mianxi_trade_type);//分期免息
        if (isShowVip()){
            mCbVipRechargeTradeType.setVisibility(View.VISIBLE);
            mCbVipResumeTradeType.setVisibility(View.VISIBLE);
        }else {
            mCbVipRechargeTradeType.setVisibility(View.GONE);
            mCbVipResumeTradeType.setVisibility(View.GONE);
        }
        if (isOpenHuabei()){
            mCbFeiqiMianxiTradeType.setVisibility(View.VISIBLE);
        }else {
            mCbFeiqiMianxiTradeType.setVisibility(View.GONE);
        }

        //业务类型
        mLlBusinessType = view.findViewById(R.id.ll_business_type);//业务类型
        mCbPayCommon = view.findViewById(R.id.cb_pay_common);//线下支付
        mCbPayScanOrder = view.findViewById(R.id.cb_pay_scan_order);//扫码点餐
        mCbPayVlife = view.findViewById(R.id.cb_pay_vlife);//威到家
        mCbPayFcard = view.findViewById(R.id.cb_pay_fcard);//饭卡
        if (AppHelper.getApkType() != 0) {
            mLlBusinessType.setVisibility(View.GONE);
        } else {
            mLlBusinessType.setVisibility(View.VISIBLE);
        }
        if (isOpenScanOrder()){
            mCbPayScanOrder.setVisibility(View.VISIBLE);
        }else {
            mCbPayScanOrder.setVisibility(View.GONE);
        }
        if (isOpenSendHome()){
            mCbPayVlife.setVisibility(View.VISIBLE);
        }else {
            mCbPayVlife.setVisibility(View.GONE);
        }
        if (MyApplication.isOpenFuncard()){
            mCbPayFcard.setVisibility(View.VISIBLE);
        }else {
            mCbPayFcard.setVisibility(View.GONE);
        }


        //收银员
        mRlCashier = view.findViewById(R.id.rl_cashier);//收银员
        mTvCashier = view.findViewById(R.id.tv_cashier);//收银员名称
        mIvCashier = view.findViewById(R.id.iv_cashier);//箭头
        mBtnReset = (Button) view.findViewById(R.id.btn_reset);//重置按钮
        mBtnConfirm = (Button) view.findViewById(R.id.btn_confirm);//确定按钮

        if (MyApplication.getIsCasher() && TextUtils.isEmpty(MyApplication.getOpCodeBill())) {
            mIvCashier.setVisibility(View.INVISIBLE);
        } else {
            mIvCashier.setVisibility(View.VISIBLE);
            mRlCashier.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnClickCashier != null) {
                        mOnClickCashier.onClickCashier();
                    }
                }
            });
        }

        mBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
            }
        });
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                if (mRbReceiveCashierBill.isChecked()){
                    otherType = "HPAY_RECEIVE_ORDER";//收款支付
                }
                if (mRbRefundCashierBill.isChecked()){
                    otherType = "HPAY_REFUND_ORDER";//退款
                }
                if (!mRbReceiveCashierBill.isChecked() && !mRbRefundCashierBill.isChecked()) {
                    otherType = "";
                }
                tradeTypeList = getTradeTypes();
                apiProvider = getApiProvider();
                tradeStatus = getTradeStatus();
                refundStatus = getRefundStatus();
                sceneList = getScene();
                handleBtn.handleOkBtn(mCashierName, mUserId, apiProvider, tradeStatus,refundStatus, otherType, tradeTypeList, sceneList);
            }
        });

        mRgBillType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_receive_cashier_bill:
                        //收款
                        if (AppHelper.getApkType() != 0) {
                            mLlBusinessType.setVisibility(View.GONE);
                        } else {
                            mLlBusinessType.setVisibility(View.VISIBLE);
                            setBusinessTypeEnable(true);
                        }

                        mLlPayStateReceive.setVisibility(View.VISIBLE);
                        mLlPayStateRefund.setVisibility(View.GONE);

                        mCbProcessingRefund.setChecked(false);
                        mCbSuccessRefund.setChecked(false);
                        mCbFailRefund.setChecked(false);
                        break;
                    case R.id.rb_refund_cashier_bill:
                        //退款
                        if (AppHelper.getApkType() != 0) {
                            mLlBusinessType.setVisibility(View.GONE);
                        } else {
                            mLlBusinessType.setVisibility(View.VISIBLE);
                            setBusinessTypeEnable(false);
                        }
                        mLlPayStateReceive.setVisibility(View.GONE);
                        mLlPayStateRefund.setVisibility(View.VISIBLE);

                        mCbNoPay.setChecked(false);
                        mCbPaySuccess.setChecked(false);
                        mCbPayClose.setChecked(false);
                        mCbRefunding.setChecked(false);

                        mCbPayCommon.setChecked(false);
                        mCbPayScanOrder.setChecked(false);
                        mCbPayVlife.setChecked(false);
                        mCbPayFcard.setChecked(false);
                        break;
                }
            }
        });

        /*mRbPayGet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setView(0);
                    if (!"HPAY_RECEIVE_ORDER".equals(otherType)) {
                        reset();
                    }
                }
            }
        });
        mRbPayRefund.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setView(1);
                    if (!"HPAY_REFUND_ORDER".equals(otherType)) {
                        reset();
                    }
                }
            }
        });
        mRbVipRecharge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setView(2);
                    if (!getTradeType(tradeTypeList, 1)) {
                        reset();
                    }
                }
            }
        });
        mRbPayVip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setView(3);
                    if (!getTradeType(tradeTypeList, 2)) {
                        reset();
                    }
                }
            }
        });*/
        //初始化一次且初始化时没数据
        //showView();//回显
    }

    //设置业务类型是否可用
    private void setBusinessTypeEnable(boolean b) {
        if (b){
            setCheckBoxEnable(mCbPayCommon,true);
            setCheckBoxEnable(mCbPayScanOrder,true);
            setCheckBoxEnable(mCbPayVlife,true);
            setCheckBoxEnable(mCbPayFcard,true);
        }else {
            setCheckBoxEnable(mCbPayCommon,false);
            setCheckBoxEnable(mCbPayScanOrder,false);
            setCheckBoxEnable(mCbPayVlife,false);
            setCheckBoxEnable(mCbPayFcard,false);
        }
    }

    private void setCheckBoxEnable(CheckBox cb,boolean b){
        if (b){
            cb.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
        }else {
            cb.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
        }
        cb.setEnabled(b);
    }

    //获取支付方式
    private List<CheckBean> getApiProviderList() {
        /*
        支付方式, 1:微信;2:支付宝;3:财付通;4:qq钱包;5:银联;6:世明;10:刷卡;11:福卡;12:饭卡支付;13:银联POS刷脸支付;14:猫酷支付;15:微信POS刷脸支付;16:美团点评支付;17:数字人民币;18:印享星券支付;99:未知;
        * */
        List<CheckBean> apiProviderList = new ArrayList<>();
        /*CheckBean checkWachat = new CheckBean();
        checkWachat.setId(1);
        checkWachat.setName("微信支付");
        CheckBean checkAlipay = new CheckBean();
        checkAlipay.setId(2);
        checkAlipay.setName("支付宝");
        CheckBean checkQQ = new CheckBean();
        checkQQ.setId(4);
        checkQQ.setName("QQ钱包");
        CheckBean checkUnion = new CheckBean();
        checkUnion.setId(5);
        checkUnion.setName("银联支付");
        CheckBean checkCardPay = new CheckBean();
        checkCardPay.setId(10);
        checkCardPay.setName("刷卡支付");
        CheckBean checkDigital = new CheckBean();
        checkDigital.setId(Constants.DIGIT_RMB_PAY_TYPE_API_CODE);
        checkDigital.setName("数字人民币");
        CheckBean checkFuka = new CheckBean();
        checkFuka.setId(11);
        checkFuka.setName("福卡支付");
        CheckBean checkUnionFace = new CheckBean();
        checkUnionFace.setId(13);
        checkUnionFace.setName("银联刷脸");
        CheckBean checkWachatFace = new CheckBean();
        checkWachatFace.setId(15);
        checkWachatFace.setName("微信刷脸");

        CheckBean vipRemainMoney = new CheckBean();
        vipRemainMoney.setId(6);
        vipRemainMoney.setName("会员卡余额");
        CheckBean fkRemainMoey = new CheckBean();
        fkRemainMoey.setId(12);
        fkRemainMoey.setName("饭卡余额");
        //回显：
        if (apiProvider != null && apiProvider.length > 0) {
            for (int i = 0; i < apiProvider.length; i++) {
                switch (apiProvider[i]) {
                    case 1:
                        checkWachat.setChecked(true);
                        break;
                    case 2:
                        checkAlipay.setChecked(true);
                        break;
                    *//*case 4:
                        checkQQ.setChecked(true);
                        break;*//*
                    case 5:
                        checkUnion.setChecked(true);
                        break;
                    case 10:
                        checkCardPay.setChecked(true);
                        break;
                    case Constants.DIGIT_RMB_PAY_TYPE_API_CODE:
                        checkDigital.setChecked(true);
                        break;
                    case 11:
                        checkFuka.setChecked(true);
                        break;
                    case 13:
                        checkUnionFace.setChecked(true);
                        break;
                    case 15:
                        checkWachatFace.setChecked(true);
                        break;
                    case 12:
                        fkRemainMoey.setChecked(true);
                        break;
                    case 6:
                        vipRemainMoney.setChecked(true);
                        break;
                }
            }
        }*/

        List<Integer> apis = new ArrayList<>(); //去重
        String allApiProvider = SpUtil.getString(activity, Constants.SP_ALL_APIPROVIDERS);
        if (!TextUtils.isEmpty(allApiProvider)){
            List<QueryPayTypeBean.Entity> apiProviders =  new Gson().fromJson(allApiProvider,  new TypeToken<List<QueryPayTypeBean.Entity>>() {}.getType());
            if (apiProviders!=null && apiProviders.size()>0){
                for (QueryPayTypeBean.Entity entity:apiProviders){
                    int apiProvider = entity.getApiProvider();
                    if (apiProvider==4 || apiProvider == 14 || apiProvider == 16){
                        // qq钱包
                        continue;
                    }
                    if (apiProvider==6){
                        //会员卡余额
                        if (!isShowVip()){
                            //没有开通会员
                            continue;
                        }
                    }

                    String apiProviderCnt = entity.getApiProviderCnt();
                    if (apiProvider!=-1 && !TextUtils.isEmpty(apiProviderCnt)){
                        if (!apis.contains(apiProvider)){
                            //去重
                            apis.add(apiProvider);
                            CheckBean checkBean = new CheckBean();
                            checkBean.setId(apiProvider);
                            if (apiProviderCnt.length()>5 && apiProviderCnt.contains("支付")){
                                //避免文案过长
                                String apiCn = apiProviderCnt.replace("支付", "");
                                checkBean.setName(apiCn);
                            }else {
                                checkBean.setName(apiProviderCnt);
                            }

                            if (apiProvider==11||apiProvider==13||apiProvider==15){
                                //福卡支付 银联刷脸 微信刷脸
                                if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.WJY)) {
                                    apiProviderList.add(checkBean);
                                }
                            }else {
                                apiProviderList.add(checkBean);
                            }
                        }
                    }
                }
            }
        }

        if (apiProviderList.size()==0){
            mTvType.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mViewPayTypeFilter.setVisibility(View.GONE);
        }

        /*if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.WJY)) {
            apiProviderList.add(checkFuka);
            apiProviderList.add(checkUnionFace);
            apiProviderList.add(checkWachatFace);
        }*/

        return apiProviderList;
    }

    private void setApiProviderList(boolean isClickedAble, List<CheckBean> apiProviderList) {
        if (apiProviderList != null && apiProviderList.size() > 0) {
            for (CheckBean checkBean : apiProviderList) {
                checkBean.setCheckAble(isClickedAble);//设置可点
                checkBean.setChecked(false);//设置状态为未选中
            }
            mAdapter.notifyDataSetChanged();
        }
    }
    //回显
    private void showView() {
        if (tradeStatus != null && tradeStatus.length > 0) {
            for (int i = 0; i < tradeStatus.length; i++) {
                switch (tradeStatus[i]) {
                    case 1:
                        mCbNoPay.setChecked(true);
                        break;
                    case 2:
                        mCbPaySuccess.setChecked(true);
                        break;
                    case 3:
                        mCbPayClose.setChecked(true);
                        break;
                    case 4:
                        mCbRefunding.setChecked(true);
                        break;
                }
            }
        }
            /** 退款状态
             INIT—初始化          1
             SUCCESS—退款成功     2
             FAIL—退款失败        3
             NOTSURE—未确定       4
             CHANGE—转入代发      5
             PROCESSING—退款处理中 6
             */
        if (refundStatus != null && refundStatus.length > 0) {
            for (int i = 0; i < refundStatus.length; i++) {
                switch (refundStatus[i]) {
                    case 2:
                        mCbSuccessRefund.setChecked(true);
                        break;
                    case 3:
                        mCbFailRefund.setChecked(true);
                        break;
                    case 6:
                        mCbProcessingRefund.setChecked(true);
                        break;
                }
            }
        }
        /*if (!TextUtils.isEmpty(otherType)) {
            if (otherType.equals("HPAY_REFUND_ORDER")) {
                mRgOther.check(mRbPayRefund.getId());
            } else if (otherType.equals("HPAY_RECEIVE_ORDER")) {
                mRgOther.check(mRbPayGet.getId());
            }
        }*/

        /*if (tradeTypeList != null && tradeTypeList.size() > 0) {
            //如果有多个值，需遍历
            for (int i = 0; i < tradeTypeList.size(); i++) {
                if (tradeTypeList.get(i) == 1) {
                    mRgOther.check(mRbVipRecharge.getId());
                } else if (tradeTypeList.get(i) == 2) {
                    mRgOther.check(mRbPayVip.getId());
                }
            }
        }*/
    }

    //重置为未选中状态
    public void reset() {

//        mRgBillType.clearCheck();
        mRbReceiveCashierBill.setChecked(true);

        mCbPayTradeType.setChecked(false);
        mCbVipRechargeTradeType.setChecked(false);
        mCbVipResumeTradeType.setChecked(false);
        mCbFeiqiMianxiTradeType.setChecked(false);

        mLlPayStateReceive.setVisibility(View.VISIBLE);
        mLlPayStateRefund.setVisibility(View.GONE);

        mCbPaySuccess.setChecked(false);
        mCbNoPay.setChecked(false);
        mCbPayClose.setChecked(false);
        mCbRefunding.setChecked(false);
        mCbProcessingRefund.setChecked(false);
        mCbSuccessRefund.setChecked(false);
        mCbFailRefund.setChecked(false);

        mCbPayCommon.setChecked(false);
        mCbPayScanOrder.setChecked(false);
        mCbPayVlife.setChecked(false);
        mCbPayFcard.setChecked(false);

        setApiProviderList(true, mApiProviderList);

        setBusinessTypeEnable(true);

        if (MyApplication.getIsCasher()) {
            mUserId = MyApplication.getUserId();
            mTvCashier.setText(MyApplication.getRealName());
        } else {
            mUserId = "";
            mTvCashier.setText(R.string.tv_all_user);
        }
    }

    //获取选择的支付方式
    public int[] getApiProvider() {
        List<Integer> list = new ArrayList<>();
        if (mApiProviderList != null && mApiProviderList.size() > 0) {
            for (CheckBean checkBean : mApiProviderList) {
                if (checkBean.isChecked())
                    list.add(checkBean.getId());
            }
        }
        /*if (mCbWxPay.isChecked()) {
            list.add(1);
        }
        if (mCbAliPay.isChecked()) {
            list.add(2);
        }
        if (mCbQqPay.isChecked()) {
            list.add(4);
        }
        if (mCbUnionPay.isChecked()) {
            list.add(5);
        }
        if (mCbCardPay.isChecked()) {
            list.add(10);
        }
        if (mCbFukaPay.isChecked()) {
            list.add(11);
        }
        if (mCbPosFacePay.isChecked()) {
            list.add(13);
        }
        if (mCbWxFacePay.isChecked()) {
            list.add(15);
        }*/
        return listToArray(list);
    }

    //获取选择的收款交易状态
    public int[] getTradeStatus() {
        List<Integer> list = new ArrayList<>();
        if (mCbPaySuccess.isChecked()) {
            list.add(2);
        }
        if (mCbNoPay.isChecked()) {
            list.add(1);
        }

        if (mCbPayClose.isChecked()) {
            list.add(3);
        }
        if (mCbRefunding.isChecked()) {
            list.add(4);
        }
        /*if (mCbReversal.isChecked()){
            list.add(8);
        }
        if (mCbPayCancel.isChecked()){
            list.add(9);
        }*/
        return listToArray(list);
    }

    //获取选择的退款交易状态
    public int[] getRefundStatus(){
        /** 退款状态
         INIT—初始化          1
         SUCCESS—退款成功     2
         FAIL—退款失败        3
         NOTSURE—未确定       4
         CHANGE—转入代发      5
         PROCESSING—退款处理中 6
         */
        List<Integer> list = new ArrayList<>();
        if (mCbProcessingRefund.isChecked()) {
            list.add(6);
        }
        if (mCbSuccessRefund.isChecked()) {
            list.add(2);
        }

        if (mCbFailRefund.isChecked()) {
            list.add(3);
        }
        return listToArray(list);
    }

    //获取选择的交易类型
    public List<Integer> getTradeTypes() {
        // 0:支付, 1:充值，2：会员卡消费，4：花呗分期
        List<Integer> list = new ArrayList<>();
        if (mCbPayTradeType.isChecked()){
            list.add(0);
        }
        if (mCbVipRechargeTradeType.isChecked()){
            list.add(1);
        }
        if (mCbVipResumeTradeType.isChecked()){
            list.add(2);
        }
        if (mCbFeiqiMianxiTradeType.isChecked()){
            list.add(4);
        }
        return list;
    }

    //获取选择的业务类型
    public List<String> getScene() {
        List<String> list = new ArrayList<>();
        if (mCbPayCommon.isChecked()) {
            list.add("OFF_LINE");
        }
        if (mCbPayScanOrder.isChecked()) {
            list.add("PAY_SCO");
        }
        if (mCbPayVlife.isChecked()) {
            list.add("WELIFE");
        }
        if (mCbPayFcard.isChecked()) {
            list.add("MEMEPPC_FC");
        }
        return list;
    }

    public int[] listToArray(List<Integer> list) {
        if (list != null && list.size() > 0) {
            int[] array = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                array[i] = list.get(i);
            }
            return array;
        } else {
            return null;
        }
    }

    public boolean getTradeType(List<Integer> tradeTypeList, int tradeType) {
        if (tradeTypeList == null || tradeTypeList.size() == 0) {
            return false;
        }
        for (int i = 0; i < tradeTypeList.size(); i++) {
            if (tradeTypeList.get(i) == tradeType) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return 是否开通会员
     */
    private boolean isShowVip(){
        return MyApplication.isOpenMember();
    }

    /**
     * @return 是否开通花呗分期
     */
    private boolean isOpenHuabei(){
        String config = MyApplication.getConfig();
        if (TextUtils.isEmpty(config)){
            return  MyApplication.isHuabeiWhite();
        }else {
            return MyApplication.isHuabeiWhite() && config.contains(Constants.CONFIG_HBFQ);
        }
    }

    /**
     * @return 是否开通扫码点餐
     */
    private boolean isOpenScanOrder(){
        String config = MyApplication.getConfig();
        if (TextUtils.isEmpty(config)){
            return  MyApplication.isOpenScanOrder();
        }else {
            return MyApplication.isOpenScanOrder() && config.contains(Constants.CONFIG_SCAN_ORDER_FOOD);
        }
    }

    /**
     * @return 是否开通威到家
     */
    private boolean isOpenSendHome(){
        return MyApplication.isOpenSendHome();
    }

}
