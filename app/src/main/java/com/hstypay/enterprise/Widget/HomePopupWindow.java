package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.UIUtils;

/**
 * Created by admin on 2017/7/11.
 */

public class HomePopupWindow extends PopupWindow implements View.OnClickListener {

    private View mMenuView,mViewLine;
    private OnSubmitListener mOnSubmitListener;
    private Activity activity;
    private LinearLayout mLlScanLogin,mLlScanVerify,mLlScanRefund;

    public interface OnSubmitListener {
        void submit(int index);
    }

    public HomePopupWindow(OnSubmitListener onSubmitListener, final Activity context,boolean isOpenFuncard) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_scan, null);
        this.activity = context;
        this.mOnSubmitListener = onSubmitListener;

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimTop);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.translucent));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        initView(isOpenFuncard);
        initListener();
    }

    private void initView(boolean isOpenFuncard) {
        mLlScanLogin = mMenuView.findViewById(R.id.ll_pop_scan_login);
        mLlScanVerify = mMenuView.findViewById(R.id.ll_pop_scan_verify);
        mLlScanRefund = mMenuView.findViewById(R.id.ll_pop_scan_refund);
        mViewLine = mMenuView.findViewById(R.id.view_line);
//        mLlScanVerify.setVisibility(isOpenFuncard ? View.VISIBLE : View.GONE);
//        mViewLine.setVisibility(isOpenFuncard ? View.VISIBLE : View.GONE);
    }

    private void initListener() {
        mLlScanLogin.setOnClickListener(this);
        mLlScanVerify.setOnClickListener(this);
        mLlScanRefund.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_pop_scan_login:
                if(mOnSubmitListener!=null) {
                    mOnSubmitListener.submit(0);
                    dismiss();
                }
                break;
            case R.id.ll_pop_scan_verify:
                if(mOnSubmitListener!=null) {
                    mOnSubmitListener.submit(1);
                    dismiss();
                }
                break;
            case R.id.ll_pop_scan_refund:
                if(mOnSubmitListener!=null) {
                    mOnSubmitListener.submit(2);
                    dismiss();
                }
                break;
        }
    }
}
