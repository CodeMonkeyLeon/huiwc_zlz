/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.UIUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class InputSNCodeDialog extends Dialog {
    private Context context;
    private ViewGroup mRootView;
    private InputSNCodeDialog.HandleBtn handleBtn;
    private InputSNCodeDialog.HandleBtnCancel handleBtnCancel;

    private Button mBtnCancel, mBtnOk;
    private EditText mEtMoney;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public InputSNCodeDialog(Context context, InputSNCodeDialog.HandleBtn handleBtn, InputSNCodeDialog.HandleBtnCancel handleBtnCancel) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_input_sn_code, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        this.handleBtnCancel = handleBtnCancel;
        initView();
        setLinster();
    }

    private void setLinster() {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mEtMoney.setText("");
                handleBtnCancel.handleCancelBtn();
            }

        });
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String setMoney = mEtMoney.getText().toString().trim();
                if (!TextUtils.isEmpty(setMoney)) {
                    handleBtn.handleOkBtn(mEtMoney.getText().toString().trim());
                    mEtMoney.setText("");
                    dismiss();
                } else {
                    MyToast.showToastShort(UIUtils.getString(R.string.hint_sn_code));
                }
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        mBtnCancel = (Button) findViewById(R.id.btnCancel);
        mBtnOk = (Button) findViewById(R.id.btnOk);
        mEtMoney = (EditText) findViewById(R.id.et_money);
        mEtMoney.setSelection(mEtMoney.getText().toString().trim().length());
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String edit);
    }

    public interface HandleBtnCancel {
        void handleCancelBtn();
    }
}
