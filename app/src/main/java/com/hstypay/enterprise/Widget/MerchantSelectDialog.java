package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.MerchantInfoAdapter;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.utils.LogUtil;

import java.util.List;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MerchantSelectDialog extends Dialog implements MerchantInfoAdapter.OnRecyclerViewItemClickListener {

    private ViewGroup mRootView;
    private Activity mContext;
    private ImageView mIvClose,mIvArrow;
    private OnClickItemListener mOnClickItemListener;
    private RecyclerView mRvMerchant;
    private List<MerchantIdBean.DataEntity> mData;
    private boolean mShouldScroll = true;
    private int mToPosition = 1;
    private CustomLinearLayoutManager mLinearLayoutManager;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public MerchantSelectDialog(Activity context, List<MerchantIdBean.DataEntity> data) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setWindowAnimations(R.style.AnimMiddle);
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_merchant_select, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        this.mData = data;
        initView();
    }

    private void initView() {
        mIvClose = (ImageView) mRootView.findViewById(R.id.iv_close);
        mRvMerchant = (RecyclerView) mRootView.findViewById(R.id.rv_merchant_info);
        mIvArrow = (ImageView) mRootView.findViewById(R.id.iv_arrow);
        mLinearLayoutManager = new CustomLinearLayoutManager(mContext);
        mRvMerchant.setLayoutManager(mLinearLayoutManager);
        MerchantInfoAdapter adapter = new MerchantInfoAdapter(mContext,mData);
        mRvMerchant.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mIvArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("mShouldScroll==="+mShouldScroll);
                if (mShouldScroll) {
                    smoothMoveToPosition(mRvMerchant, mToPosition, mLinearLayoutManager);
                }
            }
        });

        mRvMerchant.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                switch (newState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        mShouldScroll = false;
                        break;

                    //滚动停止
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        mShouldScroll = true;
                        break;
                }
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        dismiss();
        if (mOnClickItemListener != null){
            mOnClickItemListener.clickItem(position);
        }
    }

    public interface OnClickItemListener {
        void clickItem(int position);
    }

    public void setOnClickItemListener(OnClickItemListener onClickItemListener) {
        this.mOnClickItemListener = onClickItemListener;
    }

    private void smoothMoveToPosition(RecyclerView mRecyclerView, int position,CustomLinearLayoutManager linearLayoutManager) {
        // 第一个可见位置
        int firstItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
        // 最后一个可见位置
        int lastItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(mRecyclerView.getChildCount() - 1));

        LogUtil.d("mShouldScroll===3="+position+"----"+firstItem);
        if (position<mData.size()){
            if (position<mData.size()-1){
                position =firstItem+2;
            }else {
                position =firstItem+1;
            }
            mRecyclerView.smoothScrollToPosition(position);
//            mToPosition = position;
            LogUtil.d("mShouldScroll===2="+mToPosition);
        }
        /*if (position < firstItem) {
            // 如果跳转位置在第一个可见位置之前，就smoothScrollToPosition可以直接跳转
            mRecyclerView.smoothScrollToPosition(position);
        } else if (position <= lastItem) {
            // 跳转位置在第一个可见项之后，最后一个可见项之前
            // smoothScrollToPosition根本不会动，此时调用smoothScrollBy来滑动到指定位置
            int movePosition = position - firstItem;
            if (movePosition >= 0 && movePosition < mRecyclerView.getChildCount()) {
                int top = mRecyclerView.getChildAt(movePosition).getTop();
                mRecyclerView.smoothScrollBy(0, top);
            }
        } else {
            // 如果要跳转的位置在最后可见项之后，则先调用smoothScrollToPosition将要跳转的位置滚动到可见位置
            // 再通过onScrollStateChanged控制再次调用smoothMoveToPosition，执行上一个判断中的方法
            mRecyclerView.smoothScrollToPosition(position);
            mToPosition = position;
            mShouldScroll = true;
        }*/
    }


}
