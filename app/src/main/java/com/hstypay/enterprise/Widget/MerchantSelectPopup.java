package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.MerchantInfoAdapter;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MerchantSelectPopup extends PopupWindow implements MerchantInfoAdapter.OnRecyclerViewItemClickListener {

    private final View mMenuView;
    private ViewGroup mRootView;
    private Activity mContext;
    private ImageView mIvClose,mIvArrow;
    private OnClickItemListener mOnClickItemListener;
    private RecyclerView mRvMerchant;
    private List<MerchantIdBean.DataEntity> mData;
    private boolean mShouldScroll = true;
    private int mToPosition = 1;
    private CustomLinearLayoutManager mLinearLayoutManager;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public MerchantSelectPopup(Activity context, List<MerchantIdBean.DataEntity> data) {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.dialog_merchant_select, null);
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(false);
        this.setOutsideTouchable(false);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.dialog_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        /*mMenuView.setOnTouchListener(new View.OnTouchListener()
        {

            public boolean onTouch(View v, MotionEvent event)
            {
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });*/

        this.mContext = context;
        this.mData = data;
        initView(mMenuView);
    }

    private void initView(View view) {
        mIvClose = (ImageView) view.findViewById(R.id.iv_close);
        mRvMerchant = (RecyclerView) view.findViewById(R.id.rv_merchant_info);
        mIvArrow = (ImageView) view.findViewById(R.id.iv_arrow);
        mLinearLayoutManager = new CustomLinearLayoutManager(mContext);
        mRvMerchant.setLayoutManager(mLinearLayoutManager);
        MerchantInfoAdapter adapter = new MerchantInfoAdapter(mContext,mData);
        mRvMerchant.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpUtil.removeKey(Constants.SKEY);
                SpUtil.removeAll();
                dismiss();
            }
        });

        mIvArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("mShouldScroll==="+mShouldScroll);
                if (mShouldScroll) {
                    smoothMoveToPosition(mRvMerchant, mToPosition, mLinearLayoutManager);
                }
            }
        });

        mRvMerchant.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                switch (newState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        mShouldScroll = false;
                        break;

                    //滚动停止
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        mShouldScroll = true;
                        break;
                }
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        dismiss();
        if (mOnClickItemListener != null){
            mOnClickItemListener.clickItem(position);
        }
    }

    public interface OnClickItemListener {
        void clickItem(int position);
    }

    public void setOnClickItemListener(OnClickItemListener onClickItemListener) {
        this.mOnClickItemListener = onClickItemListener;
    }

    private void smoothMoveToPosition(RecyclerView mRecyclerView, int position, CustomLinearLayoutManager linearLayoutManager) {
        // 第一个可见位置
        int firstItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
        // 最后一个可见位置
        int lastItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(mRecyclerView.getChildCount() - 1));

        LogUtil.d("mShouldScroll===3="+position+"----"+firstItem);
        if (position<mData.size()){
            if (position<mData.size()-1){
                position =firstItem+2;
            }else {
                position =firstItem+1;
            }
            mRecyclerView.smoothScrollToPosition(position);
//            mToPosition = position;
            LogUtil.d("mShouldScroll===2="+mToPosition);
        }
        /*if (position < firstItem) {
            // 如果跳转位置在第一个可见位置之前，就smoothScrollToPosition可以直接跳转
            mRecyclerView.smoothScrollToPosition(position);
        } else if (position <= lastItem) {
            // 跳转位置在第一个可见项之后，最后一个可见项之前
            // smoothScrollToPosition根本不会动，此时调用smoothScrollBy来滑动到指定位置
            int movePosition = position - firstItem;
            if (movePosition >= 0 && movePosition < mRecyclerView.getChildCount()) {
                int top = mRecyclerView.getChildAt(movePosition).getTop();
                mRecyclerView.smoothScrollBy(0, top);
            }
        } else {
            // 如果要跳转的位置在最后可见项之后，则先调用smoothScrollToPosition将要跳转的位置滚动到可见位置
            // 再通过onScrollStateChanged控制再次调用smoothMoveToPosition，执行上一个判断中的方法
            mRecyclerView.smoothScrollToPosition(position);
            mToPosition = position;
            mShouldScroll = true;
        }*/
    }


}
