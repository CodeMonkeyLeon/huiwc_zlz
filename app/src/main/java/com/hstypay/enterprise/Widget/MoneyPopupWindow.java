package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.UIUtils;

import java.math.BigDecimal;

/**
 * Created by admin on 2017/7/11.
 */

public class MoneyPopupWindow extends PopupWindow implements View.OnClickListener {

    private View mMenuView;
    private OnSubmitListener mOnSubmitListener;
    private int type;
    private Activity activity;
    private LinearLayout num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, lay_point, lay_add, lay_delete, lay_clear, scan_pay;
    private TextView tv_money, tvPopTitle, tvEditTitle;
    private StringBuilder calculateBuilder;
    private double totalMoney;
    private ImageView ivClose;
    private VipPayDialog mDialog;
    private String mVipCardNo;

    public interface OnSubmitListener {
        void submit(double money);
    }

    public MoneyPopupWindow(OnSubmitListener onSubmitListener, final Activity context, int type, String vipCardNo) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.popup_set_money, null);
        this.activity = context;
        this.type = type;
        this.mVipCardNo = vipCardNo;
        this.mOnSubmitListener = onSubmitListener;

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        initView();
        initListener();
        initData();
    }

    private void initView() {
        num_0 = (LinearLayout) mMenuView.findViewById(R.id.num_0);
        num_1 = (LinearLayout) mMenuView.findViewById(R.id.num_1);
        num_2 = (LinearLayout) mMenuView.findViewById(R.id.num_2);
        num_3 = (LinearLayout) mMenuView.findViewById(R.id.num_3);
        num_4 = (LinearLayout) mMenuView.findViewById(R.id.num_4);
        num_5 = (LinearLayout) mMenuView.findViewById(R.id.num_5);
        num_6 = (LinearLayout) mMenuView.findViewById(R.id.num_6);
        num_7 = (LinearLayout) mMenuView.findViewById(R.id.num_7);
        num_8 = (LinearLayout) mMenuView.findViewById(R.id.num_8);
        num_9 = (LinearLayout) mMenuView.findViewById(R.id.num_9);
        lay_point = (LinearLayout) mMenuView.findViewById(R.id.lay_point);
        lay_add = (LinearLayout) mMenuView.findViewById(R.id.lay_add);
        lay_delete = (LinearLayout) mMenuView.findViewById(R.id.lay_delete);
        lay_clear = (LinearLayout) mMenuView.findViewById(R.id.lay_clear);
        scan_pay = (LinearLayout) mMenuView.findViewById(R.id.ly_to_scan_pay);
        tv_money = (TextView) mMenuView.findViewById(R.id.tv_money);

        ivClose = (ImageView) mMenuView.findViewById(R.id.iv_close);
        tvPopTitle = (TextView) mMenuView.findViewById(R.id.tv_pop_title);
        tvEditTitle = (TextView) mMenuView.findViewById(R.id.tv_edit_title);
        setPayButton(false);
    }

    private void initListener() {
        num_0.setOnClickListener(this);
        num_1.setOnClickListener(this);
        num_2.setOnClickListener(this);
        num_3.setOnClickListener(this);
        num_4.setOnClickListener(this);
        num_5.setOnClickListener(this);
        num_6.setOnClickListener(this);
        num_7.setOnClickListener(this);
        num_8.setOnClickListener(this);
        num_9.setOnClickListener(this);
        lay_point.setOnClickListener(this);
        lay_add.setOnClickListener(this);
        lay_delete.setOnClickListener(this);
        lay_clear.setOnClickListener(this);
        scan_pay.setOnClickListener(this);
        ivClose.setOnClickListener(this);
    }

    private void initData() {
        calculateBuilder = new StringBuilder();
        if (type == 0) {
            tvPopTitle.setText(UIUtils.getString(R.string.tv_pop_vip_verification));
            tvEditTitle.setText(UIUtils.getString(R.string.tv_edit_verification));
        } else if (type == 1) {
            tvPopTitle.setText(UIUtils.getString(R.string.tv_pop_vip_recharge));
            tvEditTitle.setText(UIUtils.getString(R.string.tv_edit_recharge));
        }
    }

    /*private void calculateProcess(Object object) {
        if (calculateBuilder.length() == 0) {
            *//*if (object instanceof Integer) {
                if (object.toString().equals("0")) {
                    return;
                }
            } else *//*if (object instanceof String) {
                if (object.equals(".") || object.equals("+")) {
                    return;
                }
            }
        } else {
            if (calculateBuilder.toString().contains("+")) {
                int addIndex = calculateBuilder.toString().lastIndexOf("+");
                if (addIndex == calculateBuilder.length() - 1) {
                    if (object.equals("+") || object.equals(".")) {
                        return;
                    }
                }
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (object.equals(".") && pointIndex > addIndex) {
                    return;
                }
            } else {
                if (calculateBuilder.toString().contains(".")) {
                    if (!calculateBuilder.toString().contains("+")) {
                        if (object.equals(".")) {
                            return;
                        }
                    }
                    if (calculateBuilder.toString().lastIndexOf(".") == calculateBuilder.length() - 1) {
                        if (object.equals("+")) {
                            calculateBuilder.substring(0, calculateBuilder.length() - 1);
                        }
                    }
                }
            }
        }
        calculateBuilder.append(object);
        calculateResult();
    }

    private void calculateResult() {
        if (TextUtils.isEmpty(calculateBuilder)) {
            setPayButton(false);
            tv_money.setText("");
        } else {
            BigDecimal bigSum = null;
            if (calculateBuilder.toString().contains("+")) {
                String[] numbers = calculateBuilder.toString().split("\\+");
                if (numbers.length == 1) {
                    LogUtil.d("number1==", numbers[0]);
                    bigSum = new BigDecimal(numbers[0]);
                } else {
                    bigSum = new BigDecimal(0);
                    if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                        String number = numbers[numbers.length - 1];
                        numbers[numbers.length - 1] = number.substring(0, number.length());
                    }
                    for (int i = 0; i < numbers.length; i++) {
                        BigDecimal big = new BigDecimal(numbers[i]);
                        bigSum = bigSum.add(big);
                    }
                }
            } else {
                String calculateString = calculateBuilder.toString();
                if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                    bigSum = new BigDecimal(calculateString.substring(0, calculateString.length() - 1));
                } else {
                    bigSum = new BigDecimal(calculateString);
                }
                //bigSum = new BigDecimal(calculateString);
            }
            if (bigSum.doubleValue() > 50000) {
                MyToast.showToast(UIUtils.getString(R.string.tv_pay_exceed_money), Toast.LENGTH_SHORT);
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                return;
            }
            if (bigSum.doubleValue() <= 0) {
                setPayButton(false);
            } else {
                setPayButton(true);
            }
            totalMoney = bigSum.doubleValue();
            //tv_money.setText(DateUtil.formatPaseMoneyUtil(bigSum));
            tv_money.setText(bigSum.toString());
        }
    }*/

    private void deleteData() {
        /*calculateBuilder = new StringBuilder();
        calculateBuilder.append(tv_money.getText().toString().trim());
        if (calculateBuilder.length() > 0) {
            calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
        }
        calculateResult();*/
        if (calculateBuilder.length() > 0) {
            calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
        }
        calculateResult();
    }

    private void clearData() {
        calculateBuilder.delete(0, calculateBuilder.length());
        tv_money.setText("");
        setPayButton(false);
    }

    private void calculateProcess(Object object) {
        if (calculateBuilder.length() == 0) {
            if (object.toString().equals(".")) {
                calculateBuilder.append("0");
            }
        }else {
            if (calculateBuilder.length() == 1 && calculateBuilder.indexOf("0")==0){
                if (object instanceof Integer){
                    calculateBuilder.delete(0,1);
                }
            }else {
                if (calculateBuilder.toString().contains(".")){
                    if (object.toString().equals(".")){
                        return;
                    }
                    if (calculateBuilder.length()>=3 && calculateBuilder.lastIndexOf(".")==calculateBuilder.length()-3){
                        return;
                    }
                }
            }
        }
        calculateBuilder.append(object);
        calculateResult();
    }

    private void calculateResult(){
        if (TextUtils.isEmpty(calculateBuilder)) {
            setPayButton(false);
            tv_money.setText("");
        } else {
            BigDecimal bigSum = new BigDecimal(calculateBuilder.toString());
            if (bigSum.doubleValue() > 300000) {
                MyToast.showToast(UIUtils.getString(R.string.tv_pay_money_once), Toast.LENGTH_SHORT);
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                return;
            }
            if (bigSum.doubleValue() <= 0) {
                setPayButton(false);
            } else {
                setPayButton(true);
            }
            totalMoney = bigSum.doubleValue();
            tv_money.setText(calculateBuilder.toString());
        }
    }

    private void setPayButton(boolean enable) {
        scan_pay.setEnabled(enable);
        scan_pay.setBackgroundResource(enable ? R.drawable.selector_vip_pay_enable : R.color.theme_color_unsure);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.num_0:
                AppHelper.execVibrator(activity);
                calculateProcess(0);
                break;
            case R.id.num_1:
                AppHelper.execVibrator(activity);
                calculateProcess(1);
                break;
            case R.id.num_2:
                AppHelper.execVibrator(activity);
                calculateProcess(2);
                break;
            case R.id.num_3:
                AppHelper.execVibrator(activity);
                calculateProcess(3);
                break;
            case R.id.num_4:
                AppHelper.execVibrator(activity);
                calculateProcess(4);
                break;
            case R.id.num_5:
                AppHelper.execVibrator(activity);
                calculateProcess(5);
                break;
            case R.id.num_6:
                AppHelper.execVibrator(activity);
                calculateProcess(6);
                break;
            case R.id.num_7:
                AppHelper.execVibrator(activity);
                calculateProcess(7);
                break;
            case R.id.num_8:
                AppHelper.execVibrator(activity);
                calculateProcess(8);
                break;
            case R.id.num_9:
                AppHelper.execVibrator(activity);
                calculateProcess(9);
                break;
            //加号
            case R.id.lay_add:
                AppHelper.execVibrator(activity);
                calculateProcess("+");
                break;
            //点
            case R.id.lay_point:
                AppHelper.execVibrator(activity);
                calculateProcess(".");
                break;
            //删除
            case R.id.lay_delete:
                AppHelper.execVibrator(activity);
                deleteData();
                break;
            //清除
            case R.id.lay_clear:
                AppHelper.execVibrator(activity);
                clearData();
                break;
            case R.id.ly_to_scan_pay:
                AppHelper.execVibrator(activity);
                dismiss();
                showDialog();
                break;
            case R.id.iv_close:
                dismiss();
                break;
        }
    }

    private void showDialog() {
        if (mDialog == null) {
            mDialog = new VipPayDialog(activity, mVipCardNo, type, totalMoney);
            mDialog.setOnClickOkListener(new VipPayDialog.OnClickOkListener() {
                @Override
                public void clickOk(double money) {
                    mOnSubmitListener.submit(money);
                }
            });
        }
        mDialog.show();
    }
}
