/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.WelcomeActivity;
import com.hstypay.enterprise.utils.StringUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MyDialogInfo extends Dialog
{
    private Context context;
    private TextView title;
    private TextView content;
    private Button btnOk;
    private Button btnCancel;
    private ViewGroup mRootView;
    private MyDialogInfo.HandleBtn handleBtn;
    private MyDialogInfo.HandleBtnCancle cancleBtn;
    private View line_img, line_img_bt;
    // 更多
    public static final int FLAG = 0;
    // 最终提交
    public static final int SUBMIT = 1;
    // 注册成功
    public static final int REGISTFLAG = 2;
    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;
    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;
    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;
    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;
    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;
    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;
    public static final int SUBMIT_SHOP = 9;
    public static final int SCAN_PAY = 10;
    public static final int VARD = 11;
    public static final int UNIFED_DIALOG = 12;
    private OnItemLongDelListener mDelListener;
    private int position;
    private OnSubmitCouponListener mOnSubmitCouponListener;
    private LinearLayout ll_title;
    private View v_line;
    private TextView content2;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public MyDialogInfo(Context context,String contentStr,String btnOkStr, String btnCancle, MyDialogInfo.HandleBtn handleBtn){
        this(context,null,contentStr,null,false,btnOkStr,btnCancle,UNIFED_DIALOG,handleBtn,null);
    }

    public MyDialogInfo(Context context, String titleStr, String contentStr,String contentStr2,boolean isShowTitleLine, String btnOkStr, String btnCancle,
                        int flagMore, MyDialogInfo.HandleBtn handleBtn, MyDialogInfo.HandleBtnCancle cancleBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.my_dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;

        initView(titleStr, contentStr, btnOkStr, btnCancle, flagMore);
        if (!StringUtils.isEmptyOrNull(titleStr))
        {
            //ll_title.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            title.setText(titleStr);
        }

        if(isShowTitleLine){
            v_line.setVisibility(View.VISIBLE);
        }

        if(!StringUtils.isEmptyOrNull(contentStr)){
            content.setVisibility(View.VISIBLE);
            content.setText(contentStr);
        }

        if(!StringUtils.isEmptyOrNull(contentStr2)){
            content2.setVisibility(View.VISIBLE);
            content2.setText(contentStr2);
        }
        setLinster(flagMore);

    }

    public void setBtnOkText(String string)
    {
        if (btnOk != null)
        {
            btnOk.setText(string);
        }
    }

    public MyDialogInfo(Context context, String titleStr, String contentStr,String contentStr2,boolean isShowTitleLine, String btnOkStr, int flagMore,
                        MyDialogInfo.HandleBtn handleBtn, MyDialogInfo.HandleBtnCancle cancleBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.my_dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        
        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;
        initView(titleStr, contentStr, btnOkStr, null, flagMore);
        if (!StringUtils.isEmptyOrNull(titleStr))
        {
            //ll_title.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            title.setText(titleStr);
            v_line.setVisibility(View.VISIBLE);
        }

        if(isShowTitleLine){
            v_line.setVisibility(View.VISIBLE);
        }

        if(!StringUtils.isEmptyOrNull(contentStr)){
            content.setVisibility(View.VISIBLE);
            content.setText(contentStr);
        }

        if(!StringUtils.isEmptyOrNull(contentStr2)){
            content2.setVisibility(View.VISIBLE);
            content2.setText(contentStr2);
        }
        setLinster(flagMore);
        
    }
    
    public void setMessage(String msg)
    {
        this.content.setText(msg);
    }
    
    public void setmDelListener(OnItemLongDelListener mDelListener, int position)
    {
        this.mDelListener = mDelListener;
        this.position = position;
    }
    
    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener)
    {
        this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster(final int flagMore)
    {
        
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                switch (flagMore)
                {
                    case SUBMIT_COUPON_INFO:
                        mOnSubmitCouponListener.onSubmitCouponListenerCancel();
                        break;
                    case EXITAUTH:
                        cancleBtn.handleCancleBtn();
                        break;
                    case UNIFED_DIALOG:
                        dismiss();
                        handleBtn.handleCancleBtn();
                        break;
                }
            }
            
        });
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                switch (flagMore)
                {
                
                    case UNIFED_DIALOG:
                        handleBtn.handleOkBtn();
                        dismiss();
                        break;
                    // 更多 资料未完善
                    // case FLAG:
                    // showPage(ShopkeeperActivity.class);
                    // break;
                    // 注册欢迎提示
                    case REGISTFLAG:
                        cancel();
                        break;
                    
                    case SUBMIT:
                        Toast.makeText(getContext(), "提交成功!", Toast.LENGTH_SHORT).show();
                        break;
                    // 是否授权弹出框
                    case EXITAUTH: // 调转到服务热线
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    
                    // 是否授权弹出框
                    case SUBMIT_SHOP: // 调转到服务热线
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    
                    case SUBMITSHOPINFO:
                    case NETWORKSTATUE:
                    case FLAG:
                        cancel();
                        ((Activity)context).finish();
                        break;
                    
                    case EXITAUTHLOGIN: // 激活从新登录
                        Intent login_intent = new Intent(context, WelcomeActivity.class);
                        context.startActivity(login_intent);
                        Activity activity = (Activity)context;
                        activity.finish();
                        break;
                    case SUBMIT_DEL_COUPON_INFO:
                        mDelListener.onItemLongDelMessage(position);
                        break;
                    case SUBMIT_COUPON_INFO:
                        mOnSubmitCouponListener.onSubmitCouponListenerOk();
                        break;
                    case VARD:
                        handleBtn.handleOkBtn();
                        break;
                }
            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView(String titleStr, String contentStr, String btnOkStr, String btnCancelStr, int flag)
    {
        v_line = findViewById(R.id.v_line);
        ll_title = (LinearLayout)findViewById(R.id.ll_title);
        title = (TextView)findViewById(R.id.title);
        content = (TextView)findViewById(R.id.content1);
        content2 = (TextView)findViewById(R.id.content2);
        btnOk = (Button)findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        line_img = (View)findViewById(R.id.line_img);
        line_img_bt = (View)findViewById(R.id.line_img_bt);
        switch (flag) {
            case UNIFED_DIALOG:
                if (!TextUtils.isEmpty(btnCancelStr)){
                    line_img.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.VISIBLE);
                    btnCancel.setText(btnCancelStr);
                }
                break;
            case REGISTFLAG: // 注册成功
                btnCancel.setVisibility(View.GONE);
                title.setTextColor(context.getResources().getColor(R.color.prompt_title));
                content.setTextColor(context.getResources().getColor(R.color.tv_content_title));
                line_img.setVisibility(View.GONE);
                break;
            case EXITAUTHLOGIN: // 激活从新登录
                btnOk.setTextColor(Color.BLUE);
                break;
            case EXITAUTH:
                if (null == btnCancelStr) {
                    btnCancel.setText(R.string.tx_go_on);
                }
                else {
                    btnCancel.setText(btnCancelStr);
                }
                btnCancel.setVisibility(View.VISIBLE);
                line_img.setVisibility(View.VISIBLE);
                //                btnOk.setTextColor(Color.BLUE);
                break;
            case FLAG:
                btnCancel.setVisibility(View.GONE);
                line_img.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        
        title.setText(titleStr);
        content.setText(contentStr);
        btnOk.setText(btnOkStr);
        if (flag == SCAN_PAY) {
            btnOk.setVisibility(View.GONE);
            line_img.setVisibility(View.GONE);
            line_img_bt.setVisibility(View.GONE);
        }
    }
    
    public void showPage(Class clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn();
        
        void handleCancleBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnCancle
    {
        void handleCancleBtn();
    }
    
    /***
     * 
     * 长按删除优惠
     * 
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener
    {
        void onItemLongDelMessage(int position);
    }
    
    /***
     * 
     * 发布和修改优惠价提示
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener
    {
        void onSubmitCouponListenerOk();
        
        void onSubmitCouponListenerCancel();
    }
}
