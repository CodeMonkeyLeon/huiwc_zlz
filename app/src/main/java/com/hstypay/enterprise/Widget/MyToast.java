package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-11-27
 * Time: 下午9:17
 * To change this template use File | Settings | File Templates.
 */
public class MyToast
{
    private static MyToast mToast;
    
    private static Handler mHandler = new Handler();

    private static LayoutInflater mInflater;

    private static Runnable r = new Runnable()
    {
        public void run()
        {
            //mToast.cancel();
        }
    };

    public static void showToastShort(Activity activity, final String msg){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(msg)) {
                    showToast(MyApplication.getContext(), msg, Toast.LENGTH_SHORT);
                }
            }
        });

    }

    public static void showToastShort(String msg){
        if (!TextUtils.isEmpty(msg)) {
            showToast(MyApplication.getContext(), msg, Toast.LENGTH_SHORT);
        }
    }

    public static void showToastLong(String msg){
        showToast(MyApplication.getContext(),msg,Toast.LENGTH_LONG);
    }

    public static void showToast(String msg, int length){
        showToast(MyApplication.getContext(),msg,length);
    }
    
    public static void showToast(Context context, String msg, int length)
    {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastRoot = mInflater.inflate(R.layout.view_toast, null);
        TextView message = (TextView)toastRoot.findViewById(R.id.tv_content);
        message.setText(msg);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(Gravity.CENTER, 0, 0);
        
        toastStart.setDuration(Toast.LENGTH_SHORT);
        toastStart.setView(toastRoot);
        toastStart.show();
    }
    
}
