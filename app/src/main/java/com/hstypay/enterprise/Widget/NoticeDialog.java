package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class NoticeDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnSubmit;
    private OnClickOkListener mOnClickOkListener;
    private TextView mTvContent;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public NoticeDialog(Activity context, String contentText, String btnText, int res) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(res, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        initView(contentText, btnText);
    }

    private void initView(String contentText, String btnText) {
        mBtnSubmit = (Button) mRootView.findViewById(R.id.btn_submit);
        mTvContent = (TextView) mRootView.findViewById(R.id.dialog_content_text);
        if (!TextUtils.isEmpty(contentText)) {
            mTvContent.setText(contentText);
        }
        if (!TextUtils.isEmpty(btnText)) {
            mBtnSubmit.setText(btnText);
        }
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk();
                }
            }
        });
    }

    public interface OnClickOkListener {
        void clickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.mOnClickOkListener = onClickOkListener;
    }

    public void setMessage(String msg)
    {
        this.mTvContent.setText(msg);
    }
}
