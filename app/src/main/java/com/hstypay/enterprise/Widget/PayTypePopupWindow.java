package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.hstypay.enterprise.R;

/**
 * Created by admin on 2017/7/11.
 */

public class PayTypePopupWindow extends PopupWindow {

    private View mMenuView;
    private Activity activity;
    private LinearLayout mLlReceiveQrcode,mLlReceiveScan,mLlReceiveBcard;
    private RelativeLayout mRlReceiveQrcode,mRlReceiveBcard;
    private OnSelectPayTypeListener mOnSelectPayTypeListener;

    public interface OnSelectPayTypeListener{
        void onSelectQrcode();
        void onSelectScan();
        void onSelectBcard();
    }

    public PayTypePopupWindow(final Activity context, OnSelectPayTypeListener onSelectPayTypeListener)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_pay_type, null);
        this.activity = context;
        this.mOnSelectPayTypeListener = onSelectPayTypeListener;
        mRlReceiveQrcode = (RelativeLayout) mMenuView.findViewById(R.id.rl_receive_qrcode);
        mLlReceiveQrcode = (LinearLayout) mMenuView.findViewById(R.id.ll_receive_qrcode);
        mLlReceiveScan = (LinearLayout) mMenuView.findViewById(R.id.ll_receive_scan);
        mLlReceiveBcard = (LinearLayout) mMenuView.findViewById(R.id.ll_receive_bcard);
        mRlReceiveBcard = (RelativeLayout) mMenuView.findViewById(R.id.rl_receive_bcard);

        mLlReceiveQrcode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnSelectPayTypeListener != null) {
                    mOnSelectPayTypeListener.onSelectQrcode();
                }
            }
        });

        mLlReceiveScan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnSelectPayTypeListener != null) {
                    mOnSelectPayTypeListener.onSelectScan();
                }
            }
        });

        mLlReceiveBcard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnSelectPayTypeListener != null) {
                    mOnSelectPayTypeListener.onSelectBcard();
                }
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener()
        {

            public boolean onTouch(View v, MotionEvent event)
            {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
}
