package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.UIUtils;

/**
 * Created by admin on 2017/7/11.
 */

public class PrintTypePopupWindow extends PopupWindow {

    private View mMenuView;
    private Activity activity;
    private ImageView mIvTypeOne;
    private ImageView mIvTypeTwo;
    private OnSelectPrintTypeListener mOnSelectPrintTypeListener;

    public interface OnSelectPrintTypeListener{
        void onSelectPrintType();
    }

    public PrintTypePopupWindow(final Activity context, OnSelectPrintTypeListener onSelectPrintTypeListener)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_print_type, null);
        this.activity = context;
        this.mOnSelectPrintTypeListener = onSelectPrintTypeListener;
        RelativeLayout rlPrintTypeOne = mMenuView.findViewById(R.id.rl_print_type_one);
        RelativeLayout rlPrintTypeTwo = mMenuView.findViewById(R.id.rl_print_type_two);
        mIvTypeOne = mMenuView.findViewById(R.id.iv_print_type_one);
        mIvTypeTwo = mMenuView.findViewById(R.id.iv_print_type_two);
        TextView tvCancel = mMenuView.findViewById(R.id.tv_cancel);

        tvCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        rlPrintTypeOne.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO,false);
                setImageView();
                if (mOnSelectPrintTypeListener != null) {
                    mOnSelectPrintTypeListener.onSelectPrintType();
                }
            }
        });

        rlPrintTypeTwo.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO,true);
                setImageView();
                if (mOnSelectPrintTypeListener != null) {
                    mOnSelectPrintTypeListener.onSelectPrintType();
                }
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.dialog_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener()
        {

            public boolean onTouch(View v, MotionEvent event)
            {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });
        setImageView();
    }

    private void setImageView(){
        boolean printTwo = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO);
        if (printTwo){
            mIvTypeTwo.setImageResource(R.mipmap.ic_radio_selected);
            mIvTypeOne.setImageResource(R.mipmap.ic_radio_default);
        }else{
            mIvTypeOne.setImageResource(R.mipmap.ic_radio_selected);
            mIvTypeTwo.setImageResource(R.mipmap.ic_radio_default);
        }
    }
}
