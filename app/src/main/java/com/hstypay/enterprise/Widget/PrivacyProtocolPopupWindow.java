package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.UIUtils;


/**
 * Created by admin on 2017/7/11.
 */

public class PrivacyProtocolPopupWindow extends PopupWindow {

    private View mMenuView;
    private PrivacyProtocolPopupWindow.OnClickEnsureListener mOnClickEnsureListener;
    private PrivacyProtocolPopupWindow.OnClickCancelListener mOnClickCancelListener;
    private Activity activity;
    private Uri imageUri;
    private LinearLayout mLlBottom;
    private TextView mTvContent;

    public interface OnClickEnsureListener {
        void agreeProtocol();
    }

    public interface OnClickCancelListener {
        void notAgreeProtocol();
    }

    public PrivacyProtocolPopupWindow(Activity context, PrivacyProtocolPopupWindow.OnClickEnsureListener onClickEnsureListener, PrivacyProtocolPopupWindow.OnClickCancelListener onClickCancelListener) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_privacy_protocol, null);
        this.activity = context;
        this.mOnClickEnsureListener = onClickEnsureListener;
        this.mOnClickCancelListener = onClickCancelListener;

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(false);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        /*mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });*/
        initView();
    }

    private void initView() {
        mTvContent = mMenuView.findViewById(R.id.tv_content_detail);
        mLlBottom = mMenuView.findViewById(R.id.ll_bottom);
        Button btnEnsure = mMenuView.findViewById(R.id.btn_ensure);
        Button btnCancel = mMenuView.findViewById(R.id.btn_cancel);
        SpannableStringBuilder spannableString = new SpannableStringBuilder();
        spannableString.append(UIUtils.getString(R.string.popup_protocol_content2));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                PrivacyProtocolDialog dialog = new PrivacyProtocolDialog(activity);
                dialog.show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(MyApplication.getContext(), R.color.theme_color));            //设置可以点击文本部分的颜色
                ds.setUnderlineText(false);
                ds.clearShadowLayer();
            }
        };
        spannableString.setSpan(clickableSpan, 10, 23, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        mTvContent.setText(spannableString);
        mTvContent.setMovementMethod(LinkMovementMethod.getInstance());
        avoidHintColor(mTvContent);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickCancelListener != null) {
                    mOnClickCancelListener.notAgreeProtocol();
                }
            }
        });

        btnEnsure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickEnsureListener != null) {
                    mOnClickEnsureListener.agreeProtocol();
                }
            }
        });
    }

    private void avoidHintColor(View view) {
        if (view instanceof TextView)
            ((TextView) view).setHighlightColor(UIUtils.getColor(android.R.color.transparent));
    }
}
