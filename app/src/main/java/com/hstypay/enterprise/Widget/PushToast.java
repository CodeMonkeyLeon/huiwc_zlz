package com.hstypay.enterprise.Widget;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.UIUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-11-27
 * Time: 下午9:17
 * To change this template use File | Settings | File Templates.
 */
public class PushToast {
    
    private static PushToast mToast;

    private static Handler mHandler = new Handler();

    private static LayoutInflater mInflater;

    private static Runnable r = new Runnable()
    {
        public void run()
        {
            //mToast.cancel();
        }
    };

    public static void showToast(String title,String msg, int length){
        showToast(MyApplication.getContext(),title,msg,length);
    }
    
    public static void showToast(Context context,String title, String msg, int length)
    {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastRoot = mInflater.inflate(R.layout.view_push_toast, null);
        TextView message = (TextView)toastRoot.findViewById(R.id.tv_toast_content);
        TextView tvTitle = (TextView)toastRoot.findViewById(R.id.tv_toast_title);
        message.setText(msg);
        tvTitle.setText(title);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(Gravity.TOP, 0, UIUtils.dp2px(10));
        toastStart.setDuration(length);
        toastStart.setView(toastRoot);
        toastStart.show();
    }

}
