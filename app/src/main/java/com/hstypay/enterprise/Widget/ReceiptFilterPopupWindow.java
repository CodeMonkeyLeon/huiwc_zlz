package com.hstypay.enterprise.Widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.receipt.ReceiptActivity;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

public class ReceiptFilterPopupWindow extends PopupWindow implements CompoundButton.OnCheckedChangeListener {

    private int[] apiProvider;
    private int[] tradeStatus;
    private ReceiptActivity activity;
    private HandleBtn handleBtn;
    private View mMenuView;
    private CheckBox cb_all_payment, cb_zfb_pay, cb_wx_pay;
    private CheckBox cb_all_state, cb_state_check, cb_state_check_pay, cb_state_closed, cb_state_paid;
    private Button btn_reset, btn_confirm;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_all_payment:
                if (cb_all_payment.isChecked()) {
                    cb_wx_pay.setChecked(false);
                    cb_zfb_pay.setChecked(false);
                }
                break;
            case R.id.cb_wx_pay:
            case R.id.cb_zfb_pay:
                if (cb_wx_pay.isChecked() || cb_zfb_pay.isChecked()) {
                    cb_all_payment.setChecked(false);
                } else {
                    cb_all_payment.setChecked(true);
                }
                break;
            case R.id.cb_all_state:
                if (cb_all_state.isChecked()) {
                    cb_state_check.setChecked(false);
                    cb_state_check_pay.setChecked(false);
                    cb_state_closed.setChecked(false);
                    cb_state_paid.setChecked(false);
                }
                break;
            case R.id.cb_state_check:
            case R.id.cb_state_check_pay:
            case R.id.cb_state_closed:
            case R.id.cb_state_paid:
                if (cb_state_check.isChecked() || cb_state_check_pay.isChecked() || cb_state_closed.isChecked() || cb_state_paid.isChecked()) {
                    cb_all_state.setChecked(false);
                } else {
                    cb_all_state.setChecked(true);
                }
                break;

        }
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(int[] apiProvider, int[] tradeStatus);
    }

    public ReceiptFilterPopupWindow(ReceiptActivity context, int[] apiProvider, int[] tradeStatus, HandleBtn handleBtn) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.receipt_pop_filter, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.apiProvider = apiProvider;
        this.tradeStatus = tradeStatus;

        initView(mMenuView);

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.popup_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int top = mMenuView.findViewById(R.id.pop_layout).getTop();
                int bottom = mMenuView.findViewById(R.id.pop_layout).getBottom();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top || y > bottom) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }

    private void initView(View mMenuView) {

        //支付方式
        cb_all_payment = mMenuView.findViewById(R.id.cb_all_payment);//全部
        cb_wx_pay = mMenuView.findViewById(R.id.cb_wx_pay);//微信
        cb_zfb_pay = mMenuView.findViewById(R.id.cb_zfb_pay);//支付宝

        //支付状态
        cb_all_state = mMenuView.findViewById(R.id.cb_all_state);//全部
        cb_state_check = mMenuView.findViewById(R.id.cb_state_check);//待查看
        cb_state_check_pay = mMenuView.findViewById(R.id.cb_state_check_pay);//已查看待支付
        cb_state_closed = mMenuView.findViewById(R.id.cb_state_closed);//已关闭
        cb_state_paid = mMenuView.findViewById(R.id.cb_state_paid);//已支付
        btn_reset = mMenuView.findViewById(R.id.btn_reset);//重置
        btn_confirm = mMenuView.findViewById(R.id.btn_confirm);//完成

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
                showView();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                apiProvider = getApiProvider();
                tradeStatus = getTradeStatus();
                handleBtn.handleOkBtn(apiProvider, tradeStatus);
            }
        });
        showView();

        cb_all_payment.setOnCheckedChangeListener(this);
        cb_wx_pay.setOnCheckedChangeListener(this);
        cb_zfb_pay.setOnCheckedChangeListener(this);
        cb_all_state.setOnCheckedChangeListener(this);
        cb_state_check.setOnCheckedChangeListener(this);
        cb_state_check_pay.setOnCheckedChangeListener(this);
        cb_state_closed.setOnCheckedChangeListener(this);
        cb_state_paid.setOnCheckedChangeListener(this);
    }

    private void showView() {
        if (apiProvider != null && apiProvider.length > 0) {
            for (int i = 0; i < apiProvider.length; i++) {
                switch (apiProvider[i]) {
                    case 1:
                        cb_wx_pay.setChecked(true);
                        break;
                    case 2:
                        cb_zfb_pay.setChecked(true);
                        break;
                }
            }
        } else {
            cb_all_payment.setChecked(true);
        }
        if (tradeStatus != null && tradeStatus.length > 0) {
            for (int i = 0; i < tradeStatus.length; i++) {
                switch (tradeStatus[i]) {
                    case 1:
                        cb_state_check.setChecked(true);
                        break;
                    case 2:
                        cb_state_check_pay.setChecked(true);
                        break;
                    case 3:
                        cb_state_closed.setChecked(true);
                        break;
                    case 4:
                        cb_state_paid.setChecked(true);
                        break;
                }
            }
        } else {
            cb_all_state.setChecked(true);
        }
    }

    public void reset() {
        cb_all_payment.setChecked(true);
        cb_wx_pay.setChecked(false);
        cb_zfb_pay.setChecked(false);

        cb_all_state.setChecked(true);
        cb_state_check.setChecked(false);
        cb_state_check_pay.setChecked(false);
        cb_state_closed.setChecked(false);
        cb_state_paid.setChecked(false);
    }

    public int[] getApiProvider() {
        List<Integer> list = new ArrayList<>();
        if (cb_wx_pay.isChecked()) {
            list.add(1);
        }
        if (cb_zfb_pay.isChecked()) {
            list.add(2);
        }
        return listToArray(list);
    }

    public int[] getTradeStatus() {
        List<Integer> list = new ArrayList<>();
        if (cb_state_check.isChecked()) {
            list.add(1);
        }
        if (cb_state_check_pay.isChecked()) {
            list.add(2);
        }

        if (cb_state_closed.isChecked()) {
            list.add(3);
        }
        if (cb_state_paid.isChecked()) {
            list.add(4);
        }
        return listToArray(list);
    }

    public int[] listToArray(List<Integer> list) {
        if (list != null && list.size() > 0) {
            int[] array = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                array[i] = list.get(i);
            }
            return array;
        } else {
            return null;
        }
    }
}
