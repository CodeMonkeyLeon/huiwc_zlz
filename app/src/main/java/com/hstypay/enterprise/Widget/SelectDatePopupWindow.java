package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.StringUtils;

import java.lang.reflect.Field;
import java.util.Calendar;


public class SelectDatePopupWindow extends PopupWindow implements OnDateChangedListener
{
    private View mMenuView;
    private SelectDatePopupWindow.HandleBtn handleBtn;
    private DatePicker datePicker;
    private Activity activity;
    Button btnOk;

    private String time;


    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     * @author Administrator
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn(String time);
    }


    public SelectDatePopupWindow(Activity context, String time, SelectDatePopupWindow.HandleBtn handleBtn)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.date_time_pick_dialog, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        
        initView(mMenuView, time);
        
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        //this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener()
        {

            public boolean onTouch(View v, MotionEvent event)
            {

                int top = mMenuView.findViewById(R.id.pop_layout).getTop();
                int bottom = mMenuView.findViewById(R.id.pop_layout).getBottom();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    //LogUtil.d("top:"+top+"---bottom:"+bottom+"---y:"+y);
                    if (y < top || y > bottom)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    
    private void initView(View view, String date)
    {
        if(StringUtils.isEmptyOrNull(date) || "今日".equals(date)){
            date = DateUtil.formatYYMD(System.currentTimeMillis());
        }
        this.time = date;
        datePicker = (DatePicker)view.findViewById(R.id.datepicker);

        setDatePickerDividerColor(datePicker,R.color.theme_color);

        Calendar calendar = Calendar.getInstance();
        if (!StringUtils.isEmptyOrNull(date))
        {
            try
            {
                String[] arr = date.split("\\-");
                datePicker.init(Integer.parseInt(arr[0]),
                    (Integer.parseInt(arr[1]) - 1),
                    Integer.parseInt(arr[2]),
                    this);
            }
            catch (Exception e)
            {
                // TODO: handle exception
                datePicker.init(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    this);
            }
        }
        else
        {
            
            datePicker.init(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                this);
        }
        
        btnOk = (Button)view.findViewById(R.id.btnOk);
        
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                handleBtn.handleOkBtn(time);
            }
        });
    }
    
    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        time = DateUtil.formatYYMD(calendar.getTimeInMillis());
    }

    private void setDatePickerDividerColor(DatePicker datePicker,int color){
        // Divider changing:

        // 获取 mSpinners
        LinearLayout llFirst       = (LinearLayout) datePicker.getChildAt(0);

        // 获取 NumberPicker
        LinearLayout mSpinners      = (LinearLayout) llFirst.getChildAt(0);
        for (int i = 0; i < mSpinners.getChildCount(); i++) {
            NumberPicker picker = (NumberPicker) mSpinners.getChildAt(i);

            Field[] pickerFields = NumberPicker.class.getDeclaredFields();
            for (Field pf : pickerFields) {
                if (pf.getName().equals("mSelectionDivider")) {
                    pf.setAccessible(true);
                    try {
                        pf.set(picker, new ColorDrawable(MyApplication.getContext().getResources().getColor(color)));
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
    }
}
