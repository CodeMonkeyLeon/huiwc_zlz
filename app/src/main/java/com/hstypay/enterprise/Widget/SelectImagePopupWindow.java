package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.PictureBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StringUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by admin on 2017/7/11.
 */

public class SelectImagePopupWindow extends PopupWindow {

    private View mMenuView;
    private SelectImagePopupWindow.HandleTv handleTv;
    private Activity activity;
    private final LinearLayout ll_image,ll_download;
    private final TextView tv_title;
    private final ImageView iv_image_url;
    private final LinearLayout tv_take_pic;
    private final TextView tv_choice;
    private final TextView tv_download;
    private final TextView tv_delete;
    private PictureBean mBean;
    private final View view_del;

    public interface HandleTv {
        void takePic();

        void choicePic();

        void delete(PictureBean bean);

        void download(PictureBean bean);
    }

    public SelectImagePopupWindow(final Activity context, PictureBean bean, String path, final SelectImagePopupWindow.HandleTv handleTv) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.take_image_dialog, null);
        this.activity = context;
        this.handleTv = handleTv;
        this.mBean = bean;
        tv_take_pic = mMenuView.findViewById(R.id.tv_take_pic);
        tv_choice = (TextView) mMenuView.findViewById(R.id.tv_choice);
        tv_download = (TextView) mMenuView.findViewById(R.id.tv_download);
        tv_delete = (TextView) mMenuView.findViewById(R.id.tv_delete);
        ll_image = (LinearLayout) mMenuView.findViewById(R.id.ll_image);
        ll_download = (LinearLayout) mMenuView.findViewById(R.id.ll_download);
        tv_title = (TextView) mMenuView.findViewById(R.id.tv_title);
        iv_image_url = (ImageView) mMenuView.findViewById(R.id.iv_image_url);
        view_del = mMenuView.findViewById(R.id.view_del);
        initView(bean, path);

        mMenuView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tv_take_pic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleTv.takePic();
            }
        });

        tv_choice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleTv.choicePic();
            }
        });

        tv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                handleTv.download(mBean);
            }
        });

        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                handleTv.delete(mBean);
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private void initView(PictureBean bean, String path) {
        /*if(!StringUtils.isEmptyOrNull(path)){
            if(bean!=null && bean.getPicName()!=null){
                tv_title.setText(bean.getPicName());
            }
           Bitmap bitmap = ImagePase.readBitmapFromStream(path);
            if(bitmap!=null) {
                iv_image_url.setImageBitmap(bitmap);
            }
            tv_choice.setText("重新上传");
            ll_image.setVisibility(View.VISIBLE);
            view_del.setVisibility(View.VISIBLE);
            tv_delete.setVisibility(View.VISIBLE);

        }else {

            if(!baseDel) {
                if (bean != null && !StringUtils.isEmptyOrNull(bean.getUrl())) {
                    if (bean.getPicName() != null) {
                        tv_title.setText(bean.getPicName());
                    }
                    Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(iv_image_url);
                    tv_choice.setText("重新上传");
                    ll_image.setVisibility(View.VISIBLE);
                    view_del.setVisibility(View.VISIBLE);
                    tv_delete.setVisibility(View.VISIBLE);

                } else {
                    view_del.setVisibility(View.GONE);
                    tv_delete.setVisibility(View.GONE);
                }
            }else {
                view_del.setVisibility(View.GONE);
                tv_delete.setVisibility(View.GONE);
            }
        }*/

        if (!StringUtils.isEmptyOrNull(path)) {
            if (bean != null && bean.getPicName() != null) {
                tv_title.setText(bean.getPicName());
            }
            Picasso.get().load(Constants.H5_BASE_URL + path).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(iv_image_url);
            tv_choice.setText("重新上传");
            ll_image.setVisibility(View.VISIBLE);
            view_del.setVisibility(View.VISIBLE);
            tv_delete.setVisibility(View.VISIBLE);

        } else {
            view_del.setVisibility(View.GONE);
            tv_delete.setVisibility(View.GONE);
        }
        if (bean!=null && (bean.getPicId() == 8 || bean.getPicId() == 9)) {
            ll_download.setVisibility(View.VISIBLE);
        } else {
            ll_download.setVisibility(View.GONE);
        }
    }
}
