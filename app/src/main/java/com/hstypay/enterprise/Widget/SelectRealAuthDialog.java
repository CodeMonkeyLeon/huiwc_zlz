package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.ImagePase;

/**
 * 实名认证弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SelectRealAuthDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnSubmit, mBtnCancel;
    private OnClickOkListener mOnClickOkListener;
    private OnClickInstructionListener mOnClickInstructionListener;
    private LinearLayout mLlAuthInstruction;
    private ImageView mIvAuthCode;
    private String mPicture;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */

    public SelectRealAuthDialog(Activity context, String picture) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.select_real_auth_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        this.mPicture = picture;
        initView(picture);
    }

    private void initView(String picture) {
        mLlAuthInstruction = mRootView.findViewById(R.id.ll_auth_instruction);
        mIvAuthCode = mRootView.findViewById(R.id.iv_auth_code);
        mBtnSubmit = mRootView.findViewById(R.id.btn_submit);
        mBtnCancel = mRootView.findViewById(R.id.btn_cancel);
        mLlAuthInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickInstructionListener != null) {
                    mOnClickInstructionListener.clickInstruction();
                }
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk();
                }
            }
        });
        if (!TextUtils.isEmpty(picture)) {
            if (ImagePase.base64ToBitmap(picture) != null)
                mIvAuthCode.setImageBitmap(ImagePase.base64ToBitmap(picture));
        }
    }

    public interface OnClickOkListener {
        void clickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.mOnClickOkListener = onClickOkListener;
    }

    public interface OnClickInstructionListener {
        void clickInstruction();
    }

    public void setOnClickInstructionListener(OnClickInstructionListener onClickInstructionListener) {
        this.mOnClickInstructionListener = onClickInstructionListener;
    }
}
