package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.hstypay.enterprise.R;

/**
 * Created by admin on 2017/7/11.
 */

public class SelectSendPopupWindow extends PopupWindow {

    private View mMenuView;
    private SelectSendPopupWindow.HandleIcon handleIcon;
    private Activity activity;

    public interface HandleIcon
    {
        void sendToFriends();
        void sendToWechat();
        void sendToQQ();
        void sendToAblum();
        void sendToEmail();
    }
    public SelectSendPopupWindow(final Activity context, final SelectSendPopupWindow.HandleIcon handleIcon)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.select_send_pop, null);
        this.activity = context;
        this.handleIcon = handleIcon;
        LinearLayout llSendFriends = (LinearLayout)mMenuView.findViewById(R.id.ll_send_friends);
        LinearLayout llSendWechat = (LinearLayout)mMenuView.findViewById(R.id.ll_send_wechat);
        LinearLayout llSendQQ = (LinearLayout)mMenuView.findViewById(R.id.ll_send_qq);
        LinearLayout llSendAblum = (LinearLayout)mMenuView.findViewById(R.id.ll_send_album);
        LinearLayout llSendEmail = (LinearLayout)mMenuView.findViewById(R.id.ll_send_email);

        llSendFriends.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                handleIcon.sendToFriends();
            }
        });

        llSendWechat.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                handleIcon.sendToWechat();
            }
        });

        llSendQQ.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                handleIcon.sendToQQ();
            }
        });

        llSendAblum.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                handleIcon.sendToAblum();
            }
        });

        llSendEmail.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                dismiss();
                handleIcon.sendToEmail();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener()
        {

            public boolean onTouch(View v, MotionEvent event)
            {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
}
