package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SelectSiteLinkDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private LinearLayout mLlHardware, mLlQrcode;
    private OnClickHardwareListener mOnClickHardwareListener;
    private OnClickQrcodeListener mOnClickQrcodeListener;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */

    public SelectSiteLinkDialog(Activity context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_site_link, null);
        this.setCanceledOnTouchOutside(true);
        this.setCancelable(true);
        setContentView(mRootView);

        this.mContext = context;
        initView();
    }

    private void initView() {
        mLlHardware = mRootView.findViewById(R.id.ll_hardware);
        mLlQrcode = mRootView.findViewById(R.id.ll_qrcode);

        mLlHardware.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickHardwareListener != null) {
                    mOnClickHardwareListener.clickHardware();
                }
            }
        });
        mLlQrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickQrcodeListener != null) {
                    mOnClickQrcodeListener.clickQrcode();
                }
            }
        });
    }

    public interface OnClickHardwareListener {
        void clickHardware();
    }

    public void setOnClickHardwareListener(OnClickHardwareListener onClickHardwareListener) {
        this.mOnClickHardwareListener = onClickHardwareListener;
    }

    public interface OnClickQrcodeListener {
        void clickQrcode();
    }

    public void setOnClickQrcodeListener(OnClickQrcodeListener onClickQrcodeListener) {
        this.mOnClickQrcodeListener = onClickQrcodeListener;
    }
}
