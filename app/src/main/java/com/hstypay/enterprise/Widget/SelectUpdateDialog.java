package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.VideoActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PushModeBean;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.BaiduMtjUtils;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.webview.MyWebView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SelectUpdateDialog extends Dialog {

    private PushModeBean.DataBean mData;
    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnSubmit, mBtnCancel;
    private OnClickOkListener mOnClickOkListener;
    private OnClickCancelListener mOnClickCancelListener;
    private MyWebView mWebView;
    private ViewGroup mView;
    private LinearLayout mLlArrow;
    private TextView mTvShowCount;
    private Uri imageUri;
    private ValueCallback<Uri> mUploadMessage;// 表单的数据信息
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    private final static int FILECHOOSER_RESULTCODE = 1;// 表单的结果回调</span>
    private boolean completeRead;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public SelectUpdateDialog(Activity context, ViewGroup view, PushModeBean.DataBean data) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.select_update_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        this.mView = view;
        this.mData = data;
        initView();
        initData();
    }

    private void initView() {
        mBtnSubmit = mRootView.findViewById(R.id.btn_submit);
        mBtnCancel = mRootView.findViewById(R.id.btn_cancel);
        mWebView = mRootView.findViewById(R.id.dialog_webView);
        mLlArrow = mRootView.findViewById(R.id.ll_arrow);
        mTvShowCount = mRootView.findViewById(R.id.tv_show_count);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setDomStorageEnabled(true);//主要是这句
        webSettings.setJavaScriptEnabled(true);//启用js
        webSettings.setBlockNetworkImage(false);//解决图片不显示
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadsImagesAutomatically(true);
        setWebChromeClient(mWebView);
        webViewScrollChangeListener();
        mWebView.addJavascriptInterface(new JsInteration(), "android");
        mWebView.loadUrl("file:///android_asset/HstyNewPlatformUpgradeLeadingPage.html");

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickCancelListener != null) {
                    mOnClickCancelListener.clickCancel();
                }
            }
        });
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk();
                }
            }
        });
        mTvShowCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowCountPopupWindow showCountPopupWindow = new ShowCountPopupWindow(new ShowCountPopupWindow.OnSelectModeListener() {
                    @Override
                    public void switchMode(int position) {
                        mTvShowCount.setText(mData.getPushFrequencys().get(position).getTypeName());
                        Map<String, Object> map = new HashMap<>();
                        map.put("pushUpgradeFrequency", mData.getPushFrequencys().get(position).getTypeId());
                        ServerClient.newInstance(mContext).updatePushFrequency(mContext, "TAG_UPDATE_PUSH_FREQUENCY", map);
                    }
                }, mContext, mData);
                showCountPopupWindow.showAsDropDown(mTvShowCount);
            }
        });
    }

    private void initData() {
        if (mData != null && mData.getPushFrequencys() != null) {
            mTvShowCount.setVisibility(View.VISIBLE);
            for (PushModeBean.DataBean.PushFrequencysBean pushFrequency : mData.getPushFrequencys()) {
                if (pushFrequency.getTypeId() == mData.getPushUpgradeFrequency()) {
                    mTvShowCount.setText(pushFrequency.getTypeName());
                    return;
                }
            }
        } else {
            mTvShowCount.setVisibility(View.GONE);
        }
    }

    public void setWebChromeClient(WebView webView) {

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            private View mCustomView;
            private CustomViewCallback mCustomViewCallback;

            @Override
            public void onProgressChanged(WebView view, int newProgress) {

            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public boolean onShowFileChooser(WebView webView,
                                             ValueCallback<Uri[]> filePathCallback,
                                             FileChooserParams fileChooserParams) {
                mUploadCallbackAboveL = filePathCallback;
                take();
                return true;
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                take();
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);

            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }
                mCustomView = view;
                mView.addView(mCustomView);
                mCustomViewCallback = callback;
                mWebView.setVisibility(View.GONE);
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }

            public void onHideCustomView() {
                mWebView.setVisibility(View.VISIBLE);
                if (mCustomView == null) {
                    return;
                }
                mCustomView.setVisibility(View.GONE);
                mView.removeView(mCustomView);
                mCustomViewCallback.onCustomViewHidden();
                mCustomView = null;
                mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                super.onHideCustomView();
            }
        });
    }

    private void take() {
        File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyApp");
        // Create the storage directory if it does not exist
        if (!imageStorageDir.exists()) {
            imageStorageDir.mkdirs();
        }
        File file = new File(imageStorageDir + File.separator + "IMG_" + System.currentTimeMillis() + ".jpg");
        imageUri = Uri.fromFile(file);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = MyApplication.getContext().getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent i = new Intent(captureIntent);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            i.setPackage(packageName);
            i.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(i);
        }
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        mContext.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    public class JsInteration {
        @JavascriptInterface
        public void playVideo(String videoUrl) {
            //调起系统所有播放器
            Intent intent = new Intent(mContext, VideoActivity.class);
            intent.putExtra(Constants.INTENT_OUT_URL, videoUrl);
            mContext.startActivity(intent);
        }
    }

    //核心代码
    private void webViewScrollChangeListener() {
        mWebView.setOnCustomScrollChangeListener(new MyWebView.OnScrollInterface() {
            @Override
            public void onSChanged(int l, int t, int oldl, int oldt) {
                //WebView的总高度
                int webViewContentHeight = (int) (mWebView.getContentHeight() * mWebView.getScale());
                //WebView的现高度
                int webViewCurrentHeight = (mWebView.getHeight() + mWebView.getScrollY());
                System.out.println("webViewContentHeight=" + webViewContentHeight);
                System.out.println("webViewCurrentHeight=" + webViewCurrentHeight);
                if ((webViewContentHeight - webViewCurrentHeight) < 10) {
                    System.out.println("WebView滑动到了底端");
                    mLlArrow.setVisibility(View.INVISIBLE);
                    if (!completeRead) {
                        BaiduMtjUtils.eventId("升级引导_浏览完成");
                        completeRead = true;
                    }
                } else {
                    System.out.println("WebView滑动没到底端");
                    mLlArrow.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public interface OnClickOkListener {
        void clickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.mOnClickOkListener = onClickOkListener;
    }

    public interface OnClickCancelListener {
        void clickCancel();
    }

    public void setOnClickCancelListener(OnClickCancelListener onClickCancelListener) {
        this.mOnClickCancelListener = onClickCancelListener;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        mWebView.destroy();
    }
}
