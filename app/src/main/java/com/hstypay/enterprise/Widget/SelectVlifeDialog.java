package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SelectVlifeDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnSubmit, mBtnCancel;
    private OnClickOkListener mOnClickOkListener;
    private OnClickCancelListener mOnClickCancelListener;
    private TextView mTvContent;
    private LinearLayout mLlPay;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */

    public SelectVlifeDialog(Activity context, String contentText, boolean showPay) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.select_vlife_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        initView(contentText, showPay);
    }

    private void initView(String contentText, boolean showPay) {
        mBtnSubmit = mRootView.findViewById(R.id.btn_submit);
        mBtnCancel = mRootView.findViewById(R.id.btn_cancel);
        mTvContent = mRootView.findViewById(R.id.dialog_content_text);
        mLlPay = mRootView.findViewById(R.id.ll_pay);
        mLlPay.setVisibility(showPay ? View.VISIBLE : View.GONE);
        if (!TextUtils.isEmpty(contentText)) {
            mTvContent.setText(contentText);
        }
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickCancelListener != null) {
                    mOnClickCancelListener.clickCancel();
                }
            }
        });
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk();
                }
            }
        });
    }

    public interface OnClickOkListener {
        void clickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.mOnClickOkListener = onClickOkListener;
    }

    public interface OnClickCancelListener {
        void clickCancel();
    }

    public void setOnClickCancelListener(OnClickCancelListener onClickCancelListener) {
        this.mOnClickCancelListener = onClickCancelListener;
    }
}
