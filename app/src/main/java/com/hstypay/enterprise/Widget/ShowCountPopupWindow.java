package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.ShowCountAdapter;
import com.hstypay.enterprise.bean.PushModeBean;
import com.hstypay.enterprise.utils.BaiduMtjUtils;
import com.hstypay.enterprise.utils.UIUtils;

/**
 * Created by admin on 2017/7/11.
 */

public class ShowCountPopupWindow extends PopupWindow implements ShowCountAdapter.OnRecyclerViewItemClickListener {

    private PushModeBean.DataBean mData;
    private View mMenuView;
    private OnSelectModeListener mOnSelectModeListener;
    private Activity activity;
    private RecyclerView mRecyclerView;

    public interface OnSelectModeListener {
        void switchMode(int pushUpgradeFrequency);
    }

    public ShowCountPopupWindow(OnSelectModeListener onSelectModeListener, final Activity context, PushModeBean.DataBean data) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pop_show_count, null);
        this.activity = context;
        this.mData = data;
        this.mOnSelectModeListener = onSelectModeListener;

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimTop);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.translucent));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        initView();
    }

    private void initView() {
        mRecyclerView = mMenuView.findViewById(R.id.recycler_view);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(storeLinearLayoutManager);
        if (mData == null)
            return;
        ShowCountAdapter showCountAdapter = new ShowCountAdapter(activity, mData.getPushFrequencys());
        showCountAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(showCountAdapter);
    }

    @Override
    public void onItemClick(int position) {
        if (mOnSelectModeListener != null) {
            mOnSelectModeListener.switchMode(position);
            if (mData.getPushFrequencys().get(position).getTypeId() == 0)
                BaiduMtjUtils.eventId("升级引导_不再显示");
            dismiss();
        }
    }
}
