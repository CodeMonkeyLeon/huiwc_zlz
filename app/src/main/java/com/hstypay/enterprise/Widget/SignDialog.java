package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SignDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnSubmit;
    private WebView mWebView;
    private TextView mTvTitle;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public SignDialog(Activity context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.notice_dialog_sign, null);
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.mContext = context;
        initView();
    }

    private void initView() {
        mBtnSubmit = (Button) mRootView.findViewById(R.id.btn_submit);
        mTvTitle = (TextView) mRootView.findViewById(R.id.tv_title);
        mWebView = (WebView) mRootView.findViewById(R.id.dialog_webView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setDomStorageEnabled(true);//主要是这句
        webSettings.setJavaScriptEnabled(true);//启用js
        webSettings.setBlockNetworkImage(false);//解决图片不显示
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadsImagesAutomatically(true);
        mWebView.setWebChromeClient(new WebChromeClient());//这行最好不要丢掉
        //该方法解决的问题是打开浏览器不调用系统浏览器，直接用webview打开
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (view.getTitle() != null && !url.contains(view.getTitle())) {
                    mTvTitle.setText(view.getTitle());
                }
            }
        });

        mWebView.loadUrl(Constants.URL_COOPERATION_PROTOCOL);
//        mTvTitle.setText(UIUtils.getString(R.string.hwc_cooperation_contact_title));

        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    ServerClient.newInstance(MyApplication.getContext()).putSignStatus(MyApplication.getContext(), Constants.TAG_PUT_SIGN, null);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        });
    }
}
