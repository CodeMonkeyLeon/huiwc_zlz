package com.hstypay.enterprise.Widget;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.Widget
 * @创建者: Jeremy
 * @创建时间: 2018/9/17 19:30
 * @描述: ${TODO}
 */
public class ToastDialog {
    private Context mContext;
    private String msg;
    private String title;
    private OnClickToastListener mOnClickToastListener;
    private int count = 0;
    private boolean isShowing = true;

    public ToastDialog(Context context, String msg, String title) {
        this.mContext = context;
        this.msg = msg;
        this.title = title;
    }

    public void showUpdateSuccessDialog(int res) {
        final WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

        WindowManager.LayoutParams para = new WindowManager.LayoutParams();
        para.height = -2;//WRAP_CONTENT
        para.width = -2;//WRAP_CONTENT
        para.format = 1;

        para.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        para.gravity = Gravity.TOP;
        if (Build.VERSION.SDK_INT >= 26 && MyApplication.getContext().getApplicationInfo().targetSdkVersion > 22) {
            para.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            PackageManager pm = mContext.getPackageManager();
            boolean permission = (PackageManager.PERMISSION_GRANTED == pm.checkPermission("android.permission.SYSTEM_ALERT_WINDOW", mContext.getPackageName()));
            if (permission || "Xiaomi".equals(Build.MANUFACTURER) || "vivo".equals(Build.MANUFACTURER)) {
                para.type = WindowManager.LayoutParams.TYPE_PHONE;
            } else {
                para.type = WindowManager.LayoutParams.TYPE_TOAST;
            }
        }

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View contentView = inflater.inflate(res, null);
        LinearLayout toast = (LinearLayout) contentView.findViewById(R.id.ll_toast);
        TextView message = (TextView) contentView.findViewById(R.id.tv_toast_content);
        TextView tvTitle = (TextView) contentView.findViewById(R.id.tv_toast_title);
        message.setText(msg);
        tvTitle.setText(title);

        toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowing = false;
                wm.removeView(contentView);
                if (mOnClickToastListener != null) {
                    mOnClickToastListener.clickToast();
                }
               /* Intent intent = new Intent("com.hstypay.enterprise.activity.MerchantActivity");
                mContext.startActivity(intent);*/
            }
        });

        wm.addView(contentView, para);

        final Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if(++count > 4){
                    cancel();
                    timer.cancel();
                    if (isShowing) {
                        wm.removeView(contentView);
                    }
                }
            }
        };
        timer.schedule(timerTask, 1000, 1000);
    }

    public interface OnClickToastListener {
        void clickToast();
    }

    public void setOnClickToastListener(OnClickToastListener onClickToastListener) {
        this.mOnClickToastListener = onClickToastListener;
    }

}
