/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.hstypay.enterprise.R;


public class TypeDialog extends Dialog {
    private Context context;
    private ViewGroup mRootView;
    private HandleBtn handleBtn;
    private TextView tv_cancel;
    private TextView tv_take_pic;
    private TextView tv_choice;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public TypeDialog(Context context, HandleBtn handleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_type, null);
       /* this.setCanceledOnTouchOutside(true);
        this.setCancelable(true);*/
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        initView();
        setLinster();
        mRootView.setOnTouchListener(new View.OnTouchListener()
        {

            public boolean onTouch(View v, MotionEvent event)
            {

                int height = mRootView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

  /*  @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {

        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            dismiss();
            return true;
        }
        return super.onTouchEvent(event);
    }*/

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster() {


    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {

        tv_cancel = findViewById(R.id.tv_cancel);
        tv_take_pic = findViewById(R.id.tv_take_pic);
        tv_choice = findViewById(R.id.tv_choice);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        tv_take_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleBtn.handleOkBtn("1");
            }
        });

        tv_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleBtn.handleOkBtn("2");
            }
        });

    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String s);
    }
}
