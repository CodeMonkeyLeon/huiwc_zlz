/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.UIUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author
 * @version
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class UpdateSuccessNoticeDialog extends Dialog
{

    private Context context;
    private TextView mTvCopy;
    private Button btnOk;
    private ViewGroup mRootView;
    private OnClickOkListener mOnClickOkListener;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public UpdateSuccessNoticeDialog(Context context)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.update_success_notice_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        initView();
    }


    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        mTvCopy = (TextView)findViewById(R.id.tv_copy);
        btnOk = (Button)findViewById(R.id.btnOk);

        SpannableStringBuilder spannableString = new SpannableStringBuilder();
        spannableString.append("网页：https://mch.hstypay.com 复制");

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                copy("https://mch.hstypay.com");
                MyToast.showToastShort("复制成功");
            }

            @Override
            public void updateDrawState(TextPaint ds) {

            }
        };
        spannableString.setSpan(new UnderlineSpan(), spannableString.length()-2, spannableString.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.theme_color)), spannableString.length()-2, spannableString.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(clickableSpan, spannableString.length()-2, spannableString.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        mTvCopy.setText(spannableString);
        mTvCopy.setMovementMethod(LinkMovementMethod.getInstance());
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener!=null){
                    mOnClickOkListener.onClickOk();
                }
            }
        });
    }

    //复制
    private void copy(String data) {
        // 获取系统剪贴板
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建一个剪贴数据集，包含一个普通文本数据条目（需要复制的数据）,其他的还有
        // newHtmlText、
        // newIntent、
        // newUri、
        // newRawUri
        ClipData clipData = ClipData.newPlainText(null, data);

        // 把数据集设置（复制）到剪贴板
        clipboard.setPrimaryClip(clipData);
    }

    public interface OnClickOkListener{
        void onClickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        mOnClickOkListener = onClickOkListener;
    }
}
