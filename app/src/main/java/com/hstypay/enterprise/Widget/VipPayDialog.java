package com.hstypay.enterprise.Widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.UIUtils;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class VipPayDialog extends Dialog {

    private ViewGroup mRootView;
    private ImageView mIvClose;
    private Activity mContext;
    private Button mBtnSubmit;
    private OnClickOkListener mOnClickOkListener;
    private TextView mTvContent, mTvTitle, mTvMoney;
    private double mMoney;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public VipPayDialog(Activity context, String contentText, int type, double money) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setWindowAnimations(R.style.AnimMiddle);
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.vip_pay_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        this.mMoney = money;
        initView(contentText, type, money);
    }

    private void initView(String contentText, int type, double money) {
        mBtnSubmit = (Button) mRootView.findViewById(R.id.btn_submit);
        mTvContent = (TextView) mRootView.findViewById(R.id.dialog_content_text);
        mTvTitle = (TextView) mRootView.findViewById(R.id.tv_dialog_title);
        mTvMoney = (TextView) mRootView.findViewById(R.id.tv_dialog_money);
        mIvClose = (ImageView) mRootView.findViewById(R.id.iv_close);
        if (type == 1) {
            if (!TextUtils.isEmpty(contentText)) {
                mTvContent.setText("向会员卡" + contentText + "充值");
            }
        } else {
            mTvContent.setText("向" + SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME) + "付款");
        }
        if (type == 1) {
            mTvTitle.setText(UIUtils.getString(R.string.tv_pop_vip_recharge));
            mTvMoney.setText(UIUtils.getString(R.string.tx_mark) + DateUtil.formatPaseMoneyUtil(money));
            mBtnSubmit.setText(UIUtils.getString(R.string.btn_vip_recharge));
        } else {
            mTvTitle.setText(UIUtils.getString(R.string.tv_pop_vip_verification));
            mTvMoney.setText(UIUtils.getString(R.string.tx_mark) + DateUtil.formatPaseMoneyUtil(money));
            mBtnSubmit.setText(UIUtils.getString(R.string.btn_vip_pay));
        }
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk(mMoney);
                }
            }
        });
        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnClickOkListener {
        void clickOk(double money);
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        this.mOnClickOkListener = onClickOkListener;
    }

}
