package com.hstypay.enterprise.Widget.calendar;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.zng.common.utils.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class CalendarList extends FrameLayout implements View.OnClickListener {
    //  private static final String TAG = MainActivity.class.getSimpleName() + "_LOG";
    RecyclerView recyclerView;
    CalendarAdapter adapter;
    private DateBean startDate;//开始时间
    private DateBean endDate;//结束时间
    OnDateSelected onDateSelected;//选中监听
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    //SimpleDateFormat month_DayFormat=new SimpleDateFormat("yyyy年MM月dd日");
    SimpleDateFormat month_DayFormat = new SimpleDateFormat("MM月dd日");
    private TextView mTvStartDate, mTvEndDate, mTvEnsure;

    public CalendarList(Context context) {
        super(context);
        init(context);
    }

    public CalendarList(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CalendarList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public boolean flag = false;

    private void init(final Context context) {
        addView(LayoutInflater.from(context).inflate(R.layout.items_calendar, this, false));
        mTvStartDate = findViewById(R.id.tv_start_date);
        mTvEndDate = findViewById(R.id.tv_end_date);
        mTvEnsure = findViewById(R.id.tv_ensure);
       /* SimpleDateFormat monFormat = new SimpleDateFormat("yyyy-MM");
        Date monDate = new Date(System.currentTimeMillis());
        String month = monFormat.format(monDate);
        String year = month.replace("-", "年");*/
        SimpleDateFormat today = new SimpleDateFormat("MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String day = today.format(date);
        String d = day.replace("-", "月");
        mTvStartDate.setText("开始：请选择");
        mTvEndDate.setText("结束：请选择");
        /*mTvStartDate.setText("开始：" + simpleDateFormat.format(date) + " " + DateUtil.getDayOfWeek(date));
        mTvEndDate.setText("结束：" + simpleDateFormat.format(date) + " " + DateUtil.getDayOfWeek(date));*/

        //日期往后推90天


        recyclerView = findViewById(R.id.recyclerView);
        adapter = new CalendarAdapter();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 7);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if (DateBean.item_type_month == adapter.data.get(i).getItemType()) {
                    return 7;
                } else {
                    return 1;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.data.addAll(days("", ""));
        if (!flag) {
            flag = true;
            recyclerView.scrollToPosition(adapter.data.size() - 1);
        }
//        DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
//        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.shape));
//        recyclerView.addItemDecoration(dividerItemDecoration);

        MyItemD myItemD = new MyItemD();
        //  recyclerView.addItemDecoration(myItemD);

        adapter.setOnRecyclerviewItemClick(new CalendarAdapter.OnRecyclerviewItemClick() {
            @Override
            public void onItemClick(View v, int position) {
                onClick(adapter.data.get(position));
            }
        });

        initListener();
    }

    private void initListener() {
        mTvEnsure.setOnClickListener(this);
    }

    /**
     * 将数字传化为集合，并且补充0
     *
     * @param startNum 数字起点
     * @param count    数字个数
     * @return
     */
    private static List<String> d(int startNum, int count) {
        String[] values = new String[count];
        for (int i = startNum; i < startNum + count; i++) {
            String tempValue = (i < 10 ? "0" : "") + i;
            values[i - startNum] = tempValue;
        }
        return Arrays.asList(values);
    }


    private void onClick(DateBean dateBean) {

        if (dateBean.getItemType() == DateBean.item_type_month || TextUtils.isEmpty(dateBean.getDay())) {
            return;
        }

        if (dateBean.getMonthStr() != null && dateBean.getDay() != null) {
            //大于当前日期
            String date = dateBean.getMonthStr() + "-" + dateBean.getDay();
            boolean b = compareDate(date);
            if (b) {
                return;
            }
        }

        if (dateBean.getMonthStr() != null && dateBean.getDay() != null) {

            //选择日期不能大于90天
            String time = dateBean.getMonthStr() + "-" + dateBean.getDay();
            Calendar calendar = reportDate();
            Date time_date = calendar.getTime();
            long time2 = time_date.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = sdf.parse(time);
                long time1 = date.getTime();
                if (time1 < time2) {
                    //toastDialog(BillMainActivity.this,"选择日期不能超过90天",null);
                    return;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //如果没有选中开始日期则此次操作选中开始日期
        if (startDate == null) {
            startDate = dateBean;
            startDate.setSelected(false);
            dateBean.setItemState(DateBean.ITEM_STATE_BEGIN_DATE);
        } else if (endDate == null) {
            //如果选中了开始日期但没有选中结束日期，本次操作选中结束日期
            //如果当前点击的结束日期跟开始日期一致 则不做操作
            if (startDate == dateBean) {
                endDate = dateBean;
                endDate.setItemState(DateBean.ITEM_STATE_END_DATE);
                startDate.setSelected(false);
                endDate.setSelected(false);
                setState();
                if (onDateSelected != null) {
                    // onDateSelected.selected(simpleDateFormat.format(startDate.getDate()),simpleDateFormat.format(endDate.getDate()));
                    mTvStartDate.setText("开始：" + simpleDateFormat.format(startDate.getDate()) + " " + DateUtil.getDayOfWeek(startDate.getDate()));
                    mTvEndDate.setText("结束：" + simpleDateFormat.format(endDate.getDate()) + " " + DateUtil.getDayOfWeek(endDate.getDate()));

                    int day = (int) ((endDate.getDate().getTime() - startDate.getDate().getTime()) / (1000 * 60 * 60 * 24));
                    int i = day + 1;
                }
            } else if (dateBean.getDate().getTime() < startDate.getDate().getTime()) {
                //当前点选的日期小于当前选中的开始日期 则本次操作重新选中开始日期
                startDate.setItemState(DateBean.ITEM_STATE_NORMAL);
                startDate = dateBean;
                startDate.setSelected(false);
                startDate.setItemState(DateBean.ITEM_STATE_BEGIN_DATE);
            } else {
                //选中结束日期
                if ((dateBean.getDate().getTime() - startDate.getDate().getTime())/1000 > 31*24*60*60){
                    ToastUtil.showToastShort(UIUtils.getString(R.string.tv_select_date_over));
                    return;
                }
                endDate = dateBean;
                endDate.setItemState(DateBean.ITEM_STATE_END_DATE);
                setState();
                if (onDateSelected != null) {
                    startDate.setSelected(true);
                    endDate.setSelected(true);
                    // onDateSelected.selected(simpleDateFormat.format(startDate.getDate()),simpleDateFormat.format(endDate.getDate()));
                    mTvStartDate.setText("开始：" + simpleDateFormat.format(startDate.getDate()) + " " + DateUtil.getDayOfWeek(startDate.getDate()));
                    mTvEndDate.setText("结束：" + simpleDateFormat.format(endDate.getDate()) + " " + DateUtil.getDayOfWeek(endDate.getDate()));
                }
            }

        } else if (startDate != null && endDate != null) {
            //结束日期和开始日期都已选中
            clearState();
            //重新选择开始日期,结束日期清除
            startDate.setItemState(DateBean.ITEM_STATE_NORMAL);
            startDate = dateBean;
            startDate.setSelected(false);
            startDate.setItemState(DateBean.ITEM_STATE_BEGIN_DATE);

            endDate.setItemState(DateBean.ITEM_STATE_NORMAL);
            endDate = null;
        }
        adapter.notifyDataSetChanged();
    }

    //选中中间的日期
    private void setState() {
        if (endDate != null && startDate != null) {
            int start = adapter.data.indexOf(startDate);
            start += 1;
            int end = adapter.data.indexOf(endDate);
            for (; start < end; start++) {

                DateBean dateBean = adapter.data.get(start);
                if (!TextUtils.isEmpty(dateBean.getDay())) {
                    dateBean.setItemState(DateBean.ITEM_STATE_SELECTED);
                }
            }
        }
    }

    //取消选中状态
    private void clearState() {
        if (endDate != null && startDate != null) {
            int start = adapter.data.indexOf(startDate);
            start += 1;
            int end = adapter.data.indexOf(endDate);
            for (; start < end; start++) {
                DateBean dateBean = adapter.data.get(start);
                dateBean.setItemState(DateBean.ITEM_STATE_NORMAL);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_ensure:
                clickOk();
                break;
        }
    }

    //日历adapter
    public static class CalendarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public ArrayList<DateBean> data = new ArrayList<>();
        private CalendarAdapter.OnRecyclerviewItemClick onRecyclerviewItemClick;

        public CalendarAdapter.OnRecyclerviewItemClick getOnRecyclerviewItemClick() {
            return onRecyclerviewItemClick;
        }

        public void setOnRecyclerviewItemClick(CalendarAdapter.OnRecyclerviewItemClick onRecyclerviewItemClick) {
            this.onRecyclerviewItemClick = onRecyclerviewItemClick;
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            if (i == DateBean.item_type_day) {
                View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_day, viewGroup, false);

                final CalendarAdapter.DayViewHolder dayViewHolder = new CalendarAdapter.DayViewHolder(rootView);
                dayViewHolder.itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRecyclerviewItemClick != null) {
                            onRecyclerviewItemClick.onItemClick(v, dayViewHolder.getLayoutPosition());
                        }
                    }
                });
                return dayViewHolder;
            } else /*if (i == DateBean.item_type_month) */{
                View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_month, viewGroup, false);
                final CalendarAdapter.MonthViewHolder monthViewHolder = new CalendarAdapter.MonthViewHolder(rootView);
                monthViewHolder.itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onRecyclerviewItemClick != null) {
                            onRecyclerviewItemClick.onItemClick(v, monthViewHolder.getLayoutPosition());
                        }
                    }
                });
                return monthViewHolder;
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            if (viewHolder instanceof CalendarAdapter.MonthViewHolder) {
                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
                SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
                String monthStr = data.get(i).getMonthStr();
                String year = "";
                String month = "";
                try {
                    year = yearFormat.format(new SimpleDateFormat("yyyy-MM").parse(monthStr));
                    month = monthFormat.format(new SimpleDateFormat("yyyy-MM").parse(monthStr));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (monthStr != null && monthStr.contains("-")) {
//                    String year = monthStr.replace("-", "年");
                    ((CalendarAdapter.MonthViewHolder) viewHolder).tv_month.setText(month + "月");
                    ((MonthViewHolder) viewHolder).tv_year.setText(year + "年");
                }

            } else {
                CalendarAdapter.DayViewHolder vh = ((CalendarAdapter.DayViewHolder) viewHolder);
                SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
                Date toDate = new Date(System.currentTimeMillis());
                String today = todayFormat.format(toDate);

                SimpleDateFormat monFormat = new SimpleDateFormat("yyyy-MM");
                Date monDate = new Date(System.currentTimeMillis());
                String month = monFormat.format(monDate);
                if (today.equalsIgnoreCase(data.get(i).getDay()) && data.get(i).getMonthStr().equalsIgnoreCase(month)) {
                    vh.tv_day.setText("今天");
                } else {
                    vh.tv_day.setText(data.get(i).getDay());
                }

                vh.tv_check_in_check_out.setVisibility(View.GONE);
                DateBean dateBean = data.get(i);

                //设置item状态
                if (dateBean.getItemState() == DateBean.ITEM_STATE_BEGIN_DATE || dateBean.getItemState() == DateBean.ITEM_STATE_END_DATE) {
                    //开始日期或结束日期
                    //   vh.itemView.setBackgroundColor(Color.parseColor("#ff6600"));
                    //  vh.itemView.setBackgroundColor(Color.parseColor("#3F73F2"));
                    vh.tv_day.setBackgroundResource(R.drawable.selectshape);
                    vh.tv_day.setTextColor(Color.WHITE);
                    vh.tv_check_in_check_out.setVisibility(View.GONE);
                    if (dateBean.isSelected()){
                        if (dateBean.getItemState() == DateBean.ITEM_STATE_END_DATE) {
//                        vh.tv_check_in_check_out.setText("离店");
                            vh.view_left.setBackgroundResource(R.color.tv_date_day_unselected);
                            vh.view_right.setBackgroundResource(R.color.translucent);
                        } else {
//                        vh.tv_check_in_check_out.setText("入住");
                            vh.view_left.setBackgroundResource(R.color.translucent);
                            vh.view_right.setBackgroundResource(R.color.tv_date_day_unselected);
                        }
                    }
                } else if (dateBean.getItemState() == DateBean.ITEM_STATE_SELECTED) {
                    //选中状态
                    vh.itemView.setBackgroundResource(R.color.tv_date_day_unselected);
                    vh.tv_day.setBackgroundResource(R.color.translucent);
                    vh.tv_day.setTextColor(UIUtils.getColor(R.color.theme_color));
                    vh.view_left.setBackgroundResource(R.color.translucent);
                    vh.view_right.setBackgroundResource(R.color.translucent);
//                    vh.itemView.setBackgroundColor(Color.parseColor("#eeeeee"));
//                    vh.tv_day.setTextColor(Color.BLACK);
                } else {
                    //正常状态
                    vh.itemView.setBackgroundResource(R.color.translucent);
                    vh.tv_day.setBackgroundResource(R.color.translucent);
                    vh.tv_day.setTextColor(UIUtils.getColor(R.color.home_text));
                    vh.view_left.setBackgroundResource(R.color.translucent);
                    vh.view_right.setBackgroundResource(R.color.translucent);
                }

                if (data.get(i).getMonthStr() != null && data.get(i).getDay() != null) {
                    //大于当前日期
                    String date = data.get(i).getMonthStr() + "-" + data.get(i).getDay();
                    boolean b = compareDate(date);
                    if (b) {
                        vh.tv_day.setTextColor(Color.parseColor("#AAAAAA"));
                    }
                }

                if (data.get(i).getMonthStr() != null && data.get(i).getDay() != null) {

                    String time = data.get(i).getMonthStr() + "-" + data.get(i).getDay();
                    Calendar calendar = reportDate();
                    Date time_date = calendar.getTime();
                    long time2 = time_date.getTime();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date date = sdf.parse(time);
                        long time1 = date.getTime();
                        if (time1 < time2) {
                            //toastDialog(BillMainActivity.this,"选择日期不能超过90天",null);
                            vh.tv_day.setTextColor(Color.parseColor("#AAAAAA"));
                            vh.tv_day.setEnabled(false);
                            Logger.i("zhouwei", "month==" + data.get(i).getMonthStr() + "==day==" + data.get(i).getDay());
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }
        }


        @Override
        public int getItemViewType(int position) {
            return data.get(position).getItemType();
        }

        public class DayViewHolder extends RecyclerView.ViewHolder {
            public View view_left, view_right;
            public TextView tv_day;
            public TextView tv_check_in_check_out;

            public DayViewHolder(@NonNull View itemView) {
                super(itemView);
                view_left = itemView.findViewById(R.id.view_left);
                view_right = itemView.findViewById(R.id.view_right);
                tv_day = itemView.findViewById(R.id.tv_day);
                tv_check_in_check_out = itemView.findViewById(R.id.tv_check_in_check_out);
            }
        }

        public class MonthViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_month, tv_year;

            public MonthViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_month = itemView.findViewById(R.id.tv_month);
                tv_year = itemView.findViewById(R.id.tv_year);
            }
        }

        public interface OnRecyclerviewItemClick {
            void onItemClick(View v, int position);
        }
    }


    private static boolean compareDate(String date) {

        Date before = getDates(date);
        SimpleDateFormat monFormats = new SimpleDateFormat("yyyy-MM-dd");
        Date monDates = new Date(System.currentTimeMillis());
        String months = monFormats.format(monDates);
        Date dates = getDates(months);
        long bef = before.getTime();
        long time = dates.getTime();
        if (bef > time) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 生成日历数据
     */
    private List<DateBean> days(String sDate, String eDate) {
        List<DateBean> dateBeans = new ArrayList<>();
        try {
            Calendar calendar = Calendar.getInstance();
            //日期格式化
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatYYYYMM = new SimpleDateFormat("yyyy-MM");

            //起始日期
            // Date startDate = new Date();
            // calendar.setTime(startDate);

            /***************************************************/
            calendar.add(Calendar.YEAR, -50);
//            calendar.add(Calendar.DAY_OF_MONTH, -100);
            Date startDate = new Date(calendar.getTimeInMillis());
            /***************************************************/

            //结束日期
            // calendar.add(Calendar.MONTH, 5);
            // Date endDate = new Date(calendar.getTimeInMillis());


            /***************************************************/
            Date endDate = new Date();
            /***************************************************/

            // Log.d(TAG, "startDate:" + format.format(startDate) + "----------endDate:" + format.format(endDate));

            //格式化开始日期和结束日期为 yyyy-mm-dd格式
            String endDateStr = format.format(endDate);
            endDate = format.parse(endDateStr);

            String startDateStr = format.format(startDate);
            startDate = format.parse(startDateStr);

            calendar.setTime(startDate);

            //  Log.d(TAG, "startDateStr:" + startDateStr + "---------endDate:" + format.format(endDate));
            //  Log.d(TAG, "endDateStr:" + endDateStr + "---------endDate:" + format.format(endDate));

            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Calendar monthCalendar = Calendar.getInstance();


            //按月生成日历 每行7个 最多6行 42个
            //每一行有七个日期  日 一 二 三 四 五 六 的顺序
            for (; calendar.getTimeInMillis() <= endDate.getTime(); ) {

                //月份item
                DateBean monthDateBean = new DateBean();
                monthDateBean.setDate(calendar.getTime());
                monthDateBean.setMonthStr(formatYYYYMM.format(monthDateBean.getDate()));
                monthDateBean.setItemType(DateBean.item_type_month);
                dateBeans.add(monthDateBean);

                //获取一个月结束的日期和开始日期
                monthCalendar.setTime(calendar.getTime());
                monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
                Date startMonthDay = calendar.getTime();

                monthCalendar.add(Calendar.MONTH, 1);
                monthCalendar.add(Calendar.DAY_OF_MONTH, -1);
                Date endMonthDay = monthCalendar.getTime();

                //重置为本月开始
                monthCalendar.set(Calendar.DAY_OF_MONTH, 1);

                //   Log.d(TAG, "月份的开始日期:" + format.format(startMonthDay) + "---------结束日期:" + format.format(endMonthDay));
                for (; monthCalendar.getTimeInMillis() <= endMonthDay.getTime(); ) {
                    //生成单个月的日历

                    //处理一个月开始的第一天
                    if (monthCalendar.get(Calendar.DAY_OF_MONTH) == 1) {
                        //看某个月第一天是周几
                        int weekDay = monthCalendar.get(Calendar.DAY_OF_WEEK);
                        switch (weekDay) {
                            case 1:
                                //周日
                                break;
                            case 2:
                                //周一
                                addDatePlaceholder(dateBeans, 1, monthDateBean.getMonthStr());
                                break;
                            case 3:
                                //周二
                                addDatePlaceholder(dateBeans, 2, monthDateBean.getMonthStr());
                                break;
                            case 4:
                                //周三
                                addDatePlaceholder(dateBeans, 3, monthDateBean.getMonthStr());
                                break;
                            case 5:
                                //周四
                                addDatePlaceholder(dateBeans, 4, monthDateBean.getMonthStr());
                                break;
                            case 6:
                                addDatePlaceholder(dateBeans, 5, monthDateBean.getMonthStr());
                                //周五
                                break;
                            case 7:
                                addDatePlaceholder(dateBeans, 6, monthDateBean.getMonthStr());
                                //周六
                                break;
                        }
                    }

                    //生成某一天日期实体 日item
                    DateBean dateBean = new DateBean();
                    dateBean.setDate(monthCalendar.getTime());
                    dateBean.setDay(monthCalendar.get(Calendar.DAY_OF_MONTH) + "");
                    dateBean.setMonthStr(monthDateBean.getMonthStr());
                    dateBeans.add(dateBean);

                    //处理一个月的最后一天
                    if (monthCalendar.getTimeInMillis() == endMonthDay.getTime()) {
                        //看某个月第一天是周几
                        int weekDay = monthCalendar.get(Calendar.DAY_OF_WEEK);
                        switch (weekDay) {
                            case 1:
                                //周日
                                addDatePlaceholder(dateBeans, 6, monthDateBean.getMonthStr());
                                break;
                            case 2:
                                //周一
                                addDatePlaceholder(dateBeans, 5, monthDateBean.getMonthStr());
                                break;
                            case 3:
                                //周二
                                addDatePlaceholder(dateBeans, 4, monthDateBean.getMonthStr());
                                break;
                            case 4:
                                //周三
                                addDatePlaceholder(dateBeans, 3, monthDateBean.getMonthStr());
                                break;
                            case 5:
                                //周四
                                addDatePlaceholder(dateBeans, 2, monthDateBean.getMonthStr());
                                break;
                            case 6:
                                addDatePlaceholder(dateBeans, 1, monthDateBean.getMonthStr());
                                //周5
                                break;
                        }
                    }

                    //天数加1
                    monthCalendar.add(Calendar.DAY_OF_MONTH, 1);
                }

                //  Log.d(TAG, "日期" + format.format(calendar.getTime()) + "----周几" + getWeekStr(calendar.get(Calendar.DAY_OF_WEEK) + ""));
                //月份加1
                calendar.add(Calendar.MONTH, 1);
            }

        } catch (Exception ex) {

        }

        return dateBeans;
    }

    //添加空的日期占位
    private void addDatePlaceholder(List<DateBean> dateBeans, int count, String monthStr) {
        for (int i = 0; i < count; i++) {
            DateBean dateBean = new DateBean();
            dateBean.setMonthStr(monthStr);
            dateBeans.add(dateBean);
        }
    }

    private String getWeekStr(String mWay) {
        if ("1".equals(mWay)) {
            mWay = "天";
        } else if ("2".equals(mWay)) {
            mWay = "一";
        } else if ("3".equals(mWay)) {
            mWay = "二";
        } else if ("4".equals(mWay)) {
            mWay = "三";
        } else if ("5".equals(mWay)) {
            mWay = "四";
        } else if ("6".equals(mWay)) {
            mWay = "五";
        } else if ("7".equals(mWay)) {
            mWay = "六";
        }
        return mWay;
    }

    public interface OnDateSelected {
        void selected(String startDate, String endDate);
    }

    public void setOnDateSelected(OnDateSelected onDateSelected) {
        this.onDateSelected = onDateSelected;
    }


    public static Date getDates(String date) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static Calendar reportDate() {

        Calendar startDateReport = Calendar.getInstance();
        startDateReport.add(Calendar.YEAR, -50);
//        startDateReport.add(startDateReport.DAY_OF_MONTH, -89);
        startDateReport.set(startDateReport.get(Calendar.YEAR), startDateReport.get(Calendar.MONTH), startDateReport.get(Calendar.DAY_OF_MONTH));
        return startDateReport;
    }


    private void clickOk() {
        if (onDateSelected != null) {
            if (startDate != null && endDate != null) {
                onDateSelected.selected(simpleDateFormat.format(startDate.getDate()), simpleDateFormat.format(endDate.getDate()));
            } else {
                if (startDate == null) {
                    ToastUtil.showToastShort("请选择开始时间");
                } else if (endDate == null) {
                    ToastUtil.showToastShort("请选择结束时间");
                }
            }
        }
    }
}
