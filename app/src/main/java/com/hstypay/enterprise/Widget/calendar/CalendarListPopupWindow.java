package com.hstypay.enterprise.Widget.calendar;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;

import com.hstypay.enterprise.R;

public class CalendarListPopupWindow extends PopupWindow
{

    private View mMenuView;
    private HandleBtn handleBtn;
    private Activity activity;
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     * @author Administrator
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn(String startTime, String endTime);
    }

    public CalendarListPopupWindow(Activity context, int height, HandleBtn handleBtn)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.calendar_pop_view, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        initView(mMenuView);
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        //this.setHeight(LayoutParams.WRAP_CONTENT);
//        this.setHeight(height);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener()
        {
            
            public boolean onTouch(View v, MotionEvent event)
            {
                
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    
    private void initView(View view)
    {
        CalendarList calendar = view.findViewById(R.id.calendarList);
        calendar.setOnDateSelected(new CalendarList.OnDateSelected() {
            @Override
            public void selected(String startDate, String endDate) {
                handleBtn.handleOkBtn(startDate,endDate);
                dismiss();
            }
        });
    }

}
