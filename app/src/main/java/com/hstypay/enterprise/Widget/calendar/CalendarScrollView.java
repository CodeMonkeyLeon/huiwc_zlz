package com.hstypay.enterprise.Widget.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class CalendarScrollView extends ScrollView {


    public CalendarScrollView(Context context) {
        this(context, null);
    }

    public CalendarScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CalendarScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

}
