package com.hstypay.enterprise.Widget.calendar;

import java.util.ArrayList;
import java.util.List;

public class Item implements  PickerView.PickerItem {

    private String text;

    public Item(String s) {
        text = s;
    }

    @Override
    public String getText() {
        return text;
    }

    public static List<Item> sampleItems() {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i <=23; i++) {
            if (i<10){

                items.add(new Item("0"+i));
            }else {
                items.add(new Item(""+i));

            }
        }
        return items;
    }

    public static List<Item> minItems() {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i <=59; i++) {
            if (i<10){

                items.add(new Item("0"+i));
            }else {
                items.add(new Item(""+i));

            }
        }
        return items;
    }
}
