package com.hstypay.enterprise.Widget.dialog

import android.app.Dialog
import android.content.Context
import com.hstypay.enterprise.R
import kotlinx.android.synthetic.main.dialog_activation_code_help.*

/**
 * @Author dean.zeng
 * @Description 激活码
 * @Date 2020-07-20 17:00
 */
class ActivationCodeHelpDialog(context: Context) : Dialog(context, R.style.bottom_dialog) {
    override fun show() {
        super.show()
        setContentView(R.layout.dialog_activation_code_help)
        img.setOnClickListener { dismiss() }
    }
}