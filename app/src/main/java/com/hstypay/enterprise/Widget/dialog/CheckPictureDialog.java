package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * Created by admin on 2017/7/12.
 */

public class CheckPictureDialog extends Dialog {

    Context context;
    private ConfirmListener confirmListener;
    private TextView ok;
    private TextView cancel;

    public CheckPictureDialog(Context context)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_check_picture);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.confirmListener = confirmListener;
        this.context = context;
        initView();

        setLister();
    }

    private void setLister() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
               // confirmListener.ok();
            }
        });

    }

    private void initView() {
        ok = (TextView) findViewById(R.id.ok);
    }

    public interface ConfirmListener
    {
        public void ok();

    }

}
