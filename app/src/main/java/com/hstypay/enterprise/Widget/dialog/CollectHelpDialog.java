package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.hstypay.enterprise.R;

/**
 * @Author dean.zeng
 * @Description 统计提示dialog
 * @Date 2020-07-08 11:00
 **/
public class CollectHelpDialog extends Dialog {

    public CollectHelpDialog(@NonNull Context context) {
        super(context, R.style.bottom_dialog);
    }


    @Override
    public void show() {
        super.show();
        setContentView(R.layout.dialog_collect_help);
        findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }
}
