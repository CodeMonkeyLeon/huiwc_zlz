package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;

/**
 * Created by admin on 2017/8/11.
 */

public class CommonConfirm extends Dialog {
    public CommonConfirm(@NonNull Context context) {
        super(context);
    }

    public CommonConfirm(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }
}
