package com.hstypay.enterprise.Widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.LoginActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.ProgressBean;
import com.hstypay.enterprise.bean.UpdateVersionBean;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.UIUtils;


/***
 * 常用对话框
 */
public class CommonConfirmDialog extends Dialog implements View.OnClickListener {
    private ProgressBar progress_download;
    private RelativeLayout rl_progress;
    private TextView tv_progress, tv_progress_size, mTvVersionCode;
    private ViewGroup mRootView;
    private Button btnCancel, btnConfirm;
    private ConfirmListener btnListener;
    private LinearLayout rlConfirm;
    private UpdateVersionBean.DataBean result;
    private SelectDialog dialogInfo;
    private boolean isWifiType;
    private Activity mContext;

    public CommonConfirmDialog(Activity context, ConfirmListener btnListener, UpdateVersionBean.DataBean result, boolean isWifi) {
        super(context);
        this.btnListener = btnListener;
        this.isWifiType = isWifi;
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mRootView = (ViewGroup) getLayoutInflater().inflate(com.hstypay.enterprise.R.layout.dialog_common_confirm, null);

        setContentView(mRootView);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.setWindowAnimations(R.style.Animation_Bottom_Dialog);
        WindowManager windowManager = context.getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.width = display.getWidth();
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(layoutParams);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        this.result = result;

        TextView tvTitle = mRootView.findViewById(R.id.title);
        if (result.getVersionTitle() != null) {
            tvTitle.setText(result.getVersionTitle());
        }
        TextView tvMsg = mRootView.findViewById(R.id.tv_msg);
        if (result.getVersionMsg() != null) {
            tvMsg.setText(result.getVersionMsg());
        }

        rlConfirm = (LinearLayout) mRootView.findViewById(R.id.rl_confirm);
        mTvVersionCode = (TextView) mRootView.findViewById(R.id.tv_version_code);
        btnConfirm = (Button) mRootView.findViewById(R.id.btn_confirm);
        btnCancel = (Button) mRootView.findViewById(R.id.btn_cancel);
        rl_progress = (RelativeLayout) mRootView.findViewById(R.id.rl_progress);
        progress_download = (ProgressBar) mRootView.findViewById(R.id.progress_download);
        tv_progress = (TextView) mRootView.findViewById(R.id.tv_progress);
        tv_progress_size = (TextView) mRootView.findViewById(R.id.tv_progress_size);
        progress_download.setMax(100);
        setView(isWifiType);
        btnConfirm.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        mTvVersionCode.setText(result.getVersionName());

        if (result.getForceUpgrade().intValue() == 0) {
            // 不是强制更新
            btnCancel.setVisibility(View.VISIBLE);
        } else if (result.getForceUpgrade().intValue() == 1) {
            setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {
                    if (keycode == KeyEvent.KEYCODE_BACK && arg2.getAction() == KeyEvent.ACTION_DOWN) {
                        if (dialogInfo == null) {
                            dialogInfo = new SelectDialog(mContext, UIUtils.getString(R.string.show_sign_out), UIUtils.getString(R.string.btnCancel), UIUtils.getString(R.string.btnOk), R.layout.select_common_dialog);
                            dialogInfo.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                                @Override
                                public void clickOk() {
                                    if (LoginActivity.instance != null) {
                                        LoginActivity.instance.finish();
                                    }
                                    MyApplication.getInstance().finishAllActivity();
                                }
                            });
                            DialogHelper.resize(mContext, dialogInfo);
                        }
                        if (!dialogInfo.isShowing()) {
                            dialogInfo.show();
                        } else {
                            dialogInfo.dismiss();
                        }
                        return true;
                    }
                    return false;
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                if (btnListener != null) {
                    btnListener.ok();
                }
                break;

            case R.id.btn_cancel:
                if (btnListener != null) {
                    this.cancel();
                    btnListener.cancel();
                }
                break;
            default:
                break;
        }
    }

    public interface ConfirmListener {
        void ok();

        void cancel();
    }

    public void setView(boolean type) {
        if (type) {
            rl_progress.setVisibility(View.INVISIBLE);
            rlConfirm.setVisibility(View.VISIBLE);
        } else {
            rl_progress.setVisibility(View.VISIBLE);
            rlConfirm.setVisibility(View.INVISIBLE);
        }
    }

    public void setProgress(ProgressBean progressBean) {
        long sum = progressBean.getDownSize();
        long total = progressBean.getTotalSize();
        int progress = (int) (sum * 1.0f / total * 100);
        progress_download.setProgress(progress);
        tv_progress.setText("下载进度：" + progress + "%");
        tv_progress_size.setText(FileUtils.getSizeStr(sum) + "/" + FileUtils.getSizeStr(total));
    }
}
