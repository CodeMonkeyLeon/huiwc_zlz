package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.hstypay.enterprise.R;


/**
 * @author zeyu.kuang
 * @time 2020/8/26
 * @desc 可自定义布局的底部弹窗dialog
 */
public class CustomViewBottomDialog extends Dialog {

    public CustomViewBottomDialog(@NonNull Context context) {
        this(context, R.style.bottom_dialog);
    }

    public CustomViewBottomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    public CustomViewBottomDialog setView(int layoutResID){
        setContentView(layoutResID);//可使用dialog.findViewById找控件
        return this;
    }

    public CustomViewBottomDialog setView(View view){
        setContentView(view);
        return this;
    }

    public View getContentView(){
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //params.windowAnimations = R.style.dialog_anim_style;//设置动画m1
        getWindow().setAttributes(params);
    }
}
