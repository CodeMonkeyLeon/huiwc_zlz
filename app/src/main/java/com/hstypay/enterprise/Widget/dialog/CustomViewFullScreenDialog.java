package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;

import com.hstypay.enterprise.R;


/**
 * @author zeyu.kuang
 * @time 2020/8/26
 * @desc 可自定义布局的全屏弹窗dialog
 */
public class CustomViewFullScreenDialog extends Dialog {


    public CustomViewFullScreenDialog(@NonNull Context context) {
        this(context, R.style.full_dialog_style);
    }

    public CustomViewFullScreenDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }


    public CustomViewFullScreenDialog setView(int layoutResID){
        setContentView(layoutResID);//可使用dialog.findViewById找控件
//        View mView = LayoutInflater.from(getContext()).inflate(layoutResID, null);// 不适合在根布局上设置margin
//        setContentView(mView);
        return this;
    }

    public CustomViewFullScreenDialog setView(View view){
        setContentView(view);
        return this;
    }

    public View getContentView(){
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().getDecorView().setPadding(0,0,0,0);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
    }


}
