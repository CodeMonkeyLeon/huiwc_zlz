/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;

public class DialogAttachInfo extends Dialog {
    private Context context;

    private TextView btnOk;

    private TextView btnCancel;

    private ViewGroup mRootView;

    private HandleBtn handleBtn;
    private TextView tv_edit_length;
    private EditText et_pay_remark;

    public void setBtnOkText(String string) {
        if (btnOk != null) {
            btnOk.setText(string);
        }
    }

    public DialogAttachInfo(Context context, HandleBtn handleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_attach_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        initView();
        setLinster();

    }


    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster() {
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                if (handleBtn != null) {
                    handleBtn.handleCancleBtn();
                }

            }

        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                if (handleBtn != null) {
                    handleBtn.handleOkBtn(et_pay_remark.getText().toString().trim());
                }

            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        tv_edit_length = mRootView.findViewById(R.id.tv_edit_length);
        et_pay_remark = mRootView.findViewById(R.id.et_pay_remark);
        btnCancel = mRootView.findViewById(R.id.btnCancel);
        btnOk = mRootView.findViewById(R.id.btnOk);

        et_pay_remark.setFocusable(true);
        et_pay_remark.setFocusableInTouchMode(true);
        et_pay_remark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv_edit_length.setText(et_pay_remark.getText().toString().trim().length() + "/30");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {

        void handleOkBtn(String s);

        void handleCancleBtn();

    }

    public void showKeyboard() {
        if (et_pay_remark != null) {
            //设置可获得焦点
            et_pay_remark.setFocusable(true);
            et_pay_remark.setFocusableInTouchMode(true);
            //请求获得焦点
            et_pay_remark.requestFocus();
            //调用系统输入法
            InputMethodManager inputManager = (InputMethodManager) et_pay_remark.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.showSoftInput(et_pay_remark, 0);
        }
    }

    public void setContent(String content) {
        et_pay_remark.setText(content);
        et_pay_remark.setSelection(content.length());
    }
}
