package com.hstypay.enterprise.Widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;


public class ScanInputDialog extends Dialog
{
    private EditText etInput;
    private Button btnOk, btnCancel;
    private ConfirmListener confirmListener;
    
    Activity context;
    
    public ScanInputDialog(Activity context, ConfirmListener confirmListener) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_input_scan);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.confirmListener = confirmListener;
        this.context = context;
        initView();
        
        setLister();
    }
    
    private void setLister() {
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtils.isEmptyOrNull(etInput.getText().toString().trim())) {
                    MyToast.showToastShort(UIUtils.getString(R.string.tv_input_code_null));
                    return;
                }
                dismiss();
                confirmListener.ok(etInput.getText().toString().trim());
            }
        });
    }
    
    private void initView() {
        etInput = (EditText)findViewById(R.id.edit_input);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        etInput.setFocusable(true);
        etInput.setFocusableInTouchMode(true);
        etInput.requestFocus();
        etInput.findFocus();

        etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(etInput.getText().toString().trim())){
                    btnOk.setBackgroundResource(R.drawable.btn_unsure_bg);
                    btnOk.setEnabled(false);
                }else {
                    btnOk.setBackgroundResource(R.drawable.btn_ensure_dialog_edit_selector);
                    btnOk.setEnabled(true);
                }
            }
        });
    }
    
    public interface ConfirmListener {
        void ok(String code);

    }
    
}
