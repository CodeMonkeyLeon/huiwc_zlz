/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;

public class ShareDialog extends Dialog
{
    private Context context;
    private ViewGroup mRootView;
    private HandleBtn handleBtn;
    private ImageView iv_share_dialog_close;
    private LinearLayout ll_share_wechat;
    private LinearLayout ll_share_alipay;

    public ShareDialog(Context context, HandleBtn handleBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.share_dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        initView();
        setLinster();

        mRootView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int top = mRootView.findViewById(R.id.pop_layout).getTop();
                int bottom = mRootView.findViewById(R.id.pop_layout).getBottom();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < top || y > bottom) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }


    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster()
    {
        iv_share_dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ll_share_wechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBtn.handleOkBtn("wx");
            }
        });

        ll_share_alipay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBtn.handleOkBtn("ali");
            }
        });

    }

    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        iv_share_dialog_close =mRootView.findViewById(R.id.iv_share_dialog_close);
        ll_share_wechat = mRootView.findViewById(R.id.ll_share_wechat);
        ll_share_alipay = mRootView.findViewById(R.id.ll_share_alipay);

    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     * @author Administrator
     *
     */
    public interface HandleBtn
    {

        void handleOkBtn(String s);

    }


}
