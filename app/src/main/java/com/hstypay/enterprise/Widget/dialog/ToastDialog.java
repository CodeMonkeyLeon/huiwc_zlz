package com.hstypay.enterprise.Widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.hstypay.enterprise.R;


/**
 * @Author dean.zeng
 * @Description 类似toast的dialog
 * @Date 2020-07-06 11:28
 **/
public class ToastDialog extends Dialog {

    private TextView mTvText;

    public ToastDialog(@NonNull Context context) {
        super(context, R.style.bottom_dialog);
    }


    @Override
    public void show() {
        super.show();
        setContentView(R.layout.dialog_toast);
        mTvText=findViewById(R.id.tvText);
        setCancelable(false);
    }

    /**
     * 设置显示内容
     */
    public void setContent(CharSequence text){
        mTvText.setText(text);
    }
}
