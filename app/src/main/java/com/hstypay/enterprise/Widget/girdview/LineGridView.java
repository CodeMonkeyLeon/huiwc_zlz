/*
 * 文 件 名:  LineGridView.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2017-5-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget.girdview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;

import com.hstypay.enterprise.R;

/**
 * 自定义
 * 
 * @author  he_hui
 * @version  [版本号, 2017-5-8]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class LineGridView extends GridView
{
    public LineGridView(Context context)
    {
        super(context);
    }
    
    public LineGridView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    public LineGridView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }
    
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
    
    @Override
    protected void dispatchDraw(Canvas canvas)
    {
        super.dispatchDraw(canvas);
        View localView1 = getChildAt(0);
        int column = getWidth() / localView1.getWidth();//列数
        int childCount = getChildCount();
        Paint localPaint;
        localPaint = new Paint();
        localPaint.setStyle(Paint.Style.FILL);
        localPaint.setColor(getContext().getResources().getColor(R.color.tx_line));//这个就是设置分割线的颜色
        for (int i = 0; i < childCount; i++)
        {
            View cellView = getChildAt(i);
            if ((i + 1) % column == 0)
            {//每一行最后一个
                canvas.drawLine(cellView.getLeft(),
                    cellView.getBottom(),
                    cellView.getRight(),
                    cellView.getBottom(),
                    localPaint);
            }
            else if ((i + 1) > (childCount - (childCount % column)))
            {//最后一行的item
                canvas.drawLine(cellView.getRight(),
                    cellView.getTop(),
                    cellView.getRight(),
                    cellView.getBottom(),
                    localPaint);
            }
            else
            {
                canvas.drawLine(cellView.getRight(),
                    cellView.getTop(),
                    cellView.getRight(),
                    cellView.getBottom(),
                    localPaint);
                canvas.drawLine(cellView.getLeft(),
                    cellView.getBottom(),
                    cellView.getRight(),
                    cellView.getBottom(),
                    localPaint);
            }
        }
    }
}
