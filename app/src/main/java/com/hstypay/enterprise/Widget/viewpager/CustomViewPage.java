/*
 * 文 件 名:  CustomViewPage.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2017-5-4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.Widget.viewpager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 自定义viewpage 解决listview嵌套viewpage问题
 * 
 * @author  he_hui
 * @version  [版本号, 2017-5-4]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CustomViewPage extends ViewPager
{
    
    public CustomViewPage(Context context)
    {
        super(context);
    }
    
    public CustomViewPage(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    float mDownX, mMovex, mDownY, mMoveY;
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev)
    {
        //目标不让外部的Viewpager拦截我们的事件
        
        //返回true:不拦截,事件回到我们这里
        //返回flase:拦截事件,不会回到这里
        switch (ev.getAction())
        {
            case MotionEvent.ACTION_DOWN://按下
                mDownX = ev.getX();
                mDownY = ev.getY();
                break;
            
            case MotionEvent.ACTION_MOVE://移动
                
                mMovex = ev.getX();
                mMoveY = ev.getY();
                
                //水平方向滑动的距离
                int dx = Math.round(Math.abs(mMovex - mDownX));
                
                //垂直方向滑动的距离
                int dy = Math.round(Math.abs(mMoveY - mDownY));
                
                //如果是水平方向的移动,那么Viewpager要自己处理
                if (dx > dy)
                {
                    
                    //左边界判断
                    //如果是第一张图片, && 向右边滑动:外部处理
                    if (getCurrentItem() == 0 && mMovex > mDownX)
                    {
                        getParent().requestDisallowInterceptTouchEvent(false);
                        
                    }
                    else if (getCurrentItem() == getAdapter().getCount() - 1 && mDownX > mMovex)
                    {
                        
                        //右边界判断:如果是最后一张图片 && 向左滑动:外部处理
                        getParent().requestDisallowInterceptTouchEvent(false);
                    }
                    else
                    {
                        
                        //是新闻顶部的轮播图,如果是北京界面的四张图片,并且不是第一张,那么就内部自己切换,内部处理
                        getParent().requestDisallowInterceptTouchEvent(true);
                        
                    }
                    
                }
                else
                {
                    //纵向移动更大,外部有listview,它需要纵向移动,所以我们把这个事件给他
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                
                break;
            
            case MotionEvent.ACTION_UP://抬起
                break;
        }
        
        return super.dispatchTouchEvent(ev);
    }
    
}
