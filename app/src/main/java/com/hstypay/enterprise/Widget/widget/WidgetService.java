package com.hstypay.enterprise.Widget.widget;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.RemoteViews;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.CollectActivity1;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.activity.PayActivity;
import com.hstypay.enterprise.activity.bill.BillActivity;

public class WidgetService extends Service {
    public static final int NOTIFICATION_ID = 0x11;
    public String TAG = "WidgetService ";
    public static final String SETTINGS_ACTION = "android.settings.APPLICATION_DETAILS_SETTINGS";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        updateWidget(WidgetService.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(SETTINGS_ACTION);
            broadcastIntent.setData(Uri.fromParts("package", getPackageName(), null));
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            String CHANNEL_ONE_ID = getPackageName();
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ONE_ID, "driver", NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription("description");
            mChannel.setShowBadge(false);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(mChannel);

            Notification notification = new Notification.Builder(this, CHANNEL_ONE_ID)
                    .setChannelId(CHANNEL_ONE_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("收款及语音播报接收中，请勿关闭")
                    .setContentIntent(pendingIntent)
                    .build();
            notification.flags = Notification.FLAG_ONGOING_EVENT;
            notification.flags |= Notification.FLAG_NO_CLEAR;
            notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
            startForeground(NOTIFICATION_ID, notification);
        }
    }

    private void updateWidget(Context context) {

        //通过 RemoteViews 加载布局文件
        //通过 setTextView 等方法实现对控件的控制
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.layout_widget);
//        remoteViews.setTextViewText(R.id.tv_date, "日  期：" + format);
//        remoteViews.setTextViewText(R.id.tv_money, "毫秒值：" + millis);

        PendingIntent payIntent = PendingIntent.getActivity(context, 0, new Intent(this, PayActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ll_pay, payIntent);   //点击跳转
        PendingIntent billIntent = PendingIntent.getActivity(context, 0, new Intent(this, BillActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ll_bill, billIntent);   //点击跳转
        PendingIntent reportIntent = PendingIntent.getActivity(context, 0, new Intent(this, CollectActivity1.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ll_report, reportIntent);   //点击跳转
        Intent intentDevice = new Intent(this, DeviceListActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent scanIntent = PendingIntent.getActivity(context, 0, intentDevice, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ll_scan, scanIntent);   //点击跳转

        ComponentName componentName = new ComponentName(this, MyWidget.class);
        AppWidgetManager.getInstance(this).updateAppWidget(componentName, remoteViews);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

}
