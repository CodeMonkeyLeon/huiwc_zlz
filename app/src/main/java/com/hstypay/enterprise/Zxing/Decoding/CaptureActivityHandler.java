/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hstypay.enterprise.Zxing.Decoding;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Zxing.Camera.CameraManager;
import com.hstypay.enterprise.Zxing.view.ViewfinderResultPointCallback;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.utils.IsChineseOrNot;
import com.hstypay.enterprise.utils.LogUtil;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

/**
 * This class handles all the messaging which comprises the state machine for capture.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class CaptureActivityHandler extends Handler
{

    private static final String TAG = CaptureActivityHandler.class.getSimpleName();

    private final CaptureActivity activity;

    private final DecodeThread decodeThread;

    private State state;

    private enum State
    {
        PREVIEW, SUCCESS, DONE
    }

    public CaptureActivityHandler(CaptureActivity activity, Vector<BarcodeFormat> decodeFormats, String characterSet)
    {
        this.activity = activity;
        decodeThread =
            new DecodeThread(activity, decodeFormats, characterSet, new ViewfinderResultPointCallback(
                activity.getViewfinderView()));
        decodeThread.start();
        state = State.SUCCESS;

        // Start ourselves capturing previews and decoding.
        CameraManager.get().startPreview();
        restartPreviewAndDecode();
    }

    @Override
    public void handleMessage(Message message)
    {
        switch (message.what)
        {
            case R.id.auto_focus:
                //Logger.d(TAG, "Got auto-focus message");
                // When one auto focus pass finishes, start another. This is the closest thing to
                // continuous AF. It does seem to hunt a bit, but I'm not sure what else to do.
                if (state == State.PREVIEW)
                {
                    CameraManager.get().requestAutoFocus(this, R.id.auto_focus);
                }
                //                Log.i("hehui", "auto_focus preview message");
                break;
            case R.id.restart_preview:
                //                Log.i("hehui", "Got restart preview message");
                restartPreviewAndDecode();
                break;
            case R.id.decode_succeeded:
                //                Log.i("hehui", "Got decode succeeded message");
                state = State.SUCCESS;
                Bundle bundle = message.getData();
                Bitmap barcode = bundle == null ? null : (Bitmap)bundle.getParcelable(DecodeThread.BARCODE_BITMAP);
                String str_result = ((Result)message.obj).getText().trim();
                String codeResult = getCodeResult(str_result);
//                activity.submitData(str_result, true);
                activity.submitData(codeResult, true);

                break;
            case R.id.decode_failed:
                //                Log.i("hehui", "decode_failed message");
                // We're decoding as fast as possible, so when one decode fails, start another.
                state = State.PREVIEW;
                CameraManager.get().requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
                break;
            case R.id.return_scan_result:
                //                Log.i("hehui", "Got return scan result message");
                str_result = ((Result)message.obj).getText();
                Intent intent = new Intent();
                intent.putExtra("code", str_result);
                //activity.setResult(Activity.RESULT_OK, (Intent) message.obj);
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
                break;
        }
    }

    public void quitSynchronously()
    {
        state = State.DONE;
        CameraManager.get().stopPreview();
        Message quit = Message.obtain(decodeThread.getHandler(), R.id.quit);
        quit.sendToTarget();
        try
        {
            decodeThread.join();
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            // continue
        }

        // Be absolutely sure we don't send any queued up messages
        removeMessages(R.id.decode_succeeded);
        //removeMessages(R.id.return_scan_result);
        removeMessages(R.id.decode_failed);
    }

    private void restartPreviewAndDecode()
    {
        if (state == State.SUCCESS) {
            state = State.PREVIEW;
            CameraManager.get().requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
            CameraManager.get().requestAutoFocus(this, R.id.auto_focus);
            activity.drawViewfinder();
        }
    }

    private String getCodeResult(String resultStr){
        String codeResult = "";
        boolean is_cN=false;
        try {
            LogUtil.d("------------"+resultStr);
            codeResult=new String(resultStr.getBytes("ISO-8859-1"),"UTF-8");
            LogUtil.d("这是转了UTF-8的"+codeResult);
            is_cN= IsChineseOrNot.isChineseCharacter(codeResult);
            //防止有人特意使用乱码来生成二维码来判断的情况
            boolean b=IsChineseOrNot.isSpecialCharacter(resultStr);
            if(b){
                is_cN=true;
            }
            LogUtil.d("是为:"+is_cN);
            if(!is_cN){
                codeResult=new String(resultStr.getBytes("ISO-8859-1"),"GB2312");
                LogUtil.d("这是转了GB2312的"+codeResult);
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return codeResult;
    }

}
