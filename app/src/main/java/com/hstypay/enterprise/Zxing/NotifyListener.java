package com.hstypay.enterprise.Zxing;

public class NotifyListener<T>
{
    public NotifyListener()
    {
        onPreExecute();
    }
    
    public void onPreExecute()
    {
        
    }
    
    public void onPostExecute()
    {
        
    }
    
    public void onSucceed(T result)
    {
        onPostExecute();
    }
    
    public void onError(Object result)
    {
        onPostExecute();
    }
    
    public void onProgress(int progress)
    {
        
    }
}
