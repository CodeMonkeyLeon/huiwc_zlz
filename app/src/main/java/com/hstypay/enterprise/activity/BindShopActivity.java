package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.MultiShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/25 10:09
 * @描述: 门店多选列表页
 */

public class BindShopActivity extends BaseActivity implements View.OnClickListener, MultiShopRecyclerAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack,mIvClean;
    private TextView mTvTitle, mButton;
    private RecyclerView mRvShop;
    private EditText mEtInput;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private TreeMap<String, String> selectMap = new TreeMap<>();
    private MultiShopRecyclerAdapter mAdapter;
    private TextView mTv_not_data;
    private TreeMap<String, String> bindStore;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_store);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();

    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mRvShop.setLayoutManager(new LinearLayoutManager(this));
        mEtInput = findViewById(R.id.et_user_input);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mTv_not_data = (TextView) findViewById(R.id.tv_not_data);

//        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mTvTitle.setText(R.string.title_select_shop);
        mButton.setText(R.string.tv_enter);
        initRecyclerView();
    }

    private void initRecyclerView() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.REQUEST_ADD_INTENT.equals(mIntentName)) {
            selectMap = new TreeMap<>((HashMap) getIntent().getSerializableExtra(Constants.RESULT_ADD_MANAGER));
        } else {
            selectMap = new TreeMap<>((HashMap) getIntent().getSerializableExtra(Constants.RESULT_EDIT_MANAGER));
            bindStore = new TreeMap<>((HashMap) getIntent().getSerializableExtra(Constants.REQUEST_MANAGER_BIND_STORE));
        }
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mAdapter = new MultiShopRecyclerAdapter(BindShopActivity.this, mList, selectMap);
        mRvShop.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
    }

    private void loadData(String storeName) {
        if (!NetworkUtils.isNetworkAvailable(BindShopActivity.this)) {
            showCommonNoticeDialog(BindShopActivity.this, getString(R.string.network_exception));
            return;
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(storeName)) {
                map.put("storeName", storeName);
            }
            ServerClient.newInstance(BindShopActivity.this).unbindManagerList(BindShopActivity.this, Constants.TAG_UNBIND_STORE_LIST, map);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mIvClean.setOnClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mList.clear();
                    loadData(mEtInput.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });
      /*  mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    mList.clear();
                    loadData("");
                }
            }
        });*/
        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtInput.isFocused()) {
                    if (mEtInput.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                        mTv_not_data.setVisibility(View.GONE);
                        mList.clear();
                        loadData("");
                    }
                }

            }
        });
        mEtInput.addTextChangedListener(editTextWatcher);
        mEtInput.setOnFocusChangeListener(listener);
    }

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_user_input:
                    if (hasFocus) {
                        if (mEtInput.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                            mTv_not_data.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };


    public void initData() {
        loadData("");
    }

    //Eventbus接收数据，所属门店
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_UNBIND_STORE_LIST)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_CASHIER_DETAIL_CHOICE_STORE);
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(BindShopActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(BindShopActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<StoreListBean.DataEntity> data = msg.getData();
                    if (bindStore != null && bindStore.size() > 0) {
                        for (String storeId : bindStore.keySet()) {
                            if (!TextUtils.isEmpty(mEtInput.getText().toString().trim())) {
                                if (bindStore.get(storeId).contains(mEtInput.getText().toString().trim())) {
                                    StoreListBean.DataEntity store = new StoreListBean().new DataEntity();
                                    store.setStoreId(storeId);
                                    store.setStoreName(bindStore.get(storeId));
                                    mList.add(store);
                                }
                            } else {
                                StoreListBean.DataEntity store = new StoreListBean().new DataEntity();
                                store.setStoreId(storeId);
                                store.setStoreName(bindStore.get(storeId));
                                mList.add(store);
                            }
                        }
                    }
                    if (data != null && data.size() > 0) {
                        mList.addAll(data);
                    }
                    LogUtil.d("size====" + mList.size());
                    if (mList != null && mList.size() > 0) {
                        mRvShop.setVisibility(View.VISIBLE);
                        mTv_not_data.setVisibility(View.GONE);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mTv_not_data.setVisibility(View.VISIBLE);
                        mRvShop.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (Constants.REQUEST_ADD_INTENT.equals(mIntentName)) {
                    if (selectMap == null || selectMap.size() == 0) {
                        MyToast.showToastShort(UIUtils.getString(R.string.please_choice_store));
                        return;
                    }
                }
                Intent intentShop = new Intent();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("data", selectMap);   //传递一个user对象列表
                intentShop.putExtras(mBundle);
                setResult(RESULT_OK, intentShop);
                finish();
                break;
            case R.id.iv_clean:
                mList.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                mTv_not_data.setVisibility(View.GONE);
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        if (mList.get(position).isSelcet()) {
            selectMap.put(mList.get(position).getStoreId(), mList.get(position).getStoreName());
        } else {
            selectMap.remove(mList.get(position).getStoreId());
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
