package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.paySite.PaySiteChoiceActivity;
import com.hstypay.enterprise.adapter.BindingCodeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.StoresBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/7/7.
 */

public class BindingCodeActivity extends BaseActivity implements View.OnClickListener, BindingCodeAdapter.OnRecyclerViewItemClickListener {

    private ImageView iv_back;
    private TextView tv_title, mTvBindingtitle, button_title;
    private RecyclerView mRvShop;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private BindingCodeAdapter mAdapter;
    private String mIntentName;
    private int mDataType;
    private SafeDialog mLoadDialog;
    private int mPostion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_code);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        mTvBindingtitle = (TextView) findViewById(R.id.tv_binding_title);
        button_title = findViewById(R.id.button_title);
        button_title.setVisibility(View.GONE);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());

    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO 触发刷新数据任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData("15", "1");
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                //TODO 触发加载更多任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "");
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initData() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mDataType = getIntent().getIntExtra(Constants.INTENT_STORE_DATA_TYPE, 0);
        if (Constants.INTENT_NAME_DEVICE_CAPTURE.equals(mIntentName)) {
            mTvBindingtitle.setText(R.string.please_check_device_store_code);
            tv_title.setText(R.string.tv_binding_device);
        } else if (Constants.INTENT_NAME_BIND.equals(mIntentName)) {
            mTvBindingtitle.setText(R.string.please_check_store_code);
            tv_title.setText(R.string.tv_binding_code);
        } else if (Constants.INTENT_NAME_CLOUD_CAPTURE.equals(mIntentName)) {
            mTvBindingtitle.setText(R.string.please_check_cloud_store_code);
            tv_title.setText(R.string.tv_binding_cloud_device);
        } else if (Constants.INTENT_NAME_DYNAMIC_CAPTURE.equals(mIntentName)) {
            mTvBindingtitle.setText(R.string.please_check_dynamic_store_code);
            tv_title.setText(R.string.tv_binding_dynamic_device);
        } else if (Constants.INTENT_NAME_FACE_CAPTURE.equals(mIntentName)) {
            mTvBindingtitle.setText(R.string.please_check_face_store_code);
            tv_title.setText(R.string.tv_binding_face_device);
        } else if (Constants.INTENT_NAME_CLOUD_PRINT.equals(mIntentName)) {
            mTvBindingtitle.setText(R.string.please_check_cloud_print_store_code);
            tv_title.setText(R.string.tv_binding_cloud_print_device);
        }
        mList = new ArrayList<>();
        mAdapter = new BindingCodeAdapter(BindingCodeActivity.this, mList);
        mRvShop.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        List<StoresBean> stores = (List<StoresBean>) getIntent().getSerializableExtra(Constants.INTENT_USER_STORE);
        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_USER_ID)) && stores != null) {
            for (int i = 0; i < stores.size(); i++) {
                StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                dataEntity.setStoreId(stores.get(i).getStoreId());
                dataEntity.setStoreName(stores.get(i).getStoreName());
                mList.add(dataEntity);
            }
            mAdapter.notifyDataSetChanged();
        } else {
            loadData(pageSize + "", "1");
        }
    }

    private void loadData(String pageSize, String currentPage) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
                showNewLoading(true, getString(R.string.public_loading));
            }
            //门店网络请求
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            if (mDataType == 1) {
                map.put("merchantDataType", mDataType);
            }
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_CHOICE_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }


    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_STORE)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null && msg.getError().getMessage() != null) {
                        MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_BIND_SITE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    toCapture(mPostion);
                    break;
                case Constants.ON_EVENT_FALSE:
                    toCapture(mPostion);
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getCashPointList() != null && msg.getData().getCashPointList().size() > 0) {
                        Intent intent = new Intent(this, PaySiteChoiceActivity.class);
                        intent.putExtra(Constants.INTENT_USER_ID, getIntent().getStringExtra(Constants.INTENT_USER_ID));
                        intent.putExtra(Constants.INTENT_BIND_STORE_ID, mList.get(mPostion).getStoreId());
                        intent.putExtra(Constants.INTENT_BIND_STORE_NAME, mList.get(mPostion).getStoreName());
                        intent.putExtra(Constants.INTENT_SITE_BINDING, (Serializable) msg.getData().getCashPointList());
                        intent.putExtra(Constants.INTENT_NAME, mIntentName);
                        startActivity(intent);
                    } else {
                        toCapture(mPostion);
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                //startActivity(new Intent(this,StoreCodeActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        //只有收款设备和收款码绑定是才去关联收银点
        if (mPostion != -1)
            mList.get(mPostion).setSelcet(false);
        mList.get(position).setSelcet(true);
        mPostion = position;
        mAdapter.notifyDataSetChanged();
        if ("2".equals(MyApplication.getCashPointOpenStatus())
                && (Constants.INTENT_NAME_DEVICE_CAPTURE.equals(mIntentName)
                || Constants.INTENT_NAME_BIND.equals(mIntentName))) {
            getList(mList.get(position).getStoreId());
        } else {
            toCapture(position);
        }
        /*Intent intent = new Intent(this, PaySiteChoiceActivity.class);
        intent.putExtra(Constants.INTENT_USER_ID, getIntent().getStringExtra(Constants.INTENT_USER_ID));
        intent.putExtra(Constants.INTENT_BIND_STORE_ID, mList.get(mPostion).getStoreId());
        intent.putExtra(Constants.INTENT_BIND_STORE_NAME, mList.get(mPostion).getStoreName());
//        intent.putExtra(Constants.INTENT_SITE_BINDING, (Serializable) msg.getData().getCashPointList());
        intent.putExtra(Constants.INTENT_NAME, mIntentName);
        startActivity(intent);*/
    }

    private void toCapture(int position) {
        Intent intent = new Intent(this, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_USER_ID, getIntent().getStringExtra(Constants.INTENT_USER_ID));
        intent.putExtra(Constants.INTENT_BIND_STORE_ID, mList.get(position).getStoreId());
        intent.putExtra(Constants.INTENT_BIND_STORE_NAME, mList.get(position).getStoreName());
        intent.putExtra(Constants.INTENT_NAME, mIntentName);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            /*if (Constants.INTENT_NAME_BIND.equals(mIntentName)) {
                startActivity(new Intent(this, StoreCodeActivity.class));
            }*/
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void getList(String storeMerchantId) {
        //门店网络请求
        DialogUtil.safeShowDialog(mLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", 15);
        map.put("currentPage", 1);
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeId", storeMerchantId);
        }
        ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_BIND_SITE_LIST, map);
    }
}
