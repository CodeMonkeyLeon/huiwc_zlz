/*
 * 文 件 名:  BluetoothSettingActivity.java
 * 版    权:
 * 描    述:  蓝牙打印设置
 * 修 改 人: zw
 * 修改时间:
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.setting.InstructionsActivity;
import com.hstypay.enterprise.activity.setting.PrintTypeActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BluetoothDeviceBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.HandlerManager;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.PreferenceUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.MarketListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

/**
 * 蓝牙打印设置列表
 *
 * @author
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BluetoothSetActivity extends BaseActivity {
    private MyAdape myAdape;
    private ViewPayTypeHolder holder;
    private RelativeLayout mRlChoice;
    private BluetoothAdapter mBtAdapter;
    private List<BluetoothDeviceBean> deviceList;
    private MarketListView pairedListView;
    private ImageView iv_voice;
    private TextView tv_null_info, mButton, mTvTitle;
    boolean connState;//连接状态
    private TextView tv_blue_scaning, mBtnAboutPrint, tv_blue_help_two;
    private ImageView iView, mIvButton;
    private boolean auto;

    private ImageView mIvBack;

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1://连接失败
                    dismissLoading();
                    showCommonNoticeDialog(BluetoothSetActivity.this, getString(R.string.tx_blue_conn_fail));
                    break;
                case 2://蓝牙可以被搜索
                    break;
                case 3://设备已经接入
                    break;
                case 4://已连接某个设备
                    dismissLoading();
                    /*if (iView != null) {
                        iView.setVisibility(View.VISIBLE);
                    }*/
                    myAdape.notifyDataSetChanged();
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    LogUtil.i("zhouwei", "连接成功");
                    showCommonNoticeDialog(BluetoothSetActivity.this, getString(R.string.tx_blue_conn_succ));
                    break;
                case HandlerManager.BLUE_CONNET_STUTS_CLOSED://蓝牙关闭
                    pairedListView.setVisibility(View.GONE);
                    tv_blue_scaning.setVisibility(View.GONE);
                    ly_sv.setVisibility(View.GONE);
                    tv_null_info.setVisibility(View.VISIBLE);
                    mLlClose.setVisibility(View.VISIBLE);
                    tv_null_info.setText(R.string.tx_blue_closed);
                    break;
                case HandlerManager.BLUE_CONNET_STUTS://蓝牙已连接上后
                    if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
                        pairedListView.setVisibility(View.VISIBLE);
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        ly_sv.setVisibility(View.VISIBLE);
                        tv_null_info.setText(R.string.tx_blue_close);
                        tv_null_info.setVisibility(View.GONE);
                        mLlClose.setVisibility(View.GONE);
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        doDiscovery();
                    }
                    break;
            }
        }
    };

    /**
     * 蓝牙UUID
     */
    public static UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    /**
     * 配对成功后的蓝牙套接字
     */
    private BluetoothSocket mBluetoothSocket;
    private ImageView mIv_auto_printm;
    private LinearLayout mLy_auto_printm;
    private TextView mTvLinkPrintTitle;
    private int mPrintDeviceType;

    /**
     * eturn 返回 mBluetoothSocket
     */
    public BluetoothSocket getmBluetoothSocket() {
        return mBluetoothSocket;
    }

    /**
     * param 对mBluetoothSocket进行赋值
     */
    public void setmBluetoothSocket(BluetoothSocket mBluetoothSocket) {
        this.mBluetoothSocket = mBluetoothSocket;
    }

    /**
     * @return 返回 mBtAdapter
     */
    public BluetoothAdapter getmBtAdapter() {
        return mBtAdapter;
    }

    private ScrollView ly_sv;

    private LinearLayout mLlClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_setting);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);

        initView();
        setLister();
        initValue();
        //注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);

    }

    private void initValue() {
        mPrintDeviceType = getIntent().getIntExtra(Constants.INTENT_PRINT_DEVICE_TYPE, 1);
        switch (mPrintDeviceType) {
            case 1:
                mTvTitle.setText(R.string.tv_pay_print);
                mIvButton.setVisibility(View.VISIBLE);
                break;
            case 2:
                mTvTitle.setText(R.string.tv_reception_print);
                mTvLinkPrintTitle.setText(R.string.tv_reception_print);
                mIvButton.setVisibility(View.INVISIBLE);
                break;
            case 3:
                mTvTitle.setText(R.string.tv_kitchen_print);
                mTvLinkPrintTitle.setText(R.string.tv_kitchen_print);
                mIvButton.setVisibility(View.INVISIBLE);
                break;
        }
        deviceList = new ArrayList<>();
        myAdape = new MyAdape(BluetoothSetActivity.this, deviceList);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        pairedListView.setAdapter(myAdape);
        connState = PreferenceUtil.getBoolean("connState", true);

        if (MyApplication.getAutoPrinter()) {
            mIv_auto_printm.setImageResource(R.mipmap.ic_switch_open);
        } else {
            mIv_auto_printm.setImageResource(R.mipmap.ic_switch_close);
        }

        if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
            iv_voice.setImageResource(R.mipmap.ic_switch_open);
            pairedListView.setVisibility(View.VISIBLE);
            tv_null_info.setVisibility(View.GONE);
            ly_sv.setVisibility(View.VISIBLE);
            mLlClose.setVisibility(View.GONE);
            if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(mPrintDeviceType))) {
                if (!mBtAdapter.isEnabled()) { //蓝牙没有打开
                    openBlue(true);
                } else {
                    doDiscovery();
                }
            } else {
                //默认去加载收索附件的蓝牙设备
                if (mBtAdapter == null) {//说明手机不带蓝牙模块
                    blueCloseState();
                    showCommonNoticeDialog(BluetoothSetActivity.this, getString(R.string.tx_blue_no_device), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            BluetoothSetActivity.this.finish();
                        }
                    });
                    return;
                }

                if (!mBtAdapter.isEnabled()) { //蓝牙没有打开
                    openBlue(false);
                } else {
                    doDiscovery();
                }
            }
        } else {
            tv_blue_scaning.setVisibility(View.GONE);
            blueCloseState();
        }
    }

    /**
     * 用户打开蓝牙时，选择的返回处理
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                doDiscovery();
            } else if (resultCode == RESULT_CANCELED) {
                showCommonNoticeDialog(BluetoothSetActivity.this, getString(R.string.tx_blue_open_fail)
                        , new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                BluetoothSetActivity.this.finish();
                            }
                        });
            }
        }
    }

    private void openBlue(final boolean isFinish) {
        SelectDialog selectDialog = new SelectDialog(BluetoothSetActivity.this, UIUtils.getString(R.string.tx_blue_no_open)
                , UIUtils.getString(R.string.btnCancel), UIUtils.getString(R.string.to_open), R.layout.select_common_dialog);
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                tv_blue_scaning.setVisibility(View.VISIBLE);
                Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(mIntent, 1);
            }
        });
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                if (isFinish) {
                    BluetoothSetActivity.this.finish();
                }
            }
        });
        DialogHelper.resize(BluetoothSetActivity.this, selectDialog);
        selectDialog.show();
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        tv_blue_scaning.setVisibility(View.VISIBLE);
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        deviceList.clear();
        mBtAdapter.startDiscovery();
    }

    void blueCloseState() {
        iv_voice.setImageResource(R.mipmap.ic_switch_close);
        pairedListView.setVisibility(View.GONE);
        tv_null_info.setVisibility(View.GONE);
        ly_sv.setVisibility(View.GONE);
        mLlClose.setVisibility(View.VISIBLE);
    }

    /**
     * 尝试配对和连接
     *
     * @param btDev
     */
    public void createBond(BluetoothDevice btDev, Handler handler) {
        BluetoothSetActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                showNewLoading(true, getString(R.string.tx_blue_conn_deviceings));
            }
        });
        connect(btDev, handler);


    }

    /**
     * 尝试连接一个设备，子线程中完成，因为会线程阻塞
     *
     * @param btDev   蓝牙设备对象
     * @param handler 结果回调事件
     * @return
     */
    private void connect(BluetoothDevice btDev, Handler handler) {
        Message message = new Message();
        try {
            //通过和服务器协商的uuid来进行连接
            mBluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
//            mBluetoothSocket = btDev.createInsecureRfcommSocketToServiceRecord(SPP_UUID);

            if (mBluetoothSocket != null) {
                switch (mPrintDeviceType) {
                    case 1:
                        MyApplication.bluetoothSocket = mBluetoothSocket;
                        break;
                    case 2:
                        MyApplication.bluetoothSocket2 = mBluetoothSocket;
                        break;
                    case 3:
                        MyApplication.bluetoothSocket3 = mBluetoothSocket;
                        break;
                }
            }
//                MyApplication.bluetoothSocket = mBluetoothSocket;
            LogUtil.d("blueTooth", "开始连接...");
            //在建立之前调用
            if (getmBtAdapter().isDiscovering()) {
                //停止搜索
                getmBtAdapter().cancelDiscovery();
            }
            //如果当前socket处于非连接状态则调用连接
            if (!getmBluetoothSocket().isConnected()) {
                //你应当确保在调用connect()时设备没有执行搜索设备的操作。
                // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                getmBluetoothSocket().connect();
            }
            LogUtil.i("hehui", "已经链接");
            if (handler == null)
                return;
            //结果回调
            /*MyApplication.setBlueDeviceName(btDev.getName());
            MyApplication.setBlueDeviceNameAddress(btDev.getAddress());
            MyApplication.setBlueState(true);*/
            message.what = 4;
            message.obj = btDev;
            handler.sendMessage(message);
            for (int i = 0; i < deviceList.size(); i++) {
                if (deviceList.get(i).getBluetoothDevice().getAddress().equalsIgnoreCase(btDev.getAddress())) {
                    deviceList.get(i).setConnected(true);
                } else {
                    deviceList.get(i).setConnected(false);
                }
            }
            MyApplication.stayPrintDevice(mPrintDeviceType, btDev);
        } catch (Exception e) {
            MyApplication.stayPrintDevice(mPrintDeviceType, btDev);
            LogUtil.e("hehui", "...链接失败" + e);
            try {
                message.what = 1;
                handler.sendMessage(message);
                getmBluetoothSocket().close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    /**
     * 按钮设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLister() {

        //是否自动打印
        mLy_auto_printm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!MyApplication.getAutoPrinter()) {
                    mIv_auto_printm.setImageResource(R.mipmap.ic_switch_open);
                    MyApplication.setAutoPrinter(true);

                } else {
                    mIv_auto_printm.setImageResource(R.mipmap.ic_switch_close);
                    MyApplication.setAutoPrinter(false);

                }
            }
        });
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //帮助
        mBtnAboutPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BluetoothSetActivity.this, InstructionsActivity.class);
                intent.putExtra(Constants.INTENT_INSTRUCTION, Constants.URL_BLUETOOTH_INSTRUCTION);
                startActivity(intent);
            }
        });
        pairedListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, final View view, int position, long arg3) {
                final BluetoothDevice bluetoothDevice = deviceList.get(position).getBluetoothDevice();

                loadDialog(BluetoothSetActivity.this, getString(R.string.tx_blue_conn_deviceings));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        createBond(bluetoothDevice, handler);
                    }
                }).start();
            }
        });

        mIvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BluetoothSetActivity.this, PrintTypeActivity.class));
            }
        });
        mRlChoice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
                    iv_voice.setImageResource(R.mipmap.ic_switch_close);
                    tv_null_info.setVisibility(View.GONE);
                    mLlClose.setVisibility(View.VISIBLE);
                    connState = false;
                    deviceList.clear();
//                    MyApplication.setBluePrintSetting(false);
                    /*MyApplication.setBlueDeviceName("");
                    MyApplication.setBlueDeviceNameAddress("");
                    MyApplication.setBlueState(false);
                    MyApplication.bluetoothSocket = null;*/
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    pairedListView.setVisibility(View.GONE);
                    tv_blue_scaning.setVisibility(View.GONE);
                    ly_sv.setVisibility(View.GONE);
                    Properties prop = new Properties();
                    prop.setProperty("switch", "OFF");
                    MyApplication.setDeviceEnable(mPrintDeviceType, false);
                } else {
                    MyApplication.setDeviceEnable(mPrintDeviceType, true);
                    connState = true;
                    deviceList.clear();
//                    MyApplication.setBluePrintSetting(true);
                    ly_sv.setVisibility(View.VISIBLE);
                    iv_voice.setImageResource(R.mipmap.ic_switch_open);
                    pairedListView.setVisibility(View.VISIBLE);
                    tv_null_info.setVisibility(View.GONE);
                    mLlClose.setVisibility(View.GONE);
                    if (!mBtAdapter.isEnabled()) { //蓝牙没有打开
                        openBlue(false);
                    } else {
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        doDiscovery();
                    }
                }

            }
        });
    }

    @Override
    protected void onStop() {
        if (mBtAdapter != null && mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        this.unregisterReceiver(mReceiver);
        super.onStop();
    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);
        super.onResume();
    }

    /**
     * 打开蓝牙
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtil.i("zw", "mReceiver actioin-->" + action);
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                try {
                    if (device != null) { //蓝牙打印设备类似
                        for (int i = 0; i < deviceList.size(); i++) {
                            BluetoothDevice info = deviceList.get(i).getBluetoothDevice();
                            if (StringUtils.isEmptyOrNull(device.getName())
                                    || info.getAddress().equalsIgnoreCase(device.getAddress())) {
                                return;
                            }
                        }
                        if (MyApplication.getDeviceEnable(mPrintDeviceType)) {
                            if (!StringUtils.isEmptyOrNull(device.getName())) {
                                BluetoothDeviceBean bluetoothDeviceBean = new BluetoothDeviceBean();
                                bluetoothDeviceBean.setBluetoothDevice(device);
                                String address = MyApplication.getBluetoothAddress(mPrintDeviceType);
                                if (!StringUtils.isEmptyOrNull(address) && address.equalsIgnoreCase(device.getAddress())) {//判断是否连接
                                    //bluetoothDeviceBean.setConnected(true);
                                    BluetoothSocket bluetoothSocket = bluetoothDeviceBean.getBluetoothDevice().createRfcommSocketToServiceRecord(SPP_UUID);
                                    if (bluetoothSocket != null && bluetoothSocket.isConnected()) {
                                        bluetoothDeviceBean.setConnected(true);
                                    } else {
                                        bluetoothDeviceBean.setConnected(false);
                                    }
                                } else {
                                    bluetoothDeviceBean.setConnected(false);
                                }

                                deviceList.add(bluetoothDeviceBean);
                            }
                            myAdape.notifyDataSetChanged();
                            myAdape.notifyDataSetInvalidated();
                        }
                    }

                } catch (Exception e) {
                    LogUtil.e("zw", "" + e + ",device.getName()-->" + device.getName());
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                tv_blue_scaning.setVisibility(View.GONE);
                dismissLoading();
            }
        }
    };

    /**
     * 页面参数 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        tv_blue_help_two = (TextView) findViewById(R.id.tv_blue_help_two);
        mBtnAboutPrint = (Button) findViewById(R.id.btn_about_print);
        tv_blue_scaning = (TextView) findViewById(R.id.tv_blue_scaning);
        mLlClose = (LinearLayout) findViewById(R.id.ly_close);
        ly_sv = (ScrollView) findViewById(R.id.ly_sv);
        tv_null_info = (TextView) findViewById(R.id.tv_null_info);
        iv_voice = (ImageView) findViewById(R.id.iv_voice);
        mRlChoice = findViewById(R.id.rl_choice);
        pairedListView = (MarketListView) findViewById(R.id.paired_devices);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvButton = (ImageView) findViewById(R.id.iv_button);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton.setVisibility(View.INVISIBLE);
        mIvButton.setVisibility(View.VISIBLE);
        mIvButton.setImageResource(R.mipmap.print_type_set);
        mIv_auto_printm = (ImageView) findViewById(R.id.iv_auto_print);
        mLy_auto_printm = (LinearLayout) findViewById(R.id.ly_auto_print);
        mTvLinkPrintTitle = (TextView) findViewById(R.id.tv_link_print_title);

        mTvTitle.setText(R.string.title_print_set);
    }

    class MyAdape extends BaseAdapter {

        private List<BluetoothDeviceBean> list;

        private Context context;

        private MyAdape(Context context, List<BluetoothDeviceBean> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = View.inflate(context, R.layout.buletooth_list_item, null);
                holder = new ViewPayTypeHolder();
                holder.iv_choice = (ImageView) convertView.findViewById(R.id.iv_choice);
                holder.userName = (TextView) convertView.findViewById(R.id.userName);
                convertView.setTag(holder);
            } else {
                holder = (ViewPayTypeHolder) convertView.getTag();
            }
            BluetoothDevice deviceInfo = list.get(position).getBluetoothDevice();
            if (!StringUtils.isEmptyOrNull(deviceInfo.getName())) {
                holder.userName.setText(deviceInfo.getName());
            }
            String address = MyApplication.getBluetoothAddress(mPrintDeviceType);
            /*LogUtil.d("Jeremy==="+address+"--"+deviceInfo.getAddress());
            if (!StringUtils.isEmptyOrNull(address) && address.equalsIgnoreCase(deviceInfo.getAddress())) {//判断是否连接
                holder.iv_choice.setVisibility(View.VISIBLE);
            } else {
                holder.iv_choice.setVisibility(View.GONE);
            }*/
            if (list.get(position).isConnected()) {//判断是否连接
                holder.iv_choice.setVisibility(View.VISIBLE);
            } else {
                holder.iv_choice.setVisibility(View.GONE);
            }
            return convertView;
        }
    }

    class ViewPayTypeHolder {
        private ImageView iv_choice;
        private TextView userName;
    }
}
