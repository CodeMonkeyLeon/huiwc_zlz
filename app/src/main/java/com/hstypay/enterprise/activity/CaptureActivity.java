package com.hstypay.enterprise.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.EditCommonDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectCloseDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Zxing.Camera.CameraManager;
import com.hstypay.enterprise.Zxing.Decoding.CaptureActivityHandler;
import com.hstypay.enterprise.Zxing.Decoding.InactivityTimer;
import com.hstypay.enterprise.Zxing.view.ViewfinderView;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintActivity;
import com.hstypay.enterprise.activity.cloundSound.CloudDeviceActivity;
import com.hstypay.enterprise.activity.coupon.CouponDetailActivity;
import com.hstypay.enterprise.activity.dynamicCode.DynamicCodeListActivity;
import com.hstypay.enterprise.activity.faceDevice.FaceDeviceListActivity;
import com.hstypay.enterprise.activity.pledge.PledgeActivity;
import com.hstypay.enterprise.activity.pledge.PledgeDetailActivity;
import com.hstypay.enterprise.activity.pledge.PledgeResultActivity;
import com.hstypay.enterprise.activity.receiveDevice.ReceiveDeviceListActivity;
import com.hstypay.enterprise.activity.storeCode.StaticCodeImageActivity;
import com.hstypay.enterprise.activity.storeCode.StoreCodeListActivity;
import com.hstypay.enterprise.activity.vanke.OrderCouponInfoActivity;
import com.hstypay.enterprise.activity.vanke.OrderPayInfoActivity;
import com.hstypay.enterprise.activity.vipCard.VipCountCheckActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.bean.BindCodeBean;
import com.hstypay.enterprise.bean.DeviceInfoBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.QrcodeBean;
import com.hstypay.enterprise.bean.ScanLoginBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.ThirdOrderBean;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoBean;
import com.hstypay.enterprise.bean.vanke.BizDetailBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.bean.vanke.CouponListBean;
import com.hstypay.enterprise.bean.vanke.VipInfoBean;
import com.hstypay.enterprise.bean.vanke.VipInfoData;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PayBeanUtil;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class CaptureActivity extends BaseActivity implements Callback, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private CaptureActivityHandler handler;

    private RadioButton mRbLogin, mRbVerify, mRbRefund;
    private RadioGroup mRgBottom;
    private ViewfinderView viewfinderView;
    private boolean hasSurface;
    private Vector<BarcodeFormat> decodeFormats;
    private boolean mNeedFlashLightOpen = true;//lignt
    private InactivityTimer inactivityTimer;
    private Context mContext;
    private LinearLayout ly_back, ll_change_qrcode;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    private LinearLayout ly_open_light, mLlBottom;
    private RelativeLayout rlInputPayCode;
    private ImageView iv_light, mDotLogin, mDotVerify, mDotRefund;
    private TextView tv_money, tv_title, mTvTitleStore, tv_input_sn_code, mTvScanNotice, tvInputPayCode, tvScanLoginTitle, tvCaptureScanLogin;
    private double doubleMoney, textMoney;
    private String mStore_id, intentName, storeName, money, characterSet, mOutTradeNo, mPayRemark, mPayHbfq, mSiteName, mSiteId;
    private boolean mIsRateFree;
    private PayBean.DataBean mData;
    private Handler mHandler;
    private int mCouponType;

    private List<CouponInfoData> mCouponList;
    private MediaPlayer mediaPlayer;
    private String contentText, mInterProviderType;
    private static final float BEEP_VOLUME = 0.10f;
    private boolean playBeep, vibrate;
    private PledgePayBean.DataBean mPledgeData;
    private SafeDialog mLoadDialog;
    private SelectCloseDialog mSelectDialog;
    private long mUnionUserFee;
    private boolean cancelTask;
    private String mOrderNo;//平台订单号
    private VipInfoData mVipInfo;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private String[] permissionArray = new String[]{
            Manifest.permission.CAMERA
    };
    private String linkCode = "";
    private String mStoreId = "";
    private String mCouponId;
    private String mStateCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_capture);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        mContext = this;
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mPayRemark = getIntent().getStringExtra(Constants.INTENT_PAY_REMARK);//备注
        mPayHbfq = getIntent().getStringExtra(Constants.INTENT_HBFQ);//花呗分期
        mIsRateFree = getIntent().getBooleanExtra(Constants.INTENT_IS_RATE_FREE, false);//花呗分期是否免息
        mUnionUserFee = getIntent().getLongExtra(Constants.INTENT_UNION_USER_FEE, 0);//花呗分期用户付息费用
        mInterProviderType = getIntent().getStringExtra(Constants.INTENT_COUPON_PROVIDER_TYPE);//优惠券类型
        BigDecimal unionUserFee = new BigDecimal(mUnionUserFee).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
        doubleMoney = getIntent().getDoubleExtra(Constants.INTENT_PAY_MONEY, 0);//金额
        mCouponType = getIntent().getIntExtra(Constants.INTENT_COUPON_TYPE, -1);//券类型
        if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            textMoney = getIntent().getDoubleExtra(Constants.INTENT_ACTUAL_MONEY, 0);//实付金额
        } else {
            textMoney = doubleMoney + unionUserFee.doubleValue();//金额
        }
        LogUtil.d("money--", doubleMoney * 100 + "");
        money = String.valueOf(BigDecimal.valueOf(doubleMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
        LogUtil.d("money", money);
        //门店Id
        mStore_id = getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
        CameraManager.init(this);
        mHandler = new Handler();
        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        contentText = getIntent().getStringExtra(Constants.INTENT_H5_SCAN);
        storeName = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_NAME);
        mSiteName = getIntent().getStringExtra(Constants.INTENT_SITE_NAME);
        mSiteId = getIntent().getStringExtra(Constants.INTENT_SITE_ID);
        mVipInfo = (VipInfoData) getIntent().getSerializableExtra(OrderPayInfoActivity.KEY_VANKE_VIP);
        initView();
        setLister();

        if (money != null) {
            tv_money.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(textMoney));
        }

        boolean results = PermissionUtils.checkPermissionArray(CaptureActivity.this, permissionArray);
        if (!results) {
            showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_capture));
        }
        mCouponList = new ArrayList<>();
        mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
    }

    /*private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(CaptureActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(CaptureActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }*/

    public static boolean isCameraUseable() {
        boolean canUse = true;
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
            // setParameters 是针对魅族MX5。MX5通过Camera.open()拿到的Camera对象不为null
            Camera.Parameters mParameters = mCamera.getParameters();
            mCamera.setParameters(mParameters);
        } catch (Exception e) {
            canUse = false;
        }
        if (mCamera != null) {
            mCamera.release();
        }
        return canUse;
    }


    private void setLister() {
        ly_back.setOnClickListener(this);
        ly_open_light.setOnClickListener(this);
        ll_change_qrcode.setOnClickListener(this);
        tv_input_sn_code.setOnClickListener(this);
        tvInputPayCode.setOnClickListener(this);
        mTvTitleStore.setOnClickListener(this);
        mRgBottom.setOnCheckedChangeListener(this);
    }

    private void initView() {
        ly_back = (LinearLayout) findViewById(R.id.ly_back);
        ly_open_light = (LinearLayout) findViewById(R.id.ly_open_light);
        iv_light = (ImageView) findViewById(R.id.iv_light);
        tv_money = (TextView) findViewById(R.id.tv_money);
        tvScanLoginTitle = (TextView) findViewById(R.id.tv_scan_login_title);
        tv_title = (TextView) findViewById(R.id.tv_title);
        mTvTitleStore = (TextView) findViewById(R.id.tv_title_store);
        tv_input_sn_code = (TextView) findViewById(R.id.tv_input_sn_code);
        tvCaptureScanLogin = (TextView) findViewById(R.id.tv_capture_scan_login);
        rlInputPayCode = (RelativeLayout) findViewById(R.id.rl_input_pay_code);
        tvInputPayCode = (TextView) findViewById(R.id.tv_input_pay_code);
        ll_change_qrcode = (LinearLayout) findViewById(R.id.ll_change_qrcode);

        mLlBottom = (LinearLayout) findViewById(R.id.ll_bottom);
        mRgBottom = findViewById(R.id.rg_bottom);
        mRbLogin = findViewById(R.id.rb_login);
        mRbVerify = findViewById(R.id.rb_verify);
        mRbRefund = findViewById(R.id.rb_refund);
        mDotLogin = findViewById(R.id.iv_dot_login);
        mDotVerify = findViewById(R.id.iv_dot_verify);
        mDotRefund = findViewById(R.id.iv_dot_refund);

        mTvScanNotice = (TextView) findViewById(R.id.tv_code_info);
        if (Constants.INTENT_NAME_DEVICE_CAPTURE.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫码可将简易pos与门店【" + storeName + "】绑定");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            //showBindDeviceDialog();
        } else if ("INTENT_BIND_DEVICE".equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫描设备SN码绑定设备");
            } else {
                mTvScanNotice.setText("扫描设备SN绑定设备\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            //showBindDeviceDialog();
        } else if (Constants.INTENT_NAME_FACE_CAPTURE.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫码可将刷脸设备与门店【" + storeName + "】绑定");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            //showBindDeviceDialog();
        } else if (Constants.INTENT_NAME_DYNAMIC_CAPTURE.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫码可将电子台卡与门店【" + storeName + "】绑定");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            //showBindDeviceDialog();
        } else if (Constants.INTENT_NAME_CLOUD_CAPTURE.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫描设备SN码将设备与门店【" + storeName + "】绑定");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            //showBindDeviceDialog();
        } else if (Constants.INTENT_CAPTURE_SITE_DEVICE.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("关联收银点");
            mTvScanNotice.setText("扫描已绑定门店设备二维码\n进行关联至“" + mSiteName + "”收银点");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            //showBindDeviceDialog();
        } else if (Constants.INTENT_CAPTURE_SITE_QRCODE.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("关联收银点");
            mTvScanNotice.setText("扫描已绑定门店收款码\n进行关联至“" + mSiteName + "”收银点");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            //showBindDeviceDialog();
        } else if (Constants.INTENT_NAME_BIND.equals(intentName)) {
            tv_title.setText("绑定收款码");
            tv_money.setVisibility(View.GONE);
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫码可对【" + storeName + "】进行绑定");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
        } else if (Constants.INTENT_NAME_SCAN_BILL.equals(intentName) || Constants.INTENT_NAME_SCAN_PLEDGE.equals(intentName)) {
            tv_money.setVisibility(View.GONE);
            mTvScanNotice.setText("将客户第三方单号放入框内");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
        } else if (Constants.INTENT_VIP_COUNT_VERIFICATION.equals(intentName)) {
            tv_money.setVisibility(View.GONE);
            mTvScanNotice.setText("扫描顾客会员卡二维码");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
        } else if (Constants.INTENT_NAME_POS_CAPTURE.equals(intentName)) {
            ll_change_qrcode.setVisibility(View.GONE);
        } else if (Constants.INTENT_CAPTURE_PLEDGE.equals(intentName)) {
            mTvScanNotice.setText("扫描顾客微信付款码，即可收取押金");
            ll_change_qrcode.setVisibility(View.GONE);
        } else if (Constants.INTENT_NAME_SCAN_LOGIN.equals(intentName)) {
            mTvScanNotice.setText(getString(R.string.tv_capture_login_notice));
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            mLlBottom.setVisibility(View.VISIBLE);
            mRbLogin.setChecked(true);
            mDotLogin.setVisibility(View.VISIBLE);
            mDotRefund.setVisibility(View.INVISIBLE);
            mDotVerify.setVisibility(View.INVISIBLE);
            tv_input_sn_code.setText(getResources().getString(R.string.scan_code_to_login_tip));
            tv_input_sn_code.setEnabled(false);
//            tvCaptureScanLogin.setVisibility(View.GONE);
//            tvScanLoginTitle.setVisibility(View.GONE);
        } else if (Constants.INTENT_NAME_SCAN_REFUND.equals(intentName)) {
            mTvScanNotice.setText(getString(R.string.tv_capture_login_notice));
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            mLlBottom.setVisibility(View.VISIBLE);
            mRbRefund.setChecked(true);
            mDotLogin.setVisibility(View.INVISIBLE);
            mDotRefund.setVisibility(View.VISIBLE);
            mDotVerify.setVisibility(View.INVISIBLE);
            tv_input_sn_code.setText("手动输入订单号");
            tv_input_sn_code.setEnabled(true);
        } else if (Constants.INTENT_NAME_SCAN_VERIFY.equals(intentName)) {
            mTvScanNotice.setText(getString(R.string.tv_capture_login_notice));
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            mLlBottom.setVisibility(View.VISIBLE);
            mRbVerify.setChecked(true);
            mDotLogin.setVisibility(View.INVISIBLE);
            mDotRefund.setVisibility(View.INVISIBLE);
            mDotVerify.setVisibility(View.VISIBLE);
            tv_input_sn_code.setText("手动输入券号");
            tv_input_sn_code.setEnabled(true);
        } else if (!TextUtils.isEmpty(mPayHbfq)) {
            mTvScanNotice.setText(getString(R.string.tv_capture_alipay_notice));
        } else if (Constants.INTENT_NAME_CLOUD_PRINT.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            if (TextUtils.isEmpty(mSiteId)) {
                mTvScanNotice.setText("扫描设备SN码将设备与门店【" + storeName + "】绑定");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定\n并关联“" + mSiteName + "”收银点");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            tv_input_sn_code.setText("SN码在设备底部或侧面");
        } else if (Constants.INTENT_VANKE_VIP.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            mTvScanNotice.setText("请将顾客会员码放入框内扫一扫");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            tv_input_sn_code.setText("手动输入顾客会员码");
        } else if (Constants.INTENT_VANKE_COUPON.equals(intentName) || Constants.INTENT_ADD_VANKE_COUPON.equals(intentName)) {
            tv_money.setVisibility(View.INVISIBLE);
            mTvScanNotice.setText("请将优惠券码放入框内扫一扫");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.VISIBLE);
            tv_input_sn_code.setText("手动输入优惠券编码");
        } else if (Constants.INTENT_H5_SCAN.equals(intentName)) {
            tv_money.setVisibility(View.GONE);
            if (TextUtils.isEmpty(contentText)) {
                mTvScanNotice.setText("");
            } else {
                mTvScanNotice.setText(contentText);
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
        } else if (Constants.INTENT_NAME_BIND_UKEY_DEVICE.equals(intentName)) {
            //绑定ukey盒子设备
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定设备");
            mTvScanNotice.setText("请扫设备上的二维码");
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.GONE);
        } else if (Constants.INTENT_NAME_BIND_UKEY_BOARDCODE.equals(intentName)) {
            //绑定ukey牌码
            tv_money.setVisibility(View.INVISIBLE);
            tv_title.setText("绑定牌码");
            if (TextUtils.isEmpty(storeName)) {
                mTvScanNotice.setText("请扫码牌上的二维码");
            } else {
                mTvScanNotice.setText("扫码与门店“" + storeName + "”绑定");
            }
            rlInputPayCode.setVisibility(View.GONE);
            ll_change_qrcode.setVisibility(View.GONE);
            tv_input_sn_code.setVisibility(View.GONE);
        } else {
            if (AppHelper.getApkType() == 1 || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                ll_change_qrcode.setVisibility(View.INVISIBLE);
            } else {
                ll_change_qrcode.setVisibility(View.VISIBLE);
            }
        }
        if (Constants.INTENT_VIP_COUNT_VERIFICATION.equals(intentName)) {
            mTvScanNotice.setTextSize(14);
        } else {
            mTvScanNotice.setTextSize(12);
        }
        if (Constants.INTENT_VANKE_COUPON.equals(intentName) && !TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_VIP_NUMBER))) {
            getUserInfo(getIntent().getStringExtra(Constants.INTENT_VIP_NUMBER));
        }
        if (Constants.INTENT_NAME_SCAN_VERIFY.equals(intentName)) {
            tv_title.setVisibility(View.GONE);
            mTvTitleStore.setVisibility(View.VISIBLE);
        } else {
            tv_title.setVisibility(View.VISIBLE);
            mTvTitleStore.setVisibility(View.GONE);
        }
        mTvTitleStore.setText(StringUtils.ellipsisString(MyApplication.getDefaultStoreName(), 20));
    }

    private void showBindDeviceDialog() {
        String content = "";
        if (Constants.INTENT_NAME_DYNAMIC_CAPTURE.equals(intentName)) {
            content = getString(R.string.dialog_bind_dynamic_content);
        } else if (Constants.INTENT_NAME_CLOUD_CAPTURE.equals(intentName)) {
            content = getString(R.string.dialog_bind_cloud_content);
        } else {
            content = getString(R.string.dialog_bind_device_content);
        }
        showCommonNoticeDialog(this, content, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                restartCamera();
                showSNCodeDialog(CaptureActivity.this);
            }
        });
        closeCamera();
    }

    private void showPayInputDialog() {
        EditCommonDialog dialog = new EditCommonDialog(this, getString(R.string.tx_pay_code)
                , getString(R.string.tv_pay_code_input), false, new EditCommonDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String edit) {
                if (Constants.INTENT_CAPTURE_PLEDGE.equals(intentName)) {
                    pledgePay(edit);
                } else {
                    pay(edit);
                }
            }
        }, new EditCommonDialog.HandleBtnCancel() {
            @Override
            public void handleCancelBtn() {
                restartCamera();
            }
        });
        dialog.show();
        closeCamera();
    }

    private void showOrderInputDialog() {
        EditCommonDialog dialog = new EditCommonDialog(this, getString(R.string.tx_order_code)
                , getString(R.string.tv_order_input), false, new EditCommonDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String edit) {
                queryOrder(edit);
            }
        }, new EditCommonDialog.HandleBtnCancel() {
            @Override
            public void handleCancelBtn() {
                restartCamera();
            }
        });
        dialog.show();
        closeCamera();
    }

    private void showCouponInputDialog() {
        EditCommonDialog dialog = new EditCommonDialog(this, getString(R.string.tx_coupon_no)
                , getString(R.string.tx_coupon_no), false, new EditCommonDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String edit) {
                queryCoupon(edit);
            }
        }, new EditCommonDialog.HandleBtnCancel() {
            @Override
            public void handleCancelBtn() {
                restartCamera();
            }
        });
        dialog.show();
        closeCamera();
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onResume() {
        super.onResume();
        surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
//        characterSet = null;


        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
    }

    void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    showDialog(getString(R.string.permission_set_content_capture));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(CaptureActivity.this);
                startActivity(intent);
            }
        });
        dialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                CaptureActivity.this.finish();
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        initCamera(surfaceHolder, false);
        // 恢复活动监控器
        //        inactivityTimer.onResume();
    }

    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            return;
        } catch (RuntimeException e) {
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = null;
            try {
                file = getResources().openRawResourceFd(R.raw.beep);
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            } finally {
                if (file != null) {
                    try {
                        file.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private final MediaPlayer.OnCompletionListener beepListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    private static final long VIBRATE_DURATION = 200L;

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    private void bindReceiveSN(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            if (!TextUtils.isEmpty(mSiteId))
                map.put("cashPointId", mSiteId);
            String storeId = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_ID);
            if (!TextUtils.isEmpty(storeId)) {
                map.put("storeMerchantId", storeId);
            }
            loadDialog(CaptureActivity.this, UIUtils.getString(R.string.bind_code_ing));
            ServerClient.newInstance(CaptureActivity.this).deviceBind(CaptureActivity.this, Constants.TAG_DEVICE_BIND, map);
        }
    }

    private void bindCloudDevice(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            if (!TextUtils.isEmpty(mSiteId))
                map.put("cashPointId", mSiteId);
            String storeId = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_ID);
            if (!TextUtils.isEmpty(storeId)) {
                map.put("storeMerchantId", storeId);
            }
            map.put("operateType", 1);
            map.put("merchatId", MyApplication.getMechantId());
            if (Constants.INTENT_NAME_CLOUD_PRINT.equals(intentName)) {
                map.put("purposeCode", 5);
            }
            loadDialog(CaptureActivity.this, UIUtils.getString(R.string.bind_code_ing));
            ServerClient.newInstance(CaptureActivity.this).cloudBind(CaptureActivity.this, Constants.TAG_CLOUD_DEVICE_BIND, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    /**
     * 扫码结果
     * 该方法在CaptureActivityHandler里被调用
     */
    public void submitData(final String code, boolean vibration) {
        if (Constants.INTENT_NAME_DEVICE_CAPTURE.equals(intentName)) {
            bindReceiveSN(code);
        } else if (Constants.INTENT_NAME_FACE_CAPTURE.equals(intentName)) {
            bindReceiveSN(code);
        } else if (Constants.INTENT_NAME_CLOUD_CAPTURE.equals(intentName)) {//云播报
            bindCloudDevice(code);
        } else if (Constants.INTENT_NAME_CLOUD_PRINT.equals(intentName)) {//云打印
            bindCloudDevice(code);//由后端处理二维码格式问题
        } else if (Constants.INTENT_NAME_DYNAMIC_CAPTURE.equals(intentName)) {
            bindCloudDevice(code);
        } else if (Constants.INTENT_NAME_SCAN_REFUND.equals(intentName)) {//扫码退款
            queryOrder(code);
        } else if (Constants.INTENT_NAME_SCAN_BILL.equals(intentName)) {//扫码查单（收款单/退款单）
            String orderNo;
            if (code.contains("=")) {
                int indexOf = code.lastIndexOf("=");
                orderNo = code.substring(indexOf + 1);
            } else {
                orderNo = code;
            }
            queryDetail(orderNo);
        } else if (Constants.INTENT_NAME_SCAN_PLEDGE.equals(intentName)) {
            if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                getDialog(ToastHelper.toStr(R.string.network_exception));
            } else {
                DialogUtil.safeCloseDialog(mLoadDialog);
                Map<String, Object> map = new HashMap<>();
                String transactionId = "";
                if (code.contains("=")) {
                    int indexOf = code.lastIndexOf("=");
                    transactionId = code.substring(indexOf + 1);
                } else {
                    transactionId = code;
                }
                LogUtil.d("authNo==" + transactionId);
                map.put("authNo", transactionId);
                ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_SCAN_BILL, map);
            }
        } else if (Constants.INTENT_NAME_BIND.equals(intentName)) {
            String[] split = code.split("&");//防止多参数链接
            String qrcodeId = "";
            if (split != null && split.length > 0) {
                for (int i = 0; i < split.length; i++) {
                    if (split[i].contains("qrcode=")) {
                        String[] mchIds = split[i].split("qrcode=");
                        for (int i1 = 0; i1 < mchIds.length; i1++) {
                            LogUtil.d("qrcode", mchIds[i1].toString() + i + "--" + i1);
                            qrcodeId = mchIds[1];
                        }
                    }
                }
            }
            if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                getDialog(ToastHelper.toStr(R.string.network_exception));
                return;
            } else {
                Map<String, Object> map = new HashMap();
                if (TextUtils.isEmpty(qrcodeId)) {
//                    getDialog(UIUtils.getString(R.string.dialog_invalid_url));
                    map.put("qrurl", code);
                } else {
                    map.put("qrcode", qrcodeId);
                }
                ServerClient.newInstance(CaptureActivity.this).checkQrcode(CaptureActivity.this, Constants.TAG_CHECK_QRCODE, map);
            }

        } else if (Constants.INTENT_VIP_COUNT_VERIFICATION.equals(intentName)) {
            Intent intent = new Intent(CaptureActivity.this, VipCountCheckActivity.class);
            intent.putExtra(Constants.INTENT_VIP_COUNT_CODE, code);
            startActivity(intent);
        } else if (Constants.INTENT_CAPTURE_PLEDGE.equals(intentName)) {
            pledgePay(code);
        } else if (Constants.INTENT_NAME_SCAN_LOGIN.equals(intentName)) {
            loginCodeStatus(code, 1);
        } else if (Constants.INTENT_NAME_SCAN_VERIFY.equals(intentName)) {
            queryCoupon(code);
        } else if (Constants.INTENT_H5_SCAN.equals(intentName)) {
            Intent intent = new Intent();
            Bundle mBundle = new Bundle();
            mBundle.putSerializable(Constants.RESULT_H5_SCAN, code);   //传递一个user对象列表
            intent.putExtras(mBundle);
            setResult(RESULT_OK, intent);
            finish();
        } else if (Constants.INTENT_CAPTURE_SITE_DEVICE.equals(intentName)) {
            linkCode = code;
            captureSiteLink(code, getIntent().getStringExtra(Constants.INTENT_SITE_TYPE), false);
        } else if (Constants.INTENT_CAPTURE_SITE_QRCODE.equals(intentName)) {
            String[] split = code.split("&");//防止多参数链接
            String qrcodeId = "";
            if (split != null && split.length > 0) {
                for (int i = 0; i < split.length; i++) {
                    if (split[i].contains("qrcode=")) {
                        String[] mchIds = split[i].split("qrcode=");
                        for (int i1 = 0; i1 < mchIds.length; i1++) {
                            LogUtil.d("qrcode", mchIds[i1].toString() + i + "--" + i1);
                            qrcodeId = mchIds[1];
                        }
                    }
                }
            }
            if (TextUtils.isEmpty(qrcodeId)) {
                qrcodeId = code;
            }
            linkCode = qrcodeId;
            captureSiteLink(qrcodeId, getIntent().getStringExtra(Constants.INTENT_SITE_TYPE), false);
        } else if (Constants.INTENT_VANKE_VIP.equals(intentName)) {
            /*getCanUseByMallCardNo(code);
            mVipNo = code;*/
            getUserInfo(code);
        } else if (Constants.INTENT_VANKE_COUPON.equals(intentName) || Constants.INTENT_ADD_VANKE_COUPON.equals(intentName)) {
            addCanUse(code);
        } else if ("INTENT_BIND_DEVICE".equals(intentName)) {
            deviceInfo(code);
        } else if (Constants.INTENT_NAME_BIND_UKEY_DEVICE.equals(intentName)) {
            //去绑定ukey盒子设备
            bindTicketDevice(code);
        } else if (Constants.INTENT_NAME_BIND_UKEY_BOARDCODE.equals(intentName)) {
            //绑定电子发票的码牌
            //"https://hpay.hstypay.com/app/crestv/qrcode?qrcode=30c0c6644c484c30b8ff6fe68838a16d&time=1458254";
            String[] split = code.split("&");//防止多参数链接
            String qrcodeId = "";
            if (split != null && split.length > 0) {
                for (int i = 0; i < split.length; i++) {
                    if (split[i].contains("qrcode=")) {
                        String[] mchIds = split[i].split("qrcode=");
                        for (int i1 = 0; i1 < mchIds.length; i1++) {
                            LogUtil.d("qrcode", mchIds[i1].toString() + i + "--" + i1);
                            qrcodeId = mchIds[1];
                        }
                    }
                }
            }
            if (TextUtils.isEmpty(qrcodeId)) {
                qrcodeId = code;
            }
            bindTicketBoardCode(qrcodeId);
        } else {
            if (!StringUtils.isEmptyOrNull(code)) {
                pay(code);
            }
        }
    }

    //绑定电子发票的码牌
    private void bindTicketBoardCode(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            //绑定发票牌码需要关联门店号
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("qrcode", code);
            map.put("storeId", mStore_id);
            //map.put("qrcode", "4ea2839ee32b455f81a0e2ef4adfc9a9");
            //map.put("storeId", "1030001360");
            ServerClient.newInstance(MyApplication.getContext()).boardCodeBindEleTicket(MyApplication.getContext(), Constants.TAG_BOARDCODE_BIND_ELETICKET, map);
        }
    }

    //绑定电子发票的终端设备
    private void bindTicketDevice(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            //绑定发票设备不需要关联门店号
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            //map.put("url", "http://webapp-test.crestv.com/qrscan/qrentry/8KqoxujA4s52U_74qSq1mZ3a8X84_dSEGCNqETxUh90?t=3");
            map.put("url", code);
            ServerClient.newInstance(MyApplication.getContext()).deviceBindEleTicket(MyApplication.getContext(), Constants.TAG_DEVICE_BIND_ELETICKET, map);
        }
    }

    private void deviceInfo(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            ToastUtil.showToastShort(getString(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServerClient.newInstance(CaptureActivity.this).getDeviceInfo(CaptureActivity.this, "TAG_DEVICE_INFO", map);
        }
    }

    private void addCanUse(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("couponCode", code);
            map.put("storeMerchantId", mStore_id);
            map.put("interProviderType", mInterProviderType);
            ServerClient.newInstance(CaptureActivity.this).addCanUse(CaptureActivity.this, Constants.TAG_CAPTURE_COUPON_INFO, map);
        }
    }

    private void getUserInfo(String vipNo) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(CaptureActivity.this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("mallCardNo", vipNo);
            map.put("storeMerchantId", mStore_id);
            ServerClient.newInstance(MyApplication.getContext()).getUserInfo(MyApplication.getContext(), Constants.TAG_CAPTURE_VIP_INFO, map);
        }
    }

    private void getCanUseByMallCardNo(VipInfoData vipInfo) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("mallCardNo", vipInfo.getMallCardNo());
            map.put("bizUid", vipInfo.getBizUid());
            map.put("storeMerchantId", mStore_id);
            map.put("orderAmount", money);
            if (mInterProviderType != null && !"-1".equals(mInterProviderType))
                map.put("interProviderType", mInterProviderType);
            if (mCouponType == 3)
                map.put("couponType", 3);
            ServerClient.newInstance(CaptureActivity.this).getCanUseByMallCardNo(CaptureActivity.this, Constants.TAG_CAPTURE_VIP_COUPON, map);
        }
    }

    private void queryOrder(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            String transactionId = "";
            if (code.contains("=")) {
                int indexOf = code.lastIndexOf("=");
                transactionId = code.substring(indexOf + 1);
            } else {
                transactionId = code;
            }
            LogUtil.d("transactionId==" + transactionId);
            map.put("transactionId", transactionId);
            loadDialog(CaptureActivity.this, UIUtils.getString(R.string.bind_search_ing));
            ServerClient.newInstance(CaptureActivity.this).scanThirdOrder(CaptureActivity.this, Constants.TAG_SCAN_THIRD_ORDER, map);
        }
    }

    private void loginCodeStatus(String code, int status) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("status", status);
            map.put("qrcodeId", code);
            ServerClient.newInstance(MyApplication.getContext()).loginCodeStatus(MyApplication.getContext(), Constants.TAG_SCAN_LOGIN_STATUS, map);
        }
    }

    private void queryCoupon(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            mCouponId = code;
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("shopId", mStoreId);
            map.put("couponId", code);
            ServerClient.newInstance(MyApplication.getContext()).queryCoupon(MyApplication.getContext(), Constants.TAG_QUERY_COUPON, map);
        }
    }

    private void captureSiteLink(String code, String deviceClass, boolean changeBindFlag) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", mStore_id);
            map.put("sn", code);
            map.put("cashPointId", mSiteId);
            map.put("deviceClass", deviceClass);
            if (changeBindFlag)
                map.put("changeBindFlag", "1");
            ServerClient.newInstance(MyApplication.getContext()).captureSiteLink(MyApplication.getContext(), Constants.TAG_CAPTURE_LINK_DEVICE, map);
        }
    }

    private void pledgePay(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            loadDialog(CaptureActivity.this, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("storeMerchantId", mStore_id);
            map.put("money", money);
            map.put("authCode", code);
            if (!TextUtils.isEmpty(mPayRemark)) {
                map.put("attach", mPayRemark);
            }
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            if (!TextUtils.isEmpty(StringUtils.getDeviceInfo(AppHelper.getSN()))) {
                map.put("opDeviceId", StringUtils.getDeviceInfo(AppHelper.getSN()));
            }
            /*if (AppHelper.getAppType() == 1) {
                map.put("deviceInfo", Constants.PAY_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("deviceInfo", Constants.PAY_CLIENT_APP);
            }*/
           /* if (!TextUtils.isEmpty(mPayHbfq)) {
                map.put("hbFqNum", mPayHbfq);
            }
            map.put("freeFee", mIsRateFree);*/
            ServerClient.newInstance(CaptureActivity.this).pledgePay(CaptureActivity.this, Constants.TAG_PLEDGE_PAY, map);
        }
    }

    private void pay(String code) {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            loadDialog(CaptureActivity.this, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("storeMerchantId", mStore_id);
            map.put("money", money);
            map.put("authCode", code);
            if (/*Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    && !TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO)) || */mVipInfo != null) {
                String outTradeNo = getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO);
                BizDetailBean bizDetailBean = new BizDetailBean();
                bizDetailBean.setIs_biz(1);
                bizDetailBean.setBiz_type(4);
                if (mVipInfo != null) {
                    bizDetailBean.setCard_no(mVipInfo.getMallCardNo());
                    bizDetailBean.setUid(mVipInfo.getBizUid());
                    bizDetailBean.setMobile(mVipInfo.getMobile());
                    bizDetailBean.setInterProviderType(mVipInfo.getInterProviderType());
                }
                bizDetailBean.setBiz_fee(getIntent().getLongExtra(Constants.INTENT_TOTAL_COUPON_MONEY, 0));
                Gson gson = new Gson();
                String bizDetial = gson.toJson(bizDetailBean);
                map.put("bizDetail", bizDetial);
                if (!TextUtils.isEmpty(outTradeNo)) {
                    map.put("outTradeNo", outTradeNo);
                }
            }
            if (!TextUtils.isEmpty(mPayRemark)) {
                map.put("attach", mPayRemark);
            }
            if (!TextUtils.isEmpty(mPayHbfq)) {
                map.put("hbFqNum", mPayHbfq);
            }
            map.put("freeFee", mIsRateFree);
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            /*if (AppHelper.getAppType() == 1) {
                map.put("deviceInfo", Constants.PAY_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("deviceInfo", Constants.PAY_CLIENT_APP);
            }*/
            /*String opDeviceId = Build.BRAND + " " + Build.MODEL + "_An" + Build.VERSION.RELEASE + "_" + AppHelper.getVerName(this);
            if (opDeviceId.length() > 32) {
                opDeviceId = Build.BRAND + "_AN" + Build.VERSION.RELEASE + "_" + AppHelper.getVerName(this);
            }
            if (opDeviceId.length() <= 32) {
                String firstCharUp = ("" + opDeviceId.trim().charAt(0)).toUpperCase();
                String opDeviceIdUp = opDeviceId.replaceFirst("" + opDeviceId.charAt(0), firstCharUp);
                map.put("opDeviceId", opDeviceIdUp.trim());
            }*/
            if (!TextUtils.isEmpty(StringUtils.getDeviceInfo(AppHelper.getSN()))) {
                map.put("opDeviceId", StringUtils.getDeviceInfo(AppHelper.getSN()));
            }
            ServerClient.newInstance(CaptureActivity.this).pay(CaptureActivity.this, Constants.PAY_TAG, map);
        }
    }

    //Eventbus接收数据，支付返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CAPTURE_COUPON_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            playBeepSoundAndVibrate();
            CouponInfoBean msg = (CouponInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    CouponInfoData data = msg.getData();
                    if (data != null) {
                        if (Constants.INTENT_ADD_VANKE_COUPON.equals(intentName)) {
                            Intent intent = new Intent();
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable(Constants.RESULT_VANKE_COUPON, data);   //传递一个user对象列表
                            intent.putExtras(mBundle);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else if (Constants.INTENT_VANKE_COUPON.equals(intentName)) {
                            boolean isExist = false;
                            for (int i = 0; i < mCouponList.size(); i++) {
                                if (mCouponList.get(i).getCouponCode().equals(data.getCouponCode())) {
                                    isExist = true;
                                    break;
                                }
                            }
                            if (!isExist) {
                                mCouponList.add(0, data);
                            }
                            toOrderCouponInfo();
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CAPTURE_VIP_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            VipInfoBean msg = (VipInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mVipInfo = msg.getData();
                    if (mVipInfo != null) {
                        if ("0".equals(mInterProviderType) || "2".equals(mInterProviderType)) {//商场券
                            getCanUseByMallCardNo(mVipInfo);
                        } else {
                            if (!Constants.INTENT_VANKE_COUPON.equals(intentName))
                                toOrderCouponInfo();
                        }
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.error_vip_info));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CAPTURE_VIP_COUPON)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            playBeepSoundAndVibrate();
            CouponListBean msg = (CouponListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<CouponInfoData> couponList = msg.getData();
                    if (couponList != null && couponList.size() > 0) {
                        mCouponList.addAll(couponList);
                    }
                    if (!Constants.INTENT_VANKE_COUPON.equals(intentName))
                        toOrderCouponInfo();
                    break;
            }
        }
        if (event.getTag().equals(Constants.PAY_TAG)) {
            dismissLoading();
            playBeepSoundAndVibrate();
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.PAY_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.PAY_TRUE:
                    if (msg.getData() != null) {
                        mData = msg.getData();
                        mStateCode = msg.getData().getStateCode();
                        mOutTradeNo = mData.getOutTradeNo();
                        mOrderNo = mData.getOrderNo();
                        if (msg.getData().isNeedQuery()) {
                            //去查询订单
                            checkOrderState();
                        } else {
                            getOrderDetail(mOrderNo);
                            /*Intent intent = new Intent();
                            intent.putExtra("capture", mData);
                            intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CAPTURE_PAYRESULT);
                            intent.setClass(CaptureActivity.this, PayResultActivity.class);
                            startActivity(intent);
                            finish();*/
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_PAY)) {
            dismissLoading();
            playBeepSoundAndVibrate();
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mPledgeData = msg.getData();
                        mOrderNo = mPledgeData.getAuthNo();
                        if (msg.getData().isNeedQuery()) {
                            //去查询订单
                            checkOrderState();
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, mPledgeData);
                            intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CAPTURE_PAYRESULT);
                            intent.setClass(CaptureActivity.this, PledgeResultActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_SCAN_BILL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        Intent intent = new Intent(CaptureActivity.this, PledgeDetailActivity.class);
                        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, msg.getData().get(0));
                        startActivity(intent);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_SCAN_LOGIN_STATUS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ScanLoginBean msg = (ScanLoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        Intent intent = new Intent(this, ScanLoginActivity.class);
                        intent.putExtra(Constants.INTENT_CAPTURE_SCAN_LOGIN, msg.getData());
                        startActivity(intent);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CAPTURE_LINK_DEVICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (Constants.ERROR_CASH_POINT_DEVICE_UNBIND.equals(msg.getError().getCode())) {
                                showLinkSiteSelect(msg.getError().getMessage(), "取消", "去绑定", new SelectDialog.OnClickOkListener() {
                                    @Override
                                    public void clickOk() {
                                        if ("1".equals(getIntent().getStringExtra(Constants.INTENT_SITE_TYPE))) {
                                            bindReceiveSN(linkCode);
                                        } else if ("2".equals(getIntent().getStringExtra(Constants.INTENT_SITE_TYPE))) {
                                            if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                                                getDialog(ToastHelper.toStr(R.string.network_exception));
                                            } else {
                                                Map<String, Object> map = new HashMap<>();
                                                map.put("storeMerchantId", mStore_id);
                                                map.put("qrcode", linkCode);
                                                if (!TextUtils.isEmpty(mSiteId))
                                                    map.put("cashPointId", mSiteId);
                                                if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_USER_ID)))
                                                    map.put("userId", getIntent().getStringExtra(Constants.INTENT_USER_ID));
                                                loadDialog(CaptureActivity.this, UIUtils.getString(R.string.bind_code_ing));
                                                ServerClient.newInstance(CaptureActivity.this).bindCode(CaptureActivity.this, Constants.TAG_BIND_CODE, map);
                                            }
                                        }
                                    }
                                });
                            } else if (Constants.ERROR_CASH_POINT_DEVICE_LINKED.equals(msg.getError().getCode())) {
                                showLinkSiteSelect(msg.getError().getMessage(), "取消", "确定", new SelectDialog.OnClickOkListener() {
                                    @Override
                                    public void clickOk() {
                                        captureSiteLink(linkCode, getIntent().getStringExtra(Constants.INTENT_SITE_TYPE), true);
                                    }
                                });
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    showCommonNoticeDialog(CaptureActivity.this, getString(R.string.dialog_link_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            setResult(RESULT_OK);
                            CaptureActivity.this.finish();
                        }
                    });
                    break;
            }
        } else if (event.getTag().equals("TAG_DEVICE_INFO")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceInfoBean msg = (DeviceInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                        restartCamera();
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData().getStatus() == 1) {
                            showCommonNoticeDialog(CaptureActivity.this, "此设备已绑定，请勿重复操作", new CommonNoticeDialog.OnClickOkListener() {
                                @Override
                                public void onClickOk() {
                                    restartCamera();
                                }
                            });
                        } else {
                            Intent intent = new Intent(this, DeviceStoreActivity.class);
                            intent.putExtra(Constants.INTENT_NAME, intentName);
                            intent.putExtra("INTENT_DEVICE_INFO", msg.getData());
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        showCommonNoticeDialog(CaptureActivity.this, "找不到此设备", new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                restartCamera();
                            }
                        });
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_QUERY_COUPON)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponHsInfoBean msg = (CouponHsInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            startActivity(new Intent(this, CouponDetailActivity.class)
                                    .putExtra("INTENT_COUPON_DETAIL_ERROR", msg.getError().getMessage()));
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null)
                        msg.getData().setCouponId(mCouponId);
                    startActivity(new Intent(this, CouponDetailActivity.class)
                            .putExtra("INTENT_COUPON_DETAIL", msg.getData())
                            .putExtra(Constants.INTENT_STORE_ID, mStoreId));
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_DEVICE_BIND_ELETICKET) || (event.getTag().equals(Constants.TAG_BOARDCODE_BIND_ELETICKET))) {
            //绑定电子发票设备、绑定牌码
            DialogUtil.safeCloseDialog(mLoadDialog);
            BaseBean<String> msg = (BaseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            showCommonNoticeDialog(CaptureActivity.this, msg.getError().getMessage());
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        Intent intent = new Intent(this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, msg.getData());
                        intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    }

    private void showLinkSiteSelect(String contentText, String cancel, String ensure, SelectDialog.OnClickOkListener onClickOkListener) {
        SelectDialog selectDialog = new SelectDialog(CaptureActivity.this, contentText, cancel, ensure, R.layout.select_common_dialog);
        if (onClickOkListener != null)
            selectDialog.setOnClickOkListener(onClickOkListener);
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                CaptureActivity.this.finish();
            }
        });
        selectDialog.show();
    }

    private void getOrderDetail(String orderNo) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(orderNo)) {
                map.put("orderNo", orderNo);
            }
            ServerClient.newInstance(CaptureActivity.this).getOrderDetail(CaptureActivity.this, Constants.TAG_CAPTURE_ORDER_DETAIL, map);
        } else {
            getDialog(UIUtils.getString(R.string.network_exception));
        }
    }

    private long timeCount = 5;
    private int count = 1;
    private int countFive = 1;
    private String dialogMessage = null;
    private NoticeDialog showDialog;
    private int timeTotal;

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_login:
                intentName = Constants.INTENT_NAME_SCAN_LOGIN;
                tv_input_sn_code.setText("扫一扫登录收银插件");
                tv_input_sn_code.setEnabled(false);
                mDotLogin.setVisibility(View.VISIBLE);
                mDotVerify.setVisibility(View.INVISIBLE);
                mDotRefund.setVisibility(View.INVISIBLE);
                break;
            case R.id.rb_verify:
                intentName = Constants.INTENT_NAME_SCAN_VERIFY;
                tv_input_sn_code.setText("手动输入券号");
                tv_input_sn_code.setEnabled(true);
                mDotLogin.setVisibility(View.INVISIBLE);
                mDotRefund.setVisibility(View.INVISIBLE);
                mDotVerify.setVisibility(View.VISIBLE);
                break;
            case R.id.rb_refund:
                intentName = Constants.INTENT_NAME_SCAN_REFUND;
                tv_input_sn_code.setText("手动输入订单号");
                tv_input_sn_code.setEnabled(true);
                mDotLogin.setVisibility(View.INVISIBLE);
                mDotVerify.setVisibility(View.INVISIBLE);
                mDotRefund.setVisibility(View.VISIBLE);
                break;
        }
        if (Constants.INTENT_NAME_SCAN_VERIFY.equals(intentName)) {
            tv_title.setVisibility(View.GONE);
            mTvTitleStore.setVisibility(View.VISIBLE);
        } else {
            tv_title.setVisibility(View.VISIBLE);
            mTvTitleStore.setVisibility(View.GONE);
        }
    }

    private void toOrderCouponInfo() {
        Intent intent = new Intent(this, OrderCouponInfoActivity.class);
        intent.putExtra(Constants.INTENT_VANKE_COUPON, (Serializable) mCouponList);
        intent.putExtra(Constants.INTENT_VANKE_VIP, mVipInfo);
        intent.putExtra(Constants.INTENT_PAY_MONEY, doubleMoney);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStore_id);
        startActivity(intent);
        finish();
    }

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            LogUtil.d("MyTimerTask计数：" + timeTotal);
            if (++timeTotal > 50) {
                LogUtil.d("MyTimerTask 结束了！！");
                System.gc();
                cancel();
            }
        }
    }

    private void checkOrderState() {
        if (count < 9 && timeTotal <= 50) {
            //toCheckOrderState();
            if (count < 4) {
                if (count == 1) {
                    Timer timer = new Timer();
                    TimerTask timerTask = new MyTimerTask();
                    timer.schedule(timerTask, 1000, 1000);
                }
                timeCount = 5;
                count++;
            } else if (count >= 4) {
                timeCount = 7;
                count++;
                countFive++;
            }
            showDialog = new NoticeDialog(CaptureActivity.this, dialogMessage, "", R.layout.notice_dialog_check_order);
            showDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    cancelTask = true;
                    toCheckOrderState();
                    remove();
                }
            });
            DialogHelper.resize(CaptureActivity.this, showDialog);
            showDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            cancelTask = true;
            toCheckOrderState();
        }
    }

    private void showWindowReverse(String orderNo, String status) {
        dismissLoading();
        mSelectDialog = new SelectCloseDialog(CaptureActivity.this, orderNo, status
                , "", "", R.layout.select_check_order_dialog);
        mSelectDialog.setOnClickCloseListener(new SelectCloseDialog.OnClickCloseListener() {
            @Override
            public void clickClose() {
                remove();
                finish();
            }
        });
        mSelectDialog.setOnClickOkListener(new SelectCloseDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                cancelTask = true;
                toCheckOrderState();
            }
        });
        mSelectDialog.setOnClickCancelListener(new SelectCloseDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                    getDialog(getString(R.string.network_exception));
                    return;
                } else {
                    if (Constants.INTENT_CAPTURE_PLEDGE.equals(intentName)) {
                        pledgeReverse();
                    } else {
                        orderReverse();
                    }
                }
            }
        });
        DialogHelper.resize(CaptureActivity.this, mSelectDialog, 0.9f);
        mSelectDialog.show();
    }

    /**
     * 冲正
     */
    private void orderReverse() {
        showNewLoading(true, getString(R.string.public_order_reverse));
        Map<String, Object> map = new HashMap<>();
        if (mData.getOutTradeNo() != null)
            map.put("outTradeNo", mData.getOutTradeNo());
        map.put("storeMerchantId", mStore_id);
        /*if (AppHelper.getAppType() == 1) {
            map.put("deviceInfo", Constants.PAY_CLIENT_POS);
        } else if (AppHelper.getAppType() == 2) {
            map.put("deviceInfo", Constants.PAY_CLIENT_APP);
        }*/
        map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
        ServerClient.newInstance(CaptureActivity.this).orderReverse(CaptureActivity.this, Constants.ORDER_REVERSE_TAG, map);
    }

    /**
     * 预授权撤销
     */
    private void pledgeReverse() {
        showNewLoading(true, getString(R.string.public_order_backout));
        Map<String, Object> map = new HashMap<>();
        map.put("outAuthNo", mPledgeData.getOutAuthNo());
        map.put("storeMerchantId", mStore_id);
        ServerClient.newInstance(CaptureActivity.this).authReverse(CaptureActivity.this, Constants.TAG_PLEDGE_CAPTURE_REVERSE, map);
    }

    private void toCheckOrderState() {
        if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
            getDialog(getString(R.string.network_exception));
            return;
        } else {
            if (Constants.INTENT_CAPTURE_PLEDGE.equals(intentName)) {
                checkPledgeStateRequest();
            } else {
                checkOrderStateRequest();
            }
        }
    }

    private void checkOrderStateRequest() {
        loadDialog(CaptureActivity.this, getString(R.string.public_order_state));
        Map<String, Object> map = new HashMap<>();
        if (mData.getOutTradeNo() != null) {
            map.put("outTradeNo", mData.getOutTradeNo());
            map.put("storeMerchantId", mStore_id);
            ServerClient.newInstance(CaptureActivity.this).checkOrderState(CaptureActivity.this, Constants.CHECK_ORDER_STATE_TAG, map);
        }
    }

    private void checkPledgeStateRequest() {
        loadDialog(CaptureActivity.this, getString(R.string.public_order_state));
        Map<String, Object> map = new HashMap<>();
        if (mPledgeData.getAuthNo() != null) {
            map.put("authNo", mPledgeData.getAuthNo());
        }
        if (mPledgeData.getOutAuthNo() != null) {
            map.put("outAuthNo", mPledgeData.getOutAuthNo());
        }
        map.put("storeMerchantId", mStore_id);
        ServerClient.newInstance(CaptureActivity.this).queryAuthDetail(CaptureActivity.this, Constants.TAG_PLEDGE_CAPTURE_DETAIL, map);
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && showDialog != null) {
                if ("USER.PAYING".equals(mStateCode) || "USERPAYING".equals(mStateCode)) {
                    showDialog.setMessage(getString(R.string.dialog_order_password) + "\n"
                            + getString(R.string.dialog_order_stuts) + timeCount
                            + getString(R.string.dialog_start) + (count - 1) + getString(R.string.dialog_end));
                } else {
                    showDialog.setMessage(getString(R.string.dialog_order_stuts) + timeCount
                            + getString(R.string.dialog_start) + (count - 1) + getString(R.string.dialog_end));
                }
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (showDialog != null && showDialog.isShowing()) {
                    showDialog.dismiss();
                }
                toCheckOrderState();
            }
        }
    };

    //Eventbus接收数据，查询订单状态返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckOrderState(NoticeEvent event) {
        if (event.getTag().equals(Constants.CHECK_ORDER_STATE_TAG)) {
            dismissLoading();
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (cancelTask) {
                        showWindowReverse(mOrderNo, "未支付");
                    } else {
                        checkOrderState();
                    }
                    //MyToast.showToastShort(getString(R.string.net_error) + Constants.PAY_TAG);
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.CHECK_ORDER_STATE_TAG);
                    break;
                case Constants.CHECK_ORDER_STATE_FALSE:
                    if (cancelTask) {
                        if (msg.getError() != null) {
                            if (msg.getError().getCode() != null) {
                                if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (msg.getError().getMessage() != null) {
                                        getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                    }
                                } else {
                                    if (msg.getError().getMessage() != null) {
                                        showWindowReverse(mOrderNo, msg.getError().getMessage());
                                    } else {
                                        showWindowReverse(mOrderNo, "未支付");
                                    }

                                }
                            } else {
                                showWindowReverse(mOrderNo, "未支付");
                            }
                        } else {
                            showWindowReverse(mOrderNo, "未支付");
                        }
                    } else {
                        checkOrderState();
                    }

                    break;
                case Constants.CHECK_ORDER_STATE_TRUE:
                    if (msg.getData() != null) {
                        LogUtil.i("zhouwei", "支付成功====" + msg);
                        PayBean.DataBean datas = msg.getData();
                        if (datas != null) {
                            mStateCode = datas.getStateCode();
                            String tradeState = datas.getTradeState() + "";
                            //1:未支付，2：支付成功,3:已关闭，4：转入退款，8：已冲正，9：已撤销
                            if (!StringUtils.isEmptyOrNull(tradeState)) {
                                if (tradeState.equals("2")) {
                                    /*Intent intent = new Intent();
                                    intent.putExtra("capture", datas);
                                    intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CAPTURE_PAYRESULT);
                                    intent.setClass(CaptureActivity.this, PayResultActivity.class);
                                    startActivity(intent);*/
                                    getOrderDetail(datas.getOrderNo());
                                    remove();
                                } else if (tradeState.equals("3")) {
                                    //已关闭
                                    remove();
                                    getFinishDialog("订单已关闭");
                                } else if (tradeState.equals("4")) {
                                    //转入退款
                                    remove();
                                    getFinishDialog("转入退款");
                                } /*else if (tradeState.equals("8")) {
                                    //已冲正
                                    remove();
                                    getDialogs("订单已冲正");
                                } */ else if (tradeState.equals("8") || tradeState.equals("9")) {
                                    //已撤销
                                    remove();
                                    getFinishDialog("订单已撤销");
                                } else if (tradeState.equals("1")) {
                                    //订单未支付
                                    if (datas.isNeedQuery()) {
                                        if (cancelTask) {
                                            showWindowReverse(mOrderNo, "未支付");
                                        } else {
                                            checkOrderState();
                                        }
                                    } else {
                                        if (!cancelTask && (count == 2 || count == 8)) {
                                            showWindowReverse(mOrderNo, "未支付");
                                        } else {
                                            getFinishDialog(getString(R.string.dialog_cancel_notice));
                                        }
                                    }
                                } else {
                                    if (datas.isNeedQuery()) {
                                        if (cancelTask) {
                                            showWindowReverse(mOrderNo, "未知状态");
                                        } else {
                                            checkOrderState();
                                        }
                                    } else {
                                        if (!cancelTask && (count == 2 || count == 8)) {
                                            showWindowReverse(mOrderNo, "未知状态");
                                        } else {
                                            getFinishDialog(getString(R.string.dialog_cancel_notice));
                                        }
                                    }
                                }
                            } else {
                                if (datas.isNeedQuery()) {
                                    if (cancelTask) {
                                        showWindowReverse(mOrderNo, "未知状态");
                                    } else {
                                        checkOrderState();
                                    }
                                } else {
                                    if (!cancelTask && (count == 2 || count == 8)) {
                                        showWindowReverse(mOrderNo, "未知状态");
                                    } else {
                                        getFinishDialog(getString(R.string.dialog_cancel_notice));
                                    }
                                }
                            }
                        } else {
                            if (cancelTask) {
                                showWindowReverse(mOrderNo, "未知状态");
                            } else {
                                checkOrderState();
                            }
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_CAPTURE_DETAIL)) {
            dismissLoading();
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (cancelTask) {
                        showWindowReverse(mOrderNo, "未支付");
                    } else {
                        checkOrderState();
                    }
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (cancelTask) {
                        if (msg.getError() != null) {
                            if (msg.getError().getCode() != null) {
                                if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (msg.getError().getMessage() != null) {
                                        getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                    }
                                } else {
                                    if (msg.getError().getMessage() != null) {
                                        showWindowReverse(mOrderNo, msg.getError().getMessage());
                                    } else {
                                        showWindowReverse(mOrderNo, "未支付");
                                    }

                                }
                            } else {
                                showWindowReverse(mOrderNo, "未支付");
                            }
                        } else {
                            showWindowReverse(mOrderNo, "未支付");
                        }
                    } else {
                        checkOrderState();
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        PledgePayBean.DataBean pledgePayBean = msg.getData();
                        if (pledgePayBean != null) {
                            String tradeStatus = pledgePayBean.getTradeStatus();
                            //1:未支付，2：支付成功,3:已关闭，4：转入退款，8：已冲正，9：已撤销
                            if (!StringUtils.isEmptyOrNull(tradeStatus)) {
                                if (tradeStatus.equals("2")) {
                                    Intent intent = new Intent();
                                    intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, pledgePayBean);
                                    intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CAPTURE_PAYRESULT);
                                    intent.setClass(CaptureActivity.this, PledgeResultActivity.class);
                                    startActivity(intent);
                                    CaptureActivity.this.finish();
                                    remove();
                                } else if (tradeStatus.equals("3")) {
                                    //已关闭
                                    remove();
                                    getFinishDialog("订单已关闭");
                                } else if (tradeStatus.equals("8") || tradeStatus.equals("9")) {
                                    //已撤销
                                    remove();
                                    getFinishDialog("订单已撤销");
                                } else if (tradeStatus.equals("1")) {
                                    if (pledgePayBean.isNeedQuery()) {
                                        if (cancelTask) {
                                            showWindowReverse(mOrderNo, "未支付");
                                        } else {
                                            checkOrderState();
                                        }
                                    } else {
                                        if (!cancelTask && (count == 2 || count == 8)) {
                                            showWindowReverse(mOrderNo, "未知状态");
                                        } else {
                                            getFinishDialog(getString(R.string.dialog_cancel_notice));
                                        }
                                    }
                                } else {
                                    if (pledgePayBean.isNeedQuery()) {
                                        if (cancelTask) {
                                            showWindowReverse(mOrderNo, "未知状态");
                                        } else {
                                            checkOrderState();
                                        }
                                    } else {
                                        if (!cancelTask && (count == 2 || count == 8)) {
                                            showWindowReverse(mOrderNo, "未知状态");
                                        } else {
                                            getFinishDialog(getString(R.string.dialog_cancel_notice));
                                        }
                                    }
                                }
                            } else {
                                if (pledgePayBean.isNeedQuery()) {
                                    if (cancelTask) {
                                        showWindowReverse(mOrderNo, "未知状态");
                                    } else {
                                        checkOrderState();
                                    }
                                } else {
                                    if (!cancelTask && (count == 2 || count == 8)) {
                                        showWindowReverse(mOrderNo, "未知状态");
                                    } else {
                                        getFinishDialog(getString(R.string.dialog_cancel_notice));
                                    }
                                }
                            }
                        } else {
                            if (cancelTask) {
                                showWindowReverse(mOrderNo, "未知状态");
                            } else {
                                checkOrderState();
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void remove() {
        if (showDialog != null) {
            showDialog.dismiss();
        }
        if (mSelectDialog != null) {
            mSelectDialog.dismiss();
        }
        mHandler.removeCallbacks(myRunnable);
    }

    //Eventbus接收数据，订单冲正返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderReverse(NoticeEvent event) {
        if (event.getTag().equals(Constants.ORDER_REVERSE_TAG)) {
            Info msg = (Info) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ORDER_REVERSE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ORDER_REVERSE_TRUE:
                    getFinishDialog(getString(R.string.order_cz_success));
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_CAPTURE_REVERSE)) {
            dismissLoading();
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getFinishDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showNoticeDialog();
                    break;
            }
        }
    }

    private void showNoticeDialog() {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(CaptureActivity.this, getString(R.string.order_cz_success), "");
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent(Constants.ACTION_PLEDGE_DATA);
                sendBroadcast(intent);
                startActivity(new Intent(CaptureActivity.this, PledgeActivity.class));
                CaptureActivity.this.finish();
            }
        });
        commonNoticeDialog.show();
    }

    //Eventbus接收数据，绑定二维码
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBindCode(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_BIND_CODE)) {
            BindCodeBean msg = (BindCodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.BIND_CODE_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else if (Constants.ERROR_CODE_UPDATE_QRCODE.equals(msg.getError().getCode())) {
                                showUpdateDialog(CaptureActivity.this);
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(CaptureActivity.this, msg.getError().getMessage(), new CommonNoticeDialog.OnClickOkListener() {
                                        @Override
                                        public void onClickOk() {
                                            CaptureActivity.this.finish();
                                        }
                                    });
                                }
                            }
                        }
                    }
                    break;
                case Constants.BIND_CODE_TRUE:
                    dismissLoading();
                    //绑定成功
                    if (Constants.INTENT_CAPTURE_SITE_QRCODE.equals(intentName)) {
                        getDialogSuccess(getString(R.string.dialog_notice_binding_success));
                    } else {
                        MyToast.showToast(UIUtils.getString(R.string.bind_code_success), Toast.LENGTH_LONG);
                        Intent intent = new Intent(this, StoreCodeListActivity.class);
                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_STATIC_CODE);
                        Bundle bundle = new Bundle();
                        bundle.getString(Constants.INTENT_USER_ID, getIntent().getStringExtra(Constants.INTENT_USER_ID));
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CHECK_QRCODE)) {
            QrcodeBean msg = (QrcodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.CHECK_QRCODE_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.CHECK_QRCODE_TRUE:
                    dismissLoading();
                    //绑定成功
                    if (msg.getData() != null) {
                        switch (msg.getData().getUseStatus()) {
                            case 0://未绑定
                                showBindDialog(this, storeName, msg.getData().getQrcode(), msg.getData().getId());
                                break;
                            case 1://已绑定
                                getDialog("绑定失败,该二维码已被绑定过");
                                break;
                            case 2://已失效
                                getDialog("绑定失败,该二维码已失效");
                                break;
                            case 3://禁用
                                getDialog("绑定失败,该二维码已被禁用");
                                break;
                        }
                    }
                    /*MyToast.showToast(UIUtils.getString(R.string.bind_code_success), Toast.LENGTH_LONG);
                    startActivity(new Intent(this, StoreCodeActivity.class));
                    finish();*/
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_SCAN_THIRD_ORDER)) {
            ThirdOrderBean msg = (ThirdOrderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.SCAN_THIRD_ORDER_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.SCAN_THIRD_ORDER_TRUE:
                    dismissLoading();
                    ThirdOrderBean.DataEntity dataEntity = msg.getData();
                    if (dataEntity != null) {
                        queryDetail(dataEntity.getOrderNo());
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_DEVICE_BIND)) {
            Info msg = (Info) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_notice_binding_success));
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_APPLY_VIP_UPDATE)) {
            Info msg = (Info) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_apply_update_success));
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_BIND)) {
            Info msg = (Info) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_notice_binding_success));
                    break;
            }
        }
    }

    private void queryDetail(String orderNo) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(orderNo)) {
                map.put("orderNo", orderNo);
            }
            ServerClient.newInstance(CaptureActivity.this).getOrderDetail(CaptureActivity.this, Constants.TAG_BILL_FIND_SEARCH_DETAIL, map);
        } else {
            getDialog(UIUtils.getString(R.string.network_exception));
        }
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_BILL_FIND_SEARCH_DETAIL)) {
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    //MyToast.showToastShort(getString(R.string.net_error) + Constants.PAY_TAG);
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_BILL_FIND_SEARCH_DETAIL);
                    break;
                case Constants.ON_EVENT_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                    //ToastHelper.showInfo(CaptureActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    dismissLoading();
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        if (Constants.INTENT_NAME_SCAN_REFUND.equals(intentName)) {
                            String tradeState = data.getTradeState() + "";
                            //1:未支付，2：支付成功,3:已关闭，4：转入退款，8：已冲正，9：已撤销
                            if (!StringUtils.isEmptyOrNull(tradeState)) {
                                if (tradeState.equals("2")) {
                                    if (data.getRefundMoney() > 0 && data.getMdiscount() > 0) {
                                        //使用代金券的订单全额退过款了就不能再退款了（使用微信代金券的订单退款时只支持全额退款）
                                        String weixinMdiscountTip = getResources().getString(R.string.capture_weixin_refund_finish);
                                        getFinishDialog(weixinMdiscountTip);
                                    } else {
                                        intent.putExtra(Constants.INTENT_ORDER_REFUND, data);
                                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_CAPTURE_REFUND);
                                        intent.setClass(CaptureActivity.this, RefundActivity.class);
                                    }

                                } else if (tradeState.equals("3")) {
                                    getFinishDialog("订单已关闭");
                                } else if (tradeState.equals("4")) {
                                    if (data.getRefundMoney() > 0 && data.getMdiscount() > 0) {
                                        //使用代金券的订单全额退过款了就不能再退款了（使用微信代金券的订单退款时只支持全额退款）
                                        String weixinMdiscountTip = getResources().getString(R.string.capture_weixin_refund_finish);
                                        getFinishDialog(weixinMdiscountTip);
                                    } else {
                                        intent.putExtra(Constants.INTENT_ORDER_REFUND, data);
                                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_CAPTURE_REFUND);
                                        intent.setClass(CaptureActivity.this, RefundActivity.class);
                                    }

                                } else if (tradeState.equals("8") || tradeState.equals("9")) {
                                    getFinishDialog("订单已撤销");
                                } else if (tradeState.equals("1")) {
                                    getFinishDialog("订单未支付");
                                } else {
                                    getFinishDialog("订单未知状态");
                                }
                            } else {
                                getFinishDialog("订单未知状态");
                            }
                        } else {
                            intent.putExtra(Constants.INTENT_BILL_DATA, data);
                            if (data.getOrderType() == 1) {
                                intent.putExtra(Constants.INTENT_TRADE_DETAIL, PayBeanUtil.getTradeDetail(data));
                                intent.setClass(CaptureActivity.this, RefundDetailActivity.class);
                            } else {
                                intent.setClass(CaptureActivity.this, PayDetailActivity.class);
                            }
                        }
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CAPTURE_ORDER_DETAIL)) {
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    dismissLoading();
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        intent.putExtra("capture", data);
                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CAPTURE_PAYRESULT);
                        intent.setClass(CaptureActivity.this, PayResultActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ly_back:
                finish();
                break;
            case R.id.ly_open_light:
                if (mNeedFlashLightOpen) {
                    turnOn();
                } else {
                    turnFlashLightOff();
                }
                break;
            case R.id.ll_change_qrcode:
                MtaUtils.mtaId(CaptureActivity.this, "B004");
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStore_id);
                    intent.putExtra(Constants.INTENT_PAY_MONEY, doubleMoney);
                    intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
                    if (!TextUtils.isEmpty(mPayHbfq)) {
                        intent.putExtra(Constants.INTENT_HBFQ, mPayHbfq);
                        intent.putExtra(Constants.INTENT_IS_RATE_FREE, mIsRateFree);
                        intent.putExtra(Constants.INTENT_UNION_USER_FEE, mUnionUserFee);
                        intent.setClass(this, QrCodeActivity.class);
                    } else {
                        intent.putExtra(Constants.INTENT_NAME, "INTENT_CHANGE_QRCODE");
                        intent.setClass(this, StaticCodeImageActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.tv_input_sn_code:
//                showBindDeviceDialog();
                if (Constants.INTENT_NAME_SCAN_REFUND.equals(intentName)) {
                    showOrderInputDialog();
                } else if (Constants.INTENT_NAME_SCAN_VERIFY.equals(intentName)) {
                    showCouponInputDialog();
                } else {
                    showSNCodeDialog(CaptureActivity.this);
                }
                break;
            case R.id.tv_input_pay_code:
                showPayInputDialog();
                break;
            case R.id.tv_title_store:
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
        }
    }

    public void showSNCodeDialog(Activity activity) {
        Dialog dialog = null;
        if (Constants.INTENT_VANKE_VIP.equals(intentName)) {
            dialog = new EditCommonDialog(this, getString(R.string.tx_vip_code)
                    , getString(R.string.hint_capture_vip_code), false, new EditCommonDialog.HandleBtn() {
                @Override
                public void handleOkBtn(String edit) {
                    restartCamera();
                    getUserInfo(edit);
                }
            }, new EditCommonDialog.HandleBtnCancel() {
                @Override
                public void handleCancelBtn() {
                    restartCamera();
                }
            });
        } else if (Constants.INTENT_VANKE_COUPON.equals(intentName) || Constants.INTENT_ADD_VANKE_COUPON.equals(intentName)) {
            dialog = new EditCommonDialog(this, getString(R.string.tx_coupon_code)
                    , getString(R.string.hint_capture_coupon_code), false, new EditCommonDialog.HandleBtn() {
                @Override
                public void handleOkBtn(String edit) {
                    restartCamera();
                    addCanUse(edit);
                }
            }, new EditCommonDialog.HandleBtnCancel() {
                @Override
                public void handleCancelBtn() {
                    restartCamera();
                }
            });
        } else if (Constants.INTENT_NAME_CLOUD_PRINT.equals(intentName)) {
            dialog = new EditCommonDialog(this, getString(R.string.tx_sn)
                    , getString(R.string.hint_sn_code), false, new EditCommonDialog.HandleBtn() {
                @Override
                public void handleOkBtn(String edit) {
                    restartCamera();
                    if (!TextUtils.isEmpty(edit)) {
                        bindCloudDevice(edit);
                    }
                }
            }, new EditCommonDialog.HandleBtnCancel() {
                @Override
                public void handleCancelBtn() {
                    restartCamera();
                }
            });
        } else if ("INTENT_BIND_DEVICE".equals(intentName)) {
            dialog = new EditCommonDialog(this, getString(R.string.tx_sn)
                    , getString(R.string.hint_sn_code), false, edit -> {
                restartCamera();
                if (!TextUtils.isEmpty(edit)) {
                    deviceInfo(edit);
                }
            }, () -> restartCamera());
        } else {
            dialog = new EditCommonDialog(this, getString(R.string.tx_sn)
                    , getString(R.string.hint_sn_code), false, edit -> {
                restartCamera();
                if (!TextUtils.isEmpty(edit)) {
                    if (Constants.INTENT_CAPTURE_SITE_DEVICE.equals(intentName)) {
                        linkCode = edit;
                        captureSiteLink(edit, getIntent().getStringExtra(Constants.INTENT_SITE_TYPE), false);
                    } else if (Constants.INTENT_NAME_CLOUD_CAPTURE.equals(intentName) || Constants.INTENT_NAME_DYNAMIC_CAPTURE.equals(intentName)) {
                        bindCloudDevice(edit);
                    } else {
                        bindReceiveSN(edit);
                    }
                }
            }, () -> restartCamera());
        }
        DialogHelper.resize(CaptureActivity.this, dialog);
        dialog.show();
        closeCamera();
    }

    //开灯
    private void turnOn() {
        mNeedFlashLightOpen = false;
        iv_light.setImageResource(R.mipmap.icon_general_scan_light_off);
        CameraManager.get().setFlashLight(true);
    }


    /**
     * 关灯
     * <功能详细描述>
     * [类、类#方法、类#成员]
     */
    private void turnFlashLightOff() {
        mNeedFlashLightOpen = true;
        iv_light.setImageResource(R.mipmap.icon_general_scan_light_on);
        CameraManager.get().setFlashLight(false);
    }

    public void getDialog(String title) {
        showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                restartCamera();
            }
        });
    }


    public void getFinishDialog(String title) {
        showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                CaptureActivity.this.finish();
            }
        });
    }

    public void getDialogSuccess(String title) {
        if (Constants.INTENT_NAME_DEVICE_CAPTURE.equals(intentName)) {
            showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    startActivity(new Intent(CaptureActivity.this, ReceiveDeviceListActivity.class));
                    CaptureActivity.this.finish();
                }
            });
        } else if (Constants.INTENT_NAME_FACE_CAPTURE.equals(intentName)) {
            showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    startActivity(new Intent(CaptureActivity.this, FaceDeviceListActivity.class));
                    CaptureActivity.this.finish();
                }
            });
        } else if (Constants.INTENT_NAME_CLOUD_CAPTURE.equals(intentName)) {
            showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    startActivity(new Intent(CaptureActivity.this, CloudDeviceActivity.class));
                    CaptureActivity.this.finish();
                }
            });
        } else if (Constants.INTENT_NAME_DYNAMIC_CAPTURE.equals(intentName)) {
            showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    startActivity(new Intent(CaptureActivity.this, DynamicCodeListActivity.class));
                    CaptureActivity.this.finish();
                }
            });
        } else if (Constants.INTENT_NAME_CLOUD_PRINT.equals(intentName)) {
            showCommonNoticeDialog(CaptureActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    startActivity(new Intent(CaptureActivity.this, CloudPrintActivity.class));
                    CaptureActivity.this.finish();
                }
            });
        } else if (Constants.INTENT_CAPTURE_SITE_DEVICE.equals(intentName) || Constants.INTENT_CAPTURE_SITE_QRCODE.equals(intentName)) {
            setResult(RESULT_OK);
            CaptureActivity.this.finish();
        }
    }

    public void showBindDialog(Activity context, String storeName, final String qrcodeId, String id) {
        SelectDialog selectDialog = new SelectDialog(context, getString(R.string.show_bind_code_title),
                getString(R.string.bind_code_content1) + storeName + "\n" + getString(R.string.bind_code_content2) + id
                , getString(R.string.btnCancel), getString(R.string.btn_bind_code_ensure), R.layout.select_common_dialog);
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                String storeId = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_ID);
                if (!StringUtils.isEmptyOrNull(qrcodeId)) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("storeMerchantId", storeId);
                    map.put("qrcode", qrcodeId);
                    if (!TextUtils.isEmpty(mSiteId))
                        map.put("cashPointId", mSiteId);
                    if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_USER_ID)))
                        map.put("userId", getIntent().getStringExtra(Constants.INTENT_USER_ID));
                    if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                        getDialog(ToastHelper.toStr(R.string.network_exception));
                        return;
                    } else {
                        loadDialog(CaptureActivity.this, UIUtils.getString(R.string.bind_code_ing));
                        ServerClient.newInstance(CaptureActivity.this).bindCode(CaptureActivity.this, Constants.TAG_BIND_CODE, map);
                    }

                }
            }
        });
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                restartCamera();
            }
        });
        DialogHelper.resize(context, selectDialog);
        selectDialog.show();
    }

    public void showUpdateDialog(Activity context) {
        SelectDialog selectDialog = new SelectDialog(context, getString(R.string.dialog_title_update_qrcode), ""
                , getString(R.string.btnCancel), getString(R.string.btn_update), R.layout.select_common_dialog);
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                    getDialog(ToastHelper.toStr(R.string.network_exception));
                } else {
                    loadDialog(CaptureActivity.this, UIUtils.getString(R.string.public_submitting));
                    ServerClient.newInstance(CaptureActivity.this).applyUpdateService(CaptureActivity.this, Constants.TAG_APPLY_VIP_UPDATE, null);
                }
            }
        });
        DialogHelper.resize(context, selectDialog);
        selectDialog.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeDialog(showDialog);
        closeDialog(mSelectDialog);
    }

    private void closeDialog(Dialog dialog) {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            if (data != null) {
                Bundle extras = data.getExtras();
                StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
                String storeName = shopBean.getStoreName();
                String storeId = shopBean.getStoreId();
                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, storeId);
                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                LogUtil.d("SP_DEFAULT_STORE_ID=" + storeId + ",,SP_DEFAULT_STORE_NAME=" + storeName);
                mStoreId = storeId;
                if (storeName.length() > 10) {
                    mTvTitleStore.setText(storeName.substring(0, 9) + "...");
                } else {
                    mTvTitleStore.setText(storeName);
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PAY_REMARK) {
            if (data != null) {
                mPayRemark = data.getStringExtra(Constants.RESULT_PAY_REMARK);
            }
        }
    }
}
