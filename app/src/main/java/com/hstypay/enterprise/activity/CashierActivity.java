package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/23 15:09
 * @描述: 选择收银员
 */
public class CashierActivity extends BaseActivity implements View.OnClickListener, CashierRecyclerAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack,mIvClean;
    private TextView mTvTitle, mTvNull, mButton;
    private RecyclerView mRvEmployee;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private EditText mEtInput;

    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private CashierRecyclerAdapter mCashierRecyclerAdapter;
    private List<DataEntity> cashierList;
    private List<DataEntity> cashierSelfList;
    private String mStoreId;
    private DataEntity mCashierBean;
    private boolean isCashier = false;
    private RelativeLayout mRlInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashier);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mEtInput = findViewById(R.id.et_user_input);
        mIvClean = findViewById(R.id.iv_clean);
        mRlInput = findViewById(R.id.ly_input);
        mRlInput.setVisibility(isCashier ? View.GONE : View.VISIBLE);

        mButton.setVisibility(View.INVISIBLE);
//        mEtInput.setClearImage(R.mipmap.ic_search_clear);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtInput.isFocused()) {
                    if (mEtInput.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                        if (isCashier) {
                            cashierSelfList.clear();
                            initSelfList(cashierSelfList);
                        } else {
                            cashierList.clear();
                            cashierList.add(mCashierBean);
                            getCashier("15", "1", mEtInput.getText().toString().trim(), mStoreId);
                            currentPage = 2;
                        }
                    }
                }

            }
        });
        mEtInput.addTextChangedListener(editTextWatcher);
        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvEmployee = (RecyclerView) findViewById(R.id.cashier_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvEmployee.setLayoutManager(mLinearLayoutManager);
        mRvEmployee.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    currentPage = 2;
                    getCashier("15", "1", mEtInput.getText().toString().trim(), mStoreId);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getCashier(pageSize + "", currentPage + "", mEtInput.getText().toString().trim(), mStoreId);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
    }

    public void initData() {
        String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if ("INTENT_QRCODE_CASHIER".equals(intentName)) {
            mTvTitle.setText(R.string.title_cashier);
        } else {
            mTvTitle.setText(R.string.title_select_cashier);
        }
        if (isCashier) {//收银员就只显示全部收银员和他自己
            mSwipeRefreshLayout.setLoadmoreEnable(false);
            mSwipeRefreshLayout.setRefreshEnable(false);
            cashierSelfList = new ArrayList<>();
            cashierSelfList.clear();
            mCashierRecyclerAdapter = new CashierRecyclerAdapter(MyApplication.getContext(), cashierSelfList);
            mCashierRecyclerAdapter.setOnItemClickListener(this);
            mRvEmployee.setAdapter(mCashierRecyclerAdapter);
            initSelfList(cashierSelfList);
        } else {
            mStoreId = getIntent().getStringExtra(Constants.REQUEST_CASHIER_INTENT);
            cashierList = new ArrayList<>();
            cashierList.clear();
            mCashierRecyclerAdapter = new CashierRecyclerAdapter(MyApplication.getContext(), cashierList);
            mCashierRecyclerAdapter.setOnItemClickListener(this);
            mRvEmployee.setAdapter(mCashierRecyclerAdapter);
            if (!"INTENT_QRCODE_CASHIER".equals(intentName)) {
                mCashierBean = new DataEntity();
                mCashierBean.setRealName(getString(R.string.tv_all_user));
                mCashierBean.setUserId("");
                cashierList.add(mCashierBean);
            }
            getCashier("15", "1", mEtInput.getText().toString().trim(), mStoreId);
        }
        String cashierId = getIntent().getStringExtra(Constants.INTENT_CASHIER_ID);
        mCashierRecyclerAdapter.setSelected(cashierId);


        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!isCashier) {
                        cashierList.clear();
                        currentPage = 2;
                        getCashier("15", "1", mEtInput.getText().toString().trim(), mStoreId);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void initSelfList(List<DataEntity> cashierSelfList) {
        DataEntity cashierAll = new DataEntity();
        cashierAll.setRealName(getString(R.string.tv_all_user));
        cashierAll.setUserId("");
        DataEntity cashierSelf = new DataEntity();
        cashierSelf.setRealName(SpUtil.getString(MyApplication.getContext(), Constants.REALNAME, "自己"));
        cashierSelf.setUserId(SpUtil.getString(MyApplication.getContext(), Constants.USER_ID, ""));
        cashierSelfList.add(cashierAll);
        cashierSelfList.add(cashierSelf);
    }

    private void initAdminList() {

    }

    //收銀員列表
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCashierEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_CASHIER)) {
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CashierActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {

                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        cashierList.addAll(msg.getData());
                        mCashierRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {

                        }
                    }
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            cashierList.clear();
            cashierList.add(mCashierBean);
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    public void getCashier(String pageSize, String currentPage, String userName, String storeId) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        if (!TextUtils.isEmpty(userName)) {
            map.put("userName", userName);
        }
        if (!TextUtils.isEmpty(storeId)) {
            map.put("storeId", storeId);
        }
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
                showNewLoading(true, getString(R.string.public_loading));
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), Constants.TAG_GET_CASHIER, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                cashierList.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                mTvNull.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        String cashierId = getIntent().getStringExtra(Constants.INTENT_CASHIER_ID);
        mCashierRecyclerAdapter.setSelected(cashierId);
        DataEntity dataEntity;
        if (isCashier) {
            dataEntity = cashierSelfList.get(position);
        } else {
            dataEntity = cashierList.get(position);
        }
        if (dataEntity != null) {
            Intent intent = new Intent();
            Bundle mBundle = new Bundle();
            mBundle.putSerializable(Constants.RESULT_CASHIER_INTENT, dataEntity);   //传递一个user对象列表
            intent.putExtras(mBundle);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
