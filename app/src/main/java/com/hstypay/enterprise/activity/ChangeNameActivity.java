package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hstypay.enterprise.activity.tinycashier.ActivationCodeDetailsActivity.KEY_UPDATE_SHORT_NAME;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 门店单选列表
 */
public class ChangeNameActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvNull, mButton;
    private EditTextDelete mEtName;
    private boolean isShortName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
    }

    public void initView() {
        String name = getIntent().getStringExtra(Constants.INTENT_CHANGE_NAME);
        isShortName = getIntent().getBooleanExtra(KEY_UPDATE_SHORT_NAME, false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtName = findViewById(R.id.et_name);
        if (isShortName) {
            mTvTitle.setText("修改门店简称");
            mEtName.setHint("请填写门店简称");
        } else {
            mTvTitle.setText(R.string.title_change_name);
        }
        mButton.setVisibility(View.VISIBLE);
        mButton.setText(R.string.btn_complete);
        mButton.setEnabled(false);
        mButton.setTextColor(UIUtils.getColor(R.color.theme_color_fifty));
        mEtName.setClearImage(R.mipmap.ic_search_clear);
        mEtName.setText(name);
        if (name.length() > 0) {
            mButton.setEnabled(true);
            mButton.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            mButton.setEnabled(false);
            mButton.setTextColor(UIUtils.getColor(R.color.theme_color_fifty));
        }
    }


    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mButton.setEnabled(true);
                    mButton.setTextColor(UIUtils.getColor(R.color.theme_color));
                } else {
                    mButton.setEnabled(false);
                    mButton.setTextColor(UIUtils.getColor(R.color.theme_color_fifty));
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (isShortName) {
                    if (StringUtils.managerName10People(mEtName.getText().toString().trim())) {
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.RESULT_CHANGE_NAME, mEtName.getText().toString().trim());   //传递一个user对象列表
                        intent.putExtras(bundle);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        showCommonNoticeDialog(ChangeNameActivity.this, getString(R.string.regex_manager_name_10_People));
                    }
                    return;
                }
                if (StringUtils.managerName(mEtName.getText().toString().trim())) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.RESULT_CHANGE_NAME, mEtName.getText().toString().trim());   //传递一个user对象列表
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    showCommonNoticeDialog(ChangeNameActivity.this, getString(R.string.regex_manager_name));
                }
                break;
        }
    }
}
