package com.hstypay.enterprise.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chinaums.mis.bean.ResponsePojo;
import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.facepay.UnionPayFacePayUtils;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.EasypayInfo;
import com.hstypay.enterprise.bean.FuxunBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.YinshangPosBean;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.UmsPayUtils;
import com.ums.anypay.service.IOnTransEndListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class CheckActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_REFUND_PWD = "key_refund_pwd";
    private ImageView mIvBack, mIvEye;
    private Button mBtnSubmit;
    private TextView mTvTitle, mButton, tvPwdHint;
    private EditText mEtCheckPwd;
    public static CheckActivity instance = null;
    private SafeDialog mLoadDialog, mSyncLoadDialog, mCancelLoadDialog;
    private PayBean.DataBean mData;
    private PayBean.DataBean mRefundData;
    private boolean isOpenPwd;
    private SafeKeyboard safeKeyboard;
    private boolean retry = true;
    private Intent mResultData;
    private YinshangPosBean mYinshangReverseBean;
    private YinshangPosBean mYinshangRefundBean;
    private Long mRefundMoney;
    private boolean isRefundPwd;
    private Handler mHandler;
    private boolean cancelTask;
    private SelectDialog mSyncDialog;
    private CommonNoticeDialog.OnClickOkListener mOnClickOkListener;
    private FuxunBean mFuxunBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        /*if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindCUPService();
        }*/
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mCancelLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), true);
        mSyncLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_order_sync), false);
        isRefundPwd = getIntent().getBooleanExtra(KEY_REFUND_PWD, false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        tvPwdHint = findViewById(R.id.tvPwdHint);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtCheckPwd = (EditText) findViewById(R.id.et_login_pwd);
        mIvEye = (ImageView) findViewById(R.id.iv_eye_pwd);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvTitle.setText(R.string.title_authentication);
        mButton.setVisibility(View.INVISIBLE);

        View rootView = findViewById(R.id.main_root);
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtCheckPwd.getId(), mEtCheckPwd);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtCheckPwd.isFocused()) {
                    if (mEtCheckPwd.getText().toString().length() > 0) {
                        setButtonEnable(mBtnSubmit, true);
                    } else {
                        setButtonEnable(mBtnSubmit, false);
                    }
                }

            }
        });
        mEtCheckPwd.addTextChangedListener(editTextWatcher);
        setButtonEnable(mBtnSubmit, false);
    }

    public void initEvent() {
        mBtnSubmit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(),Constants.APP_META_DATA_KEY))
                        && mData.getApiProvider() == 11
                        && !DateUtil.getDate().equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE))) {
                    //如果是福卡支付且今天没签到成功过
                    wizardFukaSign();
                } else {
                    submit();
                }
            }
        });
        mIvBack.setOnClickListener(this);
        mIvEye.setOnClickListener(this);
        mOnClickOkListener = new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                startMainActivity();
            }
        };
    }

    public void initData() {
        mHandler = new Handler();
        tvPwdHint.setText(isRefundPwd ? "请输入退款密码,验证身份" : getResources().getString(R.string.tv_check_title));
        if (isRefundPwd) {
            mEtCheckPwd.setInputType(InputType.TYPE_CLASS_NUMBER);
            mEtCheckPwd.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            mEtCheckPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        mData = (PayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_ORDER_REFUND);
    }


    //订单详情返回数据
    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefund(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_REFUND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        /*if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) && mData.getApiProvider() == 10) {
                            mRefundData = data;
                            Long refundMoney = getIntent().getLongExtra(Constants.INTENT_REFUND_MONEY, 0);
                            startArouseCardRefund(data, refundMoney);
                        } else {
                            startVerify(data);
                        }*/
                        //银商刷脸退款
                        if (mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {
                            mRefundData = data;
                            unionPayRefund();
                        } else if (mData.getApiProvider() == 10 || mData.getApiProvider() == 11) {
                            mRefundData = data;
                            Long refundMoney = getIntent().getLongExtra(Constants.INTENT_REFUND_MONEY, 0);
                            startArouseCardRefund(data, refundMoney);
                        } else {
                            startVerify(data);
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_BCARD_REVERSE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        /*if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) && mData.getApiProvider() == 10) {
                            startArouseCardReverse(data);
                        } else {
                            startVerify(data);
                        }*/
                        //银商刷脸撤销
                        if (mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {
                            unionPayReverse();
                        } else {
                            startArouseCardReverse(data);
                        }
                    }
                    break;
            }
        }
        //退款认证
        if (event.getTag().equals(Constants.TAG_AUTHENTICATION_REFUND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.AUTHENTICATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.AUTHENTICATION_TRUE:
                    if (mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {//刷脸退款
                        unionPayRefund();
                    } else {
                        startArouseCardRefund(mData);
                    }
                    break;
            }
        }
        //退款成功通知
        if (event.getTag().equals(Constants.NOTIFY_REFUND_EASYPAY) || event.getTag().equals(Constants.NOTIFY_REVERSE_EASYPAY)) {
            DialogUtil.safeCloseDialog(mSyncLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(CheckActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    startMainActivity();
                    break;
            }
        }
    }


    //银联刷脸撤销退款返回对象
    private ResponsePojo mResponsePojo;

    /**
     * 银商刷脸撤销
     */
    private void unionPayReverse() {
        DialogUtil.safeShowDialog(mCancelLoadDialog);
        int transType;
        if (mRefundData.getApiProvider() == 13 && mRefundData.getApiCode().equals(Constants.UNIONPAY_FACEPAY)) {
            transType = 30;
        } else {
            transType = 31;
        }

        String amount = String.valueOf(mRefundMoney);
        UnionPayFacePayUtils.Builder builder = new UnionPayFacePayUtils.Builder(transType, amount, mData.getOperNo(), mData.getReqOrderNo());
        builder = builder.setTraceNO(mData.getVoucherNo())
                .setThirdOrderNo(mData.getThirdOrderNo());
        UnionPayFacePayUtils.bankDAO(builder, this, response -> {
            mResponsePojo = response;
            LogUtil.d("Jerermy--mResponsePojo==" + new Gson().toJson(mResponsePojo));
            runOnUiThread(() -> {
                if (mResponsePojo != null) {
                    if (!"00".equals(mResponsePojo.getRspCode())) {
                        showCommonNoticeDialog(CheckActivity.this, mResponsePojo.getRspChin(), () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                unionPayReverseNotify(mResponsePojo);
                            }
                        });
                    } else {
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            unionPayReverseNotify(mResponsePojo);
                        }
                    }
                } else {
                    showCommonNoticeDialog(CheckActivity.this, "撤销回调数据有误，请确认结果！", mOnClickOkListener);
                }
            });
        });
    }


    /**
     * 银联刷脸撤销
     */
    private void unionPayReverseNotify(ResponsePojo response) {
        if (response != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> parameter = new HashMap<>();
            parameter.put("storeMerchantId", mData.getStoreMerchantId());
            parameter.put("outTradeNo", mData.getOutTradeNo());
            if ("00".equals(response.getRspCode())) {
                parameter.put("resultCode", "0");
                parameter.put("resultMsg", "OK");
            } else {
                parameter.put("resultMsg", response.getRspChin());
                parameter.put("resultCode", "1");
            }
            parameter.put("resultMsg", response.getTransMemo());
            parameter.put("refNo", response.getRefNo());
            parameter.put("voucherNo", response.getTraceNo());
            parameter.put("batchId", response.getBatchNo());
            ServerClient.newInstance(MyApplication.getContext()).reverseNotify(MyApplication.getContext(), Constants.NOTIFY_REVERSE_EASYPAY, parameter);
        }
    }

    /**
     * 银联刷脸退款
     */
    private void unionPayRefund() {
        DialogUtil.safeShowDialog(mCancelLoadDialog);
        int transType;
        if (mRefundData.getApiProvider() == 13 && mRefundData.getApiCode().equals(Constants.UNIONPAY_FACEPAY)) {
            transType = 32;
        } else {
            transType = 33;
        }
        String amount = String.valueOf(mRefundMoney);
        UnionPayFacePayUtils.Builder builder = new UnionPayFacePayUtils.Builder(transType, amount,
                mData.getOperNo(), mData.getReqOrderNo());
        builder = builder.setTimeEnd(mRefundData.getTradeTime())
                .setThirdOrderNo(mRefundData.getThirdOrderNo())
                .setRefNo(mRefundData.getOriRefNo());
        UnionPayFacePayUtils.bankDAO(builder, this, response -> {
            mResponsePojo = response;
                    /*if (!response.getRspCode().equals("00")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showCommonNoticeDialog(CheckActivity.this, response.getRspChin(), new CommonNoticeDialog.OnClickOkListener() {
                                    @Override
                                    public void onClickOk() {
                                        startMainActivity();
                                    }l
                                });
                            }
                        });
                    } else {
                        startMainActivity();
                    }
                    unionPayRefundNotify(mResponsePojo);*/
            runOnUiThread(() -> {
                if (mResponsePojo != null) {
                    if (!"00".equals(mResponsePojo.getRspCode())) {
                        showCommonNoticeDialog(CheckActivity.this, mResponsePojo.getRspChin(), () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                unionPayRefundNotify(mResponsePojo);
                            }
                        });
                    } else {
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            unionPayRefundNotify(mResponsePojo);
                        }
                    }
                } else {
                    showCommonNoticeDialog(CheckActivity.this, "退款回调数据有误，请确认结果！", mOnClickOkListener);
                }
            });
        });
    }


    private void startMainActivity() {
        remove();
        Intent intent = new Intent(Constants.ACTION_ORDER_REFUND_DATA);
        sendBroadcast(intent);
        Intent intentMainActivity = new Intent(CheckActivity.this, MainActivity.class);
        startActivity(intentMainActivity);
        finish();
    }

    private void startVerify(PayBean.DataBean data) {
        Intent intent = new Intent();
        intent.putExtra("refundDatas", data);
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CHECK_VERIFY);
        intent.putExtra(Constants.INTENT_REMARK, getIntent().getStringExtra(Constants.INTENT_REMARK));
        intent.setClass(CheckActivity.this, VerifyActivity.class);
        startActivity(intent);
        PayDetailActivity.instance.finish();
        RefundActivity.instance.finish();
        finish();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                if (mData.getApiProvider() == 11 && !DateUtil.getDate().equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE))) {
                    //如果是福卡支付且今天没签到成功过
                    wizardFukaSign();
                } else {
                    submit();
                }
                break;
            case R.id.iv_eye_pwd:
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd, mIvEye, mEtCheckPwd);
                break;
            default:
                break;
        }
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return !isOpen;
    }

    private void submit() {
        if (!NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showCommonNoticeDialog(CheckActivity.this, getString(R.string.network_exception));
            return;
        }
        if (StringUtils.isEmptyOrNull(mEtCheckPwd.getText().toString().trim())) {
            showCommonNoticeDialog(CheckActivity.this, "请输入密码！");
            return;
        }
        mRefundData = new PayBean.DataBean();
        mRefundData.setRefundNo(mData.getRefundNo());
        mRefundData.setOutRefundNo(mData.getOutRefundNo());
        mRefundData.setRefundMoney(mData.getRefundMoney());
        mRefundData.setOrderNo(mData.getOrderNo());
        mRefundData.setRefNo(mData.getRefNo());
        mRefundData.setTradeTime(mData.getTradeTime());
        mRefundData.setThirdOrderNo(mData.getThirdOrderNo());
        mRefundData.setTransactionId(mData.getTransactionId());
        mRefundData.setOutTradeNo(mData.getOutTradeNo());
        mRefundData.setVoucherNo(mData.getVoucherNo());
        mRefundData.setStoreMerchantId(mData.getStoreMerchantId());
        mRefundData.setApiCode(mData.getApiCode());
        mRefundData.setApiProvider(mData.getApiProvider());
        mRefundMoney = getIntent().getLongExtra(Constants.INTENT_REFUND_MONEY, 0);
        if (mData != null) {
            //刷卡或银商刷脸撤销
            if ((mData.getApiProvider() == 10 || mData.getApiProvider() == 13 || mData.getApiProvider() == 15 || mData.getApiProvider() == 11) && mData.getReverseFlag() == 1) {
                bcardReverse(mData);
            } else {
                //刷卡或银商刷脸退款
                if ((mData.getApiProvider() == 10 || mData.getApiProvider() == 13 || mData.getApiProvider() == 15 || mData.getApiProvider() == 11) && !mData.isPay() && mData.getRefundStatus() == 6) {
                    authentication();
                } else {
//                    fukaBcardRefund(mData,1);
                    getRefund(mData, mRefundMoney);
                }
            }
        }
    }

    private void authentication() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("password",  INITDES3Util.getSecretContent(this, mEtCheckPwd.getText().toString().trim()));
            map.put("desFlag", 1);
            if (isRefundPwd) {//退款专用密码
                ServerClient.newInstance(CheckActivity.this).verifyRefundPwd(CheckActivity.this, Constants.TAG_AUTHENTICATION_REFUND, map);
            } else {
                ServerClient.newInstance(CheckActivity.this).authentication(CheckActivity.this, Constants.TAG_AUTHENTICATION_REFUND, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getRefund(PayBean.DataBean data, Long refundMoney) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(data.getOrderNo())) {
                map.put("orderNo", data.getOrderNo());
            }
            map.put("refundMoney", refundMoney);
            map.put("password", INITDES3Util.getSecretContent(this, mEtCheckPwd.getText().toString().trim()));
            map.put("desFlag", 1);
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            String remark = getIntent().getStringExtra(Constants.INTENT_REMARK);
            if (!TextUtils.isEmpty(remark)) {
                map.put("remark", remark);
            }
            if (!TextUtils.isEmpty(StringUtils.getDeviceInfo(AppHelper.getSN()))) {
                map.put("opDeviceId", StringUtils.getDeviceInfo(AppHelper.getSN()));
            }
            ServerClient.newInstance(CheckActivity.this).getRefund(CheckActivity.this, Constants.TAG_GET_REFUND, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void bcardReverse(PayBean.DataBean data) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(data.getOrderNo())) {
                map.put("outTradeNo", data.getOutTradeNo());
            }
            map.put("password",  INITDES3Util.getSecretContent(this, mEtCheckPwd.getText().toString().trim()));
            map.put("desFlag", 1);
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            map.put("storeMerchantId", data.getStoreMerchantId());
            ServerClient.newInstance(CheckActivity.this).bcardReverse(CheckActivity.this, Constants.TAG_BCARD_REVERSE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void startArouseCardRefund(PayBean.DataBean dataBean) {
        StringBuilder builderUri = new StringBuilder();
        switch (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) {
            case Constants.HUIFU:
                builderUri.append("payment://com.pnr.pospp/refund?");
                builderUri.append("&ordAmt=").append(dataBean.getRefundMoney());
                if (!TextUtils.isEmpty(dataBean.getReqOrderNo())) {
                    builderUri.append("&oriSelfOrdId=").append(dataBean.getReqOrderNo());
                }
                if (!TextUtils.isEmpty(dataBean.getReqRefundNo())) {
                    builderUri.append("&merOrdId=").append(dataBean.getReqRefundNo());
                }
                if (!TextUtils.isEmpty(dataBean.getRetNotifyUrl())) {
                    builderUri.append("&bgRetUrl=").append(dataBean.getRetNotifyUrl());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosMemberId())) {
                    builderUri.append("&memberId=").append(MyApplication.getPosMemberId());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosEmpAccount())) {
                    builderUri.append("&merOperId=").append(MyApplication.getPosEmpAccount());
                }
                builderUri.append("&merOperIdPrinted=").append("0");
                if (!TextUtils.isEmpty(dataBean.getExt1())) {
                    builderUri.append("&merPiv=").append(dataBean.getExt1());
                }
                LogUtil.d("builderUri====refund=" + builderUri.toString());
                startActivityRefund(builderUri.toString());
                break;
            case Constants.WIZARPOS:
            case Constants.LIANDI:
            case Constants.MIBAGPAY_LIANDI:
                startActivityWizarposRefund(dataBean, dataBean.getRefundMoney());
                break;
            case Constants.WJY_WIZARPOS:
                if (dataBean.getApiProvider() == 11) {
                    fukaWizardBcardRefund(dataBean, dataBean.getRefundMoney());
                } else {
                    startActivityWizarposRefund(dataBean, dataBean.getRefundMoney());
                }
                break;
            case Constants.WJY_YINSHANG:
                if (mData.getApiProvider() == 11) {//福卡
                    fukaBcardRefund(dataBean, dataBean.getRefundMoney());
                } else {
                    startYinshangRefund(dataBean, dataBean.getRefundMoney());
                }
                break;
        }
    }

    private void startArouseCardRefund(PayBean.DataBean dataBean, Long refundMoney) {
        StringBuilder builderUri = new StringBuilder();
        switch (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) {
            case Constants.HUIFU:
                builderUri.append("payment://com.pnr.pospp/refund?");
                builderUri.append("&ordAmt=").append(refundMoney);
                if (!TextUtils.isEmpty(dataBean.getOriReqOrderNo())) {
                    builderUri.append("&oriSelfOrdId=").append(dataBean.getOriReqOrderNo());
                }
                if (!TextUtils.isEmpty(dataBean.getReqOrderNo())) {
                    builderUri.append("&merOrdId=").append(dataBean.getReqOrderNo());
                }
                if (!TextUtils.isEmpty(dataBean.getRetNotifyUrl())) {
                    builderUri.append("&bgRetUrl=").append(dataBean.getRetNotifyUrl());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosMemberId())) {
                    builderUri.append("&memberId=").append(MyApplication.getPosMemberId());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosEmpAccount())) {
                    builderUri.append("&merOperId=").append(MyApplication.getPosEmpAccount());
                }
                builderUri.append("&merOperIdPrinted=").append("0");
                if (!TextUtils.isEmpty(dataBean.getExt1())) {
                    builderUri.append("&merPiv=").append(dataBean.getExt1());
                }
                LogUtil.d("builderUri====refund+money=" + builderUri.toString());
                startActivityRefund(builderUri.toString());
                break;
            case Constants.WIZARPOS:
            case Constants.LIANDI:
            case Constants.MIBAGPAY_LIANDI:
                startActivityWizarposRefund(dataBean, refundMoney);
                break;
            case Constants.WJY_WIZARPOS:
                if (dataBean.getApiProvider() == 11) {
                    fukaWizardBcardRefund(dataBean, refundMoney);
                } else {
                    startActivityWizarposRefund(dataBean, refundMoney);
                }
                break;
            case Constants.WJY_YINSHANG:
                if (dataBean.getApiProvider() == 11) {
                    fukaBcardRefund(dataBean, refundMoney);//TODO 验一下
                } else {
                    startYinshangRefund(dataBean, refundMoney);
                }
                break;
            default:
                startVerify(dataBean);
                break;
        }
    }

    private void startArouseCardReverse(PayBean.DataBean dataBean) {
        StringBuilder builderUri = new StringBuilder();
        switch (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) {
            case Constants.HUIFU:
                builderUri.append("payment://com.pnr.pospp/paymentVoid?");
                builderUri.append("channelId=acquire");
                if (!TextUtils.isEmpty(dataBean.getVoucherNo())) {
                    builderUri.append("&oriVoucherNo=").append(dataBean.getVoucherNo());
                }
                if (!TextUtils.isEmpty(dataBean.getReqOrderNo())) {
                    builderUri.append("&merOrdId=").append(dataBean.getReqOrderNo());
                }
                if (!TextUtils.isEmpty(dataBean.getRetNotifyUrl())) {
                    builderUri.append("&bgRetUrl=").append(dataBean.getRetNotifyUrl());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosMemberId())) {
                    builderUri.append("&memberId=").append(MyApplication.getPosMemberId());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosEmpAccount())) {
                    builderUri.append("&merOperId=").append(MyApplication.getPosEmpAccount());
                }
                builderUri.append("&merOperIdPrinted=").append("0");
                if (!TextUtils.isEmpty(dataBean.getExt1())) {
                    builderUri.append("&merPiv=").append(dataBean.getExt1());
                }
                /*if (!TextUtils.isEmpty("")){
                    builderUri.append("&outPrintData=").append("");
                }*/
                LogUtil.d("builderUri====paymentVoid=" + builderUri.toString());
                startActivityRefund(builderUri.toString());
                break;
            case Constants.WIZARPOS:
            case Constants.LIANDI:
            case Constants.MIBAGPAY_LIANDI:
                startActivityWizarposReverse(dataBean);
                break;
            case Constants.WJY_WIZARPOS:
                if (mData.getApiProvider() == 11) {//银商福卡pos退货
                    fukaWizardBcardReverse(dataBean);
                } else {
                    startActivityWizarposReverse(dataBean);
                }
                break;
            case Constants.WJY_YINSHANG:
                if (mData.getApiProvider() == 11) {//银商福卡pos退货
                    fukaBcardReverse(dataBean);
                } else {
                    startYinshangReverse(dataBean);
                }
                break;
            default:
                startVerify(dataBean);
                break;
        }
    }

    /**
     * 银商福卡pos撤销
     */
    private void fukaBcardReverse(PayBean.DataBean dataBean) {
        UmsPayUtils.Builder builder = new UmsPayUtils.Builder(UmsPayUtils.Type.Revoke, dataBean.getOrderNo());
        builder.setOrgTraceNo(dataBean.getVoucherNo());
        UmsPayUtils.callTrans(this, builder, new UmsPayUtils.UmsCallBack() {
            @Override
            public void response(YinshangPosBean posBean) {
                mYinshangReverseBean = posBean;
                /*reserveYinshangNotify(mYinshangReverseBean);
                back();*/
                if (mYinshangReverseBean != null) {
                    if ("0".equals(mYinshangReverseBean.getResultCode())) {
                        if (mYinshangReverseBean.getTransData() != null && "00".equals(mYinshangReverseBean.getTransData().getResCode())) {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                reserveYinshangNotify(mYinshangReverseBean);
                            }
                        } else {
                            String resultDesc = "撤销失败";
                            if (mYinshangReverseBean.getTransData() != null && !TextUtils.isEmpty(mYinshangReverseBean.getTransData().getResDesc()))
                                resultDesc = mYinshangReverseBean.getTransData().getResDesc();
                            showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                                if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    MyToast.showToastShort(getString(R.string.network_exception));
                                    syncOrderState();
                                } else {
                                    reserveYinshangNotify(mYinshangReverseBean);
                                }
                            });
                        }
                    } else {
                        showCommonNoticeDialog(CheckActivity.this, mYinshangReverseBean.getResultMsg(), () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                reserveYinshangNotify(mYinshangReverseBean);
                            }
                        });
                    }
                } else {
                    showCommonNoticeDialog(CheckActivity.this, "撤销回调数据有误，请确认结果！", mOnClickOkListener);
                }
            }
        });
    }

    /**
     * 慧银福卡pos撤销
     */
    private void fukaWizardBcardReverse(PayBean.DataBean dataBean) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Map<String, Object> map = new HashMap<>();
                    map.put("AppID", Constants.WJYS_APPID);
                    map.put("AppName", "WJYS");
                    map.put("ItemID", "WJY02");
                    map.put("CustomerOID", dataBean.getRefundNo());
                    map.put("trxId", dataBean.getRefundNo());
                    map.put("TransType", 13);//撤销
                    map.put("TransAmount", StringUtils.formatFukaMoney(mData.getMoney() + ""));
                    if (!TextUtils.isEmpty(dataBean.getOrderNo())) {
                        if (dataBean.getOrderNo().length() > 16) {
                            map.put("TransIndexCode", dataBean.getOrderNo().substring(dataBean.getOrderNo().length() - 16));
                        } else {
                            map.put("TransIndexCode", dataBean.getOrderNo());
                        }
                    }
                    map.put("ReqTransDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                    map.put("ReqTransTime", DateUtil.formatDateToHHmmss(DateUtil.formatTime(System.currentTimeMillis())));
                    map.put("OriTraceNo", dataBean.getVoucherNo());
                    map.put("AdminPass", "123456");
                    LogUtil.d("Jeremy-reverse-json==" + new Gson().toJson(map));
                    String transact =  MainActivity.instance.getCupService().transact(new Gson().toJson(map));
                    LogUtil.d("Jeremy-reverse==" + transact);
                    mFuxunBean = new Gson().fromJson(transact, FuxunBean.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mFuxunBean != null) {
                                if ("00".equals(mFuxunBean.getRespCode())) {
                                    if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                        MyToast.showToastShort(getString(R.string.network_exception));
                                        syncOrderState();
                                    } else {
                                        reverseWizardFukaNotify(mFuxunBean);
                                    }
                                } else {
                                    String resultDesc = "撤销失败";
                                    if (!TextUtils.isEmpty(mFuxunBean.getRespDesc()))
                                        resultDesc = mFuxunBean.getRespDesc();
                                    showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                            MyToast.showToastShort(getString(R.string.network_exception));
                                            syncOrderState();
                                        } else {
                                            reverseWizardFukaNotify(mFuxunBean);
                                        }
                                    });
                                }
                            } else {
                                showCommonNoticeDialog(CheckActivity.this, "撤销回调数据有误，请确认结果！", mOnClickOkListener);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showCommonNoticeDialog(CheckActivity.this, "撤销回调数据有误，请确认结果！", mOnClickOkListener);
                        }
                    });
                }
            }
        }).start();
    }

    /**
     * 银商福卡pos退货
     */
    private void fukaBcardRefund(PayBean.DataBean dataBean, long money) {
        UmsPayUtils.Builder builder = new UmsPayUtils.Builder(UmsPayUtils.Type.Refund, dataBean.getRefundNo());
        builder.setRefNo(dataBean.getOriRefNo());
        builder.setTradeTime(dataBean.getTradeTime());
        builder.setMoney(String.valueOf(money));
        UmsPayUtils.callTrans(this, builder, posBean -> {
            mYinshangRefundBean = posBean;
            /*refundYinshangNotify(mYinshangRefundBean);
            back();*/
            if (mYinshangRefundBean != null) {
                if ("0".equals(mYinshangRefundBean.getResultCode())) {
                    if (mYinshangRefundBean.getTransData() != null && "00".equals(mYinshangRefundBean.getTransData().getResCode())) {
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            reserveYinshangNotify(mYinshangRefundBean);
                        }
                    } else {
                        String resultDesc = "退款失败";
                        if (mYinshangRefundBean.getTransData() != null && !TextUtils.isEmpty(mYinshangRefundBean.getTransData().getResDesc()))
                            resultDesc = mYinshangRefundBean.getTransData().getResDesc();
                        showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                reserveYinshangNotify(mYinshangRefundBean);
                            }
                        });
                    }
                } else {
                    showCommonNoticeDialog(CheckActivity.this, mYinshangRefundBean.getResultMsg(), () -> {
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            refundYinshangNotify(mYinshangRefundBean);
                        }
                    });
                }
            } else {
                showCommonNoticeDialog(CheckActivity.this, "退款回调数据有误，请确认结果！", mOnClickOkListener);
            }
        });
    }

    /**
     * 慧银福卡pos退货
     */
    private void fukaWizardBcardRefund(PayBean.DataBean dataBean, long money) {
        new Thread(() -> {
            try {
                Map<String, Object> map = new HashMap<>();
                map.put("AppID", Constants.WJYS_APPID);
                map.put("AppName", "WJYS");
                map.put("ItemID", "WJY02");
                map.put("CustomerOID", dataBean.getRefundNo());
                map.put("trxId", dataBean.getRefundNo());
                map.put("TransType", 3);//退货
                map.put("TransAmount", StringUtils.formatFukaMoney(money + ""));
                if (!TextUtils.isEmpty(dataBean.getRefundNo())) {
                    if (dataBean.getRefundNo().length() > 16) {
                        map.put("TransIndexCode", dataBean.getRefundNo().substring(dataBean.getRefundNo().length() - 16));
                    } else {
                        map.put("TransIndexCode", dataBean.getRefundNo());
                    }
                }
                map.put("ReqTransDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                map.put("ReqTransTime", DateUtil.formatDateToHHmmss(DateUtil.formatTime(System.currentTimeMillis())));
//            map.put("OriTraceNo", dataBean.getVoucherNo());
                map.put("AdminPass", "123456");
                map.put("OldRRN", dataBean.getOriRefNo());//原交易参考号
                map.put("OldTransDate", DateUtil.formatMMDD(dataBean.getTradeTime()));//原交易日期(MMDD)
                LogUtil.d("Jeremy-refund-json==" + new Gson().toJson(map));
                String transact =  MainActivity.instance.getCupService().transact(new Gson().toJson(map));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.d("Jeremy-refund==" + transact);
                        mFuxunBean = new Gson().fromJson(transact, FuxunBean.class);
                        if (mFuxunBean != null) {
                            if ("00".equals(mFuxunBean.getRespCode())) {
                                if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    MyToast.showToastShort(getString(R.string.network_exception));
                                    syncOrderState();
                                } else {
                                    refundWizardFukaNotify(mFuxunBean);
                                }
                            } else {
                                String resultDesc = "退款失败";
                                if (!TextUtils.isEmpty(mFuxunBean.getRespDesc()))
                                    resultDesc = mFuxunBean.getRespDesc();
                                showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                                    if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                        MyToast.showToastShort(getString(R.string.network_exception));
                                        syncOrderState();
                                    } else {
                                        refundWizardFukaNotify(mFuxunBean);
                                    }
                                });
                            }
                        } else {
                            showCommonNoticeDialog(CheckActivity.this, "退款回调数据有误，请确认结果！", mOnClickOkListener);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showCommonNoticeDialog(CheckActivity.this, "退款回调数据有误，请确认结果！", mOnClickOkListener);
                    }
                });
            }
        }).start();
    }

    /**
     * 银商pos撤销
     */
    private void startYinshangReverse(PayBean.DataBean dataBean) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "全民惠";
        String transAppId = "撤销";
        JSONObject transData = new JSONObject();
        try {
            transData.put("appId", APPID);//appId
            transData.put("isNeedPrintReceipt", false);//交易结束后自动打单
            transData.put("orgTraceNo", dataBean.getVoucherNo());
            transData.put("extOrderNo", dataBean.getOrderNo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
                mYinshangReverseBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                /*reserveYinshangNotify(mYinshangReverseBean);
                if (mYinshangReverseBean.getTransData() != null && "00".equals(mYinshangReverseBean.getTransData().getResCode())) {
                    back();
                }*/
                if (mYinshangReverseBean != null) {
                    if ("0".equals(mYinshangReverseBean.getResultCode())) {
                        if (mYinshangReverseBean.getTransData() != null && "00".equals(mYinshangReverseBean.getTransData().getResCode())) {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                reserveYinshangNotify(mYinshangReverseBean);
                            }
                        } else {
                            String resultDesc = "撤销失败";
                            if (mYinshangReverseBean.getTransData() != null && !TextUtils.isEmpty(mYinshangReverseBean.getTransData().getResDesc()))
                                resultDesc = mYinshangReverseBean.getTransData().getResDesc();
                            showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                                if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    MyToast.showToastShort(getString(R.string.network_exception));
                                    syncOrderState();
                                } else {
                                    reserveYinshangNotify(mYinshangReverseBean);
                                }
                            });
                        }
                    } else {
                        showCommonNoticeDialog(CheckActivity.this, mYinshangReverseBean.getResultMsg(), () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                reserveYinshangNotify(mYinshangReverseBean);
                            }
                        });
                    }
                } else {
                    showCommonNoticeDialog(CheckActivity.this, "撤销回调数据有误，请确认结果！", mOnClickOkListener);
                }
            }
        };
        com.ums.AppHelper.callTrans(CheckActivity.this, transAppName, transAppId, transData, listener);
    }

    /**
     * 银商pos退货
     */
    private void startYinshangRefund(PayBean.DataBean dataBean, long money) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "全民惠";
        String transAppId = "退货";
        JSONObject transData = new JSONObject();
        try {
            transData.put("amt", money);//金额
            transData.put("appId", APPID);//appId
            transData.put("isNeedPrintReceipt", false);//交易结束后自动打单
            transData.put("refNo", dataBean.getOriRefNo());
            transData.put("extOrderNo", dataBean.getRefundNo());
            transData.put("date", DateUtil.formartTradeTimeFormat(dataBean.getTradeTime(), "MMdd"));
            transData.put("tradeYear", DateUtil.formartTradeTimeFormat(dataBean.getTradeTime(), "yyyy"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
                mYinshangRefundBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                if (mYinshangRefundBean != null) {
                    if ("0".equals(mYinshangRefundBean.getResultCode())) {
                        if (mYinshangRefundBean.getTransData() != null && "00".equals(mYinshangRefundBean.getTransData().getResCode())) {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                refundYinshangNotify(mYinshangRefundBean);
                            }
                        } else {
                            String resultDesc = "退款失败";
                            if (mYinshangRefundBean.getTransData() != null && !TextUtils.isEmpty(mYinshangRefundBean.getTransData().getResDesc()))
                                resultDesc = mYinshangRefundBean.getTransData().getResDesc();
                            showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                                if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    MyToast.showToastShort(getString(R.string.network_exception));
                                    syncOrderState();
                                } else {
                                    refundYinshangNotify(mYinshangRefundBean);
                                }
                            });
                        }
                    } else {
                        showCommonNoticeDialog(CheckActivity.this, mYinshangRefundBean.getResultMsg(), () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                refundYinshangNotify(mYinshangRefundBean);
                            }
                        });
                    }

                } else {
                    showCommonNoticeDialog(CheckActivity.this, "退款回调数据有误，请确认结果！", mOnClickOkListener);
                }
            }
        };
        com.ums.AppHelper.callTrans(CheckActivity.this, transAppName, transAppId, transData, listener);
    }

    /**
     * 易生支付pos撤销刷卡订单
     *
     * @param dataBean
     */
    private void startActivityWizarposReverse(PayBean.DataBean dataBean) {
        Map<String, String> map = new HashMap<>();
        map.put("option", "refund");
        map.put("refundTrace", dataBean.getVoucherNo());
        startActivityForResult(map, Constants.REQUEST_BCARD_REVERSE);
    }

    /**
     * 易生支付pos退货
     *
     * @param dataBean
     */
    private void startActivityWizarposRefund(PayBean.DataBean dataBean, long refundMoney) {
        String refundMoneyString = new BigDecimal(refundMoney).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        Map<String, String> map = new HashMap<>();
        map.put("option", "cardRefund");
        map.put("amount", refundMoneyString);
        map.put("referenceNo", dataBean.getOriRefNo());
        LogUtil.d("referenceNo===" + dataBean.getOriRefNo() + ",," + DateUtil.formartToMMDD(dataBean.getTradeTime()));
        map.put("dateInfo", DateUtil.formartToMMDD(dataBean.getTradeTime()));
        startActivityForResult(map, Constants.REQUEST_BCARD_REFUND);
    }

    private void startActivityForResult(Map<String, String> map, int requestCode) {
        unbindDeviceService();
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("net.worthtech.worthcasher", "net.worthtech.worthcasher.activity.PayService"));
        for (Map.Entry<String, String> arg : map.entrySet()) {
            intent.putExtra(arg.getKey(), arg.getValue());
        }
        startActivityForResult(intent, requestCode);
    }

    /**
     * 唤起刷卡退款
     *
     * @param uriStr
     */
    private void startActivityRefund(String uriStr) {
        Uri uri = Uri.parse(uriStr);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivityForResult(intent, Constants.REQUEST_BCARD_REFUND);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_BCARD_REFUND || requestCode == Constants.REQUEST_BCARD_REVERSE) {
            if (data != null) {
                try {
                    LogUtil.e("REQUEST_BCARD_REFUND", URLDecoder.decode(data.toURI(), "UTF-8"));
                } catch (Exception e) {

                }
                String message;
                String responseCode;
                if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    mResultData = data;
                    message = data.getStringExtra("respmsg");
                    responseCode = data.getStringExtra("result");
                    LogUtil.e("REQUEST_BCARD_REFUND message==" + message + ",responseCode" + responseCode);

                    if (!"00".equals(responseCode)) {//撤销/退款成功
                        String resultDesc;
                        if (requestCode == Constants.REQUEST_BCARD_REVERSE) {
                            resultDesc = "撤销失败";
                        } else {
                            resultDesc = "退款失败";
                        }
                        if (!TextUtils.isEmpty(message))
                            resultDesc = message;
                        showCommonNoticeDialog(CheckActivity.this, resultDesc, () -> {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(CheckActivity.this, getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                if (requestCode == Constants.REQUEST_BCARD_REVERSE) {
                                    reverseEasyNotify(data);
                                } else {
                                    refundEasyNotify(data);
                                }
                            }
                        });
                    } else {
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(CheckActivity.this, getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            if (requestCode == Constants.REQUEST_BCARD_REVERSE) {
                                reverseEasyNotify(data);
                            } else {
                                refundEasyNotify(data);
                            }
                        }
                    }

                } else {
                    message = data.getStringExtra("message");
                    responseCode = data.getStringExtra("responseCode");
                    if (!"00".equals(responseCode)) {
                        showCommonNoticeDialog(CheckActivity.this, message, mOnClickOkListener);
                    } else {
                        startMainActivity();
                    }
                }
            } else {
                showCommonNoticeDialog(CheckActivity.this, "退款回调数据有误，请确认结果！", mOnClickOkListener);
            }
        }
    }

    /**
     * 易生刷卡撤销通知
     *
     * @param data
     */
    private void reverseEasyNotify(Intent data) {
        DialogUtil.safeShowDialog(mSyncLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("storeMerchantId", mData.getStoreMerchantId());
        map.put("outTradeNo", mData.getOutTradeNo());
        map.put("transactionId", data.getStringExtra("orderNo"));
        if ("00".equals(data.getStringExtra("result"))) {
            map.put("resultCode", "0");
        } else {
            map.put("resultCode", "1");
        }
        map.put("resultMsg", data.getStringExtra("respmsg"));
        map.put("refNo", data.getStringExtra("referenceNo"));
        map.put("voucherNo", data.getStringExtra("trace"));
        String printInfo = data.getStringExtra("printInfo");
        if (!TextUtils.isEmpty(printInfo)) {
            Gson gson = new Gson();
            EasypayInfo easypayInfo = gson.fromJson(printInfo, EasypayInfo.class);
            map.put("batchId", easypayInfo.getBatch_no());
        }
        ServerClient.newInstance(MyApplication.getContext()).reverseNotify(MyApplication.getContext(), Constants.NOTIFY_REVERSE_EASYPAY, map);
    }

    /**
     * 慧银pos福卡撤销通知
     *
     * @param bean
     */
    private void reverseWizardFukaNotify(FuxunBean bean) {
        if (bean != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (mData != null) {
                map.put("storeMerchantId", mData.getStoreMerchantId());
                map.put("outTradeNo", mData.getOutTradeNo());
                map.put("transactionId", mData.getOrderNo());
            }
            if ("00".equals(bean.getRespCode())) {
                map.put("resultCode", "0");
            } else {
                map.put("resultCode", "1");
            }
            map.put("resultMsg", bean.getRespDesc());
            map.put("refNo", bean.getReferCode());
            map.put("voucherNo", bean.getCertNum());
            map.put("batchId", bean.getBatchNum());
            ServerClient.newInstance(MyApplication.getContext()).reverseNotify(MyApplication.getContext(), Constants.NOTIFY_REVERSE_EASYPAY, map);
        }
    }

    /**
     * 银商刷卡撤销通知
     *
     * @param bean
     */
    private void reserveYinshangNotify(YinshangPosBean bean) {
        if (bean != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (mData != null) {
                map.put("transactionId", mData.getOrderNo());
                map.put("outTradeNo", mData.getOutTradeNo());
                map.put("storeMerchantId", mData.getStoreMerchantId());
            }
            if (bean.getTransData() == null) {
                map.put("resultCode", "1");
                map.put("resultMsg", bean.getResultMsg());
            } else {
                if ("00".equals(bean.getTransData().getResCode())) {
                    map.put("resultCode", "0");
                } else {
                    map.put("resultCode", "1");
                }
                map.put("resultMsg", bean.getTransData().getResDesc());
                map.put("refNo", bean.getTransData().getRefNo());
                map.put("voucherNo", bean.getTransData().getTraceNo());
                map.put("batchId", bean.getTransData().getBatchNo());
            }
            ServerClient.newInstance(MyApplication.getContext()).reverseNotify(MyApplication.getContext(), Constants.NOTIFY_REVERSE_EASYPAY, map);
        }
    }

    /**
     * 易生刷卡退货通知
     *
     * @param data
     */
    private void refundEasyNotify(Intent data) {
        DialogUtil.safeShowDialog(mSyncLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("storeMerchantId", mRefundData.getStoreMerchantId());
        map.put("outTradeNo", mRefundData.getOutTradeNo());
        map.put("transactionId", mRefundData.getOrderNo());
        map.put("outRefundNo", mRefundData.getOutRefundNo());
        map.put("refundId", mRefundData.getRefundNo());
        if ("00".equals(data.getStringExtra("result"))) {
            map.put("resultCode", "0");
        } else {
            map.put("resultCode", "1");
        }
        map.put("resultMsg", data.getStringExtra("respmsg"));
        map.put("voucherNo", data.getStringExtra("trace"));
//        map.put("batchId",);
        map.put("refNo", data.getStringExtra("referenceNo"));
        LogUtil.e("map====" + new Gson().toJson(map));
        ServerClient.newInstance(MyApplication.getContext()).refundNotify(MyApplication.getContext(), Constants.NOTIFY_REFUND_EASYPAY, map);
    }

    /**
     * 银商pos刷卡退货通知
     *
     * @param bean
     */
    private void refundYinshangNotify(YinshangPosBean bean) {
        if (bean != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (mData != null) {
                map.put("transactionId", mData.getOrderNo());
                map.put("outTradeNo", mData.getOutTradeNo());
                map.put("storeMerchantId", mData.getStoreMerchantId());
                map.put("outRefundNo", mRefundData.getOutRefundNo());
                map.put("refundId", mRefundData.getRefundNo());
            }
            if (bean.getTransData() == null) {
                map.put("resultCode", "1");
                map.put("resultMsg", bean.getResultMsg());
            } else {
                if ("00".equals(bean.getTransData().getResCode())) {
                    map.put("resultCode", "0");
                } else {
                    map.put("resultCode", "1");
                }
                map.put("resultMsg", bean.getTransData().getResDesc());
                map.put("refNo", bean.getTransData().getRefNo());
                map.put("voucherNo", bean.getTransData().getTraceNo());
            }
            ServerClient.newInstance(MyApplication.getContext()).refundNotify(MyApplication.getContext(), Constants.NOTIFY_REFUND_EASYPAY, map);
        }
    }

    /**
     * 慧银pos福卡退货通知
     *
     * @param bean
     */
    private void refundWizardFukaNotify(FuxunBean bean) {
        if (bean != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (mData != null) {
                map.put("transactionId", mData.getOrderNo());
                map.put("outTradeNo", mData.getOutTradeNo());
                map.put("storeMerchantId", mData.getStoreMerchantId());
                map.put("outRefundNo", mRefundData.getOutRefundNo());
                map.put("refundId", mRefundData.getRefundNo());
            }

            if ("00".equals(bean.getRespCode())) {
                map.put("resultCode", "0");
            } else {
                map.put("resultCode", "1");
            }
            map.put("resultMsg", bean.getRespDesc());
            map.put("refNo", bean.getReferCode());
            map.put("voucherNo", bean.getCertNum());
            map.put("batchId", bean.getBatchNum());
            ServerClient.newInstance(MyApplication.getContext()).refundNotify(MyApplication.getContext(), Constants.NOTIFY_REFUND_EASYPAY, map);
        }
    }

    /**
     * 银联刷脸退款
     */
    private void unionPayRefundNotify(ResponsePojo response) {
        if (response != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> parameter = new HashMap<>();
            parameter.put("storeMerchantId", mRefundData.getStoreMerchantId());
            parameter.put("outTradeNo", mRefundData.getOutTradeNo());
            parameter.put("transactionId", mRefundData.getOrderNo());
            parameter.put("outRefundNo", mRefundData.getOutRefundNo());
            parameter.put("refundId", mRefundData.getRefundNo());
            if ("00".equals(response.getRspCode())) {
                parameter.put("resultCode", "0");
                parameter.put("resultMsg", "OK");
            } else {
                parameter.put("resultMsg", response.getRspChin());
                parameter.put("resultCode", "1");
            }
            parameter.put("voucherNo", response.getTraceNo());
            parameter.put("refNo", response.getRefNo());
            ServerClient.newInstance(MyApplication.getContext()).refundNotify(MyApplication.getContext(), Constants.NOTIFY_REFUND_EASYPAY, parameter);
        }
    }

    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboard.isShow()) {
                safeKeyboard.hideKeyboard();
                return false;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        /*if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))
            unbindService();*/
        if (safeKeyboard != null) {
            safeKeyboard.release();
            safeKeyboard = null;
        }
        super.onDestroy();
    }


    private long timeCount = 5;
    private int count = 1;
    private String dialogMessage = null;
    private NoticeDialog showDialog;

    private void syncOrderState() {
        if (count < 5) {
            timeCount = 5;
            count++;
            showDialog = new NoticeDialog(CheckActivity.this, dialogMessage, "取消同步", R.layout.notice_dialog_check_order);
            showDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    cancelTask = true;
                    toSyncOrderStatus();
                    mHandler.removeCallbacks(myRunnable);
                }
            });
            DialogHelper.resize(CheckActivity.this, showDialog);
            showDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            cancelTask = true;
            toSyncOrderStatus();
        }
    }

    private void toSyncOrderStatus() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            if (cancelTask) {
                showSyncResult();
            } else {
                syncOrderState();
            }
        } else {
            switch (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) {
                case Constants.WJY_YINSHANG:
                    if (mData.getReverseFlag() == 1) {//撤销
                        if (mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {//银联刷脸撤销通知
                            unionPayReverseNotify(mResponsePojo);
                        } else {
                            reserveYinshangNotify(mYinshangReverseBean);
                        }
                    } else {
                        if (mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {//银联刷脸退款通知
                            unionPayRefundNotify(mResponsePojo);
                        } else {
                            refundYinshangNotify(mYinshangReverseBean);
                        }
                    }
                    break;
                case Constants.WIZARPOS:
                case Constants.LIANDI:
                case Constants.MIBAGPAY_LIANDI:
                    if (mData.getReverseFlag() == 1) {//撤销
                        reverseEasyNotify(mResultData);
                    } else {
                        refundEasyNotify(mResultData);
                    }
                    break;
                case Constants.WJY_WIZARPOS:
                    if (mData.getApiProvider() == 11) {//福卡
                        if (mData.getReverseFlag() == 1) {//撤销
                            reverseWizardFukaNotify(mFuxunBean);
                        } else {
                            refundWizardFukaNotify(mFuxunBean);
                        }
                    } else {
                        if (mData.getReverseFlag() == 1) {//撤销
                            reverseEasyNotify(mResultData);
                        } else {
                            refundEasyNotify(mResultData);
                        }
                    }
            }
        }
    }

    private void showSyncResult() {
        mSyncDialog = new SelectDialog(CheckActivity.this, "订单状态同步失败", "取消", "继续同步", R.layout.select_common_dialog);
        mSyncDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                cancelTask = true;
                toSyncOrderStatus();
            }
        });
        mSyncDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                startActivity(new Intent(CheckActivity.this, MainActivity.class));
                finish();
            }
        });
        DialogHelper.resize(CheckActivity.this, mSyncDialog);
        mSyncDialog.show();
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && showDialog != null) {
                showDialog.setMessage(getString(R.string.dialog_order_sync) + timeCount
                        + getString(R.string.dialog_sync_start) + (count) + getString(R.string.dialog_sync_end));
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (showDialog != null && showDialog.isShowing()) {
                    showDialog.dismiss();
                }
                if (count == 5)
                    cancelTask = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toSyncOrderStatus();
                    }
                });
            }
        }
    };

    private void remove() {
        if (showDialog != null) {
            showDialog.dismiss();
        }
        if (mSyncDialog != null) {
            mSyncDialog.dismiss();
        }
        mHandler.removeCallbacks(myRunnable);
    }

    /**
     * 福卡签到
     */
    private void wizardFukaSign() {
        Map<String, Object> map = new HashMap<>();
        map.put("AppID", Constants.WJYS_APPID);
        map.put("AppName", "WJYS");
        map.put("OptCode", "01");
        map.put("OptPass", "0000");
        try {
            String login =  MainActivity.instance.getCupService().login(new Gson().toJson(map));
            LogUtil.d("Jeremy—login==" + login);
            FuxunBean fuxunBean = new Gson().fromJson(login, FuxunBean.class);
            if (fuxunBean != null) {
                if ("00".equals(fuxunBean.getRespCode())) {//签到成功
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE, DateUtil.getDate());
                    submit();
                } else {
                    showCommonNoticeDialog(this, TextUtils.isEmpty(fuxunBean.getRespDesc()) ? "签到失败！" : fuxunBean.getRespDesc());
                }
            } else {
                showCommonNoticeDialog(this, "签到失败！");
            }
        } catch (Exception e) {
            showCommonNoticeDialog(this, "签到失败！");
            e.printStackTrace();
        }
    }
}
