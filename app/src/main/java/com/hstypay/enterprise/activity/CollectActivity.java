package com.hstypay.enterprise.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.CollectTypeAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//没用了的页面 用CollectActivity1
public class CollectActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private LinearLayout mLlData, mLlShop, mLlSalesman;
    private TextView mTvToal, mTvToalPay, mTvToalPayNum, mTvToalRefund, mTvToalRefundNum, mTvVipRechargeTotal, mTvVipRechargeCount, mTvCollectRule, mTvMoneyTitle, mTvCountTitle, mTvVipCount;
    private ImageView mIvBack, mIvCashierArrow, mIvShopArrow;
    private String startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
    private String endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
    private String userId;
    private String storeMerchantId;
    private List<ReportBean.DataEntity.ListEntity> mList;
    private int tab = 1;
    private String storeName;
    private String cashierName;

    private TextView tv_date, mTvNull, mTvShop, mTvCashier, mTvNotice;
    private Button mBtnPrint;
    private ScrollView mSvReport;
    private RecyclerView mRecyclerView;
    private CollectTypeAdapter mAdapter;
    private ReportBean.DataEntity mInfo;
    private ReportBroadcastReceiver mReportBroadcastReceiver;
    private RadioGroup mRgTitle;
    private LinearLayout mLlVipRechargeData, mLlVipCount, mLlVerificationTilte;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLlData = (LinearLayout) findViewById(R.id.ll_date);
        mLlShop = (LinearLayout) findViewById(R.id.ll_shop);
        mTvShop = (TextView) findViewById(R.id.tv_shop);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);
        mLlSalesman = (LinearLayout) findViewById(R.id.ll_cashier);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + getString(R.string.public_today) + ")");

        mTvToal = (TextView) findViewById(R.id.tv_total);//总金额
        mTvToalPay = (TextView) findViewById(R.id.tv_total_pay);//总收款金额
        mTvToalPayNum = (TextView) findViewById(R.id.tv_order_pay_num);//总收款笔数
        mTvToalRefund = (TextView) findViewById(R.id.tv_refund_total);//总退款金额
        mTvToalRefundNum = (TextView) findViewById(R.id.tv_refund_total_num);//总退款笔数

        mBtnPrint = (Button) findViewById(R.id.btn_print);
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mSvReport = (ScrollView) findViewById(R.id.sv_report);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mSvReport.setVisibility(View.GONE);
        mTvNull.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mIvCashierArrow = (ImageView) findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = (ImageView) findViewById(R.id.iv_shop_arrow);

        mRgTitle = (RadioGroup) findViewById(R.id.rg_title);

        mLlVerificationTilte = (LinearLayout) findViewById(R.id.ll_verification_title);
        //TODO 更改提示语
        mTvNotice = (TextView) findViewById(R.id.tv_notice);
        mLlVipRechargeData = (LinearLayout) findViewById(R.id.ll_vip_recharge_data);
        mTvVipRechargeTotal = (TextView) findViewById(R.id.tv_vip_recharge_total);
        mTvVipRechargeCount = (TextView) findViewById(R.id.tv_vip_recharge_num);
        mTvCollectRule = (TextView) findViewById(R.id.tv_collect_rule);
        mTvMoneyTitle = (TextView) findViewById(R.id.tv_total_money_title);
        mTvCountTitle = (TextView) findViewById(R.id.tv_total_count_title);

        mLlVipCount = (LinearLayout) findViewById(R.id.ll_vip_count);
        mTvVipCount = (TextView) findViewById(R.id.tv_vip_count);

        if (MyApplication.getIsCasher()) {
            mIvShopArrow.setVisibility(View.GONE);
            storeMerchantId = MyApplication.getDefaultStore();
            mLlShop.setEnabled(false);
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText(getString(R.string.tv_shop));
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            if (TextUtils.isEmpty(MyApplication.getOpCodeReport())) {
                mLlSalesman.setEnabled(false);
                mIvCashierArrow.setVisibility(View.GONE);
                mTvCashier.setText(MyApplication.getRealName());
                userId = MyApplication.getUserId();
            }
        }/*else{
            mLlSalesman.setVisibility(View.VISIBLE);
        }*/
    }

    public void initEvent() {
        mLlData.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlSalesman.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mRgTitle.setOnCheckedChangeListener(this);
    }

    public void initData() {
        storeName = getString(R.string.tv_shop);
        cashierName = getString(R.string.tv_all_user);
        mList = new ArrayList<>();
        mAdapter = new CollectTypeAdapter(mList);
        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        //HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_USER, handler);
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            getReport(getRequestMap(startTime, endTime, userId, null));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_REPORT)) {
            ReportBean msg = (ReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CollectActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_REPORT_TRUE://请求成功
                    if (MyApplication.getIsCasher()) {
                        sendTodayDataBroadcast(this);
                    }
                    if (msg.getData() != null) {
                        mInfo = msg.getData();
                        mInfo.setType(tab);
                        mInfo.setCashierName(cashierName);
                        mInfo.setStoreName(storeName);
                        if (msg.getData().getList() != null && msg.getData().getList().size() > 0) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mList.clear();
                            mList.addAll(msg.getData().getList());
                            Collections.sort(mList);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                        }
                        mTvNull.setVisibility(View.GONE);
                        mSvReport.setVisibility(View.VISIBLE);
                        setView(msg.getData());
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSvReport.setVisibility(View.GONE);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("tab", tab);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }

    public void getReport(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            ServerClient.newInstance(MyApplication.getContext()).getReport(MyApplication.getContext(), Constants.TAG_GET_REPORT, map);
        } else {
            //MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            mTvNull.setText("网络异常");
            mTvNull.setVisibility(View.VISIBLE);
            mSvReport.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissLoading();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back://返回按钮
                finish();
                break;
            case R.id.ll_date:
                MtaUtils.mtaId(CollectActivity.this, "D003");
                Intent intentDate = new Intent(CollectActivity.this, DateChoiceActivity.class);
                intentDate.putExtra(Constants.INTENT_NAME, Constants.INTENT_REPORT_CHOICE_DATE);
                startActivityForResult(intentDate, Constants.REQUEST_CHOICE_DATE);
                break;
            case R.id.ll_shop:
                MtaUtils.mtaId(CollectActivity.this, "D004");
                Intent intentShop = new Intent(this, ShopActivity.class);
                if (TextUtils.isEmpty(storeMerchantId)) {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                } else {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                }
                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_COLLECT_SHOP);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.ll_cashier:
                MtaUtils.mtaId(CollectActivity.this, "D005");
                Intent intentCashier = new Intent(this, CashierActivity.class);
                if (TextUtils.isEmpty(userId)) {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                } else {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                }
                intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                break;
            case R.id.btn_print:
                MtaUtils.mtaId(CollectActivity.this, "D006");
                mInfo.setStoreMerchantId(storeMerchantId);
                bluetoothPrint();
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeName = shopBean.getStoreName();
            storeMerchantId = shopBean.getStoreId();
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText("全部收银员");
                cashierName = "全部收银员";
                userId = "";
            }

            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            /*if (isToday(endTime)) {
                getDialog(UIUtils.getString(R.string.toast_report_later));
            }*/
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            /*if (isToday(endTime)) {
                getDialog(UIUtils.getString(R.string.toast_report_later));
            }*/
            cashierName = cashierBean.getRealName();
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHOICE_DATE) {
            Bundle extras = data.getExtras();
            String time = extras.getString(Constants.RESULT_CHOICE_DATE_INTENT);
            //String time = (String) msg.obj;
            if (!StringUtils.isEmptyOrNull(time)) {
                String t[] = time.split("\\|");
                try {
                    //LogUtil.d("date1="+DateUtil.formartDateToMMDD(t[0]) + "///" + DateUtil.formartDateToMMDD(t[1])+"///"+DateUtil.formatMD(System.currentTimeMillis()));
                    if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formartDateToMMDD(t[1]))) {
                        if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                            tv_date.setText(DateUtil.formartDateToMMDD(t[0]) + "(今天)");
                        } else {
                            tv_date.setText(DateUtil.formartDateToMMDD(t[0]));
                        }
                    } else {
                        tv_date.setText(DateUtil.formartDateToMMDD(t[0]) + " " + getString(R.string.tv_least) + " "
                                + DateUtil.formartDateToMMDD(t[1]));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                startTime = t[0];
                endTime = t[1];
                LogUtil.d("date=" + startTime + "///" + endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        }
    }

    private void bluetoothPrint() {
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            showPrintDialog(1);
            return;
        }
        if (mInfo != null) {
            if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                mInfo.setStartTime(startTime);
                mInfo.setEndTime(endTime);
            } else {
                mInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                mInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
            mInfo.setCashierName(mTvCashier.getText().toString().trim());
            mInfo.setStoreName(mTvShop.getText().toString().trim());

            if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean isSucc = BluePrintUtil.blueConnent(1, CollectActivity.this);
                        if (isSucc) {
                            BluePrintUtil.printDateSum(mInfo);
                        }else {
                            if (BluetoothAdapter.getDefaultAdapter().isEnabled()){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showPrintDialog(1);
                                    }
                                });
                            }else {
                                //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                                LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                            }
                        }
                    }
                });

            } else {
                showPrintDialog(1);
            }
        } else {
            showCommonNoticeDialog(CollectActivity.this, getString(R.string.print_data_error));
        }
    }

    public void setInitView(int tab) {
        if (tab == 2) {
            mLlVipCount.setVisibility(View.VISIBLE);
            mLlVipRechargeData.setVisibility(View.VISIBLE);
            mLlVerificationTilte.setVisibility(View.VISIBLE);
            mTvNotice.setVisibility(View.VISIBLE);
            //mTvCollectRule.setText(getString(R.string.tv_collect_vip_info));
            //mTvMoneyTitle.setText(getString(R.string.tx_vip_consume_money));
            //mTvCountTitle.setText(getString(R.string.tx_vip_consume_count));
        } else {
            mLlVipCount.setVisibility(View.GONE);
            mLlVipRechargeData.setVisibility(View.GONE);
            mLlVerificationTilte.setVisibility(View.GONE);
            mTvNotice.setVisibility(View.GONE);
            //mTvCollectRule.setText(getString(R.string.tv_bill_refund_info));
            //mTvMoneyTitle.setText(getString(R.string.tx_report_pay_money));
            //mTvCountTitle.setText(getString(R.string.tx_report_stream_pay_money_num));
        }
        mTvToal.setText("0.00");
        mTvToalPay.setText("0.00");
        mTvToalPayNum.setText("0");
        mTvToalRefund.setText("0.00");
        mTvToalRefundNum.setText("0");
        mTvVipRechargeTotal.setText("0.00");
        mTvVipRechargeCount.setText("0");
        mTvVipCount.setText("笔数：0笔");
    }

    public void setView(ReportBean.DataEntity dataEntity) {
        if (dataEntity != null) {
            if (tab == 2) {
                mTvVipRechargeTotal.setText(DateUtil.formatMoneyUtil(dataEntity.getRechargeAmount() / 100d));
                mTvVipRechargeCount.setText(dataEntity.getRechargeNumber() + "");
                mTvVipCount.setText("次数：" + dataEntity.getNumberPaymentCount() + "次");
            }
//            mTvToal.setText(DateUtil.formatMoneyUtil((dataEntity.getSuccessFee() - dataEntity.getRefundFee()) / 100d));
            mTvToal.setText(DateUtil.formatMoneyUtil((dataEntity.getIncome()) / 100d));
            mTvToalPay.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
            mTvToalPayNum.setText(dataEntity.getSuccessCount() + "");
            mTvToalRefund.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));
            mTvToalRefundNum.setText(dataEntity.getRefundCount() + "");
        }
    }

    public boolean isToday(String time) {
        boolean today = false;
        try {
            if (DateUtil.formartDateToMMDD(time).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                today = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return today;
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_REPORT_DATA);
        mReportBroadcastReceiver = new ReportBroadcastReceiver();
        registerReceiver(mReportBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mReportBroadcastReceiver);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_title_pay:
                //mLlReportPay.setVisibility(View.VISIBLE);
                //mLlReportVerify.setVisibility(View.GONE);
                mTvCollectRule.setText(getString(R.string.tv_bill_refund_info));
                mTvMoneyTitle.setText(getString(R.string.tx_report_pay_money));
                mTvCountTitle.setText(getString(R.string.tx_report_stream_pay_money_num));
                tab = 1;
                setInitView(tab);
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    showNewLoading(true, getString(R.string.public_loading));
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;
            case R.id.rb_title_verify:
                MtaUtils.mtaId(CollectActivity.this, "D008");
                //mLlReportPay.setVisibility(View.GONE);
                //mLlReportVerify.setVisibility(View.VISIBLE);
                mTvCollectRule.setText(getString(R.string.tv_collect_vip_info));
                mTvMoneyTitle.setText(getString(R.string.tx_vip_consume_money));
                mTvCountTitle.setText(getString(R.string.tx_vip_consume_count));
                tab = 2;
                setInitView(tab);
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    showNewLoading(true, getString(R.string.public_loading));
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;
        }
    }

    class ReportBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        }
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }
}
