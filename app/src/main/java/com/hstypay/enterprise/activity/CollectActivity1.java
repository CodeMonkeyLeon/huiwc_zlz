package com.hstypay.enterprise.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.DateStartEndPopupWindow;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.FilterCashierPopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.dialog.CollectHelpDialog;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.CollectTypeAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.paySite.PaySiteChoiceAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.bean.paySite.PaySiteBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;
import com.hstypay.enterprise.utils.print.CloudPrintUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectActivity1 extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private LinearLayout mLlDate, mLlShop, mLlSalesman, mLlSite, mShopPopLayout, mCashierPopLayout, mSitePopLayout, mLlCommissionFee;
    private TextView mTvTitle, mTvTotal, mTvTotalPay, mTvTotalPayNum, mTvRealMoney, mTvPayCouponMoney, mTvTotalRefund, mTvTotalRefundNum, mTvNotice, mTvSite;
    private TextView mTvVerifyActuralMoney, mTvVerifyCouponMoney, mTvVerifyRefund, mTvVerifyRefundNum, mTvVerifyMoney, mTvVerifyTotalMoney, mTvVerifyTotalCount;
    private ImageView mIvBack, mIvCashierArrow, mIvShopArrow, mIvSiteArrow, mIvFilter;
    private String startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
    private String endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
    private String userId;
    private String storeMerchantId;
    private List<ReportBean.DataEntity.ListEntity> mList;
    private int tab = 1;
    private String storeName;
    private String cashierName;

    private TextView tv_date, mTvNull, mTvShop, mTvCashier, mTvShopNull;
    private Button mBtnPrint;
    private ScrollView mSvReport;
    private RecyclerView mRecyclerView, mStoreRecyclerView, mCashierRecyclerView, mSiteRecyclerView;
    private CollectTypeAdapter mAdapter;
    private ReportBean.DataEntity mInfo;
    private ReportBroadcastReceiver mReportBroadcastReceiver;
    private RadioGroup mRgTitle;
    private LinearLayout mLlReportPay;
    private SafeDialog mLoadDialog;
    private String mShopType;
    private RadioGroup mRgType;
    private RadioButton mRbAllStore, mRbDirectStore, mRbJoinStore;
    private EditTextDelete mEtStoreInput, mEtCashierInput, mEtSiteInput;
    private ShopRecyclerAdapter mStoreAdapter;
    private CashierRecyclerAdapter mCashierAdapter;
    private boolean mShopSwitchArrow, mCashierSwitchArrow, mSiteSwitchArrow;
    private Animation rotate;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DataEntity> mCashierList;
    private List<DataEntity> mOriginCashierList;
    private List<PaySiteBean> mSiteList;
    private List<PaySiteBean> mOriginSiteList;
    private PaySiteChoiceAdapter mSiteAdapter;
    private String mSiteId = "";
    private FilterCashierPopupWindow popupWindow;

    private TextView mTvCommissionFee, mTvSettlementFee, mTvCommissionTitle;
    private LinearLayout mLlJsLayout;
    private View mLayoutVipTab;//会员消费layout
    private View mLayoutFkTab;//饭卡消费layout
    private TextView mTvFkSettlementMoney;//饭卡结算金额
    private TextView mTvFkPayMoney;//饭卡交易金额
    private TextView mTvFkPayCount;//饭卡交易笔数
    private TextView mTvFkSettlementRate;//饭卡结算率
    private TextView mTvFkSettlementMoneyContent;//饭卡结算金额
    private TextView mTvFkRefundTotalCount;//饭卡退款笔数
    private TextView mTvFkRefundTotalMoney;//饭卡退款金额
    private TextView mTvFkSettlementTopTitle;//饭卡结算金额顶部标题

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect1);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mLlDate = (LinearLayout) findViewById(R.id.ll_date);
        mLlShop = (LinearLayout) findViewById(R.id.ll_shop);
        mTvShop = (TextView) findViewById(R.id.tv_shop);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);
        mTvNotice = (TextView) findViewById(R.id.tv_notice);
        mLlSalesman = (LinearLayout) findViewById(R.id.ll_cashier);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()));
        mLlSite = (LinearLayout) findViewById(R.id.ll_pay_site);
        mTvSite = (TextView) findViewById(R.id.tv_pay_site);
        mIvSiteArrow = (ImageView) findViewById(R.id.iv_pay_site_arrow);
        mIvFilter = (ImageView) findViewById(R.id.iv_filter);

        //收款：
        mTvTotal = (TextView) findViewById(R.id.tv_total);//总金额、收款的交易净额
        mTvTotalPay = (TextView) findViewById(R.id.tv_total_pay);//总收款金额、收款的交易金额
        mTvTotalPayNum = (TextView) findViewById(R.id.tv_order_pay_num);//总收款笔数、收款的交易笔数
        mTvRealMoney = (TextView) findViewById(R.id.tv_real_money);//实收金额
        mTvPayCouponMoney = (TextView) findViewById(R.id.tv_merchant_coupon_money);//商家优惠金额
        mTvTotalRefund = (TextView) findViewById(R.id.tv_refund_total);//总退款金额
        mTvTotalRefundNum = (TextView) findViewById(R.id.tv_refund_total_num);//总退款笔数
        mTvCommissionFee = (TextView) findViewById(R.id.tvCommissionFee);//手续费
        mTvSettlementFee = (TextView) findViewById(R.id.tvSettlementFee);//结算金额
        mLlCommissionFee = findViewById(R.id.llCommissionFee);//ll手续费
        mTvCommissionTitle = findViewById(R.id.tv_commission_title);//手续费标题
        mLlJsLayout = findViewById(R.id.ll_js_layout);//ll结算金额

        //会员消费：
        mTvVerifyActuralMoney = (TextView) findViewById(R.id.tv_verify_total);//核销净额
        mTvVerifyTotalMoney = (TextView) findViewById(R.id.tv_verify_total_pay);//核销交易金额
        mTvVerifyTotalCount = (TextView) findViewById(R.id.tv_verify_pay_num);//核销交易笔数
        mTvVerifyMoney = (TextView) findViewById(R.id.tv_report_verify_money);//核销实际金额
        mTvVerifyCouponMoney = (TextView) findViewById(R.id.tv_verify_merchant_coupon_money);//会员卡商家优惠金额
        mTvVerifyRefund = (TextView) findViewById(R.id.tv_verify_refund_total);//核销退款总金额
        mTvVerifyRefundNum = (TextView) findViewById(R.id.tv_verify_refund_total_num);//核销退款总笔数

        //饭卡：
        mTvFkSettlementMoney = findViewById(R.id.tv_fk_settlement_money);//饭卡结算金额
        mTvFkPayCount = findViewById(R.id.tv_fk_pay_count);//饭卡交易金额
        mTvFkPayMoney = findViewById(R.id.tv_fk_pay_money);//饭卡交易笔数
        mTvFkSettlementRate = findViewById(R.id.tv_fk_settlement_rate);//饭卡结算率
        mTvFkSettlementMoneyContent = findViewById(R.id.tv_fk_settlement_money_content);//饭卡结算金额
        mTvFkRefundTotalCount = findViewById(R.id.tv_fk_refund_total_count);//饭卡退款笔数
        mTvFkRefundTotalMoney = findViewById(R.id.tv_fk_refund_total_money);//饭卡退款金额
        mTvFkSettlementTopTitle = findViewById(R.id.tv_fk_settlement_top_title);//饭卡结算金额顶部标题

        mBtnPrint = findViewById(R.id.btn_print);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);

        mSvReport = (ScrollView) findViewById(R.id.sv_report);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mSvReport.setVisibility(View.GONE);
        mTvNull.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mIvCashierArrow = (ImageView) findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = (ImageView) findViewById(R.id.iv_shop_arrow);

        mRgTitle = (RadioGroup) findViewById(R.id.rg_title);
        mLlReportPay = (LinearLayout) findViewById(R.id.ll_report_pay);
        mLayoutVipTab = findViewById(R.id.layout_vip_tab);
        mLayoutFkTab = findViewById(R.id.layout_fk_tab);

        mRgType = findViewById(R.id.rg_type);
        mRbAllStore = findViewById(R.id.rb_all_store);
        mRbDirectStore = findViewById(R.id.rb_direct_store);
        mRbJoinStore = findViewById(R.id.rb_join_store);
        mShopPopLayout = findViewById(R.id.shop_pop_layout);
        mEtStoreInput = findViewById(R.id.et_store_input);
        mTvShopNull = findViewById(R.id.tv_shop_null);
        mStoreRecyclerView = findViewById(R.id.recyclerView_store);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(this);
        mStoreRecyclerView.setLayoutManager(storeLinearLayoutManager);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);

        mCashierPopLayout = findViewById(R.id.cashier_pop_layout);
        mEtCashierInput = findViewById(R.id.et_cashier_input);
        CustomLinearLayoutManager cashierLinearLayoutManager = new CustomLinearLayoutManager(this);
        mCashierRecyclerView = findViewById(R.id.recyclerView_cashier);
        mCashierRecyclerView.setLayoutManager(cashierLinearLayoutManager);
        mEtCashierInput.setClearImage(R.mipmap.ic_search_clear);

        mSitePopLayout = findViewById(R.id.site_pop_layout);
        mEtSiteInput = findViewById(R.id.et_site_input);
        CustomLinearLayoutManager siteLinearLayoutManager = new CustomLinearLayoutManager(this);
        mSiteRecyclerView = findViewById(R.id.recyclerView_site);
        mSiteRecyclerView.setLayoutManager(siteLinearLayoutManager);
        mEtSiteInput.setClearImage(R.mipmap.ic_search_clear);

        if (MyApplication.getIsCasher()) {
            mIvShopArrow.setVisibility(View.GONE);
            storeMerchantId = MyApplication.getDefaultStore();
            mLlShop.setEnabled(false);
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            if (TextUtils.isEmpty(MyApplication.getOpCodeReport())) {
                mLlSalesman.setEnabled(false);
                mIvCashierArrow.setVisibility(View.GONE);
            }
            mTvCashier.setText(MyApplication.getRealName());
//            mLlCommissionFee.setVisibility(View.GONE);
            setSettlementAndCommitFeeVisible(false);
            userId = MyApplication.getUserId();
            cashierName = MyApplication.getRealName();
        } else {
            storeMerchantId = "";
            userId = "";
            cashierName = getString(R.string.tv_all_user);
        }
        //TODO 下一期加提示
        if ("12".equals(MyApplication.getMerchantType())) {
            mTvNotice.setVisibility(View.GONE);
        }
        mTvTitle.setText(getString(R.string.tv_report));
        setButtonWhite(mBtnPrint);

        mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);

    }

    public void initEvent() {
        mLlDate.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlSalesman.setOnClickListener(this);
        mLlSite.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mIvFilter.setOnClickListener(this);
        mRgTitle.setOnCheckedChangeListener(this);

        mRbAllStore.setOnClickListener(this);
        mRbDirectStore.setOnClickListener(this);
        mRbJoinStore.setOnClickListener(this);

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtCashierInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData(mEtCashierInput.getText().toString().trim());
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtCashierInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData("");
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtSiteInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setSiteData(mEtSiteInput.getText().toString().trim());
                        if (mSiteAdapter != null)
                            mSiteAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtSiteInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setSiteData("");
                        if (mSiteAdapter != null)
                            mSiteAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });

        findViewById(R.id.tv_title_click_js).setOnClickListener(this);
        findViewById(R.id.tv_title_click_yh).setOnClickListener(this);
        findViewById(R.id.tv_title_click_shishou).setOnClickListener(this);
        findViewById(R.id.tv_title_click_tk).setOnClickListener(this);
        findViewById(R.id.tv_title_click_jyze).setOnClickListener(this);
        mTvCommissionTitle.setOnClickListener(this);
        mTvFkSettlementTopTitle.setOnClickListener(this);
    }

    public void initData() {
        mShopType = "";
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mCashierList = new ArrayList<>();
        mOriginCashierList = new ArrayList<>();
        mSiteList = new ArrayList<>();
        mOriginSiteList = new ArrayList<>();
        mList = new ArrayList<>();
        mAdapter = new CollectTypeAdapter(mList);
        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

//        mRgTitle.setVisibility(MyApplication.showCollectMemberData() ? View.VISIBLE : View.GONE);
        setTabVisible(MyApplication.isOpenMember(),MyApplication.isOpenFuncardVerification());
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            getReport(getRequestMap(startTime, endTime, userId, null));
            ServerClient.newInstance(this).isOpenMember(this, Constants.TAG_COLLECT_MEMBER, null);
            ServerClient.newInstance(this).openFuncardVerification(this, Constants.TAG_COLLECT_FUNCARD, null);

        }
    }

    /**
     * 设置tab可见否
     * @param vipTabVisible 会员消费tab是否可见
     * @param fkTabVisible 饭卡消费tab是否可见
     */
    private void setTabVisible(boolean vipTabVisible,boolean fkTabVisible){
        if (!vipTabVisible && !fkTabVisible){
            mRgTitle.setVisibility(View.GONE);
        }else {
            mRgTitle.setVisibility(View.VISIBLE);
            findViewById(R.id.rb_title_verify).setVisibility(vipTabVisible?View.VISIBLE:View.GONE);
            findViewById(R.id.rb_title_fun_card).setVisibility(fkTabVisible?View.VISIBLE:View.GONE);
        }
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                mRgType.setVisibility(View.VISIBLE);
            }
            mShopList.addAll(mOriginShopList);
        } else {
            mRgType.setVisibility(View.GONE);
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    private void setCashierData(String search) {
        mCashierList.clear();
        if (TextUtils.isEmpty(search)) {
            mCashierList.addAll(mOriginCashierList);
        } else {
            for (int i = 0; i < mOriginCashierList.size(); i++) {
                if (mOriginCashierList.get(i).getRealName().contains(search)) {
                    mCashierList.add(mOriginCashierList.get(i));
                }
            }
        }
    }

    private void setSiteData(String search) {
        mSiteList.clear();
        if (TextUtils.isEmpty(search)) {
            mSiteList.addAll(mOriginSiteList);
        } else {
            for (int i = 0; i < mOriginSiteList.size(); i++) {
                if (mOriginSiteList.get(i).getCashPointName().contains(search)) {
                    mSiteList.add(mOriginSiteList.get(i));
                }
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_REPORT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportBean msg = (ReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CollectActivity1.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_REPORT_TRUE://请求成功
                    if (MyApplication.getIsCasher()) {
                        sendTodayDataBroadcast(this);
                    }
                    if (msg.getData() != null) {
                        mInfo = msg.getData();
                        mInfo.setType(tab);
                        mInfo.setCashierName(cashierName);
                        mInfo.setStoreName(storeName);
                        if (msg.getData().getList() != null && msg.getData().getList().size() > 0) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mList.clear();
                            mList.addAll(msg.getData().getList());
                            Collections.sort(mList);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                        }
                        mTvNull.setVisibility(View.GONE);
                        mSvReport.setVisibility(View.VISIBLE);
                        setView(msg.getData());
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSvReport.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        if (mRbAllStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_shop));
                            dataEntity.setStoreId("");
                            mOriginShopList.clear();
                            mOriginShopList.add(dataEntity);
                            mOriginShopList.addAll(msg.getData());
                            mShopList.addAll(mOriginShopList);
                        } else if (mRbDirectStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_direct_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        } else if (mRbJoinStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_join_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        }
                        mStoreAdapter = new ShopRecyclerAdapter(this, mShopList, storeMerchantId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                if (mRgType.getVisibility() == View.VISIBLE) {
                                    switch (mRgType.getCheckedRadioButtonId()) {
                                        case R.id.rb_all_store:
                                            mShopType = "";
                                            break;
                                        case R.id.rb_direct_store:
                                            mShopType = "21";
                                            break;
                                        case R.id.rb_join_store:
                                            mShopType = "22";
                                            break;
                                    }
                                } else {
                                    mShopType = "";
                                }
                                storeMerchantId = mShopList.get(position).getStoreId();
                                mTvShop.setText(mShopList.get(position).getStoreName());
                                if (!MyApplication.getIsCasher()) {
                                    mTvCashier.setText(getString(R.string.tv_all_user));
                                    cashierName = getString(R.string.tv_all_user);
                                    userId = "";
                                } else {
                                    mTvCashier.setText(MyApplication.getRealName());
                                    cashierName = MyApplication.getRealName();
                                    userId = MyApplication.getUserId();
                                }
                                mTvSite.setText(getString(R.string.tv_all_site));
                                mSiteId = "";
                                closeArrow(1);
                                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    DialogUtil.safeShowDialog(mLoadDialog);
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                }
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        mTvShopNull.setVisibility(View.GONE);
                        mStoreRecyclerView.setVisibility(View.VISIBLE);
                    } else {
                        mTvShopNull.setVisibility(View.VISIBLE);
                        mStoreRecyclerView.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_CASHIER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.ERROR_CODE_NO_RIGH)) {
                                DataEntity dataEntity = new DataEntity();
                                dataEntity.setRealName(getString(R.string.tv_all_user));
                                dataEntity.setUserId("");
                                mOriginCashierList.add(dataEntity);
                                mCashierList.addAll(mOriginCashierList);
                                mCashierAdapter = new CashierRecyclerAdapter(this, mCashierList);
                                mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        userId = mCashierList.get(position).getUserId();
                                        mTvCashier.setText(mCashierList.get(position).getRealName());
                                        setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                        closeArrow(2);
                                        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                            DialogUtil.safeShowDialog(mLoadDialog);
                                            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                        }
                                    }
                                });
                                mCashierRecyclerView.setAdapter(mCashierAdapter);
                                mCashierAdapter.setSelected(userId);
                                openArrow(2);
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        DataEntity dataEntity = new DataEntity();
                        dataEntity.setRealName(getString(R.string.tv_all_user));
                        dataEntity.setUserId("");
                        mOriginCashierList.add(dataEntity);
                        mOriginCashierList.addAll(msg.getData());
                        mCashierList.addAll(mOriginCashierList);
                        mCashierAdapter = new CashierRecyclerAdapter(this, mCashierList);
                        mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                userId = mCashierList.get(position).getUserId();
//                                mLlCommissionFee.setVisibility(TextUtils.isEmpty(userId) ? View.VISIBLE : View.GONE);
                                setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                mTvCashier.setText(mCashierList.get(position).getRealName());
                                closeArrow(2);
                                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    DialogUtil.safeShowDialog(mLoadDialog);
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                }
                            }
                        });
                        mCashierRecyclerView.setAdapter(mCashierAdapter);
                        mCashierAdapter.setSelected(userId);
                        openArrow(2);
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_SITE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.ERROR_CODE_NO_RIGH)) {
                                PaySiteBean dataEntity = new PaySiteBean();
                                dataEntity.setCashPointName(getString(R.string.tv_all_site));
                                dataEntity.setId("");
                                PaySiteBean otherSite = new PaySiteBean();
                                otherSite.setCashPointName(getString(R.string.tv_other_site));
                                otherSite.setId("tv_other_site");
                                mOriginSiteList.add(dataEntity);
                                mOriginSiteList.add(otherSite);
                                mSiteList.addAll(mOriginSiteList);
                                mSiteAdapter = new PaySiteChoiceAdapter(this, mSiteList, mSiteId);
                                mSiteAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        mSiteId = mSiteList.get(position).getId();
                                        mTvSite.setText(mSiteList.get(position).getCashPointName());
                                        setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                        closeArrow(3);
                                        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                            DialogUtil.safeShowDialog(mLoadDialog);
                                            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                        }
                                    }
                                });
                                mSiteRecyclerView.setAdapter(mSiteAdapter);
//                                openArrow(3);
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getCashPointList() != null) {
                        PaySiteBean dataEntity = new PaySiteBean();
                        dataEntity.setCashPointName(getString(R.string.tv_all_site));
                        dataEntity.setId("");
                        PaySiteBean otherSite = new PaySiteBean();
                        otherSite.setCashPointName(getString(R.string.tv_other_site));
                        otherSite.setId("tv_other_site");
                        mOriginSiteList.add(dataEntity);
                        mOriginSiteList.add(otherSite);
                        mOriginSiteList.addAll(msg.getData().getCashPointList());
                        mSiteList.addAll(mOriginSiteList);
                        mSiteAdapter = new PaySiteChoiceAdapter(this, mSiteList, mSiteId);
                        mSiteAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                mSiteId = mSiteList.get(position).getId();
                                mTvSite.setText(mSiteList.get(position).getCashPointName());
                                setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                closeArrow(3);
                                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    DialogUtil.safeShowDialog(mLoadDialog);
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                }
                            }
                        });
                        mSiteRecyclerView.setAdapter(mSiteAdapter);
//                        openArrow(3);
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_MEMBER)) {//查询会员开通
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, msg.getData().getRetCode());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, msg.getData().isStatisticsCode());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, false);
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, false);
                    }
//                    mRgTitle.setVisibility(MyApplication.showCollectMemberData() ? View.VISIBLE : View.GONE);
                    setTabVisible(MyApplication.isOpenMember(),MyApplication.isOpenFuncardVerification());

                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_FUNCARD)) {//查询饭卡核销开通
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, (boolean) msg.getData());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, false);
                    }
//                    mRgTitle.setVisibility(MyApplication.showCollectMemberData() ? View.VISIBLE : View.GONE);
                    setTabVisible(MyApplication.isOpenMember(),MyApplication.isOpenFuncardVerification());

                    break;
            }
        }
    }

    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("tab", tab);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (TextUtils.isEmpty(mSiteId)) {
            map.put("cashPointType", 1);//全部
        } else if ("tv_other_site".equals(mSiteId)) {
            map.put("cashPointType", 2);//其他
        } else {//单个收银点
            map.put("cashPointType", 3);
            map.put("cashPointId", mSiteId);
        }
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(mShopType))
            map.put("merchantType", mShopType);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mShopSwitchArrow = false;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mEtCashierInput.setText("");
                mCashierSwitchArrow = false;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.GONE);
                break;
            case 3:
                mEtSiteInput.setText("");
                mSiteSwitchArrow = false;
                mIvSiteArrow.startAnimation(rotate);
                mSitePopLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1://点击选择门店
                if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                    mRgType.setVisibility(View.VISIBLE);
                } else {
                    mRgType.setVisibility(View.GONE);
                }
                if (mRgType.getVisibility() == View.VISIBLE) {
                    switch (mShopType) {
                        case "":
                            mRgType.clearCheck();
                            mRbAllStore.setChecked(true);
                            break;
                        case "21":
                            mRgType.clearCheck();
                            mRbDirectStore.setChecked(true);
                            break;
                        case "22":
                            mRgType.clearCheck();
                            mRbJoinStore.setChecked(true);
                            break;
                    }
                }
                mShopSwitchArrow = true;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.VISIBLE);
                loadShopData(mShopType);
                break;
            case 2://点击选择收银员
                mCashierSwitchArrow = true;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.VISIBLE);
                break;
            case 3://点击选择收银点
                mSiteSwitchArrow = true;
                mIvSiteArrow.startAnimation(rotate);
                mSitePopLayout.setVisibility(View.VISIBLE);
                paySiteList();
                break;
        }
    }

    private void loadShopData(String shopType) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
//            if (mRbAllStore.isChecked())
//                mOriginShopList.clear();
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(shopType))
                map.put("merchantType", shopType);
            ServerClient.newInstance(MyApplication.getContext()).findStore(MyApplication.getContext(), Constants.TAG_COLLECT_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadCashierData() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginCashierList.clear();
            mCashierList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(mShopType))
                map.put("merchantType", mShopType);
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), Constants.TAG_COLLECT_CASHIER, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void paySiteList() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginSiteList.clear();
            mSiteList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_COLLECT_SITE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    public void getReport(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            ServerClient.newInstance(MyApplication.getContext()).getReport(MyApplication.getContext(), Constants.TAG_GET_REPORT, map);
        } else {
            //MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            mTvNull.setText("网络异常");
            mTvNull.setVisibility(View.VISIBLE);
            mSvReport.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_button://提示dialog
                new CollectHelpDialog(this).show();
                break;
            case R.id.iv_back://titlebar返回按钮
                finish();
                break;
            case R.id.ll_date:
                MtaUtils.mtaId(this, "1016");
                showPopupDateWindow();
                /*Intent intentDate = new Intent(CollectActivity1.this, DateChoiceActivity.class);
                intentDate.putExtra(Constants.INTENT_NAME, Constants.INTENT_REPORT_CHOICE_DATE);
                startActivityForResult(intentDate, Constants.REQUEST_CHOICE_DATE);*/
                break;
            /*case R.id.ll_shop:
                MtaUtils.mtaId(this, "1017");
                Intent intentShop = new Intent(this, ShopActivity.class);
                if (TextUtils.isEmpty(storeMerchantId)) {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                } else {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                }
                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_COLLECT_SHOP);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.ll_cashier:
                MtaUtils.mtaId(this, "1018");
                Intent intentCashier = new Intent(this, CashierActivity.class);
                if (TextUtils.isEmpty(userId)) {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                } else {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                }
                intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                break;*/
            case R.id.ll_shop:
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                }
                if (mShopSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_cashier:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                } else {
                    loadCashierData();
                }
                break;
            case R.id.ll_pay_site:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                } else {
//                    paySiteList();
                    openArrow(3);
                }
                break;
            case R.id.btn_print:
                MtaUtils.mtaId(this, "1019");
                mInfo.setStoreMerchantId(storeMerchantId);
                bluePrint();
                break;
            case R.id.rb_all_store:
                loadShopData("");
                break;
            case R.id.rb_direct_store:
                loadShopData("21");
                break;
            case R.id.rb_join_store:
                loadShopData("22");
                break;
            case R.id.iv_filter:
                showFilterPop();
                break;
            case R.id.tv_title_click_js:
                //结算金额
                StringBuilder builder = new StringBuilder();
                builder.append(getResources().getString(R.string.question_tip_report_settlement_front));
                builder.append(getResources().getString(R.string.question_tip_report_settlement_behind));
                showCommonNoticeDialog(CollectActivity1.this, builder.toString());
                break;
            case R.id.tv_title_click_yh:
                //优惠金额
                showCommonNoticeDialog(CollectActivity1.this, getResources().getString(R.string.question_tip_report_yh));
                break;
            case R.id.tv_title_click_shishou:
                //实收金额
                showCommonNoticeDialog(CollectActivity1.this, getResources().getString(R.string.question_tip_report_shishou));
                break;
            case R.id.tv_title_click_tk:
                //退款金额
                showCommonNoticeDialog(CollectActivity1.this, getResources().getString(R.string.question_tip_report_tk));
                break;
            case R.id.tv_title_click_jyze:
                //交易净额
                showCommonNoticeDialog(CollectActivity1.this, getResources().getString(R.string.question_tip_report_jyze));
                break;
            case R.id.tv_commission_title:
                //手续费
                showCommonNoticeDialog(CollectActivity1.this, getResources().getString(R.string.question_tip_report_commission));
                break;
            case R.id.tv_fk_settlement_top_title:
                //饭卡结算金额
                showCommonNoticeDialog(CollectActivity1.this, getResources().getString(R.string.question_tip_report_fk_settlement));
                break;
            default:
                break;
        }
    }

    /**
     * 设置结算金额和手续费是否可见
     * 因为通过筛选的收银点和收银员会查询不到结算金额和手续费金额，只有全部的收银点和全部的收银员才有
     * @param visible
     */
    private void setSettlementAndCommitFeeVisible(boolean visible){
        if (visible){
            mLlJsLayout.setVisibility(View.VISIBLE);
            mTvCommissionTitle.setVisibility(View.VISIBLE);
            mTvCommissionFee.setVisibility(View.VISIBLE);
        }else {
            mLlJsLayout.setVisibility(View.GONE);
            mTvCommissionTitle.setVisibility(View.INVISIBLE);
            mTvCommissionFee.setVisibility(View.INVISIBLE);
        }
    }

    public void showFilterPop() {
        if (popupWindow == null) {
            popupWindow = new FilterCashierPopupWindow(this, new FilterCashierPopupWindow.HandleBtn() {
                @Override
                public void handleOkBtn(String cashierName, String userId) {
                    CollectActivity1.this.userId = userId;
                    CollectActivity1.this.cashierName = cashierName;
                    setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                    if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                    }
                }
            });
            popupWindow.setOnClickCashier(new FilterCashierPopupWindow.OnClickCashier() {
                @Override
                public void onClickCashier() {
                    Intent intentCashier = new Intent(CollectActivity1.this, CashierActivity.class);
                    intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                    if (TextUtils.isEmpty(userId)) {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                    } else {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                    }
                    startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                }
            });
        }
        popupWindow.setCashier(userId, cashierName);
        popupWindow.showAsDropDown(mLlDate);
    }

    private void showPopupDateWindow() {
        DateStartEndPopupWindow moneyPopupWindow = new DateStartEndPopupWindow(CollectActivity1.this, startTime, endTime, new DateStartEndPopupWindow.HandleTv() {
            @Override
            public void getDate(String startTime, String endTime) {
                CollectActivity1.this.startTime = startTime;
                CollectActivity1.this.endTime = endTime;
                try {
                    if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formartDateToMMDD(endTime))) {
                        /*if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                            tv_date.setText(DateUtil.formartDateToMMDD(startTime));
                        } else {*/
                            tv_date.setText(DateUtil.formartDateToMMDD(startTime));
//                        }
                    } else {
                        tv_date.setText(DateUtil.formartDateToMMDD(startTime) + " " + getString(R.string.tv_least) + " "
                                + DateUtil.formartDateToMMDD(endTime));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        });
        moneyPopupWindow.showAtLocation(mBtnPrint, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeName = shopBean.getStoreName();
            storeMerchantId = shopBean.getStoreId();
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText(getString(R.string.tv_all_user));
                cashierName = getString(R.string.tv_all_user);
                userId = "";
            } else {
                mTvCashier.setText(MyApplication.getRealName());
                cashierName = MyApplication.getRealName();
                userId = MyApplication.getUserId();
            }
            mTvSite.setText(getString(R.string.tv_all_site));
            mSiteId = "";
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
           /* Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            cashierName = cashierBean.getRealName();*/

            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            popupWindow.setCashier(cashierBean.getUserId(), cashierBean.getRealName());
        }
    }

    public void bluePrint() {
        if (mInfo != null) {
            if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                mInfo.setStartTime(startTime);
                long now = System.currentTimeMillis();
                if (now < DateUtil.getTime(endTime,"yyyy-MM-dd HH:mm:ss")) {
                    mInfo.setEndTime(DateUtil.formatTime(now, "yyyy-MM-dd HH:mm:ss"));
                } else {
                    mInfo.setEndTime(endTime);
                }
            } else {
                mInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                mInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
            mInfo.setCashierName(mTvCashier.getText().toString().trim());
            mInfo.setStoreName(mTvShop.getText().toString().trim());
            mInfo.setCashierPointName(mTvSite.getText().toString().trim());
//            bluetoothPrint(mInfo);
            CloudPrintUtil.cloudPrint(this, new CloudPrintUtil.OnGetCloudDevicesListCallBack() {
                @Override
                public void startCloudPrint(List<DeviceBean> cloudDevices) {
                    CloudPrintUtil.startCloudPrint(CollectActivity1.this,cloudDevices,mInfo);
                }

                @Override
                public void startBluetoothPrint() {
                    bluetoothPrint(mInfo);
                }

            });
        } else {
            showCommonNoticeDialog(CollectActivity1.this, getString(R.string.print_data_error));
        }
    }

    private void bluetoothPrint(ReportBean.DataEntity info) {
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            showPrintDialog(1);
            return;
        }
        if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    boolean isSucc = BluePrintUtil.blueConnent(1, CollectActivity1.this);
                    if (isSucc) {
                        BluePrintUtil.printDateSum(info);
//                    BluePrintUtil.kitchenPrint(BluePrintUtil.getBean());
                    } else {
                        if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showPrintDialog(1);
                                }
                            });
                        } else {
                            //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                            LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                        }
                    }
                }
            }.start();

        } else {
            showPrintDialog(1);
        }
    }


    public void setView(ReportBean.DataEntity dataEntity) {
        if (dataEntity != null) {
            if (tab == 3){ //饭卡消费
                mTvFkSettlementMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
                mTvFkPayMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
                mTvFkPayCount.setText(dataEntity.getSuccessCount() + "");
                mTvFkSettlementRate.setText(TextUtils.isEmpty(dataEntity.getSettleRate())?"--":dataEntity.getSettleRate()+"");

                mTvFkSettlementMoneyContent.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
                mTvFkRefundTotalCount.setText(dataEntity.getRefundCount() + "");
                mTvFkRefundTotalMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));

            }else if (tab == 2) {//会员消费
                mTvVerifyActuralMoney.setText(DateUtil.formatMoneyUtil((dataEntity.getPayFee() - dataEntity.getRefundFee()) / 100d));
                mTvVerifyTotalMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
                mTvVerifyTotalCount.setText(dataEntity.getSuccessCount() + "");
                mTvVerifyMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getPayFee() / 100d));
                mTvVerifyCouponMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getMchDiscountsFee() / 100d));
                mTvVerifyRefund.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));
                mTvVerifyRefundNum.setText(dataEntity.getRefundCount() + "");

                mTvCommissionFee.setText(DateUtil.formatMoneyUtil(dataEntity.getCommissionFee() / 100d));
                mTvSettlementFee.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
            } else {//收款
                mTvTotal.setText(DateUtil.formatMoneyUtil((dataEntity.getIncome()) / 100d));
                mTvTotalPay.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
                mTvTotalPayNum.setText(dataEntity.getSuccessCount() + "");
                mTvRealMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getPayFee() / 100d));
                mTvPayCouponMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getMchDiscountsFee() / 100d));
                mTvTotalRefund.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));
                mTvTotalRefundNum.setText(dataEntity.getRefundCount() + "");

                if (MyApplication.isOpenFuncard()){
                    mTvCommissionTitle.setText(getResources().getString(R.string.tx_report_fw_money));
                    mTvCommissionFee.setText(DateUtil.formatMoneyUtil((dataEntity.getCommissionFee() / 100d) + (dataEntity.getFzSuccessFee()/100d)));
                }else {
                    if (dataEntity.getFzSuccessFee() > 0) {
                        mTvCommissionTitle.setText(getResources().getString(R.string.tx_report_commission_fw_money));
                        mTvCommissionFee.setText(DateUtil.formatMoneyUtil(dataEntity.getCommissionFee() / 100d) + "/" + DateUtil.formatMoneyUtil(dataEntity.getFzSuccessFee() / 100d));
                    } else {
                        mTvCommissionTitle.setText(getResources().getString(R.string.tx_report_commission_money));
                        mTvCommissionFee.setText(DateUtil.formatMoneyUtil(dataEntity.getCommissionFee() / 100d));
                    }
                }

                mTvSettlementFee.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_REPORT_DATA);
        mReportBroadcastReceiver = new ReportBroadcastReceiver();
        registerReceiver(mReportBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mReportBroadcastReceiver);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_title_pay:
                mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);
                mIvFilter.setVisibility(View.VISIBLE);
                mLlReportPay.setVisibility(View.VISIBLE);
                mLayoutVipTab.setVisibility(View.GONE);
                mLayoutFkTab.setVisibility(View.GONE);
                tab = 1;
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;
            case R.id.rb_title_verify:
                mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);
                mIvFilter.setVisibility(View.VISIBLE);
                mLlReportPay.setVisibility(View.GONE);
                mLayoutVipTab.setVisibility(View.VISIBLE);
                mLayoutFkTab.setVisibility(View.GONE);
                tab = 2;
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;
            case R.id.rb_title_fun_card:
                mLlSite.setVisibility(View.GONE);
                mIvFilter.setVisibility(View.GONE);
                mLlReportPay.setVisibility(View.GONE);
                mLayoutVipTab.setVisibility(View.GONE);
                mLayoutFkTab.setVisibility(View.VISIBLE);
                tab = 3;
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;

        }
    }

    class ReportBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        }
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }
}
