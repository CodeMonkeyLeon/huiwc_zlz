/*
 * 文 件 名:  BillMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.AccessibleDateAnimator;
import com.fourmob.datetimepicker.date.DatePickerController;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DayPickerView;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.viewpager.NewPage;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期选择
 *
 * @author
 * @see
 */
public class DateChoiceActivity extends BaseActivity {
    private LinearLayout ly_start, ly_end;

    private NewPage page;

    private View firstView, secondView;

    private View[] views = new View[2];

    private AccessibleDateAnimator animator;

    private DayPickerView mDayPickerView;

    private AccessibleDateAnimator animator1;

    private final Calendar mCalendar = Calendar.getInstance();

    private final Calendar mCalendar1 = Calendar.getInstance();

    private int mWeekStart = mCalendar.getFirstDayOfWeek();

    //    private int mMaxYear = 2037;
    //    
    //    private int mMinYear = 1902;

    private int mMaxYear = mCalendar.get(Calendar.YEAR);

    private int mMinYear = mCalendar.get(Calendar.YEAR) - 1;

    private int currentYear = mCalendar.get(Calendar.YEAR);

    private int currentMonth = mCalendar.get(Calendar.MONTH);

    private TextView tv_choice_start_time, tv_choice_end_time, tv_right_end, tv_left_end, tv_right_start,
            tv_left_start;

    private TextView tv_end_info, tv_start_info;
    private View v_start, v_end;
    private String startTime, endTime;
    private long sTime, eTime;
    private ImageView ly_back;
    private TextView mTvTitle, tv_enter;
    private int range = 90;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_choice);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        page = (NewPage) findViewById(R.id.pager);

        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_INCOME_CHOICE_DATE.equals(mIntentName)) {
            range = 32;
        }
        initView();

        setLiseter();
        /*startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";*/
    }

    void setLeftViewBg() {
        tv_right_start.setText(")");
        tv_left_start.setText("(");
        v_start.setVisibility(View.VISIBLE);
        v_end.setVisibility(View.GONE);
    }

    void setRightViewBg() {
        if (!StringUtils.isEmptyOrNull(tv_choice_end_time.getText().toString())) {
            tv_right_end.setText(")");
            tv_left_end.setText("(");
        } /*else {
            tv_choice_end_time.setText(DateUtil.formatMD(System.currentTimeMillis()));
        }*/
        v_start.setVisibility(View.GONE);
        v_end.setVisibility(View.VISIBLE);
    }

    private void setLiseter() {
        ly_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tv_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //确定
                if (StringUtils.isEmptyOrNull(startTime)) {
                    showCommonNoticeDialog(DateChoiceActivity.this, getString(R.string.tv_time_start));
                    return;
                }
                if (StringUtils.isEmptyOrNull(endTime)) {
                    showCommonNoticeDialog(DateChoiceActivity.this, getString(R.string.tv_time_end));
                    return;
                }

                if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date startDate;
                    try {
                        startDate = df.parse(startTime);
                        Date endDate = df.parse(endTime);
                        if (startDate.getTime() >= endDate.getTime()) {
                            showCommonNoticeDialog(DateChoiceActivity.this, getString(R.string.show_endtime_than_starttime));
                            return;
                        }
                        String time = startTime + "|" + endTime;
                        Intent intent = new Intent();
                        Bundle mBundle = new Bundle();
                        mBundle.putString(Constants.RESULT_CHOICE_DATE_INTENT, time);   //传递一个user对象列表
                        intent.putExtras(mBundle);
                        setResult(RESULT_OK, intent);
                        DateChoiceActivity.this.finish();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                //点击确定把选择的日期带回到报表并销毁当前页面
                finish();
            }
        });
        ly_start.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                page.setCurrentItem(0);
            }
        });

        ly_end.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!StringUtils.isEmptyOrNull(tv_choice_start_time.getText().toString())) {
                    page.setCurrentItem(1);
                }
            }
        });

        page.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0: //汇总
                        setLeftViewBg();
                        break;
                    case 1://走势
                        setRightViewBg();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void initView() {
        ly_back = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        tv_enter = findViewById(R.id.button_title);
        mTvTitle.setText("日期选择");
        tv_enter.setText("确定");

        v_start = findViewById(R.id.v_start);
        v_end = findViewById(R.id.v_end);
        ly_start = (LinearLayout) findViewById(R.id.ly_start);
        ly_end = (LinearLayout) findViewById(R.id.ly_end);
        tv_choice_start_time = (TextView) findViewById(R.id.tv_choice_start_time);

        tv_choice_end_time = (TextView) findViewById(R.id.tv_choice_end_time);
        tv_left_end = (TextView) findViewById(R.id.tv_left_end);
        tv_right_end = (TextView) findViewById(R.id.tv_right_end);
        tv_right_start = (TextView) findViewById(R.id.tv_right_start);
        tv_left_start = (TextView) findViewById(R.id.tv_left_start);
        tv_end_info = (TextView) findViewById(R.id.tv_end_info);
        tv_start_info = (TextView) findViewById(R.id.tv_start_info);

        LayoutInflater inflater = LayoutInflater.from(this);
        firstView = inflater.inflate(R.layout.date_time_pick1, null);
        views[0] = firstView;
        animator = (AccessibleDateAnimator) firstView.findViewById(R.id.animator);
        mDayPickerView = new DayPickerView(DateChoiceActivity.this, new DatePickerControllerImp(), null, range, R.color.theme_color);

        mCalendar.set(Calendar.YEAR, Calendar.YEAR);
        mCalendar.set(Calendar.MONTH, Calendar.MONTH);
        mCalendar.set(Calendar.DAY_OF_MONTH, Calendar.DAY_OF_MONTH);

        animator.addView(mDayPickerView);
        animator.setDateMillis(mCalendar.getTimeInMillis());
        secondView = inflater.inflate(R.layout.date_time_pick2, null);
        views[1] = secondView;
        animator1 = (AccessibleDateAnimator) secondView.findViewById(R.id.animator1);

        Calendar c = Calendar.getInstance();

        loadSeondDayPick(c);

        page.setAdapter(new MyAdapter());
        page.setCurrentItem(0);
        //        page.setScrollble(false);

        //初始化当前，改变颜色
        //tv_choice_start_time.setText(DateUtil.formatMD(System.currentTimeMillis()));
        /*tv_choice_start_time.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_right_start.setTextColor(getResources().getColor(R.color.title_bg_new));*/
        //tv_right_start.setText(")");
        //tv_left_start.setText("(");
        /*tv_left_start.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_start_info.setTextColor(getResources().getColor(R.color.title_bg_new));*/


    }

    class DatePickerControllerImp implements DatePickerController {

        @Override
        public int getFirstDayOfWeek() {
            return mWeekStart;
        }

        @Override
        public int getMaxYear() {
            return mMaxYear;
        }

        @Override
        public int getMinYear() {
            return mMinYear;
        }

        @Override
        public SimpleMonthAdapter.CalendarDay getSelectedDay() {
            return new SimpleMonthAdapter.CalendarDay(mCalendar);
        }

        @Override
        public void onDayOfMonthSelected(int year, int month, int day) {
            if (day <= 9) {
                tv_choice_start_time.setText((month + 1) + "-0" + day);

            } else {
                tv_choice_start_time.setText((month + 1) + "-" + day);
            }
            //tv_choice_start_time.setTextColor(getResources().getColor(R.color.title_bg_new));
            page.setScrollble(true);
            // tv_right_start.setTextColor(getResources().getColor(R.color.title_bg_new));
            tv_right_start.setText(")");
            tv_left_start.setText("(");
            //tv_left_start.setTextColor(getResources().getColor(R.color.title_bg_new));
            //tv_start_info.setTextColor(getResources().getColor(R.color.title_bg_new));
            page.setCurrentItem(1);
            v_start.setVisibility(View.GONE);
            v_end.setVisibility(View.VISIBLE);

            Calendar c = Calendar.getInstance();
            c.set(year, month, day);
            startTime = year + "-" + (month + 1) + "-" + day + " 00:00:00";
            //            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //            Log.i("hehui", "onDayOfMonthSelected-->" + df.format(c.getTimeInMillis()));
            loadSeondDayPick(c);
        }

        @Override
        public void onYearSelected(int year) {

        }

        @Override
        public void tryVibrate() {

        }

        @Override
        public void registerOnDateChangedListener(DatePickerDialog.OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public void registerOnDateChangedListener(
                com.fourmob.datetimepicker.test.MainTest.OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public int getCurrentYear() {
            // TODO Auto-generated method stub
            return currentYear;
        }

        @Override
        public int getCurrentMonth() {
            // TODO Auto-generated method stub
            return currentMonth;
        }

    }

    class DatePickerControllerImp2 implements DatePickerController {
        Calendar calendar;

        public DatePickerControllerImp2(Calendar calendar) {
            this.calendar = calendar;
        }

        @Override
        public int getFirstDayOfWeek() {
            return mWeekStart;
        }

        @Override
        public int getMaxYear() {
            return mMaxYear;
        }

        @Override
        public int getMinYear() {
            return mMinYear;
        }

        @Override
        public SimpleMonthAdapter.CalendarDay getSelectedDay() {
            return new SimpleMonthAdapter.CalendarDay(calendar);
        }

        @Override
        public void onDayOfMonthSelected(int year, int month, int day) {
            if (day <= 9) {
                tv_choice_end_time.setText((month + 1) + "-0" + day);

            } else {
                tv_choice_end_time.setText((month + 1) + "-" + day);
            }
            endTime = year + "-" + (month + 1) + "-" + day + " 23:59:59";
            //            Log.i("hehui", ",y-->" + year + ",m-->" + month + ",d-->" + day);
            setRightViewBg();
        }

        @Override
        public void onYearSelected(int year) {

        }

        @Override
        public void tryVibrate() {

        }

        @Override
        public void registerOnDateChangedListener(DatePickerDialog.OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public void registerOnDateChangedListener(com.fourmob.datetimepicker.test.MainTest.OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public int getCurrentYear() {
            return currentYear;
        }

        @Override
        public int getCurrentMonth() {
            // TODO Auto-generated method stub
            return currentMonth;
        }

    }

    void loadSeondDayPick(Calendar c) {
        animator1.removeAllViews();
        DayPickerView mDayPickerView1 = new DayPickerView(DateChoiceActivity.this, new DatePickerControllerImp2(c), c, range, R.color.theme_color);

        mCalendar1.set(Calendar.YEAR, Calendar.YEAR);
        mCalendar1.set(Calendar.MONTH, Calendar.MONTH);
        mCalendar1.set(Calendar.DAY_OF_MONTH, Calendar.DAY_OF_MONTH);
        animator1.addView(mDayPickerView1);
        animator1.setDateMillis(c.getTimeInMillis());

    }

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            if (views.length < 3) {
                return views.length;
            } else {
                return Integer.MAX_VALUE;
            }
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            //            ((ViewPager)container).removeView(mImageViews[position % mImageViews.length]);

        }

        /**
         * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
         */
        @Override
        public Object instantiateItem(View container, int position) {
            try {

                ((ViewPager) container).addView(views[position % views.length], 0);
            } catch (Exception e) {
            }
            return views[position % views.length];
        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

    }

}
