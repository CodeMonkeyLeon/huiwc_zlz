package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintInfoActivity;
import com.hstypay.enterprise.activity.cloundSound.CloudSetActivity;
import com.hstypay.enterprise.activity.dynamicCode.DynamicSetActivity;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.deviceManage.DeviceManageAdapter;
import com.hstypay.enterprise.adapter.deviceManage.DeviceManageTypeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.bean.DeviceListBean;
import com.hstypay.enterprise.bean.DeviceType;
import com.hstypay.enterprise.bean.DeviceTypeBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author dean.zeng
 * @Description 云打印设备列表
 * @Date 2020-02-05 15:28
 **/
public class DeviceListActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvButton, mIvStoreArrow, mIvDeviceTypeArrow;
    private LinearLayout mLlDeviceList, mLlDeviceEmpty, mLlChoiceStore, mLlDeviceType, mStorePopLayout;
    private EditTextDelete mEtInput, mEtStoreInput;
    private RecyclerView mRecyclerView, mStoreRecyclerView, mTypeRecyclerView;
    private Button mBtnSubmit, mBtnStore;
    private TextView mButton, mTvTitle, mTvStoreName, mTvDeviceTypeName, mTvNull, mTvShopNull, mTvCloudCart, mTvCloudCart1;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DeviceType> mTypeList;
    private List<DeviceBean> mDeviceList;
    private Animation rotate;
    private SafeDialog mLoadDialog;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private boolean isNotFirstRequest;
    private String categoryCode = "";
    private CustomLinearLayoutManager mLinearLayoutManager;
    private DeviceManageAdapter mAdapter;
    private ShopRecyclerAdapter mStoreAdapter;
    private boolean mTypeSwitchArrow, mStoreSwitchArrow;
    private String mShopType;
    private RadioGroup mRgType;
    private RadioButton mRbAllStore, mRbDirectStore, mRbJoinStore;
    private String storeMerchantId;
    private DeviceManageTypeAdapter mDeviceTypeAdapter;
    private View viewBg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        if (!MyApplication.getIsCasher()) {
            mLlChoiceStore.setOnClickListener(this);
        }
        mLlDeviceType.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mBtnStore.setOnClickListener(this);
        mTvCloudCart.setOnClickListener(this);
//        mIvButton.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.string_title_device_list);
        mButton = findViewById(R.id.button_title);
        mButton.setText(R.string.btn_add_device);
        /*mButton.setVisibility(View.INVISIBLE);
        mIvButton = findViewById(R.id.iv_button);
        mIvButton.setVisibility(View.VISIBLE);
        Drawable icon = getResources().getDrawable(R.mipmap.icon_general_expect);
        Drawable tintIcon = DrawableCompat.wrap(icon);
        DrawableCompat.setTint(tintIcon, UIUtils.getColor(R.color.home_text));
        mIvButton.setImageDrawable(tintIcon);*/
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mBtnStore = (Button) findViewById(R.id.btn_store);
        mTvStoreName = (TextView) findViewById(R.id.tv_store_name);
        mIvStoreArrow = (ImageView) findViewById(R.id.iv_store_arrow);
        mLlChoiceStore = (LinearLayout) findViewById(R.id.ll_choice_store);
        mTvDeviceTypeName = (TextView) findViewById(R.id.tv_device_type);
        mIvDeviceTypeArrow = (ImageView) findViewById(R.id.iv_device_type_arrow);
        mLlDeviceType = (LinearLayout) findViewById(R.id.ll_device_type);
        mLlDeviceList = (LinearLayout) findViewById(R.id.ll_cloud_device_list);
        mLlDeviceEmpty = (LinearLayout) findViewById(R.id.ll_cloud_device_empty);
        mEtInput = findViewById(R.id.et_input);
        mTvCloudCart = (TextView) findViewById(R.id.tv_cloud_device_cart);
        mTvCloudCart1 = (TextView) findViewById(R.id.tv_cloud_device_cart1);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mTvShopNull = (TextView) findViewById(R.id.tv_shop_null);
        viewBg = findViewById(R.id.view_bg);

        if (MyApplication.getIsCasher()) {
            mIvStoreArrow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(MyApplication.getDefaultStoreName())) {
                mTvStoreName.setText(StringUtils.omitString(MyApplication.getDefaultStoreName(), 8));
            }
        }
        SpannableStringBuilder spannableString = new SpannableStringBuilder();
        spannableString.append(getString(R.string.tv_device_service));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if (AppHelper.getAppType() == 2) {
                    if (!Constants.KINGDEEPAY.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        AppHelper.call(DeviceListActivity.this, getString(R.string.contact_service_telephone));
                    }
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
            }
        };
        int indexOfStart = getString(R.string.tv_device_service).indexOf(getString(R.string.contact_service_telephone));
        int indexOfEnd = getString(R.string.tv_device_service).indexOf(getString(R.string.contact_service_telephone)) + getString(R.string.contact_service_telephone).length();
        if (indexOfStart >=0 && indexOfEnd >= 0) {
            spannableString.setSpan(clickableSpan, indexOfStart, indexOfEnd, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            mTvCloudCart.setText(spannableString);
            mTvCloudCart.setMovementMethod(LinkMovementMethod.getInstance());
            avoidHintColor(mTvCloudCart);
        }

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mDeviceList.clear();
                        currentPage = 2;
                        getDeviceList(pageSize, 1, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mDeviceList.clear();
                        currentPage = 2;
                        getDeviceList(pageSize, 1, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        initRecyclerView();
        initSwipeRefreshLayout();

        mRgType = findViewById(R.id.rg_type);
        mRbAllStore = findViewById(R.id.rb_all_store);
        mRbDirectStore = findViewById(R.id.rb_direct_store);
        mRbJoinStore = findViewById(R.id.rb_join_store);
        mStorePopLayout = (LinearLayout) findViewById(R.id.shop_pop_layout);
        mEtStoreInput = findViewById(R.id.et_store_input);
        mStoreRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_store);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(this);
        mStoreRecyclerView.setLayoutManager(storeLinearLayoutManager);

        CustomLinearLayoutManager typeLinearLayoutManager = new CustomLinearLayoutManager(this);
        mTypeRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_device_type);
        mTypeRecyclerView.setLayoutManager(typeLinearLayoutManager);

        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);
        setButtonEnable(mBtnSubmit, true);
        setButtonWhite(mBtnStore);
        if (Constants.KINGDEEPAY.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mBtnStore.setVisibility(View.INVISIBLE);
        }
    }

    private void avoidHintColor(View view) {
        if (view instanceof TextView)
            ((TextView) view).setHighlightColor(getResources().getColor(android.R.color.transparent));
    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initData() {
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mTypeList = new ArrayList<>();
        mDeviceList = new ArrayList<>();
        mShopType = "";
        storeMerchantId = "";

        mAdapter = new DeviceManageAdapter(DeviceListActivity.this, mDeviceList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new DeviceManageAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent();
                switch (mDeviceList.get(position).getCategoryCode()) {
                    case "HAPY_POS"://扫码pos
                    case "HPAY_POS"://智能pos
                    case "HPAY_FACEPAY"://刷脸终端
                    case "HPAY_RAM"://动态台卡
                        intent.setClass(DeviceListActivity.this, DynamicSetActivity.class);
                        break;
                    case "CLOUD_BOX"://云音箱
                        intent.setClass(DeviceListActivity.this, CloudSetActivity.class);
                        break;
                    case "CLOUD_PRINT"://云打印
                        intent.setClass(DeviceListActivity.this, CloudPrintInfoActivity.class);
                        break;
                }
                intent.putExtra(Constants.INTENT_STORE_ID, mDeviceList.get(position).getStoreMerchantId());
                intent.putExtra(Constants.INTENT_DEVICE_SN, mDeviceList.get(position).getSn());
                intent.putExtra(Constants.INTENT_DEVICE_ID, mDeviceList.get(position).getDeviceId());
                startActivity(intent);
            }
        });

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mDeviceList.clear();
            currentPage = 2;
            getDeviceList(pageSize, 1, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getDeviceList(pageSize, 1, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getDeviceList(pageSize, currentPage, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            /*case R.id.iv_button:
                DevicePopupWindow devicePopupWindow = new DevicePopupWindow(new DevicePopupWindow.OnSubmitListener() {
                    @Override
                    public void submit(int index) {
                        switch (index) {
                            case 0:
                                toMiniProgram();
                                break;
                            case 1:
                                Intent intent = new Intent(DeviceListActivity.this, CaptureActivity.class);
                                intent.putExtra(Constants.INTENT_NAME, "INTENT_BIND_DEVICE");
                                startActivity(intent);
                                break;
                        }
                    }
                }, this);
                devicePopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        viewBg.setVisibility(View.GONE);
                    }
                });
                devicePopupWindow.showAsDropDown(mIvButton);
                viewBg.setVisibility(View.VISIBLE);
                break;*/
            case R.id.button_title:
            case R.id.btn_submit:
                Intent intent = new Intent(DeviceListActivity.this, CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, "INTENT_BIND_DEVICE");
                startActivity(intent);
                break;
            case R.id.btn_store:
                toMiniProgram();
                break;
            case R.id.ll_device_type:
                if (mStoreSwitchArrow) {
                    closeArrow(1);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(2);
                } else {
                    openArrow(2);
                }
                break;
            case R.id.ll_choice_store:
                if (mTypeSwitchArrow) {
                    closeArrow(2);
                }
                if (mStoreSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
        }
    }

    private void toMiniProgram() {
        if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
            String appId = "wx6e411e48e4dacb00"; // 填应用AppId
            IWXAPI api = WXAPIFactory.createWXAPI(DeviceListActivity.this, appId);
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = "gh_1d2ba02ad88f"; // 填小程序原始id
            req.path = "pages/device-mall/home/home";  //拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
            api.sendReq(req);
        } else {
            MyToast.showToastShort("请先安装微信！");
        }
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mStoreSwitchArrow = false;
                mIvStoreArrow.startAnimation(rotate);
                mStorePopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mTypeSwitchArrow = false;
                mIvDeviceTypeArrow.startAnimation(rotate);
                mTypeRecyclerView.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                    mRgType.setVisibility(View.VISIBLE);
                } else {
                    mRgType.setVisibility(View.GONE);
                }
                if (mRgType.getVisibility() == View.VISIBLE) {
                    switch (mShopType) {
                        case "":
                            mRgType.clearCheck();
                            mRbAllStore.setChecked(true);
                            break;
                        case "21":
                            mRgType.clearCheck();
                            mRbDirectStore.setChecked(true);
                            break;
                        case "22":
                            mRgType.clearCheck();
                            mRbJoinStore.setChecked(true);
                            break;
                    }
                }
                mStoreSwitchArrow = true;
                mIvStoreArrow.startAnimation(rotate);
                mStorePopLayout.setVisibility(View.VISIBLE);
                loadShopData(mShopType);
                break;
            case 2:
                mTypeSwitchArrow = true;
                mIvDeviceTypeArrow.startAnimation(rotate);
                loadDeviceData();
                break;
        }
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                mRgType.setVisibility(View.VISIBLE);
            }
            mShopList.addAll(mOriginShopList);
        } else {
            mRgType.setVisibility(View.GONE);
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIvStoreArrow.clearAnimation();
    }

    private void loadShopData(String shopType) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(shopType))
                map.put("merchantType", shopType);
            ServerClient.newInstance(MyApplication.getContext()).findStore(MyApplication.getContext(), Constants.TAG_DEVICE_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadDeviceData() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mTypeList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).deviceTypeList(MyApplication.getContext(), Constants.TAG_DEVICE_TYPE, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getDeviceList(int pageSize, int currentPage, boolean showLoading) {
        //门店网络请求
        if (showLoading) {
            DialogUtil.safeShowDialog(mLoadDialog);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        if (!TextUtils.isEmpty(mEtInput.getText().toString().trim()))
            map.put("sn", mEtInput.getText().toString().trim());
        if (!TextUtils.isEmpty(categoryCode))
            map.put("categoryCode", categoryCode);
        if (!TextUtils.isEmpty(mShopType))
            map.put("merchantType", mShopType);
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeMerchantId", storeMerchantId);
        }
        ServerClient.newInstance(MyApplication.getContext()).getDeviceList(MyApplication.getContext(), Constants.TAG_DEVICE_LIST, map);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_DEVICE_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        if (mRbAllStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_shop));
                            dataEntity.setStoreId("");
                            mOriginShopList.clear();
                            mOriginShopList.add(dataEntity);
                            mOriginShopList.addAll(msg.getData());
                            mShopList.addAll(mOriginShopList);
                        } else if (mRbDirectStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_direct_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        } else if (mRbJoinStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_join_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        }
                        mStoreAdapter = new ShopRecyclerAdapter(this, mShopList, storeMerchantId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                if (mRgType.getVisibility() == View.VISIBLE) {
                                    switch (mRgType.getCheckedRadioButtonId()) {
                                        case R.id.rb_all_store:
                                            mShopType = "";
                                            break;
                                        case R.id.rb_direct_store:
                                            mShopType = "21";
                                            break;
                                        case R.id.rb_join_store:
                                            mShopType = "22";
                                            break;
                                    }
                                } else {
                                    mShopType = "";
                                }
                                storeMerchantId = mShopList.get(position).getStoreId();
                                mTvStoreName.setText(StringUtils.omitString(mShopList.get(position).getStoreName(), 8));
                                closeArrow(1);
                                mDeviceList.clear();
                                currentPage = 2;
                                getDeviceList(pageSize, 1, true);
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        mTvShopNull.setVisibility(View.GONE);
                        mStoreRecyclerView.setVisibility(View.VISIBLE);
                    } else {
                        mTvShopNull.setVisibility(View.VISIBLE);
                        mStoreRecyclerView.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_DEVICE_TYPE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceTypeBean msg = (DeviceTypeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        DeviceType dataEntity = new DeviceType();
                        dataEntity.setCategoryName(getString(R.string.tv_all_device_type));
                        dataEntity.setCategoryCode("");
                        mTypeList.add(dataEntity);
                        mTypeList.addAll(msg.getData());
                        mDeviceTypeAdapter = new DeviceManageTypeAdapter(this, mTypeList, categoryCode);
                        mDeviceTypeAdapter.setOnItemClickListener(new DeviceManageTypeAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                categoryCode = mTypeList.get(position).getCategoryCode();
                                mTvDeviceTypeName.setText(mTypeList.get(position).getCategoryName());
                                closeArrow(2);
                                mDeviceList.clear();
                                currentPage = 2;
                                getDeviceList(pageSize, 1, true);
                            }
                        });
                        mTypeRecyclerView.setVisibility(View.VISIBLE);
                        mTypeRecyclerView.setAdapter(mDeviceTypeAdapter);
                        LogUtil.d("111111===" + mTypeList.size());
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_DEVICE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceListBean msg = (DeviceListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mLlDeviceList.setVisibility(View.VISIBLE);
                        mButton.setVisibility(View.VISIBLE);
                        mLlDeviceEmpty.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mDeviceList.addAll(msg.getData().getData());
                    } else {
                        if (!isNotFirstRequest) {
                            mLlDeviceList.setVisibility(View.GONE);
                            mLlDeviceEmpty.setVisibility(View.VISIBLE);
                            mButton.setVisibility(View.INVISIBLE);
                        } else {
                            if (isLoadmore) {
                                mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                                mTvNull.setVisibility(View.GONE);
                                MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                            } else {
                                mSwipeRefreshLayout.setVisibility(View.GONE);
                                mTvNull.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                    break;
            }
            mAdapter.notifyDataSetChanged();
            isLoadmore = false;
            isRefreshed = false;
            isNotFirstRequest = true;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mDeviceList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mEtInput.setText("");
    }
}
