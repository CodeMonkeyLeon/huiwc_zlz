package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.paySite.PaySiteChoiceActivity;
import com.hstypay.enterprise.adapter.BindingCodeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.StoresBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/7/7.
 */

public class DeviceStoreActivity extends BaseActivity implements View.OnClickListener, BindingCodeAdapter.OnRecyclerViewItemClickListener {

    private ImageView iv_back;
    private TextView tv_title, mTvBindingtitle, button_title, mTvDeviceType, mTvDeviceModel, mTvDeviceSn;
    private RecyclerView mRvShop;
    private Button mBtnEnsure;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private BindingCodeAdapter mAdapter;
    private String mIntentName;
    private int mDataType;
    private SafeDialog mLoadDialog;
    private int mPosition = -1;
    private DeviceBean mDeviceInfo;
    private boolean storeSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_store);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        mTvBindingtitle = (TextView) findViewById(R.id.tv_binding_title);
        button_title = findViewById(R.id.button_title);
        mBtnEnsure = findViewById(R.id.btn_ensure);
        button_title.setText("跳过");
        button_title.setVisibility(View.INVISIBLE);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mTvDeviceType = findViewById(R.id.tv_device_type);
        mTvDeviceModel = findViewById(R.id.tv_device_model);
        mTvDeviceSn = findViewById(R.id.tv_device_sn);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());

    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO 触发刷新数据任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData("15", "1");
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                //TODO 触发加载更多任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "");
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initData() {
        mDeviceInfo = (DeviceBean) getIntent().getSerializableExtra("INTENT_DEVICE_INFO");
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        /*if ("2".equals(mDeviceInfo.getBindSet())) {
            button_title.setVisibility(View.INVISIBLE);
        }*/
        if ("2".equals(mDeviceInfo.getBindSet())) {//必须关联门店
            setButtonEnable(mBtnEnsure, false);
        } else {
            setButtonEnable(mBtnEnsure, true);
        }
        mTvDeviceType.setText(mDeviceInfo.getCategoryName());
        mTvDeviceModel.setText(mDeviceInfo.getDeviceModel());
        mTvDeviceSn.setText(mDeviceInfo.getSn());
        mDataType = getIntent().getIntExtra(Constants.INTENT_STORE_DATA_TYPE, 0);
        tv_title.setText(R.string.title_select_shop);
        mList = new ArrayList<>();
        mAdapter = new BindingCodeAdapter(DeviceStoreActivity.this, mList);
        mRvShop.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        List<StoresBean> stores = (List<StoresBean>) getIntent().getSerializableExtra(Constants.INTENT_USER_STORE);
        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_USER_ID)) && stores != null) {
            for (int i = 0; i < stores.size(); i++) {
                StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                dataEntity.setStoreId(stores.get(i).getStoreId());
                dataEntity.setStoreName(stores.get(i).getStoreName());
                mList.add(dataEntity);
            }
            mAdapter.notifyDataSetChanged();
        } else {
            loadData(pageSize + "", "1");
        }
    }

    private void loadData(String pageSize, String currentPage) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
                showNewLoading(true, getString(R.string.public_loading));
            }
            //门店网络请求
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            if (mDataType == 1) {
                map.put("merchantDataType", mDataType);
            }
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_CHOICE_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }


    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_STORE)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null && msg.getError().getMessage() != null) {
                        MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        } else if (event.getTag().equals(Constants.TAG_BIND_SITE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                case Constants.ON_EVENT_FALSE:
                    bindReceiveSN(mDeviceInfo.getSn());
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getCashPointList() != null && msg.getData().getCashPointList().size() > 0) {
                        Intent intent = new Intent(this, PaySiteChoiceActivity.class);
                        intent.putExtra(Constants.INTENT_USER_ID, getIntent().getStringExtra(Constants.INTENT_USER_ID));
                        intent.putExtra(Constants.INTENT_BIND_STORE_ID, mList.get(mPosition).getStoreId());
                        intent.putExtra(Constants.INTENT_BIND_STORE_NAME, mList.get(mPosition).getStoreName());
                        intent.putExtra(Constants.INTENT_SITE_BINDING, (Serializable) msg.getData().getCashPointList());
                        intent.putExtra(Constants.INTENT_DEVICE_SN, mDeviceInfo.getSn());
                        intent.putExtra(Constants.INTENT_NAME, mIntentName);
                        startActivity(intent);
                    } else {
                        bindReceiveSN(mDeviceInfo.getSn());
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_DEVICE_BIND_DEVICESTOREACTIVITY")) {
            Info msg = (Info) event.getMsg();
            DialogUtil.safeCloseDialog(mLoadDialog);
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceStoreActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(DeviceStoreActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(DeviceStoreActivity.this, getString(R.string.dialog_notice_binding_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            mPosition = -1;
                            startActivity(new Intent(DeviceStoreActivity.this, DeviceListActivity.class));
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        button_title.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                //startActivity(new Intent(this,StoreCodeActivity.class));
                finish();
                break;
            case R.id.button_title:
                bindReceiveSN(mDeviceInfo.getSn());
                break;
            case R.id.btn_ensure:
                if (mPosition == -1) {
                    MyToast.showToastShort(getString(R.string.please_check_bind_store));
                    return;
                }
                if ("2".equals(MyApplication.getCashPointOpenStatus())
                        && "HAPY_POS".equals(mDeviceInfo.getCategoryCode())) {
                    getList(mList.get(mPosition).getStoreId());
                } else {
                    bindReceiveSN(mDeviceInfo.getSn());
                }
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        //只有收款设备和收款码绑定是才去关联收银点
        if (mPosition != -1) {
            if (position != mPosition) {
                mList.get(mPosition).setSelcet(false);
                mList.get(position).setSelcet(true);
            } else {
                if (mList.get(position).isSelcet()) {
                    mList.get(position).setSelcet(false);
                } else {
                    mList.get(position).setSelcet(true);
                }
            }
        } else {
            mList.get(position).setSelcet(true);
        }

        mPosition = position;
        if ("2".equals(mDeviceInfo.getBindSet())) {//必须关联门店
            setButtonEnable(mBtnEnsure, mList.get(mPosition).isSelcet());
        }
        mAdapter.notifyDataSetChanged();
        /*if (mPostion != -1 && "2".equals(MyApplication.getCashPointOpenStatus())
                && "HAPY_POS".equals(mDeviceInfo.getCategoryCode())) {
            getList(mList.get(position).getStoreId());
        } else {
            bindReceiveSN(mDeviceInfo.getSn());
        }*/
    }

    private void bindReceiveSN(String code) {
        if (!NetworkUtils.isNetworkAvailable(DeviceStoreActivity.this)) {
            ToastUtil.showToastShort(getString(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            if (mPosition != -1 && !TextUtils.isEmpty(mList.get(mPosition).getStoreId()) && mList.get(mPosition).isSelcet()) {
                map.put("storeMerchantId", mList.get(mPosition).getStoreId());
            }
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(DeviceStoreActivity.this).deviceBind(DeviceStoreActivity.this, "TAG_DEVICE_BIND_DEVICESTOREACTIVITY", map);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void getList(String storeMerchantId) {
        //门店网络请求
        DialogUtil.safeShowDialog(mLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", 15);
        map.put("currentPage", 1);
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeId", storeMerchantId);
        }
        ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_BIND_SITE_LIST, map);
    }
}
