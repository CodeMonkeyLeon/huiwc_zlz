package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.EmpRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.LinkEmployeeBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/23 15:09
 * @描述: 选择收银员
 */
public class EmpEnsureActivity extends BaseActivity implements View.OnClickListener, EmpRecyclerAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack, mIvClean;
    private TextView mTvTitle, mTvNull, mButton;
    private RecyclerView mRvEmployee;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private EditText mEtInput;

    private EmpRecyclerAdapter mEmpRecyclerAdapter;
    private List<LinkEmployeeBean.DataEntity> employees = new ArrayList<>();
    private String mStoreId;
    private boolean isCashier = false;
    private RelativeLayout mRlInput;
    private LinearLayout mLlEnsure;
    private Button mBtnEnsure;
    private LinkEmployeeBean.DataEntity dataEntity;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ensure_cashier);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mEtInput = findViewById(R.id.et_user_input);
        mIvClean = findViewById(R.id.iv_clean);
        mRlInput = findViewById(R.id.ly_input);
        mLlEnsure = findViewById(R.id.ll_ensure);
        mBtnEnsure = findViewById(R.id.btn_ensure);
        mRlInput.setVisibility(isCashier ? View.GONE : View.VISIBLE);
        mLlEnsure.setVisibility(View.VISIBLE);
        setButtonEnable(mBtnEnsure, true);
        mButton.setVisibility(View.INVISIBLE);
        mEtInput.setHint(R.string.tv_search_store_employee);

        initRecyclerView();
    }

    private void initRecyclerView() {
        mRvEmployee = (RecyclerView) findViewById(R.id.cashier_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvEmployee.setLayoutManager(mLinearLayoutManager);
        mRvEmployee.setItemAnimator(new DefaultItemAnimator());
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchData();
                    return true;
                }
                return false;
            }
        });
        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    mEmpRecyclerAdapter.setDatas(employees);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void initData() {
        mTvTitle.setText(R.string.title_cashier);
        mStoreId = getIntent().getStringExtra(Constants.REQUEST_CASHIER_INTENT);
        employees.clear();
        mEmpRecyclerAdapter = new EmpRecyclerAdapter();
        mEmpRecyclerAdapter.setOnItemClickListener(this);
        mRvEmployee.setAdapter(mEmpRecyclerAdapter);
        String cashierId = getIntent().getStringExtra(Constants.INTENT_CASHIER_ID);
        mEmpRecyclerAdapter.setSelected(cashierId);
        linkEmployeeList(mStoreId);
    }

    private void linkEmployeeList(String storeId) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeMerchantId", storeId);
            ServerClient.newInstance(MyApplication.getContext()).linkEmployeeList(MyApplication.getContext(), Constants.TAG_LINK_EMPLOYEE_LIST, map);
        }
    }

    //模糊查询
    private void searchData() {
        String searchKey = mEtInput.getText().toString().trim();
        if (!TextUtils.isEmpty(searchKey) && employees.size() > 0) {
            List<LinkEmployeeBean.DataEntity> searchEmps = new ArrayList<>();
            for (LinkEmployeeBean.DataEntity employee : employees) {


                if ((employee.getName()!= null && employee.getName().contains(searchKey))
                        || (employee.getPhone()!= null && employee.getPhone().contains(searchKey))) {
                    searchEmps.add(employee);
                }
            }
            mEmpRecyclerAdapter.setDatas(searchEmps);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LINK_EMPLOYEE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LinkEmployeeBean msg = (LinkEmployeeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmpEnsureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    employees.clear();
                    List<LinkEmployeeBean.DataEntity> data = msg.getData();
                    if (data != null && data.size() > 0) {
                        employees.addAll(data);
                        mEmpRecyclerAdapter.setDatas(employees);
                    } else {
                        MyToast.showToastShort(getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                employees.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                mTvNull.setVisibility(View.GONE);
                break;
            case R.id.btn_ensure:
//                if (dataEntity != null) {
                    Intent intent = new Intent();
                    Bundle mBundle = new Bundle();
                    mBundle.putSerializable(Constants.RESULT_CASHIER_INTENT, dataEntity);   //传递一个user对象列表
                    intent.putExtras(mBundle);
                    setResult(RESULT_OK, intent);
//                }
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        dataEntity = employees.get(position);
        if (dataEntity.isSelected()) {
            dataEntity.setSelected(false);
            mEmpRecyclerAdapter.setSelected("");
        } else {
            dataEntity.setSelected(true);
            mEmpRecyclerAdapter.setSelected(dataEntity.getUserId());
        }

    }
}
