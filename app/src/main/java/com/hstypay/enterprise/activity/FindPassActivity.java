package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.IdentifyCodeBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.ImageUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017/7/4.
 */

public class FindPassActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private EditText mEtCode;
    private TextView mTvGetCode;
    private Button btn_next_step;
    private ImageView iv_clean_input;
    private LinearLayout ly_title;
    private EditText et_id;
    private TimeCount time;
    private TextView mTvTitle, mButton;
    private TextView mTvVoiceButton;
    private static final int GET_MESSAGE_CODE = 1;
    private static final int GET_VOICE_CODE = 2;
    private SafeDialog mLoadDialog;
    private RelativeLayout mRlVerifyCodeFindPwd;//rl图形验证码
    private EditText mEtVerifyCodeFindPwd;//et图形验证码
    private ImageView mIvVerifyCodeFindPwd;//iv图形验证码


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        iv_clean_input.setOnClickListener(this);
        mTvGetCode.setOnClickListener(this);
        btn_next_step.setOnClickListener(this);
        mTvVoiceButton.setOnClickListener(this);
        mIvVerifyCodeFindPwd.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(FindPassActivity.this, UIUtils.getString(R.string.public_loading), false);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mEtCode = (EditText) findViewById(R.id.et_code);
        mTvGetCode = (TextView) findViewById(R.id.tv_get_code);
        mTvVoiceButton = (TextView) findViewById(R.id.tv_phone_verify);
        btn_next_step = (Button) findViewById(R.id.btn_next_step);
        iv_clean_input = (ImageView) findViewById(R.id.iv_clean_input);
        ly_title = (LinearLayout) findViewById(R.id.ly_title);
        et_id = (EditText) findViewById(R.id.et_id);//手机号
        mRlVerifyCodeFindPwd = findViewById(R.id.rl_verify_code_find_pwd);
        mEtVerifyCodeFindPwd = findViewById(R.id.et_verify_code_find_pwd);
        mIvVerifyCodeFindPwd = findViewById(R.id.iv_verify_code_find_pwd);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        iv_clean_input.setVisibility(View.VISIBLE);
                    } else {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                }

                if (!StringUtils.isEmptyOrNull(et_id.getText().toString())
                        && !StringUtils.isEmptyOrNull(mEtCode.getText().toString())
                    &&!StringUtils.isEmptyOrNull(mEtVerifyCodeFindPwd.getText().toString())) {
                    setButtonEnable(btn_next_step, true);
                } else {
                    setButtonEnable(btn_next_step, false);
                }

            }
        });
        mEtCode.addTextChangedListener(editTextWatcher);
        et_id.addTextChangedListener(editTextWatcher);
        mEtVerifyCodeFindPwd.addTextChangedListener(editTextWatcher);
        mEtCode.setOnFocusChangeListener(listener);
        et_id.setOnFocusChangeListener(listener);

        setButtonEnable(btn_next_step, false);
        //获取软键盘的焦点
        /*et_id.setFocusable(true);
        et_id.setFocusableInTouchMode(true);
        et_id.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);*/

    }

    private void initData() {
        mTvTitle.setText(R.string.title_telephone_verify);
        mButton.setVisibility(View.INVISIBLE);
        //如果是商户进来
        ly_title.setVisibility(View.VISIBLE);
        btn_next_step.getBackground().setAlpha(102);

    }

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_code:
                    if (hasFocus) {
                        //StatService.trackCustomKVEvent(FindPassActivity.this, "A015", null);
                    }
                    break;
                case R.id.et_id:
                    if (hasFocus) {
                        if (et_id.getText().toString().length() > 0) {
                            iv_clean_input.setVisibility(View.VISIBLE);
                        } else {
                            iv_clean_input.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean_input:
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
                break;
            case R.id.tv_get_code:
                //获取验证码
                MtaUtils.mtaId(FindPassActivity.this, "A005");
                getCode(GET_MESSAGE_CODE);
                break;
            //下一步
            case R.id.btn_next_step:
                MtaUtils.mtaId(FindPassActivity.this, "A007");
                forgetPwd();
                break;
            case R.id.tv_phone_verify:
                getCode(GET_VOICE_CODE);
                break;
            case R.id.iv_verify_code_find_pwd://图形验证码
                getIdentifyingCode();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHECK_PHONE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.SEND_PHONE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(FindPassActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(FindPassActivity.this, getString(R.string.error_request));
                    }
                    break;
                case Constants.SEND_PHONE_TRUE://验证码返回成功
                    MtaUtils.mtaId(FindPassActivity.this, "A006");
                    loadGetCode();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_VOICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.GET_VOICE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(FindPassActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(FindPassActivity.this, getString(R.string.error_request));
                    }
                    break;
                case Constants.GET_VOICE_TRUE:
                    showCommonNoticeDialog(FindPassActivity.this, getString(R.string.dialog_notice_voice_tx), getString(R.string.dialog_notice_voice_after));
                    break;
            }
        }
    }

    //Eventbus接收数据,验证验证码
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckCode(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHECK_CODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_DATA_ERROR:
                    MyToast.showToastShort(getString(R.string.data_error));
                    break;
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.CHECK_CODE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(FindPassActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(FindPassActivity.this, getString(R.string.error_request));
                    }
                    break;
                case Constants.CHECK_CODE_TRUE://验证码返回成功
                    //验证码正确跳转到重置密码页面
                    MtaUtils.mtaId(FindPassActivity.this, "A008");
                    requestMerchant(et_id.getText().toString().trim());
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_RESET_MERCHANT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantIdBean msg = (MerchantIdBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(FindPassActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                                    showCommonNoticeDialog(FindPassActivity.this, msg.getError().getMessage());
                                } else {
                                    showCommonNoticeDialog(FindPassActivity.this, getString(R.string.error_request));
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && msg.getData() != null && msg.getData().size() > 1) {
                        Intent intent = new Intent(FindPassActivity.this, MerchantIdActivity.class);
                        intent.putExtra(Constants.INTENT_RESET_MERCHANT_ID, (Serializable) msg.getData());
                        startActivity(intent);
                    } else {
                        resetPassword();
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_FIND_GETIDENTIFYINGCODE)) {
            //图形验证码
            dismissLoading();
            IdentifyCodeBean msg = (IdentifyCodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.GETIDENTIFYINGCODE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.error_get_verifycode));
                    }
                    break;
                case Constants.GETIDENTIFYINGCODE_TRUE:
                    if (msg!=null && !TextUtils.isEmpty( msg.getData())){
                        String data = msg.getData();
                        Bitmap bitmap = ImageUtil.base64ToBitmap(data);

                        if (bitmap!=null){
                            //Bitmap bitmap1 = ImageUtil.drawBitmapBg(getResources().getColor(R.color.dot_normal_bg), bitmap);
                            if (mRlVerifyCodeFindPwd.getVisibility() != View.VISIBLE){
                                showCommonNoticeDialog(FindPassActivity.this, getString(R.string.tip_input_bitmap_code));
                            }
                            mRlVerifyCodeFindPwd.setVisibility(View.VISIBLE);
                            mIvVerifyCodeFindPwd.setImageBitmap(bitmap);
                        }
                    }
                    break;
            }
        }
    }

    private void resetPassword() {
        Intent intent = new Intent(FindPassActivity.this, ResetPasswordActivity.class);
        intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, et_id.getText().toString().trim());
        startActivity(intent);
    }

    private void requestMerchant(String telPhone) {
        if (NetworkUtils.isNetWorkValid(FindPassActivity.this)) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("telPhone", telPhone);
            ServerClient.newInstance(FindPassActivity.this).getLoginMerchant(FindPassActivity.this, Constants.TAG_GET_RESET_MERCHANT, map);
        } else {
            showCommonNoticeDialog(FindPassActivity.this, getString(R.string.network_exception));
        }
    }

    private void forgetPwd() {
        if (!StringUtils.isEmptyOrNull(et_id.getText().toString())) {
            if (et_id.getText().toString().length() != 11) {
                showCommonNoticeDialog(FindPassActivity.this, getString(R.string.tv_phone_num_error));
                return;
            }
        }
        if (!StringUtils.isEmptyOrNull(mEtCode.getText().toString())) {
            if (mEtCode.getText().toString().length() != 6) {
                showCommonNoticeDialog(FindPassActivity.this, getString(R.string.code_error));
                return;
            }
        }

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(FindPassActivity.this).checkCode(FindPassActivity.this, Constants.TAG_CHECK_CODE, et_id.getText().toString(), mEtCode.getText().toString().trim());
        } else {
            showCommonNoticeDialog(FindPassActivity.this, getString(R.string.network_exception));
        }
    }

    private void getCode(int type) {
        if (!StringUtils.isEmptyOrNull(et_id.getText().toString())) {
            if (et_id.getText().toString().length() != 11) {
                showCommonNoticeDialog(FindPassActivity.this, getString(R.string.tv_phone_num_error));
                return;
            }
        } else {
            showCommonNoticeDialog(FindPassActivity.this, getString(R.string.phone_not_null));
            return;
        }

        if (type == GET_MESSAGE_CODE) {
            if (mRlVerifyCodeFindPwd.getVisibility() != View.VISIBLE){
                //还没有获取到图形验证码，去获取图形验证码
                getIdentifyingCode();
            }else {
                String bitmapCode = mEtVerifyCodeFindPwd.getText().toString().trim();
                if (TextUtils.isEmpty(bitmapCode)){
                    showCommonNoticeDialog(FindPassActivity.this, getString(R.string.tip_input_bitmap_code));
                }else {
                    getMessageCode(bitmapCode);
                }
            }

        } else if (type == GET_VOICE_CODE) {
            getVoiceCode();
        }
    }

    //获取图形验证码
    private void getIdentifyingCode() {
        if (!StringUtils.isEmptyOrNull(et_id.getText().toString())) {
            if (et_id.getText().toString().length() != 11) {
                showCommonNoticeDialog(FindPassActivity.this, getString(R.string.tv_phone_num_error));
                return;
            }
        } else {
            showCommonNoticeDialog(FindPassActivity.this, getString(R.string.phone_not_null));
            return;
        }
        if (NetworkUtils.isNetWorkValid(this)) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(this).getIdentifyingCode(this,Constants.TAG_FIND_GETIDENTIFYINGCODE,2,et_id.getText().toString().trim());
        } else {
            showCommonNoticeDialog(this, getString(R.string.network_exception));
        }
    }


    /**
     * 获取短信验证码
     * @param bitmapCode  图形验证码
     */
    private void getMessageCode(String bitmapCode) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", et_id.getText().toString().trim());
            map.put("verificationCode",bitmapCode);
            ServerClient.newInstance(FindPassActivity.this).getCode(FindPassActivity.this, Constants.TAG_CHECK_PHONE, map);
        } else {
            showCommonNoticeDialog(FindPassActivity.this, getString(R.string.network_exception));
        }
    }

    private void getVoiceCode() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(FindPassActivity.this).getVoiceCode(FindPassActivity.this, Constants.TAG_GET_VOICE, et_id.getText().toString().trim());
        } else {
            showCommonNoticeDialog(FindPassActivity.this, getString(R.string.network_exception));
        }
    }

    private void loadGetCode() {
        Countdown();
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();

    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            mTvGetCode.setEnabled(true);
            mTvGetCode.setClickable(true);
            mTvGetCode.setText(R.string.bt_code_get);
            mTvGetCode.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            mTvGetCode.setEnabled(false);
            mTvGetCode.setClickable(false);
            mTvGetCode.setTextColor(getResources().getColor(R.color.user_edit_color));
            mTvGetCode.setText(getString(R.string.verify_code_count_down) + millisUntilFinished / 1000 + "s");
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
