package com.hstypay.enterprise.activity;

import android.content.Context;
import android.widget.ImageView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.Constants;
import com.squareup.picasso.Picasso;
import com.youth.banner.loader.ImageLoader;

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        // Picasso.with( MyApplication.getContext()).load(imgPath).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(image);
        if ((Constants.H5_BASE_URL + Constants.URL_DEFAULT_BANNER_BOUNTY).equals(path.toString())) {
            Picasso.get().load(R.mipmap.banner_bounty_default).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(imageView);
        }else if ((Constants.H5_BASE_URL + Constants.URL_DEFAULT_BANNER_HUABEI).equals(path.toString())) {
            Picasso.get().load(R.mipmap.banner_huabei_default).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(imageView);
        }else {
            Picasso.get().load(path + "").placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(imageView);
        }
        //
        // Uri uri = Uri.parse(path+"");
        //  imageView.setImageURI(uri);
       /* Glide.with(context)
                .load(path).placeholder(R.mipmap.banner).error(R.mipmap.icon_general_noloading)
                .crossFade()
                .into(imageView);*/
    }
}
