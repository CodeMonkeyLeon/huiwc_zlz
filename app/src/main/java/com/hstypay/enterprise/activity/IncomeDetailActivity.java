package com.hstypay.enterprise.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.ServiceChargeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportDetailBean;
import com.hstypay.enterprise.bean.ReportIncomeBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class IncomeDetailActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private LinearLayout mLlContent;
    private RelativeLayout mRlSelectMonth;
    private TextView mTvTitle, mButton, mTvReportBefore, mTvReportDate, mTvReportAfter, mTvTradeNetIncome, mTvActualReceiveMoney, mTvTradeMoney, mTvTradeCount, mTvRefundMoney, mTvRefundCount, mTvCouponMoney, mTvServiceMoney, mTvDateRange, mTvNotDate;
    private RecyclerView mRvServiceChargeDetail;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private ArrayList<Object> mList;
    private ServiceChargeAdapter mRateRecyclerAdapter;
    private String month = "";
    private String startTime = "";
    private String endTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton.setVisibility(View.INVISIBLE);

        mLlContent = (LinearLayout) findViewById(R.id.ll_content);

        mRlSelectMonth = (RelativeLayout) findViewById(R.id.rl_select_month);
        mTvReportBefore = (TextView) findViewById(R.id.tv_report_before);
        mTvReportDate = (TextView) findViewById(R.id.tv_report_date);
        mTvReportAfter = (TextView) findViewById(R.id.tv_report_after);

        mTvDateRange = (TextView) findViewById(R.id.tv_date_range);
        mTvNotDate = (TextView) findViewById(R.id.tv_not_data);

        mTvTradeNetIncome = (TextView) findViewById(R.id.tv_trade_net_income);
        mTvActualReceiveMoney = (TextView) findViewById(R.id.tv_actual_receive_money);
        mTvTradeMoney = (TextView) findViewById(R.id.tv_trade_money);
        mTvTradeCount = (TextView) findViewById(R.id.tv_trade_count);

        mTvRefundMoney = (TextView) findViewById(R.id.tv_refund_money);
        mTvRefundCount = (TextView) findViewById(R.id.tv_refund_count);
        mTvCouponMoney = (TextView) findViewById(R.id.tv_coupon_money);
        mTvServiceMoney = (TextView) findViewById(R.id.tv_service_charge_total);

        mRvServiceChargeDetail = (RecyclerView) findViewById(R.id.rv_service_charge_detail);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mTvReportBefore.setOnClickListener(this);
        mTvReportAfter.setOnClickListener(this);
    }

    public void initData() {
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvServiceChargeDetail.setLayoutManager(mLinearLayoutManager);
        mList = new ArrayList<>();
        mRateRecyclerAdapter = new ServiceChargeAdapter(MyApplication.getContext(), mList);
        mRvServiceChargeDetail.setAdapter(mRateRecyclerAdapter);

        int tab = getIntent().getIntExtra(Constants.INTENT_REPORT_TAB, 0);
        if (tab == 2) {
            mTvTitle.setText(R.string.title_report_month);
            month = getIntent().getStringExtra(Constants.INTENT_REPORT_MONTH);
            if (!TextUtils.isEmpty(month)) {
                getReportMonthDetail(month);
                setAfterButton();
                mTvReportDate.setText(DateUtil.formartDateToYYMM(month));
                mRlSelectMonth.setVisibility(View.VISIBLE);
                mTvDateRange.setVisibility(View.GONE);
            }
        } else if (tab == 1) {
            startTime = getIntent().getStringExtra(Constants.INTENT_REPORT_START_TIME);
            endTime = getIntent().getStringExtra(Constants.INTENT_REPORT_END_TIME);
            if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
                try {
                    if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formartDateToMMDD(endTime))) {
                        mTvDateRange.setText(getString(R.string.tv_income_date) + DateUtil.formartDateToYYMMDD(startTime));
                    } else {
                        mTvDateRange.setText(getString(R.string.tv_income_date) + DateUtil.formartDateToYYMMDD(startTime) + " " + getString(R.string.tv_least) + " "
                                + DateUtil.formartDateToYYMMDD(endTime));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                getReportDayDetail(startTime, endTime);
            }
            mTvDateRange.setVisibility(View.VISIBLE);
            mRlSelectMonth.setVisibility(View.GONE);
            mTvTitle.setText(R.string.title_report_day);
        }
    }


    private void getReportDayDetail(String startTime, String endTime) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("startTime", startTime);
            map.put("endTime", endTime);
            ServerClient.newInstance(MyApplication.getContext()).reportDayDetail(MyApplication.getContext(), Constants.TAG_REPORT_DAY_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getReportMonthDetail(String payTradeTime) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("payTradeTime", payTradeTime);
            ServerClient.newInstance(MyApplication.getContext()).reportMonthDetail(MyApplication.getContext(), Constants.TAG_REPORT_MONTH_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_REPORT_MONTH_DETAIL) || event.getTag().equals(Constants.TAG_REPORT_DAY_DETAIL)) {
            dismissLoading();
            ReportIncomeBean msg = (ReportIncomeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(IncomeDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    ReportDetailBean data = msg.getData();
                    if (data != null) {
                        /*mLlContent.setVisibility(View.VISIBLE);
                        mTvNotDate.setVisibility(View.GONE);*/
                        mTvTradeNetIncome.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(data.getPayNetFee()));
                        mTvActualReceiveMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(data.getPayFee()));
                        mTvTradeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(data.getSuccessFee()));
                        mTvTradeCount.setText(data.getSuccessCount() + getString(R.string.tv_count));

                        mTvRefundMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(data.getRefundFee()));
                        mTvRefundCount.setText(data.getRefundCount() + getString(R.string.tv_count));
                        mTvCouponMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(data.getMchDiscountsFee()));
                        mTvServiceMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(data.getCommissionFee()));
                    } else {
                        mTvTradeNetIncome.setText(getString(R.string.tx_mark) + 0.00);
                        mTvActualReceiveMoney.setText(getString(R.string.tx_mark) + 0.00);
                        mTvTradeMoney.setText(getString(R.string.tx_mark) + 0.00);
                        mTvTradeCount.setText(0 + getString(R.string.tv_count));

                        mTvRefundMoney.setText(getString(R.string.tx_mark) + 0.00);
                        mTvRefundCount.setText(0 + getString(R.string.tv_count));
                        mTvCouponMoney.setText(getString(R.string.tx_mark) + 0.00);
                        mTvServiceMoney.setText(getString(R.string.tx_mark) + 0.00);
                        //mTvNotDate.setVisibility(View.VISIBLE);
                        //mLlContent.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_report_before:
                MtaUtils.mtaId(IncomeDetailActivity.this, "K004");
                month = DateUtil.getBeforeMonth(month);
                getReportMonthDetail(month);
                setBeforeButton();
                setAfterButton();
                mTvReportDate.setText(DateUtil.formartDateToYYMM(month));
                break;
            case R.id.tv_report_after:
                MtaUtils.mtaId(IncomeDetailActivity.this, "K004");
                month = DateUtil.getAfterMonth(month);
                getReportMonthDetail(month);
                setBeforeButton();
                setAfterButton();
                mTvReportDate.setText(DateUtil.formartDateToYYMM(month));
                break;
            default:
                break;
        }
    }

    private boolean afterDateRange(String month) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(month);
            Calendar nowDate = Calendar.getInstance();
            int nowYear = nowDate.get(Calendar.YEAR);
            int nowMonth = nowDate.get(Calendar.MONTH);
            Calendar selectDate = Calendar.getInstance();
            selectDate.setTime(date);
            int selectYear = selectDate.get(Calendar.YEAR);
            int selectMonth = selectDate.get(Calendar.MONTH);
            if (nowYear == selectYear) {
                if (nowMonth > selectMonth + 1) {
                    return true;
                }
            } else {
                if (11 + nowMonth > selectMonth) {
                    return true;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void setAfterButton() {
        if (afterDateRange(month)) {
            mTvReportAfter.setEnabled(true);
            mTvReportAfter.setTextColor(UIUtils.getColor(R.color.black));
        } else {
            mTvReportAfter.setEnabled(false);
            mTvReportAfter.setTextColor(UIUtils.getColor(R.color.home_text_expect));
        }
    }

    private boolean beforeDateRange(String month) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(month);
            Calendar nowDate = Calendar.getInstance();
            int nowYear = nowDate.get(Calendar.YEAR);
            int nowMonth = nowDate.get(Calendar.MONTH);
            Calendar selectDate = Calendar.getInstance();
            selectDate.setTime(date);
            int selectYear = selectDate.get(Calendar.YEAR);
            //selectDate.add(Calendar.MONTH,6);
            int selectMonth = selectDate.get(Calendar.MONTH);
            if (nowYear == selectYear) {
                if (nowMonth < selectMonth + 6) {
                    return true;
                }
            } else {
                if (6 + nowMonth < selectMonth) {
                    return true;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void setBeforeButton() {
        if (beforeDateRange(month)) {
            mTvReportBefore.setEnabled(true);
            mTvReportBefore.setTextColor(UIUtils.getColor(R.color.black));
        } else {
            mTvReportBefore.setEnabled(false);
            mTvReportBefore.setTextColor(UIUtils.getColor(R.color.home_text_expect));
        }
    }
}
