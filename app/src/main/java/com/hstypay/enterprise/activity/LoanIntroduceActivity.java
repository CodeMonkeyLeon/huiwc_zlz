package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.RSA.AESUtil;
import com.hstypay.enterprise.utils.RSA.RSAUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

import java.security.PublicKey;

import javax.crypto.SecretKey;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class LoanIntroduceActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mBtnTitle;
    private Button mBtnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_introduce);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvTitle.setText(R.string.title_loan_introduce);
        mBtnTitle.setVisibility(View.INVISIBLE);

    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                MtaUtils.mtaId(LoanIntroduceActivity.this, "H002");
                loan("{\"mobilePhone\":\"" + MyApplication.getLoginPhone() + "\"}");
                break;
        }
    }


    private void loan(String phone) {
        String publicKeyStr = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy7+ptuC9B8yLLWTYhqoY8cM2mwePWvDHj7ajuoM/SClhc9Cli/X2AnQ4SgJamxw39wJehH+9j4Dk+HZpuRYjldTv3HuqGdfcWrZXMl9EnbAmb68bdWcUxKKqHeUY2AbyXPRxDY5pGeer9HvLeqoych0q4ukKK9hzmeF5XDYe6iEv15GGwfUWv3BqmpscyN3sod8DB/km8fF0wHj01zyPVuwRw2/O8gv6M+Sl3RQemkuVN1na3EI9g7FbtsVI5cys7nC08vKoPmuXIOcTRQKEtcBtSnMUWKfyOLC6AdlhTZ96n9nwgXdI94NgI9aUE2B1qFmDSyoc2Itd6P5V5PAITQIDAQAB";
        //将Base64编码后的公钥转换成PublicKey对象
        try {
            PublicKey publicKey = RSAUtil.string2PublicKey(publicKeyStr);
            // 生成AES秘钥，并Base64编码
            String aesKeyStr = AESUtil.genKeyAES();
            LogUtil.d("AES秘钥Base64编码:" + aesKeyStr);
            //用公钥加密AES秘钥
            byte[] publicEncrypt = RSAUtil.publicEncrypt(aesKeyStr.getBytes(), publicKey);
            //公钥加密AES秘钥后的内容Base64编码
            String publicEncryptStr = RSAUtil.byte2Base64(publicEncrypt);
            LogUtil.d("公钥加密AES秘钥并Base64编码的结果：" + publicEncryptStr);
            //将Base64编码后的AES秘钥转换成SecretKey对象
            SecretKey aesKey = AESUtil.loadKeyAES(aesKeyStr);
            //用AES秘钥加密实际的内容
            byte[] encryptAES = AESUtil.encryptAES(phone.getBytes(), aesKey);
            // AES秘钥加密后的内容Base64编码
            String encryptAESStr = AESUtil.byte2Base64(encryptAES);
            LogUtil.d("AES秘钥加密实际的内容并Base64编码的结果：" + encryptAESStr);

            String loanUrl = "http://apitest.51xcm.cn/loan/v1/home/unionLogin?entryKey=" + publicEncryptStr + "&data=" + encryptAESStr;
            LogUtil.d("加密后的链接：" + loanUrl);
            Intent intent = new Intent(LoanIntroduceActivity.this, RegisterActivity.class);
            intent.putExtra(Constants.REGISTER_INTENT, loanUrl);
            intent.putExtra(Constants.REGISTER_ALLOW_CLOSE, true);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
            MyToast.showToastShort(getString(R.string.data_error));
        }

    }
}
