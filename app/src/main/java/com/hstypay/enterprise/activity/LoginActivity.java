package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.BuildConfig;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.ExchangeDialog;
import com.hstypay.enterprise.Widget.MerchantSelectPopup;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.PrivacyProtocolPopupWindow;
import com.hstypay.enterprise.Widget.ProtocolPopupWindow;
import com.hstypay.enterprise.adapter.AccountItemAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.IdentifyCodeBean;
import com.hstypay.enterprise.bean.LoginBean;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.ImageUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/7/5.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private EditText user_name, userPwd;
    private View mLine, mViewLine;
    private TextView mTvSetWifi;
    private TextView mTvMsgLogin;
    private ImageView showPwd, mIvClean, mIv_login;
    private ImageButton mIbLogin;
    private Button btn_regist, btn_forget_pwd, mBtnLogin;
    private String mName, mPwd;
    private int srollHeight = 0;
    private boolean isFristIn = true;
    private RelativeLayout mRlLogin;
    public static LoginActivity instance = null;
    private boolean isOpenPwd;
    private LinearLayout mLlBtn, mLlLoginBttom;
    private final static int COUNTS = 5;//点击次数
    private final static long DURATION = 3 * 1000L;//规定有效时间
    private long[] mHits = new long[COUNTS];
    private ExchangeDialog mDialog;
    private RecyclerView mRecyclerView;
    private boolean mSwitch;
    private AccountItemAdapter mAdapter;
    private List<String> mRecords;
    private PrivacyProtocolPopupWindow mUserProtocolPopupWindow;
    private ProtocolPopupWindow mInfoProtocolPopupWindow;
    private boolean inited;
    private SafeKeyboard safeKeyboard;
    private RelativeLayout mRlVerifyCodeLogin;
    private ImageView mIvVerifyCodeLogin;//图形验证码
    private EditText mEtVerifyCodeLogin;
    private MerchantSelectPopup mMerchantPopupWindow;
    private CommonNoticeDialog mDialogReset;
    private int viewHeight;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (AppHelper.getApkType() == 2) {
            StatusBarUtil.setTranslucentStatus(this);
        } else {
            StatusBarUtil.setImmersiveStatusBar(this, true);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        instance = this;
        initView();
        initListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        SpUtil.removeAll();
        isOpenPwd = false;
        showPwd.setImageResource(R.mipmap.ic_login_password_hiding);
        userPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        initConfigView();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        userPwd.setText("");
        if (!TextUtils.isEmpty(user_name.getText().toString().trim())) {
            userPwd.setFocusable(true);
            userPwd.setFocusableInTouchMode(true);
            userPwd.requestFocus();
            setViewHeight(userPwd);
        } else {
            user_name.setFocusable(true);
            user_name.setFocusableInTouchMode(true);
            user_name.requestFocus();
            setViewHeight(user_name);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void initListener() {
        showPwd.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        btn_forget_pwd.setOnClickListener(this);
        mIbLogin.setOnClickListener(this);
        mBtnLogin.setOnClickListener(this);
        btn_regist.setOnClickListener(this);
        mIvVerifyCodeLogin.setOnClickListener(this);
        mTvMsgLogin.setOnClickListener(this);
        if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mTvSetWifi.setOnClickListener(this);
            mTvSetWifi.setVisibility(View.VISIBLE);
        }
        if (!BuildConfig.IS_OFFICIAL) {
            mIv_login.setOnClickListener(this);
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setButtonState();
            }
        };

        user_name.addTextChangedListener(textWatcher);
        userPwd.addTextChangedListener(textWatcher);
    }

    private void initView() {
        MtaUtils.mtaId(LoginActivity.this, "A001");
        mRlLogin = (RelativeLayout) findViewById(R.id.login_top);
        mLlBtn = (LinearLayout) findViewById(R.id.ll_btn_register);
        user_name = (EditText) findViewById(R.id.user_name);
        userPwd = (EditText) findViewById(R.id.userPwd);
        showPwd = (ImageView) findViewById(R.id.iv_eye_pwd);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mLine = findViewById(R.id.line);
        mIbLogin = findViewById(R.id.ib_login);
        mBtnLogin = findViewById(R.id.btn_submit);
        btn_regist = (Button) findViewById(R.id.btn_register);
        btn_forget_pwd = (Button) findViewById(R.id.btn_forget_pwd);
        mIv_login = (ImageView) findViewById(R.id.iv_login);
        mTvSetWifi = (TextView) findViewById(R.id.tv_set_wifi);
        mRlVerifyCodeLogin = findViewById(R.id.rl_verify_code_login);
        mIvVerifyCodeLogin = findViewById(R.id.iv_verify_code_login);
        mEtVerifyCodeLogin = findViewById(R.id.et_verify_code_login);
        mTvMsgLogin = findViewById(R.id.tv_msg_login);

        mLlLoginBttom = (LinearLayout) findViewById(R.id.ll_login_bottom);
        mViewLine = findViewById(R.id.view_line);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecords = new ArrayList<>();
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new AccountItemAdapter(MyApplication.getContext(), mRecords);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new AccountItemAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (getRecords() != null)
                    user_name.setText(getRecords().get(position));
                userPwd.setFocusable(true);
                userPwd.setFocusableInTouchMode(true);
                userPwd.requestFocus();
                setViewHeight(userPwd);
                mLlLoginBttom.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        });
        mAdapter.setOnItemDeleteClickListener(new AccountItemAdapter.OnRecyclerDeleteClickListener() {
            @Override
            public void onItemDelete(int position) {
                deleteRecord(mRecords, position);
            }
        });

        if (getRecords() != null) {
            mRecords.addAll(getRecords());
        }
        if (mRecords != null && mRecords.size() > 0) {
            user_name.setText(mRecords.get(0));
            user_name.setSelection(user_name.getText().toString().trim().length());
        } else {
            mIvClean.setVisibility(View.INVISIBLE);
        }
        setButtonState();
        /*if (AppHelper.getAppType() == 1) {
            mLlBtn.setVisibility(View.GONE);
            controlKeyboardLayout(mRlLogin, btn_login);
        } else if (AppHelper.getAppType() == 2) {
            mLlBtn.setVisibility(View.VISIBLE);
            controlKeyboardLayout(mRlLogin, btn_forget_pwd);
        }*/

        View rootView = findViewById(R.id.main_root);
        final LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(user_name.getId(), user_name);
        safeKeyboard.putEditText(userPwd.getId(), userPwd);
        safeKeyboard.putEditText(mEtVerifyCodeLogin.getId(), mEtVerifyCodeLogin);
        safeKeyboard.setOnKeyboardShowListener(new SafeKeyboard.OnKeyboardShowListener() {
            @Override
            public void onKeyboardShow(boolean show) {
                if (show) {
                    LogUtil.d("viewHeight--" + viewHeight);
                    controlKeyboardLayout(mRlLogin, viewHeight - 300);
                } else {
                    LogUtil.d("viewHeight-1-" + viewHeight);
                    controlKeyboardLayout(mRlLogin, 0);
                }
            }
        });
        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    setViewHeight(view);
                    if (safeKeyboard.isShow()) {
                        LogUtil.d("viewHeight-2-" + viewHeight + "--" + view.getId());
                        controlKeyboardLayout(mRlLogin, viewHeight - 300);
                    }
                }
            }
        };
        user_name.setOnFocusChangeListener(onFocusChangeListener);
        userPwd.setOnFocusChangeListener(onFocusChangeListener);
        mEtVerifyCodeLogin.setOnFocusChangeListener(onFocusChangeListener);

        if (AppHelper.getApkType() == 2) {
            mViewLine.setVisibility(View.GONE);
        }

    }

    private void setViewHeight(View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        viewHeight = location[1];//获取当前位置的纵坐标
        LogUtil.d("viewHeight-3-" + viewHeight + "--" + view.getId());
    }

    private void initConfigView() {
        String config = MyApplication.getConfig();
        if (!TextUtils.isEmpty(config) && config.contains(Constants.CONFIG_FORGET)) {
            btn_forget_pwd.setVisibility(View.VISIBLE);
        } else {
            btn_forget_pwd.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(config) && config.contains(Constants.CONFIG_REGISTER)) {
            btn_regist.setVisibility(View.VISIBLE);
        } else {
            btn_regist.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(config) && config.contains(Constants.CONFIG_FORGET) && config.contains(Constants.CONFIG_REGISTER)) {
            mViewLine.setVisibility(View.VISIBLE);
        } else {
            mViewLine.setVisibility(View.GONE);
        }
    }

    private void deleteRecord(List<String> accounts, int position) {
        Gson gson = new Gson();
        accounts.remove(position);
        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_RECORD, gson.toJson(accounts));
        mAdapter.notifyDataSetChanged();
        if (accounts.size() == 0) {
            mIvClean.clearAnimation();
            mIvClean.setVisibility(View.INVISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mLlLoginBttom.setVisibility(View.VISIBLE);
        }
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return !isOpen;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && !inited) {
            inited = true;
            if (!MyApplication.protocolShown()) {
                showUserProtocol();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_verify_code_login://图形验证码
                getIdentifyingCode();
                break;
            case R.id.iv_eye_pwd:
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd, showPwd, userPwd);
                break;
            //登录
            case R.id.ib_login:
            case R.id.btn_submit:
                MtaUtils.mtaId(LoginActivity.this, "A002");
                login();
                break;
            //注册
            case R.id.btn_register:
                MtaUtils.mtaId(LoginActivity.this, "A011");
                showInfoProtocol(Constants.URL_INFO_PROTOCOL);
                break;
            //忘记密码
            case R.id.btn_forget_pwd:
                MtaUtils.mtaId(LoginActivity.this, "A004");
                Intent intent = new Intent(LoginActivity.this, FindPassActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_set_wifi:
                setWIFI();
                break;
            case R.id.tv_msg_login:
                startActivity(new Intent(LoginActivity.this, LoginMsgActivity.class));
                break;
            case R.id.iv_clean:
                if (TextUtils.isEmpty(user_name.getText().toString().trim())) {
                    Animation rotate;
                    if (mSwitch) {
                        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
                        mSwitch = false;
                        mLlLoginBttom.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
                        mSwitch = true;
                        mLlLoginBttom.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }
                    rotate.setInterpolator(new LinearInterpolator());
                    rotate.setFillAfter(true);
                    mIvClean.startAnimation(rotate);
                } else {
                    mSwitch = false;
                    mIvClean.clearAnimation();
                    user_name.setText("");
                    user_name.setFocusable(true);
                    user_name.setFocusableInTouchMode(true);
                    user_name.requestFocus();
                    setViewHeight(user_name);
                }
                break;
            case R.id.iv_login:
                System.arraycopy(mHits, 1, mHits, 0, mHits.length - 1);
                //实现左移，然后最后一个位置更新距离开机的时间，如果最后一个时间和最开始时间小于DURATION，即连续5次点击
                mHits[mHits.length - 1] = SystemClock.uptimeMillis();
                if (mHits[0] >= (SystemClock.uptimeMillis() - DURATION)) {
                    mHits = new long[COUNTS];
                    showExchangeDialog();
                }
                break;
        }
    }

    private void toRegister() {
        Intent toRegisterIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        toRegisterIntent.putExtra(Constants.REGISTER_INTENT, Constants.URL_REGISTER_FIRST);
        startActivity(toRegisterIntent);
    }

    private void showUserProtocol() {
        if (mUserProtocolPopupWindow == null) {
            mUserProtocolPopupWindow = new PrivacyProtocolPopupWindow(LoginActivity.this, new PrivacyProtocolPopupWindow.OnClickEnsureListener() {
                @Override
                public void agreeProtocol() {
                    mUserProtocolPopupWindow.dismiss();
                    SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PROTOCOL_SHOWN + AppHelper.getVerCode(MyApplication.getContext()), true);
                    StatService.setAuthorizedState(MyApplication.getContext(), true);
                }
            }, new PrivacyProtocolPopupWindow.OnClickCancelListener() {
                @Override
                public void notAgreeProtocol() {
                    MyToast.showToastShort(getString(R.string.toast_user_protocol));
                }
            });
        }
        mUserProtocolPopupWindow.showAtLocation(mLlLoginBttom, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void showInfoProtocol(String url) {
        mInfoProtocolPopupWindow = new ProtocolPopupWindow(LoginActivity.this, url, true, new ProtocolPopupWindow.OnClickEnsureListener() {
            @Override
            public void agreeProtocol() {
                mInfoProtocolPopupWindow.dismiss();
                toRegister();
            }
        }, new ProtocolPopupWindow.OnClickCancelListener() {
            @Override
            public void notAgreeProtocol() {
                MyToast.showToastShort(getString(R.string.toast_info_protocol));
            }
        });
        mInfoProtocolPopupWindow.showAtLocation(mLlLoginBttom, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void setWIFI() {
        Intent i = new Intent();
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            i.setClassName("com.android.settings", "com.android.settings.Settings$WifiSettingsActivity");
        } else {
            i.setClassName("com.android.settings"
                    , "com.android.settings.wifi.WifiSettings");
        }
        startActivity(i);
    }

    private void showMerchantPopup(final List<MerchantIdBean.DataEntity> data) {
        mMerchantPopupWindow = new MerchantSelectPopup(LoginActivity.this, data);
        mMerchantPopupWindow.setOnClickItemListener(new MerchantSelectPopup.OnClickItemListener() {
            @Override
            public void clickItem(int position) {
                /*user_name.setText(data.get(position).getOrgId());
                user_name.setSelection(user_name.getText().toString().trim().length());
                userPwd.setText("");*/
                mchLogin(data.get(position).getOrgId());
            }
        });
        mMerchantPopupWindow.showAtLocation(mLlBtn, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void showExchangeDialog() {
        if (mDialog == null) {
            mDialog = new ExchangeDialog(LoginActivity.this);
            DialogHelper.resize(LoginActivity.this, mDialog);
        }
        mDialog.show();
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LOGIN) || event.getTag().equals("TAG_MERCHANT_LOGIN_" + this.getClass().getSimpleName())) {
            dismissLoading();
            LoginBean msg = (LoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    if (event.getTag().equals("TAG_MERCHANT_LOGIN_" + this.getClass().getSimpleName())) {
                        SpUtil.removeKey(Constants.SKEY);
                        SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    }
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (event.getTag().equals("TAG_MERCHANT_LOGIN_" + this.getClass().getSimpleName())) {
                        SpUtil.removeKey(Constants.SKEY);
                        SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    }
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.LOGIN_ERROR_CODE_VERIFY_ERROR) || msg.getError().getCode().equals(Constants.LOGIN_ERROR_CODE_VERIFY_NULL)) {
                                //获取图形验证码
                                getIdentifyingCode();
                                if (mRlVerifyCodeLogin.getVisibility() != View.GONE) {
                                    if (msg.getError().getMessage() != null) {
                                        showCommonNoticeDialog(LoginActivity.this, msg.getError().getMessage());
                                    }
                                }
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LoginActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(LoginActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://登录成功
                    MtaUtils.mtaId(LoginActivity.this, "A003");
                    stayRecord();
                    SpUtil.putString(LoginActivity.this, Constants.CASHIER_REFUND_TAG, "");
                    SpUtil.putString(LoginActivity.this, Constants.CASHIER_BILL_TAG, "");
                    SpUtil.putString(LoginActivity.this, Constants.CASHIER_REPORT_TAG, "");
                    SpUtil.putString(LoginActivity.this, Constants.FUNC_CODE_REFUND, "");
                    SpUtil.putString(LoginActivity.this, Constants.PERMISSIONS_REFUND, "");
                    if (msg.getData() != null) {
                        boolean isShowWSY = msg.getData().isShowWSY();
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_SHOW_WSY, isShowWSY);
                        SpUtil.putString(MyApplication.getContext(), Constants.SIGN_KEY, msg.getData().getSignKey());//签名的key
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST, msg.getData().isStandardFlag());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM, msg.getData().isStandardIndexFlag());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BELONG_HSHF, msg.getData().isBelonedHSHFFlag());
                        if (msg.getData().getUser() != null) {
                            int userId = msg.getData().getUser().getUserId();
                            SpUtil.putString(MyApplication.getContext(), Constants.USER_ID, userId + "");//用户id
                            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_STAY_USER_ID, userId + "");//用户id
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, msg.getData().getUser().getAttestationStatus());//商户认证状态
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_WXREG_STATUS, msg.getData().getUser().getWxRegStatus());
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_ALIREG_STATUS, msg.getData().getUser().getAliRegStatus());
                            boolean isUser = msg.getData().getUser().isMerchantFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_MERCHANT_FLAG, isUser);//是否是超管(商户)
                            //是否进件成功
                            String orgId = msg.getData().getUser().getOrgId();
                            SpUtil.putString(MyApplication.getContext(), Constants.IS_SUCCESS_DATA, orgId);
                            //是否是管理员
                            boolean isAdmin = msg.getData().getUser().isAdminFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_ADMIN_FLAG, isAdmin);
                            //是否是收银员
                            boolean isCasher = msg.getData().getUser().isCasherFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CASHER_FLAG, isCasher);
                            //是否是店长
                            boolean isManager = msg.getData().getUser().isStoreManagerFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_MANAGER_FLAG, isManager);

                            //realname
                            String realName = msg.getData().getUser().getRealName();
                            SpUtil.putString(MyApplication.getContext(), Constants.REALNAME, realName);
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.NEED_CHANGE_PWD, msg.getData().getUser().isUpdatePwdFlag());
                            SpUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE, msg.getData().getUser().getLoginPhone());
                            //用户名
                            SpUtil.putString(MyApplication.getContext(), Constants.APP_USERNAME, msg.getData().getUser().getUserName());
                            SpUtil.putString(MyApplication.getContext(), Constants.APP_LOGIN_NAME, msg.getData().getUser().getLoginName());

                            if (msg.getData().getUser().getEmp() != null) {
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getEmp().getEmpId())) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_EMP_ID, msg.getData().getUser().getEmp().getEmpId());
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_EMP_ID, "");
                                }
                            }
                            //商户号
                            if (msg.getData().getUser().getMerchant() != null) {
//                                    SpUtil.putInt(MyApplication.getContext(), Constants.SP_PLEDGE_STATUS, msg.getData().getUser().getMerchant().getPreauthStatus());
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getRewardFlage())) {
                                    SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BOUNTY_MERCHANT, true);
                                } else {
                                    SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BOUNTY_MERCHANT, false);
                                }
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getMerchantId())) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_ID, msg.getData().getUser().getMerchant().getMerchantId());
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_ID, "");
                                }
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getMerchantType())) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_TYPE, msg.getData().getUser().getMerchant().getMerchantType());
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_TYPE, "");
                                }
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getMerchantName())) {
                                    String merchant_name = msg.getData().getUser().getMerchant().getMerchantName();//商户名称
                                    SpUtil.putString(MyApplication.getContext(), Constants.MERCHANT_NAME, merchant_name);
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.MERCHANT_NAME, "");
                                }
                            }
                        } else {
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, 0);//商户认证状态
                        }
                        if (msg.getData().getOps() != null && msg.getData().getOps().getUserOps() != null && msg.getData().getOps().getUserOps().size() > 0) {
                            List<LoginBean.DataBean.OpsBean.UserOpsBean> userOps = msg.getData().getOps().getUserOps();
                            for (int u = 0; u < userOps.size(); u++) {
                                LoginBean.DataBean.OpsBean.UserOpsBean userOpsBean = userOps.get(u);
                                if (userOpsBean != null) {
                                    if (!StringUtils.isEmptyOrNull(userOpsBean.getOpCode())) {
                                        if (userOpsBean.getOpCode().equals("APPLY-REFUND")) {
                                            //收银员退款权限
                                            SpUtil.putString(LoginActivity.this, Constants.CASHIER_REFUND_TAG, userOpsBean.getOpCode());
                                        }
                                        if (userOpsBean.getOpCode().equals("APPLY-WATER")) {
                                            //收银员查看流水权限
                                            SpUtil.putString(LoginActivity.this, Constants.CASHIER_BILL_TAG, userOpsBean.getOpCode());
                                        }
                                        if (userOpsBean.getOpCode().equals("APPLY-BILL")) {
                                            //收银员查看统计权限
                                            SpUtil.putString(LoginActivity.this, Constants.CASHIER_REPORT_TAG, userOpsBean.getOpCode());
                                        }
                                    }
                                } else {
                                    SpUtil.putString(LoginActivity.this, Constants.CASHIER_REFUND_TAG, "");
                                    SpUtil.putString(LoginActivity.this, Constants.CASHIER_BILL_TAG, "");
                                    SpUtil.putString(LoginActivity.this, Constants.CASHIER_REPORT_TAG, "");
                                }
                            }
                        } else {
                            SpUtil.putString(LoginActivity.this, Constants.CASHIER_REFUND_TAG, "");
                            SpUtil.putString(LoginActivity.this, Constants.CASHIER_BILL_TAG, "");
                            SpUtil.putString(LoginActivity.this, Constants.CASHIER_REPORT_TAG, "");
                        }

                        if (msg.getData().getFuncs() != null && msg.getData().getFuncs().size() > 0) {
                            List<LoginBean.DataBean.FuncsBean> funcs = msg.getData().getFuncs();
                            for (int i = 0; i < funcs.size(); i++) {
                                LoginBean.DataBean.FuncsBean s = funcs.get(i);
                                if (s != null) {
                                    if ("APP-OPERATION".equals(s.getFuncCode())) {
                                        SpUtil.putString(LoginActivity.this, Constants.FUNC_CODE_REFUND, s.getFuncCode());
                                    } else {
                                        SpUtil.putString(LoginActivity.this, Constants.FUNC_CODE_REFUND, "");
                                    }
                                    List<LoginBean.DataBean.FuncsBean.PermissionsBean> permissions = s.getPermissions();
                                    if (permissions != null && permissions.size() > 0) {
                                        for (int p = 0; p < permissions.size(); p++) {
                                            LoginBean.DataBean.FuncsBean.PermissionsBean permissionsBean = permissions.get(p);
                                            if (permissionsBean != null && "APPLY-REFUND".equals(permissionsBean.getPermissionCode())) {
                                                //有退款权限
                                                SpUtil.putString(LoginActivity.this, Constants.PERMISSIONS_REFUND, permissionsBean.getPermissionCode());
                                            } else {
                                                SpUtil.putString(LoginActivity.this, Constants.PERMISSIONS_REFUND, "");
                                            }
                                        }
                                    } else {
                                        SpUtil.putString(LoginActivity.this, Constants.PERMISSIONS_REFUND, "");
                                    }
                                } else {
                                    SpUtil.putString(LoginActivity.this, Constants.FUNC_CODE_REFUND, "");
                                    SpUtil.putString(LoginActivity.this, Constants.PERMISSIONS_REFUND, "");
                                }
                            }
                        } else {
                            SpUtil.putString(LoginActivity.this, Constants.FUNC_CODE_REFUND, "");
                            SpUtil.putString(LoginActivity.this, Constants.PERMISSIONS_REFUND, "");
                        }
                        if (mName.length() == 11 && !mName.startsWith("10") && event.getTag().equals(Constants.TAG_LOGIN) && MyApplication.getIsMerchant()) {
                            requestMerchant(mName);
                        } else {
                            if (msg.getData().getUser() != null) {
                                toMainActivity(msg.getData().getUser().getOrgId());
                            } else {
                                showCommonNoticeDialog(LoginActivity.this, "登录返回数据有误！");
                            }
                        }
                    }
                    LogUtil.d("SpUtil==" + MyApplication.getCashierRefund() + "--" + MyApplication.getPermissionsRefund());
                    mRlVerifyCodeLogin.setVisibility(View.GONE);
                    break;

            }
        }
        if (event.getTag().equals(Constants.TAG_GET_LOGIN_MERCHANT)) {
            dismissLoading();
            MerchantIdBean msg = (MerchantIdBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LoginActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    //                        requestLogin();
                                    showCommonNoticeDialog(LoginActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (msg.getData().size() > 1) {
                            showMerchantPopup(msg.getData());
                        } else {
                            toMainActivity(msg.getData().get(0).getOrgId());
                        }
                    } else {
                        toMainActivity(mName);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GETIDENTIFYINGCODE)) {
            //图形验证码
            dismissLoading();
            IdentifyCodeBean msg = (IdentifyCodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.GETIDENTIFYINGCODE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(LoginActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(LoginActivity.this, getString(R.string.error_get_verifycode));
                    }
                    break;
                case Constants.GETIDENTIFYINGCODE_TRUE:
                    if (msg != null && !TextUtils.isEmpty(msg.getData())) {
                        String data = msg.getData();
                        Bitmap bitmap = ImageUtil.base64ToBitmap(data);
                        if (bitmap != null) {
                            mRlVerifyCodeLogin.setVisibility(View.VISIBLE);
                            mIvVerifyCodeLogin.setImageBitmap(bitmap);
                        }
                    }
                    break;
            }
        }
    }

    private void toMainActivity(String orgId) {

        if (MyApplication.isNeedUpdatePwd()) {
            if (mDialogReset == null) {
                mDialogReset = new CommonNoticeDialog(LoginActivity.this, getString(R.string.dialog_change_pwd_title), "", getString(R.string.dialog_change_button));
                mDialogReset.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                    @Override
                    public void onClickOk() {
                        resetPassword(orgId);
                    }
                });
                DialogHelper.resize(LoginActivity.this, mDialogReset);
            }
            mDialogReset.show();
        } else {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }

    private void resetPassword(String orgId) {
        Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
        intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, orgId);
        startActivity(intent);
    }


    //获取图形验证码
    private void getIdentifyingCode() {
        if (StringUtils.isEmptyOrNull(user_name.getText().toString().trim())) {
            showCommonNoticeDialog(LoginActivity.this, getString(R.string.input_user_name));
            return;
        }
        if (NetworkUtils.isNetWorkValid(LoginActivity.this)) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(LoginActivity.this).getIdentifyingCode(this, Constants.TAG_GETIDENTIFYINGCODE, 1, user_name.getText().toString().trim());
        } else {
            showCommonNoticeDialog(LoginActivity.this, getString(R.string.network_exception));
        }
    }

    /**
     * 商户号免密登录
     *
     * @param orgId
     */
    private void mchLogin(String orgId) {
        if (NetworkUtils.isNetWorkValid(LoginActivity.this)) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("orgId", orgId);
            ServerClient.newInstance(MyApplication.getContext()).mchLogin(MyApplication.getContext(), "TAG_MERCHANT_LOGIN_" + this.getClass().getSimpleName(), map);
        } else {
            showCommonNoticeDialog(LoginActivity.this, getString(R.string.network_exception));
        }
    }

    @Override
    public void onBackPressed() {
        if (mUserProtocolPopupWindow != null && mUserProtocolPopupWindow.isShowing()) {
            MyToast.showToastShort(getString(R.string.toast_user_protocol));
            return;
        }
        if (mInfoProtocolPopupWindow != null && mInfoProtocolPopupWindow.isShowing()) {
            mInfoProtocolPopupWindow.dismiss();
            return;
        }
        if (mMerchantPopupWindow != null && mMerchantPopupWindow.isShowing()) {
            SpUtil.removeKey(Constants.SKEY);
            SpUtil.removeAll();
            mMerchantPopupWindow.dismiss();
        }
        super.onBackPressed();
    }

    private List<String> getRecords() {
        String loginRecord = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_RECORD);
        if (!TextUtils.isEmpty(loginRecord)) {
            Gson gson = new Gson();
            List<String> accounts = gson.fromJson(loginRecord, new TypeToken<ArrayList<String>>() {
            }.getType());
            return accounts;
        } else {
            return new ArrayList<>();
        }
    }

    private void stayRecord() {
        String loginRecord = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_RECORD);
        Gson gson = new Gson();
        List<String> records = new ArrayList<>();
        if (!TextUtils.isEmpty(loginRecord)) {
            records = gson.fromJson(loginRecord, new TypeToken<ArrayList<String>>() {
            }.getType());
            Iterator<String> it = records.iterator();
            while (it.hasNext()) {
                String account = it.next();
                if (user_name.getText().toString().trim().equals(account)) {
                    it.remove();
                }
            }
            if (records.size() == 3) {
                records.remove(2);
            }
        }
        records.add(0, user_name.getText().toString().trim());
        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_RECORD, gson.toJson(records));
        mRecords.clear();
        mRecords.addAll(records);
        mAdapter.notifyDataSetChanged();

    }

    private void login() {
        mName = user_name.getText().toString().trim();
        mPwd = userPwd.getText().toString().trim();
        if (StringUtils.isEmptyOrNull(mName)) {
            showCommonNoticeDialog(LoginActivity.this, getString(R.string.input_user_name));
            user_name.setFocusable(true);
            user_name.setFocusableInTouchMode(true);
            user_name.requestFocus();
            setViewHeight(user_name);
            return;
        }

        if (mPwd.length() < 6) {
            showCommonNoticeDialog(LoginActivity.this, getString(R.string.pwd_prompting));//6-32位字母、数字
            return;
        }

        if (mRlVerifyCodeLogin.getVisibility() == View.VISIBLE) {
            String verifyCode = mEtVerifyCodeLogin.getText().toString().trim();
            if (TextUtils.isEmpty(verifyCode)) {
                showCommonNoticeDialog(LoginActivity.this, getString(R.string.input_bitmap_code));
                return;
            }
        }
        /**
         * 如果是11位且不是“10”开头，则是手机号登录,请求绑定的商户列表
         */
        requestLogin();
        /*if (mName.length() == 11 && !mName.startsWith("10")) {
            requestMerchant(mName);
        } else {
            requestLogin();
        }*/
    }

    private void requestLogin() {
        if (NetworkUtils.isNetWorkValid(LoginActivity.this)) {
            showNewLoading(true, getString(R.string.tv_login));
            String verifyCode = null;//图形验证码
            if (mRlVerifyCodeLogin != null && mRlVerifyCodeLogin.getVisibility() == View.VISIBLE) {
                verifyCode = mEtVerifyCodeLogin.getText().toString().trim();
            }
            Map<String, Object> map = new HashMap<>();
            map.put("userName", mName);
            map.put("password", INITDES3Util.getSecretContent(this, mPwd));
            map.put("desFlag", 1);
            if (ConfigUtil.verifyServiceId()) {
                map.put("serviceProviderId", Constants.SERVICE_PROVIDER_ID);
            }
            if (ConfigUtil.getConfig()) {
                map.put("orgId", Constants.ORG_ID);
                map.put("orgType", Constants.ORG_TYPE);
                map.put("appCode", Constants.APP_CODE + Constants.ORG_ID);
            }
            if (!TextUtils.isEmpty(verifyCode)) {
                map.put("verificationCode", verifyCode);
            }
            try {
                ServerClient.newInstance(LoginActivity.this).login(LoginActivity.this, Constants.TAG_LOGIN, map);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showCommonNoticeDialog(LoginActivity.this, ToastHelper.toStr(R.string.network_exception));
        }
    }

    private void requestMerchant(String telPhone) {
        if (NetworkUtils.isNetWorkValid(LoginActivity.this)) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("telPhone", telPhone);
            ServerClient.newInstance(LoginActivity.this).getLoginMerchant(LoginActivity.this, Constants.TAG_GET_LOGIN_MERCHANT, map);
        } else {
            showCommonNoticeDialog(LoginActivity.this, getString(R.string.network_exception));
        }
    }

    private void controlKeyboardLayout(final View root, final View scrollToView) {
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                //获取root在窗体的可视区域
                root.getWindowVisibleDisplayFrame(rect);
                //获取root在窗体的不可视区域高度(被其他View遮挡的区域高度)
                int rootInvisibleHeight = root.getRootView().getHeight() - rect.bottom;
                //若不可视区域高度大于100，则键盘显示
                if (rootInvisibleHeight > 300) {
                    int[] location = new int[2];
                    //获取scrollToView在窗体的坐标
                    scrollToView.getLocationInWindow(location);
                    //计算root滚动高度，使scrollToView在可见区域
                    srollHeight = (location[1] + scrollToView.getHeight()) - rect.bottom;
                    if (isFristIn) {
                        isFristIn = false;
                    }
                    mIv_login.setVisibility(View.INVISIBLE);
                    root.scrollTo(0, srollHeight);
                } else {
                    //键盘隐藏
                    mIv_login.setVisibility(View.VISIBLE);
                    root.scrollTo(0, 0);
                }
            }
        });
    }

    private void controlKeyboardLayout(final View root, final int height) {
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int rootInvisibleHeight = height;
                if (rootInvisibleHeight > 300) {
//                    mIv_login.setVisibility(View.INVISIBLE);
                    root.scrollTo(0, rootInvisibleHeight);
                } else {
//                    mIv_login.setVisibility(View.VISIBLE);
                    root.scrollTo(0, 0);
                }
            }
        });
    }

    private void setButtonState() {
        mLlLoginBttom.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        if (user_name.getText().toString().length() == 0) {
            List<String> records = getRecords();
            if (records != null && records.size() > 0) {
                mIvClean.setImageResource(R.mipmap.ic_arrow_down);
                mIvClean.setVisibility(View.VISIBLE);
            } else {
                mIvClean.setVisibility(View.INVISIBLE);
            }
        } else {
            mIvClean.setImageResource(R.mipmap.ic_login_delete);
            mIvClean.setVisibility(View.VISIBLE);
        }
        if (!StringUtils.isEmptyOrNull(user_name.getText().toString().trim())
                && !StringUtils.isEmptyOrNull(userPwd.getText().toString().trim())) {
            mIbLogin.setEnabled(true);
            Drawable ovalDefault = ShapeSelectorUtils.createOvalDefault(MyApplication.getContext(), MyApplication.themeColor());
            Drawable ovalPressed = ShapeSelectorUtils.createOvalDefault(MyApplication.getContext(), MyApplication.themeColor());
            ovalPressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(ovalDefault, ovalPressed, ovalPressed);
            mIbLogin.setBackground(stateListDrawable);
            setButtonEnable(mBtnLogin, true);
        } else {
            mIbLogin.setEnabled(false);
            Drawable ovalDefault = ShapeSelectorUtils.createOvalDefault(MyApplication.getContext(), MyApplication.themeColor());
            ovalDefault.setAlpha(102);
            mIbLogin.setBackground(ovalDefault);
            setButtonEnable(mBtnLogin, false);
        }
    }

    private final MediaPlayer.OnCompletionListener beepListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getRecords() != null && getRecords().size() > 0) {
            mRecords.clear();
            mRecords.addAll(getRecords());
            mAdapter.notifyDataSetChanged();
            user_name.setText(mRecords.get(0));
        }
    }

    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboard.isShow()) {
                safeKeyboard.hideKeyboard();
                return false;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (safeKeyboard != null) {
            safeKeyboard.release();
            safeKeyboard = null;
        }
        if (mDialogReset != null)
            mDialogReset.dismiss();
        super.onDestroy();
        mIvClean.clearAnimation();
    }
}
