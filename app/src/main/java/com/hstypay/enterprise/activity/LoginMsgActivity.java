package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin on 2017/7/5.
 */
public class LoginMsgActivity extends BaseActivity implements View.OnClickListener {
    private EditText mEtTel;
    private TextView mTvInputHint;
    private Button mBtnSubmit;
    private ImageView mIvBack;
    private TextView mTvPwdLogin;
    private SafeDialog mLoadDialog;
    private View mTitleLine;
    private SafeKeyboard safeKeyboard;
    private boolean isFirst = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        setContentView(R.layout.activity_login_msg);
        if (AppHelper.getApkType() == 2) {
            StatusBarUtil.setTranslucentStatus(this);
        } else {
            StatusBarUtil.setImmersiveStatusBar(this, true);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        initView();
        initListener();

    }

    private void initView() {
        mLoadDialog = getLoadDialog(LoginMsgActivity.this, UIUtils.getString(R.string.public_loading), false);
        mEtTel = findViewById(R.id.et_tel);
        mTvPwdLogin = findViewById(R.id.tv_pwd_login);
        mTvInputHint = findViewById(R.id.tv_input_code_hint);
        mIvBack = findViewById(R.id.iv_back);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mTitleLine = findViewById(R.id.view_title_line);
        mTitleLine.setVisibility(View.GONE);

        setButtonEnable(mBtnSubmit, false);

        View rootView = findViewById(R.id.main_root);
        final LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_number_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_number_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtTel.getId(), mEtTel);
        /*safeKeyboard.setOnKeyboardShowListener(new SafeKeyboard.OnKeyboardShowListener() {
            @Override
            public void onKeyboardShow(boolean show) {
                if (show) {
                    controlKeyboardLayout(mRlLogin, UIUtils.dp2px(150));
                } else {
                    controlKeyboardLayout(mRlLogin, 0);
                }
            }
        });*/
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mTvPwdLogin.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mEtTel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setButtonEnable(mBtnSubmit, s.length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_pwd_login:
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                getMessageCode(mEtTel.getText().toString().trim());
                break;
        }
    }

    /**
     * 获取短信验证码
     *
     * @param phone 手机号
     */
    private void getMessageCode(String phone) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", phone);
            ServerClient.newInstance(MyApplication.getContext()).getCode(MyApplication.getContext(), "TAG_LOGIN_MSG", map);
        } else {
            showCommonNoticeDialog(LoginMsgActivity.this, getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_LOGIN_MSG")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.SEND_PHONE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(LoginMsgActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(LoginMsgActivity.this, getString(R.string.error_request));
                    }
                    break;
                case Constants.SEND_PHONE_TRUE://验证码返回成功
                    startActivity(new Intent(LoginMsgActivity.this, LoginMsgVerifyActivity.class)
                            .putExtra(Constants.INTENT_TELEPHONE_AUTHENTICATION, mEtTel.getText().toString().trim()));
                    break;
            }
        }
    }
}
