package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hsty.verifyblankview.VerifyBlankView;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MerchantSelectPopup;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LoginBean;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by admin on 2017/7/5.
 */
public class LoginMsgVerifyActivity extends BaseActivity implements View.OnClickListener {
    private VerifyBlankView mVbCode;
    private TextView mTvInputHint;
    private Button mBtnSubmit;
    private ImageView mIvBack;
    private TextView mTvTimer;
    private TextView mTvLoginTel;
    private TimeCount time;
    private String mPhone;
    private MerchantSelectPopup mMerchantPopupWindow;
    private SafeDialog mLoadDialog;
    private CommonNoticeDialog mDialogReset;
    private SafeKeyboard safeKeyboard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        setContentView(R.layout.activity_login_msg_verify);
        if (AppHelper.getApkType() == 2) {
            StatusBarUtil.setTranslucentStatus(this);
        } else {
            StatusBarUtil.setImmersiveStatusBar(this, true);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(LoginMsgVerifyActivity.this, UIUtils.getString(R.string.public_loading), false);
        mVbCode = findViewById(R.id.vb_code);
        mTvTimer = findViewById(R.id.tv_timer);
        mTvInputHint = findViewById(R.id.tv_input_code_hint);
        mIvBack = findViewById(R.id.iv_back);
        mTvLoginTel = findViewById(R.id.tv_login_tel);
        mBtnSubmit = findViewById(R.id.btn_submit);

        setButtonEnable(mBtnSubmit, false);

        mVbCode.setOnTextFullListener(new VerifyBlankView.OnTextFullListener() {
            @Override
            public void onTextFull(boolean isFull) {
                setButtonEnable(mBtnSubmit, isFull);
            }
        });
        View rootView = findViewById(R.id.main_root);
        final LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_number_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_number_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mVbCode.getId(), mVbCode);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mTvTimer.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    private void initData() {
        mPhone = getIntent().getStringExtra(Constants.INTENT_TELEPHONE_AUTHENTICATION);
        mTvLoginTel.setText(mPhone);

        countStart();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_timer:
                countStart();
                getMessageCode(mPhone);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                telephoneLogin(mPhone, mVbCode.getFilledText());
                break;
        }
    }

    /**
     * 获取短信验证码
     *
     * @param phone 手机号
     */
    private void getMessageCode(String phone) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", phone);
            ServerClient.newInstance(MyApplication.getContext()).getCode(MyApplication.getContext(), "TAG_LOGIN_MSG_" + this.getClass().getSimpleName(), map);
        } else {
            showCommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.network_exception));
        }
    }

    /**
     * 商户号免密登录
     *
     * @param loginPhone 登录手机号
     * @param msgCode    短信验证码
     */
    private void telephoneLogin(String loginPhone, String msgCode) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("loginPhone", loginPhone);
            map.put("validCode", msgCode);
            if (ConfigUtil.getConfig()) {
                map.put("orgId", Constants.ORG_ID);
                map.put("orgType", Constants.ORG_TYPE);
                map.put("appCode", Constants.APP_CODE + Constants.ORG_ID);
            }
            ServerClient.newInstance(MyApplication.getContext()).msgLogin(MyApplication.getContext(), "TAG_MSG_LOGIN", map);
        } else {
            showCommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.network_exception));
        }
    }

    /**
     * 商户号免密登录
     *
     * @param orgId
     */
    private void mchLogin(String orgId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("orgId", orgId);
            ServerClient.newInstance(MyApplication.getContext()).mchLogin(MyApplication.getContext(), "TAG_MSG_MERCHANT_LOGIN_" + this.getClass().getSimpleName(), map);
        } else {
            showCommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.network_exception));
        }
    }

    private void requestMerchant(String telPhone) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("telPhone", telPhone);
            ServerClient.newInstance(MyApplication.getContext()).getLoginMerchant(MyApplication.getContext(), "TAG_MSG_LOGIN_MERCHANT_" + this.getClass().getSimpleName(), map);
        } else {
            showCommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mMerchantPopupWindow != null && mMerchantPopupWindow.isShowing()) {
            SpUtil.removeKey(Constants.SKEY);
            SpUtil.removeAll();
            mMerchantPopupWindow.dismiss();
        }
        super.onBackPressed();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_MSG_LOGIN") || event.getTag().equals("TAG_MSG_MERCHANT_LOGIN_" + this.getClass().getSimpleName())) {
            dismissLoading();
            LoginBean msg = (LoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    if (event.getTag().equals("TAG_MSG_MERCHANT_LOGIN_" + this.getClass().getSimpleName())) {
                        SpUtil.removeKey(Constants.SKEY);
                        SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    }
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (event.getTag().equals("TAG_MSG_MERCHANT_LOGIN_" + this.getClass().getSimpleName())) {
                        SpUtil.removeKey(Constants.SKEY);
                        SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    }
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LoginMsgVerifyActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(LoginMsgVerifyActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://登录成功
                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REFUND_TAG, "");
                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_BILL_TAG, "");
                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REPORT_TAG, "");
                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.FUNC_CODE_REFUND, "");
                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.PERMISSIONS_REFUND, "");
                    if (msg.getData() != null) {
                        boolean isShowWSY = msg.getData().isShowWSY();
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_SHOW_WSY, isShowWSY);
                        SpUtil.putString(MyApplication.getContext(), Constants.SIGN_KEY, msg.getData().getSignKey());//签名的key
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST, msg.getData().isStandardFlag());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM, msg.getData().isStandardIndexFlag());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BELONG_HSHF, msg.getData().isBelonedHSHFFlag());
                        if (msg.getData().getUser() != null) {
                            int userId = msg.getData().getUser().getUserId();
                            SpUtil.putString(MyApplication.getContext(), Constants.USER_ID, userId + "");//用户id
                            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_STAY_USER_ID, userId + "");//用户id
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, msg.getData().getUser().getAttestationStatus());//商户认证状态
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_WXREG_STATUS, msg.getData().getUser().getWxRegStatus());
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_ALIREG_STATUS, msg.getData().getUser().getAliRegStatus());
                            boolean isUser = msg.getData().getUser().isMerchantFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_MERCHANT_FLAG, isUser);//是否是超管(商户)
                            //是否进件成功
                            String orgId = msg.getData().getUser().getOrgId();
                            SpUtil.putString(MyApplication.getContext(), Constants.IS_SUCCESS_DATA, orgId);
                            //是否是管理员
                            boolean isAdmin = msg.getData().getUser().isAdminFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_ADMIN_FLAG, isAdmin);
                            //是否是收银员
                            boolean isCasher = msg.getData().getUser().isCasherFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CASHER_FLAG, isCasher);
                            //是否是店长
                            boolean isManager = msg.getData().getUser().isStoreManagerFlag();
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_MANAGER_FLAG, isManager);

                            //realname
                            String realName = msg.getData().getUser().getRealName();
                            SpUtil.putString(MyApplication.getContext(), Constants.REALNAME, realName);
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.NEED_CHANGE_PWD, msg.getData().getUser().isUpdatePwdFlag());
                            SpUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE, msg.getData().getUser().getLoginPhone());
                            //用户名
                            SpUtil.putString(MyApplication.getContext(), Constants.APP_USERNAME, msg.getData().getUser().getUserName());
                            SpUtil.putString(MyApplication.getContext(), Constants.APP_LOGIN_NAME, msg.getData().getUser().getLoginName());

                            if (msg.getData().getUser().getEmp() != null) {
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getEmp().getEmpId())) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_EMP_ID, msg.getData().getUser().getEmp().getEmpId());
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_EMP_ID, "");
                                }
                            }
                            //商户号
                            if (msg.getData().getUser().getMerchant() != null) {
//                                    SpUtil.putInt(MyApplication.getContext(), Constants.SP_PLEDGE_STATUS, msg.getData().getUser().getMerchant().getPreauthStatus());
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getRewardFlage())) {
                                    SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BOUNTY_MERCHANT, true);
                                } else {
                                    SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BOUNTY_MERCHANT, false);
                                }
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getMerchantId())) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_ID, msg.getData().getUser().getMerchant().getMerchantId());
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_ID, "");
                                }
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getMerchantType())) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_TYPE, msg.getData().getUser().getMerchant().getMerchantType());
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.APP_MERCHANT_TYPE, "");
                                }
                                if (!StringUtils.isEmptyOrNull(msg.getData().getUser().getMerchant().getMerchantName())) {
                                    String merchant_name = msg.getData().getUser().getMerchant().getMerchantName();//商户名称
                                    SpUtil.putString(MyApplication.getContext(), Constants.MERCHANT_NAME, merchant_name);
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.MERCHANT_NAME, "");
                                }
                            }
                        } else {
                            SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, 0);//商户认证状态
                        }
                        if (msg.getData().getOps() != null && msg.getData().getOps().getUserOps() != null && msg.getData().getOps().getUserOps().size() > 0) {
                            List<LoginBean.DataBean.OpsBean.UserOpsBean> userOps = msg.getData().getOps().getUserOps();
                            for (int u = 0; u < userOps.size(); u++) {
                                LoginBean.DataBean.OpsBean.UserOpsBean userOpsBean = userOps.get(u);
                                if (userOpsBean != null) {
                                    if (!StringUtils.isEmptyOrNull(userOpsBean.getOpCode())) {
                                        if (userOpsBean.getOpCode().equals("APPLY-REFUND")) {
                                            //收银员退款权限
                                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REFUND_TAG, userOpsBean.getOpCode());
                                        }
                                        if (userOpsBean.getOpCode().equals("APPLY-WATER")) {
                                            //收银员查看流水权限
                                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_BILL_TAG, userOpsBean.getOpCode());
                                        }
                                        if (userOpsBean.getOpCode().equals("APPLY-BILL")) {
                                            //收银员查看统计权限
                                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REPORT_TAG, userOpsBean.getOpCode());
                                        }
                                    }
                                } else {
                                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REFUND_TAG, "");
                                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_BILL_TAG, "");
                                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REPORT_TAG, "");
                                }
                            }
                        } else {
                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REFUND_TAG, "");
                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_BILL_TAG, "");
                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.CASHIER_REPORT_TAG, "");
                        }

                        if (msg.getData().getFuncs() != null && msg.getData().getFuncs().size() > 0) {
                            List<LoginBean.DataBean.FuncsBean> funcs = msg.getData().getFuncs();
                            for (int i = 0; i < funcs.size(); i++) {
                                LoginBean.DataBean.FuncsBean s = funcs.get(i);
                                if (s != null) {
                                    if ("APP-OPERATION".equals(s.getFuncCode())) {
                                        SpUtil.putString(LoginMsgVerifyActivity.this, Constants.FUNC_CODE_REFUND, s.getFuncCode());
                                    } else {
                                        SpUtil.putString(LoginMsgVerifyActivity.this, Constants.FUNC_CODE_REFUND, "");
                                    }
                                    List<LoginBean.DataBean.FuncsBean.PermissionsBean> permissions = s.getPermissions();
                                    if (permissions != null && permissions.size() > 0) {
                                        for (int p = 0; p < permissions.size(); p++) {
                                            LoginBean.DataBean.FuncsBean.PermissionsBean permissionsBean = permissions.get(p);
                                            if (permissionsBean != null && "APPLY-REFUND".equals(permissionsBean.getPermissionCode())) {
                                                //有退款权限
                                                SpUtil.putString(LoginMsgVerifyActivity.this, Constants.PERMISSIONS_REFUND, permissionsBean.getPermissionCode());
                                            } else {
                                                SpUtil.putString(LoginMsgVerifyActivity.this, Constants.PERMISSIONS_REFUND, "");
                                            }
                                        }
                                    } else {
                                        SpUtil.putString(LoginMsgVerifyActivity.this, Constants.PERMISSIONS_REFUND, "");
                                    }
                                } else {
                                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.FUNC_CODE_REFUND, "");
                                    SpUtil.putString(LoginMsgVerifyActivity.this, Constants.PERMISSIONS_REFUND, "");
                                }
                            }
                        } else {
                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.FUNC_CODE_REFUND, "");
                            SpUtil.putString(LoginMsgVerifyActivity.this, Constants.PERMISSIONS_REFUND, "");
                        }
                        if (event.getTag().equals("TAG_MSG_LOGIN") && MyApplication.getIsMerchant()) {
                            requestMerchant(mPhone);
                        } else {
                            if (msg.getData().getUser() != null) {
                                toMainActivity(msg.getData().getUser().getOrgId());
                            } else {
                                showCommonNoticeDialog(LoginMsgVerifyActivity.this,"登录返回数据有误！");
                            }
                        }
                    }
                    break;

            }
        } else if (event.getTag().equals("TAG_MSG_LOGIN_MERCHANT_" + this.getClass().getSimpleName())) {
            dismissLoading();
            MerchantIdBean msg = (MerchantIdBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(LoginMsgVerifyActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.error_get_merchant));
                    }
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeKey(Constants.LOGIN_COOKIE);
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (msg.getData().size() > 1) {
                            showMerchantPopup(msg.getData());
                        } else {
                            toMainActivity(msg.getData().get(0).getOrgId());
                        }
                    } else {
                        toMainActivity(mPhone);
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_LOGIN_MSG_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.SEND_PHONE_FALSE:
                    if (msg.getError() != null && !TextUtils.isEmpty(msg.getError().getMessage())) {
                        showCommonNoticeDialog(LoginMsgVerifyActivity.this, msg.getError().getMessage());
                    } else {
                        showCommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.error_request));
                    }
                    break;
                case Constants.SEND_PHONE_TRUE://验证码返回成功
                    MyToast.showToastShort("验证码已发送");
                    break;
            }
        }
    }

    private void toMainActivity(String orgId) {
        if (MyApplication.isNeedUpdatePwd()) {
            if (mDialogReset == null) {
                mDialogReset = new CommonNoticeDialog(LoginMsgVerifyActivity.this, getString(R.string.dialog_change_pwd_title), "", getString(R.string.dialog_change_button));
                mDialogReset.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                    @Override
                    public void onClickOk() {
                        resetPassword(orgId);
                    }
                });
                DialogHelper.resize(LoginMsgVerifyActivity.this, mDialogReset);
            }
            mDialogReset.show();
        } else {
            Intent intent = new Intent(LoginMsgVerifyActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void resetPassword(String orgId) {
        Intent intent = new Intent(LoginMsgVerifyActivity.this, ResetPasswordActivity.class);
        intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, orgId);
        startActivity(intent);
    }

    private void showMerchantPopup(final List<MerchantIdBean.DataEntity> data) {
        mMerchantPopupWindow = new MerchantSelectPopup(LoginMsgVerifyActivity.this, data);
        mMerchantPopupWindow.setOnClickItemListener(new MerchantSelectPopup.OnClickItemListener() {
            @Override
            public void clickItem(int position) {
                /*user_name.setText(data.get(position).getOrgId());
                user_name.setSelection(user_name.getText().toString().trim().length());
                userPwd.setText("");*/
                mchLogin(data.get(position).getOrgId());
            }
        });
        mMerchantPopupWindow.showAtLocation(mBtnSubmit, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void countStart() {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();

    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            mTvTimer.setEnabled(true);
            mTvTimer.setClickable(true);
            mTvTimer.setText(R.string.bt_code_get);
            mTvTimer.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            mTvTimer.setEnabled(false);
            mTvTimer.setClickable(false);
            mTvTimer.setTextColor(getResources().getColor(R.color.home_hint_text));
            mTvTimer.setText(millisUntilFinished / 1000 + "s后可重新获取");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (time != null)
            time.cancel();
        if (mDialogReset != null)
            mDialogReset.dismiss();
    }
}
