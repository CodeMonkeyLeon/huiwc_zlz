package com.hstypay.enterprise.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.google.gson.Gson;
import com.hstypay.enterprise.BuildConfig;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoScrollLazyViewPager;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SignDialog;
import com.hstypay.enterprise.Widget.dialog.CommonConfirmDialog;
import com.hstypay.enterprise.activity.eleticket.BindUkeyTicketBoardCodeActivity;
import com.hstypay.enterprise.activity.eleticket.BindUkeyTicketDeviceActivity;
import com.hstypay.enterprise.activity.setting.ChangePwdActivity;
import com.hstypay.enterprise.activity.vipCard.ShareVipActivity;
import com.hstypay.enterprise.activity.vipCard.VipListActivity;
import com.hstypay.enterprise.activity.vlife.VlifeOpenActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierLimitBean;
import com.hstypay.enterprise.bean.ConfigBean;
import com.hstypay.enterprise.bean.GoodsNameBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LocationBean;
import com.hstypay.enterprise.bean.PrintContentBean;
import com.hstypay.enterprise.bean.ProgressBean;
import com.hstypay.enterprise.bean.QueryMchStatusEleTicketBean;
import com.hstypay.enterprise.bean.QueryPayTypeBean;
import com.hstypay.enterprise.bean.SendHomeBean;
import com.hstypay.enterprise.bean.SignStatus;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.SvcAppListBean;
import com.hstypay.enterprise.bean.UpdateVersionBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.fragment.ApplicationFragment;
import com.hstypay.enterprise.fragment.BUserFragment;
import com.hstypay.enterprise.fragment.BillFragment;
import com.hstypay.enterprise.fragment.HomeFragment;
import com.hstypay.enterprise.fragment.MchTalkFragment;
import com.hstypay.enterprise.fragment.ReceiveFragment;
import com.hstypay.enterprise.fragment.ReportDataFragment;
import com.hstypay.enterprise.fragment.ReportFragment;
import com.hstypay.enterprise.fragment.UserFragment;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.service.DownLoadService;
import com.hstypay.enterprise.service.live.ForegroundLiveService;
import com.hstypay.enterprise.service.live.JobSchedulerManager;
import com.hstypay.enterprise.service.live.PlayerMusicService;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.VersionUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.voice.VoiceUtils;
import com.igexin.sdk.PushManager;
import com.pax.dal.IDAL;
import com.pax.neptunelite.api.NeptuneLiteUser;
import com.wizarpos.fucard.CloudPosPaymentClient.aidl.ICloudPay;
import com.zng.common.PrintPowerOnAndOffUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 11:28
 * @描述: ${TODO}
 */

public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    public static MainActivity instance;
    public NoScrollLazyViewPager mVpMain;
    public RadioGroup mRgBottomNav;
    private android.app.FragmentManager mManager;

    private Fragment[] mFragments;
    private RadioButton mRb_home, rb_bottom_app;
    private RadioButton mRbIncome;
    private RadioButton mRb_receive, mRb_report, mRb_bill, mRb_user;
    private RelativeLayout mRlRbState;
    public RelativeLayout mRlContent;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private SignDialog mSignDialog;
    private CommonNoticeDialog mDialogReset;
    private UpdateVersionBean.DataBean mUpdateVersion;
    private CommonConfirmDialog mCommonConfirmDialog;
    private boolean isWifiType;
    private Uri apkurl;
    private boolean noPay;//不可收款
    private int mVersionCode;
    private TodayDataBroadcastReceiver mTodayDataBroadcastReceiver;
    private Intent mService;
    private PrintPowerOnAndOffUtils mPrintPowerOnAndOffUtils;
    private int mIndex = -1;
    private HomeFragment mHomeFragment;//首页
    //    private VipFragment mVipFragment;//会员
    private ReportDataFragment mReportDataFragment;//报表
    private ReceiveFragment mReceiveFragment;//收款
    private BillFragment mBillFragment;//账单
    private ReportFragment mReportFragment;//统计
    private UserFragment mUserFragment;//我的
    private BUserFragment mBUserFragment;//我的（行业APP）
    public static IDAL idal = null;
    private N900Device n900Device;
    private SelectDialog mDialogFloat;
    private boolean canDraw;
    private AppOpsManager.OnOpChangedListener onOpChangedListener;
    private boolean mIsPushClientId;//是否已上传个推的cid
    public SafeDialog mLoadDialog;
    private ApplicationFragment mApplicationFragment;
    private RadioButton mRbBottomMchTalk;
    private MchTalkFragment mMchTalkFragment;//商家说

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;
        MyApplication.getInstance().addActivity(this);
        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_NOTICE_CLOSED, false);
        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_QRCODE_NOTICE_CLOSED, false);
        initView();
        initEvent();
        registerBroadcast();
        intData();
//        PermissionUtils.checkPermissionArray(this, permissionArray, PermissionUtils.PERMISSION_REQUEST_CODE);
        initPos();

        if (Build.VERSION.SDK_INT >= 23) {
            AppOpsManager opsManager = (AppOpsManager) MyApplication.getContext().getSystemService(Context.APP_OPS_SERVICE);
            canDraw = Settings.canDrawOverlays(MyApplication.getContext());
            onOpChangedListener = new AppOpsManager.OnOpChangedListener() {

                @Override
                public void onOpChanged(String op, String packageName) {
                    String myPackageName = MyApplication.getContext().getPackageName();
                    if (myPackageName.equals(packageName) &&
                            AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW.equals(op)) {
                        canDraw = !canDraw;
                    }
                }
            };
            opsManager.startWatchingMode(AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW,
                    null, onOpChangedListener);
            if (!canDraw) {
                getLoginDialog(MainActivity.this);
            }
        }
    }

    /**
     * 初始化视图
     */
    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mRgBottomNav = (RadioGroup) findViewById(R.id.rg_bottom_nav);
        //mVpMain = (NoScrollLazyViewPager) findViewById(R.id.vp_main);
        mRb_home = (RadioButton) findViewById(R.id.rb_bottom_home);
        mRbBottomMchTalk = (RadioButton) findViewById(R.id.rb_bottom_mch_talk);
        mRbIncome = (RadioButton) findViewById(R.id.rb_bottom_income);
        mRb_user = (RadioButton) findViewById(R.id.rb_bottom_user);
        mRb_receive = (RadioButton) findViewById(R.id.rb_bottom_receive);
        mRb_report = (RadioButton) findViewById(R.id.rb_bottom_report);
        mRb_bill = (RadioButton) findViewById(R.id.rb_bottom_bill);
        mRlRbState = (RelativeLayout) findViewById(R.id.rl_rb_state);
        rb_bottom_app = (RadioButton) findViewById(R.id.rb_bottom_app);
        mRlContent = findViewById(R.id.rl_content);


    }

    JobSchedulerManager mJobManager;

    private void keepAlive() {
        //JobScheduler 大于5.0才可以使用
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mJobManager = JobSchedulerManager.getJobSchedulerInstance(this);
            mJobManager.startJobScheduler();
        }
        Intent intent = new Intent(this, ForegroundLiveService.class);
        Intent liveService = new Intent(this, PlayerMusicService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
            startForegroundService(liveService);
        } else {
            startService(intent);
            startService(liveService);
        }
    }

    private void updateVersion() {
        //版本升级接口
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mVersionCode = VersionUtils.getVersionCode(MyApplication.getContext());
            Map<String, Object> map = new HashMap<>();
            map.put("client", Constants.REQUEST_CLIENT_APP);
            map.put("versionCode", mVersionCode);
            map.put("appChannel", ConfigUtil.getAppChannel());
            map.put("appType", "HWC");
            map.put("currentVersion", VersionUtils.getVersionName(MyApplication.getContext()));
            ServerClient.newInstance(MyApplication.getContext()).updateVersion2(MyApplication.getContext(), Constants.MAIN_UPDATE_VERSION_TAG, map);
        }
    }

    /**
     * 初始化事件
     */
    private void initEvent() {
        mRgBottomNav.setOnCheckedChangeListener(this);
    }

    /**
     * 初始化数据
     */
    private void intData() {
// 获取测试设备ID
        String testDeviceId = StatService.getTestDeviceId(MyApplication.getContext());
// 日志输出
        android.util.Log.d("BaiduMobStat", "Test DeviceId : " + testDeviceId);
        initFragment();

        if (AppHelper.getAppType() == 2) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                collectInfo();
            } else {
                location();
            }
        }
        if (ConfigUtil.updateVersionEnable()) {
            updateVersion();
        }
        if (ConfigUtil.getuiEnable()) {
            getMessage();
        }

        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                ServerClient.newInstance(MainActivity.this).getGoodsName(MainActivity.this, Constants.GET_GOODS_NAME_TAG, null);
                ServerClient.newInstance(MainActivity.this).storePort(MainActivity.this, Constants.STORE_PORT_TAG, null);
                ServerClient.newInstance(MainActivity.this).riskWhiteCheck(MainActivity.this, Constants.TAG_RISK_WHITE, null);
                ServerClient.newInstance(MainActivity.this).cashPointCheck(MainActivity.this, Constants.TAG_CASHPOINT_WHITE, null);
                ServerClient.newInstance(MainActivity.this).querySvcAppList(MainActivity.this, "TAG_SVC_APP_LIST", null);
                ServerClient.newInstance(MyApplication.getContext()).isOpenMember(MyApplication.getContext(), Constants.TAG_OPEN_MEMBER, null);
                Map<String, Object> configMap = null;
                if (!StringUtils.isEmptyOrNull(Constants.ORG_ID)) {
                    configMap = new HashMap<>();
                    configMap.put("orgId", Constants.ORG_ID);
                    configMap.put("orgType", Constants.ORG_TYPE);
                }
                ServerClient.newInstance(MyApplication.getContext()).newConfig(MyApplication.getContext(), "TAG_MAIN_NEW_CONFIG", configMap);
                haveOpenSvcApp();
                if (MyApplication.getIsMerchant()) {
                    ServerClient.newInstance(MainActivity.this).querySignStatus(MainActivity.this, Constants.TAG_QUERY_SIGN, null);
                }
                if (MyApplication.getIsCasher()) {
                    ServerClient.newInstance(MyApplication.getContext()).queryCashierLimit(MyApplication.getContext(), Constants.TAG_QUERY_CASHIER_LIMIT, null);
                }
                //查询开通的支付方式
                ServerClient.newInstance(MainActivity.this).queryPayType(MainActivity.this, Constants.TAG_QUERY_PAYTYPE, null);

            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        } else {
            SpUtil.removeKey(Constants.SP_GOODS_NAME);
        }

        String loginPhone = MyApplication.getLoginPhone();
        if (TextUtils.isEmpty(loginPhone)) {
            NoticeDialog bindingDialog = new NoticeDialog(MainActivity.this, null, UIUtils.getString(R.string.dialog_btn_bind)
                    , R.layout.notice_dialog_binding);
            bindingDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    Intent intent = new Intent(MainActivity.this, AuthenticationActivity.class);
                    intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_MAIN_AUTHENTICATION);
                    startActivity(intent);
                }
            });
            bindingDialog.show();
        }

        if (MyApplication.isNeedUpdatePwd()) {
            if (mDialogReset == null) {
                mDialogReset = new CommonNoticeDialog(MainActivity.this, getString(R.string.dialog_change_pwd_title), "", getString(R.string.dialog_change_button));
                mDialogReset.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                    @Override
                    public void onClickOk() {
                        startActivity(new Intent(MainActivity.this, ChangePwdActivity.class));
                    }
                });
                DialogHelper.resize(MainActivity.this, mDialogReset);
            }
            mDialogReset.show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //引导用户前往通知页面打开允许应用通知开关
            checkNotificationEnabled();
        } else {
            keepAlive();
        }
    }

    /**
     * 应用开通状态查询
     */
    private void haveOpenSvcApp() {
        DialogUtil.safeShowDialog(mLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("appCode", "PREAUTH-PAY");
        ServerClient.newInstance(MyApplication.getContext()).haveOpenSvcApp(MyApplication.getContext(), "TAG_MAIN_PREAUTH_PAY", map);
    }

    private void initPos() {
        /*if (Constants.ZYTPAY_LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        } else */
        if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintPowerOnAndOffUtils = new PrintPowerOnAndOffUtils(MyApplication.getContext());
            mPrintPowerOnAndOffUtils.powerOn();
        } else if ("a920".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                idal = NeptuneLiteUser.getInstance().getDal(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (null == idal) {
                Toast.makeText(MainActivity.this, "error occurred,DAL is null.", Toast.LENGTH_LONG).show();
            }
        } else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            n900Device = N900Device.getInstance(this);
            connectDevice(n900Device);
//        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
        } else if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
            bindCUPService();
        }
    }

    private void initFragment() {
        mHomeFragment = new HomeFragment();
        mApplicationFragment = new ApplicationFragment();
        mMchTalkFragment = new MchTalkFragment();
        mReportDataFragment = new ReportDataFragment();
        mReceiveFragment = new ReceiveFragment();
        mBillFragment = new BillFragment();
        mReportFragment = new ReportFragment();
        mUserFragment = new UserFragment();
        mBUserFragment = new BUserFragment();
        //添加到数组
        if (AppHelper.getApkType() == 1) {
            mFragments = new Fragment[]{mHomeFragment, mApplicationFragment, mMchTalkFragment, mReportDataFragment, mReceiveFragment, mBillFragment, mReportFragment, mBUserFragment};
        } else {
            mFragments = new Fragment[]{mHomeFragment, mApplicationFragment, mMchTalkFragment, mReportDataFragment, mReceiveFragment, mBillFragment, mReportFragment, mUserFragment};
        }

        //设置可见的tab和初始化选中tab及fragment
        initConfigRadioGroup();

    }

    //设置可见的tab和初始化选中tab
    private void initConfigRadioGroup() {
        if (AppHelper.getAppType() == 2) {
            mRb_receive.setVisibility(View.GONE);
            mRb_report.setVisibility(View.GONE);
            mRb_bill.setVisibility(View.GONE);
            rb_bottom_app.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                mRbIncome.setVisibility(View.VISIBLE);
            } else {
                if (!MyApplication.getIsMerchant() || TextUtils.isEmpty(MyApplication.getConfig()) || !MyApplication.getConfig().contains(Constants.CONFIG_REPORT)) {
                    mRbIncome.setVisibility(View.GONE);
                } else {
                    mRbIncome.setVisibility(View.VISIBLE);
                }
            }

            if (TextUtils.isEmpty(MyApplication.getConfig()) || !MyApplication.getConfig().contains(Constants.CONFIG_BUSINESS_SAID)) {
                mRbBottomMchTalk.setVisibility(View.GONE);
            } else {
                mRbBottomMchTalk.setVisibility(View.VISIBLE);
            }

            mRb_home.setChecked(true);
        } else if (AppHelper.getAppType() == 1) {
            if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                mRb_receive.setVisibility(View.VISIBLE);
            } else {
                if (TextUtils.isEmpty(MyApplication.getConfig()) || !MyApplication.getConfig().contains(Constants.CONFIG_PAY)) {
                    mRb_receive.setVisibility(View.GONE);
                } else {
                    mRb_receive.setVisibility(View.VISIBLE);
                }
            }
            if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                mRb_report.setVisibility(View.VISIBLE);
            } else {
                if (TextUtils.isEmpty(MyApplication.getConfig()) || !MyApplication.getConfig().contains(Constants.CONFIG_COUNT)) {
                    mRb_report.setVisibility(View.GONE);
                } else {
                    mRb_report.setVisibility(View.VISIBLE);
                }
            }
            if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                mRb_bill.setVisibility(View.VISIBLE);
            } else {
                if (TextUtils.isEmpty(MyApplication.getConfig()) || !MyApplication.getConfig().contains(Constants.CONFIG_BILL)) {
                    mRb_bill.setVisibility(View.GONE);
                } else {
                    mRb_bill.setVisibility(View.VISIBLE);
                }
            }
            mRb_home.setVisibility(View.GONE);
            mRbBottomMchTalk.setVisibility(View.GONE);
            mRbIncome.setVisibility(View.GONE);
            rb_bottom_app.setVisibility(View.GONE);
            mRb_receive.setChecked(true);
        }
    }

    public void setRadioBtnChecked(int checkedRadioBtnId) {
        if (mRgBottomNav != null) {
            Log.d("tagtag", "setRadioBtnChecked:" + checkedRadioBtnId);
            mRgBottomNav.check(checkedRadioBtnId);
        }
    }

    public void setIndexSelected(int index) {
        if (mIndex == index || mFragments == null || mFragments.length == 0) {
            return;
        }
        if (mApplicationFragment != null && mApplicationFragment.isEditState() && mRgBottomNav.getCheckedRadioButtonId() != R.id.rb_bottom_app) {
            boolean interupt = mApplicationFragment.setEditSaveTip(mCheckedId);
            if (interupt) {
                rb_bottom_app.setChecked(true);
                return;
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        //隐藏
        if (mIndex != -1) {
            ft.hide(mFragments[mIndex]);
        }
        //判断是否添加
        if (!mFragments[index].isAdded()) {
            ft.add(R.id.content, mFragments[index], mFragments[index].getClass().getSimpleName()).show(mFragments[index]);
        } else {
            ft.show(mFragments[index]);
        }

        ft.commit();
        //再次赋值
        mIndex = index;
    }

    public ICloudPay getCupService() {
        return mCupService;
    }

    private boolean isShowVip() {
        return !TextUtils.isEmpty(MyApplication.getIsSuccessData()) && (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM)
                || MyApplication.isOpenMember());
    }

    private void getMessage() {
        //推送
        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
//            PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), com.hstypay.enterprise.service.GetuiReciverService.class);
            String clientid = PushManager.getInstance().getClientid(MyApplication.getContext());
            LogUtil.i("Jeremy", "login=client1==" + clientid);
            if (clientid != null) {
                mIsPushClientId = true;
                SpUtil.putString(MyApplication.getContext(), Constants.GETUI_CLIENT_ID, clientid);
                Map<String, Object> map = new HashMap<>();
                if (AppHelper.getAppType() == 1) {
                    map.put("client", Constants.REQUEST_CLIENT_POS);
                } else if (AppHelper.getAppType() == 2) {
                    map.put("client", Constants.REQUEST_CLIENT_APP);
                }
                map.put("pushDeviceId", clientid);
                LogUtil.i("Jeremy", "login=client==" + clientid);
                ServerClient.newInstance(MyApplication.getContext()).getuiPushVoice(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_TAG, map);
            }
            PushManager.getInstance().turnOnPush(MyApplication.getContext());
        }
    }

    private void voiceAllSwitch(int pushClose) {
        Map<String, Object> map = new HashMap<>();
        String clientId = PushManager.getInstance().getClientid(this.getApplicationContext());
        if (!StringUtils.isEmptyOrNull(clientId)) {
            map.put("pushDeviceId", clientId);
        }
        if (AppHelper.getAppType() == 1) {
            map.put("client", Constants.REQUEST_CLIENT_POS);
        } else if (AppHelper.getAppType() == 2) {
            map.put("client", Constants.REQUEST_CLIENT_APP);
        }
        map.put("pushClose", pushClose);
        ServerClient.newInstance(MyApplication.getContext()).voiceAllSwitch(MyApplication.getContext(), Constants.TAG_VOICE_ALL_SWITCH, map);
    }

    private void location() {
        String lng = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOCATION_LONGITUDE);
        String lat = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOCATION_LATITUDE);
        if (!TextUtils.isEmpty(lng) && !TextUtils.isEmpty(lat)) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap();
                map.put("longitude", lng);
                map.put("dimensionality", lat);
                ServerClient.newInstance(MyApplication.getContext()).questLocation(MyApplication.getContext(), Constants.TAG_MAIN_QUEST_LOCATION, map);
            }
        }
        /*LocationUtil locationUtil = new LocationUtil();
        locationUtil.setOnClickOkListener(new LocationUtil.OnLocationListener() {
            @Override
            public void location(double lat, double lng) {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    Map<String, Object> map = new HashMap();
                    map.put("longitude", lng);
                    map.put("dimensionality", lat);
                    ServerClient.newInstance(MyApplication.getContext()).questLocation(MyApplication.getContext(), Constants.TAG_MAIN_QUEST_LOCATION, map);
                }
            }
        });
        locationUtil.getCNBylocation(this);*/
    }

    private void collectInfo() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            if (AppHelper.getAppType() == 1) {
                map.put("client", Constants.REQUEST_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("client", Constants.REQUEST_CLIENT_APP);
            }
            map.put("versionCode", AppHelper.getVerCode(MyApplication.getContext()));
            map.put("phoneBrand", Build.BRAND);
            map.put("phoneModel", Build.MODEL);
            map.put("firmwareVersion", Build.VERSION.RELEASE);
            if (!TextUtils.isEmpty(AppHelper.getProvidersName(MyApplication.getContext()))) {
                map.put("telecomOperator", AppHelper.getProvidersName(MyApplication.getContext()));
            }
            map.put("network", NetworkUtils.getNetWorkStatus(MyApplication.getContext()));
            if ("WIFI".equals(NetworkUtils.getNetWorkStatus(MyApplication.getContext()))) {
                map.put("wifiName", NetworkUtils.getConnectWifiSsid(MyApplication.getContext()));
            }
            ServerClient.newInstance(MyApplication.getContext()).collectInfo(MyApplication.getContext(), Constants.TAG_COLLECT_INFO, map);
        }
    }

    private void collectInfo(String provinceCode, String cityCode, String countryCode, String address) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            if (AppHelper.getAppType() == 1) {
                map.put("client", Constants.REQUEST_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("client", Constants.REQUEST_CLIENT_APP);
            }
            map.put("versionCode", AppHelper.getVerCode(MyApplication.getContext()));
            map.put("privince", provinceCode);
            map.put("city", cityCode);
            map.put("county", countryCode);
            map.put("address", address);
            map.put("phoneBrand", Build.BRAND);
            map.put("phoneModel", Build.MODEL);
            map.put("firmwareVersion", Build.VERSION.RELEASE);
            if (!TextUtils.isEmpty(AppHelper.getProvidersName(MyApplication.getContext()))) {
                map.put("telecomOperator", AppHelper.getProvidersName(MyApplication.getContext()));
            }
            map.put("network", NetworkUtils.getNetWorkStatus(MyApplication.getContext()));
            if ("WIFI".equals(NetworkUtils.getNetWorkStatus(MyApplication.getContext()))) {
                map.put("wifiName", NetworkUtils.getConnectWifiSsid(MyApplication.getContext()));
            }
            ServerClient.newInstance(MyApplication.getContext()).collectInfo(MyApplication.getContext(), Constants.TAG_COLLECT_INFO, map);
        }
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onQueryPayType(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_QUERY_PAYTYPE)) {
            QueryPayTypeBean msg = (QueryPayTypeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.QUERY_RATE_FALSE:
                    break;
                case Constants.QUERY_RATE_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        try {
                            Gson gson = new Gson();
                            String apiProviders = gson.toJson(msg.getData());
                            SpUtil.putString(MyApplication.getContext(), Constants.SP_ALL_APIPROVIDERS, apiProviders);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_ALL_APIPROVIDERS, "");
                    }
                    break;
            }
        }
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetGoodsName(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_OPEN_MEMBER)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, msg.getData().getRetCode());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, false);
                    }
                    if (mHomeFragment != null) {
                        mHomeFragment.initMenuDataUI(true);//刷新fragment
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_RISK_WHITE)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_RISK_WHITE, msg.getData().isRiskWhite());
                    } else {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_RISK_WHITE, 0);
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_CASHPOINT_WHITE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            if (Constants.ON_EVENT_TRUE.equals(event.getCls())) {
                if (msg.getData() != null) {
                    SpUtil.putInt(MyApplication.getContext(), Constants.SP_CASHPOINT_WHITE, msg.getData().getIsCashPointWhite());
                } else {
                    SpUtil.putInt(MyApplication.getContext(), Constants.SP_CASHPOINT_WHITE, 0);
                }
            }
        } else if (event.getTag().equals(Constants.GET_GOODS_NAME_TAG)) {
            GoodsNameBean msg = (GoodsNameBean) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.GET_GOODS_NAME_FALSE:
                    break;
                case Constants.GET_GOODS_NAME_TRUE:
                    if (msg.getData() != null && msg.getData().getPayGoodsName() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_GOODS_NAME, msg.getData().getPayGoodsName());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_SHORT_NAME, msg.getData().getMerchantShortName());
                    } else {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_GOODS_NAME, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_SHORT_NAME, "");
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.STORE_PORT_TAG)) {
            StoreBean bean = (StoreBean) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (bean.getData().getStoreName() != null) {
                                String storeName = bean.getData().getStoreName();
                                if (!TextUtils.isEmpty(storeName)) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, "");
                                }
                            }

                            if (!TextUtils.isEmpty(bean.getData().getStoreId())) {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, bean.getData().getStoreId());
                            } else {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, true);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                            }
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_VIP_STORE_LIST)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData().size() == 0) {
                            showNoStoreDialog();
                        } else if (msg.getData().size() == 1) {
                            Intent intent = new Intent(MainActivity.this, ShareVipActivity.class);
                            intent.putExtra(Constants.INTENT_VIP_STORE_ID, msg.getData().get(0).getStoreId());
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(MainActivity.this, VipListActivity.class);
                            intent.putExtra(Constants.INTENT_VIP_STORE_LIST, msg);
                            startActivity(intent);
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
        } else if (event.getTag().equals(Constants.TAG_SCAN_ORDER_STORE_LIST) || event.getTag().equals(Constants.TAG_DAOJIA_STORE_LIST)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData().size() == 0) {
                            showNoStoreDialog();
                        } else if (msg.getData().size() == 1) {
                            if (event.getTag().equals(Constants.TAG_SCAN_ORDER_STORE_LIST)) {
                                mchOauth(msg.getData().get(0).getStoreId());
                            } else if (event.getTag().equals(Constants.TAG_DAOJIA_STORE_LIST)) {
                                getDaojiaUrl(msg.getData().get(0).getStoreId());
                            }
                        } else {
                            if (event.getTag().equals(Constants.TAG_SCAN_ORDER_STORE_LIST)) {
                                Intent intentShop = new Intent(this, ShopActivity.class);
                                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_SCAN_ORDER_STORE_LIST);
                                startActivityForResult(intentShop, Constants.REQUEST_SCAN_ORDER_STORE_LIST);
                            } else if (event.getTag().equals(Constants.TAG_DAOJIA_STORE_LIST)) {
                                Intent intentShop = new Intent(this, ShopActivity.class);
                                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_DAOJIA_STORE_LIST);
                                startActivityForResult(intentShop, Constants.REQUEST_DAOJIA_STORE_LIST);
                            }
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
        } else if (event.getTag().equals(Constants.TAG_SCAN_ORDER_URL)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    String orderUrl = (String) msg.getData();
                    Intent intent = new Intent(this, RegisterActivity.class);
                    intent.putExtra(Constants.REGISTER_INTENT, orderUrl);
                    intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                    intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                    startActivity(intent);
                    break;
            }
            dismissLoading();
        } else if (event.getTag().equals(Constants.TAG_DAOJIA_URL)) {
            SendHomeBean msg = (SendHomeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        String jumpUrl = msg.getData().getJumpUrl();
                        Intent intent = new Intent(this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, jumpUrl);
                        intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                        startActivity(intent);
                    }
                    break;
            }
            dismissLoading();
        } else if (event.getTag().equals(Constants.TAG_QUERY_DAOJIA)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, (boolean) msg.getData());
                        if ((boolean) msg.getData()) {
                            queryDaoJiaStores();
                        } else {
                            startActivity(new Intent(MainActivity.this, VlifeOpenActivity.class));
                        }
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, false);
                    }
                    /*if (msg.getData() != null) {
                        if (msg.getData().getApplyStatus() == 1 && (msg.getData().getEdition() == 1 || (msg.getData().getEdition() == 2 && msg.getData().getOrderStatus() == 2 && msg.getData().getApplyStatus() == 1))) {
                            //开通了威到家业务(付费详情)
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_BUSINESS_ENABLE, true);
                            if (MyApplication.getIsMerchant()) {//用户是商户，点击了威到家菜单
                                queryDaoJiaStores();
                            } else {//用户不是商户，需要刷新首页显示威到家菜单按钮给用户点击
                                if (mHomeFragment != null) {
                                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, true);
                                    mHomeFragment.initMenuDataUI(true);//刷新fragment，此时的fragment是已经创建完成了的
                                } else {
                                    LogUtil.d("daojia", "是pos渠道，没有HomeFragment，不能到达此处");
                                }
                            }
                        } else {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_BUSINESS_ENABLE, false);
                            if (MyApplication.getIsMerchant()) {
                                Intent intent = new Intent(MainActivity.this, VlifeOpenActivity.class);
                                intent.putExtra(Constants.INTENT_DAOJIA_EDITION, msg.getData());
                                startActivity(intent);
                            } else {
                                LogUtil.d("daojia", "非商户，默认不显示威到家菜单按钮");
                            }
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }*/
                    break;
            }
            dismissLoading();
        } else if (event.getTag().equals(Constants.TAG_QUERY_SIGN)) {
            SignStatus bean = (SignStatus) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.QUERY_SIGN_FALSE:
                    break;
                case Constants.QUERY_SIGN_TRUE:
                    if (bean.getData() != null && bean.getData().getMemberContractStatus() != 10) {
                        showDialog();
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_PUT_SIGN)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    break;
            }
        }
        //版本更新返回
        else if (event.getTag().equals(Constants.MAIN_UPDATE_VERSION_TAG)) {
            UpdateVersionBean msg = (UpdateVersionBean) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mUpdateVersion = msg.getData();
                    if (mUpdateVersion != null) {
                        updateVersions(mUpdateVersion);
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.MAIN_UPDATE_PROGRESS_TAG)) {
            switch (event.getCls()) {
                case Constants.UPDATE_DOWNLOAD_SUCCESS:
                    File file = (File) event.getMsg();
                    if (file != null && file.isFile()) {
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, file);
                        apkurl = contentUri;
                        /*if (isWifiType) {
                            showUpgradeInfoDialog(mUpdateVersion, new ComDialogListener(mUpdateVersion, true), true);
                        } else {*/
//                            installAPK(apkurl, MainActivity.this);
                        openAPKFile();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(true);
                        }
//                        }
                    }
                    break;
                case Constants.UPDATE_DOWNLOAD_FAILED:
//                    if (!isWifiType) {
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(true);
//                        }
                        MyToast.showToast("下载失败，请重新下载！", Toast.LENGTH_SHORT);
                    }
                    break;
                case Constants.UPDATE_DOWNLOADING:
//                    if (!isWifiType) {
                    ProgressBean progressBean = (ProgressBean) event.getMsg();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setProgress(progressBean);
                    }
//                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_MAIN_QUEST_LOCATION)) {
            dismissLoading();
            LocationBean msg = (LocationBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    collectInfo();
                    break;
                case Constants.QUEST_LOCATION_FALSE:
                    collectInfo();
                    break;
                case Constants.QUEST_LOCATION_TRUE://请求成功
                    if (msg.getData() != null) {
                        LocationBean.DataEntity data = msg.getData();
                        collectInfo(data.getProvinceCode(), data.getCitycode(), data.getCitycode(), data.getTownship());
                    } else {
                        collectInfo();
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_QUERY_CASHIER_LIMIT)) {
            CashierLimitBean msg = (CashierLimitBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        if (msg.getData().getApplyRefund()) {
                            //收银员退款权限
                            SpUtil.putString(MainActivity.this, Constants.CASHIER_REFUND_TAG, "APPLY-REFUND");
                        } else {
                            SpUtil.putString(MainActivity.this, Constants.CASHIER_REFUND_TAG, "");
                        }
                        if (msg.getData().getApplyWater()) {
                            //收银员查看流水权限
                            SpUtil.putString(MainActivity.this, Constants.CASHIER_BILL_TAG, "APPLY-WATER");
                        } else {
                            SpUtil.putString(MainActivity.this, Constants.CASHIER_BILL_TAG, "");
                        }
                        if (msg.getData().getApplyBill()) {
                            //收银员查看统计权限
                            SpUtil.putString(MainActivity.this, Constants.CASHIER_REPORT_TAG, "APPLY-BILL");
                        } else {
                            SpUtil.putString(MainActivity.this, Constants.CASHIER_REPORT_TAG, "");
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_PRINT_ORDER_CONTENT)) {
            PrintContentBean content = (PrintContentBean) event.getMsg();
            MyThread myThread = new MyThread(content);
            new Thread(myThread).start(); //开启线程1
            new Thread(new Runnable() {
                @Override
                public void run() {
                    play();
                }
            }).start();
        } else if (event.getTag().equals("TAG_SVC_APP_LIST")) {
            SvcAppListBean msg = (SvcAppListBean) event.getMsg();
            if (Constants.ON_EVENT_TRUE.equals(event.getCls())) {
                if (msg.getData() != null && msg.getData().size() > 0) {
                    for (int i = 0; i < msg.getData().size(); i++) {
                        if ("CASH-POINT".equals(msg.getData().get(i).getAppCode())) {
                            SpUtil.putString(MyApplication.getContext(), Constants.SP_CASH_POINT, msg.getData().get(i).getId());
                        }
                        if ("ALIPAY-ISTALLMENT".equals(msg.getData().get(i).getAppCode())) {
                            SpUtil.putString(MyApplication.getContext(), Constants.SP_ALIPAY_ISTALLMENT, msg.getData().get(i).getId());
                        }
                    }
                }
            }
        } else if (event.getTag().equals("TAG_MAIN_PREAUTH_PAY")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            if (Constants.ON_EVENT_TRUE.equals(event.getCls())) {
                if (msg.getData() != null) {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_PLEDGE_OPEN_STATUS, msg.getData().getServiceStatus());
                } else {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_PLEDGE_OPEN_STATUS, "");
                }
            }
        }
    }

    //电子发票设备绑定状态
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onQueryMchStatusEleTicket(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_QUERY_MCHSTATUS_ELETICKET)) {
            dismissLoading();
            QueryMchStatusEleTicketBean msg = (QueryMchStatusEleTicketBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MainActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null) {
                        if (msg.getData() != null) {
                            String deviceBindStatus = msg.getData().getDeviceBindStatus();
                            if ("1".equals(deviceBindStatus)) {
                                //已绑定ukey盒子
                                startActivity(new Intent(MainActivity.this, BindUkeyTicketBoardCodeActivity.class));
                            } else {
                                //未绑定ukey盒子
                                startActivity(new Intent(MainActivity.this, BindUkeyTicketDeviceActivity.class));
                            }
                        } else {
                            MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_MAIN_NEW_CONFIG")) {
            ConfigBean msg = (ConfigBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
//                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(MainActivity.this, msg.getError().getMessage());
                            }
                        } else if (Constants.ERROR_CODE_CONFIG.equals(msg.getError().getCode())) {
                            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getStayUserId(), Constants.CONFIG_ALL);
                            setFragment();
                        } /*else {
                            if (msg.getError().getMessage() != null) {
                                showCommonNoticeDialog(MainActivity.this, msg.getError().getMessage());
                            }
                        }*/
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        Gson gson = new Gson();
                        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getStayUserId(), gson.toJson(msg));
                    } else {
                        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getStayUserId(), Constants.CONFIG_ALL);
                    }
                    setFragment();
                    break;
            }
        }
    }

    private void setFragment() {
        initConfigRadioGroup();
        try {
            if (mHomeFragment != null) {
                mHomeFragment.initValue(true);//刷新fragment，此时的fragment是已经创建完成了的
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_NOTICE)) {
                    mHomeFragment.setNoticeVisible(View.VISIBLE);
                    homeMessage();
                } else {
                    mHomeFragment.setNoticeVisible(View.GONE);
                }
            }
            if (mUserFragment != null) {
                mUserFragment.setView();//刷新fragment，此时的fragment是已经创建完成了的
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //获取通知数据
    private void homeMessage() {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", "15");
        map.put("currentPage", "1");
        ServerClient.newInstance(MyApplication.getContext()).getMessage(MyApplication.getContext(), Constants.TAG_GET_HOME_MESSAGE, map);
    }
    /**
     * 查询威到家的门店信息
     */
    public void queryDaoJiaStores() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            loadDialog(MainActivity.this, UIUtils.getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", 15);
            map.put("currentPage", 1);
            map.put("merchantDataType", 1);
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_DAOJIA_STORE_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    class MyThread implements Runnable {
        PrintContentBean content;

        public MyThread(PrintContentBean content) {
            super();
            this.content = content;
        }

        @SuppressLint("MissingPermission")
        @Override
        public void run() {
            synchronized (MainActivity.this) {
                boolean isPrintClientOrder = MyApplication.isPrintClientOrder() && isPrint(1, content.getScoPrintTypeList());
                boolean isPrintPayOrder = MyApplication.isPrintPayOrder() && isPrint(3, content.getScoPrintTypeList());
                if (MyApplication.getDeviceEnable(2) && (isPrintClientOrder || isPrintPayOrder)) {
                    if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(2))) {
                        boolean isSucc = BluePrintUtil.blueConnent(2, MainActivity.this);
                        if (isSucc) {
                            if (isPrintClientOrder) {
                                BluePrintUtil.payOrderPrint(content, true);
                            }
                            if (isPrintPayOrder) {
                                BluePrintUtil.payOrderPrint(content, false);
                            }
                        } else {
                            MainActivity.this.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                                        showPrintDialog(2);
                                    } else {
                                        //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                                        LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                                    }
                                }
                            });
                        }
                    } else {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showPrintDialog(2);
                            }
                        });
                    }
                }
                if (MyApplication.getDeviceEnable(3) && MyApplication.isPrintKitchenOrder() && isPrint(2, content.getScoPrintTypeList())) {
                    if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(3))) {
                        boolean isSucc = BluePrintUtil.blueConnent(3, MainActivity.this);
                        if (isSucc) {
                            BluePrintUtil.kitchenPrint(content);
                        } else {
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                                        showPrintDialog(3);
                                    } else {
                                        //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                                        LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                                    }
                                }
                            });
                        }
                    } else {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showPrintDialog(3);
                            }
                        });
                    }
                }
            }
        }

    }

    private boolean isPrint(int type, int[] socPrintTypeList) {
        if (socPrintTypeList != null && socPrintTypeList.length > 0) {
            for (int i : socPrintTypeList) {
                if (type == i)
                    return true;
            }
        }
        return false;
    }

    private synchronized void play() {
        if (VoiceUtils.with(this).getIsPlay()) {
            LogUtil.d("play-voice===" + "正在播放语音 ");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Thread.sleep(100);//休眠0.1秒
                        play();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                    /**
                     * 要执行的操作
                     */
                }
            }.start();
        } else {
            LogUtil.d("play-voice===" + "不冲突");
            VoiceUtils.with(this).playReceiveOrder();
        }
    }

    /**
     * 获取扫码点单地址
     *
     * @param storeId 门店id
     */
    private void mchOauth(String storeId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(false, UIUtils.getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", storeId);
            ServerClient.newInstance(MyApplication.getContext()).mchOauth(MyApplication.getContext(), Constants.TAG_SCAN_ORDER_URL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getDaojiaUrl(String storeId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(false, UIUtils.getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", storeId);
            ServerClient.newInstance(MyApplication.getContext()).getDaojiaUrl(MyApplication.getContext(), Constants.TAG_DAOJIA_URL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void updateVersions(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (VersionUtils.getVersionName(MyApplication.getContext()).equals(data.getVersionName())) {
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, false);
                return;
            }
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
//                if (AppHelper.getApkType() == 0 || AppHelper.getApkType() == 2 || AppHelper.getApkType() == 3 || AppHelper.getApkType() == 4) {
                update(data);
//                }
            }
            /*if (data.getVersionCode() > 0) {
                if (data.getVersionCode() == VersionUtils.getVersionCode(MyApplication.getContext())) {
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, false);
                    return;
                }
                if (data.getVersionCode() > VersionUtils.getVersionCode(MyApplication.getContext())) {
                    if (AppHelper.getApkType() == 0 || AppHelper.getApkType() == 2) {
                        boolean results = PermissionUtils.checkPermissionArray(MainActivity.this, permissionArray);
                        if (results) {
                            update(data);
                        } else {
                            showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_update));
                        }
                    }
                }
            }*/
        }
    }

    private void update(UpdateVersionBean.DataBean data) {
        //进行版本升级
        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, true);
        String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
        mService = new Intent(MainActivity.this, DownLoadService.class);
        mService.putExtra("appurl", data.getAppUrl());
        mService.putExtra("versionName", data.getVersionName());
        if (!versionName.equals(data.getVersionName())) {
            //如何服务器版本大于保存的版本，直接下载最新的版本
            //启动服务
            LogUtil.d("版本升级---aaaa");
            /*if (NetworkUtils.isWifi(MyApplication.getContext())) {
                isWifiType = true;
                LogUtil.d("版本升级---aaaa--wifi");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.this.startService(mService);
                    }
                }).start();
            } else {*/
            isWifiType = false;
            showUpgradeInfoDialog(data, new ComDialogListener(data), true);
//            }
        } else {
            LogUtil.d("版本升级---bbb");
            String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
            if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                LogUtil.d("版本升级---ccc");
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, new File(string));
                apkurl = contentUri;
                showUpgradeInfoDialog(data, new ComDialogListener(data), true);
            }
        }
    }

    /*private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(MainActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(MainActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }*/

    /*private void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(MainActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    installDownloadApk(mUpdateVersion);
                } else {
                    showDialog(getString(R.string.permission_set_content_update));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void installDownloadApk(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                //进行版本升级
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, true);
                String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
                mService = new Intent(MainActivity.this, DownLoadService.class);
                mService.putExtra("appurl", data.getAppUrl());
                mService.putExtra("versionName", data.getVersionName());
                mService.putExtra(Constants.INTENT_NAME, Constants.MAIN_UPDATE_PROGRESS_TAG);
                if (!versionName.equals(data.getVersionName())) {
                    //如何服务器版本大于保存的版本，直接下载最新的版本
                    //启动服务
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.this.startService(mService);
                        }
                    }).start();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(false);
                    }
                } else {
                    String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
                    if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, new File(string));
                        apkurl = contentUri;
                        showUpgradeInfoDialog(data, new ComDialogListener(data), true);
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                MainActivity.this.startService(mService);
                            }
                        }).start();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(false);
                        }
                    }
                }
            }
        }

    }

    public void showUpgradeInfoDialog(UpdateVersionBean.DataBean result, CommonConfirmDialog.ConfirmListener listener, boolean isWifi) {
        mCommonConfirmDialog = new CommonConfirmDialog(MainActivity.this, listener, result, isWifi);
        if (!mCommonConfirmDialog.isShowing()) {
            mCommonConfirmDialog.show();
        }
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpdateVersionBean.DataBean result;

        public ComDialogListener(UpdateVersionBean.DataBean result) {
            this.result = result;
        }

        @Override
        public void ok() {
            /*if (isWifi) {
                if (apkurl != null) {
//                    LogUtil.d("zhouwei4==" + apkurl.getPath());
                    openAPKFile();
//                    installAPK(apkurl, MainActivity.this);
                }
            } else {*/
            boolean results = PermissionUtils.checkPermissionArray(MainActivity.this, permissionArray);
            if (results) {
                if (apkurl != null) {
                    openAPKFile();
                } else {
                    installDownloadApk(result);
                }
            } else {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_update));
            }
//            }
        }

        @Override
        public void cancel() {
            //SpUtil.putBoolean(MyApplication.getContext(), Constants.UPDATE_VERSION_CANCEL, false);
            if (mService != null) {
                stopService(mService);
            }
        }
    }

    /**
     * 打开安装包
     */
    private void openAPKFile() {
        String filePath = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
        String mimeDefault = "application/vnd.android.package-archive";
        File apkFile = null;
        if (!TextUtils.isEmpty(filePath) && !TextUtils.isEmpty(Uri.parse(filePath).getPath())) {
            apkFile = new File(filePath);
        }
        if (apkFile == null) {
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //兼容7.0
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //这里牵涉到7.0系统中URI读取的变更
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, new File(filePath));
                intent.setDataAndType(contentUri, mimeDefault);
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                        return;
                    }
                }
            } else {
                intent.setDataAndType(Uri.fromFile(apkFile), mimeDefault);
            }
            if (getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                //如果APK安装界面存在，携带请求码跳转。使用forResult是为了处理用户 取消 安装的事件。外面这层判断理论上来说可以不要，但是由于国内的定制，这个加上还是比较保险的
                startActivity(intent);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到设置-允许安装未知来源-页面
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {
        //后面跟上包名，可以直接跳转到对应APP的未知来源权限设置界面。使用startActivityForResult 是为了在关闭设置界面之后，获取用户的操作结果，然后根据结果做其他处理
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, Constants.REQUEST_INSTALL_PACKAGES);
    }

    /**
     * 功用：未知来源权限弹窗
     * 说明：8.0系统中升级APK时，如果跳转到了 未知来源权限设置界面，并且用户没用允许该权限，会弹出此窗口
     */
    private void showUnKnowResourceDialog() {
        SelectDialog dialog = new SelectDialog(MainActivity.this, "未知来源权限设置界面", R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                    }
                }
            }
        });
    }

    public String getMIMEType(File var0) {
        String var1 = "";
        String var2 = var0.getName();
        String var3 = var2.substring(var2.lastIndexOf(".") + 1, var2.length()).toLowerCase();
        var1 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(var3);
        return var1;
    }

    private void showDialog() {
        if (mSignDialog == null) {
            mSignDialog = new SignDialog(MainActivity.this);
            mSignDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK
                            && event.getAction() == KeyEvent.ACTION_UP) {
                    }
                    return false;

                }
            });
        }
        mSignDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    /*    if (Constants.ZYTPAY_LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        }*/
        if (isToSetNotice) {
            isToSetNotice = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                checkNotificationEnabled();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        } else */
        if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintPowerOnAndOffUtils.powerOff();
        } else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            n900Device.disconnect();
//        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
        } else if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
            unbindService();
        }
        if (mSignDialog != null) {
            mSignDialog.dismiss();
            mSignDialog = null;
        }
        if (mDialogReset != null) {
            mDialogReset.dismiss();
            mDialogReset = null;
        }
        if (mCommonConfirmDialog != null) {
            mCommonConfirmDialog.dismiss();
        }

        if (mTodayDataBroadcastReceiver != null) {
            unregisterReceiver(mTodayDataBroadcastReceiver);
        }
    }

    private int mCheckedId;

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        //底部导航按钮选中状态切换的时候就切换ViewPager显示的页面
        mCheckedId = checkedId;
        Log.d("tagtag", "mCheckedId:" + mCheckedId);
        switch (checkedId) {
            case R.id.rb_bottom_home:
                setIndexSelected(0);
                break;
            case R.id.rb_bottom_app:
                setIndexSelected(1);
                break;
            case R.id.rb_bottom_mch_talk:
                setIndexSelected(2);
                break;
            case R.id.rb_bottom_income:
                setIndexSelected(3);
                break;
            case R.id.rb_bottom_receive:
                setIndexSelected(4);
                break;
            case R.id.rb_bottom_bill:
                setIndexSelected(5);
                break;
            case R.id.rb_bottom_report:
                setIndexSelected(6);
                break;
            case R.id.rb_bottom_user:
                setIndexSelected(7);
                break;
        }
    }

    /**
     * 监听返回键
     */
    @Override
    public void onBackPressed() {
        showExitDialog(this);
    }

    /**
     * 如果需要监听返回键被按下的事件，可以实现这个接口
     */
    public interface OnBackPressedListener {
        void onPressed();
    }

    /**
     * 给fragment提供touch事件
     */
    public interface MyTouchListener {
        void onTouchEvent(MotionEvent event);
    }

    // 保存MyTouchListener接口的列表
    private ArrayList<MyTouchListener> myTouchListeners = new ArrayList<MyTouchListener>();

    /**
     * 提供给Fragment通过getActivity()方法来注册自己的触摸事件的方法
     *
     * @param listener
     */
    public void registerMyTouchListener(MyTouchListener listener) {
        myTouchListeners.add(listener);
    }

    /**
     * 提供给Fragment通过getActivity()方法来取消注册自己的触摸事件的方法
     *
     * @param listener
     */
    public void unRegisterMyTouchListener(MyTouchListener listener) {
        myTouchListeners.remove(listener);
    }

    /**
     * 分发触摸事件给所有注册了MyTouchListener的接口
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        for (MyTouchListener listener : myTouchListeners) {
            listener.onTouchEvent(ev);
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 设置小圆点显示
     *
     * @param isUnRead
     */
    public void setRadioButtonState(boolean isUnRead) {
        if (isUnRead) {
            mRlRbState.setVisibility(View.VISIBLE);
        } else {
            mRlRbState.setVisibility(View.GONE);
        }
    }

    public void setStateHide() {
        mRlRbState.setVisibility(View.GONE);
    }

    public void setStateShow() {
        mRlRbState.setVisibility(View.VISIBLE);
    }

    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_TODAY_DATA);
        intentFilter.addAction(Constants.ACTION_RECEIVED_CLIENT_ID_PUSH);
        mTodayDataBroadcastReceiver = new TodayDataBroadcastReceiver();
        registerReceiver(mTodayDataBroadcastReceiver, intentFilter);
    }

    class TodayDataBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.ACTION_RECEIVED_CLIENT_ID_PUSH.equals(intent.getAction())) {
                //个推的clientId
                String clientId = intent.getStringExtra(Constants.INTENT_KEY_CLIENTID);
                LogUtil.i("Jeremy", "login=client2==" + clientId);
                if (!mIsPushClientId) {
                    mIsPushClientId = true;
                    Map<String, Object> map = new HashMap<>();
                    if (AppHelper.getAppType() == 1) {
                        map.put("client", Constants.REQUEST_CLIENT_POS);
                    } else if (AppHelper.getAppType() == 2) {
                        map.put("client", Constants.REQUEST_CLIENT_APP);
                    }
                    map.put("pushDeviceId", clientId);
                    ServerClient.newInstance(MyApplication.getContext()).getuiPushVoice(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_TAG, map);
                }
            }
        }
    }


    private void showNoStoreDialog() {
        NoticeDialog noticeDialog = new NoticeDialog(MainActivity.this
                , getString(R.string.tv_no_store), "", R.layout.notice_dialog_common);
        noticeDialog.show();
    }

    public void getLoginDialog(Activity context) {
        if (mDialogFloat == null) {
            mDialogFloat = new SelectDialog(context, "请允许应用显示悬浮窗,商户审核通知需用到该权限", "取消", "去设置", R.layout.select_common_dialog);
            mDialogFloat.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, Constants.REQUEST_SYSTEM_OVERLAY_WINDOW);
                }
            });
            mDialogFloat.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    MyToast.showToastShort("取消后无法显示悬浮窗");
                }
            });
            DialogHelper.resize(context, mDialogFloat);
        }
        mDialogFloat.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_SYSTEM_OVERLAY_WINDOW) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (onOpChangedListener != null) {
                    AppOpsManager opsManager = (AppOpsManager) MyApplication.getContext().getSystemService(Context.APP_OPS_SERVICE);
                    opsManager.stopWatchingMode(onOpChangedListener);
                    onOpChangedListener = null;
                }
                // The draw overlay permission status can be retrieved from canDraw
                LogUtil.d("canDrawOverlay = {}" + canDraw);
                if (!canDraw) {
                    MyToast.showToastShort("允许应用显示悬浮窗才能显示商户审核通知浮窗");
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SCAN_ORDER_STORE_LIST) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            if (shopBean != null) {
                mchOauth(shopBean.getStoreId());
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_DAOJIA_STORE_LIST) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            if (shopBean != null) {
                getDaojiaUrl(shopBean.getStoreId());
            }
        }
        if (requestCode == Constants.REQUEST_INSTALL_PACKAGES) {
            if (resultCode == RESULT_OK) {
                openAPKFile();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        showUnKnowResourceDialog();
                    }
                }
            }
        }
    }

    //引导用户前往通知页面打开允许应用通知开关
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void checkNotificationEnabled() {
        boolean isEnabled = isNotificationEnabled(this);
        LogUtil.i("MainActivity", "is notification enabled: " + isEnabled);
        if (!isEnabled) {
            getNoticeDialog(this);
        } else {
            keepAlive();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public boolean isNotificationEnabled(Context context) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        return notificationManagerCompat.areNotificationsEnabled();
    }

    private boolean isToSetNotice;

    public void getNoticeDialog(Activity context) {
        SelectDialog dialog = new SelectDialog(context, "请允许应用显示通知,接收推送通知时需用到该权限", "取消", "去设置", R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = new Intent();
                if (Build.VERSION.SDK_INT >= 26) {
                    // android 8.0引导设置通知开启
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());
                } else if (Build.VERSION.SDK_INT >= 21) {
                    // android 5.0-7.0引导设置通知开启
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("app_package", getPackageName());
                    intent.putExtra("app_uid", getApplicationInfo().uid);
                } else {
                    // 其他
                    intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                    intent.setData(Uri.fromParts("package", getPackageName(), null));
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                isToSetNotice = true;
            }
        });
        dialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                ToastUtil.showToastLong("取消后无法显示通知");
            }
        });
        DialogHelper.resize(context, dialog);
        dialog.show();
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        switch (config.orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                break;
        }
    }

}
