package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.adapter.MerchantInfoAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StatusBarUtil;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 门店单选列表
 */
public class MerchantIdActivity extends BaseActivity implements View.OnClickListener, MerchantInfoAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mButton;
    private RecyclerView mRvMerchant;
    private List<MerchantIdBean.DataEntity> mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_id);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mButton.setVisibility(View.INVISIBLE);
        mTvTitle.setText(getString(R.string.title_select_merchant));

        mRvMerchant = (RecyclerView) findViewById(R.id.rv_merchant_info);

    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    public void initData() {
        mData = (List<MerchantIdBean.DataEntity>) getIntent().getSerializableExtra(Constants.INTENT_RESET_MERCHANT_ID);
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRvMerchant.setLayoutManager(linearLayoutManager);
        MerchantInfoAdapter adapter = new MerchantInfoAdapter(this, mData);
        mRvMerchant.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        resetPassword(position);
    }

    private void resetPassword(int position) {
        Intent intent = new Intent(MerchantIdActivity.this, ResetPasswordActivity.class);
        intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, mData.get(position).getOrgId());
        startActivity(intent);
    }
}
