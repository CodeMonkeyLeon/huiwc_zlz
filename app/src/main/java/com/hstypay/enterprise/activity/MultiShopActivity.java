package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.MultiShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/25 10:09
 * @描述: 门店多选列表页
 */

public class MultiShopActivity extends BaseActivity implements View.OnClickListener, MultiShopRecyclerAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mButton;
    private RecyclerView mRvShop;
    private EditText mEtInput;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private TreeMap<String, String> selectMap = new TreeMap<>();
    private MultiShopRecyclerAdapter mAdapter;
    private TextView mTv_not_data;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private ImageView mIvClean;
    private int storeDataType;
    //private List<CshierDetailBean.DataBean.StoresBean> storeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        currentPage = 2;
        initView();
        initEvent();
        initData();

    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mRvShop.setLayoutManager(new LinearLayoutManager(this));
        mEtInput = (EditText) findViewById(R.id.et_user_input);
        mTv_not_data = (TextView) findViewById(R.id.tv_not_data);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mTvTitle.setText(R.string.title_select_shop);
        mButton.setText(R.string.tv_enter);
        initRecyclerView();
        initSwipeRefreshLayout();

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtInput.isFocused()) {
                    if (mEtInput.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);

                    } else {
                        mIvClean.setVisibility(View.GONE);
                        mTv_not_data.setVisibility(View.GONE);
                        mList.clear();
                        loadData("15", "1", "");
                    }
                }

            }
        });
        mEtInput.addTextChangedListener(editTextWatcher);
        mEtInput.setOnFocusChangeListener(listener);

    }


    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_user_input:
                    if (hasFocus) {
                        if (mEtInput.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                            mTv_not_data.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };


    private void initRecyclerView() {
        String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        storeDataType = getIntent().getIntExtra(Constants.INTENT_STORE_DATA_TYPE, 0);
        if (Constants.INTENT_NAME_CASHIER_MULTISHOP.equals(intentName)) {
            selectMap = new TreeMap<>((HashMap) getIntent().getSerializableExtra(Constants.INTENT_CASHIER_SHOP));
        }
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new MultiShopRecyclerAdapter(MultiShopActivity.this, mList, selectMap);
        mRvShop.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setRefreshEnable(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO 触发刷新数据任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    currentPage = 2;
                    mList.clear();
                    /*if (!StringUtils.isEmptyOrNull(mEtInput.getText().toString())) {
                        //getSearchData("15","1",mEtInput.getText().toString());
                    } else {*/
                    loadData("15", "1", "");
                    //}
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                //TODO 触发加载更多任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    if (!StringUtils.isEmptyOrNull(mEtInput.getText().toString().trim())) {
                        loadData(pageSize + "", currentPage + "", mEtInput.getText().toString().trim());
                    } else {
                        loadData(pageSize + "", currentPage + "", "");
                    }
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        mLinearLayoutManager.setScrollEnabled(false);
                        mRvShop.setLayoutManager(mLinearLayoutManager);
                        break;
                }
            }
        });
    }

    private void loadData(String pageSize, String currentPage, String content) {

        if (!NetworkUtils.isNetworkAvailable(MultiShopActivity.this)) {
            showCommonNoticeDialog(MultiShopActivity.this, ToastHelper.toStr(R.string.network_exception));
        } else {
            if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
                showNewLoading(true, getString(R.string.public_loading));
            }
            // mList.clear();
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            if (content != null) {
                map.put("storeName", content);
            }
            if (storeDataType == 1) {
                map.put("merchantDataType", storeDataType);
            }
            ServerClient.newInstance(MultiShopActivity.this).choiceStore(MultiShopActivity.this, Constants.TAG_CASHIER_DETAIL_CHOICE_STORE, map);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mIvClean.setOnClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mList.clear();
                    currentPage = 2;
                    loadData("15", "1", mEtInput.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });
    }

    public void initData() {
        showNewLoading(true, getString(R.string.public_loading));
        loadData("15", "1", "");
    }

    //Eventbus接收数据，所属门店
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCashierDetailChoiceStore(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CASHIER_DETAIL_CHOICE_STORE)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    setErrorState(isRefreshed, isLoadmore, 200);
                    //MyToast.showToastShort(getString(R.string.net_error) + Constants.PAY_TAG);
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_CASHIER_DETAIL_CHOICE_STORE);
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    dismissLoading();
                    setErrorState(isRefreshed, isLoadmore, 200);
                    if (msg.getError() != null) {

                        if (msg.getError().getCode() != null) {

                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {

                                if (msg.getError().getMessage() != null) {

                                    getLoginDialog(MultiShopActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(MultiShopActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    dismissLoading();
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    List<StoreListBean.DataEntity> data = msg.getData();
                    if (data != null && data.size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTv_not_data.setVisibility(View.GONE);
                        mList.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (mList == null || mList.size() == 0) {
                            mTv_not_data.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                        } else {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mTv_not_data.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (selectMap == null || selectMap.size() == 0) {
                    MyToast.showToast(UIUtils.getString(R.string.please_choice_store), Toast.LENGTH_SHORT);
                    return;
                }
                Intent intentShop = new Intent();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("data", selectMap);   //传递一个user对象列表
                intentShop.putExtras(mBundle);
                setResult(RESULT_OK, intentShop);
                finish();
                break;

            case R.id.iv_clean:
                mList.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                mTv_not_data.setVisibility(View.GONE);
                //loadData("15", "1","");
                break;
            default:
                break;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                    mLinearLayoutManager.setScrollEnabled(true);
                    mRvShop.setLayoutManager(mLinearLayoutManager);
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                    mLinearLayoutManager.setScrollEnabled(true);
                    mRvShop.setLayoutManager(mLinearLayoutManager);
                }
            }, delay);
        }
    }

    @Override
    public void onItemClick(int position) {
        if (mList.get(position).isSelcet()) {
            selectMap.put(mList.get(position).getStoreId(), mList.get(position).getStoreName());
        } else {
            selectMap.remove(mList.get(position).getStoreId());
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
