package com.hstypay.enterprise.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.AndroidBug5497Workaround;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.HtmlMTABean;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/27 10:33
 * @描述: ${TODO}
 */

public class OutWebViewActivity extends BaseActivity implements View.OnClickListener {

    private WebView mWvResgister;
    private ImageView mIvBack, mIvClose;
    private TextView mTvTitlte;

    private ValueCallback<Uri> mUploadMessage;// 表单的数据信息
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    private final static int FILECHOOSER_RESULTCODE = 1;// 表单的结果回调</span>
    private Uri imageUri;
    private String url;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private String[] locationPermissionArray = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE5 = 1005;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 1006;
    private ProgressBar mPg;
    private String fromUrl,intentLoan;
    private boolean isAllowClose;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_webview);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        AndroidBug5497Workaround.assistActivity(this);
        fromUrl = getIntent().getStringExtra(Constants.REGISTER_INTENT_URL);
        intentLoan = getIntent().getStringExtra(Constants.INTENT_NAME);
        isAllowClose = getIntent().getBooleanExtra(Constants.REGISTER_ALLOW_CLOSE, false);
        if (savedInstanceState != null) {
            url = savedInstanceState.getString("url");
        } else {
            url = getIntent().getStringExtra(Constants.INTENT_OUT_URL);
        }
        if (url == null) {
            return;
        }
        if (Constants.INTENT_LOAN.equals(intentLoan)) {
            isAllowClose = true;
        }
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvClose = (ImageView) findViewById(R.id.iv_close);
        mTvTitlte = (TextView) findViewById(R.id.tv_title);
        mWvResgister = (WebView) findViewById(R.id.wv_register);
        mPg = (ProgressBar) findViewById(R.id.progressBar);
        mIvBack.setOnClickListener(this);
        mIvClose.setOnClickListener(this);

        if (AppHelper.getAndroidSDKVersion() < 17) {
            mWvResgister.removeJavascriptInterface("searchBoxjavaBridge_");//解决 CVE-2014-1939 漏洞
            mWvResgister.removeJavascriptInterface("accessibility");//解决  CVE-2014-7224漏洞
            mWvResgister.removeJavascriptInterface("accessibilityTraversal");//解决  CVE-2014-7224漏洞
        }

        WebSettings webSettings = mWvResgister.getSettings();
        setZoom(webSettings);
        //定位权限
        webSettings.setDatabaseEnabled(true);
        //设置定位的数据库路径
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setGeolocationDatabasePath(dir);
        webSettings.setGeolocationEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
        webSettings.setDomStorageEnabled(true);

//        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccess(false);
        webSettings.setAllowFileAccessFromFileURLs(false);
        webSettings.setAllowUniversalAccessFromFileURLs(false);
        //webSettings.setSavePassword(false);

        String ua = webSettings.getUserAgentString();
        webSettings.setUserAgentString(ua + AppHelper.getUserAgent(MyApplication.getContext()));
        //设置为可调用js方法
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        webSettings.setJavaScriptEnabled(true);
        webSettings.setBlockNetworkImage(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        mWvResgister.requestFocus();
        mWvResgister.addJavascriptInterface(new JsInteration(), "android");

        setWebChromeClient(mWvResgister);

        String[] cookies = SpUtil.getString(MyApplication.getContext(), Constants.LOGIN_COOKIE, "").split("<<<->>>");
        if (cookies != null && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                setCookie(url, cookies[i]);
            }
        }
        mWvResgister.loadUrl(url);

        mWvResgister.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Constants.INTENT_LOAN.equals(intentLoan)) {
                    mTvTitlte.setText("汇贷款");
                } else {
                    mTvTitlte.setText(view.getTitle());
                }
                mIvClose.setVisibility(isAllowClose ? View.VISIBLE : View.INVISIBLE);
                //调用拨号程序
                if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:")) {
                    if (AppHelper.getAppType() == 2) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    } else if (AppHelper.getAppType() == 1) {
                        return true;
                    }
                }
                return false;
            }

            //华为mate9保时捷版打不开
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (Constants.INTENT_LOAN.equals(intentLoan)) {
                    mTvTitlte.setText("汇贷款");
                } else {
                    mTvTitlte.setText(view.getTitle());
                }
            }
        });
    }

    public void setWebChromeClient(WebView wvResgister) {
        wvResgister.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mPg.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    mPg.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    mPg.setProgress(newProgress);//设置进度值
                }
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (Constants.INTENT_LOAN.equals(intentLoan)) {
                    mTvTitlte.setText("汇贷款");
                } else {
                    mTvTitlte.setText(view.getTitle());
                }
            }

            @Override
            public boolean onShowFileChooser(WebView webView,
                                             ValueCallback<Uri[]> filePathCallback,
                                             FileChooserParams fileChooserParams) {
                mUploadCallbackAboveL = filePathCallback;
                take();
                return true;
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                take();
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);

            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE5:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE6:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_location));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void setCookie(String url, String stringCookie) {
        CookieSyncManager.createInstance(MyApplication.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeSessionCookies(null);
            cookieManager.setAcceptThirdPartyCookies(mWvResgister, true);
            cookieManager.flush();
        } else {
            cookieManager.removeSessionCookie();
            cookieManager.setAcceptCookie(true);
            CookieSyncManager.getInstance().sync();
        }
        cookieManager.setAcceptCookie(true);
        cookieManager.setCookie(url, stringCookie);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                WebBackForwardList webBackForwardList = mWvResgister.copyBackForwardList();
                if (mWvResgister.canGoBack()) {
                        mWvResgister.goBack();
                } else {
                    if (Constants.WELCOME_TO_REGISTER.equals(fromUrl)) {
                        int autoStatus = SpUtil.getInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, 0);
                        if (autoStatus == 2) {
                            startActivity(new Intent(this, MainActivity.class));
                            finish();
                        } else {
                            finish();
                        }
                    } else if (Constants.WELCOME_TO_REGISTER_SECOND.equals(fromUrl)) {
                        startActivity(new Intent(OutWebViewActivity.this, LoginActivity.class));
                        finish();
                    } else {
                        finish();
                    }
                }
                break;
            case R.id.iv_close:
                finish();
                break;
            default:
                break;
        }
    }

    /*private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(OutWebViewActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(OutWebViewActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }

    private void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(OutWebViewActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }*/
    
    public class JsInteration {

        @JavascriptInterface
        public void completeSubmit() {
            OutWebViewActivity.this.finish();
        }

        @JavascriptInterface
        public boolean checkCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(OutWebViewActivity.this, permissionArray);
            return cameraPermission;
        }

        @JavascriptInterface
        public void requestCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(OutWebViewActivity.this, permissionArray);
            if (!cameraPermission){
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE5, permissionArray, getString(R.string.permission_content_photo));
            }
        }

        @JavascriptInterface
        public boolean checkLocationPermission() {
            boolean locationPermission = PermissionUtils.checkPermissionArray(OutWebViewActivity.this, locationPermissionArray);
            return locationPermission;
        }

        @JavascriptInterface
        public void requestLocationPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(OutWebViewActivity.this, locationPermissionArray);
            if (!cameraPermission){
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE6, locationPermissionArray, getString(R.string.permission_content_location));
            }
        }

        @JavascriptInterface
        public void mtaFunction(String json) {
            if (TextUtils.isEmpty(json))
                return;
            Gson gson = new Gson();
            HtmlMTABean htmlMTABean = gson.fromJson(json, HtmlMTABean.class);
            if (htmlMTABean != null) {
                if (htmlMTABean.getProps() != null && htmlMTABean.getProps().size() > 0) {
                    Properties prop = new Properties();
                    for (int i = 0; i < htmlMTABean.getProps().size(); i++) {
                        HtmlMTABean.PropsEntity propsEntity = htmlMTABean.getProps().get(i);
                        prop.setProperty(propsEntity.getKey(), propsEntity.getValue());
                    }
                    MtaUtils.mtaPro(OutWebViewActivity.this, htmlMTABean.getId(), prop);
                } else {
                    MtaUtils.mtaId(OutWebViewActivity.this, htmlMTABean.getId());
                }
            }
        }

        /**
         * 跳转到微信
         */
        @JavascriptInterface
        public void getWechatApi() {
            try {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                ComponentName cmp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setComponent(cmp);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // TODO: handle exception
                MyToast.showToastLong("检查到您手机没有安装微信，请安装后使用该功能");
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mWvResgister.canGoBack()) {
            mWvResgister.goBack();
        } else {
            if (Constants.WELCOME_TO_REGISTER.equals(fromUrl)) {
                int autoStatus = SpUtil.getInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, 0);
                if (autoStatus == 2) {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                } else {
                    finish();
                }
            } else if (Constants.WELCOME_TO_REGISTER_SECOND.equals(fromUrl)) {
                startActivity(new Intent(OutWebViewActivity.this, LoginActivity.class));
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage && null == mUploadCallbackAboveL) return;
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (mUploadCallbackAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (mUploadMessage != null) {

                if (result != null) {
                    String path = getPath(getApplicationContext(),
                            result);
                    Uri uri = Uri.fromFile(new File(path));
                    mUploadMessage
                            .onReceiveValue(uri);
                } else {
                    mUploadMessage.onReceiveValue(imageUri);
                }
                mUploadMessage = null;

            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        LogUtil.i("OutWebViewActivity", "ThirdActivity onSaveInstanceState");
        outState.putString("url", url);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        LogUtil.i("OutWebViewActivity", "ThirdActivity onRestoreInstanceState");
        if (savedInstanceState != null) {
            String url = (String) savedInstanceState.getString("url");
        }
    }

    @SuppressWarnings("null")
    @TargetApi(Build.VERSION_CODES.BASE)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent data) {
        if (requestCode != FILECHOOSER_RESULTCODE
                || mUploadCallbackAboveL == null) {
            return;
        }
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (data == null) {
                results = new Uri[]{imageUri};
            } else {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();

                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        if (results != null) {
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        } else {
            results = new Uri[]{imageUri};
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        }

        return;
    }


    private void take() {
        File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyApp");
        // Create the storage directory if it does not exist
        if (!imageStorageDir.exists()) {
            imageStorageDir.mkdirs();
        }
        File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        imageUri = Uri.fromFile(file);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent i = new Intent(captureIntent);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            i.setPackage(packageName);
            i.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(i);
        }
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        OutWebViewActivity.this.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void setZoom(WebSettings settings) {
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWvResgister!=null) {
            mWvResgister.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mWvResgister!=null) {
            mWvResgister.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWvResgister!=null) {
            mWvResgister.destroy();
        }
    }
}
