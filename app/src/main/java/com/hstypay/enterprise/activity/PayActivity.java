package com.hstypay.enterprise.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SingleLineZoomTextView;
import com.hstypay.enterprise.adapter.HuabeiRateAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.HbfqRateBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class PayActivity extends BaseActivity implements View.OnClickListener {

    private View mViewLine;
    private ImageView ivBack, mIvStoreName;
    private LinearLayout num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, lay_point, lay_add, lay_clear, lay_delete, scan_pay;
    private TextView tv_money, mTvRemark/*, mTvSixMoney, mTvSixRate, mTvTwelveMoney, mTvTwelveRate*/;
    private SingleLineZoomTextView tv_calculate;
    private Context context;
    private RecyclerView mRecyclerViewRate;
    private ScrollView mSvHbfq;

    private LinearLayout tv_choice_store, mLlHbfqDetail, mLlKeyboardView, mLlMoney;
    private RelativeLayout /*mRlSix, mRlTwelve,*/ mRlUnion, mRlHuabei, mRlView;
    private TextView mTv_store_name;
    private String store_Id = "";

    private StringBuilder calculateBuilder;
    private double totalMoney;
    private Button mBtnPay;
    private String mPayRemark = "";
    private RadioButton mCbHbfq, mCbUnion;
    private View mViewUnion, mViewHbfq;
    private boolean enable;
    private String mHbFqNum = "";
    private boolean isRateFree;
    private boolean mUnionFeeFlag;
    private long mUnionUserFee;
    private List<HbfqRateBean.DataBean> mRateData;
    private SelectDialog mSelectDialog;
    private HuabeiRateAdapter mAdapter;
    private long lastInputTimeMillis;
    private boolean firstRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        context = MyApplication.getContext();
        initView();
        initListener();
        initData();
    }

    private void initView() {
        ivBack = (ImageView) findViewById(R.id.iv_back);
        num_0 = (LinearLayout) findViewById(R.id.num_0);
        num_1 = (LinearLayout) findViewById(R.id.num_1);
        num_2 = (LinearLayout) findViewById(R.id.num_2);
        num_3 = (LinearLayout) findViewById(R.id.num_3);
        num_4 = (LinearLayout) findViewById(R.id.num_4);
        num_5 = (LinearLayout) findViewById(R.id.num_5);
        num_6 = (LinearLayout) findViewById(R.id.num_6);
        num_7 = (LinearLayout) findViewById(R.id.num_7);
        num_8 = (LinearLayout) findViewById(R.id.num_8);
        num_9 = (LinearLayout) findViewById(R.id.num_9);
        lay_point = (LinearLayout) findViewById(R.id.lay_point);
        lay_add = (LinearLayout) findViewById(R.id.lay_add);
        lay_clear = (LinearLayout) findViewById(R.id.lay_clear);
        lay_delete = (LinearLayout) findViewById(R.id.lay_delete);
        scan_pay = (LinearLayout) findViewById(R.id.ly_to_scan_pay);
        tv_money = (TextView) findViewById(R.id.tv_money);
        tv_calculate = (SingleLineZoomTextView) findViewById(R.id.tv_calculate);

        tv_choice_store = (LinearLayout) findViewById(R.id.tv_choice_store);
        mTv_store_name = (TextView) findViewById(R.id.tv_store_name);
        mIvStoreName = (ImageView) findViewById(R.id.iv_store_name);
        mTvRemark = findViewById(R.id.btn_remark);

        mSvHbfq = findViewById(R.id.sv_hbfq);
        mRlUnion = findViewById(R.id.rl_union);
        mRlHuabei = findViewById(R.id.rl_huabei);
        mViewLine = findViewById(R.id.view_line_huabei);
        mLlHbfqDetail = (LinearLayout) findViewById(R.id.ll_hbfq_detail);
        mCbHbfq = findViewById(R.id.cb_hbfq);
        mCbUnion = findViewById(R.id.cb_union);
        mViewUnion = findViewById(R.id.view_union);
        mViewHbfq = findViewById(R.id.view_hbfq);
        mLlMoney = findViewById(R.id.ll_money);
        mRlView = findViewById(R.id.rl_view);
        mBtnPay = findViewById(R.id.btn_confirm);
        /*mRlSix = (RelativeLayout) findViewById(R.id.rl_six);
        mTvSixMoney = (TextView) findViewById(R.id.tv_six_money);
        mTvSixRate = (TextView) findViewById(R.id.tv_six_rate);
        mRlTwelve = (RelativeLayout) findViewById(R.id.rl_twelve);
        mTvTwelveMoney = (TextView) findViewById(R.id.tv_twelve_money);
        mTvTwelveRate = (TextView) findViewById(R.id.tv_twelve_rate);*/
        mLlKeyboardView = findViewById(R.id.keyboard_view);
        mRecyclerViewRate = findViewById(R.id.recyclerView_huabei_rate);

        mViewLine.setVisibility(MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ) ? View.VISIBLE : View.GONE);
        mRlHuabei.setVisibility(MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ) ? View.VISIBLE : View.GONE);
        enable = false;
        setPayButton(enable);

        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.WJY)) {
            mSvHbfq.setVisibility(View.GONE);
        }
    }

    private void initListener() {
        ivBack.setOnClickListener(this);
        num_0.setOnClickListener(this);
        num_1.setOnClickListener(this);
        num_2.setOnClickListener(this);
        num_3.setOnClickListener(this);
        num_4.setOnClickListener(this);
        num_5.setOnClickListener(this);
        num_6.setOnClickListener(this);
        num_7.setOnClickListener(this);
        num_8.setOnClickListener(this);
        num_9.setOnClickListener(this);
        lay_point.setOnClickListener(this);
        lay_add.setOnClickListener(this);
        lay_clear.setOnClickListener(this);
        lay_delete.setOnClickListener(this);
        scan_pay.setOnClickListener(this);
        if (!MyApplication.getIsCasher()) {
            tv_choice_store.setOnClickListener(this);
            mIvStoreName.setVisibility(View.VISIBLE);
        } else {
            mIvStoreName.setVisibility(View.INVISIBLE);
        }
        mTvRemark.setOnClickListener(this);
        mViewUnion.setOnClickListener(this);
        mViewHbfq.setOnClickListener(this);
        mLlMoney.setOnClickListener(this);
        mBtnPay.setOnClickListener(this);
    }

    private void initData() {
        calculateBuilder = new StringBuilder();
        store_Id = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
        if (!TextUtils.isEmpty(store_Id)) {
            getStoreVoice(store_Id);
        }
        mRateData = new ArrayList<>();
        setRecyclerView();
    }

    private void getHbfqRateList(double money) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            String moneyString = String.valueOf(BigDecimal.valueOf(money).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            Map<String, Object> map = new HashMap<>();
            map.put("mchId", MyApplication.getMechantId());
            map.put("amount", moneyString);
            ServerClient.newInstance(PayActivity.this).hbfqRateList(PayActivity.this, Constants.TAG_HBFQ_RATE_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String storeName = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME);
        if (!TextUtils.isEmpty(storeName)) {
            if (storeName.length() > 10) {
                mTv_store_name.setText(storeName.substring(0, 9) + "...");
            } else {
                mTv_store_name.setText(storeName);
            }
        }
        store_Id = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
        if (TextUtils.isEmpty(store_Id)) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(PayActivity.this).storePort(PayActivity.this, Constants.STORE_PORT_TAG, null);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PUT_STORE)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.PUT_STORE_FALSE:
                    if (msg != null && msg.getError() != null) {
                        showCommonNoticeDialog(PayActivity.this, msg.getError().getMessage());
                    }
                    break;
                case Constants.PUT_STORE_TRUE:
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_HBFQ_RATE_LIST)) {
            dismissLoading();
            HbfqRateBean msg = (HbfqRateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PayActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mRateData.clear();
                        mRateData.addAll(msg.getData());
                        if (msg.getData().size() > 0) {
                            /*mHbFqNum = mRateData.get(0).getHbfqNum();
                            isRateFree = mRateData.get(0).isFreeFee();*/
                            setData(mRateData);
//                            isRateFree = isRateFree(mRateData);
                            setRateInfo(mRateData);
                            mLlHbfqDetail.setVisibility(View.VISIBLE);
                            setRecyclerView();
                        } else {
                            mLlHbfqDetail.setVisibility(View.GONE);
                        }
                    } else {
                        mLlHbfqDetail.setVisibility(View.GONE);
                        showCommonNoticeDialog(PayActivity.this, getString(R.string.error_data));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.GETUI_PUSH_VOICE_LIST_TAG)) {
            VoiceBean msg = (VoiceBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PayActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (msg.getData().get(0).getPushClose() == 1) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, true);
                        } else {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, false);
                        }
                        LogUtil.d("SP_STORE_VOICE---" + MyApplication.isOpenVoice());
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.STORE_PORT_TAG)) {
            StoreBean bean = (StoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(PayActivity.this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PayActivity.this, bean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (bean.getData().getStoreName() != null) {
                                String storeName = bean.getData().getStoreName();
                                if (!TextUtils.isEmpty(storeName)) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                                    mTv_store_name.setVisibility(View.VISIBLE);
                                    if (storeName.length() > 10) {
                                        mTv_store_name.setText(storeName.substring(0, 9) + "...");
                                    } else {
                                        mTv_store_name.setText(storeName);
                                    }
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, "");
                                    mTv_store_name.setVisibility(View.INVISIBLE);
                                }
                            }
                            if (!TextUtils.isEmpty(bean.getData().getStoreId())) {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, bean.getData().getStoreId());
                                store_Id = bean.getData().getStoreId();
                                getStoreVoice(store_Id);
                            } else {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, true);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                                store_Id = "";
                            }
                        }
                    }
                    break;
            }
            dismissLoading();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_remark:
                Intent intent = new Intent(PayActivity.this, RemarkActivity.class);
                intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
                startActivityForResult(intent, Constants.REQUEST_PAY_REMARK);
                break;
            case R.id.num_0:
                AppHelper.execVibrator(context);
                calculateProcess(0);
                break;
            case R.id.num_1:
                AppHelper.execVibrator(context);
                calculateProcess(1);
                break;
            case R.id.num_2:
                AppHelper.execVibrator(context);
                calculateProcess(2);
                break;
            case R.id.num_3:
                AppHelper.execVibrator(context);
                calculateProcess(3);
                break;
            case R.id.num_4:
                AppHelper.execVibrator(context);
                calculateProcess(4);
                break;
            case R.id.num_5:
                AppHelper.execVibrator(context);
                calculateProcess(5);
                break;
            case R.id.num_6:
                AppHelper.execVibrator(context);
                calculateProcess(6);
                break;
            case R.id.num_7:
                AppHelper.execVibrator(context);
                calculateProcess(7);
                break;
            case R.id.num_8:
                AppHelper.execVibrator(context);
                calculateProcess(8);
                break;
            case R.id.num_9:
                AppHelper.execVibrator(context);
                calculateProcess(9);
                break;
            //加号
            case R.id.lay_add:
                AppHelper.execVibrator(context);
                calculateProcess("+");
                break;
            //全删
            case R.id.lay_clear:
                AppHelper.execVibrator(context);
                if (calculateBuilder.toString().contains("+")) {
                    showDialog();
                } else {
                    clearData();
                }
                break;
            //点
            case R.id.lay_point:
                AppHelper.execVibrator(context);
                calculateProcess(".");
                break;
            //删除
            case R.id.lay_delete:
                AppHelper.execVibrator(context);
                deleteData();
                break;
            //支付
            case R.id.ly_to_scan_pay:
            case R.id.btn_confirm:
                MtaUtils.mtaId(PayActivity.this, "B002");
                if (MyApplication.isStoreNull()) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                } else {
                    AppHelper.execVibrator(context);
                    toCaptureActivity();
                }
                break;
            //选择门店
            case R.id.tv_choice_store:
                MtaUtils.mtaId(PayActivity.this, "B003");
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_ID, store_Id);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.view_union:
                mCbUnion.setChecked(true);
                mCbHbfq.setChecked(false);
                mLlHbfqDetail.setVisibility(View.GONE);
                break;
            case R.id.view_hbfq:
                if (!enable) {
                    mCbHbfq.setChecked(false);
                    mCbUnion.setChecked(true);
                    MyToast.showToastShort(getString(R.string.et_hit_code_money));
                } else {
                    if (!mCbHbfq.isChecked()) {
                        mCbUnion.setChecked(false);
                        mCbHbfq.setChecked(true);
                        getHbfqRateList(totalMoney);
                        if (!firstRequest && mLlKeyboardView.getVisibility() == View.VISIBLE) {
                            setKeyboardView(false);
                        }
                        firstRequest = true;
                    }
                }
                break;
            case R.id.ll_money:
                setKeyboardView(mLlKeyboardView.getVisibility() == View.GONE);
                break;
        }
        LogUtil.d("number_calculate==" + calculateBuilder.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            String storeName = shopBean.getStoreName();
            String storeId = shopBean.getStoreId();
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, storeId);
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
            LogUtil.d("SP_DEFAULT_STORE_ID=" + storeId + ",,SP_DEFAULT_STORE_NAME=" + storeName);
            store_Id = storeId;
            if (storeName.length() > 10) {
                mTv_store_name.setText(storeName.substring(0, 9) + "...");
            } else {
                mTv_store_name.setText(storeName);
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeName", storeName);
                map.put("storeId", storeId);
                ServerClient.newInstance(MyApplication.getContext()).postStore(MyApplication.getContext(), Constants.TAG_PUT_STORE, map);
                getStoreVoice(store_Id);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PAY_REMARK) {
            mPayRemark = data.getStringExtra(Constants.RESULT_PAY_REMARK);
        }
    }

    //扫描二维码
    private void toCaptureActivity() {
        if (TextUtils.isEmpty(store_Id)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(context, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        if (mLlHbfqDetail.getVisibility() == View.VISIBLE) {
            intent.putExtra(Constants.INTENT_HBFQ, mHbFqNum);
            if (mUnionFeeFlag) {
                intent.putExtra(Constants.INTENT_UNION_USER_FEE, mUnionUserFee);
                intent.putExtra(Constants.INTENT_IS_RATE_FREE, true);
            } else {
                intent.putExtra(Constants.INTENT_IS_RATE_FREE, isRateFree);
            }
        }
        startActivity(intent);
    }

    private void clearData() {
        totalMoney = 0;
        calculateBuilder.delete(0, calculateBuilder.length());
        tv_money.setText(getString(R.string.tv_money_zero));
        tv_money.setTextColor(UIUtils.getColor(R.color.line_color));
        tv_calculate.setText(getString(R.string.tv_money_zero));
        tv_calculate.setTextColor(UIUtils.getColor(R.color.line_color));
        enable = false;
        setPayButton(enable);
        mLlHbfqDetail.setVisibility(View.GONE);
    }

    private void deleteData() {
        if (calculateBuilder.length() > 0) {
            calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            tv_calculate.setText(calculateBuilder.toString());
        }
        calculateResult();
    }

    private void calculateProcess(Object object) {
        if (object instanceof Integer) {
            if ((calculateBuilder.toString().lastIndexOf("0") == 0 && calculateBuilder.length() == 1) ||
                    (calculateBuilder.toString().lastIndexOf("0") == calculateBuilder.length() - 1
                            && calculateBuilder.toString().lastIndexOf("+") == calculateBuilder.length() - 2)) {
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            }
            if (calculateBuilder.toString().contains(".")) {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (calculateBuilder.length() >= 3)
                    if (pointIndex == calculateBuilder.length() - 3 &&
                            calculateBuilder.toString().lastIndexOf("+") != calculateBuilder.length() - 1) {
                        return;
                    }
            }
        }
        if (object instanceof String) {
            if (TextUtils.isEmpty(calculateBuilder)) {
                if (object.equals("+")) {
                    return;
                }
                if (object.equals(".")) {
                    calculateBuilder.append("0");
                }
            } else {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (pointIndex == calculateBuilder.length() - 1) {
                    if (object.equals("+")) {
                        calculateBuilder.append("0");
                    }
                }
                if (!calculateBuilder.toString().contains("+")) {
                    if (calculateBuilder.toString().contains(".") && object.equals(".")) {
                        return;
                    }
                } else {
                    int addIndex = calculateBuilder.toString().lastIndexOf("+");
                    if (addIndex == calculateBuilder.length() - 1) {
                        if (object.equals("+")) {
                            return;
                        }
                        if (object.equals(".")) {
                            calculateBuilder.append("0");
                        }
                    } else {
                        if (pointIndex > addIndex && object.equals(".")) {
                            return;
                        }
                    }
                }
            }
        }
        calculateBuilder.append(object);
        calculateResult();
        tv_calculate.setText(calculateBuilder.toString());
    }

    private void calculateResult() {
        if (TextUtils.isEmpty(calculateBuilder)) {
            tv_money.setText(getString(R.string.tv_money_zero));
            tv_calculate.setText(getString(R.string.tv_money_zero));
            tv_calculate.setTextColor(UIUtils.getColor(R.color.line_color));
            enable = false;
            setPayButton(enable);
        } else {
            BigDecimal bigSum = null;
            if (calculateBuilder.toString().contains("+")) {
                String[] numbers = calculateBuilder.toString().split("\\+");
                if (numbers.length == 1) {
                    LogUtil.d("number1==", numbers[0]);
                    bigSum = new BigDecimal(numbers[0]);
                } else {
                    bigSum = new BigDecimal(0);
                    if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                        numbers[numbers.length - 1] = numbers[numbers.length - 1] + "0";
                    }
                    for (int i = 0; i < numbers.length; i++) {
                        LogUtil.d("number2==", numbers[i] + "--" + i);
                        BigDecimal big = new BigDecimal(numbers[i]);
                        bigSum = bigSum.add(big);
                        LogUtil.d("number3==", bigSum.doubleValue() + "");
                    }
                }
            } else {
                String calculateString = calculateBuilder.toString();
                if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                    bigSum = new BigDecimal(calculateString + "0");
                } else {
                    bigSum = new BigDecimal(calculateString);
                }
            }
            if (bigSum.doubleValue() > 300000) {
                MyToast.showToast(UIUtils.getString(R.string.tv_pay_exceed_money), Toast.LENGTH_SHORT);
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                return;
            }
            if (bigSum.doubleValue() <= 0) {
                enable = false;
            } else {
                enable = true;
            }
            setPayButton(enable);
            totalMoney = bigSum.doubleValue();
            tv_money.setText(DateUtil.formatPaseMoneyUtil(bigSum));
//            setHbfqDetail(mRateData, totalMoney);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestHbfqDetail();
                }
            },500);
//            requestHbfqDetail();
        }
        tv_money.setTextColor(UIUtils.getColor(enable ? R.color.tv_pay_total_money : R.color.line_color));
        tv_calculate.setTextColor(TextUtils.isEmpty(calculateBuilder) ? UIUtils.getColor(R.color.line_color) : UIUtils.getColor(R.color.tv_pay_total_money));
    }

    private void setPayButton(boolean enable) {
        if (enable) {
            Drawable rectangleDefault = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            Drawable rectanglePressed = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectanglePressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
            scan_pay.setBackground(stateListDrawable);
            scan_pay.setEnabled(true);
        } else {
            Drawable rectangleUnable = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectangleUnable.setAlpha(102);
            scan_pay.setBackground(rectangleUnable);
            scan_pay.setEnabled(false);
        }
        if (!enable) {
//            mCbHbfq.setChecked(false);
            mLlHbfqDetail.setVisibility(View.GONE);
        }
    }

    public void showDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(PayActivity.this, getString(R.string.dialog_notice_clear), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    clearData();
                }
            });
        }
        mSelectDialog.show();
    }

    private void getStoreVoice(String storeId) {
        Map<String, Object> voiceMap = new HashMap<>();
        voiceMap.put("storeMerchantId", storeId);
        String clientid = PushManager.getInstance().getClientid(this.getApplicationContext());
        if (!StringUtils.isEmptyOrNull(clientid)) {
            voiceMap.put("pushDeviceId", clientid);
        }
        voiceMap.put("client", Constants.REQUEST_CLIENT_APP);
        ServerClient.newInstance(MyApplication.getContext()).getuiPushVoiceList(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_LIST_TAG, voiceMap);
    }

    private void setHbfqDetail(List<HbfqRateBean.DataBean> listRate, double money) {
        if (listRate != null && listRate.size() > 0) {
            for (int i = 0; i < listRate.size(); i++) {
                HbfqRateBean.DataBean dataBean = listRate.get(i);
                BigDecimal eachPrin = new BigDecimal(DateUtil.formatMoneyInt(money)).divide(new BigDecimal(dataBean.getHbfqNum()), BigDecimal.ROUND_DOWN);
                BigDecimal totalFeeInDecimal = new BigDecimal(DateUtil.formatMoneyInt(money)).multiply(new BigDecimal(dataBean.getRate()));
                long totalFeeInLong = totalFeeInDecimal.setScale(0, BigDecimal.ROUND_HALF_EVEN).longValue();
                BigDecimal eachFee = BigDecimal.valueOf(totalFeeInLong).divide(new BigDecimal(dataBean.getHbfqNum()), BigDecimal.ROUND_DOWN);
                BigDecimal prinAndFee = eachFee.add(eachPrin);
                /*if ("6".equals(dataBean.getHbfqNum())) {
                    mTvSixRate.setText("手续费" + getString(R.string.tx_mark) + eachFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "/期");
                    mTvSixMoney.setText(getString(R.string.tx_mark) + prinAndFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "x" + dataBean.getHbfqNum() + "期");
                }
                if ("12".equals(dataBean.getHbfqNum())) {
                    mTvTwelveRate.setText("手续费" + getString(R.string.tx_mark) + eachFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "/期");
                    mTvTwelveMoney.setText(getString(R.string.tx_mark) + prinAndFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "x" + dataBean.getHbfqNum() + "期");
                }*/
            }
        }
    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerViewRate.setLayoutManager(linearLayoutManager);
        mRecyclerViewRate.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new HuabeiRateAdapter(MyApplication.getContext(), mRateData, mHbFqNum);
        mRecyclerViewRate.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new HuabeiRateAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mHbFqNum = mRateData.get(position).getHbfqNum();
                isRateFree = mRateData.get(position).isFreeFee();
                mUnionFeeFlag = mRateData.get(position).getUnionFeeFlag();
                mUnionUserFee = mRateData.get(position).getUnionUserFee();
                setRecyclerView();
            }
        });
    }

    private void setKeyboardView(final boolean show) {
        Animation rotate;
        if (show) {
            rotate = AnimationUtils.loadAnimation(this, R.anim.bottom_dialog_enter);
        } else {
            rotate = AnimationUtils.loadAnimation(this, R.anim.bottom_dialog_exit);
        }
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(false);
        mLlKeyboardView.startAnimation(rotate);
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (show) {
                    mLlKeyboardView.setVisibility(View.VISIBLE);
                    mBtnPay.setVisibility(View.GONE);
                } else {
                    mLlKeyboardView.setVisibility(View.GONE);
                    mBtnPay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void requestHbfqDetail() {
        if (mCbHbfq.isChecked()) {
            if (System.currentTimeMillis() - lastInputTimeMillis > 500) {
                getHbfqRateList(totalMoney);
            }
            lastInputTimeMillis = System.currentTimeMillis();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLlKeyboardView.clearAnimation();
    }

    private void setData(List<HbfqRateBean.DataBean> rateData) {
        int hbfqNum = 0;
        if (rateData != null && rateData.size() > 0) {
            for (int i = 0; i < rateData.size(); i++) {
                if (mHbFqNum.equals(rateData.get(i).getHbfqNum())) {
                    return;
                }
                if (i == 0) {
                    hbfqNum = Integer.valueOf(rateData.get(i).getHbfqNum());
                }
                if (rateData.get(i).isFreeFee() && (hbfqNum < Integer.valueOf(rateData.get(i).getHbfqNum()))) {
                    hbfqNum = Integer.valueOf(rateData.get(i).getHbfqNum());
                }
            }
        }
        mHbFqNum = String.valueOf(hbfqNum);
    }

    private void setRateInfo(List<HbfqRateBean.DataBean> rateData) {
        if (rateData != null && rateData.size() > 0) {
            for (int i = 0; i < rateData.size(); i++) {
                if (mHbFqNum.equals(rateData.get(i).getHbfqNum())) {
                    isRateFree = rateData.get(i).isFreeFee();
                    mUnionFeeFlag = rateData.get(i).getUnionFeeFlag();
                    mUnionUserFee = rateData.get(i).getUnionUserFee();
                    return;
                }
            }
        }
        isRateFree = false;
        mUnionFeeFlag = false;
        mUnionUserFee = 0;
    }
}
