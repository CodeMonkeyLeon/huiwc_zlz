package com.hstypay.enterprise.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chinaums.mis.bean.ResponsePojo;
import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.facepay.UnionPayFacePayUtils;
import com.hstypay.enterprise.activity.vanke.CouponVerifyActivity;
import com.hstypay.enterprise.adapter.CouponAdapter;
import com.hstypay.enterprise.adapter.RefundRecordAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.EasypayInfo;
import com.hstypay.enterprise.bean.FuxunBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.RefundBean;
import com.hstypay.enterprise.bean.ShareBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.bean.YinshangPosBean;
import com.hstypay.enterprise.bean.vanke.CouponReverseBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.CreateOneDiCodeUtil;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.ums.anypay.service.IOnTransEndListener;
import com.ums.upos.sdk.system.BaseSystemManager;
import com.ums.upos.sdk.system.OnServiceStatusListener;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * 交易详情
 * Created by admin on 2017/7/3.
 */
public class PayDetailActivity extends BaseActivity {
    private ImageView mIvBack, mIvTradeBarCode, mIvCouponArrow;
    private Button mBtnPrint, mButton;
    private ScrollView mSvContent;
    private TextView mTvTitle, mTvTradeMoney, mTvRealMoney, mTvCouponMoney, mTvCashier, mTvTradeTime, mTvTradeWay, mTvTradeWayCode, mTvPayRemark, mTvTradeState, mTvFreeRechargeCoupon;
    private TextView mTvTitlePayRemark, mTvThirdCode, mTvBountyMoney, mTvRefundNotice, mTvTradeCodeNumber, mTvTradeCode, mTrayTitleInfo, mTvPayStore, mTvUnionPayMoney, mTvSite;
    private LinearLayout mLlPayRemark, mLlCoupon, mLlBounty, mLlRefundRecord, mLlUnionPayMoney, mLlFreeRechargeCoupon, mLlSite;
    private RecyclerView mRvRefundRecord, mRvCoupon;
    private LinearLayout mTvCashierTitle, mLlTradeWayTitle;
    private String mOrderNo;
    private SafeDialog mLoadDialog, mCancelLoadDialog;
    private String mStoreMerchantId;
    private String mIntentName;

    private PayBean.DataBean mBilldata;
    private TradeDetailBean mTradeDetail;
    public static PayDetailActivity instance = null;
    private boolean mSwitch;
    private SelectDialog mSelectDialog;
    private boolean retry = true;
    private Intent mResultData;

    private Printer printer;
    private PosPrintUtil mPosPrintUtil;
    private IWoyouService woyouService;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;
    private AidlPrinter printerDev = null;
    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private String otherType;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private FuxunBean mFuxunBean;
    private boolean notNeedReverse;


    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service

    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    /**
     * 绑定商米打印服务
     */
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        /*if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindCUPService();
        }*/
        initView();
        initData();
        initListener();
        initPosPrint();

    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }/* else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService();
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initListener() {
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        cancelCoupon();
                        break;
                    case R.id.btn_refund:
                        //退款
                        MtaUtils.mtaId(PayDetailActivity.this, "C005");
                        if (MyApplication.getIsBounty()) {
                            bountyRefundEnable();
                        } else {
                            refund(mBilldata);
                        }
                        break;
                    case R.id.ll_coupon:
                        Animation rotate;
                        if (mSwitch) {
                            rotate = AnimationUtils.loadAnimation(MyApplication.getContext(), R.anim.rotate_reverse);
                            mSwitch = false;
                            mRvCoupon.setVisibility(View.GONE);
                        } else {
                            rotate = AnimationUtils.loadAnimation(MyApplication.getContext(), R.anim.rotate);
                            mSwitch = true;
                            mRvCoupon.setVisibility(View.VISIBLE);
                        }
                        rotate.setInterpolator(new LinearInterpolator());
                        rotate.setFillAfter(true);
                        mIvCouponArrow.startAnimation(rotate);
                        break;
                    default:
                        break;
                }
            }
        };
        mIvBack.setOnClickListener(onSingleClickListener);
        mButton.setOnClickListener(onSingleClickListener);
        mBtnPrint.setOnClickListener(new OnSingleClickListener(2000) {
            @Override
            public void onSingleClick(View view) {
                //判断是打印还是查询
                getPrint();
            }
        });
        mLlCoupon.setOnClickListener(onSingleClickListener);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mCancelLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_checking), true);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_pay_detail);

        mTvRealMoney = (TextView) findViewById(R.id.tv_real_money);//实收金额
        mTvTradeMoney = (TextView) findViewById(R.id.tx_receivable);//收款金额
        mTvCouponMoney = (TextView) findViewById(R.id.tx_coupon);//优惠金额
        mTvTradeTime = (TextView) findViewById(R.id.tv_order_time);//交易时间
        mTvTradeWay = (TextView) findViewById(R.id.tv_bank);//支付方式
        mTvTradeCode = (TextView) findViewById(R.id.tv_mch_transNo);//平台订单号
        mTvPayRemark = (TextView) findViewById(R.id.tv_pay_remark);//付款备注
        mTvTradeState = (TextView) findViewById(R.id.tv_trade_state);//交易状态
        mIvTradeBarCode = (ImageView) findViewById(R.id.iv_trade_barcode);//条形码
        mTvTradeCodeNumber = (TextView) findViewById(R.id.tv_trade_code_number);
        mTrayTitleInfo = (TextView) findViewById(R.id.tv_third_order_title);//第三方单号title
        mTvThirdCode = (TextView) findViewById(R.id.wx_tvOrderCode);//第三方单号
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mButton = (Button) findViewById(R.id.btn_refund);
        mTvTitlePayRemark = (TextView) findViewById(R.id.tv_title_pay_remark);//付款备注title
        //收银员title
        mTvCashierTitle = (LinearLayout) findViewById(R.id.ll_cashier);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);//收银员
        mTvPayStore = (TextView) findViewById(R.id.tv_pay_store);//收款门店
        mLlTradeWayTitle = (LinearLayout) findViewById(R.id.lr_wx);//
        mLlPayRemark = (LinearLayout) findViewById(R.id.ll_pay_remark);
        mLlCoupon = (LinearLayout) findViewById(R.id.ll_coupon);//优惠金额
        mIvCouponArrow = (ImageView) findViewById(R.id.iv_coupon_arrow);//优惠金额

        mLlBounty = (LinearLayout) findViewById(R.id.ll_bounty);//营销奖励
        mTvBountyMoney = (TextView) findViewById(R.id.tv_bounty);//营销奖励金额

        mLlFreeRechargeCoupon = (LinearLayout) findViewById(R.id.ll_free_recharge_coupon);//支付宝免充值优惠金额
        mTvFreeRechargeCoupon = (TextView) findViewById(R.id.tv_free_recharge_coupon);//支付宝免充值优惠金额

        mLlRefundRecord = (LinearLayout) findViewById(R.id.ll_refund_record);
        mRvRefundRecord = (RecyclerView) findViewById(R.id.rv_refund_record);
        mRvCoupon = (RecyclerView) findViewById(R.id.rv_coupon);
        mLlUnionPayMoney = findViewById(R.id.ll_union_pay_money);
        mTvUnionPayMoney = findViewById(R.id.tv_union_pay_money);
        mLlSite = findViewById(R.id.ll_site);
        mTvSite = findViewById(R.id.tv_site);

        mTvRefundNotice = (TextView) findViewById(R.id.tv_refund_notice);//退款提示
        setButtonWhite(mBtnPrint);
        setButtonEnable(mButton, true);
    }

    public void initData() {
        getPrintActive();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mBilldata = (PayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_BILL_DATA);
        mBilldata.setPay(true);
        otherType = getIntent().getStringExtra(Constants.INTENT_OTHER_TYPE);
        if (mBilldata != null) {
            //收款金额
            mOrderNo = mBilldata.getOrderNo();
            if (MyApplication.getIsBounty()) {
                mSvContent.setVisibility(View.GONE);
                getBountyMoney(mOrderNo);
            }
            mStoreMerchantId = mBilldata.getStoreMerchantId();
            setView(mBilldata);
            List<RefundBean> refundList = mBilldata.getRefundDTOList();
            if (refundList != null && refundList.size() > 0) {
                mLlRefundRecord.setVisibility(View.VISIBLE);
                initRecyclerView(refundList);
            } else {
                mLlRefundRecord.setVisibility(View.GONE);
            }

            if ("TAG_AUTO_ORDER_DETAIL".equals(mIntentName)) {
                if (mBilldata.getTradeState() == 1) {
                    //发送网络请求
                    SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
                    getCardPayResult();
                }
                Map<String, Object> map = new HashMap<>();
                map.put("orderNo", mBilldata.getOrderNo());
                ServerClient.newInstance(MyApplication.getContext()).bcardResultTrack(MyApplication.getContext(), "BCARD_RESULT_TRACK", map);
            }
        }
    }

    private void getCardPayResult() {
        if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    && mBilldata.getApiProvider() == 11) {
                if (!DateUtil.getDate().equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE))) {
                    wizardFukaSign();
                } else {
                    checkWizardFukaPay();
                }
            } else {
                checkEasyPayBcard();
            }
        } else if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (mBilldata.getApiProvider() == 13 || mBilldata.getApiProvider() == 15) {//13 银联刷脸 ；15 微信刷脸
                checkYSFacepayOrder();
            } else {
                checkYinshangOrder();
            }
        }
    }

    private void initRecyclerView(final List<RefundBean> refundList) {
        CustomLinearLayoutManager customLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvRefundRecord.setLayoutManager(customLinearLayoutManager);
        RefundRecordAdapter adapter = new RefundRecordAdapter(this, refundList);
        mRvRefundRecord.setAdapter(adapter);
        adapter.setOnItemClickListener(new RefundRecordAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                finish();
                Intent intent = new Intent(PayDetailActivity.this, RefundDetailActivity.class);
                mTradeDetail = getTradeDetail(mBilldata, refundList.get(position));
                intent.putExtra(Constants.INTENT_TRADE_DETAIL, mTradeDetail);
                intent.putExtra(Constants.INTENT_BILL_DATA, mBilldata);
                if (Constants.INTENT_CAPTURE_REFUND.equals(mIntentName)) {
                    intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_CAPTURE_REFUND);
                } else {
                    intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_TRADE_DETAIL);
                }
                startActivity(intent);
            }
        });
    }


    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void setView(PayBean.DataBean billData) {
        if (mBilldata.getReverseFlag() == 1) {
            mButton.setText(R.string.tv_reverse);
        } else {
            mButton.setText(R.string.tv_refund);
        }
        if (billData.getApiProvider() == 10 || billData.getApiProvider() == 11 || billData.getApiProvider() == 13) {
            if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                if (mBilldata.getReverseFlag() == 1) {
                    mTvRefundNotice.setText(R.string.tv_reverse_notice);
                } else {
                    mTvRefundNotice.setVisibility(View.GONE);
                }
            } else {
                mTvRefundNotice.setText(R.string.tv_refund_notice);
            }
        } else {
            mTvRefundNotice.setVisibility(View.GONE);
        }
        mTvRealMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(billData.getRealMoney() / 100d));
        mTvTradeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(billData.getMoney() / 100d));

        if (billData.getMchDiscountsMoney() > 0) {
            mLlCoupon.setVisibility(View.VISIBLE);
            mTvCouponMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyUtil(billData.getMchDiscountsMoney() / 100d));
            CustomLinearLayoutManager customLinearLayoutManager = new CustomLinearLayoutManager(this);
            mRvCoupon.setLayoutManager(customLinearLayoutManager);
            CouponAdapter adapter = new CouponAdapter(this, billData.getCouponInfoList());
            mRvCoupon.setAdapter(adapter);
        }

        /*if (billData.getMdiscount() >0){
            mLlFreeRechargeCoupon.setVisibility(View.VISIBLE);
            mTvFreeRechargeCoupon.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(billData.getMdiscount() / 100d));
        }*/

        if (billData.getUnionPayMoney() > 0) {
            mLlUnionPayMoney.setVisibility(View.VISIBLE);
            mTvUnionPayMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(billData.getUnionPayMoney() / 100d));
        }

        //交易时间
        if (!StringUtils.isEmptyOrNull(billData.getTradeTime())) {
            mTvTradeTime.setText(billData.getTradeTime());
        }
        mTvTradeWay.setText(OrderStringUtil.tradeTypeFromString(billData.getApiProvider(), billData.getApiCode()));
        mTrayTitleInfo.setText("交易单号");

        //交易单号
        if (!StringUtils.isEmptyOrNull(billData.getOrderNo())) {
            mTvTradeCode.setText(billData.getOrderNo());
        }
        //门店
        if (!StringUtils.isEmptyOrNull(billData.getStoreMerchantIdCnt())) {
            mTvPayStore.setText(billData.getStoreMerchantIdCnt());
        }
        //付款备注
        if (!StringUtils.isEmptyOrNull(billData.getAttach())) {
            mTvPayRemark.setText(billData.getAttach());
        } else {
            mLlPayRemark.setVisibility(View.GONE);
            mTvTitlePayRemark.setVisibility(View.GONE);
            mTvPayRemark.setVisibility(View.GONE);
        }
        //收银点
        if (!StringUtils.isEmptyOrNull(billData.getCashPointName())) {
            mLlSite.setVisibility(View.VISIBLE);
            mTvSite.setText(billData.getCashPointName());
        } else {
            mLlSite.setVisibility(View.GONE);
        }

        int tradeStatus = billData.getTradeState();
        if (tradeStatus == 1 || tradeStatus == 9) {
            //如果未支付隐藏第三支付单号
            if (tradeStatus == 1) {
                mTvTradeState.setText("未支付");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
                mBtnPrint.setText("获取收款结果");
                if (billData.getApiProvider() == 10 || billData.getApiProvider() == 11 || billData.getApiProvider() == 13 || billData.getApiProvider() == 15) {
                    if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        mBtnPrint.setVisibility(View.VISIBLE);
                    } else {
                        mBtnPrint.setVisibility(View.INVISIBLE);
                    }
                } else {
                    mBtnPrint.setVisibility(View.VISIBLE);
                }
            } else {
                mTvTradeState.setText("已撤销");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
                mBtnPrint.setVisibility(View.GONE);
                mLlTradeWayTitle.setVisibility(View.GONE);
            }
            mTvRealMoney.setTextColor(UIUtils.getColor(R.color.home_value_text));
        } else {
            mTvRealMoney.setTextColor(UIUtils.getColor(R.color.theme_color));
            if (tradeStatus == 2) {
                mBtnPrint.setText("打印小票");
//                mTvTradeState.setText("支付成功");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
            } else if (tradeStatus == 3) {
//                mTvTradeState.setText("已关闭");
                mBtnPrint.setVisibility(View.GONE);
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
            } else if (tradeStatus == 4) {
//                mTvTradeState.setText("转入退款");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
                mBtnPrint.setText("打印小票");
            } else if (tradeStatus == 8) {
//                mTvTradeState.setText("已冲正");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
                mBtnPrint.setVisibility(View.GONE);
                mLlTradeWayTitle.setVisibility(View.GONE);
            } else {
//                mTvTradeState.setText("未知");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
                mBtnPrint.setVisibility(View.GONE);
                mLlTradeWayTitle.setVisibility(View.GONE);
            }
            mTvTradeState.setText(OrderStringUtil.getTradeStateString(billData.getTradeState()));
        }
        if (!StringUtils.isEmptyOrNull(billData.getTransactionId())) {
            mTvThirdCode.setText(billData.getTransactionId());
            mLlTradeWayTitle.setVisibility(View.VISIBLE);
        } else {
            mLlTradeWayTitle.setVisibility(View.GONE);
        }
        if (!StringUtils.isEmptyOrNull(billData.getCashierName())) {
            mTvCashierTitle.setVisibility(View.VISIBLE);
            mTvCashier.setText(billData.getCashierName());
        } else {
            mTvCashierTitle.setVisibility(View.GONE);
        }

        if (billData.getTradeType() != 1 && (tradeStatus == 2 || tradeStatus == 4) && billData.getEditEnable() == 1) {
            if (MyApplication.getIsCasher()) {
                LogUtil.d("SpUtil=1="+MyApplication.getCashierRefund()+"--"+MyApplication.getPermissionsRefund());
                if (!StringUtils.isEmptyOrNull(MyApplication.getCashierRefund()) && !StringUtils.isEmptyOrNull(MyApplication.getPermissionsRefund())) {
                    if (billData.getApiProvider() == 10 || billData.getApiProvider() == 11 || billData.getApiProvider() == 13 || billData.getApiProvider() == 15) {
                        if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                            mButton.setVisibility(View.VISIBLE);
                        } else {
                            mButton.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        mButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    mButton.setVisibility(View.GONE);
                }
            } else {
                if (!StringUtils.isEmptyOrNull(MyApplication.getPermissionsRefund())) {
                    if (billData.getApiProvider() == 10 || billData.getApiProvider() == 11 || billData.getApiProvider() == 13 || billData.getApiProvider() == 15) {
                        if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                            mButton.setVisibility(View.VISIBLE);
                        } else {
                            mButton.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        mButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    mButton.setVisibility(View.GONE);
                }
            }
        } else {
            mButton.setVisibility(View.GONE);
        }

        //一维码
        if (!StringUtils.isEmptyOrNull(billData.getOrderNo())) {
            mIvTradeBarCode.setVisibility(View.VISIBLE);
            mTvTradeCodeNumber.setVisibility(View.VISIBLE);
            WindowManager wm = this.getWindowManager();
            int width = wm.getDefaultDisplay().getWidth();
            final int w = (int) (width * 0.85);
            mIvTradeBarCode.setImageBitmap(CreateOneDiCodeUtil.createCode(billData.getOrderNo(), w, 180));
            mTvTradeCodeNumber.setText(billData.getOrderNo());
        }
        if (Constants.INTENT_CAPTURE_REFUND.equals(mIntentName)) {
            mButton.setVisibility(View.GONE);
            mBtnPrint.setVisibility(View.GONE);
        }

        if (mBilldata.getReverseFlag() != 1) {
            //是退款
//            long canRefundMoney = billData.getPayMoney() - billData.getRefundMoney() - billData.getMdiscount();//可退款金额
//            if (canRefundMoney<=0){}
            if (billData.getRefundMoney() > 0 && billData.getMdiscount() > 0) {
                //使用代金券的订单全额退过款了就不能再退款了（使用微信代金券的订单退款时只支持全额退款）
                mButton.setVisibility(View.GONE);
            }
        }
    }

    /*@Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                cancelCoupon();
                break;
            case R.id.btn_refund:
                //退款
                MtaUtils.mtaId(PayDetailActivity.this, "C005");
                if (MyApplication.getIsBounty()) {
                    bountyRefundEnable();
                } else {
                    refund(mBilldata);
                }
                break;
            case R.id.blue_print:
                //判断是打印还是查询
                getPrint();
                break;
            case R.id.ll_coupon:
                Animation rotate;
                if (mSwitch) {
                    rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
                    mSwitch = false;
                    mRvCoupon.setVisibility(View.GONE);
                } else {
                    rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
                    mSwitch = true;
                    mRvCoupon.setVisibility(View.VISIBLE);
                }
                rotate.setInterpolator(new LinearInterpolator());
                rotate.setFillAfter(true);
                mIvCouponArrow.startAnimation(rotate);
                break;
            default:
                break;
        }
    }*/

    private void getPrint() {
        int tradeState = mBilldata.getTradeState();
        if (tradeState == 1) {
            //发送网络请求
            if (mBilldata.getApiProvider() == 10 || mBilldata.getApiProvider() == 11 || mBilldata.getApiProvider() == 13 || mBilldata.getApiProvider() == 15) {
                getCardPayResult();
            } else {
                checkOrder();
            }
        } else {
            MtaUtils.mtaId(PayDetailActivity.this, "C003");
            TradeDetailBean tradeDetail = getTradeDetail(mBilldata, null);
            tradeDetail.setPay(true);
            mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, tradeDetail, false);
        }
    }


    private void bountyRefundEnable() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!StringUtils.isEmptyOrNull(mOrderNo)) {
                DialogUtil.safeShowDialog(mLoadDialog);
                Map<String, Object> map = new HashMap<>();
                map.put("orderNo", mOrderNo);
                ServerClient.newInstance(PayDetailActivity.this).bountyRefundEnable(PayDetailActivity.this, Constants.TAG_BOUNTY_REFUND_ENABLE, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void checkOrder() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!StringUtils.isEmptyOrNull(mOrderNo)) {
                DialogUtil.safeShowDialog(mLoadDialog);
                Map<String, Object> map = new HashMap<>();
                map.put("orderNo", mOrderNo);
                map.put("storeMerchantId", mStoreMerchantId);
                ServerClient.newInstance(PayDetailActivity.this).checkOrderState(PayDetailActivity.this, Constants.TAG_SEARCH_ORDER_STATE, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 易生查单接口订单
     */
    private void checkEasyPayBcard() {
        unbindDeviceService();
        Map<String, String> map = new HashMap<>();
        map.put("option", "check");
        map.put("orderNo", mBilldata.getOrderNo());
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("net.worthtech.worthcasher", "net.worthtech.worthcasher.activity.PayService"));
        for (Map.Entry<String, String> arg : map.entrySet()) {
            intent.putExtra(arg.getKey(), arg.getValue());
        }
        startActivityForResult(intent, Constants.REQUEST_BCARD_CHECK);
    }

    /**
     * 慧银福卡查单接口订单
     */
    private void checkWizardFukaPay() {
        unbindDeviceService();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Map<String, Object> map = new HashMap<>();
                    map.put("AppID", Constants.WJYS_APPID);
                    map.put("AppName", "WJYS");
                    map.put("ItemID", "WJY02");
                    map.put("CustomerOID", mBilldata.getOrderNo());
                    map.put("trxId", mBilldata.getOrderNo());
                    map.put("TransType", 152);//查单、重打印
                    map.put("TransAmount", StringUtils.formatFukaMoney(mBilldata.getMoney() + ""));
                    if (!TextUtils.isEmpty(mBilldata.getOrderNo())) {
                        if (mBilldata.getOrderNo().length() > 16) {
                            map.put("TransIndexCode", mBilldata.getOrderNo().substring(mBilldata.getOrderNo().length() - 16));
                        } else {
                            map.put("TransIndexCode", mBilldata.getOrderNo());
                        }
                    }
                    map.put("ReqTransDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                    map.put("ReqTransTime", DateUtil.formatDateToHHmmss(DateUtil.formatTime(System.currentTimeMillis())));
                    map.put("NoPrintReceipt", 1);
                    LogUtil.d("Jeremy-pay-check==" + new Gson().toJson(map));
                    String transact = MainActivity.instance.getCupService().transact(new Gson().toJson(map));
                    LogUtil.d("Jeremy-pay==" + transact);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mFuxunBean = new Gson().fromJson(transact, FuxunBean.class);
                            if (mFuxunBean != null) {
                                if ("00".equals(mFuxunBean.getRespCode()) && mBilldata.getOrderNo().equals(mFuxunBean.getTrxID())) {
                                    if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                        MyToast.showToastShort(getString(R.string.network_exception));
                                    } else {
                                        payFukaNotify(mFuxunBean);
                                    }
                                } else {
                                    String resultDesc = "交易失败";
                                    if (!TextUtils.isEmpty(mFuxunBean.getRespDesc()))
                                        resultDesc = mFuxunBean.getRespDesc();
                                    showCommonNoticeDialog(PayDetailActivity.this, resultDesc);
                                }
                            } else {
                                showCommonNoticeDialog(PayDetailActivity.this, "回调数据有误！");
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showCommonNoticeDialog(PayDetailActivity.this, "回调数据有误！" + e.getMessage());
                        }
                    });
                }
            }
        }).start();

    }

    /**
     * 福卡签到
     */
    private void wizardFukaSign() {
        Map<String, Object> map = new HashMap<>();
        map.put("AppID", Constants.WJYS_APPID);
        map.put("AppName", "WJYS");
        map.put("OptCode", "01");
        map.put("OptPass", "0000");
        try {
            String login = MainActivity.instance.getCupService().login(new Gson().toJson(map));
            LogUtil.d("Jeremy—login==" + login);
            FuxunBean fuxunBean = new Gson().fromJson(login, FuxunBean.class);
            if (fuxunBean != null) {
                if ("00".equals(fuxunBean.getRespCode())) {//签到成功
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE, DateUtil.getDate());
                    checkWizardFukaPay();
                } else {
                    showCommonNoticeDialog(this, TextUtils.isEmpty(fuxunBean.getRespDesc()) ? "签到失败！" : fuxunBean.getRespDesc());
                }
            } else {
                showCommonNoticeDialog(this, "签到失败！");
            }
        } catch (Exception e) {
            showCommonNoticeDialog(this, "签到失败！");
            e.printStackTrace();
        }
    }

    /**
     * 易生刷卡补打小票
     *
     * @param appMetaData
     * @param orderModel
     */
    /**
     * 银商pos刷卡支付
     */
    private void checkYinshangOrder() {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "公共资源";
        String transAppId = "交易明细";
        JSONObject transData = new JSONObject();
        try {
            transData.put("appId", APPID);//appId
            transData.put("isNeedPrintReceipt", true);//交易结束后自动打单
            transData.put("isShowDetailPage", false);//不显示详情
//            transData.put("traceNo", mBilldata.getVoucherNo());
            transData.put("extOrderNo", mBilldata.getOrderNo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
                YinshangPosBean yinshangPosBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                /*if (yinshangPosBean != null && yinshangPosBean.getTransData() != null && "00".equals(yinshangPosBean.getTransData().getResCode())) {
                    payYinshangNotify(yinshangPosBean);
                } else {
                    showCommonNoticeDialog(PayDetailActivity.this, "该订单未支付");
                }*/
                if (yinshangPosBean != null) {
                    if ("0".equals(yinshangPosBean.getResultCode())) {
                        if (yinshangPosBean.getTransData() != null && "00".equals(yinshangPosBean.getTransData().getResCode())) {
                            payYinshangNotify(yinshangPosBean);
                        } else {
                            showCommonNoticeDialog(PayDetailActivity.this, yinshangPosBean.getTransData().getResDesc());
                        }
                    } else {
                        showCommonNoticeDialog(PayDetailActivity.this, yinshangPosBean.getResultMsg());
                    }
                } else {
                    showCommonNoticeDialog(PayDetailActivity.this, "回调数据有误！");
                }
            }
        };
        com.ums.AppHelper.callTrans(PayDetailActivity.this, transAppName, transAppId, transData, listener);
    }

    /**
     * 银商pos人脸支付查单
     */
    private void checkYSFacepayOrder() {
        DialogUtil.safeShowDialog(mCancelLoadDialog);
        UnionPayFacePayUtils.Builder builder = new UnionPayFacePayUtils.Builder(43, "1", mBilldata.getOpUserId(), mBilldata.getReqOrderNo());
        UnionPayFacePayUtils.bankDAO(builder, this, responsePojo -> {
            runOnUiThread(() -> DialogUtil.safeCloseDialog(mCancelLoadDialog));
            LogUtil.d("Jeremy--responsePojo====" + new Gson().toJson(responsePojo));
            if (responsePojo != null && responsePojo.getRspCode().equals("00")) {
                payYSFaceNotify(responsePojo);
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showCommonNoticeDialog(PayDetailActivity.this, responsePojo.getRspChin());
                    }
                });
            }
        });
    }

    /*支付成功通知*/
    private void payYSFaceNotify(ResponsePojo response) {
        if (!NetworkUtils.isNetworkAvailable(PayDetailActivity.this)) {
            showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("timeEnd", setTradeTime(Calendar.getInstance().get(Calendar.YEAR) + response.getTradeDate() + response.getTradeTime()));
            map.put("transactionId", mBilldata.getOrderNo());
            map.put("outTradeNo", mBilldata.getOutTradeNo());
            map.put("totalFee", new BigDecimal(mBilldata.getMoney()));
            map.put("storeMerchantId", mBilldata.getStoreMerchantId());
            map.put("payType", response.getBankName());
            map.put("voucherNo", response.getTraceNo());
            map.put("batchId", response.getBatchNo());
            map.put("refNo", response.getRefNo());
            String[] transMemo = response.getTransMemo().split("&");
            if (transMemo.length > 2) {
                map.put("thirdOrderNo", transMemo[1]);
            }
            if (transMemo.length > 7) {
                map.put("cardType", transMemo[7]);
            }
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), "DETAIL_NOTIFY_PAY_EASYPAY", map);
        }
    }

    private String setTradeTime(String stringResultDate) {
        SimpleDateFormat sdfCreateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfResultHour = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date createTime = sdfCreateTime.parse(mBilldata.getTradeTime());
            Date resultDate = sdfResultHour.parse(stringResultDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(createTime);
            int createTimeHour = cal.get(Calendar.HOUR_OF_DAY);
            cal.setTime(resultDate);
            int resultDateHour = cal.get(Calendar.HOUR_OF_DAY);
            LogUtil.d("Jeremy---" + createTimeHour + "---" + resultDateHour);
            if (Math.abs(resultDateHour - createTimeHour) > 2) {
                cal.set(Calendar.HOUR_OF_DAY, (resultDateHour + 12) % 24);
            }
            Date time = cal.getTime();
            return sdfResultHour.format(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stringResultDate;
    }

    private void refund(PayBean.DataBean billData) {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_ORDER_REFUND, billData);
        intent.setClass(PayDetailActivity.this, RefundActivity.class);
        startActivity(intent);
    }

    /**
     * 银商刷卡支付通知
     *
     * @param bean
     */
    private void payYinshangNotify(YinshangPosBean bean) {
        if (!NetworkUtils.isNetworkAvailable(PayDetailActivity.this)) {
            showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", bean.getTransData().getExtOrderNo());
            if (!TextUtils.isEmpty(bean.getTransData().getAmt())) {
                map.put("totalFee", new BigDecimal(bean.getTransData().getAmt()).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            }
            map.put("cardType", bean.getTransData().getCardType());
            if (mBilldata != null) {
                map.put("storeMerchantId", mBilldata.getStoreMerchantId());
            }
            map.put("payType", bean.getTransData().getCardType());
            map.put("timeEnd", DateUtil.formartTradeTime(bean.getTransData().getDate() + " " + bean.getTransData().getTime(), "yyyy/MM/dd HH:mm:ss"));
            map.put("voucherNo", bean.getTransData().getTraceNo());
            map.put("batchId", bean.getTransData().getBatchNo());
            map.put("refNo", bean.getTransData().getRefNo());
            map.put("payCardId", bean.getTransData().getCardNo());
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), "DETAIL_NOTIFY_PAY_EASYPAY", map);
        }
    }

    //Eventbus接收数据订单状态
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals("DETAIL_NOTIFY_PAY_EASYPAY")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PayDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PayDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    checkOrder();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_SEARCH_ORDER_STATE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.CHECK_ORDER_STATE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PayDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PayDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CHECK_ORDER_STATE_TRUE:
                    PayBean.DataBean dataBean = msg.getData();
                    if (dataBean != null) {
                        if (MyApplication.getIsBounty()) {
                            getBountyMoney(mOrderNo);
                        }
                        mBilldata.setTradeFinishTime(dataBean.getTradeFinishTime());
                        mBilldata.setTradeState(dataBean.getTradeState());
                        mBilldata.setTradeStateName(dataBean.getTradeStateName());
                        mBilldata.setMchId(dataBean.getMchId());
                        mBilldata.setStoreMerchantId(dataBean.getStoreMerchantId());
                        mBilldata.setStoreMerchantIdCnt(dataBean.getStoreMerchantIdCnt());

                        if (!TextUtils.isEmpty(dataBean.getThirdOrderNo()))
                            mBilldata.setThirdOrderNo(dataBean.getThirdOrderNo());
                        if (!TextUtils.isEmpty(dataBean.getOperNo()))
                            mBilldata.setOperNo(dataBean.getOperNo());
                        if (!TextUtils.isEmpty(dataBean.getReqOrderNo()))
                            mBilldata.setReqOrderNo(dataBean.getReqOrderNo());
                        if (!TextUtils.isEmpty(dataBean.getVoucherNo()))
                            mBilldata.setVoucherNo(dataBean.getVoucherNo());
                        if (!TextUtils.isEmpty(dataBean.getApiCode()))
                            mBilldata.setApiCode(dataBean.getApiCode());
                        if (dataBean.getApiProvider() != 0)
                            mBilldata.setApiProvider(dataBean.getApiProvider());
                        if (!TextUtils.isEmpty(dataBean.getApiProviderName()))
                            mBilldata.setApiProviderName(dataBean.getApiProviderName());

                        int tradeState = dataBean.getTradeState();
                        if (tradeState == 1) {
                            showCommonNoticeDialog(PayDetailActivity.this, "该订单未支付");
                        } else if (tradeState == 3) {
                            showCommonNoticeDialog(PayDetailActivity.this, "该订单已关闭");
                        } else if (tradeState == 4) {
                            ToastHelper.showInfo(PayDetailActivity.this, "该订单已转入退款");
                        } /*else if (tradeState == 8) {
                            ToastHelper.showInfo(PayDetailActivity.this, "该订单已冲正");
                        } */ else if (tradeState == 8 || tradeState == 9) {
                            showCommonNoticeDialog(PayDetailActivity.this, "该订单已撤销");
                        } else if (tradeState == 8) {
                            showCommonNoticeDialog(PayDetailActivity.this, "该订单已冲正");
                        } else if (tradeState == 9) {
                            showCommonNoticeDialog(PayDetailActivity.this, "该订单已撤销");
                        } else if (tradeState == 2) {
                            showCommonNoticeDialog(PayDetailActivity.this, "该订单支付成功");
                            //交易时间
                            if (!StringUtils.isEmptyOrNull(dataBean.getTradeFinishTime())) {
                                mBilldata.setTradeTime(dataBean.getTradeFinishTime());
                            }
                            if (!StringUtils.isEmptyOrNull(dataBean.getOutTransactionId())) {
                                mBilldata.setTransactionId(dataBean.getOutTransactionId());
                            }
                        } else {
                            showWindowReverse();
                        }
                        setView(mBilldata);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_BOUNTY_REFUND_ENABLE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ShareBean info = (ShareBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PayDetailActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PayDetailActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        if (info.getData().isRefundEnable()) {
                            refund(mBilldata);
                        } else {
                            showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.tv_bounty_refund));
                        }
                    } else {
                        showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.error_data));
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PayDetailActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PayDetailActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                        LogUtil.e("print------1---" + info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_BOUNTY_MONEY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            mSvContent.setVisibility(View.VISIBLE);
            ShareBean info = (ShareBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PayDetailActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PayDetailActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null && info.getData().getRewardMoney() > 0) {
                        mBilldata.setRewardMoney(info.getData().getRewardMoney());
                        mLlBounty.setVisibility(View.VISIBLE);
                        mTvBountyMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(info.getData().getRewardMoney() / 100d));
                    }
                    break;
            }
        }
        /*if (event.getTag().equals(Constants.NOTIFY_PAY_EASYPAY_CHECK)) {
            checkOrder();
        }*/
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void showWindowReverse() {
        dismissLoading();
        mSelectDialog = new SelectDialog(PayDetailActivity.this, getString(R.string.tv_revers_prompt)
                , getString(R.string.revers_tx), getString(R.string.query_tx), R.layout.select_common_dialog);
        mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                checkOrder();
            }
        });
        mSelectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                if (!NetworkUtils.isNetworkAvailable(PayDetailActivity.this)) {
                    showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.network_exception));
                } else {
                    showNewLoading(true, getString(R.string.public_order_reverse));
                    Map<String, Object> map = new HashMap<>();
                    if (mBilldata != null) {
                        map.put("outTradeNo", mBilldata.getOutTradeNo());
                        map.put("storeMerchantId", mBilldata.getStoreMerchantId());
                        map.put("deviceVersion", AppHelper.getVerCode(MyApplication.getContext()));
                        /*if (AppHelper.getAppType() == 1) {
                            map.put("deviceInfo", Constants.PAY_CLIENT_POS);
                        } else if (AppHelper.getAppType() == 2) {
                            map.put("deviceInfo", Constants.PAY_CLIENT_APP);
                        }*/
                        map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
                        ServerClient.newInstance(PayDetailActivity.this).orderReverse(PayDetailActivity.this, Constants.ORDER_DETAIL_REVERSE_TAG, map);
                    }
                }
            }
        });
        DialogHelper.resize(PayDetailActivity.this, mSelectDialog);
        mSelectDialog.show();
    }

    //Eventbus接收数据，订单详情冲正返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderReverse(NoticeEvent event) {
        if (event.getTag().equals(Constants.ORDER_DETAIL_REVERSE_TAG)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ORDER_REVERSE_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PayDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PayDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ORDER_REVERSE_TRUE:
                    dismissLoading();
                    //冲正成功
                    showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.order_cz_success));
                    break;
            }
        } else if (event.getTag().equals("TAG_DETAIL_COUPON_CANCEL")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponReverseBean msg = (CouponReverseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    CouponReverseBean.CouponReverseData couponReverseData = msg.getData();
                    if (couponReverseData != null) {
                        if (couponReverseData.getReverseResult() == 0) {
                            toCouponVerify(Constants.INTENT_COUPON_CANCEL_SUCCESS, "");
                        } else {
                            if (couponReverseData.isNeedReverse()) {
                                notNeedReverse = false;
                                toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, "");
                            } else {
                                notNeedReverse = true;
                                toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, couponReverseData.getReverseMessage());
                            }
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 跳转到撤销页面
     *
     * @param intentName
     * @param errorString
     */
    private void toCouponVerify(String intentName, String errorString) {
        Intent intent = new Intent(PayDetailActivity.this, CouponVerifyActivity.class);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        intent.putExtra(Constants.INTENT_OUT_TRADE_NO, getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO));
        intent.putExtra(Constants.INTENT_USED_COUPON, getIntent().getBooleanExtra(Constants.INTENT_USED_COUPON, false));
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID));
        intent.putExtra(Constants.INTENT_NOT_NEED_REVERSE_COUPON, notNeedReverse);
        if (!TextUtils.isEmpty(errorString)) {
            intent.putExtra(Constants.INTENT_COUPON_CANCEL_FAILED, errorString);
        }
        startActivity(intent);
    }

    private void getBountyMoney(String orderNo) {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap();
            map.put("orderNo", orderNo);
            ServerClient.newInstance(MyApplication.getContext()).bountyMoney(MyApplication.getContext(), Constants.TAG_GET_BOUNTY_MONEY, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onBackPressed() {
        cancelCoupon();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            LogUtil.e("REQUEST_BCARD_PAY", URLDecoder.decode(data.toURI(), "UTF-8"));
        } catch (Exception e) {

        }
        if (requestCode == Constants.REQUEST_BCARD_PAY) {
            if (data != null) {
                String message = data.getStringExtra("message");
                String responseCode = data.getStringExtra("responseCode");
                if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                    showCommonNoticeDialog(PayDetailActivity.this, message);
                }
            }
        }
        if (requestCode == Constants.REQUEST_BCARD_PRINT) {
            if (data != null) {
                String message = data.getStringExtra("message");
                String responseCode = data.getStringExtra("responseCode");
                if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                    showCommonNoticeDialog(PayDetailActivity.this, message);
                }
            }
        }
        if (requestCode == Constants.REQUEST_BCARD_REPRINT) {
            if (data != null) {
                String message = data.getStringExtra("message");
                String responseCode = data.getStringExtra("responseCode");
                if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                    showCommonNoticeDialog(PayDetailActivity.this, message);
                }
            }
        }
        if (requestCode == Constants.REQUEST_BCARD_CHECK) {
            if (data != null) {
                String message;
                String responseCode;
                if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    message = data.getStringExtra("respmsg");
                    responseCode = data.getStringExtra("result");
                    if ("00".equals(responseCode)) {
                        mResultData = data;
                        payNotify(data);
                    } else if ("25".equals(responseCode)) {
                        showCommonNoticeDialog(PayDetailActivity.this, "订单查询不到原交易");
                    } else {
                        showCommonNoticeDialog(PayDetailActivity.this, message);
                    }
                } else {
                    message = data.getStringExtra("message");
                    responseCode = data.getStringExtra("responseCode");
                    showCommonNoticeDialog(PayDetailActivity.this, message + "(" + responseCode + ")");
                }
            }
        }
    }

    /**
     * 易生刷卡支付通知
     *
     * @param data
     */
    private void payNotify(Intent data) {
        if (!NetworkUtils.isNetworkAvailable(PayDetailActivity.this)) {
            showCommonNoticeDialog(PayDetailActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", mBilldata.getOrderNo());
            map.put("totalFee", new BigDecimal(data.getStringExtra("amount")).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            map.put("cardType", data.getStringExtra("paytype"));
            map.put("storeMerchantId", mBilldata.getStoreMerchantId());
            map.put("payType", data.getStringExtra("paytype"));
            String printInfo = data.getStringExtra("printInfo");
            if (!TextUtils.isEmpty(printInfo)) {
                Gson gson = new Gson();
                EasypayInfo easypayInfo = gson.fromJson(printInfo, EasypayInfo.class);
                map.put("timeEnd", DateUtil.formartTradeTime(easypayInfo.getDate_time()));
                map.put("voucherNo", easypayInfo.getTrace());
                map.put("batchId", easypayInfo.getBatch_no());
                map.put("refNo", easypayInfo.getRefer_no());
                map.put("payCardId", easypayInfo.getCard_no());
            }
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), "DETAIL_NOTIFY_PAY_EASYPAY", map);
        }
    }

    /**
     * 易生福卡支付通知
     *
     * @param data
     */
    private void payFukaNotify(FuxunBean data) {
        if (data != null) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", data.getCustomerOID());
            if (!TextUtils.isEmpty(data.getTransAmount())) {
                map.put("totalFee", new BigDecimal(data.getTransAmount()));
            }
            if (mBilldata != null) {
                map.put("storeMerchantId", mBilldata.getStoreMerchantId());

                map.put("payType", data.getTransType());
                if (TextUtils.isEmpty(DateUtil.formartWizardFukaTradeTime(data.getTransDate() + data.getTransTime(), mBilldata.getCreateTime()))) {
                    map.put("timeEnd", mBilldata.getTradeTime());
                } else {
                    map.put("timeEnd", DateUtil.formartWizardFukaTradeTime(data.getTransDate() + data.getTransTime(), mBilldata.getCreateTime()));
                }
            }
            map.put("voucherNo", data.getCertNum());
            map.put("batchId", data.getBatchNum());
            map.put("refNo", data.getReferCode());
            map.put("payCardId", data.getCardNum());
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), "DETAIL_NOTIFY_PAY_EASYPAY", map);
        }
    }

    private TradeDetailBean getTradeDetail(PayBean.DataBean data, RefundBean refundBean) {
        TradeDetailBean tradeDetailBean = new TradeDetailBean();
        if (data != null) {
            tradeDetailBean.setStoreMerchantIdCnt(data.getStoreMerchantIdCnt());
            tradeDetailBean.setOrderNo(data.getOrderNo());
            tradeDetailBean.setTransactionId(data.getTransactionId());
            tradeDetailBean.setTradeTime(data.getTradeTime());
            tradeDetailBean.setTradeState(data.getTradeState());
            tradeDetailBean.setCashierName(data.getCashierName());
            tradeDetailBean.setOpUserRealName(data.getOpUserRealName());
            tradeDetailBean.setApiProvider(data.getApiProvider());
            tradeDetailBean.setApiCode(data.getApiCode());
            tradeDetailBean.setMoney(data.getMoney());
            tradeDetailBean.setRealMoney(data.getRealMoney());
            tradeDetailBean.setPayMoney(data.getPayMoney());
            tradeDetailBean.setCouponFee(data.getMchDiscountsMoney());
            tradeDetailBean.setCouponInfoList(data.getCouponInfoList());
            tradeDetailBean.setAttach(data.getAttach());
            tradeDetailBean.setOutTradeNo(data.getOutTradeNo());
            tradeDetailBean.setReqOrderNo(data.getReqOrderNo());
            tradeDetailBean.setOriReqOrderNo(data.getOriReqOrderNo());
            tradeDetailBean.setVoucherNo(data.getVoucherNo());
            tradeDetailBean.setOpenid(data.getOpenid());
            tradeDetailBean.setThirdMerchantId(data.getThirdMerchantId());
            tradeDetailBean.setThirdOrderNo(data.getThirdOrderNo());
            tradeDetailBean.setTradeCode(data.getTradeCode());
            tradeDetailBean.setMdiscount(data.getMdiscount());
            tradeDetailBean.setMerchantName(data.getMerchantName());
            tradeDetailBean.setCashPointName(data.getCashPointName());

            tradeDetailBean.setPayCardId(data.getPayCardId());
            tradeDetailBean.setPayCardType(data.getPayCardType());
            tradeDetailBean.setPayCardBankName(data.getPayCardBankName());
            tradeDetailBean.setBatchID(data.getBatchID());
            tradeDetailBean.setDeviceSn(data.getTermNo());
        }

        if (refundBean != null) {
            tradeDetailBean.setReqRefundNo(refundBean.getReqRefundNo());
            tradeDetailBean.setRefundNo(refundBean.getRefundNo());
            tradeDetailBean.setRefundUpdateTime(refundBean.getRefundTime());
            tradeDetailBean.setRefundUser(refundBean.getRefundUser());
            tradeDetailBean.setRefundStatus(refundBean.getRefundStatus());
            tradeDetailBean.setRefundMoney(refundBean.getRefundMoney());
            tradeDetailBean.setRefundTime(refundBean.getCreateTime());
            tradeDetailBean.setStandby4(refundBean.getStandby4());
            tradeDetailBean.setPtRefundReason(refundBean.getPtRefundReason());
            tradeDetailBean.setPtAuditStatus(refundBean.getPtAuditStatus());
            tradeDetailBean.setDeviceSn(data.getTermNo());
        }
        return tradeDetailBean;
    }

    private void finishActivity() {
        if ("INTENT_NAME_FACE_PAYDETAIL".equals(getIntent().getStringExtra(Constants.INTENT_NAME))) {
            Intent intent = new Intent(Constants.ACTION_RECEIVE_RESET_DATA);
            sendBroadcast(intent);
            startActivity(new Intent(this, MainActivity.class));
        } else {
            if (mBilldata != null) {
                Intent intent = new Intent();
                intent.putExtra(Constants.RESULT_CASHIER_INTENT, mBilldata.getTradeState());
                intent.putExtra(Constants.RESULT_ORDER_NO, mBilldata.getOrderNo());
                setResult(RESULT_OK, intent);
            }
        }
        finish();
    }

    private void cancelCoupon() {
        if (TextUtils.isEmpty(getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO)) || mBilldata.getTradeState() == 2) {
            finishActivity();
        } else {
            showCheckCancelCoupon();
        }
    }

    private void showCheckCancelCoupon() {
        SelectDialog selectDialog = new SelectDialog(PayDetailActivity.this, "是否撤销优惠"
                , "请先核对订单是否支付成功；\n" +
                "若支付成功，请点击\"返回首页\"；\n" +
                "否则请点击\"撤销\"，会撤销该订单使用的优惠券", "撤销"
                , "返回首页", R.layout.select_common_dialog);
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                cancelUse(getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO));
            }
        });
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                finishActivity();
            }
        });
        selectDialog.show();
    }

    private void cancelUse(String outTradeNo) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("outTradeNo", outTradeNo);
            map.put("storeMerchantId", getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID));
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            ServerClient.newInstance(MyApplication.getContext()).cancelUse(MyApplication.getContext(), "TAG_DETAIL_COUPON_CANCEL", map);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
         if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
             if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                 //联迪银商
                 try {
                     BaseSystemManager.getInstance().deviceServiceLogin(
                             this, null, "99999998",//设备ID，生产找后台配置
                             new OnServiceStatusListener() {
                                 @Override
                                 public void onStatus(int arg0) {//arg0可见ServiceResult.java
                                     if (0 == arg0 || 2 == arg0 || 100 == arg0) {//0：登录成功，有相关参数；2：登录成功，无相关参数；100：重复登录。

                                     }
                                 }
                             });
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
         if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
             if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                 try {
                     BaseSystemManager.getInstance().deviceServiceLogout();
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
        }
    }
}
