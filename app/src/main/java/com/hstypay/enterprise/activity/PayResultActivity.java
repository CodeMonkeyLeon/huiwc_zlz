package com.hstypay.enterprise.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.CouponAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PushModel;
import com.hstypay.enterprise.bean.ShareBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.hstypay.enterprise.voice.VoiceUtils;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.ums.upos.sdk.system.BaseSystemManager;
import com.ums.upos.sdk.system.OnServiceStatusListener;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;


/**
 * 支付成功
 * Created by admin on 2017/7/3.
 */

public class PayResultActivity extends BaseActivity implements View.OnClickListener {
    private Button mBlue_print;
    private ScrollView mSvContent;
    private ImageView mIvBack, mIvPayStatus, mIvCouponArrow;
    private Button mBtnComplete, mBtnPrint;
    private TextView mButton, mTvTitle, mTvPayStatus, mTvCouponMoney, mTvRemark, mTvBountyMoney, mTvRealMoney, mTvPayStore;
    private TextView mTvTradeMoney, mTvTradeTime, mTvTradeWay, mTvTradeCodeTitle, mTvCashier, mTvTradeWayCode, mTvTradeCode, mTvSite;
    private PayBean.DataBean mPayBean;
    private LinearLayout mLlCashier, mLlCoupon, mLlRemark, mLlBounty, mLlThirdCode, mLlTradeCode, mLlSite;
    private Printer printer;
    private PosPrintUtil mPosPrintUtil;
    private IWoyouService woyouService;
    private SafeDialog mLoadDialog;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private RecyclerView mRvCoupon;
    private String mIntentName;
    private boolean mSwitch;

    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private N900Device n900Device;

    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
            autoPrint();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("print----onServiceDisconnected");
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogUtil.d("print----onServiceConnected");
            woyouService = IWoyouService.Stub.asInterface(service);
            autoPrint();
        }
    };

    /**
     * 商米绑定打印服务
     **/
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 汇付绑定打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_result);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();

        //注册handler 检查蓝牙打开和关闭的操作
        // HandlerManager.registerHandler( HandlerManager.BLUE_CONNET, handler);
        // HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);
        initListener();
        initData();
        initPosPrint();
        if (!MyApplication.getIsCasher()) {
            sendTodayDataBroadcast(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        } else if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                try {
                    BaseSystemManager.getInstance().deviceServiceLogout();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.MIBAGPAY_LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            autoPrint();
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
            autoPrint();
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
            autoPrint();
        } else if ("a920".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("hdy".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if (Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
                autoPrint();
            } else {
                //联迪银商
                try {
                    BaseSystemManager.getInstance().deviceServiceLogin(
                            this, null, "99999998",//设备ID，生产找后台配置
                            new OnServiceStatusListener() {
                                @Override
                                public void onStatus(int arg0) {//arg0可见ServiceResult.java
                                    if (0 == arg0 || 2 == arg0 || 100 == arg0) {//0：登录成功，有相关参数；2：登录成功，无相关参数；100：重复登录。
                                        autoPrint();
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (AppHelper.getAppType() == 2 && MyApplication.autoPrint()) {
            autoPrint();
        }
    }

    private void autoPrint() {
        if (Constants.INTENT_NAME_CAPTURE_PAYRESULT.equals(mIntentName)) {
            if (mPayBean != null) {
                mPayBean.setPay(true);
                TradeDetailBean tradeDetail = getTradeDetail(mPayBean);
                tradeDetail.setPay(true);
                mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, tradeDetail, true);
            }
            mIntentName = "";
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mBlue_print.setOnClickListener(this);
        mBtnComplete.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mLlCoupon.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvPayStatus = (ImageView) findViewById(R.id.logo_title);
        mTvPayStatus = (TextView) findViewById(R.id.tv_pay_status);
        mTvRealMoney = (TextView) findViewById(R.id.tv_real_money);//实收金额
        mTvTradeMoney = (TextView) findViewById(R.id.tx_receivable);//收款金额
        mTvTradeTime = (TextView) findViewById(R.id.tv_order_time);//交易时间
        mTvTradeWay = (TextView) findViewById(R.id.tv_bank);//支付方式
        mTvTradeCodeTitle = (TextView) findViewById(R.id.tv_third_order_title);//支付方式单号title
        mTvTradeWayCode = (TextView) findViewById(R.id.tv_third_order_no);//支付方式单号
        mLlTradeCode = findViewById(R.id.ll_trade_code);//平台订单号
        mTvTradeCode = (TextView) findViewById(R.id.tv_mch_transNo);//平台订单号
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);//收银员
        mBtnComplete = (Button) findViewById(R.id.btn_success);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mTvTitle.setText(R.string.title_pay_detail);
        mButton.setText(R.string.tv_refund);
        //mButton.setVisibility(View.VISIBLE);
        mBlue_print = (Button) findViewById(R.id.blue_print);
        mButton.setVisibility(View.INVISIBLE);
        mLlCashier = (LinearLayout) findViewById(R.id.ll_cashier);
        mLlThirdCode = (LinearLayout) findViewById(R.id.lr_wx);

        mTvPayStore = (TextView) findViewById(R.id.tv_pay_store);//交易门店
        mLlCoupon = (LinearLayout) findViewById(R.id.ll_coupon);//优惠金额
        mTvCouponMoney = (TextView) findViewById(R.id.tx_coupon);//优惠金额
        mRvCoupon = (RecyclerView) findViewById(R.id.rv_coupon);//优惠详情
        mIvCouponArrow = (ImageView) findViewById(R.id.iv_coupon_arrow);

        mLlBounty = (LinearLayout) findViewById(R.id.ll_bounty);//营销奖励
        mTvBountyMoney = (TextView) findViewById(R.id.tv_bounty);//营销奖励金额

        mTvRemark = (TextView) findViewById(R.id.tv_pay_remark);//备注
        mLlRemark = (LinearLayout) findViewById(R.id.ll_pay_remark);//备注

        mLlSite = findViewById(R.id.ll_site);//收银点
        mTvSite = findViewById(R.id.tv_site);//收银点

        setButtonEnable(mBtnComplete, true);
        setButtonWhite(mBtnPrint);
    }

    public void initData() {
        getPrintActive();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mPayBean = (PayBean.DataBean) getIntent().getSerializableExtra("capture");

        if (mPayBean != null) {
            if (MyApplication.getIsBounty()) {
                mSvContent.setVisibility(View.GONE);
                getBountyMoney(mPayBean.getOrderNo());
            }
            //交易单号
            if (!StringUtils.isEmptyOrNull(mPayBean.getOrderNo())) {
                mTvTradeCode.setText(mPayBean.getOrderNo());
                mLlTradeCode.setVisibility(View.VISIBLE);
            } else {
                mLlTradeCode.setVisibility(View.GONE);
            }
            //第三方单号
            if (!StringUtils.isEmptyOrNull(mPayBean.getTransactionId())) {
                mTvTradeWayCode.setText(mPayBean.getTransactionId());
                mLlThirdCode.setVisibility(View.VISIBLE);
            } else {
                mLlThirdCode.setVisibility(View.GONE);
            }
            //支付时间
            if (!StringUtils.isEmptyOrNull(mPayBean.getTradeTime())) {
                mTvTradeTime.setText(mPayBean.getTradeTime());
            }
            //交易门店
            if (!StringUtils.isEmptyOrNull(mPayBean.getStoreMerchantIdCnt())) {
                mTvPayStore.setText(mPayBean.getStoreMerchantIdCnt());
            } else {
                mTvPayStore.setText(MyApplication.getDefaultStoreName());
            }
            //支付类型
            if (!StringUtils.isEmptyOrNull(mPayBean.getAttach())) {
                mLlRemark.setVisibility(View.VISIBLE);
                mTvRemark.setText(mPayBean.getAttach());
            } else {
                mLlRemark.setVisibility(View.GONE);
            }
            //收银员
            if (!StringUtils.isEmptyOrNull(mPayBean.getCashierName())) {
                mLlCashier.setVisibility(View.VISIBLE);
                mTvCashier.setText(mPayBean.getCashierName());
            } else {
                mLlCashier.setVisibility(View.GONE);
            }
            //收银点
            if (!StringUtils.isEmptyOrNull(mPayBean.getCashPointName())) {
                mLlSite.setVisibility(View.VISIBLE);
                mTvSite.setText(mPayBean.getCashPointName());
            } else {
                mLlSite.setVisibility(View.GONE);
            }
            int apiProvider = mPayBean.getApiProvider();
            mTvTradeWay.setText(OrderStringUtil.tradeTypeFromString(apiProvider, mPayBean.getApiCode()));
            mTvTradeCodeTitle.setText("交易单号");

            mTvRealMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(mPayBean.getRealMoney() / 100d));
            String money = mPayBean.getMoney() + "";
            if (!StringUtils.isEmptyOrNull(money)) {
                mTvTradeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Long.parseLong(money) / 100d));
                //改由推送语音播报
                String intent_name = getIntent().getStringExtra(Constants.INTENT_NAME);
                String appMetaData = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
                if (Constants.WJY_YINSHANG.equals(appMetaData) && Constants.INTENT_NAME_CAPTURE_PAYRESULT.equals(intent_name) && MyApplication.isOpenVoice()) {
                    PushModel pushModel = new PushModel();
                    if (AppHelper.getApkType() == 0) {
                        pushModel.setVoiceContent("旺旺，收款成功，" + (Long.parseLong(money) / 100d) + "元");
                    } else {
                        pushModel.setVoiceContent("收款成功，" + (Long.parseLong(money) / 100d) + "元");
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String moneyString = new BigDecimal(mPayBean.getMoney()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).toString();
                            play(moneyString);
                        }
                    }).start();
                }
            }
            if (mPayBean.getMchDiscountsMoney() > 0) {
                mLlCoupon.setVisibility(View.VISIBLE);
                mTvCouponMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyUtil(mPayBean.getMchDiscountsMoney() / 100d));
                CustomLinearLayoutManager customLinearLayoutManager = new CustomLinearLayoutManager(this);
                mRvCoupon.setLayoutManager(customLinearLayoutManager);
                CouponAdapter adapter = new CouponAdapter(this, mPayBean.getCouponInfoList());
                mRvCoupon.setAdapter(adapter);
            }
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
            case R.id.btn_success:
                toMainActivity();
                break;
            //打印小票
            case R.id.blue_print:
                getPrint();
                break;
            case R.id.ll_coupon:
                Animation rotate;
                if (mSwitch) {
                    rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
                    mSwitch = false;
                    mRvCoupon.setVisibility(View.GONE);
                } else {
                    rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
                    mSwitch = true;
                    mRvCoupon.setVisibility(View.VISIBLE);
                }
                rotate.setInterpolator(new LinearInterpolator());
                rotate.setFillAfter(true);
                mIvCouponArrow.startAnimation(rotate);
                break;
            default:
                break;
        }
    }

    public void getPrint() {
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        if (mPayBean != null) {
            mPayBean.setPay(true);
            TradeDetailBean tradeDetail = getTradeDetail(mPayBean);
            tradeDetail.setPay(true);
            mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, tradeDetail, false);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            toMainActivity();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void toMainActivity() {
        Intent intent = new Intent(Constants.ACTION_RECEIVE_RESET_DATA);
        sendBroadcast(intent);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PayResultActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PayResultActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_BOUNTY_MONEY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            mSvContent.setVisibility(View.VISIBLE);
            ShareBean info = (ShareBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PayResultActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PayResultActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null && info.getData().getRewardMoney() > 0) {
                        mLlBounty.setVisibility(View.VISIBLE);
                        mTvBountyMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(info.getData().getRewardMoney() / 100d));
                    }
                    break;
            }
        }
    }

    public static void startActivity(Context mContext, PayBean order) {
        Intent it = new Intent();
        it.setClass(mContext, PayResultActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("order", order);
        mContext.startActivity(it);
    }

    private synchronized void play(final String str) {
        if (VoiceUtils.with(this).getIsPlay()) {
            LogUtil.d("正在播放语音 ");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Thread.sleep(100);//休眠0.1秒
                        play(str);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                    /**
                     * 要执行的操作
                     */
                }
            }.start();
        } else {
            LogUtil.d("不冲突");
            VoiceUtils.with(this).play(str, true);
        }
    }

    private void getBountyMoney(String orderNo) {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap();
            map.put("orderNo", orderNo);
            ServerClient.newInstance(MyApplication.getContext()).bountyMoney(MyApplication.getContext(), Constants.TAG_GET_BOUNTY_MONEY, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private TradeDetailBean getTradeDetail(PayBean.DataBean data) {
        TradeDetailBean tradeDetailBean = new TradeDetailBean();
        tradeDetailBean.setStoreMerchantIdCnt(data.getStoreMerchantIdCnt());
        tradeDetailBean.setOrderNo(data.getOrderNo());
        tradeDetailBean.setTransactionId(data.getTransactionId());
        tradeDetailBean.setTradeTime(data.getTradeTime());
        tradeDetailBean.setTradeState(data.getTradeState());
        tradeDetailBean.setCashierName(data.getCashierName());
        tradeDetailBean.setOpUserRealName(data.getOpUserRealName());
        tradeDetailBean.setApiProvider(data.getApiProvider());
        tradeDetailBean.setApiCode(data.getApiCode());
        tradeDetailBean.setMoney(data.getMoney());
        tradeDetailBean.setRealMoney(data.getRealMoney());
        tradeDetailBean.setPayMoney(data.getPayMoney());
        tradeDetailBean.setCouponFee(data.getMchDiscountsMoney());
        tradeDetailBean.setCouponInfoList(data.getCouponInfoList());
        tradeDetailBean.setAttach(data.getAttach());
        tradeDetailBean.setOutTradeNo(data.getOutTradeNo());
        tradeDetailBean.setReqOrderNo(data.getReqOrderNo());
        tradeDetailBean.setOriReqOrderNo(data.getOriReqOrderNo());
        tradeDetailBean.setVoucherNo(data.getVoucherNo());
        tradeDetailBean.setCashPointName(data.getCashPointName());
        tradeDetailBean.setDeviceSn(data.getTermNo());

        tradeDetailBean.setOpenid(data.getOpenid());
        tradeDetailBean.setThirdMerchantId(data.getThirdMerchantId());
        tradeDetailBean.setThirdOrderNo(data.getThirdOrderNo());
        tradeDetailBean.setTradeCode(data.getTradeCode());
        tradeDetailBean.setCouponInfoData(data.getCouponInfoData());
        if (data.getCouponGroupInfoData() != null && data.getCouponGroupInfoData().size() > 0) {
            tradeDetailBean.setCouponGroupInfoData(data.getCouponGroupInfoData());
        }
        return tradeDetailBean;
    }
}
