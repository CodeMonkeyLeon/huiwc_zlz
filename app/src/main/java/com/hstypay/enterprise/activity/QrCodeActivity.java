package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QrCodeActivity extends BaseActivity implements View.OnClickListener {
    private Button mBtnChangeScan;
    private TextView mButton, mTvTitle, mTvMoney, mTvTitleWechat, mTvTitleAlipay, mTvTitleUnion, mTvTitleQQ, mTvTitleHuabei, mTvTitleJd, mTvTitleFuka, mTvCodeNo, mTvCompany;
    private ImageView mIvBack, mIvQrCode, mIvLogoWechat, mIvLogoAlipay, mIvLogoUnion, mIvLogoQQ, mIvLogoHuabei, mIvLogoJd, mIvLogoFuka, mIvQrLogo;
    private TextView mTvCodeRemark;
    private LinearLayout mLlBitmap;
    private RelativeLayout mRlQrLogo;
    private ScrollView mSvContent;

    private Bitmap bitmap;
    private String mStoreMerchantId, mUserId, money, mPayHbfq;
    private double payMoney;
    private String mPayRemark;
    private boolean mIsRateFree;
    private long mUnionUserFee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMoney = (TextView) findViewById(R.id.tv_set_money);//设置的金额
        mIvQrCode = (ImageView) findViewById(R.id.img_qr_code);//二维码图片
        mTvCodeNo = (TextView) findViewById(R.id.tv_code_no);//二维码编号
        mTvCompany = (TextView) findViewById(R.id.tv_company);//店名
        mLlBitmap = (LinearLayout) findViewById(R.id.ll_qrcode_bitmap);
        mBtnChangeScan = findViewById(R.id.btn_change_scan);
        mSvContent = findViewById(R.id.static_qrcode_sv);
        mTvCodeRemark = (TextView) findViewById(R.id.tv_code_remark);//收款备注

        mRlQrLogo = findViewById(R.id.rl_qr_logo);
        mIvQrLogo = findViewById(R.id.img_qr_logo);
        mIvLogoWechat = findViewById(R.id.iv_logo_wechat);
        mIvLogoAlipay = findViewById(R.id.iv_logo_alipay);
        mIvLogoUnion = findViewById(R.id.iv_logo_union);
        mIvLogoQQ = findViewById(R.id.iv_logo_qq);
        mIvLogoHuabei = findViewById(R.id.iv_logo_huabei);
        mIvLogoJd = findViewById(R.id.iv_logo_jd);
        mIvLogoFuka = findViewById(R.id.iv_logo_fuka);

        mTvTitleWechat = findViewById(R.id.tv_title_wechat);
        mTvTitleAlipay = findViewById(R.id.tv_title_alipay);
        mTvTitleUnion = findViewById(R.id.tv_title_union);
        mTvTitleQQ = findViewById(R.id.tv_title_qq);
        mTvTitleHuabei = findViewById(R.id.tv_title_huabei);
        mTvTitleJd = findViewById(R.id.tv_title_jd);
        mTvTitleFuka = findViewById(R.id.tv_title_fuka);

        mTvTitle.setText(R.string.title_qrcode);
        mButton.setVisibility(View.INVISIBLE);
        setButtonEnable(mBtnChangeScan, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnChangeScan.setOnClickListener(this);
    }

    public void initData() {
        Intent intent = getIntent();
        if (Constants.INTENT_NAME_POS_QRCODE.equals(intent.getStringExtra(Constants.INTENT_NAME))) {
            mBtnChangeScan.setVisibility(View.INVISIBLE);
        }
        mStoreMerchantId = intent.getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
        mUserId = SpUtil.getString(MyApplication.getContext(), Constants.USER_ID, "");
        payMoney = intent.getDoubleExtra(Constants.INTENT_PAY_MONEY, 0);
        mPayRemark = getIntent().getStringExtra(Constants.INTENT_PAY_REMARK);//备注
        mPayHbfq = intent.getStringExtra(Constants.INTENT_HBFQ);
        mIsRateFree = intent.getBooleanExtra(Constants.INTENT_IS_RATE_FREE, false);
        mUnionUserFee = getIntent().getLongExtra(Constants.INTENT_UNION_USER_FEE, 0);//花呗分期用户付息费用
        money = String.valueOf(BigDecimal.valueOf(payMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
        BigDecimal unionUserFee = new BigDecimal(mUnionUserFee).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
        double textMoney = payMoney + unionUserFee.doubleValue();//金额

        getStoreCode("15", "1", mStoreMerchantId);
        if (payMoney > 0) {
            mTvMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(textMoney));
        }
        /*if (!TextUtils.isEmpty(mPayRemark)){
            mTvCodeRemark.setText("备注："+mPayRemark);
            mTvCodeRemark.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_change_scan:
                Intent intent = new Intent(this, CaptureActivity.class);
                intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreMerchantId);
                intent.putExtra(Constants.INTENT_PAY_MONEY, payMoney);
                intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
                intent.putExtra(Constants.INTENT_HBFQ, mPayHbfq);
                intent.putExtra(Constants.INTENT_IS_RATE_FREE, mIsRateFree);
                intent.putExtra(Constants.INTENT_UNION_USER_FEE, mUnionUserFee);
                startActivity(intent);
                finish();
                break;
        }
    }

    public void setQrcode(StoreCodeListBean.DataBeanX.DataBean qrCodeStore, String userId, String money, String payHbfq, boolean isRateFree) {
        String storeName = qrCodeStore.getStoreName();
        if (!TextUtils.isEmpty(storeName)) {
            mTvCompany.setText(storeName);
        }
        String codeMsg;
        String hbFqNum;
        if (TextUtils.isEmpty(payHbfq)) {
            hbFqNum = "1";
        } else {
            hbFqNum = payHbfq;
        }
        if (qrCodeStore.getQrType() == 3 && !TextUtils.isEmpty(qrCodeStore.getWxappid())) {
            /*if (TextUtils.isEmpty(money)) {
                codeMsg = Constants.H5_STATIC_QRCODE + "wxappid=" + qrCodeStore.getWxappid() + "&storeMerchantId=" + qrCodeStore.getStoreId() + "&userId=" + userId + "&qrcode=" + qrCodeStore.getQrcode();
            } else {*/
            codeMsg = Constants.H5_STATIC_QRCODE + "wxappid=" + qrCodeStore.getWxappid() + "&storeMerchantId=" + mStoreMerchantId +
                    "&userId=" + mUserId + "&qrcode=" + qrCodeStore.getQrcode() + "&money=" + money + "&hbFqNum=" + hbFqNum + "&remark=" + mPayRemark /*+ "&freeFee=" + isRateFree*/;
//            }
        } else if (qrCodeStore.getQrType() == 4) {
            if (isRateFree) {
                codeMsg = qrCodeStore.getQrUrl() + "&money=" + money + "&hbFqNum=" + hbFqNum + "&freeFee=1" + "&hbFlag=1" + "&unionUserFee=" + mUnionUserFee + "&remark=" + mPayRemark;
            } else {
                codeMsg = qrCodeStore.getQrUrl() + "&money=" + money + "&hbFqNum=" + hbFqNum + "&freeFee=0" + "&hbFlag=1" + "&unionUserFee=" + mUnionUserFee + "&remark=" + mPayRemark;
            }
//                codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + mStoreMerchantId +
//                        "&userId=" + mUserId + "&qrcode=" + qrCodeStore.getQrcode() + "&money=" + money /*+ "&hbFqNum=" + hbFqNum + "&freeFee=" + isRateFree*/;
        } else {
           /* if (TextUtils.isEmpty(money)) {
                codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + qrCodeStore.getStoreId() + "&userId=" + userId + "&qrcode=" + qrCodeStore.getQrcode();
            } else {*/
            codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + mStoreMerchantId +
                    "&userId=" + mUserId + "&qrcode=" + qrCodeStore.getQrcode() + "&money=" + money + "&hbFqNum=" + hbFqNum + "&remark=" + mPayRemark /*+ "&freeFee=" + isRateFree*/;
//            }
        }
        LogUtil.d("codeMsg===" + codeMsg);
        try {
            bitmap = MaxCardManager.getInstance().create2DCode(codeMsg,
                    DisplayUtil.dip2Px(QrCodeActivity.this, 400),
                    DisplayUtil.dip2Px(QrCodeActivity.this, 400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
        mIvQrCode.setImageBitmap(bitmap);
    }

    public void setQrcode(String storeMerchantId, String userId, String qrcode, String money, String payHbfq, boolean isRateFree) {
        String codeMsg;
        String hbFqNum;
        if (TextUtils.isEmpty(payHbfq)) {
            hbFqNum = "1";
        } else {
            hbFqNum = payHbfq;
        }
        if (payMoney <= 0) {
            codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + storeMerchantId + "&userId="
                    + userId + "&qrcode=" + qrcode + "&hbFqNum=" + hbFqNum + "&freeFee=" + isRateFree;
        } else {
            codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + storeMerchantId +
                    "&userId=" + userId + "&qrcode=" + qrcode + "&money=" + money + "&hbFqNum=" + hbFqNum + "&freeFee=" + isRateFree;
        }

        LogUtil.d("codeMsg===" + codeMsg);

        try {
            bitmap = MaxCardManager.getInstance().create2DCode(codeMsg,
                    DisplayUtil.dip2Px(QrCodeActivity.this, 400),
                    DisplayUtil.dip2Px(QrCodeActivity.this, 400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
        mIvQrCode.setImageBitmap(bitmap);
    }

    private void setQrcodeImage(StoreCodeListBean.DataBeanX.DataBean qrCodeInfo) {
        if (qrCodeInfo != null) {
            if (qrCodeInfo.getApiProvider() != null) {
                for (int i = 0; i < qrCodeInfo.getApiProvider().size(); i++) {
                    if ("1".equals(qrCodeInfo.getApiProvider().get(i))) {
                        mIvLogoWechat.setVisibility(View.VISIBLE);
                        mTvTitleWechat.setVisibility(View.VISIBLE);
                    }
                    if ("2".equals(qrCodeInfo.getApiProvider().get(i))) {
                        mIvLogoAlipay.setVisibility(View.VISIBLE);
                        mTvTitleAlipay.setVisibility(View.VISIBLE);
                    }
                    if ("4".equals(qrCodeInfo.getApiProvider().get(i))) {
                        mIvLogoQQ.setVisibility(View.VISIBLE);
                        mTvTitleQQ.setVisibility(View.VISIBLE);
                    }
                    if ("5".equals(qrCodeInfo.getApiProvider().get(i))) {
                        mIvLogoUnion.setVisibility(View.VISIBLE);
                        mTvTitleUnion.setVisibility(View.VISIBLE);
                    }
                    if ("6".equals(qrCodeInfo.getApiProvider().get(i))) {
                        mIvLogoJd.setVisibility(View.VISIBLE);
                        mTvTitleJd.setVisibility(View.VISIBLE);
                    }
                    if ("11".equals(qrCodeInfo.getApiProvider().get(i))) {
                        mIvLogoFuka.setVisibility(View.VISIBLE);
                        mTvTitleFuka.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                mIvLogoWechat.setVisibility(View.VISIBLE);
                mTvTitleWechat.setVisibility(View.VISIBLE);
                mIvLogoAlipay.setVisibility(View.VISIBLE);
                mTvTitleAlipay.setVisibility(View.VISIBLE);
                mIvLogoUnion.setVisibility(View.VISIBLE);
                mTvTitleUnion.setVisibility(View.VISIBLE);
            }
        } else {
            mIvLogoWechat.setVisibility(View.VISIBLE);
            mTvTitleWechat.setVisibility(View.VISIBLE);
            mIvLogoAlipay.setVisibility(View.VISIBLE);
            mTvTitleAlipay.setVisibility(View.VISIBLE);
            mIvLogoUnion.setVisibility(View.VISIBLE);
            mTvTitleUnion.setVisibility(View.VISIBLE);
        }
    }

    //请求门店二维码
    public void getStoreCode(String pageSize, String currentPage, String storeId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            if (!TextUtils.isEmpty(storeId)) {
                map.put("storeId", storeId);
            }
            ServerClient.newInstance(MyApplication.getContext()).qrcodeList(MyApplication.getContext(), Constants.TAG_CHANGE_STORE_CODE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHANGE_STORE_CODE)) {
            dismissLoading();
            StoreCodeListBean msg = (StoreCodeListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(QrCodeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        List<StoreCodeListBean.DataBeanX.DataBean> data = msg.getData().getData();
                        for (int i = 0; i < data.size(); i++) {
                            if (TextUtils.isEmpty(mPayHbfq)) {
                                if (data.get(i).getQrType() == 1 || data.get(i).getQrType() == 3) {
                                    mSvContent.setVisibility(View.VISIBLE);
                                    String qrcode = data.get(i).getQrcode();
                                    setQrcode(data.get(i), qrcode, money, mPayHbfq, mIsRateFree);
                                    setQrcodeImage(data.get(i));
                                    return;
                                }
                            } else {
                                //花呗码
                                if (data.get(i).getQrType() == 4) {
                                    mSvContent.setVisibility(View.VISIBLE);
                                    String qrcode = data.get(i).getQrcode();
                                    setQrcode(data.get(i), qrcode, money, mPayHbfq, mIsRateFree);
                                    setQrcodeImage(data.get(i));
                                    return;
                                }
                            }
                        }
                        if (TextUtils.isEmpty(mPayHbfq)) {
                            showCommonNoticeDialog(QrCodeActivity.this, getString(R.string.tv_store_third_code_null));
                        } else {
                            showCommonNoticeDialog(QrCodeActivity.this, getString(R.string.tv_store_huabei_code_null));
                        }
                    } else {
                        showCommonNoticeDialog(QrCodeActivity.this, getString(R.string.tv_store_code_null));
                    }
                    break;
            }
        }
    }
}
