package com.hstypay.enterprise.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.AndroidBug5497Workaround;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.v5kf.client.lib.V5ClientAgent;
import com.v5kf.client.lib.V5ClientAgent.ClientOpenMode;
import com.v5kf.client.lib.V5ClientConfig;
import com.v5kf.client.lib.entity.V5Message;
import com.v5kf.client.ui.ClientChatActivity;
import com.v5kf.client.ui.callback.ChatActivityFuncIconClickListener;
import com.v5kf.client.ui.callback.OnChatActivityListener;
import com.v5kf.client.ui.callback.OnURLClickListener;
import com.v5kf.client.ui.callback.UserWillSendMessageListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/27 10:33
 * @描述: ${TODO}
 */

public class QuestionActivity extends BaseActivity implements View.OnClickListener, OnChatActivityListener {

    private WebView mWvResgister;
    private ImageView mIvBack, mIvClose;
    private TextView mTvTitlte;

    private ValueCallback<Uri> mUploadMessage;// 表单的数据信息
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    private final static int FILECHOOSER_RESULTCODE = 1;// 表单的结果回调</span>
    private Uri imageUri;
    private String url;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private String[] locationPermissionArray = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE5 = 1005;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 1006;
    private ProgressBar mPg;
    private boolean isAllowClose;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        AndroidBug5497Workaround.assistActivity(this);
        initV5Chat();
        isAllowClose = getIntent().getBooleanExtra(Constants.REGISTER_ALLOW_CLOSE, false);
        if (savedInstanceState != null) {
            url = savedInstanceState.getString("url");
        } else {
            url = getIntent().getStringExtra(Constants.REGISTER_INTENT);
        }
        if (url == null) {
            return;
        }
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvClose = (ImageView) findViewById(R.id.iv_close);
        mTvTitlte = (TextView) findViewById(R.id.tv_title);
        mWvResgister = (WebView) findViewById(R.id.wv_register);
        mPg = (ProgressBar) findViewById(R.id.progressBar);
        mIvBack.setOnClickListener(this);
        mIvClose.setOnClickListener(this);

        if (AppHelper.getAndroidSDKVersion() < 17) {
            mWvResgister.removeJavascriptInterface("searchBoxjavaBridge_");//解决 CVE-2014-1939 漏洞
            mWvResgister.removeJavascriptInterface("accessibility");//解决  CVE-2014-7224漏洞
            mWvResgister.removeJavascriptInterface("accessibilityTraversal");//解决  CVE-2014-7224漏洞
        }

        WebSettings webSettings = mWvResgister.getSettings();
        setZoom(webSettings);
        //定位权限
        webSettings.setDatabaseEnabled(true);
        //设置定位的数据库路径
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setGeolocationDatabasePath(dir);
        webSettings.setGeolocationEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
        webSettings.setDomStorageEnabled(true);

//        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccess(false);
        webSettings.setAllowFileAccessFromFileURLs(false);
        webSettings.setAllowUniversalAccessFromFileURLs(false);
        //webSettings.setSavePassword(false);

        String ua = webSettings.getUserAgentString();
        webSettings.setUserAgentString(ua + AppHelper.getUserAgent(MyApplication.getContext()));
        //设置为可调用js方法
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        webSettings.setJavaScriptEnabled(true);
        webSettings.setBlockNetworkImage(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        mWvResgister.requestFocus();
        mWvResgister.addJavascriptInterface(new JsInteration(), "android");

        setWebChromeClient(mWvResgister);

        String[] cookies = SpUtil.getString(MyApplication.getContext(), Constants.LOGIN_COOKIE, "").split("<<<->>>");
        if (cookies != null && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                setCookie(url, cookies[i]);
            }
        }
        LogUtil.d("Jeremy QuestionActivity url=" + url);
        mWvResgister.loadUrl(url);

        mWvResgister.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mIvClose.setVisibility(isAllowClose ? View.VISIBLE : View.INVISIBLE);
                LogUtil.d("QuestionActivity url=" + url);
                QuestionActivity.this.url = url;
                if (url.contains("static/WebH5/opinion_feedback/opinion_feedback.html")) {
//                    feedback();
                    startChatActivity();
                    return true;
                }
                if (url.contains("weixin://")) {
                    skipToWechat(url);
                    return true;
                }
                //调用拨号程序
                if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:")) {
                    if (AppHelper.getAppType() == 2) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    } else if (AppHelper.getAppType() == 1) {
                        return true;
                    }
                }
                return false;
            }

            //华为mate9保时捷版打不开
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mTvTitlte.setText(view.getTitle());
            }
        });
    }

    public void setWebChromeClient(WebView wvRegister) {
        wvRegister.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mPg.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    mPg.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    mPg.setProgress(newProgress);//设置进度值
                }
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                mTvTitlte.setText(title);
            }

            @Override
            public boolean onShowFileChooser(WebView webView,
                                             ValueCallback<Uri[]> filePathCallback,
                                             FileChooserParams fileChooserParams) {
                mUploadCallbackAboveL = filePathCallback;
                take();
                return true;
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                take();
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);

            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

        });
    }

    /*private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(QuestionActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(QuestionActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }

    private void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(QuestionActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE5:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE6:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_location));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void setCookie(String url, String stringCookie) {
        CookieSyncManager.createInstance(MyApplication.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeSessionCookies(null);
            cookieManager.setAcceptThirdPartyCookies(mWvResgister, true);
            cookieManager.flush();
        } else {
            cookieManager.removeSessionCookie();
            cookieManager.setAcceptCookie(true);
            CookieSyncManager.getInstance().sync();
        }
        cookieManager.setAcceptCookie(true);
        cookieManager.setCookie(url, stringCookie);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                if (mWvResgister.canGoBack()) {
                    mWvResgister.goBack();
                } else {
                    finish();
                }
                break;
            case R.id.iv_close:
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * 初始化V5KF 界面
     */
    private void initV5Chat() {
        // V5客服系统客户端配置
        V5ClientConfig config = V5ClientConfig.getInstance(MyApplication.getContext());
        V5ClientConfig.USE_HTTPS = true; // 使用加密连接，默认true
        V5ClientConfig.SOCKET_TIMEOUT = 20000; // 请求超时时间
        config.setHeartBeatEnable(true); // 是否允许发送心跳包保活
        config.setHeartBeatTime(30000); // 心跳包间隔时间ms
        config.setShowLog(true); // 显示日志，默认为true
        config.setLogLevel(V5ClientConfig.LOG_LV_DEBUG); // 显示日志级别，默认为全部显示

        /*if (!TextUtils.isEmpty(MyApplication.getRealName())) {
            config.setNickname(StringUtils.hideFirstName(MyApplication.getRealName()));
        } else if (!TextUtils.isEmpty(MyApplication.getUsername())) {
            config.setNickname(StringUtils.hideLoginName(MyApplication.getUsername()));
        }*/
        config.setNickname(MyApplication.getMechantId());
        config.setGender(0); // 设置用户性别: 0-未知  1-男  2-女
        // 设置用户头像URL
        //config.setAvatar("http://debugimg-10013434.image.myqcloud.com/fe1382d100019cfb572b1934af3d2c04/thumbnail");
        config.setVip(0); // 设置用户VIP等级（0-5）
        /**
         *【建议】设置用户OpenId，以识别不同登录用户，不设置则默认由SDK生成，替代v1.2.0之前的uid,
         *  openId将透传到座席端(建议使用含字母数字和下划线的字符串，尽量不用特殊字符，若含特殊字符系统会进行URL encode处理)
         *	若您是旧版本SDK用户，只是想升级，为兼容旧版，避免客户信息改变可继续使用config.setUid，可不用openId
         */
        config.setOpenId(MyApplication.getUserId());
        //config.setUid(uid); //【弃用】请使用setOpenId替代
        // 设置device_token：集成第三方推送(腾讯信鸽、百度云推)时设置此参数以在离开会话界面时接收推送消息
        //config.setDeviceToken(XGPushConfig.getToken(getApplicationContext())); // 【建议】设置deviceToken

        // 客户信息键值对（JSONObject）
        JSONObject customContent = new JSONObject();
        try {
            customContent.put("APP名称", AppHelper.getAppName(MyApplication.getContext()));
            customContent.put("APP本版号", AppHelper.getVerName(MyApplication.getContext()));
            customContent.put("安卓系统版本", AppHelper.getAndroidSDKVersionName());
            customContent.put("手机型号", Build.BRAND + ";"+Build.MODEL);
            if (!TextUtils.isEmpty(AppHelper.getIPAddress(MyApplication.getContext())))
                customContent.put("IP", AppHelper.getIPAddress(MyApplication.getContext()));
            customContent.put("APP_CHANNEL", AppHelper.getAppMetaData(MyApplication.getContext(),Constants.APP_META_DATA_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // 设置客户信息（自定义JSONObjectjian键值对，开启会话前设置）
        config.setUserInfo(customContent);
    }

    private void startChatActivity() {
        /* 开启会话界面 */
        // 可用Bundle传递以下参数
        Bundle bundle = new Bundle();
        bundle.putInt("numOfMessagesOnRefresh", 10);    // 下拉刷新数量，默认为10
        bundle.putInt("numOfMessagesOnOpen", 10);        // 开场显示历史消息数量，默认为10
        bundle.putBoolean("enableVoice", true);            // 是否允许发送语音
        bundle.putBoolean("showAvatar", true);            // 是否显示对话双方的头像
        /*
         * 设置开场白模式，默认为clientOpenModeDefault，可根据客服启动场景设置开场问题
         * clientOpenModeDefault	// 默认开场白方式（无历史消息显示则显示开场白，优先以设置的param字符串为开场白，param为null则使用后台配置的开场白）
         * clientOpenModeQuestion	// 自定义问题开场白，param字符串为问题内容（不为空），设置开场问题获得对应开场白（此模式不可与优先人工客服同用，否则将失效）
         * clientOpenModeNone		// 无开场白方式，仅显示历史消息
         * clientOpenModeAutoHuman  // 开场自动转人工客服
         */
        bundle.putInt("clientOpenMode", ClientOpenMode.clientOpenModeDefault.ordinal());
        //bundle.putString("clientOpenParam", "您好，请问有什么需要帮助的吗？");

        //Context context = getApplicationContext();
        //Intent chatIntent = new Intent(context, ClientChatActivity.class);
        //chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //chatIntent.putExtras(bundle);
        //context.startActivity(chatIntent);
        // 进入会话界面(可使用上面的方式或者调用下面方法)，携带bundle(不加bundle参数则全部使用默认配置)
        V5ClientAgent.getInstance().startV5ChatActivityWithBundle(MyApplication.getContext(), bundle);

        /* 添加聊天界面监听器(非必须，有相应需求则添加) */
        // 界面生命周期监听[非必须]
        V5ClientAgent.getInstance().setChatActivityListener(QuestionActivity.this);
        // 消息发送监听[非必须]，可在此处向坐席透传来自APP客户的相关信息
        V5ClientAgent.getInstance().setUserWillSendMessageListener(new UserWillSendMessageListener() {

            @Override
            public V5Message onUserWillSendMessage(V5Message message) {
                // TODO 可在此处添加消息参数(JSONObject键值对均为字符串)，采集信息透传到坐席端（v1.2.0版本开始不建议使用此方式，除非有实时更新需求的自定义信息）
                // 【注意】v1.2.0以上版本建议使用V5ClientConfig的setUserInfo方法传递客户信息，可不必依附于消息
                //if (flag_userBrowseSomething) {
                //    JSONObject customContent = new JSONObject();
                //    try {
                //        customContent.put("用户级别", "VIP");
                //        customContent.put("用户积分", "300");
                //        customContent.put("来自应用", "ClientDemo");
                //    } catch (JSONException e) {
                //        e.printStackTrace();
                //    }
                //    message.setCustom_content(customContent);
                //
                //    flag_userBrowseSomething = false;
                //}
                return message; // 注：必须将消息对象以返回值返回
            }
        });

        /**
         * 点击链接监听
         * onURLClick返回值：是否消费了此点击事件，返回true则SDK内不再处理此事件，否则默认跳转到指定网页
         */
        V5ClientAgent.getInstance().setURLClickListener(new OnURLClickListener() {

            @Override
            public boolean onURLClick(Context context, V5ClientAgent.ClientLinkType type, String url) {
                // TODO Auto-generated method stub
                switch (type) {
                    case clientLinkTypeArticle: // 点击图文

                        break;
                    case clientLinkTypeURL: // 点击URL链接

                        break;

                    case clientLinkTypeEmail: // 点击电子邮件

                        break;

                    case clientLinkTypePhoneNumber: // 点击电话号码

                        break;
                }
                LogUtil.d("onURLClick:" + url);
                return false; // 是否消费了此点击事件
            }
        });

        /**
         * 点击对话输入框底部功能按钮
         */
        V5ClientAgent.getInstance().setChatActivityFuncIconClickListener(new ChatActivityFuncIconClickListener() {

            /**
             * Activity点击底部功能按钮事件，icon参数值及含义如下：
             * 		v5_icon_ques			//常见问题
             * 		v5_icon_relative_ques	//相关问题
             * 		v5_icon_photo			//图片
             * 		v5_icon_camera			//拍照
             * 		v5_icon_worker			//人工客服
             * 返回值代表是否消费了此事件
             * @param icon 点击的图标名称(对应SDK目录下res/values/v5_arrays中v5_chat_func_icon的值)
             * @return boolean 是否消费事件(返回true则不响应默认点击效果，由此回调处理)
             */
            @Override
            public boolean onChatActivityFuncIconClick(String icon) {
                // TODO Auto-generated method stub
                if (icon.equals("v5_icon_worker")) {
                    // 转到指定客服,参数：(组id, 客服id),参数为0则不指定客服组或者客服,获取组id请咨询客服
                    V5ClientAgent.getInstance().transferHumanService(0, 0);
                    // 返回true来拦截SDK内默认的实现
                    return true;
                }
                return false;
            }
        });
    }

    public class JsInteration {

        @JavascriptInterface
        public boolean checkCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(QuestionActivity.this, permissionArray);
            return cameraPermission;
        }

        @JavascriptInterface
        public void requestCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(QuestionActivity.this, permissionArray);
            if (!cameraPermission) {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE5, permissionArray, getString(R.string.permission_content_photo));
            }
        }

        @JavascriptInterface
        public boolean checkLocationPermission() {
            boolean locationPermission = PermissionUtils.checkPermissionArray(QuestionActivity.this, locationPermissionArray);
            return locationPermission;
        }

        @JavascriptInterface
        public void requestLocationPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(QuestionActivity.this, locationPermissionArray);
            if (!cameraPermission) {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE6, locationPermissionArray, getString(R.string.permission_content_location));
            }
        }

        /**
         * 跳转到微信
         */
        @JavascriptInterface
        public void getWechatApi() {
            try {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                ComponentName cmp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setComponent(cmp);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                MyToast.showToastLong("检查到您手机没有安装微信，请安装后使用该功能");
            }
        }
    }

    private void skipToWechat(String uriString) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri uri = Uri.parse(uriString);
        intent.setData(uri);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        if (mWvResgister.canGoBack()) {
            mWvResgister.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage && null == mUploadCallbackAboveL) return;
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (mUploadCallbackAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (mUploadMessage != null) {

                if (result != null) {
                    String path = getPath(getApplicationContext(),
                            result);
                    Uri uri = Uri.fromFile(new File(path));
                    mUploadMessage
                            .onReceiveValue(uri);
                } else {
                    mUploadMessage.onReceiveValue(imageUri);
                }
                mUploadMessage = null;

            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("url", url);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            String url = (String) savedInstanceState.getString("url");
        }
    }

    @SuppressWarnings("null")
    @TargetApi(Build.VERSION_CODES.BASE)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent data) {
        if (requestCode != FILECHOOSER_RESULTCODE
                || mUploadCallbackAboveL == null) {
            return;
        }
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (data == null) {
                results = new Uri[]{imageUri};
            } else {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();

                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        if (results != null) {
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        } else {
            results = new Uri[]{imageUri};
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        }

        return;
    }


    private void take() {
        File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyApp");
        // Create the storage directory if it does not exist
        if (!imageStorageDir.exists()) {
            imageStorageDir.mkdirs();
        }
        File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        imageUri = Uri.fromFile(file);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent i = new Intent(captureIntent);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            i.setPackage(packageName);
            i.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(i);
        }
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        QuestionActivity.this.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void setZoom(WebSettings settings) {
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
    }

    /* 会话界面生命周期和连接状态的回调 */
    @Override
    public void onChatActivityCreate(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityCreate>");
        activity.setChatTitle("客服");
    }

    @Override
    public void onChatActivityStart(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityStart>");
    }

    @Override
    public void onChatActivityStop(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityStop>");
    }

    @Override
    public void onChatActivityDestroy(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityDestroy>");
    }

    @Override
    public void onChatActivityConnect(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityConnect>");
        /*
         * 连接建立后才可以调用消息接口发送消息，以下是发送消息示例
         */
        // 【转】指定人工客服（调用时立即转），参数: 客服组id,客服id （以下数字仅作为示例，具体ID请前往V5后台查看客服信息）
        //V5ClientAgent.getInstance().transferHumanService(0, 132916);
        // 【指定人工客服】点击转人工按钮或者问题触发转人工时会转到指定人工，参数"0 132916"中两个数字先后对应需要转的客服组ID和客服ID
        //V5ClientAgent.getInstance().sendMessage(new V5ControlMessage(4, 2, "0 114052"), null);

        // 发送图文消息
//		V5ArticlesMessage articleMsg = new V5ArticlesMessage();
//		V5ArticleBean article = new V5ArticleBean(
//				"V5KF",
//				"http://rs.v5kf.com/upload/10000/14568171024.png",
//				"http://www.v5kf.com/public/weixin/page.html?site_id=10000&id=218833&uid=3657455033351629359",
//				"V5KF是围绕核心技术“V5智能机器人”研发的高品质在线客服系统。可以运用到各种领域，目前的主要产品有：微信智能云平台、网页智能客服系统...");
//		ArrayList<V5ArticleBean> articlesList = new ArrayList<V5ArticleBean>();
//		articlesList.add(article);
//		articleMsg.setArticles(articlesList);
//		V5ClientAgent.getInstance().sendMessage(articleMsg, null);
    }

    @Override
    public void onChatActivityReceiveMessage(ClientChatActivity activity, V5Message message) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityReceiveMessage> " + message.getDefaultContent(MyApplication.getContext()));
    }

    @Override
    public void onChatActivityServingStatusChange(ClientChatActivity activity, V5ClientAgent.ClientServingStatus status) {
        // TODO Auto-generated method stub
        switch (status) {
            case clientServingStatusRobot:
            case clientServingStatusQueue:
                activity.setChatTitle("机器人服务中");
                break;
            case clientServingStatusWorker:
                activity.setChatTitle(V5ClientConfig.getInstance(MyApplication.getContext()).getWorkerName() + "为您服务");
                break;
            case clientServingStatusInTrust:
                activity.setChatTitle("机器人托管中");
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWvResgister != null) {
            mWvResgister.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mWvResgister != null) {
            mWvResgister.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWvResgister != null) {
            mWvResgister.destroy();
        }
    }

    @Override
    public void finish() {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();
        super.finish();
    }
}
