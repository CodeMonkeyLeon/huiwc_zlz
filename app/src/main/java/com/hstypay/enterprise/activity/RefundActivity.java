package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ApiRes;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.RefundPwd;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class RefundActivity extends BaseActivity implements View.OnClickListener {


    private static final String TAG_QUERY_REFUND_PWD = "tag_query_refund_pwd";
    private ImageView mIvBack;
    private Button mBtnRefund;
    private TextView mButton, mTvTitle, mTvMark;
    private PayBean.DataBean mData;
    private TextView mTv_can_refund_money, mTv_mch_transNo, mTvRefundNotice, mTvCheckOrderDetail, mTvCount;
    private EditText mEt_refund_money, mEtOrderRemark;
    public static RefundActivity instance = null;
    private String mIntentName;
    private SafeDialog mLoadDialog;
    private TextView mTvWeixinDaijinTip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnRefund = (Button) findViewById(R.id.btn_submit);
        mTvMark = (TextView) findViewById(R.id.tv_mark);
        mTv_mch_transNo = (TextView) findViewById(R.id.tv_mch_transNo);
        mTv_can_refund_money = (TextView) findViewById(R.id.tv_can_refund_money);//可退金额
        mEt_refund_money = (EditText) findViewById(R.id.et_refund_money);//退款金额
        mTvRefundNotice = (TextView) findViewById(R.id.tv_bounty_refund_notice);
        mTvCheckOrderDetail = (TextView) findViewById(R.id.tv_check_order_detail);
        mEtOrderRemark = findViewById(R.id.et_refund_remark);
        mTvCount = (TextView) findViewById(R.id.tv_text_count);

        mTvTitle.setText(R.string.title_apply_refund);
        mButton.setVisibility(View.INVISIBLE);

        setButtonEnable(mBtnRefund, false);

        mTvWeixinDaijinTip = findViewById(R.id.tv_weixin_daijin_tip);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnRefund.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                MtaUtils.mtaId(RefundActivity.this, "C006");
                boolean containsMeituan = false;
                if (mData.getCouponProviderTypeList() != null && mData.getCouponProviderTypeList().size() > 0) {
                    for (Integer couponProviderType : mData.getCouponProviderTypeList()) {
                        if (couponProviderType == 1) {
                            containsMeituan = true;
                        }
                    }
                }
                if (containsMeituan) {
                    showCancelCoupon(getString(R.string.dialog_meituan_refund));
                } else {
                    long refundMoney = BigDecimal.valueOf(Utils.Double.tryParse(mEt_refund_money.getText().toString().trim(), -1)).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
                    if (mData.getMchDiscountsMoney() > 0 && mData.getPayMoney() != refundMoney) {
                        showCancelCoupon(getString(R.string.dialog_part_refund));
                    } else {
                        submit();
                    }
                }
            }
        });
        mTvCheckOrderDetail.setOnClickListener(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mData.getRefundStatus() != 6 || mData.isPay()) {
                    if (s.toString().contains(".")) {
                        if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                            s = s.toString().subSequence(0,
                                    s.toString().indexOf(".") + 3);
                            mEt_refund_money.setText(s);
                            mEt_refund_money.setSelection(mEt_refund_money.getText().toString().trim().length());
                        }
                    }
                    if (s.toString().trim().equals(".")) {
                        s = "0" + s;
                        mEt_refund_money.setText(s);
                        mEt_refund_money.setSelection(mEt_refund_money.getText().toString().trim().length());
                    }
                    if (s.toString().startsWith("0")
                            && s.toString().trim().length() > 1) {
                        if (!s.toString().substring(1, 2).equals(".")) {
                            mEt_refund_money.setText(s.toString().substring(1));
                            mEt_refund_money.setSelection(mEt_refund_money.getText().toString().trim().length());
                            return;
                        }
                    }
                    if (Utils.Double.tryParse(s.toString().trim(), 0) > ((mData.getPayMoney() - mData.getRefundMoney()) / 100d)) {
                        mEt_refund_money.setText(s.subSequence(0, s.length() - 1));
                        mEt_refund_money.setSelection(mEt_refund_money.getText().toString().trim().length());
                        MyToast.showToastShort(getString(R.string.regex_refund_money_large));
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mEt_refund_money.getText().toString().trim().length() > 0) {
                    setButtonEnable(mBtnRefund, true);
                } else {
                    setButtonEnable(mBtnRefund, false);
                }
            }
        };
        mEt_refund_money.addTextChangedListener(textWatcher);
        mEtOrderRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTvCount.setText((s.length()) + "/20");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void initData() {
        mData = (PayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_ORDER_REFUND);
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_CAPTURE_REFUND.equals(mIntentName)) {
            mTvCheckOrderDetail.setVisibility(View.VISIBLE);
        }
        if (mData != null) {
            if (Constants.INTENT_CAPTURE_REFUND.equals(mIntentName)) {
                if (mData.getTradeType() == 1 || (mData.getTradeState() != 2 && mData.getTradeState() != 4) || mData.getEditEnable() != 1) {
                    showCommonNoticeDialog(this, "该订单暂不支持退款", new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            finish();
                        }
                    });
                    return;
                }
                if (MyApplication.getIsCasher()) {
                    if (!StringUtils.isEmptyOrNull(MyApplication.getCashierRefund()) && !StringUtils.isEmptyOrNull(MyApplication.getPermissionsRefund())) {
                        if (mData.getApiProvider() == 10 || mData.getApiProvider() == 11 || mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {
                            if (!(Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))) {
                                showCommonNoticeDialog(this, "请在pos机上退款", new CommonNoticeDialog.OnClickOkListener() {
                                    @Override
                                    public void onClickOk() {
                                        finish();
                                    }
                                });
                                return;
                            }
                        }
                    } else {
                        showCommonNoticeDialog(this, "该订单暂无权限退款", new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                finish();
                            }
                        });
                        return;
                    }
                } else {
                    if (!StringUtils.isEmptyOrNull(MyApplication.getPermissionsRefund())) {
                        if (mData.getApiProvider() == 10 || mData.getApiProvider() == 11 || mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {
                            if (!(Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))) {
                                showCommonNoticeDialog(this, "请在pos机上退款", new CommonNoticeDialog.OnClickOkListener() {
                                    @Override
                                    public void onClickOk() {
                                        finish();
                                    }
                                });
                                return;
                            }
                        }
                    } else {
                        showCommonNoticeDialog(this, "该订单暂无权限退款", new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                finish();
                            }
                        });
                        return;
                    }
                }
            }
            if (MyApplication.getIsBounty() && mData.getRewardMoney() > 0) {
                mTvRefundNotice.setVisibility(View.VISIBLE);
            }
            if (!StringUtils.isEmptyOrNull(mData.getOrderNo())) {
                mTv_mch_transNo.setText(getString(R.string.tv_trade_num) + "：" + mData.getOrderNo());
            }
            if (mData.getMdiscount()>0){
                //使用了微信代金券的订单仅支持全额退款!
                mTvWeixinDaijinTip.setVisibility(View.VISIBLE);
                mTvWeixinDaijinTip.setText(getResources().getString(R.string.weixin_refund_real_money_tip)+(DateUtil.formatMoneyUtil(mData.getRealMoney() / 100d))+"元");
                mEt_refund_money.setEnabled(false);
                mEt_refund_money.setText(DateUtil.formatMoney((mData.getPayMoney() - mData.getRefundMoney()) / 100d));
            }else {
                mTvWeixinDaijinTip.setVisibility(View.GONE);
            }

            if (mData.getApiProvider() == 6 && mData.getCouponFee() > 0) {
                mEt_refund_money.setEnabled(false);
                mEt_refund_money.setText(DateUtil.formatMoney((mData.getPayMoney() - mData.getRefundMoney()) / 100d));
            }
            mEt_refund_money.setFocusable(true);
            mEt_refund_money.setFocusableInTouchMode(true);
            mEt_refund_money.requestFocus();
//            if (mData.getApiProvider() == 10 || mData.getApiProvider() == 11 || mData.getApiProvider() == 13 || mData.getApiProvider() == 15) {
                if (mData.getReverseFlag() == 1) {
                    if ((mData.getPayMoney() - mData.getRefundMoney()) <= 0) {
                        mTvMark.setVisibility(View.GONE);
                        setButtonEnable(mBtnRefund, false);
                    } else {
                        setButtonEnable(mBtnRefund, true);
                    }
                    mEt_refund_money.setEnabled(false);
                    mEt_refund_money.setText(DateUtil.formatMoney((mData.getPayMoney() - mData.getRefundMoney()) / 100d));
                    StringBuilder content = new StringBuilder();
                    content.append("该笔订单总额")
                            .append(DateUtil.formatMoneyUtil(mData.getPayMoney() / 100d));
                    if (mData.getRefundMoney() > 0) {
                        content.append("元，已申请退款")
                                .append(DateUtil.formatMoneyUtil(mData.getRefundMoney() / 100d))
                                .append("元，可退款金额")
                                .append(DateUtil.formatMoneyUtil((mData.getPayMoney() - mData.getRefundMoney()) / 100d));
                    }
                    content.append("元");
                    mTv_can_refund_money.setText(content.toString());
                } else {
                    if (mData.getRefundStatus() == 6 && !mData.isPay()) {
                        setButtonEnable(mBtnRefund, true);
                        mEt_refund_money.setEnabled(false);
                        mEt_refund_money.setText(DateUtil.formatMoney(mData.getRefundMoney() / 100d));
                        mTv_can_refund_money.setText("可退款金额"
                                + DateUtil.formatMoneyUtil(mData.getRefundMoney() / 100d)
                                + "元");
                    } else {
                        StringBuilder content = new StringBuilder();
                        content.append("该笔订单总额")
                                .append(DateUtil.formatMoneyUtil(mData.getPayMoney() / 100d));
                        if (mData.getRefundMoney() > 0) {
                            content.append("元，已申请退款")
                                    .append(DateUtil.formatMoneyUtil(mData.getRefundMoney() / 100d))
                                    .append("元，可退款金额")
                                    .append(DateUtil.formatMoneyUtil((mData.getPayMoney() - mData.getRefundMoney()) / 100d));
                        }
                        content.append("元");
                        if ((mData.getPayMoney() - mData.getRefundMoney()) <= 0) {
                            mTvMark.setVisibility(View.GONE);
                            mEt_refund_money.setEnabled(false);
                            mEtOrderRemark.setEnabled(false);
                            setButtonEnable(mBtnRefund, false);
                            mTv_can_refund_money.setText(content.toString());
                        } else {
                            content.append("，全额退款");
                            setTextClickable(mTv_can_refund_money, content.toString());
                        }
                    }
                }
        }
    }

    private void setTextClickable(TextView textView, String content) {
        SpannableStringBuilder spannableString = new SpannableStringBuilder();
        spannableString.append(content);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                mEt_refund_money.setText(DateUtil.formatMoney((mData.getPayMoney() - mData.getRefundMoney()) / 100d));
                mEt_refund_money.setSelection(mEt_refund_money.getText().toString().trim().length());
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(MyApplication.getContext(), R.color.theme_color));            //设置可以点击文本部分的颜色
                ds.setUnderlineText(false);            //设置该文本部分是否显示超链接形式的下划线
                ds.clearShadowLayer();
            }
        };
        spannableString.setSpan(clickableSpan, content.length() - 4, content.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_check_order_detail:
                Intent intent = new Intent();
                intent.putExtra(Constants.INTENT_BILL_DATA, mData);
                intent.putExtra(Constants.INTENT_NAME, mIntentName);
                intent.setClass(RefundActivity.this, PayDetailActivity.class);
                startActivity(intent);
                if (!Constants.INTENT_CAPTURE_REFUND.equals(mIntentName)) {
                    finish();
                }
                break;
            default:
                break;
        }
    }

    private void showCancelCoupon(String title) {
        SelectDialog selectDialog = new SelectDialog(RefundActivity.this, title
                , getString(R.string.btnCancel)
                , getString(R.string.btnContinue), R.layout.select_common_dialog);
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                submit();
            }
        });
        selectDialog.show();
    }

    private void submit() {
        if (!((mData.getApiProvider() == 10 || mData.getApiProvider() == 11 || mData.getApiProvider() == 13 || mData.getApiProvider() == 15) && (mData.getReverseFlag() == 1 || (mData.getRefundStatus() == 6 && !mData.isPay())))) {
            if (!StringUtils.isNumberData(mEt_refund_money.getText().toString().trim()) || Utils.Double.tryParse(mEt_refund_money.getText().toString().trim(), -1) <= 0) {
                MyToast.showToastShort(getString(R.string.regex_number_refund_money));
                return;
            }
            long refundMoney = BigDecimal.valueOf(Utils.Double.tryParse(mEt_refund_money.getText().toString().trim(), -1)).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
            if (refundMoney > (mData.getPayMoney() - mData.getRefundMoney())) {
                MyToast.showToastShort(getString(R.string.regex_refund_money_large));
                return;
            }
        }
        queryRefundPwd();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    /**
     * 查询退款密码
     */
    private void queryRefundPwd() {
        DialogUtil.safeShowDialog(mLoadDialog);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", mData.getStoreMerchantId());
            ServerClient.newInstance(this).queryRefundPwd(this, TAG_QUERY_REFUND_PWD, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefund(NoticeEvent event) {
        if (event.getTag().equals(TAG_QUERY_REFUND_PWD)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            if (event.getCls().equals(Constants.ON_EVENT_TRUE)) {
                ApiRes<RefundPwd> res = (ApiRes<RefundPwd>) event.getMsg();
                RefundPwd refundPwd = res.getData();
                long refundMoney = BigDecimal.valueOf(Utils.Double.tryParse(mEt_refund_money.getText().toString().trim(), -1)).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
                Intent intent = new Intent(RefundActivity.this, CheckActivity.class);
                intent.putExtra(Constants.INTENT_ORDER_REFUND, mData)
                        .putExtra(Constants.INTENT_REFUND_MONEY, refundMoney)
                        .putExtra(CheckActivity.KEY_REFUND_PWD, refundPwd.getRefundCheckType() == 2)
                        .putExtra(Constants.INTENT_REMARK, mEtOrderRemark.getText().toString().trim());
                startActivity(intent);
            } else {
                failHandler(event);
            }
        }
    }
}
