package com.hstypay.enterprise.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.dialog.CustomViewFullScreenDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.ProductData;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.CreateOneDiCodeUtil;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.ums.upos.sdk.exception.SdkException;
import com.ums.upos.sdk.system.BaseSystemManager;
import com.ums.upos.sdk.system.OnServiceStatusListener;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * 交易详情
 * Created by admin on 2017/7/3.
 */
public class RefundDetailActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvTradeBarCode;
    private Button mBtnPrint;
    private ScrollView mSvContent;
    private String mIntentName;
    private TextView mButton, mTvTitle, mTvRefundMoney, mTvTradeState, mTvRefundDate, mTvRefundPerson, mTvRefundOverDate, mTvTradeCodeNumber, mTvRefundOrderNo, mTvRefundStore, mTvRefundRemark;
    private LinearLayout mLlRefundOverDate, mLlRefundOrderNo, mLlRefundPerson, mLlRefundRemark;
    private RelativeLayout mRlToOrder;

    private PayBean.DataBean mPayData;
    private TradeDetailBean mData;
    private SafeDialog mLoadDialog;
    public static RefundDetailActivity instance = null;
    private Printer printer;
    private PosPrintUtil mPosPrintUtil;
    private IWoyouService woyouService;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;

    private AidlPrinter printerDev = null;
    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private String otherType;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;
    private TextView mTvWeixinDiscountTip;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private ArrayList<ProductData> datas;
    private int count = 1;
    private Handler mHandler = new Handler();
    private LinearLayout mLlRefundDesc;//退款描述
    private TextView mTvRefundDesc;//退款描述
    private Button mBtnCancelRefund;//撤销退款
    private View mViewHeight;//间隔

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service

    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    /**
     * 绑定商米打印服务
     */
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        initView();
        initData();
        initListener();
        initPosPrint();

    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }
        // 移除所有消息
//        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                //联迪银商
            /*mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {*/
                try {
                    BaseSystemManager.getInstance().deviceServiceLogin(
                            RefundDetailActivity.this, null, "99999998",//设备ID，生产找后台配置
                            new OnServiceStatusListener() {
                                @Override
                                public void onStatus(int arg0) {//arg0可见ServiceResult.java
                                    if (0 == arg0 || 2 == arg0 || 100 == arg0) {//0：登录成功，有相关参数；2：登录成功，无相关参数；100：重复登录。

                                    }
                                }
                            });
                } catch (SdkException e) {
                    e.printStackTrace();
                }
            }
//                }
//            }, 500);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                try {
                    BaseSystemManager.getInstance().deviceServiceLogout();
                } catch (SdkException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mRlToOrder.setOnClickListener(this);
        mBtnCancelRefund.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_refund_detail);
        mButton.setVisibility(View.INVISIBLE);

        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mTvRefundMoney = (TextView) findViewById(R.id.tv_refund_money);//退款金额
        mTvTradeState = (TextView) findViewById(R.id.tv_trade_state);//交易状态
        mTvRefundDate = (TextView) findViewById(R.id.tv_refund_date);//退款发起时间
        mTvRefundOrderNo = (TextView) findViewById(R.id.tv_refund_order_no);//退款单号
        mLlRefundOrderNo = (LinearLayout) findViewById(R.id.ll_refund_order_no);//退款单号
        mLlRefundPerson = findViewById(R.id.ll_refund_person);//退款发起人
        mTvRefundPerson = (TextView) findViewById(R.id.tv_refund_person);//退款发起人
        mTvRefundStore = (TextView) findViewById(R.id.tv_refund_store);//退款门店
        mTvRefundOverDate = (TextView) findViewById(R.id.tv_refund_over_date);//退款完成时间
        mLlRefundOverDate = (LinearLayout) findViewById(R.id.ll_refund_over_date);//退款完成时间title
        mLlRefundRemark = (LinearLayout) findViewById(R.id.ll_refund_remark);//退款备注
        mTvRefundRemark = findViewById(R.id.tv_refund_remark);//退款备注
        mRlToOrder = (RelativeLayout) findViewById(R.id.rl_to_order);
        mIvTradeBarCode = (ImageView) findViewById(R.id.iv_trade_barcode);//条形码
        mTvTradeCodeNumber = (TextView) findViewById(R.id.tv_trade_code_number);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mTvWeixinDiscountTip = findViewById(R.id.tv_weixin_discount_tip); //微信代金券提示语
        mLlRefundDesc = findViewById(R.id.ll_refund_desc); //ll退款描述
        mTvRefundDesc = findViewById(R.id.tv_refund_desc);//退款描述
        mBtnCancelRefund = findViewById(R.id.btn_cancel_refund);//撤销退款按钮
        mViewHeight = findViewById(R.id.view_height);//间隔

        setButtonWhite(mBtnPrint);
        setButtonEnable(mBtnCancelRefund, true);

    }

    public void initData() {
        getPrintActive();
        mData = (TradeDetailBean) getIntent().getSerializableExtra(Constants.INTENT_TRADE_DETAIL);
        mPayData = (PayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_BILL_DATA);
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (mData != null) {
            setView(mData);
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void setView(TradeDetailBean data) {

        //退款单号
        if (!TextUtils.isEmpty(data.getRefundNo())) {
            mLlRefundOrderNo.setVisibility(View.VISIBLE);
            mTvRefundOrderNo.setText(data.getRefundNo());
        } else {
            mLlRefundOrderNo.setVisibility(View.GONE);
        }
        //退款状态
        int refundStatus = data.getRefundStatus();
        if (isRefunding(refundStatus)) {
            mTvTradeState.setText("退款中");
            mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
        } else if (refundStatus == 2) {
            mTvTradeState.setText("退款成功");
            mTvTradeState.setTextColor(UIUtils.getColor(R.color.tv_status_green));
            mLlRefundOverDate.setVisibility(View.VISIBLE);
        } else if (isRefundFail(data, refundStatus)) {
            mTvTradeState.setText("退款失败");
            mTvTradeState.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
        } else if (refundStatus == 5) {
            mTvTradeState.setText("转入代发");
            mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_btn_text));
        }
        //退款描述
        if (isRefunding(refundStatus) || isRefundFail(data, refundStatus)) {
            //是退款中或退款失败
            String ptRefundReason = data.getPtRefundReason();
            if (TextUtils.isEmpty(ptRefundReason)) {
                mLlRefundDesc.setVisibility(View.GONE);
            } else {
                mLlRefundDesc.setVisibility(View.VISIBLE);
                mTvRefundDesc.setText(ptRefundReason);
            }
        } else {
            mLlRefundDesc.setVisibility(View.GONE);
        }


        //如果是退款中隐藏退款完成时间与退款金额
        if (refundStatus == 1) {
            mTvRefundOverDate.setVisibility(View.GONE);
        }
        //退款完成时间
        if (!StringUtils.isEmptyOrNull(data.getRefundUpdateTime())) {
            mTvRefundOverDate.setText(data.getRefundUpdateTime());
        }
        //退款发起时间
        if (!StringUtils.isEmptyOrNull(data.getRefundTime())) {
            mTvRefundDate.setText(data.getRefundTime());
        }
        //退款发起人
        if (!StringUtils.isEmptyOrNull(data.getRefundUser())) {
            mTvRefundPerson.setText(data.getRefundUser());
        } else {
            mLlRefundPerson.setVisibility(View.GONE);
        }
        //门店
        if (!StringUtils.isEmptyOrNull(data.getStoreMerchantIdCnt())) {
            mTvRefundStore.setText(data.getStoreMerchantIdCnt());
        }
        //退款备注
        if (!StringUtils.isEmptyOrNull(data.getStandby4())) {
            mLlRefundRemark.setVisibility(View.VISIBLE);
            mTvRefundRemark.setText(data.getStandby4());
        }
        //退款金额
        mTvRefundMoney.setVisibility(View.VISIBLE);
        mTvRefundMoney.setText(getString(R.string.tx_mark_coupon) + " " + DateUtil.formatMoneyUtil(data.getRefundMoney() / 100d));
        //微信代金券
        if (mPayData != null && mPayData.getMdiscount() > 0) {
            mTvWeixinDiscountTip.setVisibility(View.VISIBLE);
            mTvWeixinDiscountTip.setText("原订单使用微信代金券，原退款操作" + DateUtil.formatMoneyUtil(data.getPayMoney() / 100d) + "元，顾客到账" + DateUtil.formatMoneyUtil(data.getRefundMoney() / 100d) + "元");
        }

        //一维码
        if (!StringUtils.isEmptyOrNull(data.getRefundNo())) {
            mIvTradeBarCode.setVisibility(View.VISIBLE);
            mTvTradeCodeNumber.setVisibility(View.VISIBLE);
            WindowManager wm = this.getWindowManager();
            int width = wm.getDefaultDisplay().getWidth();
            final int w = (int) (width * 0.85);
            mIvTradeBarCode.setImageBitmap(CreateOneDiCodeUtil.createCode(data.getRefundNo(), w, 180));
            mTvTradeCodeNumber.setText(data.getRefundNo());
        }
        if (data.getApiProvider() == 10 && (refundStatus == 1 || refundStatus == 3 || refundStatus == 4 || refundStatus == 6)) {
            mBtnPrint.setVisibility(View.INVISIBLE);
        } else {
            mBtnPrint.setVisibility(View.VISIBLE);
        }

        if (data.getRefundStatus() == 6 && data.getApiProvider() == 10 &&
                (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))) {
            mButton.setVisibility(View.VISIBLE);
            mButton.setText("重新退款");
        } else {
            mButton.setVisibility(View.INVISIBLE);
        }
        if (Constants.INTENT_CAPTURE_REFUND.equals(mIntentName)) {
            mButton.setVisibility(View.INVISIBLE);
            mBtnPrint.setVisibility(View.INVISIBLE);
        }

        if (canCancelRefund(data)) {
            //可以发起撤销退款
            if (MyApplication.getIsCasher()) {
                if (!StringUtils.isEmptyOrNull(MyApplication.getCashierRefund()) && !StringUtils.isEmptyOrNull(MyApplication.getPermissionsRefund())) {
                    //有权限撤销退款
                    mBtnCancelRefund.setVisibility(View.VISIBLE);
                    mViewHeight.setVisibility(View.GONE);
                } else {
                    mBtnCancelRefund.setVisibility(View.GONE);
                    mViewHeight.setVisibility(View.VISIBLE);
                }
            } else {
                if (!StringUtils.isEmptyOrNull(MyApplication.getPermissionsRefund())) {
                    //有权限撤销退款
                    mBtnCancelRefund.setVisibility(View.VISIBLE);
                    mViewHeight.setVisibility(View.GONE);
                } else {
                    mBtnCancelRefund.setVisibility(View.GONE);
                    mViewHeight.setVisibility(View.VISIBLE);
                }
            }
        } else {
            mBtnCancelRefund.setVisibility(View.GONE);
            mViewHeight.setVisibility(View.VISIBLE);
        }

    }

    //是否可以发起撤销退款
    private boolean canCancelRefund(TradeDetailBean data) {
        if (((data.getRefundStatus() == 6 && data.getPtAuditStatus() == 1) || data.getRefundStatus() == 1)) {
            //（退款状态为处理中 && 平台审核状态为待审核）|| 退款状态为初始状态
            return true;
        }
        return false;
    }

    //是否是退款失败
    private boolean isRefundFail(TradeDetailBean data, int refundStatus) {
        return data.getApiProvider() != 6 && refundStatus == 3;
    }

    //是否是退款中
    private boolean isRefunding(int refundStatus) {
        return refundStatus == 1 || refundStatus == 4 || refundStatus == 6;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if (mData != null) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.RESULT_CASHIER_INTENT, mData.getTradeState());   //传递一个user对象列表
                    intent.putExtra(Constants.RESULT_ORDER_NO, mData.getOrderNo());
                    setResult(RESULT_OK, intent);
                }
                finish();
                break;
            case R.id.blue_print:
                getPrint();
                break;
            case R.id.rl_to_order:
                if (Constants.INTENT_TRADE_DETAIL.equals(mIntentName)) {
                    startOrderActivity(mPayData);
                } else {
                    getOrderDetail(mData.getOrderNo());
                }
                break;
            case R.id.button_title:
                refund(mPayData);
                break;
            case R.id.btn_cancel_refund:
                showCancelRefundTip();
                break;
            default:
                break;
        }
    }


    //撤销退款确认弹框
    private void showCancelRefundTip() {
        CustomViewFullScreenDialog dialog = new CustomViewFullScreenDialog(this);
        dialog.setView(R.layout.dialog_edit_cancel_confirm_refund);
        EditText et_reason_refund_dialog = dialog.findViewById(R.id.et_reason_refund_dialog);
        Button btn_cancel_refund_dialog = dialog.findViewById(R.id.btn_cancel_refund_dialog);
        Button btn_submit_refund_dialog = dialog.findViewById(R.id.btn_submit_refund_dialog);
        TextView tv_counter_reason_refund = dialog.findViewById(R.id.tv_counter_reason_refund);//计数
        et_reason_refund_dialog.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                tv_counter_reason_refund.setText("" + length + "/30");
            }
        });
        btn_cancel_refund_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_submit_refund_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = et_reason_refund_dialog.getText().toString().trim();
                if (reason == null || reason.length() < 1) {
                    ToastUtil.showToastShort(getResources().getString(R.string.input_cancel_refund_reason));
                    return;
                }
                dialog.dismiss();
                cancelRefund(reason);
            }
        });
        dialog.show();

    }


    /**
     * 撤销退款
     *
     * @param reason 原因
     */
    private void cancelRefund(String reason) {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap();
            map.put("ptRefundReason", reason);
            map.put("refundNo", mData.getRefundNo());
            ServerClient.newInstance(MyApplication.getContext()).cancelRefund(MyApplication.getContext(), Constants.TAG_CANCEL_REFUND, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void refund(PayBean.DataBean billData) {
        PayBean.DataBean dataBean = billData;
        dataBean.setRefundMoney(mData.getRefundMoney());
        dataBean.setOutRefundNo(mData.getOutTradeNo());
        dataBean.setRefundNo(mData.getRefundNo());
        dataBean.setPay(false);
        dataBean.setRefundStatus(mData.getRefundStatus());
        dataBean.setOriRefNo(mPayData.getRefNo());
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_ORDER_REFUND, dataBean);
        intent.setClass(RefundDetailActivity.this, RefundActivity.class);
        startActivity(intent);
    }

    private void getPrint() {
        MtaUtils.mtaId(RefundDetailActivity.this, "C003");
        mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mData, false);
    }

    //Eventbus接收数据订单状态
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(RefundDetailActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(RefundDetailActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_ORDER_FROM_REFUND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(RefundDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(RefundDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        startOrderActivity(data);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CANCEL_REFUND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            BaseBean msg = (BaseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(RefundDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(RefundDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    MyToast.showToastShort("撤销退款成功");
                    finish();
                    break;
            }
        }
    }

    private void startOrderActivity(PayBean.DataBean data) {
        finish();
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_BILL_DATA, data);
        intent.putExtra(Constants.INTENT_NAME, mIntentName);
        intent.setClass(RefundDetailActivity.this, PayDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_BCARD_PAY) {
            if (data != null) {
                String message = data.getStringExtra("message");
                String responseCode = data.getStringExtra("responseCode");
                if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                    showCommonNoticeDialog(RefundDetailActivity.this, message);
                }
            }
        }
        if (requestCode == Constants.REQUEST_BCARD_PRINT) {
            if (data != null) {
                String message = data.getStringExtra("message");
                String responseCode = data.getStringExtra("responseCode");
                if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                    showCommonNoticeDialog(RefundDetailActivity.this, message);
                }
            }
        }
        if (requestCode == Constants.REQUEST_BCARD_REPRINT) {
            if (data != null) {
                String message = data.getStringExtra("message");
                String responseCode = data.getStringExtra("responseCode");
                if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                    showCommonNoticeDialog(RefundDetailActivity.this, message);
                }
            }
        }
    }

    private void getOrderDetail(String orderNo) {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap();
            map.put("orderNo", orderNo);
            ServerClient.newInstance(MyApplication.getContext()).getOrderDetail(MyApplication.getContext(), Constants.TAG_ORDER_FROM_REFUND, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }
}
