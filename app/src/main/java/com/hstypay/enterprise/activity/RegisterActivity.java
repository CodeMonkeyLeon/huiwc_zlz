package com.hstypay.enterprise.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.AndroidBug5497Workaround;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.HtmlMTABean;
import com.hstypay.enterprise.bean.SaveImageBean;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.ImageUtil;
import com.hstypay.enterprise.utils.LocationUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import okhttp3.Call;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/27 10:33
 * @描述: ${TODO}
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private WebView mWvResgister;
    private ImageView mIvBack, mIvClose;
    private TextView mTvTitlte;

    private ValueCallback<Uri> mUploadMessage;// 表单的数据信息
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    private final static int FILECHOOSER_RESULTCODE = 1;// 表单的结果回调</span>
    private Uri imageUri;
    private String url;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private String[] storgeDermissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String[] locationPermissionArray = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE3 = 1003;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE5 = 1005;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 1006;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE7 = 1007;
    private ProgressBar mPg;
    private String fromUrl;
    private boolean isAllowClose, isAlwaysAllowClose, isAllowCache;
    private List<Integer> mFailedUrl;
    private int count;
    private String jsonData;
    private String method;
    private String picUrl = "";
    private String mPhoto;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        AndroidBug5497Workaround.assistActivity(this);
        fromUrl = getIntent().getStringExtra(Constants.REGISTER_INTENT_URL);
        isAllowClose = getIntent().getBooleanExtra(Constants.REGISTER_ALLOW_CLOSE, false);
        isAlwaysAllowClose = getIntent().getBooleanExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, false);
        isAllowCache = getIntent().getBooleanExtra(Constants.REGISTER_ALLOW_CACHE, false);
        if (savedInstanceState != null) {
            url = savedInstanceState.getString("url");
        } else {
            url = getIntent().getStringExtra(Constants.REGISTER_INTENT);
        }
        if (Constants.URL_REGISTER_FIRST.equals(url)) {
            SpUtil.removeKey(Constants.MERCHANT_NAME);
        }
        LogUtil.d("RegisterActivity oncreate" + url);
        if (url == null) {
            return;
        }
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvClose = (ImageView) findViewById(R.id.iv_close);
        mTvTitlte = (TextView) findViewById(R.id.tv_title);
        mWvResgister = (WebView) findViewById(R.id.wv_register);
        mPg = (ProgressBar) findViewById(R.id.progressBar);
        mIvBack.setOnClickListener(this);
        mIvClose.setOnClickListener(this);

        if (AppHelper.getAndroidSDKVersion() < 17) {
            mWvResgister.removeJavascriptInterface("searchBoxjavaBridge_");//解决 CVE-2014-1939 漏洞
            mWvResgister.removeJavascriptInterface("accessibility");//解决  CVE-2014-7224漏洞
            mWvResgister.removeJavascriptInterface("accessibilityTraversal");//解决  CVE-2014-7224漏洞
        }
        if (isAlwaysAllowClose) {
            mIvClose.setVisibility(View.VISIBLE);
        }
        WebSettings webSettings = mWvResgister.getSettings();
        setZoom(webSettings);
        //定位权限
        webSettings.setDatabaseEnabled(true);
        //设置定位的数据库路径
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setGeolocationDatabasePath(dir);
        webSettings.setGeolocationEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
        webSettings.setDomStorageEnabled(true);

//        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccess(false);
        webSettings.setAllowFileAccessFromFileURLs(false);
        webSettings.setAllowUniversalAccessFromFileURLs(false);
        //webSettings.setSavePassword(false);

        String ua = webSettings.getUserAgentString();
        webSettings.setUserAgentString(ua + AppHelper.getUserAgent(MyApplication.getContext()));
        //设置为可调用js方法
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        if (!isAllowCache) {
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        }
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        webSettings.setJavaScriptEnabled(true);
        webSettings.setBlockNetworkImage(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        mWvResgister.requestFocus();
        mWvResgister.addJavascriptInterface(new JsInteration(), "android");
        mWvResgister.addJavascriptInterface(new AndroidShare(), "androidShare");

        setWebChromeClient(mWvResgister);

        String[] cookies = SpUtil.getString(MyApplication.getContext(), Constants.LOGIN_COOKIE, "").split("<<<->>>");
        if (cookies != null && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                setCookie(url, cookies[i]);
            }
        }
        LogUtil.d("Jeremy RegisterActivity url=" + url);
        mWvResgister.loadUrl(url);

        mWvResgister.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //mWvResgister.loadUrl(url);
                if (!isAlwaysAllowClose) {
                    mIvClose.setVisibility(isAllowClose ? View.VISIBLE : View.INVISIBLE);
                }
                LogUtil.d("RegisterActivity url=" + url);
                RegisterActivity.this.url = url;
                if (url.contains("WebH5/register/success.html")
                        || url.contains("WebH5/opinion_feedback/submit_success.html")) {
                    mIvBack.setVisibility(View.GONE);
                } else {
                    mIvBack.setVisibility(View.VISIBLE);
                }
                //调用拨号程序
                if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:")) {
                    if (AppHelper.getAppType() == 2) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                        return true;
                    } else if (AppHelper.getAppType() == 1) {
                        return true;
                    }
                }
                return false;
            }

            //华为mate9保时捷版打不开
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // super.onReceivedSslError(view, handler, error);
                handler.proceed();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                LogUtil.d("title===", view.getTitle());
                if (Constants.LOAN_TO_REGISTER.equals(fromUrl)) {
                    mTvTitlte.setText("汇贷款");
                } else {
                    mTvTitlte.setText(view.getTitle());
                }
            }
        });
        setOnLongClickListener(mWvResgister);
    }

    private void setOnLongClickListener(final WebView webView) {
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final WebView.HitTestResult hitTestResult = webView.getHitTestResult();
                if (hitTestResult.getType() == WebView.HitTestResult.IMAGE_TYPE ||
                        hitTestResult.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
                    picUrl = hitTestResult.getExtra();//获取图片链接
                    showOnLongSaveDialog("保存图片到本地");
                    return true;
                } else {
                    return false;//保持长按可以复制文字
                }
            }
        });
    }

    public void setWebChromeClient(WebView wvResgister) {
        wvResgister.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mPg.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    mPg.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    mPg.setProgress(newProgress);//设置进度值
                }
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                LogUtil.d("title===", title);
                if (Constants.LOAN_TO_REGISTER.equals(fromUrl)) {
                    mTvTitlte.setText("汇贷款");
                } else {
                    mTvTitlte.setText(title);
                }
            }

            @Override
            public boolean onShowFileChooser(WebView webView,
                                             ValueCallback<Uri[]> filePathCallback,
                                             FileChooserParams fileChooserParams) {
                mUploadCallbackAboveL = filePathCallback;
                take();
                return true;
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                take();
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);

            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    downLoadImage(jsonData);
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE3:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ImageUtil.saveAssetsImage(MyApplication.getContext(), mPhoto+".jpg");;
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE7:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    saveImageData(picUrl);
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE5:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE6:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_location));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void setCookie(String url, String stringCookie) {
        CookieSyncManager.createInstance(MyApplication.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeSessionCookies(null);
            cookieManager.setAcceptThirdPartyCookies(mWvResgister, true);
            cookieManager.flush();
        } else {
            cookieManager.removeSessionCookie();
            cookieManager.setAcceptCookie(true);
            CookieSyncManager.getInstance().sync();
        }
        cookieManager.setAcceptCookie(true);
        cookieManager.setCookie(url, stringCookie);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                WebBackForwardList webBackForwardList = mWvResgister.copyBackForwardList();
                LogUtil.d("RegisterActivity--" + webBackForwardList.getSize());
                if (mWvResgister.canGoBack()) {
                    LogUtil.d("RegisterActivity--" + true);
                    if (Constants.LOAN_TO_REGISTER.equals(fromUrl)) {
                        finish();
                    } else {
                        mWvResgister.goBack();
                    }
                } else {
                    LogUtil.d("RegisterActivity--" + false);
                    if (Constants.WELCOME_TO_REGISTER.equals(fromUrl)) {
                        int autoStatus = SpUtil.getInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, 0);
                        if (autoStatus == 2) {
                            startActivity(new Intent(this, MainActivity.class));
                            finish();
                        } else {
                            finish();
                        }
                    } else if (Constants.WELCOME_TO_REGISTER_SECOND.equals(fromUrl)) {
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else {
                        finish();
                    }
                }
                break;
            case R.id.iv_close:
                finish();
                break;
            default:
                break;
        }
    }

    public class JsInteration {

        @JavascriptInterface
        public void refreshPageTitle(String title) {
            mTvTitlte.setText(title);
        }

        @JavascriptInterface
        public void saveImageToPhotosAlbum(String json) {
            /*try {
                String callback = json.getString("callback");
                LogUtil.d("传值=="+callback);
                JSONArray file = json.getJSONArray("file");
                for (int i =0;i<file.length();i++){
                    LogUtil.d("传值=="+(String) file.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            jsonData = json;
            boolean results = PermissionUtils.checkPermissionArray(RegisterActivity.this, storgeDermissionArray);
            if (results) {
                downLoadImage(json);
            } else {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, storgeDermissionArray, getString(R.string.permission_content_storage));
            }
        }

        @JavascriptInterface
        public void completeRegister() {
            if (url.equals(Constants.URL_REGISTER_FIRST)) {
                RegisterActivity.this.finish();
            } else {
                RegisterActivity.this.startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                RegisterActivity.this.finish();
                MyApplication.getInstance().finishAllActivity();
            }
        }

        @JavascriptInterface
        public void completeSubmit() {
            RegisterActivity.this.finish();
        }

        @JavascriptInterface
        public boolean checkCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray);
            LogUtil.d("checkCameraPermission==="+cameraPermission);
            return cameraPermission;
        }

        @JavascriptInterface
        public void requestCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray);
            LogUtil.d("checkCameraPermission===11111");
            if (!cameraPermission) {
                LogUtil.d("checkCameraPermission===2222");
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE5, permissionArray, getString(R.string.permission_content_photo));
            }
        }

        @JavascriptInterface
        public boolean checkLocationPermission() {
            boolean locationPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, locationPermissionArray);
            return locationPermission;
        }

        @JavascriptInterface
        public void requestLocationPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, locationPermissionArray);
            if (!cameraPermission) {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE6, locationPermissionArray, getString(R.string.permission_content_location));
            }
        }

        @JavascriptInterface
        public void mtaFunction(String json) {
            if (TextUtils.isEmpty(json))
                return;
            Gson gson = new Gson();
            HtmlMTABean htmlMTABean = gson.fromJson(json, HtmlMTABean.class);
            if (htmlMTABean != null) {
                if (htmlMTABean.getProps() != null && htmlMTABean.getProps().size() > 0) {
                    Properties prop = new Properties();
                    for (int i = 0; i < htmlMTABean.getProps().size(); i++) {
                        HtmlMTABean.PropsEntity propsEntity = htmlMTABean.getProps().get(i);
                        prop.setProperty(propsEntity.getKey(), propsEntity.getValue());
                    }
                    MtaUtils.mtaPro(RegisterActivity.this, htmlMTABean.getId(), prop);
                } else {
                    MtaUtils.mtaId(RegisterActivity.this, htmlMTABean.getId());
                }
            }
        }

        @JavascriptInterface
        public void requestLocation() {
            LocationUtil locationUtil = new LocationUtil();
            locationUtil.setOnClickOkListener(new LocationUtil.OnLocationListener() {
                @Override
                public void location(double lat, double lng) {

                }
            });
        }

        /**
         * 跳转到微信
         */
        @JavascriptInterface
        public void getWechatApi() {
            try {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                ComponentName cmp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setComponent(cmp);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // TODO: handle exception
                MyToast.showToastLong("检查到您手机没有安装微信，请安装后使用该功能");
            }
        }

        @JavascriptInterface
        public void setCacheInfo(String param) {
            LogUtil.d("cache==1="+param);
            String merchantId = "";
            String data = "";
            try {
                JSONObject jsonObject = new JSONObject(param);
                merchantId = jsonObject.optString("key");
                data = jsonObject.optString("data");
                SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CACHE_INFO + merchantId, data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public String getCacheInfo(String param) {
            LogUtil.d("cache==3="+param);
            String merchantId = "";
            try {
                JSONObject jsonObject = new JSONObject(param);
                merchantId = jsonObject.optString("key");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LogUtil.d("cache==2="+SpStayUtil.getString(MyApplication.getContext(), Constants.SP_CACHE_INFO + merchantId, ""));
            return SpStayUtil.getString(MyApplication.getContext(), Constants.SP_CACHE_INFO + merchantId, "");
        }

        @JavascriptInterface
        public void cleanCacheInfo(String param) {
            String merchantId = "";
            try {
                JSONObject jsonObject = new JSONObject(param);
                merchantId = jsonObject.optString("key");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SpStayUtil.removeKey(Constants.SP_CACHE_INFO + merchantId);
        }

        @JavascriptInterface
        public void downloadPhoto(String photo) {
            mPhoto = photo;
            try {
                boolean results = PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray);
                if (results) {
                    ImageUtil.saveAssetsImage(MyApplication.getContext(), photo+".jpg");
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE3, permissionArray, getString(R.string.permission_content_storage));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class AndroidShare {

        @JavascriptInterface
        public void callScan(String method) {
            try {
                JSONObject jsonObject = new JSONObject(method);
                RegisterActivity.this.method = jsonObject.optString("callback");
                Intent intent = new Intent(RegisterActivity.this, CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_H5_SCAN);
                intent.putExtra(Constants.INTENT_H5_SCAN, jsonObject.optString("text"));
                startActivityForResult(intent, Constants.REQUEST_H5_SCAN);
                LogUtil.d("jsonObject.optString_text_)" + jsonObject.optString("text"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void quitApp() {
            RegisterActivity.this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (mWvResgister.canGoBack()) {
            mWvResgister.goBack();
        } else {
            if (Constants.WELCOME_TO_REGISTER.equals(fromUrl)) {
                int autoStatus = SpUtil.getInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, 0);
                if (autoStatus == 2) {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                } else {
                    finish();
                }
            } else if (Constants.WELCOME_TO_REGISTER_SECOND.equals(fromUrl)) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage && null == mUploadCallbackAboveL) return;
            Uri result = (data == null || resultCode != RESULT_OK) ? null : data.getData();
            if (mUploadCallbackAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (mUploadMessage != null) {
                if (result != null) {
                    String path = getPath(getApplicationContext(), result);
                    Uri uri = Uri.fromFile(new File(path));
                    mUploadMessage.onReceiveValue(uri);
                } else {
                    mUploadMessage.onReceiveValue(imageUri);
                }
                mUploadMessage = null;
            }
        }
        if (requestCode == Constants.REQUEST_H5_SCAN) {//扫一扫
            if (data == null)
                return;
            String contentInfo = data.getStringExtra(Constants.RESULT_H5_SCAN);
            mWvResgister.loadUrl("javascript:" + method + "('" + contentInfo + "')");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        LogUtil.i("RegisterActivity", "ThirdActivity onSaveInstanceState");
        outState.putString("url", url);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        LogUtil.i("RegisterActivity", "ThirdActivity onRestoreInstanceState");
        if (savedInstanceState != null) {
            String url = (String) savedInstanceState.getString("url");
        }
    }

    @SuppressWarnings("null")
    @TargetApi(Build.VERSION_CODES.BASE)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent data) {
        if (requestCode != FILECHOOSER_RESULTCODE
                || mUploadCallbackAboveL == null) {
            return;
        }
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (data == null) {
                results = new Uri[]{imageUri};
            } else {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();

                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        if (results != null) {
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        } else {
            results = new Uri[]{imageUri};
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        }
        return;
    }


    private void take() {
        File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyApp");
        // Create the storage directory if it does not exist
        if (!imageStorageDir.exists()) {
            imageStorageDir.mkdirs();
        }
        File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        imageUri = Uri.fromFile(file);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent i = new Intent(captureIntent);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            i.setPackage(packageName);
            i.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(i);
        }
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        RegisterActivity.this.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void setZoom(WebSettings settings) {
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWvResgister != null) {
            mWvResgister.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mWvResgister != null) {
            mWvResgister.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWvResgister != null) {
            mWvResgister.destroy();
        }
    }

    @Override
    public void finish() {
        super.finish();
        if (mWvResgister != null) {
            ViewParent parent = mWvResgister.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(mWvResgister);
            }
            mWvResgister.stopLoading();
            mWvResgister.getSettings().setJavaScriptEnabled(false);
            mWvResgister.clearHistory();
            mWvResgister.clearView();
            mWvResgister.removeAllViews();
            mWvResgister.destroy();

        }
        /*ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();*/
    }

    private void downLoadImage(String jsonString) {
        count = 0;
        if (TextUtils.isEmpty(jsonString)) {
            MyToast.showToastShort("请选择需要保存的图片！");
            return;
        }
        final Gson gson = new Gson();
        SaveImageBean saveImageBean = gson.fromJson(jsonString, SaveImageBean.class);
        final List<String> urlList = saveImageBean.getFile();
        final String callbackName = saveImageBean.getCallback();
        mFailedUrl = new ArrayList<>();
        String absolutePath;
        try {
            absolutePath = AppHelper.getCacheDir();
        } catch (Exception e) {
            e.printStackTrace();
            if (urlList != null && urlList.size() > 0) {
                for (int i = 0; i < urlList.size(); i++) {
                    mFailedUrl.add(i);
                }
            }
            String failedUrlString = gson.toJson(mFailedUrl);
            mWvResgister.loadUrl("javascript:" + callbackName + "(" + failedUrlString + ")");
            MyToast.showToastShort("SD卡不存在");
            return;
        }
        if (urlList == null || urlList.size() == 0) {
            MyToast.showToastShort("图片地址有误！");
            return;
        }
        for (int i = 0; i < urlList.size(); i++) {
            String url = urlList.get(i);
            final int index = i;
            if (!TextUtils.isEmpty(url)) {
                String fileName = url.substring(url.lastIndexOf("/"));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showNewLoading(true, getString(R.string.public_loading));
                    }
                });
                OkHttpUtils.get().url(url).build().
                        connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new FileCallBack(absolutePath, fileName) {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        mFailedUrl.add(index);
                        count++;
                        if (count >= urlList.size()) {
                            mWvResgister.loadUrl("javascript:" + callbackName + "('" + gson.toJson(mFailedUrl) + "')");
                        }
                        dismissLoading();
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        saveImage(response.getAbsolutePath());
                        count++;
                        if (count >= urlList.size()) {
                            LogUtil.d("json==5=" + "javascript:" + callbackName + "('" + gson.toJson(mFailedUrl) + "')");
                            mWvResgister.loadUrl("javascript:" + callbackName + "('" + gson.toJson(mFailedUrl) + "')");
                        }
                        dismissLoading();
                    }
                });
            }
        }
    }

    private Uri saveImage(String imagePath) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATA, imagePath);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        Uri uri = MyApplication.getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(imagePath));
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
        return uri;
    }

    public void saveImageData(String data) {
        try {
            Bitmap bitmap = webData2bitmap(data);
            if (bitmap != null) {
                save2Album(bitmap, System.currentTimeMillis() + ".jpg");
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MyToast.showToastShort("保存失败");
                    }
                });
            }
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MyToast.showToastShort("保存失败");
                }
            });
            e.printStackTrace();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Bitmap webData2bitmap(String data) {
        byte[] imageBytes = Base64.decode(data.split(",")[1], Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    private void save2Album(Bitmap bitmap, String fileName) {
        final File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                    MyToast.showToastShort("保存成功");
                }
            });
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MyToast.showToastShort("保存失败");
                }
            });
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception ignored) {
                }
            }
        }
    }

    private void showOnLongSaveDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.tx_btn_save), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                boolean results = PermissionUtils.checkPermissionArray(RegisterActivity.this, storgeDermissionArray);
                if (results) {
                    saveImageData(picUrl);
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE7, storgeDermissionArray, getString(R.string.permission_content_storage));
                }
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }

}
