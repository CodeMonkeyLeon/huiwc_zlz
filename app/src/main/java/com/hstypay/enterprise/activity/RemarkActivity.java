package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class RemarkActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnRemark;
    private TextView mButton, mTvTitle, mTvRemarkLength;
    private EditText mEtRemark;
    private String mPayRemark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remark);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnRemark = (Button) findViewById(R.id.btn_submit);
        mTvTitle.setText(R.string.title_pay_remark);
        mButton.setVisibility(View.VISIBLE);
        mButton.setText(getString(R.string.dialog_btn_clear));

        mEtRemark = (EditText) findViewById(R.id.et_pay_remark);
        mTvRemarkLength = (TextView) findViewById(R.id.tv_edit_length);

        setButtonEnable(mBtnRemark,true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnRemark.setOnClickListener(this);
        mButton.setOnClickListener(this);

        mEtRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTvRemarkLength.setText(mEtRemark.getText().toString().trim().length() + "/32");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void initData() {
        mPayRemark = getIntent().getStringExtra(Constants.INTENT_PAY_REMARK);
        mEtRemark.setText(mPayRemark);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                submit();
                break;
            case R.id.button_title:
                mEtRemark.setText("");
                break;
            default:
                break;
        }
    }

    private void submit() {
        Intent intent = new Intent();
        intent.putExtra(Constants.RESULT_PAY_REMARK, mEtRemark.getText().toString().trim());
        setResult(RESULT_OK, intent);
        finish();
    }
}
