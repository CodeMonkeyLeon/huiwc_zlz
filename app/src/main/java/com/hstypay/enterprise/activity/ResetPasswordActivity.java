package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017/7/5.
 */

public class ResetPasswordActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private EditText mEtPwd1;
    private ImageView mIvEyeNew, mIvEyeEnsure;
    private EditText mEtPwd2;
    private Button btn_next_step;
    private TextView mTvTitle, mBtnTitle;
    private String mTelphone;
    private String mIntentName;
    private boolean isOpenNew, isOpenEnsure;
    private CommonNoticeDialog mNoticeDialog;
    private SafeDialog mLoadDialog;
    private SafeKeyboard safeKeyboard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        btn_next_step.setOnClickListener(this);
        mIvEyeNew.setOnClickListener(this);
        mIvEyeEnsure.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.change_pwd), false);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_reset_password);
        mBtnTitle = findViewById(R.id.button_title);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mEtPwd1 = (EditText) findViewById(R.id.et_new_pwd1);
        mEtPwd2 = (EditText) findViewById(R.id.et_new_pwd2);
        btn_next_step = (Button) findViewById(R.id.btn_next_step);
        mIvEyeNew = (ImageView) findViewById(R.id.iv_eye_new);
        mIvEyeEnsure = (ImageView) findViewById(R.id.iv_eye_ensure);

        View rootView = findViewById(R.id.main_root);
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtPwd1.getId(), mEtPwd1);
        safeKeyboard.putEditText(mEtPwd2.getId(), mEtPwd2);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (!StringUtils.isEmptyOrNull(mEtPwd1.getText().toString().trim())
                        && !StringUtils.isEmptyOrNull(mEtPwd2.getText().toString().trim())) {
                    setButtonEnable(btn_next_step, true);
                } else {
                    setButtonEnable(btn_next_step, false);
                }
            }
        });
        mEtPwd1.addTextChangedListener(editTextWatcher);
        mEtPwd2.addTextChangedListener(editTextWatcher);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        setButtonEnable(btn_next_step, false);
    }

    private void initData() {
        mTelphone = getIntent().getStringExtra(Constants.INTENT_RESET_PWD_TELPHONE);
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //确定
            case R.id.btn_next_step:
                MtaUtils.mtaId(ResetPasswordActivity.this, "A009");
                nextStep();
                break;
            case R.id.iv_eye_new:
                isOpenNew = setEye(isOpenNew);
                setPwdVisible(isOpenNew, mIvEyeNew, mEtPwd1);
                break;
            case R.id.iv_eye_ensure:
                isOpenEnsure = setEye(isOpenEnsure);
                setPwdVisible(isOpenEnsure, mIvEyeEnsure, mEtPwd2);
                break;
        }
    }

    private void nextStep() {
        if (StringUtils.isEmptyOrNull(mEtPwd1.getText().toString().trim()) || StringUtils.isEmptyOrNull(mEtPwd2.getText().toString().trim())) {
            showCommonNoticeDialog(ResetPasswordActivity.this, getString(R.string.pwd_not_null));
            return;
        }

        if (!mEtPwd1.getText().toString().trim().equals(mEtPwd2.getText().toString().trim())) {
            showCommonNoticeDialog(ResetPasswordActivity.this, getString(R.string.tx_pass_notdiff));
            mEtPwd2.setFocusable(true);
            mEtPwd2.setFocusableInTouchMode(true);
            mEtPwd2.requestFocus();
            return;
        }

        /*if(mEtPwd1.getText().toString().trim().length()<6 || mEtPwd2.getText().toString().trim().length()<6){
            ToastHelper.showInfo(ResetPasswordActivity.this, ToastHelper.toStr(R.string.pwd_prompting));
            return;
        }*/
//        if (!StringUtils.isLetterDigit(mEtPwd1.getText().toString().trim())){
        if (mEtPwd1.getText().toString().trim().length() < 8) {
            mEtPwd1.setFocusable(true);
            mEtPwd1.setFocusableInTouchMode(true);
            mEtPwd1.requestFocus();
            showCommonNoticeDialog(ResetPasswordActivity.this, getString(R.string.pwd_prompting));
            return;
        }

//        if (!StringUtils.isLetterDigit(mEtPwd2.getText().toString().trim())){
        if (mEtPwd2.getText().toString().trim().length() < 8) {
            mEtPwd2.setFocusable(true);
            mEtPwd2.setFocusableInTouchMode(true);
            mEtPwd2.requestFocus();
            showCommonNoticeDialog(ResetPasswordActivity.this, getString(R.string.pwd_prompting));
            return;
        }

        /*if (!StringUtils.isLetterDigit(mEtPwd1.getText().toString().trim())) {
            ToastHelper.showInfo(ResetPasswordActivity.this, ToastHelper.toStr(R.string.tx_pass_not_legal));
            mEtPwd1.setFocusable(true);
            mEtPwd1.setFocusableInTouchMode(true);
            mEtPwd1.requestFocus();
            return;
        }
        if (!StringUtils.isLetterDigit(mEtPwd2.getText().toString().trim())) {
            ToastHelper.showInfo(ResetPasswordActivity.this, ToastHelper.toStr(R.string.tx_pass_not_legal));
            mEtPwd2.setFocusable(true);
            mEtPwd2.setFocusableInTouchMode(true);
            mEtPwd2.requestFocus();
            return;
        }*/

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("newPassword", INITDES3Util.getSecretContent(this, mEtPwd2.getText().toString().trim()));
            map.put("desFlag", 1);
            if ("INTENT_EMP_RESET_PWD".equals(mIntentName)) {
                map.put("userId", mTelphone);
                ServerClient.newInstance(ResetPasswordActivity.this).empResetPwd(ResetPasswordActivity.this, "TAG_EMP_RESET_PWD", map);
            } else {
                map.put("telphone", mTelphone);
                ServerClient.newInstance(ResetPasswordActivity.this).resetPwd(ResetPasswordActivity.this, Constants.TAG_RESET_PWD, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

        //密码修改成功跳转到登录界面
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResetPassword(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_RESET_PWD) || event.getTag().equals("TAG_EMP_RESET_PWD")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_DATA_ERROR:
                    //数据解析错误
                    MyToast.showToastShort(getString(R.string.data_error));
                    break;
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ResetPasswordActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ResetPasswordActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    MtaUtils.mtaId(ResetPasswordActivity.this, "A010");
                    showResetSuccessDialog(event.getTag());
                    break;
            }
        }

    }

    private void showResetSuccessDialog(String tag) {
        if (mNoticeDialog == null) {
            mNoticeDialog = new CommonNoticeDialog(ResetPasswordActivity.this, getString(R.string.edit_info_success), getString(R.string.dialog_notice_button));
            mNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    if (Constants.TAG_RESET_PWD.equals(tag))
                        startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
                    finish();
                }
            });
        }
        mNoticeDialog.show();
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return isOpen ? false : true;
    }

    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboard.isShow()) {
                safeKeyboard.hideKeyboard();
                return false;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (safeKeyboard != null) {
            safeKeyboard.release();
            safeKeyboard = null;
        }
        super.onDestroy();
    }
}
