package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.RoleRecyclerAdapter;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.RoleSelectBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述: 角色单选列表
 */
public class RoleListActivity extends BaseActivity implements View.OnClickListener, RoleRecyclerAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvNull, mButton;
    private RecyclerView mRvShop;
    private EditText mEtInput;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<RoleSelectBean> mList = new ArrayList<>();
    private RoleRecyclerAdapter mAdapter;
    private Button mBtn_complete;
    private String intentName;
    private ImageView mIvClean;
    private int storeDateType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        currentPage = 2;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mRvShop.setLayoutManager(new LinearLayoutManager(this));
        mEtInput = (EditText) findViewById(R.id.et_user_input);
        mTvTitle.setText(R.string.title_select_role);
        mButton.setVisibility(View.INVISIBLE);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        View layout_search_header = findViewById(R.id.layout_search_header);
        layout_search_header.setVisibility(View.GONE);

        initRecyclerView();
    }


    private void initRecyclerView() {
        SHSwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setRefreshEnable(false);
        swipeRefreshLayout.setLoadmoreEnable(false);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());
    }


    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
    }

    public void initData() {
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        String roleCode = getIntent().getStringExtra(Constants.INTENT_STORE_ID);
        storeDateType = getIntent().getIntExtra(Constants.INTENT_STORE_DATA_TYPE, 0);
        mList = new ArrayList<>();
        getData();
        mAdapter = new RoleRecyclerAdapter(RoleListActivity.this, mList, roleCode);
        mAdapter.setOnItemClickListener(this);
        mRvShop.setAdapter(mAdapter);

    }

    private void getData() {
        mList.clear();
        if ( MyApplication.getIsMerchant()){
            //是商户才可以添加店长
            RoleSelectBean storeManager = new RoleSelectBean();
            storeManager.setRoleName("店长");
            storeManager.setRoleCode(Constants.ROLE_STORE_NICK_MANAGER);
            storeManager.setRoleId(""+Constants.ROLE_STORE_MANAGER);
            mList.add(storeManager);
        }
        RoleSelectBean cashier = new RoleSelectBean();
        cashier.setRoleName("收银员");
        cashier.setRoleCode(Constants.ROLE_STORE_NICK_CASHIER);
        cashier.setRoleId(""+Constants.ROLE_STORE_CASHIER);
        mList.add(cashier);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                // mList.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        RoleSelectBean dataBean = mList.get(position);
        if (dataBean != null) {
            Intent intent = new Intent();
            Bundle mBundle = new Bundle();
            mBundle.putSerializable(Constants.RESULT_ROLE_BEAN_INTENT, dataBean);   //传递一个user对象列表
            intent.putExtras(mBundle);
            setResult(RESULT_OK, intent);
            finish();
        }
    }


}
