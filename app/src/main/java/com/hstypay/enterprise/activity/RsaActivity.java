package com.hstypay.enterprise.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.RSA.Base64Utils;
import com.hstypay.enterprise.utils.RSA.RSAUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2018/12/13 11:41
 * @描述: ${TODO}
 */
public class RsaActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG="MainActivity";
    private Button encryptionBtn, decryptionBtn;// 加密，解密
    private EditText inputContent,outputEncryContent, outputDecryContent;// 需加密的内容，加密后的内容，解密后的内容

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rsa);
        initView();
    }

    private void initView()
    {
        encryptionBtn = (Button) findViewById(R.id.encryption_btn);
        decryptionBtn = (Button) findViewById(R.id.decryption_btn);
        encryptionBtn.setOnClickListener(this);
        decryptionBtn.setOnClickListener(this);

        inputContent = (EditText) findViewById(R.id.input_content);
        outputEncryContent= (EditText) findViewById(R.id.output_encrycontent);
        outputDecryContent = (EditText) findViewById(R.id.output_decrycontent);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            // 加密
            case R.id.encryption_btn:
                String source = inputContent.getText().toString().trim();
                Log.d(TAG, source);
                InputStream inPublic = null;
                try
                {	Log.d(TAG, "进入");
                    // 从文件中得到公钥
                    inPublic = getAssets().open("rsa_public_key.pem");
                    PublicKey publicKey = RSAUtils.loadPublicKey(inPublic);
                    // 加密
//                    byte[] encryptByte = RSAUtils.encryptData(source.getBytes(), publicKey);
                    //分段加密
                    byte[] encryptByte = RSAUtils.RSAEncode(source.getBytes(), publicKey);

                    // 为了方便观察吧加密后的数据用base64加密转一下，要不然看起来是乱码,所以解密是也是要用Base64先转换
                    String afterencrypt = Base64Utils.encode(encryptByte);
                    Log.d(TAG, afterencrypt );
                    outputEncryContent.setText(afterencrypt);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (inPublic != null) {
                        try {
                            inPublic.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            // 解密
            case R.id.decryption_btn:
                String encryptContent = outputEncryContent.getText().toString().trim();
                InputStream inPrivate = null;
                try {
                    // 从文件中得到私钥
                    inPrivate = getResources().getAssets().open("rsa_private_key_pkcs8.pem");
//                    PrivateKey privateKey = RSAUtils.loadPrivateKey(inPrivate);
                    PrivateKey privateKey = RSAUtils.loadPrivateKey(inPrivate);
                    // 因为RSA加密后的内容经Base64再加密转换了一下，所以先Base64解密回来再给RSA解密
//                    byte[] decryptByte = RSAUtils.decryptData(Base64Utils.decode(encryptContent), privateKey);
                    //分段解密
                    byte[] decryptByte = RSAUtils.RSADecode(Base64Utils.decode(encryptContent), privateKey);
                    String decryptStr = new String(decryptByte);
                    Log.d(TAG, decryptStr);
                    outputDecryContent.setText(decryptStr);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (inPrivate != null) {
                        try {
                            inPrivate.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}
