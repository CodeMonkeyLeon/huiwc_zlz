package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ScanLoginBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ScanLoginActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnSubmit, mBtnCancel, mBtnScan;
    private TextView mButton, mTvTitle, mTvScanStatus, mTvScanNotice;
    private LinearLayout mLlScanFailed, mLlScanSuccess;
    public static ScanLoginActivity instance = null;
    private SafeDialog mLoadDialog;
    private ScanLoginBean.DataBean mDataBean;
    private RelativeLayout mRlContent;
    private CommonNoticeDialog mDialogInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_login);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mLlScanFailed = findViewById(R.id.ll_scan_login_failed);
        mTvScanStatus = findViewById(R.id.tv_scan_login_status);
        mTvScanNotice = findViewById(R.id.tv_scan_login_notice);
        mLlScanSuccess = findViewById(R.id.ll_scan_login_success);
        mRlContent = findViewById(R.id.rl_content);

        mBtnScan = findViewById(R.id.btn_scan);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mBtnCancel = findViewById(R.id.btn_cancel);

        mButton.setVisibility(View.INVISIBLE);
        mTvTitle.setText(R.string.tv_scan_login);
        setButtonEnable(mBtnSubmit,true);
        setButtonWhite(mBtnCancel);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnScan.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
    }

    public void initData() {
        mDataBean = (ScanLoginBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_CAPTURE_SCAN_LOGIN);
        switch (mDataBean.getStatus()) {
            case 0:
            case 1:
                mRlContent.setVisibility(View.VISIBLE);
                mLlScanSuccess.setVisibility(View.VISIBLE);
                mLlScanFailed.setVisibility(View.GONE);
                mBtnSubmit.setVisibility(View.VISIBLE);
                mBtnCancel.setVisibility(View.VISIBLE);
                mBtnScan.setVisibility(View.GONE);
                break;
            case 2:
                mRlContent.setVisibility(View.VISIBLE);
                mLlScanSuccess.setVisibility(View.GONE);
                mLlScanFailed.setVisibility(View.VISIBLE);
                mTvScanStatus.setText(R.string.tv_scan_login_unused);
                mTvScanNotice.setText(R.string.tv_scan_login_unused_notice);
                mBtnScan.setVisibility(View.VISIBLE);
                mBtnSubmit.setVisibility(View.GONE);
                mBtnCancel.setVisibility(View.INVISIBLE);
                break;
            case 5:
                mRlContent.setVisibility(View.VISIBLE);
                mLlScanSuccess.setVisibility(View.GONE);
                mLlScanFailed.setVisibility(View.VISIBLE);
                mTvScanStatus.setText(R.string.tv_scan_login_error);
                mTvScanNotice.setText(R.string.tv_scan_login_error_notice);
                mBtnScan.setVisibility(View.GONE);
                mBtnSubmit.setVisibility(View.GONE);
                mBtnCancel.setVisibility(View.INVISIBLE);
                break;
            case 3:
                getDialog("登录已取消");
                break;
            case 4:
                getDialog("操作失败");
                break;
        }
    }

    public void getDialog(String title) {
        mDialogInfo = new CommonNoticeDialog(ScanLoginActivity.this, title, getString(R.string.dialog_notice_button));
        mDialogInfo.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                finish();
                startActivity(new Intent(ScanLoginActivity.this, MainActivity.class));
            }
        });
        DialogHelper.resize(ScanLoginActivity.this, mDialogInfo);
        mDialogInfo.show();
    }

    public void getDialog(String title, final boolean stay) {
        mDialogInfo = new CommonNoticeDialog(ScanLoginActivity.this, title, getString(R.string.dialog_notice_button));
        mDialogInfo.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                if (!stay) {
                    finish();
                    startActivity(new Intent(ScanLoginActivity.this, MainActivity.class));
                }
            }
        });
        DialogHelper.resize(ScanLoginActivity.this, mDialogInfo);
        mDialogInfo.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_SCAN_LOGIN) || event.getTag().equals(Constants.TAG_SCAN_LOGIN_STATUS_CANCEL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ScanLoginBean msg = (ScanLoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ScanLoginActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage(), true);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        switch (msg.getData().getStatus()) {
                            case 0:
                            case 1:
                                if (event.getTag().equals(Constants.TAG_SCAN_LOGIN)) {
                                    getDialog("操作失败");
                                } else {
                                    getDialog("操作失败", true);
                                }
                                break;
                            case 2:
                                mRlContent.setVisibility(View.VISIBLE);
                                mLlScanSuccess.setVisibility(View.GONE);
                                mLlScanFailed.setVisibility(View.VISIBLE);
                                mTvScanStatus.setText(R.string.tv_scan_login_unused);
                                mTvScanNotice.setText(R.string.tv_scan_login_unused_notice);
                                mBtnScan.setVisibility(View.VISIBLE);
                                mBtnSubmit.setVisibility(View.GONE);
                                mBtnCancel.setVisibility(View.INVISIBLE);
                                break;
                            case 5:
                                mRlContent.setVisibility(View.VISIBLE);
                                mLlScanSuccess.setVisibility(View.GONE);
                                mLlScanFailed.setVisibility(View.VISIBLE);
                                mTvScanStatus.setText(R.string.tv_scan_login_error);
                                mTvScanNotice.setText(R.string.tv_scan_login_error_notice);
                                mBtnScan.setVisibility(View.GONE);
                                mBtnSubmit.setVisibility(View.GONE);
                                mBtnCancel.setVisibility(View.INVISIBLE);
                                break;
                            case 3:
                                getDialog("登录已取消");
                                break;
                            case 4:
                                if (event.getTag().equals(Constants.TAG_SCAN_LOGIN)) {
//                                    getDialog("操作成功");
                                    finish();
                                    startActivity(new Intent(ScanLoginActivity.this, MainActivity.class));
                                } else {
                                    getDialog("操作失败");
                                }
                                break;
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                startActivity(new Intent(ScanLoginActivity.this, MainActivity.class));
                break;
            case R.id.btn_scan:
                finish();
                break;
            case R.id.btn_submit:
                submit();
                break;
            case R.id.btn_cancel:
                loginCodeStatus(mDataBean.getQrcodeId(), 3);
                break;
            default:
                break;
        }
    }

    private void submit() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(mDataBean.getQrcodeId())) {
                map.put("qrcodeId", mDataBean.getQrcodeId());
            }
            ServerClient.newInstance(ScanLoginActivity.this).qrcodeLogin(ScanLoginActivity.this, Constants.TAG_SCAN_LOGIN, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loginCodeStatus(String code, int status) {
        if (!NetworkUtils.isNetworkAvailable(ScanLoginActivity.this)) {
            getDialog(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("status", status);
            map.put("qrcodeId", code);
            ServerClient.newInstance(MyApplication.getContext()).loginCodeStatus(MyApplication.getContext(), Constants.TAG_SCAN_LOGIN, map);
        }
    }
}
