package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.setting.AboutUsActivity;
import com.hstypay.enterprise.activity.setting.ChangePwdActivity;
import com.hstypay.enterprise.activity.setting.GoodsNameActivity;
import com.hstypay.enterprise.activity.setting.PrintSelectSetActivity;
import com.hstypay.enterprise.activity.setting.PrintTypeActivity;
import com.hstypay.enterprise.activity.setting.QrcodeSetActivity;
import com.hstypay.enterprise.activity.setting.VoiceSetActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.MaterialStatusBean;
import com.hstypay.enterprise.bean.QrcodeSetDetailBean;
import com.hstypay.enterprise.commonlib.http.ApiManager;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.HandlerManager;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


public class SettingActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mButton, mTvTitle;
    private RelativeLayout mRlPwdSafe, mRlChangeTel, mRlGoodsName, mRlShortName, mRlPrintSet, mRlVoice, mRlPrintTypeSet, mRlQrcodeSet, mRlAboutUs;
    private Button mBtnLogout;
    private SelectDialog dialogInfo;
    private TextView mPrinterName, mTvGoodsName, mTvChangeTel, mTvShortNameTitle, mTvShortName, mTvChangeTelTitle;
    private ImageView mIvGoodsName, mIvShortName, mIvGoodsNameLine, mIvPrintTypeLine, mIvQrcodeSetLine;
    private SafeDialog mLoadDialog;
    private LinearLayout mLlSetVoice, mLlMerchantName;
    private String mStoreId;

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case HandlerManager.BLUE_CONNET_STUTS_CLOSED://蓝牙关闭
                    mPrinterName.setText(R.string.tv_no_conn_printer);
                    break;//tv_no_conn_printer
                case HandlerManager.BLUE_CONNET_STUTS://蓝牙已连接上后
                    String name = MyApplication.getBlueDeviceName();
                    if (!StringUtils.isEmptyOrNull(name)) {
                        mPrinterName.setText(name);
                    } else {
                        mPrinterName.setText(R.string.tv_no_conn_printer);
                    }
                    break;
                case HandlerManager.BLUE_CONNET:
                    String deviceName = MyApplication.getBlueDeviceName();
                    if (!StringUtils.isEmptyOrNull(deviceName)) {
                        mPrinterName.setText(deviceName);
                    } else {
                        mPrinterName.setText(R.string.tv_no_conn_printer);
                    }
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);

        initView();
        initEvent();
        //initData();
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET, handler);
        //注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.string_title_setting);
        mButton.setVisibility(View.INVISIBLE);
        mLlMerchantName = findViewById(R.id.ll_merchant_name);
        mRlPwdSafe = (RelativeLayout) findViewById(R.id.rl_pwd_safe);
        mRlChangeTel = (RelativeLayout) findViewById(R.id.rl_change_tel);
        mRlGoodsName = (RelativeLayout) findViewById(R.id.rl_goods_name);
        mRlShortName = (RelativeLayout) findViewById(R.id.rl_short_name);
        mRlPrintSet = (RelativeLayout) findViewById(R.id.rl_print_set);
        mRlPrintTypeSet = (RelativeLayout) findViewById(R.id.rl_print_type_set);
        mIvPrintTypeLine = (ImageView) findViewById(R.id.iv_print_type_line);
        mRlQrcodeSet = (RelativeLayout) findViewById(R.id.rl_qrcode_set);
        mIvQrcodeSetLine = (ImageView) findViewById(R.id.iv_qrcode_set_line);
        mRlVoice = (RelativeLayout) findViewById(R.id.rl_voice_notice);
        mRlAboutUs = (RelativeLayout) findViewById(R.id.rl_about_us);
        mBtnLogout = (Button) findViewById(R.id.ll_logout);
        mPrinterName = (TextView) findViewById(R.id.tv_printer_name);
        mTvChangeTel = (TextView) findViewById(R.id.tv_change_tel);
        mTvChangeTelTitle = (TextView) findViewById(R.id.tv_change_tel_title);
        mTvShortNameTitle = (TextView) findViewById(R.id.tv_short_name_title);
        mTvShortName = (TextView) findViewById(R.id.tv_short_name);
        mIvShortName = (ImageView) findViewById(R.id.iv_short_name);
        mTvGoodsName = (TextView) findViewById(R.id.tv_goods_name);
        mIvGoodsName = (ImageView) findViewById(R.id.iv_goods_name);
        mIvGoodsNameLine = (ImageView) findViewById(R.id.iv_goods_name_line);
        mLlSetVoice = (LinearLayout) findViewById(R.id.ll_set_voice);

        mPrinterName.setVisibility(View.INVISIBLE);
        if (AppHelper.getApkType() != 1) {
            mLlSetVoice.setVisibility(View.VISIBLE);
        } else {
            mLlSetVoice.setVisibility(View.GONE);
        }
        if (AppHelper.getAppType() == 1) {
            mRlPrintSet.setVisibility(View.GONE);
            mRlPrintTypeSet.setVisibility(View.VISIBLE);
            mIvPrintTypeLine.setVisibility(View.VISIBLE);
        } else if (AppHelper.getAppType() == 2) {
            mRlPrintSet.setVisibility(View.VISIBLE);
            mRlPrintTypeSet.setVisibility(View.GONE);
            mIvPrintTypeLine.setVisibility(View.GONE);
        }
        if (AppHelper.getApkType() == 1) {
            mRlGoodsName.setVisibility(View.GONE);
            mRlShortName.setVisibility(View.GONE);
        } else {
            if ((MyApplication.getIsMerchant() || MyApplication.getIsAdmin())) {
                mRlGoodsName.setEnabled(true);
                mIvGoodsName.setVisibility(View.VISIBLE);
                if (MyApplication.getExamineStatus() == 1) {
                    mIvShortName.setVisibility(View.VISIBLE);
                    if (AppHelper.getAppType() == 2) {
                        mRlShortName.setEnabled(true);
                    } else if (AppHelper.getAppType() == 1) {
                        mRlShortName.setEnabled(false);
                        mIvShortName.setVisibility(View.INVISIBLE);
                    }
                } else {
                    mRlShortName.setVisibility(View.GONE);
                }
            } else {
                mRlGoodsName.setEnabled(false);
                mIvGoodsName.setVisibility(View.INVISIBLE);
                mRlShortName.setEnabled(false);
                mIvShortName.setVisibility(View.INVISIBLE);
            }
        }
        if (MyApplication.getIsMerchant()) {
            mLlMerchantName.setVisibility(View.VISIBLE);
            mRlShortName.setVisibility(MyApplication.getExamineStatus() == 1 ? View.VISIBLE : View.GONE);
            mIvGoodsNameLine.setVisibility(MyApplication.getExamineStatus() == 1 ? View.VISIBLE : View.GONE);
            mRlQrcodeSet.setVisibility(View.VISIBLE);
            mIvQrcodeSetLine.setVisibility(View.VISIBLE);
        } else {
            mLlMerchantName.setVisibility(View.GONE);
        }
        setButtonWhite(mBtnLogout);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlPwdSafe.setOnClickListener(this);
        mRlChangeTel.setOnClickListener(this);
        mRlGoodsName.setOnClickListener(this);
        mRlShortName.setOnClickListener(this);
        mRlPrintSet.setOnClickListener(this);
        mRlPrintTypeSet.setOnClickListener(this);
        mRlQrcodeSet.setOnClickListener(this);
        mRlVoice.setOnClickListener(this);
        mBtnLogout.setOnClickListener(this);
        mRlAboutUs.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initData() {
        mTvGoodsName.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_GOODS_NAME));
        mTvShortName.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_SHORT_NAME));

        String deviceName = MyApplication.getBlueDeviceName();
        if (!StringUtils.isEmptyOrNull(deviceName) && MyApplication.getBlueState()) {
            mPrinterName.setText(deviceName);
        }
        String loginPhone = MyApplication.getLoginPhone();
        if (!StringUtils.isEmptyOrNull(loginPhone)) {
            mTvChangeTelTitle.setText(UIUtils.getString(R.string.change_tel));
            mTvChangeTel.setText(loginPhone);
        } else {
            mTvChangeTelTitle.setText(UIUtils.getString(R.string.binding_tel));
            mTvChangeTel.setText("");
        }

        int merchantType = SpUtil.getInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE);
        if (merchantType == 1) {//企业
            mTvShortNameTitle.setText(UIUtils.getString(R.string.short_name));
        } else if (merchantType == 2) {//个人
            mTvShortNameTitle.setText(UIUtils.getString(R.string.shop_name));
        }
    }

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        dismissLoading();
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_pwd_safe:
                MtaUtils.mtaId(SettingActivity.this, "S001");
                startActivity(new Intent(this, ChangePwdActivity.class));
                break;
            case R.id.rl_change_tel:
                MtaUtils.mtaId(SettingActivity.this, "T001");
                Intent intentChangeTel = new Intent(this, AuthenticationActivity.class);
                intentChangeTel.putExtra(Constants.INTENT_NAME, Constants.INTENT_TELEPHONE_AUTHENTICATION);
                startActivity(intentChangeTel);
                break;
            case R.id.rl_goods_name:
                MtaUtils.mtaId(SettingActivity.this, "V001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    String goodsName = SpUtil.getString(MyApplication.getContext(), Constants.SP_GOODS_NAME);
                    Intent intent = new Intent(this, GoodsNameActivity.class);
                    intent.putExtra(Constants.INTENT_GET_GOODS_NAME, goodsName);
                    startActivity(intent);
                }
                break;
            case R.id.rl_short_name:
                MtaUtils.mtaId(SettingActivity.this, "U001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    materialStatus();
                }
                break;
            case R.id.rl_print_set:
                MtaUtils.mtaId(SettingActivity.this, "W001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    startActivity(new Intent(this, PrintSelectSetActivity.class));
                }
                break;
            case R.id.rl_print_type_set:
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    startActivity(new Intent(this, PrintTypeActivity.class));
                }
                break;
            case R.id.rl_voice_notice:
                MtaUtils.mtaId(SettingActivity.this, "X001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    startActivity(new Intent(this, VoiceSetActivity.class));
                }
                break;
            case R.id.ll_logout:
                dialogInfo = new SelectDialog(SettingActivity.this, getString(R.string.show_sign_out), getString(R.string.btnCancel), getString(R.string.btnOk), R.layout.select_common_dialog);
                dialogInfo.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                    @Override
                    public void clickOk() {
                        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                            showNewLoading(true, "正在退出...");
                            ServerClient.newInstance(SettingActivity.this).Loginout(SettingActivity.this, Constants.LOGINOUT_TAG, null);
                        } else {
                            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                        }
                    }
                });
                DialogHelper.resize(SettingActivity.this, dialogInfo);
                dialogInfo.show();
                break;
            case R.id.rl_about_us:
                /*if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {*/
                startActivity(new Intent(SettingActivity.this, AboutUsActivity.class));
//                }
                break;
            case R.id.rl_qrcode_set:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    ServerClient.newInstance(SettingActivity.this).fixedQRCodeSetQuery(SettingActivity.this, "TAG_QRCODE_SET_QUERY", null);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            default:
                break;
        }
    }

    private void voiceAllSwitch(int pushClose) {
        Map<String, Object> map = new HashMap<>();
        String clientId = PushManager.getInstance().getClientid(this.getApplicationContext());
        if (!StringUtils.isEmptyOrNull(clientId)) {
            map.put("pushDeviceId", clientId);
        }
        if (AppHelper.getAppType() == 1) {
            map.put("client", Constants.REQUEST_CLIENT_POS);
        } else if (AppHelper.getAppType() == 2) {
            map.put("client", Constants.REQUEST_CLIENT_APP);
        }
        map.put("pushClose", pushClose);
        ServerClient.newInstance(MyApplication.getContext()).voiceAllSwitch(MyApplication.getContext(), Constants.TAG_VOICE_ALL_SWITCH, map);
    }

    public void materialStatus() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(MyApplication.getContext()).materialStatus(MyApplication.getContext(), Constants.TAG_STATUS_SHORT_NAME, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.LOGINOUT_TAG)) {
            Info info = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    break;
                case Constants.ON_EVENT_FALSE:
                    dismissLoading();
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(SettingActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(SettingActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    dismissLoading();
                    SettingActivity.this.finish();
                    ApiManager.get().setMService(null);
                    MyApplication.iscache = false;
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    startActivity(new Intent(this, LoginActivity.class));
                    MyApplication.getInstance().finishAllActivity();
                    PushManager.getInstance().turnOffPush(MyApplication.getContext());
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_STATUS_SHORT_NAME)) {
            dismissLoading();
            MaterialStatusBean info = (MaterialStatusBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(SettingActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(SettingActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        MaterialStatusBean.DataEntity data = info.getData();
                        if (data.getExamineStatus() == 1) {
                            startNextActivity();
                        } else {
                            if ("1".equals(data.getStandby7())) {
                                startNextActivity();
                            } else {
                                showCommonNoticeDialog(SettingActivity.this, getString(R.string.toast_not_material_change));
                            }
                        }
                    }
                    break;
            }
        }else if (event.getTag().equals("TAG_QRCODE_SET_QUERY")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            QrcodeSetDetailBean info = (QrcodeSetDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(SettingActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(SettingActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        QrcodeSetDetailBean.DataEntity data = info.getData();
                        startActivity(new Intent(SettingActivity.this, QrcodeSetActivity.class).putExtra("INTENT_QRCODE_SET_DETAIL",data));
                    } else {
                        showCommonNoticeDialog(SettingActivity.this, getString(R.string.error_data));
                    }
                    break;
            }
        }
       /* if (event.getTag().equals(Constants.DELETE_PUSH_VOICE_DEVICEID_TAG)) {

            Info info = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    Log.i("zhouwei",getString(R.string.net_error)+Constants.DELETE_PUSH_VOICE_DEVICEID_TAG);
                    break;

                case Constants.ADD_CASHIER_MANAGE_FALSE:
                    Log.i("zhouwei","语音播报删除失败");
                    break;
                case Constants.ADD_CASHIER_MANAGE_TRUE:
                    Log.i("zhouwei","语音播报删除成功");
                    break;
            }

        }*/
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void startNextActivity() {
        Intent intent = new Intent(this, AuthenticationActivity.class);
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_SHORT_AUTHENTICATION);
        startActivity(intent);
    }
}
