package com.hstypay.enterprise.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DensityUtils;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

public class ShareActivity extends BaseActivity implements View.OnClickListener {
    private String mCodeUrl;
    private long mCodeTimeout;
    private Bitmap mQRCode;
    private ImageView mIvBack, mIvShareCode;
    private TextView mTvUserTitle, mTvCodeTimeout;
    private LinearLayout mLlShareWecaht, mLlShareFrients, mLlShareWeibo, mLlShareQQ, mLlShareQQSpace, mLlShareIcon;
    private SafeDialog dialog;
    private UMWeb web;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_QQ = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_SPACE = 103;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvShareCode = (ImageView) findViewById(R.id.iv_share_qrcode);
        mTvUserTitle = (TextView) findViewById(R.id.tv_user_share_title);
        mTvCodeTimeout = (TextView) findViewById(R.id.tv_code_notice_timeout);
        mLlShareWecaht = (LinearLayout) findViewById(R.id.ll_share_wechat);
        mLlShareFrients = (LinearLayout) findViewById(R.id.ll_share_friends);
        mLlShareWeibo = (LinearLayout) findViewById(R.id.ll_share_weibo);
        mLlShareQQ = (LinearLayout) findViewById(R.id.ll_share_qq);
        mLlShareQQSpace = (LinearLayout) findViewById(R.id.ll_share_qq_space);
        mLlShareIcon = (LinearLayout) findViewById(R.id.ll_share_icon);
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);

        if (AppHelper.getAppType() == 1 || AppHelper.getApkType() != 0) {
            mLlShareIcon.setVisibility(View.GONE);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mLlShareWecaht.setOnClickListener(this);
        mLlShareFrients.setOnClickListener(this);
        mLlShareWeibo.setOnClickListener(this);
        mLlShareQQ.setOnClickListener(this);
        mLlShareQQSpace.setOnClickListener(this);
    }

    public void initData() {
        mCodeUrl = getIntent().getStringExtra(Constants.INTENT_CODE_URL);
        mCodeTimeout = getIntent().getLongExtra(Constants.INTENT_CODE_TIMEOUT, 0);
        LogUtil.d("mCodeTimeout==" + mCodeTimeout);
        initMedia();
        if (!TextUtils.isEmpty(mCodeUrl)) {
            try {
                mQRCode = MaxCardManager.getInstance().create2DCode(mCodeUrl,
                        DensityUtils.dip2px(ShareActivity.this, 165),
                        DensityUtils.dip2px(ShareActivity.this, 165));
            } catch (WriterException e) {
                e.printStackTrace();
            }
            mIvShareCode.setImageBitmap(mQRCode);
        }
        String realName = SpUtil.getString(MyApplication.getContext(), Constants.REALNAME);
        if (TextUtils.isEmpty(realName)) {
            mTvUserTitle.setVisibility(View.GONE);
        } else {
            mTvUserTitle.setText(UIUtils.getString(R.string.share_title_name_before) + realName + UIUtils.getString(R.string.share_title_name_after));
        }
        String timeout = formatTime(mCodeTimeout);
        if (TextUtils.isEmpty(timeout)) {
            mTvCodeTimeout.setVisibility(View.GONE);
        } else {
            mTvCodeTimeout.setText(UIUtils.getString(R.string.share_timeout_before) + timeout + UIUtils.getString(R.string.share_timeout_after));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_share_wechat:
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    ShareWeb(SHARE_MEDIA.WEIXIN, R.mipmap.share_icon);
                }
                break;
            case R.id.ll_share_friends:
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    ShareWeb(SHARE_MEDIA.WEIXIN_CIRCLE, R.mipmap.friends_logo);
                }
                break;
            case R.id.ll_share_weibo:

                break;
            case R.id.ll_share_qq:
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareActivity.this, permissionArray);
                    if (results) {
                        ShareWeb(SHARE_MEDIA.QQ, R.mipmap.share_icon);
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_QQ, permissionArray, getString(R.string.permission_content_share));
                    }
                }
                break;
            case R.id.ll_share_qq_space:
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareActivity.this, permissionArray);
                    if (results) {
                        ShareWeb(SHARE_MEDIA.QZONE, R.mipmap.space_logo);
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_SPACE, permissionArray, getString(R.string.permission_content_share));
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_QQ:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ShareWeb(SHARE_MEDIA.QQ, R.mipmap.share_icon);
                } else {
                    showDialog(getString(R.string.permission_set_content_share));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_SPACE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ShareWeb(SHARE_MEDIA.QZONE, R.mipmap.space_logo);
                } else {
                    showDialog(getString(R.string.permission_set_content_share));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void initMedia() {
        web = new UMWeb(mCodeUrl);
        web.setDescription("我叫" + SpUtil.getString(MyApplication.getContext(), Constants.REALNAME) + "，我为汇旺财代言。");
        web.setTitle("汇旺财，会发财");
    }

    private void ShareWeb(SHARE_MEDIA platform, int thumb_img) {
        UMImage thumb = new UMImage(ShareActivity.this, thumb_img);
        web.setThumb(thumb);
        new ShareAction(ShareActivity.this)
                .withMedia(web)
                .setPlatform(platform)
                .setCallback(shareListener).share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }

    private String formatTime(long timeout) {
        if (timeout >= 3600) {
            return timeout / 60 / 60 + "小时";
        } else if (timeout >= 60) {
            return timeout / 60 + "分钟";
        } else if (timeout > 0) {
            return timeout + "秒";
        } else {
            return "";
        }
    }
}
