package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.setting.QrcodeSetActivity;
import com.hstypay.enterprise.adapter.CommonAdapter;
import com.hstypay.enterprise.adapter.CommonViewHolder;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.hstypay.enterprise.utils.print.Cmd;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 门店单选列表
 */
public class ShopSelectActivity extends BaseActivity implements View.OnClickListener, ShopRecyclerAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvNull, mButton;
    private RecyclerView mRvShop;
    private EditText mEtInput;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private CommonAdapter<StoreListBean.DataEntity> commonAdapter;
    private Button mBtn_complete;
    private String intentName;
    private ImageView mIvClean;
    private int storeDateType;
    private RelativeLayout mRlSearch;
    private TextView mTvNotice;
    private int mPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_select);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        currentPage = 2;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mRvShop.setLayoutManager(new LinearLayoutManager(this));
        mEtInput = (EditText) findViewById(R.id.et_user_input);
        mButton.setVisibility(View.INVISIBLE);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mRlSearch = findViewById(R.id.layout_search_header);
        mTvNotice = findViewById(R.id.tv_notice);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtInput.isFocused()) {
                    if (mEtInput.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                        mList.clear();
                        setHeader(intentName, mList);
                        loadData("15", "1", "");
                    }
                }
            }
        });
        mEtInput.addTextChangedListener(editTextWatcher);
        mEtInput.setOnFocusChangeListener(listener);
        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_user_input:
                    if (hasFocus) {
                        if (mEtInput.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };

    private void initRecyclerView() {
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData("15", "1", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        mLinearLayoutManager.setScrollEnabled(false);
                        mRvShop.setLayoutManager(mLinearLayoutManager);
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        mLinearLayoutManager.setScrollEnabled(false);
                        mRvShop.setLayoutManager(mLinearLayoutManager);
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
    }

    public void initData() {
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        storeDateType = getIntent().getIntExtra(Constants.INTENT_STORE_DATA_TYPE, 0);
        mList = new ArrayList<>();

        commonAdapter = new CommonAdapter<>(mList, new CommonAdapter.OnBindDataListener<StoreListBean.DataEntity>() {
            @Override
            public int getLayoutId(int type) {
                return R.layout.item_shop_select;
            }

            @Override
            public void onBindViewHolder(StoreListBean.DataEntity model, CommonViewHolder viewHolder, int type, int position) {
                viewHolder.setText(R.id.tv_shop_choice, model.getStoreName());
                viewHolder.setTextColor(R.id.tv_shop_choice, UIUtils.getColor(model.isSelcet() ? R.color.theme_color : R.color.home_text));
                viewHolder.getView(R.id.layout_item_shop).setOnClickListener(view -> {
                    if (mPosition != -1) {
                        mList.get(mPosition).setSelcet(false);
                        commonAdapter.notifyItemChanged(mPosition);
                    }
                    mList.get(position).setSelcet(true);
                    commonAdapter.notifyItemChanged(position);
                    mPosition = position;
                    if (Constants.INTENT_NAME_QRCODE_SET.equals(intentName)) {
                        Intent intent = new Intent(ShopSelectActivity.this, QrcodeSetActivity.class);
                        intent.putExtra("INTENT_QRCODE_SET_STORE", mList.get(position));
                        intent.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                        startActivity(intent);
                    }
                });
            }
        });
        mRvShop.setAdapter(commonAdapter);
        setHeader(intentName, mList);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
        if (Constants.INTENT_NAME_QRCODE_SET.equals(intentName)) {
            mTvTitle.setText(R.string.title_qrcode_set);
        }

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        loadData("15", "1", mEtInput.getText().toString().trim());
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void loadData(String pageSize, String currentPage, String storeName) {
        if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
            showNewLoading(true, getString(R.string.public_loading));
        }
        //门店网络请求
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        if (!TextUtils.isEmpty(storeName)) {
            map.put("storeName", storeName);
        }
        if (storeDateType == 1) {
            map.put("merchantDataType", storeDateType);
        }
        ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_CHOICE_STORE, map);
    }

    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_STORE)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ShopSelectActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mSwipeRefreshLayout.setLoadmoreEnable(true);
                        //mRvShop.setVisibility(View.VISIBLE);
                        //mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData());
                        commonAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            //mRvShop.setVisibility(View.VISIBLE);
                            //mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            /*mRvShop.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setLoadmoreEnable(false);*/
                        }
                    }
                    break;
            }
            dismissLoading();
            mLinearLayoutManager.setScrollEnabled(true);
            mRvShop.setLayoutManager(mLinearLayoutManager);
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            setHeader(intentName, mList);
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                // mList.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        StoreListBean.DataEntity dataBean = mList.get(position);
        if (dataBean != null) {
            Intent intent = new Intent();
            Bundle mBundle = new Bundle();
            mBundle.putSerializable(Constants.RESULT_SHOP_BEAN_INTENT, dataBean);   //传递一个user对象列表
            intent.putExtras(mBundle);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void setHeader(String name, List<StoreListBean.DataEntity> list) {
        if (Constants.INTENT_NAME_BILL_SHOP.equals(name) || Constants.INTENT_NAME_COLLECT_SHOP.equals(name)) {
            StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
            dataEntity.setStoreName("全部门店");
            dataEntity.setStoreId("");
            list.add(dataEntity);
        }
    }

}
