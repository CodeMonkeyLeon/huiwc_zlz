package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.RateRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.LimitBean;
import com.hstypay.enterprise.bean.RateBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class SignInfoActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack,mIvForward,mIvVipRateForward;
    private TextView mTvSignTime,mTvVipSignTime,mTvMonthLimitMoney,mTvLimitCountMoney,mTvLimitDayMoney,mTvLimitDayCount;
    private RecyclerView mRvSignInfo,mRvUnionRate,mRvVipRate;
    private RelativeLayout mRlContactInfo,mRlPrivacyProtocol,mRlPassedTop,mRlCooperationContactInfo;
    private List<RateBean.DataEntity.RateListEntity> mList;
    private List<RateBean.DataEntity.RateListEntity> mVipRateList;
    private RateRecyclerAdapter mRateRecyclerAdapter,mVipRateAdapter;
    private LinearLayout mLlSignRate,mLlSignRateList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_info);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mTvSignTime = (TextView) findViewById(R.id.tv_sign_time);
        mTvVipSignTime = (TextView) findViewById(R.id.tv_vip_sign_time);
        mIvForward = (ImageView) findViewById(R.id.iv_sign_title_forward);
        mIvVipRateForward = (ImageView) findViewById(R.id.iv_vip_rate_title_forward);
        mRvSignInfo = (RecyclerView) findViewById(R.id.rv_sign_info);
        /*mRvUnionRate = (RecyclerView) findViewById(R.id.rv_union_rate);*/
        mRvVipRate = (RecyclerView) findViewById(R.id.rv_vip_rate_detail);
        mLlSignRateList = (LinearLayout) findViewById(R.id.ll_sign_rate_list);
        mRlContactInfo = (RelativeLayout) findViewById(R.id.rl_contact_info);
        mRlPrivacyProtocol = (RelativeLayout) findViewById(R.id.rl_privacy_protocol);
        mRlPassedTop = (RelativeLayout) findViewById(R.id.rl_passed_top);
        mLlSignRate = (LinearLayout) findViewById(R.id.ll_sign_rate);
        mRlCooperationContactInfo = (RelativeLayout) findViewById(R.id.rl_cooperation_contact_info);

        mTvMonthLimitMoney = (TextView) findViewById(R.id.tv_month_limit_money);
        mTvLimitCountMoney = (TextView) findViewById(R.id.tv_limit_count_money);
        mTvLimitDayMoney = (TextView) findViewById(R.id.tv_limit_day_money);
        mTvLimitDayCount = (TextView) findViewById(R.id.tv_limit_day_count);
        mLlSignRateList.setVisibility(View.GONE);

        if (MyApplication.getIsMerchant() && MyApplication.getExamineStatus() == 1){
            mRlPassedTop.setVisibility(View.VISIBLE);
            mTvSignTime.setGravity(Gravity.RIGHT);
        }else {
            mRlPassedTop.setVisibility(View.GONE);
            mTvSignTime.setGravity(Gravity.CENTER);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvForward.setOnClickListener(this);
        mIvVipRateForward.setOnClickListener(this);
        mRlContactInfo.setOnClickListener(this);
        mRlPrivacyProtocol.setOnClickListener(this);
        mRlCooperationContactInfo.setOnClickListener(this);
    }

    public void initData() {
        CustomLinearLayoutManager signLayoutManager = new CustomLinearLayoutManager(this);
        mRvSignInfo.setLayoutManager(signLayoutManager);
        mList = new ArrayList<>();
        mRateRecyclerAdapter = new RateRecyclerAdapter(MyApplication.getContext(), mList);
        mRvSignInfo.setAdapter(mRateRecyclerAdapter);
        /*CustomLinearLayoutManager unionLayoutManager = new CustomLinearLayoutManager(this);
        mRvUnionRate.setLayoutManager(unionLayoutManager);
        mRateUnionAdapter = new RateUnionAdapter(MyApplication.getContext(), mUnionList);
        mRvUnionRate.setAdapter(mRateUnionAdapter);*/
        CustomLinearLayoutManager UnionLayoutManager = new CustomLinearLayoutManager(this);
        mRvVipRate.setLayoutManager(UnionLayoutManager);
        mVipRateList = new ArrayList<>();
        mVipRateAdapter = new RateRecyclerAdapter(MyApplication.getContext(), mVipRateList);
        mRvVipRate.setAdapter(mVipRateAdapter);
        if (!NetworkUtils.isNetworkAvailable(this)) {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            loadDialog(SignInfoActivity.this, "加载中...");
            ServerClient.newInstance(SignInfoActivity.this).queryRate(SignInfoActivity.this, Constants.TAG_QUERY_RATE, null);
            ServerClient.newInstance(SignInfoActivity.this).queryVipRechargeRate(SignInfoActivity.this, Constants.TAG_VIP_RECHARGE_RATE, null);
        }
        /*if (!NetworkUtils.isNetworkAvailable(this)) {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            loadDialog(SignInfoActivity.this, "加载中...");
            ServerClient.newInstance(SignInfoActivity.this).queryVipRechargeRate(SignInfoActivity.this, Constants.TAG_VIP_RECHARGE_RATE, null);
        }*/
        if (!NetworkUtils.isNetworkAvailable(this)) {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            loadDialog(SignInfoActivity.this, "加载中...");
            ServerClient.newInstance(SignInfoActivity.this).limitDetail(SignInfoActivity.this, Constants.TAG_LIMIT_DETAIL, null);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_sign_title_forward:
                MtaUtils.mtaId(SignInfoActivity.this,"L003");
                if (mList==null || mList.size()==0) {
                    setRecyclerView(false);
                }else {
                    setRecyclerView(true);
                }
                break;
            case R.id.iv_vip_rate_title_forward:
                if (mVipRateList==null || mVipRateList.size()==0) {
                    setVipRecyclerView(false);
                }else {
                    setVipRecyclerView(true);
                }
                break;
            case R.id.rl_contact_info:
                Intent intent = new Intent(this, RegisterActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT,Constants.URL_SIGN_PROTOCOL);
                startActivity(intent);
                break;
            case R.id.rl_privacy_protocol:
                Intent intentPrivacy = new Intent(this, RegisterActivity.class);
                intentPrivacy.putExtra(Constants.REGISTER_INTENT,Constants.URL_USER_PROTOCOL);
                startActivity(intentPrivacy);
                break;
            case R.id.rl_cooperation_contact_info:
                Intent intentCooperation = new Intent(this, RegisterActivity.class);
                intentCooperation.putExtra(Constants.REGISTER_INTENT,Constants.URL_COOPERATION_PROTOCOL);
                startActivity(intentCooperation);
                break;
            default:
                break;
        }
    }

    private void setRecyclerView(boolean hasRate){
        if (hasRate) {
            if (mLlSignRateList.getVisibility() == View.VISIBLE){
                mLlSignRateList.setVisibility(View.GONE);
                mIvForward.setImageResource(R.mipmap.ic_arrow_down);
            }else if (mLlSignRateList.getVisibility() == View.GONE){
                mLlSignRateList.setVisibility(View.VISIBLE);
                mIvForward.setImageResource(R.mipmap.ic_arrow_up);
            }
        }
    }

   /* private void setUnionRecyclerView(boolean hasRate){
        if (hasRate) {
            if (mRvUnionRate.getVisibility() == View.VISIBLE){
                mRvUnionRate.setVisibility(View.GONE);
                mIvUnionForward.setImageResource(R.mipmap.icon_arrow_bottom);
            }else if (mRvUnionRate.getVisibility() == View.GONE){
                mRvUnionRate.setVisibility(View.VISIBLE);
                mIvUnionForward.setImageResource(R.mipmap.icon_arrow_top);
            }
        }
    }*/


    private void setVipRecyclerView(boolean hasRate){
        if (hasRate) {
            if (mRvVipRate.getVisibility() == View.VISIBLE){
                mRvVipRate.setVisibility(View.GONE);
                mIvVipRateForward.setImageResource(R.mipmap.ic_arrow_down);
            }else if (mRvVipRate.getVisibility() == View.GONE){
                mRvVipRate.setVisibility(View.VISIBLE);
                mIvVipRateForward.setImageResource(R.mipmap.ic_arrow_up);
            }
        }
    }

    //Eventbus接收数据,版本更新
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onQueryRate(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_QUERY_RATE)) {
            RateBean msg = (RateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.QUERY_RATE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(SignInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUERY_RATE_TRUE://请求成功
                    if (msg.getData()!=null){
                        if (!TextUtils.isEmpty(msg.getData().getDate())){
                            mTvSignTime.setText(msg.getData().getDate());
                        }
                        if (msg.getData().getRateList()!=null && msg.getData().getRateList().size()>0) {
                            mList.addAll(msg.getData().getRateList());
                            for (int i = msg.getData().getRateList().size() -1 ; i >= 0; i--) {
                                if (msg.getData().getRateList().get(i).getApiProvider() == 5){
                                    mList.remove(i);
                                    mList.add(msg.getData().getRateList().get(i));
                                }else if (msg.getData().getRateList().get(i).getApiProvider() == 10){
                                    mList.remove(i);
                                    mList.add(msg.getData().getRateList().get(i));
                                }
                            }
                            /*if (rateUnion!=null) {
                                mList.add(rateUnion);
                            }
                            if (rateBcard!=null) {
                                mList.add(rateBcard);
                            }*/
                            mRateRecyclerAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_VIP_RECHARGE_RATE)) {
            RateBean msg = (RateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(SignInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData()!=null && msg.getData().getRateList()!=null && msg.getData().getRateList().size()>0){
                        RateBean.DataEntity.RateListEntity rateListEntity = new RateBean().new DataEntity().new RateListEntity();
                        rateListEntity.setApiProvider(6);
                        rateListEntity.setRate(msg.getData().getRateList().get(0).getRate());
                        mList.add(0,rateListEntity);
                        mRateRecyclerAdapter.notifyDataSetChanged();
                        /*if (!TextUtils.isEmpty(msg.getData().getDate())){
                            mTvVipSignTime.setVisibility(View.VISIBLE);
                            mTvVipSignTime.setText("签约时间："+msg.getData().getDate());
                        }else {
                            mTvVipSignTime.setVisibility(View.GONE);
                        }
                        if (msg.getData().getRateList()!=null && msg.getData().getRateList().size()>0){
                            mVipRateList.addAll(msg.getData().getRateList());
                            mVipRateAdapter.notifyDataSetChanged();
                            mSlVipSignRate.setVisibility(View.VISIBLE);
                        }else {
                            mSlVipSignRate.setVisibility(View.GONE);
                        }*/
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_LIMIT_DETAIL)) {
            LimitBean msg = (LimitBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(SignInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData()!=null){
                        mTvMonthLimitMoney.setText(DateUtil.formatMoneyByLong(msg.getData().getMonthMaxQuota()));
                        mTvLimitCountMoney.setText(DateUtil.formatMoneyByLong(msg.getData().getSingleMaxQuota()));
                        mTvLimitDayMoney.setText(DateUtil.formatMoneyByLong(msg.getData().getDayMaxQuota()));
                        mTvLimitDayCount.setText(DateUtil.formatCountByLong(msg.getData().getDayMaxSum()));
                    }
                    break;
            }
            dismissLoading();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
