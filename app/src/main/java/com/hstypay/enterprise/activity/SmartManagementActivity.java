package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SelectUpdateDialog;
import com.hstypay.enterprise.Widget.UpdateSuccessNoticeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PushModeBean;
import com.hstypay.enterprise.bean.UrlBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.BaiduMtjUtils;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SmartManagementActivity extends BaseActivity implements View.OnClickListener {

    private SafeDialog mLoadDialog;
    private ImageView mIvBack;
    private Button mBtnUse;
    private LinearLayout mLlContent;
    private SelectUpdateDialog mSelectUpdateDialog;
    private SelectDialog mSelectDialog;
    private UpdateSuccessNoticeDialog mUpdateSuccessNoticeDialog;
    private String mSimpleName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_management);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        mSimpleName = this.getClass().getSimpleName();

        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(SmartManagementActivity.this, UIUtils.getString(R.string.public_loading), false);
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.title_smart_management));
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mBtnUse = findViewById(R.id.btn_ensure);
        mLlContent = findViewById(R.id.ll_content);

        setButtonEnable(mBtnUse, true);
    }

    public void initEvent() {
        mBtnUse.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    public void initData() {

    }

    /**
     * 应用开通状态查询
     */
    private void smartManagement() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).smartManagement(MyApplication.getContext(), "TAG_SMART_MANAGEMENT", null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_SWITCH_NEW_PLATFORM" + mSimpleName)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(SmartManagementActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(SmartManagementActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM, true);
                    showUpdatedSuccessDialog();
                    break;
            }
        } else if (event.getTag().equals("TAG_SMART_MANAGEMENT")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            UrlBean msg = (UrlBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                        if (msg.getError().getMessage() != null) {
                            getLoginDialog(SmartManagementActivity.this, msg.getError().getMessage());
                        }
                    } else {
                        if (msg.getError().getMessage() != null) {
                            MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (!TextUtils.isEmpty(msg.getData())) {
                        Intent intent = new Intent(SmartManagementActivity.this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, msg.getData());
                        intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                        startActivity(intent);
                    } else {
                        ToastUtil.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
    }

    private void showUpdatedSuccessDialog() {
        if (mUpdateSuccessNoticeDialog == null) {
            mUpdateSuccessNoticeDialog = new UpdateSuccessNoticeDialog(this);
        }
        mUpdateSuccessNoticeDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_ensure:
                if (!SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM)) {
                    showPlatformUpdateDialog(null);
                } else {
                    smartManagement();
                }
                break;
            default:
                break;
        }
    }

    private void showPlatformUpdateDialog(PushModeBean.DataBean data) {
        mSelectUpdateDialog = new SelectUpdateDialog(this, mLlContent, data);
        DialogHelper.resizeFull(this, mSelectUpdateDialog);
        mSelectUpdateDialog.setOnClickCancelListener(() -> {
            mSelectUpdateDialog.dismiss();
            mSelectUpdateDialog = null;
            BaiduMtjUtils.eventId("升级引导_考虑一下");
            BaiduMtjUtils.eventEnd("升级引导_停留时长");
        });
        mSelectUpdateDialog.setOnClickOkListener(() -> {
            showUpdateNoticeDialog();
            BaiduMtjUtils.eventId("升级引导_立刻升级");
            BaiduMtjUtils.eventEnd("升级引导_停留时长");
        });
        mSelectUpdateDialog.show();
        BaiduMtjUtils.eventId("升级引导_弹出");
        BaiduMtjUtils.eventStart("升级引导_停留时长");
        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_SHOW_TIME + MyApplication.getUserId(), DateUtil.formatYYMD(SystemClock.currentThreadTimeMillis()));
    }

    private void showUpdateNoticeDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(this, getString(R.string.dialog_update_title), getString(R.string.dialog_update_notice), "返回", "确认升级", R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    mSelectUpdateDialog.dismiss();
                    mSelectUpdateDialog = null;
                    BaiduMtjUtils.eventId("升级引导_确认升级");
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        ServerClient.newInstance(MyApplication.getContext()).switchNewPlatform(MyApplication.getContext(), "TAG_SWITCH_NEW_PLATFORM" + mSimpleName, null);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            });
            mSelectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    BaiduMtjUtils.eventId("升级引导_返回");
                }
            });
            DialogHelper.resize(this, mSelectDialog);
        }
        mSelectDialog.show();
    }


}
