package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.StoreGroupListAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ManagerListBean;
import com.hstypay.enterprise.bean.StoreGroupBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述: 门店所属分组列表选择
 */
public class StoreGroupListActivity extends BaseActivity implements View.OnClickListener, StoreGroupListAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvNull, mButton;
    private RecyclerView mRvShop;
    private EditText mEtInput;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<StoreGroupBean.StoreGroupItemData> mList = new ArrayList<>();
    private StoreGroupListAdapter mAdapter;
    private Button mBtn_complete;
    private String intentName;
    private ImageView mIvClean;
    private String mStoreId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        currentPage = 2;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mRvShop.setLayoutManager(new LinearLayoutManager(this));
        mEtInput = (EditText) findViewById(R.id.et_user_input);
        mTvTitle.setText(R.string.title_select_shop_group);
        mButton.setVisibility(View.GONE);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mEtInput.setHint(getResources().getString(R.string.tx_group_search_input));
        View layout_search_header = findViewById(R.id.layout_search_header);
        layout_search_header.setVisibility(View.GONE);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtInput.isFocused()) {
                    if (mEtInput.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                        mList.clear();
                        setHeader(intentName, mList);
                        loadData("15", "1", "");
                    }
                }
            }
        });
        mEtInput.addTextChangedListener(editTextWatcher);
        mEtInput.setOnFocusChangeListener(listener);
        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_user_input:
                    if (hasFocus) {
                        if (mEtInput.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };

    private void initRecyclerView() {
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO 触发刷新数据任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData("15", "1", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                //TODO 触发加载更多任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        mLinearLayoutManager.setScrollEnabled(false);
                        mRvShop.setLayoutManager(mLinearLayoutManager);
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        mLinearLayoutManager.setScrollEnabled(false);
                        mRvShop.setLayoutManager(mLinearLayoutManager);
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mButton.setOnClickListener(this);
    }

    public void initData() {
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_VALUE_ADD_STORE.equals(intentName)){
            //是新增编辑门店，要点击确定按钮才选择完成
            mButton.setVisibility(View.VISIBLE);
            mButton.setText("确定");
        }
        mStoreId = getIntent().getStringExtra(Constants.INTENT_GROUP_ID);//groupID
        mList = new ArrayList<>();
        mAdapter = new StoreGroupListAdapter(StoreGroupListActivity.this, mList);
        mAdapter.setOnItemClickListener(this);
        mRvShop.setAdapter(mAdapter);
        //mRvShop.setVisibility(View.GONE);
        setHeader(intentName, mList);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        loadData("15", "1", mEtInput.getText().toString().trim());
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void loadData(String pageSize, String currentPage, String groupName) {
        if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
            showNewLoading(true, getString(R.string.public_loading));
        }
        //门店网络请求
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        if (!TextUtils.isEmpty(groupName)) {
            map.put("groupName", groupName);
        }
        ServerClient.newInstance(MyApplication.getContext()).choiceStoreGroup(MyApplication.getContext(), Constants.TAG_CHOICE_STORE, map);
    }

    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_STORE)) {
            StoreGroupBean msg = (StoreGroupBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreGroupListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSwipeRefreshLayout.setLoadmoreEnable(true);
                        //mRvShop.setVisibility(View.VISIBLE);
                        //mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData().getData());
                        for (StoreGroupBean.StoreGroupItemData itemData:mList){
                            String userId = itemData.getGroupId();
                            if (!TextUtils.isEmpty(mStoreId)&& mStoreId.equals(userId)){
                                itemData.setSelected(true);
                            }else {
                                itemData.setSelected(false);
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            //mRvShop.setVisibility(View.VISIBLE);
                            //mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            /*mRvShop.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setLoadmoreEnable(false);*/
                        }
                    }
                    break;
            }
            dismissLoading();
            mLinearLayoutManager.setScrollEnabled(true);
            mRvShop.setLayoutManager(mLinearLayoutManager);
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            setHeader(intentName, mList);
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                // mList.clear();
                mEtInput.setText("");
                mIvClean.setVisibility(View.GONE);
                break;
            case R.id.button_title:
                StoreGroupBean.StoreGroupItemData selectDataBean = null;
                for (StoreGroupBean.StoreGroupItemData dataBean:mList){
                    if (dataBean.isSelected()){
                        selectDataBean = dataBean;
                        break;
                    }
                }
                Intent intent = new Intent();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable(Constants.RESULT_STORE_GROUP_BEAN_INTENT, selectDataBean);   //传递一个user对象列表
                intent.putExtras(mBundle);
                setResult(RESULT_OK, intent);
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        if (Constants.INTENT_VALUE_ADD_STORE.equals(intentName)){
            //是新增编辑门店，要点击确定按钮才选择完成
            boolean selected = mList.get(position).isSelected();
            mList.get(position).setSelected(!selected);
            for (int i=0;i<mList.size();i++){
                if (i != position){
                    mList.get(i).setSelected(false);
                }
            }
            mAdapter.notifyDataSetChanged();
        }else {
            StoreGroupBean.StoreGroupItemData dataBean = mList.get(position);
            if (dataBean != null) {
                Intent intent = new Intent();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable(Constants.RESULT_STORE_GROUP_BEAN_INTENT, dataBean);   //传递一个user对象列表
                intent.putExtras(mBundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    public void setHeader(String name, List<StoreGroupBean.StoreGroupItemData> list) {
        if (Constants.INTENT_NAME_BILL_SHOP.equals(name) || Constants.INTENT_NAME_COLLECT_SHOP.equals(name)) {
            StoreGroupBean.StoreGroupItemData dataEntity = new StoreGroupBean.StoreGroupItemData();
            dataEntity.setGroupName("全部门店分组");
            dataEntity.setGroupId("");
            list.add(dataEntity);
        }
    }

}
