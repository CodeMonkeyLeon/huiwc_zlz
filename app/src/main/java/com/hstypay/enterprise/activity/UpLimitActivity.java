package com.hstypay.enterprise.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ImageBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LicenseDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.ImageFactory;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

import static com.hstypay.enterprise.utils.AppHelper.getPicPath;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/24 10:09
 * @描述: 商户提额
 */
public class UpLimitActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvCheckStatus;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle, mTvNotice, mTvReason;
    private LinearLayout mLlLicenseNotice, mLlLicenseRecognition, mLlExamineStatus;
    private RelativeLayout ly_phone_one;
    private LinearLayout ly_first;
    private ImageView iv_pic_one;
    private EditTextDelete mEtName, mEtLicenseNo, mEtShortName;

    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private String ispicture;
    private File tempFile;
    private Uri originalUri;
    private String picOnePath, imageUrl;
    private boolean takepic = false;
    private String picOnePath1;
    private String mLicenseImg;
    private SafeDialog mLoadDialog;
    private LicenseDetailBean.DataEntity mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_limit);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(UpLimitActivity.this, UIUtils.getString(R.string.public_loading), false);

        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.tx_up_limit);
        mButton.setVisibility(View.INVISIBLE);

        mBtnSubmit = (Button) findViewById(R.id.btn_save);
        mLlLicenseNotice = (LinearLayout) findViewById(R.id.ll_license_notice);
        mLlLicenseRecognition = (LinearLayout) findViewById(R.id.ll_license_recognition);
        mLlExamineStatus = (LinearLayout) findViewById(R.id.ll_examine_status);
        mIvCheckStatus = (ImageView) findViewById(R.id.iv_check_status);
        mTvNotice = (TextView) findViewById(R.id.tv_merchant_notice);
        mTvReason = (TextView) findViewById(R.id.tv_merchant_reason);
        ly_phone_one = (RelativeLayout) findViewById(R.id.ly_phone_one);
        ly_first = (LinearLayout) findViewById(R.id.ly_first);
        iv_pic_one = (ImageView) findViewById(R.id.iv_pic_one);

        mEtName = (EditTextDelete) findViewById(R.id.et_merchant_name);
        mEtLicenseNo = (EditTextDelete) findViewById(R.id.et_license_no);
        mEtShortName = (EditTextDelete) findViewById(R.id.et_merchant_short);

        setButtonEnable(mBtnSubmit, false);
    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        ly_phone_one.setOnClickListener(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setButtonView();
            }
        };
        mEtName.addTextChangedListener(textWatcher);
        mEtShortName.addTextChangedListener(textWatcher);
        mEtLicenseNo.addTextChangedListener(textWatcher);

    }

    public void initData() {
        mData = (LicenseDetailBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_LICENSE_DETAL);
        if (mData != null && !dataIsNull(mData)) {
            if (!TextUtils.isEmpty(mData.getMerchantName())) {
                mEtName.setText(mData.getMerchantName());
            }
            if (!TextUtils.isEmpty(mData.getLicenseCode())) {
                mEtLicenseNo.setText(mData.getLicenseCode());
            }
            if (!TextUtils.isEmpty(mData.getMerchantShortName())) {
                mEtShortName.setText(mData.getMerchantShortName());
            }
            if (!StringUtils.isEmptyOrNull(mData.getLicensePhoto())) {
                Picasso.get().load(Constants.H5_BASE_URL + mData.getLicensePhoto()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(iv_pic_one);
            }
            mLlLicenseNotice.setVisibility(View.GONE);
            mLlExamineStatus.setVisibility(View.VISIBLE);
            if (mData.getExamineStatus() == 0) {
                ly_phone_one.setEnabled(false);
                mEtName.setEnable(false);
                mEtName.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                mEtLicenseNo.setEnable(false);
                mEtLicenseNo.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                mEtShortName.setEnable(false);
                mEtShortName.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                mIvCheckStatus.setImageResource(R.mipmap.icon_examine);
                mLlLicenseRecognition.setVisibility(View.GONE);
                mBtnSubmit.setVisibility(View.GONE);
                mTvReason.setVisibility(View.GONE);
                mTvNotice.setText(UIUtils.getString(R.string.tx_checking));
            } else if (mData.getExamineStatus() == 2) {
                mIvCheckStatus.setImageDrawable(UIUtils.getResource().getDrawable(R.mipmap.icon_examine_fail));
                mLlLicenseRecognition.setVisibility(View.VISIBLE);
                mBtnSubmit.setVisibility(View.VISIBLE);
                mTvReason.setVisibility(View.VISIBLE);
                mTvReason.setText(mData.getExamineRemark());
                mTvNotice.setText(UIUtils.getString(R.string.notice_failed_limit_tx));
            }
        } else {
            mBtnSubmit.setText(UIUtils.getString(R.string.submit));
            mLlLicenseNotice.setVisibility(View.VISIBLE);
            mLlExamineStatus.setVisibility(View.GONE);
        }
        setButtonView();
    }

    public boolean getButtonState() {
        return mEtName.getText().toString().trim().length() == 0 || mEtShortName.getText().toString().trim().length() == 0
                || mEtLicenseNo.getText().toString().trim().length() == 0 || TextUtils.isEmpty(mLicenseImg);
    }

    private void setButtonView() {
        if (getButtonState()) {
            setButtonEnable(mBtnSubmit, false);
        } else {
            if (!mBtnSubmit.isEnabled()) {
                setButtonEnable(mBtnSubmit, true);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //营业执照
            case R.id.ly_phone_one:
                ispicture = "1";
                choice();
                break;
            case R.id.btn_save://保存
                submit();
                break;
            default:
                break;
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(UpLimitActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    boolean results = PermissionUtils.checkPermissionArray(UpLimitActivity.this, permissionArray);
                    if (results) {
                        startCamera();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_business_photo));
                    }
                } else {
                    showCommonNoticeDialog(UpLimitActivity.this, getString(R.string.tx_sd_pic));
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    boolean results = PermissionUtils.checkPermissionArray(UpLimitActivity.this, permissionArray);
                    if (results) {
                        takeImg();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_business_photo));
                    }
                } else {
                    showCommonNoticeDialog(UpLimitActivity.this, getString(R.string.tx_sd_pic));
                }
            }
        });
        picPopupWindow.showAtLocation(iv_pic_one, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";

            tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()){
                tempFile.getParentFile().mkdirs();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(UpLimitActivity.this,
                        Constants.FILE_PROVIDER, tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submit() {
        if (mEtName.getText().toString().trim().contains(" ")) {
            MyToast.showToastShort(UIUtils.getString(R.string.regex_merchant_name));
            mEtName.setFocusable(true);
            mEtName.setFocusableInTouchMode(true);
            mEtName.requestFocus();
            return;
        }

        if (mEtShortName.getText().toString().trim().contains(" ")) {
            MyToast.showToastShort(UIUtils.getString(R.string.regex_merchant_short_name));
            mEtShortName.setFocusable(true);
            mEtShortName.setFocusableInTouchMode(true);
            mEtShortName.requestFocus();
            return;
        }

        if (!StringUtils.isLicenseNo(mEtLicenseNo.getText().toString().trim())) {
            MyToast.showToastShort(UIUtils.getString(R.string.regex_merchant_license_no));
            mEtLicenseNo.setFocusable(true);
            mEtLicenseNo.setFocusableInTouchMode(true);
            mEtLicenseNo.requestFocus();
            return;
        }

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("licensePhoto", mLicenseImg);
            map.put("merchantName", mEtName.getText().toString().trim());
            map.put("licenseCode", mEtLicenseNo.getText().toString().trim());
            map.put("merchantShortName", mEtShortName.getText().toString().trim());
            ServerClient.newInstance(MyApplication.getContext()).editLicense(MyApplication.getContext(), Constants.TAG_EDIT_LICENSE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void UploadImage() {

        if (!NetworkUtils.isNetworkAvailable(UpLimitActivity.this)) {
            showCommonNoticeDialog(UpLimitActivity.this, getString(R.string.network_exception));
            return;
        }
        showNewLoading(true, getString(R.string.public_uploading));

        String url = Constants.BASE_URL + "merchant/upload/file";
        PostFormBuilder post = OkHttpUtils.post();
        if (!TextUtils.isEmpty(picOnePath)) {
            //picOnePath1 = picOnePath.substring(0, picOnePath.lastIndexOf(".")) + "(1).jpg";
            picOnePath1 = AppHelper.getImageCacheDir(picOnePath);
            LogUtil.d("pic====" + picOnePath + ",,," + picOnePath1);
            ImageFactory.compressPicture(picOnePath, picOnePath1);
            post = post.addFile("licenseImg", "licenseImg", new File(picOnePath1));
        }
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                dismissLoading();
                MyToast.showToastShort("上传失败");
                FileUtils.deleteFile(picOnePath1);
            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(picOnePath1);
                dismissLoading();
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null) {
                    if (imageBean.isStatus()) {
                        ImageBean.DataEntity imageInfo = imageBean.getData();
                        if (imageInfo != null) {
                            mLicenseImg = imageInfo.getLicenseImg();
                            setButtonView();
                        }
                        if (imageInfo != null && imageInfo.getBusinessLicense() != null) {
                            mEtName.setText(imageInfo.getBusinessLicense().getName());
                            mEtLicenseNo.setText(imageInfo.getBusinessLicense().getCreditno());
                            setButtonView();
                        }
                    } else {
                        if (imageBean.getError() != null && imageBean.getError().getMessage() != null) {
                            MyToast.showToastShort(imageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    //Eventbus接收数据，退款权限返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_EDIT_LICENSE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(UpLimitActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(UpLimitActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialog();
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }


    public void getDialog() {
        NoticeDialog dialogInfo = new NoticeDialog(UpLimitActivity.this, null, null, R.layout.notice_dialog_verify);
        dialogInfo.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                UpLimitActivity.this.finish();
            }
        });
        dialogInfo.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = AppHelper.getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        takepic = false;
                        /*File file = new File(path);
                        int MAX_POST_SIZE = 8 * 1024 * 1024;
                        if (file.exists() && file.length() > MAX_POST_SIZE) {
                            showCommonNoticeDialog(UpLimitActivity.this, getString(R.string.tv_pic_than_5MB));
                            return;
                        } else {
                            bitmap = ImagePase.readBitmapFromStream(path);
                        }*/
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            if (ispicture.equals("1")) {
                                picOnePath = path;
                                ly_first.setVisibility(View.GONE);
                                ly_phone_one.setBackground(new BitmapDrawable(bitmap));
                                UploadImage();
                            }
                        }
                    }
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = getPicPath(originalUri);
                    takepic = true;
                    /*Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (file.exists() && file.length() / 1024 > 100) {
                        bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    } else {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                            bitmap_pci = ImagePase.createBitmap(imageUrl, ImagePase.bitmapSize_Image);
                        }
                    }*/
                    Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        if (ispicture.equals("1")) {
                            picOnePath = pathPhoto;
                            ly_first.setVisibility(View.GONE);
                            ly_phone_one.setBackground(new BitmapDrawable(bitmap_pci));
                            UploadImage();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private boolean dataIsNull(LicenseDetailBean.DataEntity data) {
        if (data == null)
            return true;
        return data.getLicensePhoto() == null && data.getLicenseCode() == null
                && data.getExamineRemark() == null && data.getExamineStatusCnt() == null
                && data.getMerchantShortName() == null && data.getMerchantName() == null;
    }

}
