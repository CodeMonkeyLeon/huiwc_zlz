package com.hstypay.enterprise.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.ums.upos.sdk.system.BaseSystemManager;
import com.ums.upos.sdk.system.OnServiceStatusListener;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

public class VerifyActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnPrint, mBtnComplete;
    private TextView mButton, mTvTitle, mTvRefundMoney, mTvApplyDate, mTvConductor, mTvRefundCode;
    private PayBean.DataBean mRefundDatas;
    private SafeDialog mLoadDialog;
    private PosPrintUtil mPosPrintUtil;
    private Printer printer;
    private IWoyouService woyouService;
    private PrintUtils mPrintUtils;
    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private String mIntentName;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
            autoPrint();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
            autoPrint();
        }
    };

    /**
     * 绑定商米打印服务
     **/
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        initEvent();
        initData();
        initPosPrint();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        } else if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                try {
                    BaseSystemManager.getInstance().deviceServiceLogout();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.MIBAGPAY_LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            autoPrint();
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
            autoPrint();
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
            autoPrint();
        } else if ("a920".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("hdy".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if (Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                //联迪银商
                try {
                    BaseSystemManager.getInstance().deviceServiceLogin(
                            this, null, "99999998",//设备ID，生产找后台配置
                            new OnServiceStatusListener() {
                                @Override
                                public void onStatus(int arg0) {//arg0可见ServiceResult.java
                                    if (0 == arg0 || 2 == arg0 || 100 == arg0) {//0：登录成功，有相关参数；2：登录成功，无相关参数；100：重复登录。
                                        autoPrint();
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                autoPrint();
            }
        }
        if (AppHelper.getAppType() == 2 && MyApplication.autoPrint()) {
            autoPrint();
        }
    }

    private void autoPrint() {
        if (Constants.INTENT_NAME_CHECK_VERIFY.equals(mIntentName)) {
            if (mRefundDatas != null) {
                mRefundDatas.setPay(false);
                mRefundDatas.setMoney(mRefundDatas.getRefundMoney());
                mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, getTradeDetail(mRefundDatas), true);
            }
            mIntentName = "";
        }
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mTvRefundMoney = (TextView) findViewById(R.id.tv_refund_money);
        mTvApplyDate = (TextView) findViewById(R.id.tv_apply_date);
        mTvConductor = (TextView) findViewById(R.id.tv_refund_conductor);
        mTvRefundCode = (TextView) findViewById(R.id.tv_refund_code);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mBtnComplete = findViewById(R.id.btn_complete);

        mTvTitle.setText(R.string.checking_tx);
        mButton.setVisibility(View.INVISIBLE);
        setButtonWhite(mBtnPrint);
        setButtonEnable(mBtnComplete, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mBtnComplete.setOnClickListener(this);
    }

    public void initData() {
        getPrintActive();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mRefundDatas = (PayBean.DataBean) getIntent().getSerializableExtra("refundDatas");
        if (mRefundDatas != null) {
            String refundMoney = mRefundDatas.getRefundMoney() + "";
            if (!StringUtils.isEmptyOrNull(refundMoney)) {
                mTvRefundMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Long.parseLong(refundMoney) / 100d));
            }

            if (!StringUtils.isEmptyOrNull(mRefundDatas.getRefundTime())) {
                mTvApplyDate.setText(mRefundDatas.getRefundTime());
            }
            if (!StringUtils.isEmptyOrNull(mRefundDatas.getRefundUserRealName())) {
                mTvConductor.setText(mRefundDatas.getRefundUserRealName());
            }

            if (!StringUtils.isEmptyOrNull(mRefundDatas.getRefundNo())) {
                mTvRefundCode.setText(mRefundDatas.getRefundNo());
            }
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
            case R.id.btn_complete:
                VerifyActivity.this.finish();
                Intent intent = new Intent(Constants.ACTION_ORDER_REFUND_DATA);
                sendBroadcast(intent);
                break;
            case R.id.blue_print:
                //bluetoothPrint();
                getPrint();
                break;
            default:
                break;
        }
    }

    public void getPrint() {
        /*if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.LIANDI)) {
            bindDeviceService();
        } else */
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        if (mRefundDatas != null) {
            mRefundDatas.setPay(false);
            mRefundDatas.setMoney(mRefundDatas.getRefundMoney());
            mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, getTradeDetail(mRefundDatas), false);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderState(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(VerifyActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(VerifyActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            VerifyActivity.this.finish();
            Intent intent = new Intent(Constants.ACTION_ORDER_REFUND_DATA);
            sendBroadcast(intent);
        }

        return super.onKeyDown(keyCode, event);
    }

    private TradeDetailBean getTradeDetail(PayBean.DataBean data) {
        TradeDetailBean tradeDetailBean = new TradeDetailBean();
        if (data != null) {
            tradeDetailBean.setStoreMerchantIdCnt(data.getStoreMerchantIdCnt());
            tradeDetailBean.setOrderNo(data.getOrderNo());
            tradeDetailBean.setTransactionId(data.getTransactionId());
            tradeDetailBean.setTradeTime(data.getTradeTime());
            tradeDetailBean.setTradeState(data.getTradeState());
            tradeDetailBean.setCashierName(data.getCashierName());
            tradeDetailBean.setOpUserRealName(data.getOpUserRealName());
            tradeDetailBean.setApiProvider(data.getApiProvider());
            tradeDetailBean.setApiCode(data.getApiCode());
            tradeDetailBean.setMoney(data.getMoney());
            tradeDetailBean.setRealMoney(data.getRealMoney());
            tradeDetailBean.setPayMoney(data.getPayMoney());
            tradeDetailBean.setCouponFee(data.getMchDiscountsMoney());
            tradeDetailBean.setCouponInfoList(data.getCouponInfoList());
            tradeDetailBean.setAttach(data.getAttach());
            tradeDetailBean.setOutTradeNo(data.getOutTradeNo());
            tradeDetailBean.setReqOrderNo(data.getReqOrderNo());
            tradeDetailBean.setOriReqOrderNo(data.getOriReqOrderNo());
            tradeDetailBean.setVoucherNo(data.getVoucherNo());
            tradeDetailBean.setCashPointName(data.getCashPointName());

            tradeDetailBean.setOpenid(data.getOpenid());
            tradeDetailBean.setThirdMerchantId(data.getThirdMerchantId());
            tradeDetailBean.setThirdOrderNo(data.getThirdOrderNo());
            tradeDetailBean.setTradeCode(data.getTradeCode());
            tradeDetailBean.setDeviceSn(StringUtils.getDeviceInfo(AppHelper.getSN()));

            tradeDetailBean.setRefundNo(data.getRefundNo());
            tradeDetailBean.setRefundTime(data.getRefundTime());
            tradeDetailBean.setRefundUser(data.getRefundUserRealName());
            tradeDetailBean.setRefundStatus(data.getRefundStatus());
            tradeDetailBean.setRefundMoney(data.getRefundMoney());
            tradeDetailBean.setRefundUpdateTime(data.getRefundUpdateTime());
            tradeDetailBean.setStandby4(getIntent().getStringExtra(Constants.INTENT_REMARK));
        }

        return tradeDetailBean;
    }
}
