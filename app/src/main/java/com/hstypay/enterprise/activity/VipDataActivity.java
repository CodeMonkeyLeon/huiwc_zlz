package com.hstypay.enterprise.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;


public class VipDataActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout mLlData, mLlShop, mLlSalesman;
    private ImageView mIvBack, mIvCashierArrow, mIvShopArrow;
    private String startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
    private String endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
    private String userId;
    private String storeMerchantId;
    private int tab = 2;
    private String storeName;
    private String cashierName;

    private TextView tv_date, mTvNull, mTvShop, mTvCashier, mTvTitle;
    private TextView mTvNewVipCount, mTvComeVipCount, mTvRechargeMoney, mTvRechargeCount, mTvConsumeMoney, mTvConsumeCount, mButton;
    private ScrollView mSvReport;
    private ReportBean.DataEntity mInfo;
    private ReportBroadcastReceiver mReportBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_data);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLlData = (LinearLayout) findViewById(R.id.ll_date);
        mLlShop = (LinearLayout) findViewById(R.id.ll_shop);
        mTvShop = (TextView) findViewById(R.id.tv_shop);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);
        mLlSalesman = (LinearLayout) findViewById(R.id.ll_cashier);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + getString(R.string.public_today) + ")");

        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mTvTitle.setText(getString(R.string.title_vip_data));
        mButton.setText(getString(R.string.tv_collect));

        mSvReport = (ScrollView) findViewById(R.id.sv_report);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mSvReport.setVisibility(View.GONE);
        mTvNull.setVisibility(View.GONE);
        mIvCashierArrow = (ImageView) findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = (ImageView) findViewById(R.id.iv_shop_arrow);

        mTvNewVipCount = (TextView) findViewById(R.id.tv_new_vip_count);
        mTvComeVipCount = (TextView) findViewById(R.id.tv_come_vip_count);
        mTvRechargeMoney = (TextView) findViewById(R.id.tv_vip_recharge_money);
        mTvRechargeCount = (TextView) findViewById(R.id.tv_vip_recharge_count);
        mTvConsumeMoney = (TextView) findViewById(R.id.tv_vip_consume_money);
        mTvConsumeCount = (TextView) findViewById(R.id.tv_new_vip_count);

        if (MyApplication.getIsCasher()) {
            mIvCashierArrow.setVisibility(View.GONE);
            mTvCashier.setText(MyApplication.getRealName());
            userId = MyApplication.getUserId();
            mLlSalesman.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            storeMerchantId = MyApplication.getDefaultStore();
            mLlShop.setEnabled(false);
        }
    }

    public void initEvent() {
        mLlData.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlSalesman.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
    }

    public void initData() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            getReport(getRequestMap(startTime, endTime, userId, null));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_REPORT)) {
            ReportBean msg = (ReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipDataActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_REPORT_TRUE://请求成功
                    if (MyApplication.getIsCasher()) {
                        sendTodayDataBroadcast(this);
                    }
                    if (msg.getData() != null) {
                        mInfo = msg.getData();
                        mInfo.setType(tab);
                        mInfo.setCashierName(cashierName);
                        mInfo.setStoreName(storeName);
                        mTvNull.setVisibility(View.GONE);
                        mSvReport.setVisibility(View.VISIBLE);
                        setView(msg.getData());
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSvReport.setVisibility(View.GONE);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("tab", tab);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }

    public void getReport(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).getReport(MyApplication.getContext(), Constants.TAG_GET_REPORT, map);
        } else {
            mTvNull.setText("网络异常");
            mTvNull.setVisibility(View.VISIBLE);
            mSvReport.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissLoading();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back://titlebar返回按钮
                finish();
                break;
            case R.id.ll_date:
                Intent intentDate = new Intent(VipDataActivity.this, DateChoiceActivity.class);
                intentDate.putExtra(Constants.INTENT_NAME, Constants.INTENT_REPORT_CHOICE_DATE);
                startActivityForResult(intentDate, Constants.REQUEST_CHOICE_DATE);
                break;
            case R.id.ll_shop:
                Intent intentShop = new Intent(this, ShopActivity.class);
                if (TextUtils.isEmpty(storeMerchantId)) {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                } else {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                }
                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_COLLECT_SHOP);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.ll_cashier:
                Intent intentCashier = new Intent(this, CashierActivity.class);
                if (TextUtils.isEmpty(userId)) {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                } else {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                }
                intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                break;
            case R.id.btn_print:
                bluetoothPrint();
                break;
            case R.id.button_title:
                startActivity(new Intent(VipDataActivity.this, VipDataCollectActivity.class));
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeName = shopBean.getStoreName();
            storeMerchantId = shopBean.getStoreId();
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText("全部收银员");
                cashierName = "全部收银员";
                userId = "";
            }
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            cashierName = cashierBean.getRealName();
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHOICE_DATE) {
            Bundle extras = data.getExtras();
            String time = extras.getString(Constants.RESULT_CHOICE_DATE_INTENT);
            if (!StringUtils.isEmptyOrNull(time)) {
                String t[] = time.split("\\|");
                try {
                    if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formartDateToMMDD(t[1]))) {
                        if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                            tv_date.setText(DateUtil.formartDateToMMDD(t[0]) + "(今天)");
                        } else {
                            tv_date.setText(DateUtil.formartDateToMMDD(t[0]));
                        }
                    } else {
                        tv_date.setText(DateUtil.formartDateToMMDD(t[0]) + " " + getString(R.string.tv_least) + " "
                                + DateUtil.formartDateToMMDD(t[1]));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                startTime = t[0];
                endTime = t[1];
                LogUtil.d("date=" + startTime + "///" + endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        }
    }

    private void bluetoothPrint() {
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            showPrintDialog(1);
            return;
        }

        if (mInfo != null) {
            if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                mInfo.setStartTime(startTime);
                mInfo.setEndTime(endTime);
            } else {
                mInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                mInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
        }


        /*if (MyApplication.bluetoothSocket != null && MyApplication.bluetoothSocket.isConnected()) {
            BluePrintUtil.printDateSum(mInfo);
        } else {*/
        if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
            new Thread(){
                @Override
                public void run() {
                    super.run();
                    boolean isSucc = BluePrintUtil.blueConnent(1, VipDataActivity.this);
                    if (isSucc) {
                        BluePrintUtil.printDateSum(mInfo);
                    }else {
                        if (BluetoothAdapter.getDefaultAdapter().isEnabled()){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showPrintDialog(1);
                                }
                            });
                        }else {
                            //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                            LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                        }
                    }
                }
            }.start();

        } else {
            showPrintDialog(1);
        }
//        }
    }

    public void setView(ReportBean.DataEntity dataEntity) {
        if (dataEntity != null) {

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_REPORT_DATA);
        mReportBroadcastReceiver = new ReportBroadcastReceiver();
        registerReceiver(mReportBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mReportBroadcastReceiver);
    }

    class ReportBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        }
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }
}
