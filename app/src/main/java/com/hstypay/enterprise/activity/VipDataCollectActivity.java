package com.hstypay.enterprise.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class VipDataCollectActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle,mTvVipCount,mTvVipMoney,mTvRechargeMoney,mTvRechargeCount,mTvConsumeMoney,mTvConsumeCount, mButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_data_collect);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mTvTitle.setText(getString(R.string.tv_collect));
        mButton.setVisibility(View.INVISIBLE);

        mTvVipCount = (TextView) findViewById(R.id.tv_collect_vip_count);//累计会员个数
        mTvVipMoney = (TextView) findViewById(R.id.tv_collect_vip_money);//累计会员余额
        mTvRechargeMoney = (TextView) findViewById(R.id.tv_collect_recharge_money);//累计充值金额
        mTvRechargeCount = (TextView) findViewById(R.id.tv_collect_recharge_count);//累计充值笔数
        mTvConsumeMoney = (TextView) findViewById(R.id.tv_collect_consume_money);//累计消费金额
        mTvConsumeCount = (TextView) findViewById(R.id.tv_collect_consume_count);//累计充值笔数
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    public void initData() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {

        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_REPORT)) {
            ReportBean msg = (ReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipDataCollectActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_REPORT_TRUE://请求成功

                    break;
            }
            dismissLoading();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissLoading();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back://titlebar返回按钮
                finish();
                break;
            default:
                break;
        }
    }
}
