package com.hstypay.enterprise.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.BuildConfig;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.LaunchAdvertiseBean;
import com.hstypay.enterprise.bean.LoginBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.OkHttpUtil;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.CheckRoot;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SignCheck;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.List;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class WelcomeActivity extends BaseActivity implements View.OnClickListener {
    private CommonNoticeDialog mDialogFree;
    private RelativeLayout mRlAdv;
    private GifImageView mIvAdv;
    private LinearLayout mLlAdvTime;
    private TextView mTvAdvTime;
    private String url;
    private TimeCount time;
    private String advUrl;
    private static final int AUTO_LOGINING = 1;
    private static final int AUTO_LOGIN_SUCCESS = 2;
    private static final int AUTO_LOGIN_FAILED = 3;
    private int loginStatus = 0;
    private boolean isClickAdv;
    private boolean isTimerFinish;
    private boolean isGetImageFialed;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1://第一次使用
                    startActivity(new Intent(WelcomeActivity.this, GuideActivity.class));
                    finish();
                    break;
                case 2:
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        loginStatus = AUTO_LOGINING;
                        ServerClient.newInstance(WelcomeActivity.this).autoPwd(WelcomeActivity.this, Constants.TAG_AUTO_PWD);
                    } else {
                        startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                        WelcomeActivity.this.finish();
                    }
                    break;
                case 3:
                    showImage();
                    break;
                case 4:
                    skipLogin();
                    break;
            }
        }
    };
    private SelectDialog mSelectDialog;
    private NoticeDialog mNoticeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if (AppHelper.getApkType() == 2) {
            StatusBarUtil.setTranslucentStatus(this);
        } else {
            StatusBarUtil.setImmersiveStatusBar(this, true);
        }
        mRlAdv = (RelativeLayout) findViewById(R.id.rl_adv);
        mIvAdv = (GifImageView) findViewById(R.id.iv_adv);
        mLlAdvTime = (LinearLayout) findViewById(R.id.ll_adv_time);
        mTvAdvTime = (TextView) findViewById(R.id.tv_adv_time);

        mIvAdv.setOnClickListener(this);
        mLlAdvTime.setOnClickListener(this);
        SpUtil.putInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, loginStatus);

        // 避免从桌面启动程序后，会重新实例化入口类的activity
        if (!this.isTaskRoot()) {
            Intent intent = getIntent();
            if (intent != null) {
                String action = intent.getAction();
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                    finish();
                    return;
                }
            }
        }

        SignCheck signCheck;
        if (/*AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.WJY)
                && !*/Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            signCheck = new SignCheck(this, "48:0E:49:73:C5:57:FF:F9:0B:D9:6B:FA:EF:DE:9E:E2:21:83:18:86");
        } else {
            signCheck = new SignCheck(this, "3C:B5:60:C4:4C:AF:72:00:05:32:C3:0B:54:BE:BA:83:9C:88:10:43");
        }
        if (BuildConfig.IS_OFFICIAL && !signCheck.check()) {
            if (mNoticeDialog == null) {
                mNoticeDialog = new NoticeDialog(WelcomeActivity.this, getString(R.string.notice_sign_check), null, R.layout.notice_dialog_common);
                mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                    @Override
                    public void clickOk() {
                        WelcomeActivity.this.finish();
                        MyApplication.getInstance().finishAllActivity();
                    }
                });
            }
            mNoticeDialog.show();
        } else {
            boolean allowContinue = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_ALLOW_ROOT_CONTINUE);
            if (CheckRoot.isDeviceRooted() && !allowContinue) {
                showRootDialog();
            } else {
                setFullFunc();
            }
        }
    }

    private void setFullFunc() {
        initData();
    }

    protected void initData() {
        boolean isNotFirstUse = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_NOT_FIRST_USE);
        if (isNotFirstUse) {
            if (!MyApplication.protocolShown() || !NetworkUtils.isNetWorkValid(MyApplication.getContext()) || MyApplication.isNeedUpdatePwd()) {
                mHandler.sendEmptyMessageDelayed(4, 1000);
            } else {
                if (!TextUtils.isEmpty(MyApplication.getCookies())) {
                    mHandler.sendEmptyMessageDelayed(2, 600);
                } else {
                    mHandler.sendEmptyMessageDelayed(4, 600);
                }
                if (ConfigUtil.advertisementEnable()) {
                    ServerClient.newInstance(MyApplication.getContext()).downloadImg(MyApplication.getContext(), Constants.TAG_DOWNLOAD_IMAGE, null);
                }
            }
        } else {
            mHandler.sendEmptyMessageDelayed(1, 1000);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //免密登录返回的数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAutoPwd(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_AUTO_PWD)) {
            dismissLoading();
            LoginBean msg = (LoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                case Constants.AUTO_PWD_FALSE:
                    loginStatus = AUTO_LOGIN_FAILED;
                    SpUtil.putInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, loginStatus);
                    /**
                     * 无启动页广告
                     */
                    if (!ConfigUtil.advertisementEnable()) {
                        autoLogin(loginStatus);
                    } else {
                        if (isTimerFinish || isGetImageFialed) {
                            autoLogin(loginStatus);
                        }
                    }
                    break;
                case Constants.AUTO_PWD_TRUE://登录成功
                    loginStatus = AUTO_LOGIN_SUCCESS;
                    SpUtil.putInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, loginStatus);
                    SpUtil.putString(MyApplication.getContext(), Constants.CASHIER_REFUND_TAG, "");
                    SpUtil.putString(MyApplication.getContext(), Constants.CASHIER_BILL_TAG, "");
                    SpUtil.putString(MyApplication.getContext(), Constants.CASHIER_REPORT_TAG, "");
                    SpUtil.putString(MyApplication.getContext(), Constants.FUNC_CODE_REFUND, "");
                    SpUtil.putString(MyApplication.getContext(), Constants.PERMISSIONS_REFUND, "");
                    if (msg.getData() != null) {
                        if (msg.getData().getOps() != null && msg.getData().getOps().getUserOps() != null && msg.getData().getOps().getUserOps().size() > 0) {
                            List<LoginBean.DataBean.OpsBean.UserOpsBean> userOps = msg.getData().getOps().getUserOps();
                            for (int u = 0; u < userOps.size(); u++) {
                                LoginBean.DataBean.OpsBean.UserOpsBean userOpsBean = userOps.get(u);
                                if (userOpsBean != null) {
                                    if (!StringUtils.isEmptyOrNull(userOpsBean.getOpCode())) {
                                        if (userOpsBean.getOpCode().equals("APPLY-REFUND")) {
                                            //收银员退款权限
                                            SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REFUND_TAG, userOpsBean.getOpCode());
                                        }
                                        if (userOpsBean.getOpCode().equals("APPLY-WATER")) {
                                            //收银员查看流水权限
                                            SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_BILL_TAG, userOpsBean.getOpCode());
                                        }
                                        if (userOpsBean.getOpCode().equals("APPLY-BILL")) {
                                            //收银员查看统计权限
                                            SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REPORT_TAG, userOpsBean.getOpCode());
                                        }
                                    }
                                } else {
                                    SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REFUND_TAG, "");
                                    SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_BILL_TAG, "");
                                    SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REPORT_TAG, "");
                                }
                            }
                        } else {
                            SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REFUND_TAG, "");
                            SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_BILL_TAG, "");
                            SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REPORT_TAG, "");
                        }

                        if (msg.getData().getFuncs() != null && msg.getData().getFuncs().size() > 0) {
                            List<LoginBean.DataBean.FuncsBean> funcs = msg.getData().getFuncs();
                            for (int i = 0; i < funcs.size(); i++) {
                                LoginBean.DataBean.FuncsBean s = funcs.get(i);
                                if (s != null) {
                                    if ("APP-OPERATION".equals(s.getFuncCode())) {
                                        SpUtil.putString(WelcomeActivity.this, Constants.FUNC_CODE_REFUND, s.getFuncCode());
                                    } else {
                                        SpUtil.putString(WelcomeActivity.this, Constants.FUNC_CODE_REFUND, "");
                                    }
                                    List<LoginBean.DataBean.FuncsBean.PermissionsBean> permissions = s.getPermissions();
                                    if (permissions != null && permissions.size() > 0) {
                                        for (int p = 0; p < permissions.size(); p++) {
                                            LoginBean.DataBean.FuncsBean.PermissionsBean permissionsBean = permissions.get(p);
                                            if (permissionsBean != null && "APPLY-REFUND".equals(permissionsBean.getPermissionCode())) {
                                                //有退款权限
                                                SpUtil.putString(WelcomeActivity.this, Constants.PERMISSIONS_REFUND, permissionsBean.getPermissionCode());
                                            } else {
                                                SpUtil.putString(WelcomeActivity.this, Constants.PERMISSIONS_REFUND, "");
                                            }
                                        }
                                    } else {
                                        SpUtil.putString(WelcomeActivity.this, Constants.PERMISSIONS_REFUND, "");
                                    }
                                } else {
                                    SpUtil.putString(WelcomeActivity.this, Constants.FUNC_CODE_REFUND, "");
                                    SpUtil.putString(WelcomeActivity.this, Constants.PERMISSIONS_REFUND, "");
                                }
                            }
                        } else {
                            SpUtil.putString(WelcomeActivity.this, Constants.FUNC_CODE_REFUND, "");
                            SpUtil.putString(WelcomeActivity.this, Constants.PERMISSIONS_REFUND, "");
                        }
                    }
                    if (msg.getData() != null) {
                        if (msg.getData().getOps() != null) {
                            if (msg.getData().getOps().getUserOps() != null && msg.getData().getOps().getUserOps().size() > 0) {
                                List<LoginBean.DataBean.OpsBean.UserOpsBean> userOps = msg.getData().getOps().getUserOps();
                                if (userOps != null && userOps.size() > 0) {
                                    for (int u = 0; u < userOps.size(); u++) {
                                        LoginBean.DataBean.OpsBean.UserOpsBean userOpsBean = userOps.get(u);
                                        if (userOpsBean != null) {
                                            if ("APPLY-REFUND".equals(userOpsBean.getOpCode())) {
                                                //收银员退款权限
                                                SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REFUND_TAG, userOpsBean.getOpCode());
                                            }
                                            if (userOpsBean.getOpCode().equals("APPLY-WATER")) {
                                                //收银员查看流水权限
                                                SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_BILL_TAG, userOpsBean.getOpCode());
                                            }
                                            if (userOpsBean.getOpCode().equals("APPLY-BILL")) {
                                                //收银员查看统计权限
                                                SpUtil.putString(WelcomeActivity.this, Constants.CASHIER_REPORT_TAG, userOpsBean.getOpCode());
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //=======
                        if (msg.getData().getFuncs() != null && msg.getData().getFuncs().size() > 0) {
                            List<LoginBean.DataBean.FuncsBean> funcs = msg.getData().getFuncs();
                            if (funcs != null) {
                                for (int i = 0; i < funcs.size(); i++) {
                                    LoginBean.DataBean.FuncsBean s = funcs.get(i);
                                    if (s != null) {
                                        if (!StringUtils.isEmptyOrNull(s.getFuncCode())) {
                                            String app = "APP-OPERATION";
                                            if (s.getFuncCode().equals(app)) {
                                                SpUtil.putString(WelcomeActivity.this, Constants.FUNC_CODE_REFUND, s.getFuncCode());
                                                String funcCode = s.getFuncCode();//APP-OPERATION

                                            }
                                        }
                                        List<LoginBean.DataBean.FuncsBean.PermissionsBean> permissions = s.getPermissions();
                                        if (permissions != null) {
                                            for (int p = 0; p < permissions.size(); p++) {
                                                LoginBean.DataBean.FuncsBean.PermissionsBean permissionsBean = permissions.get(p);
                                                if (permissionsBean != null) {
                                                    if (!StringUtils.isEmptyOrNull(permissionsBean.getPermissionCode())) {
                                                        String refund = "APPLY-REFUND";
                                                        if (permissionsBean.getPermissionCode().equals(refund)) {
                                                            //有退款权限
                                                            SpUtil.putString(WelcomeActivity.this, Constants.PERMISSIONS_REFUND, permissionsBean.getPermissionCode());

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (!ConfigUtil.advertisementEnable()) {
                        Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        if (isClickAdv) {
                            Intent intent = new Intent(this, RegisterActivity.class);
                            intent.putExtra(Constants.REGISTER_ALLOW_CLOSE, true);
                            intent.putExtra(Constants.REGISTER_INTENT, advUrl);
                            intent.putExtra(Constants.REGISTER_INTENT_URL, Constants.WELCOME_TO_REGISTER);
                            startActivity(intent);
                            finish();
                        } else {
                            if (isTimerFinish || isGetImageFialed) {
                                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                    /*Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();*/
                    break;
            }

        }

        if (event.getTag().equals(Constants.TAG_DOWNLOAD_IMAGE)) {
            LaunchAdvertiseBean msg = (LaunchAdvertiseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                case Constants.ON_EVENT_FALSE:
                    //mHandler.sendEmptyMessageDelayed(2, 500);
                    if (TextUtils.isEmpty(MyApplication.getCookies())) {
                        mHandler.sendEmptyMessageDelayed(4, 800);
                    } else {
                        isGetImageFialed = true;
                        autoLogin(loginStatus);
                    }
                    break;
                case Constants.ON_EVENT_TRUE://广告请求成功
                    if (msg.getData() != null && !TextUtils.isEmpty(msg.getData().getAdvImage())
                            && !TextUtils.isEmpty(getEnd(msg.getData().getAdvImage())) && msg.getData().getStatus() == 1) {
                        String end = getEnd(msg.getData().getAdvImage());
                        String endUrl = getUrlEnd(msg.getData().getAdvImage());
                        LogUtil.d("Constants---end--", end);
                        advUrl = msg.getData().getAdvUrl();
                        url = getExternalFilesDir("adv").getAbsolutePath() + "/" + msg.getData().getAdvCode() + endUrl;
                        LogUtil.d("Constants---1--", url);
                        File file = new File(url);
                        if (file.exists()) {
                            LogUtil.d("Constants---exists--", url);
                            mHandler.sendEmptyMessageDelayed(3, 1000);
                            //showImage();
                        } else {
                            LogUtil.d("Constants---unexists--", url);
                            FileUtils.delAllFile(getExternalFilesDir("adv").getAbsolutePath() + "/");
                            String urlImg = Constants.H5_BASE_URL + msg.getData().getAdvImage();
                            OkHttpUtil.saveFile(urlImg, msg.getData().getAdvCode() + endUrl, Constants.TAG_SAVE_IMAGE, MyApplication.getContext());
                        }
                    } else {
                        isGetImageFialed = true;
                        FileUtils.delAllFile(getExternalFilesDir("adv").getAbsolutePath() + "/");
                        //autoLogin(loginStatus);
                        //mHandler.sendEmptyMessageDelayed(2, 500);
                        skipType();
                    }
                    break;
            }

        }
        if (event.getTag().equals(Constants.TAG_SAVE_IMAGE)) {
            switch (event.getCls()) {
                case Constants.SAVE_IMAGE_FALSE:
                    LogUtil.d("Constants---2false--", url);
                    //mHandler.sendEmptyMessageDelayed(2, 500);
                    isGetImageFialed = true;
                    //autoLogin(loginStatus);
                    skipType();
                    break;
                case Constants.SAVE_IMAGE_TRUE://广告请求成功
                    LogUtil.d("Constants---2true--", url);
                    mHandler.sendEmptyMessageDelayed(3, 500);
                    //showImage();
                    break;
            }

        }
    }

    private void showImage() {
        mRlAdv.setVisibility(View.VISIBLE);
        if (getEnd(url).equalsIgnoreCase(".gif")) {
            mIvAdv.setImageDrawable(getDrawable(url));
        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(url);
            mIvAdv.setImageBitmap(bitmap);
        }
        countDown();
    }

    public void getDialog(String title, final boolean needClear) {
        if (mDialogFree == null) {
            mDialogFree = new CommonNoticeDialog(WelcomeActivity.this, title, getString(R.string.dialog_notice_button));
            mDialogFree.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    if (needClear) {
                        SpUtil.removeKey(Constants.SKEY);
                        SpUtil.removeAll();
                    }
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_NOT_FIRST_USE, true);
                    startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                    WelcomeActivity.this.finish();
                }
            });
        }
        DialogHelper.resize(WelcomeActivity.this, mDialogFree);
        mDialogFree.show();
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void countDown() {
        if (time == null) {
            time = new TimeCount(4000, 1000);
        }
        time.start();
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            //mHandler.sendEmptyMessageDelayed(2, 500);
            isTimerFinish = true;
            if (TextUtils.isEmpty(MyApplication.getCookies())) {
                if (!isClickAdv) {
                    skipLogin();
                }
            } else {
                if (loginStatus != AUTO_LOGIN_SUCCESS) {
                    mRlAdv.setVisibility(View.GONE);
                }
                autoLogin(loginStatus);
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            mTvAdvTime.setText(millisUntilFinished / 1000 + "");
        }

    }

    private String getEnd(String fileName) {
        if (!TextUtils.isEmpty(fileName) && fileName.contains(".")) {
            String fileEnd = fileName.substring(fileName.lastIndexOf("."));
            return fileEnd;
        } else {
            return "";
        }
    }

    private String getUrlEnd(String fileName) {
        if (!TextUtils.isEmpty(fileName) && fileName.contains(".")) {
            if (fileName.contains("/")) {
                String fileEnd = fileName.substring(fileName.lastIndexOf("/") + 1);
                return fileEnd;
            } else {
                return fileName;
            }
        } else {
            return "";
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_adv:
                if (!TextUtils.isEmpty(MyApplication.getCookies())) {
                    isClickAdv = true;
                    if (loginStatus == AUTO_LOGIN_SUCCESS && !TextUtils.isEmpty(advUrl)) {
                        Intent intent = new Intent(this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_INTENT, advUrl);
                        intent.putExtra(Constants.REGISTER_INTENT_URL, Constants.WELCOME_TO_REGISTER);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    if (!TextUtils.isEmpty(advUrl)) {
                        isClickAdv = true;
                        Intent intent = new Intent(this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_INTENT, advUrl);
                        intent.putExtra(Constants.REGISTER_INTENT_URL, Constants.WELCOME_TO_REGISTER_SECOND);
                        startActivity(intent);
                        finish();
                    }
                }
                break;
            case R.id.ll_adv_time:
                //mHandler.sendEmptyMessageDelayed(2, 500);
                time.cancel();
                //mRlAdv.setVisibility(View.GONE);
                skipType();
                break;
        }
    }

    private GifDrawable getDrawable(String url) {
        try {
            return new GifDrawable(url);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void autoLogin(int status) {
        switch (status) {
            case AUTO_LOGIN_FAILED:
                SpUtil.removeKey(Constants.SKEY);
                startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                WelcomeActivity.this.finish();
                break;
            case AUTO_LOGIN_SUCCESS:
                if (!isClickAdv) {
                    Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;
            case AUTO_LOGINING:
                //showNewLoading(true, getString(R.string.login_auto));
                break;
        }
    }

    private void skipLogin() {
        SpUtil.removeKey(Constants.SKEY);
        startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
        finish();
    }

    private void skipType() {
        if (TextUtils.isEmpty(MyApplication.getCookies())) {
            mHandler.sendEmptyMessageDelayed(4, 800);
        } else {
            autoLogin(loginStatus);
        }
    }

    private void showRootDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(WelcomeActivity.this, UIUtils.getString(R.string.tv_root_notice), UIUtils.getString(R.string.btn_exit), UIUtils.getString(R.string.btn_continue), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    /*SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_ALLOW_ROOT_CONTINUE, true);
                    initData();*/
                    setFullFunc();
                    SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_ALLOW_ROOT_CONTINUE, true);
                }
            });
            mSelectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    WelcomeActivity.this.finish();
                    MyApplication.getInstance().finishAllActivity();
                }
            });
        }
        mSelectDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSelectDialog != null) {
            mSelectDialog.cancel();
        }
    }
}
