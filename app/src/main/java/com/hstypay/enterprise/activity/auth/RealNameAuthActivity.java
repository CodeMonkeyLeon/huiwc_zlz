package com.hstypay.enterprise.activity.auth;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.bean.AliRealNameAuthBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.v5kf.client.lib.V5ClientAgent.ClientOpenMode;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.PopPhoneWindow;
import com.hstypay.enterprise.Widget.SelectRealAuthDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.RealNameAuthBean;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.v5kf.client.lib.V5ClientAgent;
import com.v5kf.client.lib.V5ClientConfig;
import com.v5kf.client.lib.entity.V5Message;
import com.v5kf.client.ui.ClientChatActivity;
import com.v5kf.client.ui.callback.ChatActivityFuncIconClickListener;
import com.v5kf.client.ui.callback.OnChatActivityListener;
import com.v5kf.client.ui.callback.OnURLClickListener;
import com.v5kf.client.ui.callback.UserWillSendMessageListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class RealNameAuthActivity extends BaseActivity implements OnChatActivityListener {
    private RecyclerView mRecyclerView;
    private List<RealNameAuthBean> data;
    private SafeDialog mLoadDialog;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private String mPicture;
    private TextView mTvTitle;
    private ImageView mIvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_auth);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mLoadDialog = getLoadDialog(RealNameAuthActivity.this, UIUtils.getString(R.string.public_loading), false);
        mRecyclerView = findViewById(R.id.recycler_view);
        mTvTitle = findViewById(R.id.tv_title);

        mTvTitle.setText(getString(R.string.title_realname_auth));
    }

    private void initEvent(){
        mIvBack.setOnClickListener(view->{
            finish();
        });
    }

    private void initData() {
        data = new ArrayList<>();
        //1-无微信支付类型； 2-未拿到交易识别码；3-已获取交易识别码，无V3证书，联系客服实名；4-已获取交易识别码，有V3证书，去实名；5-已实名
        //支付宝实名认证状态 1 无支付宝支付类型 2 无交易识别码 3 资料未提交 4 商家资料审核中 5 商家资料审核不通过 6 已获取交易识别码，去实名 7 已实名
        switch (MyApplication.getWxrexStatus()) {
            case 3:
                data.add(new RealNameAuthBean(getString(R.string.tv_wechat_realname_auth)
                        , getString(R.string.contact_service), 3, 1, R.mipmap.icon_wechat));
                break;
            case 4:
                data.add(new RealNameAuthBean(getString(R.string.tv_wechat_realname_auth)
                        , getString(R.string.tv_to_realname), 4, 1, R.mipmap.icon_wechat));
                break;
            case 5:
                data.add(new RealNameAuthBean(getString(R.string.tv_wechat_realname_auth)
                        , getString(R.string.tv_auth_ok), 5, 1, R.mipmap.icon_wechat));
                break;
        }
        switch (MyApplication.getAlirexStatus()) {
            case 3:
            case 4:
            case 5:
            case 6:
                data.add(new RealNameAuthBean(getString(R.string.tv_ali_realname_auth)
                        , getString(R.string.tv_not_auth), 4, 2, R.mipmap.icon_alipay));
                break;
            case 7:
                data.add(new RealNameAuthBean(getString(R.string.tv_ali_realname_auth)
                        , getString(R.string.tv_auth_ok), 5, 2, R.mipmap.icon_alipay));
                break;
        }

        mRecyclerView.setLayoutManager(new CustomLinearLayoutManager(this));
        mRecyclerView.setAdapter(new BaseQuickAdapter<RealNameAuthBean, BaseViewHolder>(R.layout.item_realname_auth, data) {
            @Override
            protected void convert(BaseViewHolder helper, RealNameAuthBean item) {
                helper.setText(R.id.tv_pay_type, item.getPayTypeName())
                        .setText(R.id.tv_auth_status, item.getAuthStatusStr())
                        .setImageResource(R.id.iv_pay_type, item.getImageRes())
                        .setVisible(R.id.iv_arrow, item.getAuthStatus() != 5);
                helper.itemView.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        if (item.getAuthStatus() != 5) {
                            if (item.getApiProvider() == 1) {//微信
                                switch (item.getAuthStatus()) {
                                    case 3:
                                        if (AppHelper.getAppType() == 1 || AppHelper.getApkType() != 0) {
                                            showPhoneDialog();
                                        } else {
                                            startChatActivity();
                                        }
                                        break;
                                    case 4:
                                        getWxRealNameAuth();
                                        break;
                                }
                            } else if (item.getApiProvider() == 2 && item.getAuthStatus() == 4) {//支付宝
                                getAliRealNameAuth();
                            }
                        }
                    }
                });
            }
        });
    }

    /**
     * 微信实名认证查询
     */
    private void getWxRealNameAuth() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).getWxRealNameAuth(MyApplication.getContext(), Constants.TAG_WX_REAL_NAME_AUTH, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    /**
     * 微信实名认证查询
     */
    private void getAliRealNameAuth() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).getAliRealNameAuth(MyApplication.getContext(), Constants.TAG_ALI_REAL_NAME_AUTH, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void showPhoneDialog() {
        PopPhoneWindow picPopupWindow = new PopPhoneWindow(RealNameAuthActivity.this, new PopPhoneWindow.HandleTv() {
            @Override
            public void call(String phone) {
                AppHelper.call(MyApplication.getContext(), phone);
            }
        });
        picPopupWindow.showAtLocation(mTvTitle, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void showAuthDialog(String picture) {
        SelectRealAuthDialog dialog = new SelectRealAuthDialog(RealNameAuthActivity.this, picture);
        dialog.setOnClickInstructionListener(() -> {
            String jumpUrl = "https://pay.weixin.qq.com/static/help_guide/register_guide.shtml";
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri content_url = Uri.parse(jumpUrl);
            intent.setData(content_url);
            startActivity(intent);
        });
        dialog.setOnClickOkListener(() -> {
            boolean results = PermissionUtils.checkPermissionArray(RealNameAuthActivity.this, permissionArray);
            if (results) {
                if (ImagePase.base64ToBitmap(mPicture) != null) {
                    ScreenUtils.saveImageToGallery(MyApplication.getContext(), ImagePase.base64ToBitmap(mPicture));
                    showCommonNoticeDialog(RealNameAuthActivity.this, getString(R.string.dialog_download_picture));
                } else {
                    showCommonNoticeDialog(RealNameAuthActivity.this, getString(R.string.toast_download_image_failed));
                }
            } else {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
            }
        });
        DialogHelper.resize(RealNameAuthActivity.this, dialog);
        dialog.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_WX_REAL_NAME_AUTH)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(RealNameAuthActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && !TextUtils.isEmpty(msg.getData().getQrcodeUrl())) {
                        mPicture = msg.getData().getQrcodeUrl();
                        showAuthDialog(msg.getData().getQrcodeUrl());
                    } else {
                        showCommonNoticeDialog(RealNameAuthActivity.this, getString(R.string.tv_auth_error_data));
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_ALI_REAL_NAME_AUTH)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            AliRealNameAuthBean msg = (AliRealNameAuthBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(RealNameAuthActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_ALIREG_STATUS, msg.getData().getStatus());
                        Intent intent;
                        if (msg.getData().getStatus() == 6 && !TextUtils.isEmpty(msg.getData().getQrcodeUrl())) {
                            intent = new Intent(RealNameAuthActivity.this, RealNameAuthCodeActivity.class);
                            intent.putExtra("INTENT_QRCODE_URL", msg.getData().getQrcodeUrl());
                        } else {
                            intent = new Intent(RealNameAuthActivity.this, RealNameAuthResultActivity.class);
                            intent.putExtra("INTENT_ALI_AUTH_BEAN", msg.getData());
                        }
                        startActivity(intent);
                    } else {
                        showCommonNoticeDialog(RealNameAuthActivity.this, getString(R.string.tv_auth_error_data));
                    }
                    break;
            }
        }
    }

    private void startChatActivity() {
        /* 开启会话界面 */
        // 可用Bundle传递以下参数
        Bundle bundle = new Bundle();
        bundle.putInt("numOfMessagesOnRefresh", 10);    // 下拉刷新数量，默认为10
        bundle.putInt("numOfMessagesOnOpen", 10);        // 开场显示历史消息数量，默认为10
        bundle.putBoolean("enableVoice", true);            // 是否允许发送语音
        bundle.putBoolean("showAvatar", true);            // 是否显示对话双方的头像
        /*
         * 设置开场白模式，默认为clientOpenModeDefault，可根据客服启动场景设置开场问题
         * clientOpenModeDefault	// 默认开场白方式（无历史消息显示则显示开场白，优先以设置的param字符串为开场白，param为null则使用后台配置的开场白）
         * clientOpenModeQuestion	// 自定义问题开场白，param字符串为问题内容（不为空），设置开场问题获得对应开场白（此模式不可与优先人工客服同用，否则将失效）
         * clientOpenModeNone		// 无开场白方式，仅显示历史消息
         * clientOpenModeAutoHuman  // 开场自动转人工客服
         */
        bundle.putInt("clientOpenMode", ClientOpenMode.clientOpenModeDefault.ordinal());
        //bundle.putString("clientOpenParam", "您好，请问有什么需要帮助的吗？");

        //Context context = getApplicationContext();
        //Intent chatIntent = new Intent(context, ClientChatActivity.class);
        //chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //chatIntent.putExtras(bundle);
        //context.startActivity(chatIntent);
        // 进入会话界面(可使用上面的方式或者调用下面方法)，携带bundle(不加bundle参数则全部使用默认配置)
        V5ClientAgent.getInstance().startV5ChatActivityWithBundle(MyApplication.getContext(), bundle);

        /* 添加聊天界面监听器(非必须，有相应需求则添加) */
        // 界面生命周期监听[非必须]
        V5ClientAgent.getInstance().setChatActivityListener(RealNameAuthActivity.this);
        // 消息发送监听[非必须]，可在此处向坐席透传来自APP客户的相关信息
        V5ClientAgent.getInstance().setUserWillSendMessageListener(new UserWillSendMessageListener() {

            @Override
            public V5Message onUserWillSendMessage(V5Message message) {
                // TODO 可在此处添加消息参数(JSONObject键值对均为字符串)，采集信息透传到坐席端（v1.2.0版本开始不建议使用此方式，除非有实时更新需求的自定义信息）
                return message; // 注：必须将消息对象以返回值返回
            }
        });

        /**
         * 点击链接监听
         * onURLClick返回值：是否消费了此点击事件，返回true则SDK内不再处理此事件，否则默认跳转到指定网页
         */
        V5ClientAgent.getInstance().setURLClickListener(new OnURLClickListener() {

            @Override
            public boolean onURLClick(Context context, V5ClientAgent.ClientLinkType type, String url) {
                // TODO Auto-generated method stub
                switch (type) {
                    case clientLinkTypeArticle: // 点击图文

                        break;
                    case clientLinkTypeURL: // 点击URL链接

                        break;

                    case clientLinkTypeEmail: // 点击电子邮件

                        break;

                    case clientLinkTypePhoneNumber: // 点击电话号码

                        break;
                }
                LogUtil.d("onURLClick:" + url);
                return false; // 是否消费了此点击事件
            }
        });

        /**
         * 点击对话输入框底部功能按钮
         */
        V5ClientAgent.getInstance().setChatActivityFuncIconClickListener(new ChatActivityFuncIconClickListener() {

            /**
             * Activity点击底部功能按钮事件，icon参数值及含义如下：
             * 		v5_icon_ques			//常见问题
             * 		v5_icon_relative_ques	//相关问题
             * 		v5_icon_photo			//图片
             * 		v5_icon_camera			//拍照
             * 		v5_icon_worker			//人工客服
             * 返回值代表是否消费了此事件
             * @param icon 点击的图标名称(对应SDK目录下res/values/v5_arrays中v5_chat_func_icon的值)
             * @return boolean 是否消费事件(返回true则不响应默认点击效果，由此回调处理)
             */
            @Override
            public boolean onChatActivityFuncIconClick(String icon) {
                // TODO Auto-generated method stub
                if (icon.equals("v5_icon_worker")) {
                    // 转到指定客服,参数：(组id, 客服id),参数为0则不指定客服组或者客服,获取组id请咨询客服
                    V5ClientAgent.getInstance().transferHumanService(0, 0);
                    // 返回true来拦截SDK内默认的实现
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /* 会话界面生命周期和连接状态的回调 */
    @Override
    public void onChatActivityCreate(ClientChatActivity activity) {
        LogUtil.d("<onChatActivityCreate>");
        activity.setChatTitle("客服");
    }

    @Override
    public void onChatActivityStart(ClientChatActivity activity) {
        LogUtil.d("<onChatActivityStart>");
    }

    @Override
    public void onChatActivityStop(ClientChatActivity activity) {
        LogUtil.d("<onChatActivityStop>");
    }

    @Override
    public void onChatActivityDestroy(ClientChatActivity activity) {
        LogUtil.d("<onChatActivityDestroy>");
    }

    @Override
    public void onChatActivityConnect(ClientChatActivity activity) {
        LogUtil.d("<onChatActivityConnect>");
    }

    @Override
    public void onChatActivityReceiveMessage(ClientChatActivity activity, V5Message message) {
        LogUtil.d("<onChatActivityReceiveMessage> " + message.getDefaultContent(MyApplication.getContext()));
    }

    @Override
    public void onChatActivityServingStatusChange(ClientChatActivity activity, V5ClientAgent.ClientServingStatus status) {
        switch (status) {
            case clientServingStatusRobot:
            case clientServingStatusQueue:
                activity.setChatTitle("机器人服务中");
                break;
            case clientServingStatusWorker:
                activity.setChatTitle(V5ClientConfig.getInstance(MyApplication.getContext()).getWorkerName() + "为您服务");
                break;
            case clientServingStatusInTrust:
                activity.setChatTitle("机器人托管中");
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    if (ImagePase.base64ToBitmap(mPicture) != null) {
                        ScreenUtils.saveImageToGallery(MyApplication.getContext(), ImagePase.base64ToBitmap(mPicture));
                        showCommonNoticeDialog(RealNameAuthActivity.this, getString(R.string.dialog_download_picture));
                    } else {
                        showCommonNoticeDialog(RealNameAuthActivity.this, getString(R.string.toast_download_image_failed));
                    }
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
}
