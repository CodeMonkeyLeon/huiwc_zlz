package com.hstypay.enterprise.activity.auth;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.PopPhoneWindow;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectRealAuthDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.AliRealNameAuthBean;
import com.hstypay.enterprise.bean.RealNameAuthBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.v5kf.client.lib.V5ClientAgent;
import com.v5kf.client.lib.V5ClientAgent.ClientOpenMode;
import com.v5kf.client.lib.V5ClientConfig;
import com.v5kf.client.lib.entity.V5Message;
import com.v5kf.client.ui.ClientChatActivity;
import com.v5kf.client.ui.callback.ChatActivityFuncIconClickListener;
import com.v5kf.client.ui.callback.OnChatActivityListener;
import com.v5kf.client.ui.callback.OnURLClickListener;
import com.v5kf.client.ui.callback.UserWillSendMessageListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class RealNameAuthResultActivity extends BaseActivity {
    private TextView mTvTitle;
    private TextView mTvAuthNotice;
    private TextView mTvAuthResult;
    private TextView mTvAuthReason;
    private ImageView mIvAuthStatus;
    private AliRealNameAuthBean.DataEntity mData;
    private ImageView mIvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_auth_result);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvAuthStatus = findViewById(R.id.iv_auth_status);
        mTvAuthResult = findViewById(R.id.tv_auth_result);
        mTvAuthNotice = findViewById(R.id.tv_auth_notice);
        mTvAuthReason = findViewById(R.id.tv_auth_reason);
        mTvTitle = findViewById(R.id.tv_title);

        mTvTitle.setText(getString(R.string.title_realname_auth));
    }

    private void initEvent(){
        mIvBack.setOnClickListener(view->{
            finish();
        });
    }

    private void initData() {
        mData = (AliRealNameAuthBean.DataEntity) getIntent().getSerializableExtra("INTENT_ALI_AUTH_BEAN");
        if (!TextUtils.isEmpty(mData.getFailDesc())){
            mTvAuthReason.setText("原因："+mData.getFailDesc());
        } else {
            mTvAuthReason.setVisibility(View.INVISIBLE);
        }
        if (mData != null) {
            switch (mData.getStatus()) {
                case 3:
                    mIvAuthStatus.setImageResource(R.mipmap.icon_ali_auth_checking);
                    mTvAuthResult.setText(R.string.tv_merchant_info_unsubmit);
                    mTvAuthNotice.setText(R.string.tv_merchant_info_unsubmit_notice);
                    break;
                case 4:
                    mIvAuthStatus.setImageResource(R.mipmap.icon_ali_auth_checking);
                    mTvAuthResult.setText(R.string.tv_merchant_info_checking);
                    mTvAuthNotice.setText(R.string.tv_merchant_info_checking_notice);
                    break;
                case 5:
                    mIvAuthStatus.setImageResource(R.mipmap.icon_ali_auth_failed);
                    mTvAuthResult.setText(R.string.tv_merchant_info_failed);
                    mTvAuthNotice.setText(R.string.tv_merchant_info_failed_notice);
                    break;
                default:
                    mIvAuthStatus.setImageResource(R.mipmap.icon_ali_auth_error);
                    mTvAuthResult.setText(R.string.tv_merchant_info_error);
                    mTvAuthNotice.setText(R.string.tv_merchant_info_error_notice);
                    break;
            }
        }
    }
}
