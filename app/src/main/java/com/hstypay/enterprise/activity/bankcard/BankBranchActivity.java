package com.hstypay.enterprise.activity.bankcard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.BranchRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BranchListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 16:57
 * @描述: ${TODO}
 */

public class BankBranchActivity extends BaseActivity implements View.OnClickListener, BranchRecyclerAdapter.OnRecyclerViewItemClickListener {

    private EditText mEtInput;
    private RecyclerView mRecyclerView;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private TextView mTvNull, mTvTitle, mButton;
    private List<BranchListBean.DataEntity> mList = new ArrayList<>();
    private BranchRecyclerAdapter mAdapter;
    private List<BranchListBean.DataEntity> mData;
    private ImageView mIvClean, mIvBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton.setVisibility(View.INVISIBLE);
        mTvTitle.setText(R.string.title_account_bank_branch);

        mEtInput = (EditText) findViewById(R.id.et_user_input);
        mEtInput.setHint(UIUtils.getString(R.string.hint_bank_branch_name));
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mTvNull = (TextView) findViewById(R.id.tv_null);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);

        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    mIvClean.setVisibility(View.INVISIBLE);
                    if (mData == null || mData.size() == 0)
                        return;
                    mList.clear();
                    mList.addAll(mData);
                    mAdapter.notifyDataSetChanged();
                } else {
                    mIvClean.setVisibility(View.VISIBLE);
                    if (mData == null || mData.size() == 0)
                        return;
                    mList.clear();
                    for (int i = 0; i < mData.size(); i++) {
                        if (mData.get(i).getBankBranchName().contains(s.toString().trim())) {
                            mList.add(mData.get(i));
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
                if (mList.size() == 0) {
                    mTvNull.setVisibility(View.VISIBLE);
                } else {
                    mTvNull.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initData() {
        long bankBranchId = getIntent().getLongExtra(Constants.INTENT_BANK_BRANCH_ID, -1L);
        long bankId = getIntent().getLongExtra(Constants.INTENT_BANK_ID, -1L);
        String provinceCode = getIntent().getStringExtra(Constants.INTENT_BANK_PROVINCE);
        String cityCode = getIntent().getStringExtra(Constants.INTENT_BANK_CITY);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter = new BranchRecyclerAdapter(BankBranchActivity.this, mList, bankBranchId);
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        getData(bankId,provinceCode,cityCode);
    }

    private void getData(long bankId,String provinceCode,String cityCode) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String,Object> map = new HashMap<>();
            map.put("bankId",bankId);
            map.put("provinceCode",provinceCode);
            map.put("cityCode",cityCode);
            ServerClient.newInstance(MyApplication.getContext()).bankBranchList(MyApplication.getContext(), Constants.TAG_BANK_BRANCH_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_BANK_BRANCH_LIST)) {
            dismissLoading();
            BranchListBean msg = (BranchListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(BankBranchActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {

                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    mData = msg.getData();
                    if (mData != null && mData.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mList.clear();
                        mList.addAll(mData);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mTvNull.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                mEtInput.setText("");
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        BranchListBean.DataEntity dataBean = mList.get(position);
        if (dataBean != null) {
            Intent intent = new Intent();
            Bundle mBundle = new Bundle();
            mBundle.putSerializable(Constants.RESULT_BANK_BRANCH_LIST, dataBean);   //传递一个user对象列表
            intent.putExtras(mBundle);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
