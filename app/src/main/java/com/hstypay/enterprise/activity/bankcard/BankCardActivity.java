package com.hstypay.enterprise.activity.bankcard;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.BankDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.ProtocolPopupWindow;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.MaterialStatusBean;
import com.hstypay.enterprise.bean.MyCardBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 我的银行卡
 */
public class BankCardActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvOldCardLogo, mIvNewCardLogo, mIvExam;
    private Button mBtnSave;
    private TextView mButton, mTvTitle, mTvOldCardName, mTvNewCardName, mTvOldCardId, mTvNewCardId, mTvNull;
    private LinearLayout mLlOldCard, mLlContent;
    private RelativeLayout mRlOldCard, mRlNewCard;
    private ProtocolPopupWindow mAccountProtocolPopupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_card);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);

        initView();
        initEvent();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_my_card);
        mButton.setVisibility(View.INVISIBLE);

        mLlContent = (LinearLayout) findViewById(R.id.ll_content);
        mLlOldCard = (LinearLayout) findViewById(R.id.ll_old_card);
        mRlOldCard = (RelativeLayout) findViewById(R.id.rl_old_bank_card);
        mRlNewCard = (RelativeLayout) findViewById(R.id.rl_new_bank_card);
        mIvOldCardLogo = (ImageView) findViewById(R.id.iv_old_card_logo);
        mIvNewCardLogo = (ImageView) findViewById(R.id.iv_new_card_logo);
        mIvExam = (ImageView) findViewById(R.id.iv_exam_icon);
        mTvOldCardName = (TextView) findViewById(R.id.tv_old_bank_name);
        mTvNewCardName = (TextView) findViewById(R.id.tv_new_bank_name);
        mTvOldCardId = (TextView) findViewById(R.id.tv_old_card_id);
        mTvNewCardId = (TextView) findViewById(R.id.tv_new_card_id);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mBtnSave = (Button) findViewById(R.id.btn_save);

        mLlContent.setVisibility(View.GONE);
        setButtonEnable(mBtnSave, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlNewCard.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(MyApplication.getContext()).myBankCard(MyApplication.getContext(), Constants.TAG_MY_CARD, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_new_bank_card:
                startActivity(new Intent(BankCardActivity.this, VerifyBankActivity.class));
                break;
            case R.id.btn_save:
                MtaUtils.mtaId(BankCardActivity.this, "O002");
                materialStatus();
                //showDialog();
                break;
        }
    }


    public void materialStatus() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(MyApplication.getContext()).materialStatus(MyApplication.getContext(), Constants.TAG_STATUS_BANK_CARD, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void showDialog() {
        BankDialog bankDialog = new BankDialog(BankCardActivity.this);
        bankDialog.setOnClickOkListener(new BankDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                startActivity(new Intent(BankCardActivity.this, VerifyBankActivity.class));
            }
        });
        bankDialog.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_MY_CARD)) {
            dismissLoading();
            MyCardBean info = (MyCardBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(BankCardActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(BankCardActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null && info.getData().size() > 0) {
                        mLlContent.setVisibility(View.VISIBLE);
                        List<MyCardBean.DataEntity> cardList = info.getData();
                        if (cardList.size() == 1) {
                            mRlNewCard.setEnabled(false);
                            mLlOldCard.setVisibility(View.GONE);
                            setBankCardView(cardList.get(0), mRlNewCard, mIvNewCardLogo, mTvNewCardName, mTvNewCardId);
                            mIvExam.setVisibility(View.GONE);
                        } else {
                            mLlOldCard.setVisibility(View.VISIBLE);
                            mIvExam.setVisibility(View.VISIBLE);
                            mBtnSave.setVisibility(View.GONE);
                            for (int i = 0; i < cardList.size(); i++) {
                                if (cardList.get(i).getExamineStatus() == 1) {//旧卡
                                    setBankCardView(cardList.get(i), mRlOldCard, mIvOldCardLogo, mTvOldCardName, mTvOldCardId);
                                } else {
                                    setBankCardView(cardList.get(i), mRlNewCard, mIvNewCardLogo, mTvNewCardName, mTvNewCardId);
                                    if (cardList.get(i).getExamineStatus() == 0) {
                                        mRlNewCard.setEnabled(false);
                                        mIvExam.setImageResource(R.mipmap.bank_card_examing);
                                    } else if (cardList.get(i).getExamineStatus() == 2) {
                                        mRlNewCard.setEnabled(true);
                                        mIvExam.setImageResource(R.mipmap.bank_card_exam_failed);
                                    }
                                }
                            }
                        }
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_STATUS_BANK_CARD)) {
            dismissLoading();
            MaterialStatusBean info = (MaterialStatusBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(BankCardActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(BankCardActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        MaterialStatusBean.DataEntity data = info.getData();
                        if (data.getExamineStatus() == 1) {
//                            showDialog();
                            showAccountProtocol(Constants.URL_CHANGE_BANK_PROTOCOL);
                        } else {
                            if ("1".equals(data.getStandby9())) {
//                                showDialog();
                                showAccountProtocol(Constants.URL_CHANGE_BANK_PROTOCOL);
                            } else {
                                showCommonNoticeDialog(BankCardActivity.this, getString(R.string.toast_not_material_change));
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void showAccountProtocol(String url) {
        if (mAccountProtocolPopupWindow == null) {
            mAccountProtocolPopupWindow = new ProtocolPopupWindow(BankCardActivity.this, url, true, new ProtocolPopupWindow.OnClickEnsureListener() {
                @Override
                public void agreeProtocol() {
                    mAccountProtocolPopupWindow.dismiss();
                    showDialog();
                }
            }, new ProtocolPopupWindow.OnClickCancelListener() {
                @Override
                public void notAgreeProtocol() {
                    MyToast.showToastShort(getString(R.string.toast_change_bank_protocol));
                }
            });
        }
        mAccountProtocolPopupWindow.showAtLocation(mBtnSave, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void setBankCardView(MyCardBean.DataEntity dataEntity, RelativeLayout rlBankCard, ImageView ivCardLogo, TextView tvCardName, TextView tvCardId) {
        tvCardName.setText(dataEntity.getBankIdCnt());
        tvCardId.setText(StringUtils.hideBankCardId(dataEntity.getAccountCode()));
        switch (dataEntity.getBankId() + "") {
            case "1"://中国工商银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_gongshang_logo);
                break;
            case "2"://中国银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_zhongguo_logo);
                break;
            case "3"://中国建设银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_jianshe_logo);
                break;
            case "4"://中国农业银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_green);
                ivCardLogo.setImageResource(R.mipmap.bank_card_nongye_logo);
                break;
            case "5"://招商银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_zhaoshang_logo);
                break;
            case "6"://中国邮政储蓄银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_green);
                ivCardLogo.setImageResource(R.mipmap.bank_card_youzheng_logo);
                break;
            case "7"://中国光大银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_orange);
                ivCardLogo.setImageResource(R.mipmap.bank_card_guangda_logo);
                break;
            case "8"://浦发银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_pufa_logo);
                break;
            case "10"://兴业银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_xingye_logo);
                break;
            case "11"://平安银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_orange);
                ivCardLogo.setImageResource(R.mipmap.bank_card_pingan_logo);
                break;
            case "13"://中信银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_zhongxin_logo);
                break;
            case "14"://交通银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_jiaotong_logo);
                break;
            case "15"://中国民生银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_green);
                ivCardLogo.setImageResource(R.mipmap.bank_card_minsheng_logo);
                break;
            case "46"://华夏银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_huaxia_logo);
                break;
            case "49"://北京银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_beijing_logo);
                break;
            case "141"://上海银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_shanghai_logo);
                break;
            case "163"://长沙银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_changsha_logo);
                break;
            case "506"://广州银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_guangzhou_logo);
                break;
            case "10000016"://江西银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_green);
                ivCardLogo.setImageResource(R.mipmap.bank_card_jiangxi_logo);
                break;
            case "10000134"://广发银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_red);
                ivCardLogo.setImageResource(R.mipmap.bank_card_guangfa_logo);
                break;
            case "10000192"://农村合作银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_green);
                ivCardLogo.setImageResource(R.mipmap.bank_card_ncxys_logo);
                break;
            case "10000193"://农村信用联社
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_green);
                ivCardLogo.setImageResource(R.mipmap.bank_card_ncxys_logo);
                break;
            case "10000213"://厦门银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_xiamen_logo);
                break;
            case "10000292"://深圳农村商业银行
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue);
                ivCardLogo.setImageResource(R.mipmap.bank_card_sznongcun_logo);
                break;
            default:
                rlBankCard.setBackgroundResource(R.mipmap.bank_card_blue_default);
                ivCardLogo.setImageResource(R.mipmap.bank_card_default_logo);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mAccountProtocolPopupWindow != null && mAccountProtocolPopupWindow.isShowing()) {
            mAccountProtocolPopupWindow.dismiss();
            return;
        }
        super.onBackPressed();
    }
}
