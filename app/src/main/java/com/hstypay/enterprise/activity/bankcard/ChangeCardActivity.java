package com.hstypay.enterprise.activity.bankcard;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.activity.setting.ChangeTelActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BankListBean;
import com.hstypay.enterprise.bean.BranchListBean;
import com.hstypay.enterprise.bean.CardDetailBean;
import com.hstypay.enterprise.bean.ChangeBankBean;
import com.hstypay.enterprise.bean.ImageBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.ImageFactory;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.qiezzi.choseviewlibrary.ChoseCityPicker;
import com.qiezzi.choseviewlibrary.bean.AddressBean;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 修改银行卡
 */

public class ChangeCardActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, iv_pic_six, mIvBank, mIvBankAddress, mIvBankBranch;
    private TextView mTvTitle, mTvAcountName, mTvCardId, mTvCardBank, mTvCardAddress, mTvCardBankBranch, mTvNull, mTvReason, mButton;
    private Button mBtnSave;
    private SafeDialog mLoadDialog;
    private LinearLayout mLlExamineStatus, ly_sixth;
    private RelativeLayout ly_phone_six, mRlCardBank, mRlCardAddress, mRlCardBankBranch;
    private EditTextDelete mEtAccountNo;
    private ScrollView mSvContent;

    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    private String imageUrl;
    private File tempFile;
    private Uri originalUri;
    private String picSixPath;
    private CardDetailBean.DataEntity mData;
    private long mBankId = -1L;
    private long mBankBranchId = -1L;
    private String mProvinceCode;
    private String mCityCode;
    private ChoseCityPicker mCityPicker;
    private String mBkCardPhoto;
    private String mContactLine;
    private String mBranchName;
    private String mBankName;
    private SelectDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_card);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mBtnSave = (Button) findViewById(R.id.btn_save);
        mTvTitle.setText(R.string.title_change_card);
        mButton.setVisibility(View.INVISIBLE);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        mLlExamineStatus = (LinearLayout) findViewById(R.id.ll_examine_status);
        mTvReason = (TextView) findViewById(R.id.tv_merchant_reason);
        ly_phone_six = (RelativeLayout) findViewById(R.id.ly_phone_six);
        ly_sixth = (LinearLayout) findViewById(R.id.ly_sixth);
        iv_pic_six = (ImageView) findViewById(R.id.iv_pic_six);

        mTvAcountName = (TextView) findViewById(R.id.tv_account_name);
        mTvCardId = (TextView) findViewById(R.id.tv_card_id);//身份证号
        mEtAccountNo = (EditTextDelete) findViewById(R.id.et_account_no);//银行卡号
        mRlCardBank = (RelativeLayout) findViewById(R.id.rl_card_bank);
        mTvCardBank = (TextView) findViewById(R.id.tv_card_bank);
        mRlCardAddress = (RelativeLayout) findViewById(R.id.rl_card_address);
        mTvCardAddress = (TextView) findViewById(R.id.tv_card_address);
        mRlCardBankBranch = (RelativeLayout) findViewById(R.id.rl_card_bank_branch);
        mTvCardBankBranch = (TextView) findViewById(R.id.tv_card_bank_branch);
        mIvBank = (ImageView) findViewById(R.id.iv_bank_arrow);
        mIvBankBranch = (ImageView) findViewById(R.id.iv_bank_branch_arrow);
        mIvBankAddress = (ImageView) findViewById(R.id.iv_bank_address_arrow);
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
        ly_phone_six.setOnClickListener(this);

        mRlCardBank.setOnClickListener(this);
        mRlCardAddress.setOnClickListener(this);
        mRlCardBankBranch.setOnClickListener(this);

        mEtAccountNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setButtonView();
            }
        });
    }

    private void initData() {
        mData = (CardDetailBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_BANK_DETAIL);
        if (mData != null) {
            mSvContent.setVisibility(View.VISIBLE);
            if (mData.getCardType() == 1) {
                if (!StringUtils.isEmptyOrNull(mData.getBkCardPhoto())) {
                    Picasso.get()
                            .load(Constants.H5_BASE_URL + mData.getBkCardPhoto())
                            .placeholder(R.mipmap.icon_general_noloading)
                            .error(R.mipmap.icon_general_noloading)
                            .into(iv_pic_six);
                }
                mTvAcountName.setText(mData.getAccountName());
                mTvCardId.setText(mData.getIdCard());
                mEtAccountNo.setText(mData.getAccountCode());
                mEtAccountNo.setSelection(mEtAccountNo.getText().length());
                mTvCardBank.setText(mData.getBankIdCnt());
                mTvCardAddress.setText(mData.getProvinceCnt() + " " + mData.getCityCnt());
                mTvCardBankBranch.setText(mData.getBankBranchIdCnt());
                mBankId = mData.getBankId();
                mBankName = mData.getBankIdCnt();
                mBankBranchId = mData.getBankBranchId();
                mBranchName = mData.getBankBranchIdCnt();
                mProvinceCode = mData.getProvince();
                mCityCode = mData.getCity();
                mContactLine = mData.getContactLine();
                mBkCardPhoto = mData.getBkCardPhoto();
                if (mData.getMerchantExamineStatus() == 2) {
                    mBtnSave.setText(UIUtils.getString(R.string.tv_enter));
                } else {
                    mBtnSave.setVisibility(View.GONE);
                    mEtAccountNo.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                    mTvCardBank.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                    mTvCardBankBranch.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                    mTvCardAddress.setTextColor(UIUtils.getColor(R.color.tv_not_change));
                    mIvBank.setVisibility(View.INVISIBLE);
                    mIvBankBranch.setVisibility(View.INVISIBLE);
                    mIvBankAddress.setVisibility(View.INVISIBLE);
                    mEtAccountNo.setEnable(false);
                    mRlCardBank.setEnabled(false);
                    mRlCardBankBranch.setEnabled(false);
                    mRlCardAddress.setEnabled(false);
                    ly_phone_six.setEnabled(false);
                }
            } else {
                if (mData.getExamineStatus() == 2) {
                    mLlExamineStatus.setVisibility(View.VISIBLE);
                    mTvReason.setText(mData.getExamineRemark());
                    if (!StringUtils.isEmptyOrNull(mData.getBkCardPhoto())) {
                        Picasso.get()
                                .load(Constants.H5_BASE_URL + mData.getBkCardPhoto())
                                .placeholder(R.mipmap.icon_general_noloading)
                                .error(R.mipmap.icon_general_noloading)
                                .into(iv_pic_six);
                    }
                    mTvAcountName.setText(mData.getAccountName());
                    mTvCardId.setText(mData.getIdCard());
                    mEtAccountNo.setText(mData.getAccountCode());
                    mEtAccountNo.setSelection(mEtAccountNo.getText().length());
                    mTvCardBank.setText(mData.getBankIdCnt());
                    mTvCardAddress.setText(mData.getProvinceCnt() + " " + mData.getCityCnt());
                    mTvCardBankBranch.setText(mData.getBankBranchIdCnt());
                    mBankId = mData.getBankId();
                    mBankName = mData.getBankIdCnt();
                    mBankBranchId = mData.getBankBranchId();
                    mBranchName = mData.getBankBranchIdCnt();
                    mProvinceCode = mData.getProvince();
                    mCityCode = mData.getCity();
                    mContactLine = mData.getContactLine();
                    mBkCardPhoto = mData.getBkCardPhoto();
                    mBtnSave.setText(UIUtils.getString(R.string.submit));
                } else {
                    mTvAcountName.setText(mData.getAccountName());
                    mTvCardId.setText(mData.getIdCard());
                }
            }
        } else {
            mTvNull.setVisibility(View.VISIBLE);
        }
        setButtonView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //正面照
            case R.id.ly_phone_six:
                choice();
                break;
            case R.id.rl_card_bank:
                Intent intentBank = new Intent(ChangeCardActivity.this, BankActivity.class);
                intentBank.putExtra(Constants.INTENT_BANK_LIST, mBankId);
                startActivityForResult(intentBank, Constants.REQUEST_BANK_LIST);
                break;
            case R.id.rl_card_address:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    ServerClient.newInstance(MyApplication.getContext()).getAddress(MyApplication.getContext(), Constants.TAG_GET_ADDRESS, null);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.rl_card_bank_branch:
                if (mBankId == -1L) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_card_bank_empty));
                    return;
                }
                if (TextUtils.isEmpty(mProvinceCode) || TextUtils.isEmpty(mCityCode)) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_card_address_empty));
                    return;
                }
                Intent intentBankBranch = new Intent(ChangeCardActivity.this, BankBranchActivity.class);
                intentBankBranch.putExtra(Constants.INTENT_BANK_BRANCH_ID, mBankBranchId);
                intentBankBranch.putExtra(Constants.INTENT_BANK_ID, mBankId);
                intentBankBranch.putExtra(Constants.INTENT_BANK_PROVINCE, mProvinceCode);
                intentBankBranch.putExtra(Constants.INTENT_BANK_CITY, mCityCode);
                startActivityForResult(intentBankBranch, Constants.REQUEST_BANK_BRANCH_LIST);
                break;
            case R.id.btn_save:
                if (mData.getCardType() == 1) {
                    mData.setAccountType(2);
                    mData.setBkCardPhoto(mBkCardPhoto);
                    mData.setAccountCode(mEtAccountNo.getText().toString().trim());
                    mData.setBankId(mBankId);
                    mData.setBankIdCnt(mBankName);
                    mData.setProvince(mProvinceCode);
                    mData.setCity(mCityCode);
                    mData.setBankBranchId(mBankBranchId);
                    mData.setBankBranchIdCnt(mBranchName);
                    mData.setContactLine(mContactLine);
                    Intent intent = new Intent();
                    Bundle mBundle = new Bundle();
                    mBundle.putSerializable(Constants.RESULT_MERCHANT_CARD_INTENT, mData);
                    intent.putExtras(mBundle);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    submit("", Constants.TAG_EDIT_CARD);
                }
                break;
        }
    }

    private void submit(String verifyCode, String tag) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("accountType", 2);
            map.put("bkCardPhoto", mBkCardPhoto);
            map.put("accountCode", mEtAccountNo.getText().toString().trim());
            map.put("bankId", mBankId);
            map.put("province", mProvinceCode);
            map.put("city", mCityCode);
            map.put("bankBranchId", mBankBranchId);
            map.put("bankName", mBranchName);
            map.put("contactLine", mContactLine);
            if (!TextUtils.isEmpty(verifyCode)) {
                map.put("verifyCode", verifyCode);
            }
            ServerClient.newInstance(MyApplication.getContext()).editBankCard(MyApplication.getContext(), tag, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(ChangeCardActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    boolean results = PermissionUtils.checkPermissionArray(ChangeCardActivity.this, permissionArray);
                    if (results) {
                        startCamera();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1);
                    }
                } else {
                    showCommonNoticeDialog(ChangeCardActivity.this, getString(R.string.tx_sd_pic));
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    boolean results = PermissionUtils.checkPermissionArray(ChangeCardActivity.this, permissionArray);
                    if (results) {
                        takeImg();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2);
                    }
                } else {
                    showCommonNoticeDialog(ChangeCardActivity.this, getString(R.string.tx_sd_pic));
                }
            }
        });
        picPopupWindow.showAtLocation(iv_pic_six, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";
            tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()){
                tempFile.getParentFile().mkdirs();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(ChangeCardActivity.this,
                        Constants.FILE_PROVIDER, tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    private void showNotice(final int requestCode) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(ChangeCardActivity.this
                , getString(R.string.permission_content_bank_card), getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(ChangeCardActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    private void showDialog() {
        if (mDialog == null) {
            mDialog = new SelectDialog(this, getString(R.string.permission_set_content_camera)
                    , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
            mDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    Intent intent = getAppDetailSettingIntent(ChangeCardActivity.this);
                    startActivity(intent);
                }
            });
            DialogHelper.resize(this, mDialog);
        }
        mDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = AppHelper.getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            picSixPath = path;
                            setBitmap(ly_sixth, ly_phone_six, bitmap);
                            uploadImage(picSixPath, "bankCardImg");

                        }
                    }
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = AppHelper.getPicPath(originalUri);
                    LogUtil.d("path=" + pathPhoto);
                    Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        picSixPath = pathPhoto;
                        setBitmap(ly_sixth, ly_phone_six, bitmap_pci);
                        uploadImage(picSixPath, "bankCardImg");
                    }
                    break;
                default:
                    break;
            }
        }
        if (requestCode == Constants.REQUEST_BANK_LIST && resultCode == Activity.RESULT_OK) {
            BankListBean.DataEntity dataBean = (BankListBean.DataEntity) data.getExtras().getSerializable(Constants.RESULT_BANK_LIST);
            mBankId = dataBean.getBankId();
            mBankName = dataBean.getBankName();
            mTvCardBank.setText(dataBean.getBankName());
            mTvCardBankBranch.setText("");
            setButtonView();
        }
        if (requestCode == Constants.REQUEST_BANK_BRANCH_LIST && resultCode == Activity.RESULT_OK) {
            BranchListBean.DataEntity dataBean = (BranchListBean.DataEntity) data.getExtras().getSerializable(Constants.RESULT_BANK_BRANCH_LIST);
            mBankBranchId = dataBean.getBankBranchId();
            mContactLine = dataBean.getContactLine();
            mBranchName = dataBean.getBankBranchName();
            mTvCardBankBranch.setText(dataBean.getBankBranchName());
            setButtonView();
        }
        if (requestCode == Constants.REQUEST_CHANGE_BANK && resultCode == Activity.RESULT_OK) {
            String verifyCode = data.getStringExtra(Constants.RESULT_CHANGE_BANK);
            submit(verifyCode, Constants.TAG_EDIT_CARD_VERIFY);
        }
    }

    private void uploadImage(String path, final String type) {
        if (!NetworkUtils.isNetworkAvailable(ChangeCardActivity.this)) {
            showCommonNoticeDialog(ChangeCardActivity.this, getString(R.string.network_exception));
            return;
        }
        showNewLoading(true, getString(R.string.public_uploading));

        String url = Constants.BASE_URL + "merchant/upload/file";
        PostFormBuilder post = OkHttpUtils.post();
        String pathCompress = null;
        if (!TextUtils.isEmpty(path)) {
//            pathCompress = path.substring(0, path.lastIndexOf(".")) + "(1).jpg";
            pathCompress = AppHelper.getImageCacheDir(path);
            ImageFactory.compressPicture(path, pathCompress);
            post = post.addFile(type, type, new File(pathCompress));
        }
        final String finalPathCompress = pathCompress;
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                dismissLoading();
                MyToast.showToastShort("上传失败");
                FileUtils.deleteFile(finalPathCompress);
            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(finalPathCompress);
                dismissLoading();
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null) {
                    if (imageBean.isStatus()) {
                        ImageBean.DataEntity imageInfo = imageBean.getData();
                        mBkCardPhoto = imageInfo.getBankCardImg();
                        setButtonView();
                        if (imageInfo.getBankCard() != null) {
                            mEtAccountNo.setText(imageInfo.getBankCard().getNumber());
                            mTvCardBank.setText(imageInfo.getBankCard().getBankname());
                            mBankId = StringUtils.paseStrToLong(imageInfo.getBankCard().getBankno());
                            setButtonView();
                        }
                    } else {
                        if (imageBean.getError() != null && imageBean.getError().getMessage() != null) {
                            MyToast.showToastShort(imageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void setBitmap(LinearLayout ll, RelativeLayout rl, Bitmap bitmap) {
        ll.setVisibility(View.GONE);
        rl.setBackground(new BitmapDrawable(bitmap));
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_EDIT_CARD)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ChangeBankBean msg = (ChangeBankBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ChangeCardActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    ChangeBankBean.DataEntity data = msg.getData();
                    if (data != null && data.getVerifyCode() == 1) {
                        Intent intent = new Intent(ChangeCardActivity.this, ChangeTelActivity.class);
                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_CHANGE_BANK);
                        intent.putExtra(Constants.INTENT_CHANGE_BANK, data.getTel());
                        startActivityForResult(intent, Constants.REQUEST_CHANGE_BANK);
                    } else {
                        getDialog();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_EDIT_CARD_VERIFY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ChangeBankBean msg = (ChangeBankBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ChangeCardActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    getDialog();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_ADDRESS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            AddressBean msg = (AddressBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_ADDRESS_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ChangeCardActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_ADDRESS_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        area(msg);
                    }
                    break;
            }
        }
    }

    private void area(AddressBean addressBean) {
        mCityPicker = new ChoseCityPicker(ChangeCardActivity.this, addressBean);
        mCityPicker.setOnGetAddress(new ChoseCityPicker.OnGetAddress() {
            @Override
            public void getAddress(String province, String city, String area) {
                //获取省市区地址
                mTvCardAddress.setText(province + " " + city);
                mData.setProvinceCnt(province);
                mData.setCityCnt(city);
                setButtonView();
            }
        });
        mCityPicker.setOnGetAddressCode(new ChoseCityPicker.OnGetAddressCode() {
            @Override
            public void getAddressCode(String province, String city, String area) {
                //获取省市区code
                mProvinceCode = province;
                mCityCode = city;
                mData.setProvince(province);
                mData.setCity(city);
                mTvCardBankBranch.setText("");
            }
        });
        mCityPicker.show();
    }

    public void getDialog() {
        NoticeDialog dialogInfo = new NoticeDialog(ChangeCardActivity.this, null, null, R.layout.notice_dialog_verify);
        dialogInfo.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                ChangeCardActivity.this.finish();
            }
        });
        DialogHelper.resize(ChangeCardActivity.this,dialogInfo);
        dialogInfo.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    public boolean getButtonState() {
        return mEtAccountNo.getText().toString().trim().length() == 0
                || mTvCardBank.getText().toString().trim().length() == 0 || mTvCardAddress.getText().toString().trim().length() == 0
                || mTvCardBankBranch.getText().toString().trim().length() == 0 || TextUtils.isEmpty(mBkCardPhoto);
    }

    private void setButtonView() {
        setButtonEnable(mBtnSave,getButtonState());
        if (getButtonState()) {
            setButtonEnable(mBtnSave,false);
        } else {
            setButtonEnable(mBtnSave,true);
        }
    }
}
