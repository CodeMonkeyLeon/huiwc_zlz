package com.hstypay.enterprise.activity.bankcard;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CardDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class VerifyBankActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvEye;
    private TextView mTvTitle, mTvUsername, mBtnTitle;
    private EditText mEtPwd;
    private Button mBtnSubmit;
    private SafeDialog mLoadDialog;
    private NoticeDialog mNoticeDialog;
    private boolean isOpenPwd;
    private SafeKeyboard safeKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_authentication);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvUsername = (TextView) findViewById(R.id.tv_login_username);
        mEtPwd = (EditText) findViewById(R.id.et_login_pwd);
        mIvEye = (ImageView) findViewById(R.id.iv_eye_pwd);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mTvTitle.setText(R.string.title_authentication);
        mBtnTitle.setVisibility(View.INVISIBLE);
        View rootView = findViewById(R.id.main_root);
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtPwd.getId(), mEtPwd);

        mTvUsername.setText(MyApplication.getLoginName());
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvEye.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

        mEtPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    setButtonEnable(mBtnSubmit, true);
                } else {
                    setButtonEnable(mBtnSubmit, false);
                }
            }
        });
        setButtonEnable(mBtnSubmit, false);
    }

    private void initData() {
        mTvUsername.setText(MyApplication.getLoginName());
//        mTvUsername.setText(SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_NAME));
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return !isOpen;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    map.put("password",  INITDES3Util.getSecretContent(this, mEtPwd.getText().toString().trim()));
                    map.put("desFlag", 1);
                    ServerClient.newInstance(MyApplication.getContext()).authentication(MyApplication.getContext(), Constants.TAG_AUTHENTICATION, map);
                } else {
                    MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
                }
                break;
            case R.id.iv_eye_pwd:
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd, mIvEye, mEtPwd);
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_AUTHENTICATION)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.AUTHENTICATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VerifyBankActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    //MyToast.showToastShort(msg.getError().getMessage());
                                    showDialog(msg.getError().getMessage(), R.layout.notice_dialog_lock);
                                }
                            }
                        }
                    }
                    break;
                case Constants.AUTHENTICATION_TRUE:
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        ServerClient.newInstance(MyApplication.getContext()).bankCardDetail(MyApplication.getContext(), Constants.TAG_CARD_DETAIL, null);
                    } else {
                        MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
                    }

                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CARD_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CardDetailBean msg = (CardDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VerifyBankActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.INTENT_BANK_DETAIL, msg.getData());
                        if (msg.getData().getMerchantClass() == 1) {
                            intent.setClass(VerifyBankActivity.this, ChangeCompanyCardActivity.class);
                        } else if (msg.getData().getMerchantClass() == 2) {
                            intent.setClass(VerifyBankActivity.this, ChangeCardActivity.class);
                        }
                        startActivity(intent);
                        finish();
                    } else {
                        MyToast.showToastShort(getString(R.string.data_error));
                    }
                    break;
            }
        }
    }

    private void showDialog(String content, int res) {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(VerifyBankActivity.this, content, null, res);
            mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    mNoticeDialog.dismiss();
                }
            });
        }
        mNoticeDialog.show();
    }

    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboard.isShow()) {
                safeKeyboard.hideKeyboard();
                return false;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (safeKeyboard != null) {
            safeKeyboard.release();
            safeKeyboard = null;
        }
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }
    }
}
