package com.hstypay.enterprise.activity.bill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.RefundDetailActivity;
import com.hstypay.enterprise.adapter.BillFindRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PayBeanUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BillFindActivity extends BaseActivity implements View.OnClickListener, BillFindRecyclerAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack;
    private RecyclerView mRvBill;
    private TextView mTvSearch;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private EditTextDelete mEtInput;

    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<BillsBean.DataEntity> mList = new ArrayList<>();
    private BillFindRecyclerAdapter mAdapter;
    private ImageView mIvSearch;
    private TextView mTvSearchNull;
    private String storeMerchantId;
    private RadioGroup mRgBillSearch;
    private String otherType = Constants.HPAY_RECEIVE_ORDER;//HPAY_REFUND_ORDER：退款查询、HPAY_RECEIVE_ORDER：收款查询，默认是收款查询

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_find);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvSearch = findViewById(R.id.button_title);
        mIvSearch = (ImageView) findViewById(R.id.iv_search);
        mEtInput = (EditTextDelete) findViewById(R.id.et_user_input);
        mTvSearchNull = (TextView) findViewById(R.id.tv_null);
        mRgBillSearch = findViewById(R.id.rg_bill_search);

        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mEtInput.setFocusable(true);
        mEtInput.setFocusableInTouchMode(true);
        mEtInput.requestFocus();
        InputMethodManager inputManager =(InputMethodManager)mEtInput.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(mEtInput, 0);
        initRecyclerView();
        initSwipeRefreshLayout();
    }
    private void initRecyclerView() {
        mRvBill = (RecyclerView) findViewById(R.id.bill_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvBill.setLayoutManager(mLinearLayoutManager);
        mRvBill.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setRefreshEnable(false);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
        mIvSearch.setOnClickListener(this);
        mRgBillSearch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_receive_bill_search:
                        //收款单
                        otherType = Constants.HPAY_RECEIVE_ORDER;
                        mAdapter.setOtherType(otherType);
                        break;
                    case R.id.rb_refund_bill_search:
                        //退款单
                        otherType = Constants.HPAY_REFUND_ORDER;
                        mAdapter.setOtherType(otherType);
                        break;
                }
            }
        });
    }
    public void initData() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() /*|| MyApplication.getIsManager()*/) {
            storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
        } else if (MyApplication.getIsCasher()) {
            storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
        }
        mList = new ArrayList<>();
        mAdapter = new BillFindRecyclerAdapter(BillFindActivity.this, mList);
        mRvBill.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mList.clear();
                    currentPage = 2;
                    loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    mList.clear();
                    currentPage = 2;
                    mTvSearchNull.setVisibility(View.VISIBLE);
                    mRgBillSearch.setVisibility(View.VISIBLE);
                    mSwipeRefreshLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void loadData(String pageSize, String currentPage, String orderNo) {
        if (TextUtils.isEmpty(orderNo) || orderNo.length()<4){
            MyToast.showToast(UIUtils.getString(R.string.et_bill_four_number), Toast.LENGTH_SHORT);
            return;
        }else {
            //门店网络请求
            if (Utils.Integer.tryParse(currentPage,0)==1 && !isRefreshed) {
                showNewLoading(true,getString(R.string.public_loading));
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            map.put("orderNo", orderNo);
            map.put("otherType",otherType);
            ServerClient.newInstance(MyApplication.getContext()).getBillsByNumber(MyApplication.getContext(), Constants.TAG_GET_BILLS_BY_NUMBER, map);
        }
    }
    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBillsByNumberEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_BILLS_BY_NUMBER)) {
            BillsBean msg = (BillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_BILLS_BY_NUMBER_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if(msg.getError()!=null){
                        if(msg.getError().getCode()!=null){
                            if(msg.getError().getCode().equals(MyApplication.getFreeLogin())){
                                if(msg.getError().getMessage()!=null){
                                    getLoginDialog(BillFindActivity.this,msg.getError().getMessage());
                                }
                            }else{
                                if(msg.getError().getMessage()!=null) {
                                     MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_BILLS_BY_NUMBER_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mOnItemSearchListener.onItemSearch(mEtInput.getText().toString().trim());
                        mTvSearchNull.setVisibility(View.GONE);
                        mRgBillSearch.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (mList==null|| mList.size()==0) {
                            mTvSearchNull.setVisibility(View.VISIBLE);
                            mRgBillSearch.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                        }else{
                            mTvSearchNull.setVisibility(View.GONE);
                            mRgBillSearch.setVisibility(View.GONE);
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_title:
                finish();
                break;
            case R.id.iv_search:
                mList.clear();
                currentPage = 2;
                loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
                break;

            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        BillsBean.DataEntity dataEntity = mList.get(position);
        if(dataEntity!=null){
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                if (dataEntity.getApiProvider() == 7 || dataEntity.getApiProvider() == 9) {
                    if (!StringUtils.isEmptyOrNull(dataEntity.getOrderNo())) {
                        map.put("orderNo", dataEntity.getOrderNo());
                    }
                    map.put("storeMerchantId", storeMerchantId);
                    map.put("type", dataEntity.getApiProvider());
                } else {
                    if (!StringUtils.isEmptyOrNull(dataEntity.getOrderNo())) {
                        map.put("orderNo", dataEntity.getOrderNo());
                    }
                    if (!StringUtils.isEmptyOrNull(dataEntity.getRefundNo())) {
                        map.put("refundNo", dataEntity.getRefundNo());
                    }
                }
                ServerClient.newInstance(BillFindActivity.this).getOrderDetail(BillFindActivity.this,Constants.TAG_BILL_FIND_SEARCH_DETAIL,map);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }

    }

    //订单详情返回数据
    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderDetail(NoticeEvent event) {
        if(event.getTag().equals(Constants.TAG_BILL_FIND_SEARCH_DETAIL)) {
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    //MyToast.showToastShort(getString(R.string.net_error) + Constants.PAY_TAG);
                    MyToast.showToastShort(getString(R.string.net_error));
                    LogUtil.i("zhouwei",getString(R.string.net_error) + Constants.TAG_BILL_FIND_SEARCH_DETAIL);
                    break;
                case Constants.ON_EVENT_FALSE:
                    dismissLoading();
                    if(msg.getError()!=null){
                        if(msg.getError().getCode()!=null){
                            if(msg.getError().getCode().equals(MyApplication.getFreeLogin())){
                                if(msg.getError().getMessage()!=null){
                                    getLoginDialog(BillFindActivity.this,msg.getError().getMessage());
                                }
                            }else{
                                if(msg.getError().getMessage()!=null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    dismissLoading();
                    PayBean.DataBean data = msg.getData();
                    if(data !=null){
                        Intent intent=new Intent();
                        if (Constants.HPAY_REFUND_ORDER.equals(otherType)) {
                            intent.putExtra(Constants.INTENT_TRADE_DETAIL, PayBeanUtil.getTradeDetail(data));
                            intent.putExtra(Constants.INTENT_BILL_DATA, data);
                            intent.setClass(BillFindActivity.this, RefundDetailActivity.class);
                            startActivityForResult(intent, Constants.REQUEST_BILL_DETAIL);
                        } else {
                            intent.putExtra(Constants.INTENT_BILL_DATA, data);
                            intent.setClass(BillFindActivity.this, PayDetailActivity.class);
                            startActivityForResult(intent, Constants.REQUEST_BILL_DETAIL);
                        }

                    }
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_BILL_DETAIL) {
            if (data != null) {
                int orderStatus = data.getIntExtra(Constants.RESULT_CASHIER_INTENT, -1);
                String orderNo = data.getStringExtra(Constants.RESULT_ORDER_NO);
                if (orderStatus == -1 || orderNo == null)
                    return;
                for (int i = 0; i < mList.size(); i++) {
                    if (orderNo.equals(mList.get(i).getOrderNo())) {
                        mList.get(i).setTradeState(orderStatus);
                        mAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        }
    }

    private OnItemSearchListener mOnItemSearchListener;

    public interface OnItemSearchListener {
        void onItemSearch(String text);
    }

    public void setOnItemSearchListener(OnItemSearchListener listener) {
        this.mOnItemSearchListener = listener;
    }
}