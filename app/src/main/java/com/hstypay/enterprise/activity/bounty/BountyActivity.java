package com.hstypay.enterprise.activity.bounty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.DateChoiceActivity;
import com.hstypay.enterprise.adapter.BountyRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BountyDataBean;
import com.hstypay.enterprise.bean.ShareBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class BountyActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvBountyMoneyTitle, mTvBountyMoney, mTvBountyCount, mTvBountyDate, mBtnTitle;
    private EditText mEtPwd;
    private SafeDialog mLoadDialog;
    private RecyclerView mRvBounty;
    private int currentPage = 1;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private NoticeDialog mNoticeDialog;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private BountyRecyclerAdapter mBillRecyclerAdapter;
    private List<BountyDataBean.DataEntity> mBountyList;

    private String startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
    private String endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bounty);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtPwd = (EditText) findViewById(R.id.et_login_pwd);
        mTvTitle.setText(R.string.title_bounty);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mTvBountyDate = (TextView) findViewById(R.id.tv_bounty_date);
        mTvBountyMoneyTitle = (TextView) findViewById(R.id.tv_bounty_money_title);
        mTvBountyMoney = (TextView) findViewById(R.id.tv_bounty_money);
        mTvBountyCount = (TextView) findViewById(R.id.tv_bounty_count);

        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvBounty = (RecyclerView) findViewById(R.id.rv_bounty);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvBounty.setLayoutManager(mLinearLayoutManager);
        mRvBounty.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                initAllData();
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    currentPage++;
                    Map<String, Object> map = getRequestMap(pageSize + "", currentPage + "", startTime, endTime);
                    getBountyList(map);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    private void initAllData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            isRefreshed = true;
            currentPage = 1;
            mBountyList.clear();
            Map<String, Object> map = getRequestMap(pageSize + "", currentPage + "", startTime, endTime);
            getBountyList(map);
            Map<String, Object> collectMap = new HashMap<>();
            collectMap.put("startTime", startTime);
            collectMap.put("endTime", endTime);
            if (MyApplication.getIsCasher()) {
                collectMap.put("userId", MyApplication.getUserId());
            }
            getBountyCollect(collectMap);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, 500);
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mTvBountyDate.setOnClickListener(this);
        mTvBountyMoneyTitle.setOnClickListener(this);
    }

    private void initData() {
        mTvBountyDate.setText(DateUtil.getDate());
        mBountyList = new ArrayList<>();
        mBillRecyclerAdapter = new BountyRecyclerAdapter(MyApplication.getContext(), mBountyList);
        mRvBounty.setAdapter(mBillRecyclerAdapter);

        initAllData();
    }

    private Map<String, Object> getRequestMap(String pageSize, String currentPage, String startTime, String endTime) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (MyApplication.getIsCasher()) {
            map.put("userId", MyApplication.getUserId());
        }
        return map;
    }

    public void getBountyList(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).getBountyList(MyApplication.getContext(), Constants.TAG_GET_BOUNTY_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    public void getBountyCollect(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).getBountyCollect(MyApplication.getContext(), Constants.TAG_GET_BOUNTY_COLLECT, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_bounty_date:
                Intent intentDate = new Intent(BountyActivity.this, DateChoiceActivity.class);
                intentDate.putExtra(Constants.INTENT_NAME, Constants.INTENT_REPORT_CHOICE_DATE);
                startActivityForResult(intentDate, Constants.REQUEST_CHOICE_DATE);
                break;
            case R.id.tv_bounty_money_title:
                showNoticeDialog();
                break;

        }
    }

    private void showNoticeDialog() {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(BountyActivity.this, null, null, R.layout.notice_dialog_bounty);
        }
        mNoticeDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHOICE_DATE) {
            Bundle extras = data.getExtras();
            String time = extras.getString(Constants.RESULT_CHOICE_DATE_INTENT);
            if (!StringUtils.isEmptyOrNull(time)) {
                String t[] = time.split("\\|");
                try {
                    if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formartDateToMMDD(t[1]))) {
                        /*if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                            mTvBountyDate.setText(DateUtil.formartDateToMMDD(t[0]) + "(今天)");
                        } else {*/
                        mTvBountyDate.setText(DateUtil.formartDateToYYMMDD(t[0]));
                        //}
                    } else {
                        mTvBountyDate.setText(DateUtil.formartDateToYYMMDD(t[0]) + " " + getString(R.string.tv_least) + " "
                                + DateUtil.formartDateToYYMMDD(t[1]));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //开始时间与结束时间发送到服务器获取数据
                startTime = t[0];
                endTime = t[1];
                LogUtil.d("date=" + startTime + "///" + endTime);
                initAllData();
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_BOUNTY_LIST)) {
            BountyDataBean msg = (BountyDataBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(BountyActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null) {
                        List<BountyDataBean.DataEntity> list = msg.getData();
                        if (list != null && list.size() > 0) {
                            mBountyList.addAll(list);
                            mBillRecyclerAdapter.notifyDataSetChanged();
                        } else {
                            mBillRecyclerAdapter.notifyDataSetChanged();
                            if (isLoadmore) {
                                MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                            }
                        }
                    }
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_GET_BOUNTY_COLLECT)) {
            ShareBean msg = (ShareBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(BountyActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        mTvBountyMoney.setText(DateUtil.formatMoneyUtil(msg.getData().getRewardFee() / 100d));
                        mTvBountyCount.setText(msg.getData().getRewardCount() + "");
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }
}
