package com.hstypay.enterprise.activity.cashier;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.ChangeNameActivity;
import com.hstypay.enterprise.activity.ResetPasswordActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.activity.storeCode.StoreCodeListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.StoresBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/24 10:09
 * @描述: 收银员详情
 */
public class EmployeeInfoActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private ImageView mIvBack, mIvStoreArrow, mIvForbidden;
    private Button mBtnSubmit,mBtnResetPwd;
    private TextView mButton, mTvTitle, mTvOrderTitle, mTvAddCashierTitle, mTvReportTitle, mTvRefundTitle, mTvForbiddenTitle, mTvOpenAdress, mTv_login_name,mEtEmpName;
    private RelativeLayout mRlEmpShop,mRlEmpName,mRlStaticCode;
    private CheckBox mCbOrderList, mCbReport, mCbRefund;
    private boolean isEnable = false;
    private CashierDetailBean.DataBean mInfo;
    //private TreeMap<String, String> mStoreMap;
    private List<String> mList = new ArrayList<>();
    private List<StoresBean> mStoresBean = new ArrayList<>();
    private String mStoreId;
    private NoticeDialog mNoticeDialog;
    private SelectDialog mSelectDialog;
    private LinearLayout mLlRefundPermission;
    private SafeDialog mLoadDialog;
    private TextView mTvEmpRole, mTvEmpTel, mTvActiveNotice;
    private boolean isVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_info);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mRlEmpShop = (RelativeLayout) findViewById(R.id.rl_emp_shop);
        mRlEmpName = (RelativeLayout) findViewById(R.id.rl_emp_name);
        mRlStaticCode = (RelativeLayout) findViewById(R.id.rl_static_code);
        mIvStoreArrow = (ImageView) findViewById(R.id.iv_open_forward);
        mTvOpenAdress = (TextView) findViewById(R.id.openAdress);
        mEtEmpName = findViewById(R.id.et_emp_name);
        mTvEmpRole = findViewById(R.id.tv_emp_role);
        mTv_login_name = (TextView) findViewById(R.id.tv_login_name);
        mTvEmpTel = (TextView) findViewById(R.id.tv_emp_tel);
        mTvTitle.setText(R.string.title_emp_detail);
        mButton.setVisibility(View.INVISIBLE);
        mTvOrderTitle = (TextView) findViewById(R.id.tv_order_title);
        mTvReportTitle = (TextView) findViewById(R.id.tv_report_title);
        mTvRefundTitle = (TextView) findViewById(R.id.tv_refund_title);
        mCbOrderList = findViewById(R.id.cb_order_list);
        mCbReport = findViewById(R.id.cb_report);
        mCbRefund = findViewById(R.id.cb_refund);
        mTvForbiddenTitle = (TextView) findViewById(R.id.tv_forbidden_title);
        mIvForbidden = (ImageView) findViewById(R.id.iv_forbidden_switch);
        mTvAddCashierTitle = (TextView) findViewById(R.id.tv_add_user_title);
        mTvActiveNotice = (TextView) findViewById(R.id.tv_active_notice);
        mLlRefundPermission = (LinearLayout) findViewById(R.id.ll_refund_permission);

        mBtnResetPwd = (Button) findViewById(R.id.btn_reset_pwd);
        mBtnSubmit = (Button) findViewById(R.id.btn_delete);
    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mRlEmpShop.setOnClickListener(this);
        mRlEmpName.setOnClickListener(this);
        mRlStaticCode.setOnClickListener(this);
        mIvForbidden.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mBtnResetPwd.setOnClickListener(this);
        mTvAddCashierTitle.setOnClickListener(this);
        mCbOrderList.setOnCheckedChangeListener(this);
        mCbRefund.setOnCheckedChangeListener(this);
        mCbReport.setOnCheckedChangeListener(this);
        mCbReport.setOnCheckedChangeListener(this);

    }

    public void initData() {
        mInfo = (CashierDetailBean.DataBean) getIntent().getSerializableExtra("storeInfo");
//        String cashierUserName = getIntent().getStringExtra("cashierUserName");
        if (mInfo != null) {
            mTvActiveNotice.setVisibility(mInfo.getActivateStatus() != 1 ? View.VISIBLE : View.GONE);
            mLlRefundPermission.setVisibility(mInfo.isRoleApplyRefund() ? View.VISIBLE : View.GONE);
            mEtEmpName.setText(mInfo.getRealName());
            mTvEmpRole.setText("收银员");
//            mEtEmpName.setSelection(mEtEmpName.getText().toString().length());
            List<StoresBean> stores = mInfo.getStores();
            if (stores != null && stores.size() > 0) {
                String storeName = "";
//                for (int i = 0; i < stores.size(); i++) {
                mStoreId = stores.get(0).getStoreId();
                storeName = stores.get(0).getStoreName();
                mList.clear();
                mList.add(mStoreId);
                mStoresBean.clear();
                StoresBean storesBean = new StoresBean();
                storesBean.setStoreId(mStoreId);
                storesBean.setStoreName(storeName);
                mStoresBean.add(storesBean);
//                }
                if (StringUtils.getStrLength(storeName) > 26) {
                    mTvOpenAdress.setText(StringUtils.splitStr(storeName, 24) + "...");
                } else {
                    mTvOpenAdress.setText(storeName);
                }
            }
            mTv_login_name.setText(mInfo.getLoginPhone());
            mTvEmpTel.setText(mInfo.getLoginPhone());
            mCbRefund.setChecked(mInfo.isApplyRefund());
            mCbOrderList.setChecked(mInfo.isApplyWater());
            mCbReport.setChecked(mInfo.isApplyBill());

            if (mInfo.isEnabled()) {
                mIvForbidden.setImageResource(R.mipmap.ic_switch_open);
                isEnable = true;
            } else {
                mIvForbidden.setImageResource(R.mipmap.ic_switch_close);
                isEnable = false;
            }
            setView(mInfo.getEditEnable());
        }
    }

    private void setView(int editEnable) {
        if (editEnable == 0) {
//            mEtEmpName.setEnable(false);
            mRlEmpShop.setEnabled(false);
            mCbOrderList.setEnabled(false);
            mCbReport.setEnabled(false);
            mCbRefund.setEnabled(false);
            mIvForbidden.setEnabled(false);
            mIvStoreArrow.setVisibility(View.INVISIBLE);
            mBtnSubmit.setVisibility(View.GONE);

            mEtEmpName.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvOpenAdress.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTv_login_name.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvOrderTitle.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvReportTitle.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvRefundTitle.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvForbiddenTitle.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvEmpRole.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvEmpTel.setTextColor(UIUtils.getColor(R.color.home_value_text));
        } else {
//            mEtEmpName.setEnable(false);
            mRlEmpShop.setEnabled(true);
            mCbOrderList.setEnabled(true);
            mCbReport.setEnabled(true);
            mCbRefund.setEnabled(true);
            mIvForbidden.setEnabled(true);
            mIvStoreArrow.setVisibility(View.VISIBLE);
            mBtnSubmit.setVisibility(View.VISIBLE);

            mEtEmpName.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvOpenAdress.setTextColor(UIUtils.getColor(R.color.home_text));
            mTv_login_name.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvOrderTitle.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvReportTitle.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvRefundTitle.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvForbiddenTitle.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvEmpRole.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvEmpTel.setTextColor(UIUtils.getColor(R.color.home_text));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_emp_shop:
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                startActivityForResult(intentShop, Constants.REQUEST_CASHIER_SHOP_EDIT_CODE);
                break;
            case R.id.rl_emp_name:
                Intent intentName = new Intent(this, ChangeNameActivity.class);
                intentName.putExtra(Constants.INTENT_CHANGE_NAME, mEtEmpName.getText().toString().trim());
                startActivityForResult(intentName, Constants.REQUEST_CHANGE_NAME);
                break;
            case R.id.iv_forbidden_switch:
                setForbiddenView();
                break;
            case R.id.btn_delete://删除
                showDeleteDialog();
                break;
            case R.id.tv_add_user_title:
                showNoticeDialog();
                break;
            case R.id.rl_static_code:
                Intent intentCode = new Intent(this, StoreCodeListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.INTENT_USER_ID, mInfo.getUserId());
                bundle.putSerializable(Constants.INTENT_USER_STORE, (Serializable) mInfo.getStores());
                intentCode.putExtras(bundle);
                startActivity(intentCode);
                break;
            case R.id.btn_reset_pwd:
                resetPassword(mInfo.getUserId());
                break;
            default:
                break;
        }
    }

    private void resetPassword(String phone) {
        Intent intent = new Intent(EmployeeInfoActivity.this, ResetPasswordActivity.class);
        intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, phone);
        intent.putExtra(Constants.INTENT_NAME, "INTENT_EMP_RESET_PWD");
        startActivity(intent);
    }

    public void showDeleteDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(EmployeeInfoActivity.this
                    , getString(R.string.dialog_delete_notice)
                    , getString(R.string.btnCancel)
                    , getString(R.string.ensure_check), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    delete();
                }
            });
            DialogHelper.resize(EmployeeInfoActivity.this, mSelectDialog);
        }
        mSelectDialog.show();
    }

    private void delete() {
        if (!NetworkUtils.isNetworkAvailable(EmployeeInfoActivity.this)) {
            showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("userId", mInfo.getUserId());
            map.put("storeIds", mList);
            ServerClient.newInstance(EmployeeInfoActivity.this).deleteCashier(EmployeeInfoActivity.this, Constants.TAG_DELETE_CASHIER, map);
        }
    }


    private void submit() {
//        String userId = getIntent().getStringExtra("userId");
        if (TextUtils.isEmpty(mEtEmpName.getText().toString().trim())) {
            showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.et_name_not_null));
            return;
        }
        if (StringUtils.isEmptyOrNull(mTvOpenAdress.getText().toString())) {
            showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.store_name));
            return;
        }
        if (!NetworkUtils.isNetworkAvailable(EmployeeInfoActivity.this)) {
            showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("realName", mEtEmpName.getText().toString().trim());
            if (mList != null && mList.size() > 0) {
                map.put("storeIds", mList);
            }
            map.put("userId", mInfo.getUserId());
            ServerClient.newInstance(EmployeeInfoActivity.this).cashierEdit(EmployeeInfoActivity.this, Constants.TAG_CASHIER_EDIT, map);
        }
    }

    private void showNoticeDialog() {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(EmployeeInfoActivity.this, null, null, R.layout.notice_dialog_cashier_permission);
            DialogHelper.resize(EmployeeInfoActivity.this, mNoticeDialog, 0.8f);
        }
        mNoticeDialog.show();
    }

    private void getPermission(String opCode, String userId, CheckBox checkBox) {
        if (!NetworkUtils.isNetworkAvailable(EmployeeInfoActivity.this)) {
            showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("opCode", opCode);
            map.put("userId", userId);
            if (checkBox.isChecked()) {
                map.put("openFlag", "1");
            } else {
                map.put("openFlag", "0");
            }
            ServerClient.newInstance(EmployeeInfoActivity.this).cashierPermission(EmployeeInfoActivity.this, Constants.TAG_CASHIER_PERMISSION, map);
        }
    }

    private void setForbiddenView() {
        if (isEnable) {
            MtaUtils.mtaId(EmployeeInfoActivity.this, "I007");
            mIvForbidden.setImageResource(R.mipmap.ic_switch_close);
            isEnable = false;
            setForbidden(mInfo.getUserId(), isEnable);//开关关闭
        } else {
            MtaUtils.mtaId(EmployeeInfoActivity.this, "I006");
            mIvForbidden.setImageResource(R.mipmap.ic_switch_open);
            isEnable = true;
            setForbidden(mInfo.getUserId(), isEnable);//开关开启
        }
    }

    private void setForbidden(String userId, boolean enabled) {
        if (!NetworkUtils.isNetworkAvailable(EmployeeInfoActivity.this)) {
            showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("enabled", enabled);
            map.put("userId", userId);
            ServerClient.newInstance(EmployeeInfoActivity.this).freezeCashier(EmployeeInfoActivity.this, Constants.TAG_CASHIER_FREEZE, map);
        }
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFreezeCashier(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CASHIER_FREEZE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.CASHIER_FREEZE_TRUE:
                    setResult(RESULT_OK);
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_SHOP_EDIT_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            if (StringUtils.getStrLength(shopBean.getStoreName()) > 26) {
                mTvOpenAdress.setText(StringUtils.splitStr(shopBean.getStoreName(), 24) + "...");
            } else {
                mTvOpenAdress.setText(shopBean.getStoreName());
            }
            mStoreId = shopBean.getStoreId();
            mList.clear();
            mList.add(mStoreId);
            mStoresBean.clear();
            StoresBean storesBean = new StoresBean();
            storesBean.setStoreId(mStoreId);
            storesBean.setStoreName(shopBean.getStoreName());
            mStoresBean.add(storesBean);
            submit();
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHANGE_NAME) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                String name = extras.getString(Constants.RESULT_CHANGE_NAME);
                mEtEmpName.setText(name);
                submit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    //Eventbus接收数据，退款权限返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCashierEdit(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CASHIER_EDIT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_CASHIER_EDIT);
                    break;
                case Constants.CASHIER_EDIT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmployeeInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(EmployeeInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CASHIER_EDIT_TRUE:
//                    getDialog(getString(R.string.cashier_change_success));
                    mInfo.setRealName(mEtEmpName.getText().toString().trim());
                    mInfo.setStores(mStoresBean);
                    setResult(RESULT_OK);
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_DELETE_CASHIER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmployeeInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(EmployeeInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(EmployeeInfoActivity.this, getString(R.string.delete_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    //Eventbus接收数据，退款权限返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderReverse(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CASHIER_PERMISSION)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierDetailBean msg = (CashierDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmployeeInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(EmployeeInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }


    /*public void getDialog(String title) {
        showCommonNoticeDialog(EmployeeInfoActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent();
                intent.putExtra(Constants.INTENT_NAME_MANAGER_INFO, mEtEmpName.getText().toString().trim());
                setResult(RESULT_OK, intent);
                EmployeeInfoActivity.this.finish();
            }
        });
    }*/

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_order_list:
                if (isVisible)
                    getPermission("APPLY-WATER", mInfo.getUserId(), mCbOrderList);
                break;
            case R.id.cb_report:
                if (isVisible)
                    getPermission("APPLY-BILL", mInfo.getUserId(), mCbReport);
                break;
            case R.id.cb_refund:
                if (isVisible)
                    getPermission("APPLY-REFUND", mInfo.getUserId(), mCbRefund);
                break;
        }
    }

    /*@Override
    public void onBackPressed() {
        finishActivity(Constants.INTENT_FINISH);
        super.onBackPressed();
    }*/

    /*private void finishActivity(String intentName){
        Intent intent = new Intent();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(Constants.RESULT_MANAGER_DELETE, mInfo);
        mBundle.putString(Constants.INTENT_NAME, intentName);
        intent.putExtras(mBundle);
        setResult(RESULT_OK, intent);
        finish();
    }*/
}
