package com.hstypay.enterprise.activity.cloudprint;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.dialog.CustomViewBottomDialog;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudPrintClearDataBean;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author dean.zeng
 * @Description 云打印机设置\详情
 * @Date 2020-02-05 15:47
 **/
public class CloudPrintInfoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle, mTvDeviceType, mTvDeviceNo, mTvDeviceKey,mTvDeviceStore, mButton;
    private LinearLayout mLlContent;
    private SafeDialog mLoadDialog;
    private String mDeviceSn;
    private String mStoreID;
    private String mDeviceId;
    private CommonNoticeDialog mDialogSuccess;
    private CloudPrintDetailBean.DataBean mCloudDetailBean;
    private SelectDialog mSelectDialog;
    private RelativeLayout mRlWaitCloudPrintCount;//rl待打印数据
    private RelativeLayout mRlSetPayTicketPrint;//支付小票打印
    private RelativeLayout mRlSetScanOrderDiningPrint;
    private RelativeLayout mRlStore;
    private TextView mTvDeviceModel;//设备型号
    private TextView mTvCloudDeviceStatus;//设备状态
    private TextView mTvWaitCloudPrintCount;//待打印数据数量

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_print_info);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(CloudPrintInfoActivity.this, getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvDeviceType = (TextView) findViewById(R.id.tv_cloud_device_type);
        mTvDeviceNo = (TextView) findViewById(R.id.tv_cloud_device_no);
        mTvDeviceKey = (TextView) findViewById(R.id.tv_cloud_device_key);
        mLlContent = (LinearLayout) findViewById(R.id.ll_content);
        mRlWaitCloudPrintCount = findViewById(R.id.rl_wait_cloud_print_count);
        mRlSetPayTicketPrint = findViewById(R.id.rl_set_pay_ticket_print);
        mRlSetScanOrderDiningPrint = findViewById(R.id.rl_set_scan_order_dining_print);
        mTvDeviceModel = findViewById(R.id.tv_device_model);
        mTvCloudDeviceStatus = findViewById(R.id.tv_cloud_device_status);
        mTvWaitCloudPrintCount = findViewById(R.id.tv_wait_cloud_print_count);
        mRlStore =  findViewById(R.id.rl_store);
        mTvDeviceStore = findViewById(R.id.tv_device_store);

        mTvTitle.setText(R.string.tv_title_cloud_print);
        mButton.setText(R.string.btn_unbind);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mRlWaitCloudPrintCount.setOnClickListener(this);
        mRlSetScanOrderDiningPrint.setOnClickListener(this);
        mRlSetPayTicketPrint.setOnClickListener(this);
    }

    private void initData() {
        mDeviceSn = getIntent().getStringExtra(Constants.INTENT_DEVICE_SN);
        mStoreID = getIntent().getStringExtra(Constants.INTENT_STORE_ID);
        mDeviceId = getIntent().getStringExtra(Constants.INTENT_DEVICE_ID);

    }

    @Override
    protected void onResume() {
        super.onResume();
        cloudDetail();
    }

    private void cloudDetail() {
        if (!NetworkUtils.isNetworkAvailable(CloudPrintInfoActivity.this)) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", mDeviceSn);
            ServerClient.newInstance(CloudPrintInfoActivity.this).cloudPrintDetail2(CloudPrintInfoActivity.this, Constants.TAG_CLOUD_DEVICE_DETAIL, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_DETAIL)) {
            //云打印详情数据
            DialogUtil.safeCloseDialog(mLoadDialog);
            CloudPrintDetailBean msg = (CloudPrintDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudPrintInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mCloudDetailBean = msg.getData();
                    if (msg.getData() != null) {
                        mDeviceSn = msg.getData().getSn();
                        mLlContent.setVisibility(View.VISIBLE);
                        mTvDeviceType.setText(msg.getData().getCategoryName());
                        mTvDeviceModel.setText(msg.getData().getDeviceModel());
                        mTvDeviceNo.setText(msg.getData().getSn());
                        if (!TextUtils.isEmpty(msg.getData().getStoreMerchantId())) {
                            mRlStore.setVisibility(View.VISIBLE);
                            mTvDeviceStore.setText(msg.getData().getStoreMerchantIdCnt());
                        } else {
                            mRlStore.setVisibility(View.GONE);
                        }
                        /*if ("1".equals(state)){
                            //设备离线状态
                            mTvCloudDeviceStatus.setText(getResources().getString(R.string.cloud_device_state_offline));
                        }else if ("2".equals(state)){
                            //设备在线状态
                            mTvCloudDeviceStatus.setText(getResources().getString(R.string.cloud_device_state_online));
                        }else if ("3".equals(state)){
                            //设备不正常状态
                            mTvCloudDeviceStatus.setText(getResources().getString(R.string.cloud_device_state_unnormal));
                        }*/
                        if (TextUtils.isEmpty(msg.getData().getState())) {
                            mTvCloudDeviceStatus.setText("--");
                        } else {
                            mTvCloudDeviceStatus.setText(msg.getData().getState());
                        }
                        if (TextUtils.isEmpty(msg.getData().getWaitingPrintCount())) {
                            mTvWaitCloudPrintCount.setText("0条");
                        } else {
                            mTvWaitCloudPrintCount.setText(msg.getData().getWaitingPrintCount() + "条");
                        }
                        if(msg.getData().getSetUpBean() == null)
                            mRlSetScanOrderDiningPrint.setVisibility(View.GONE);
                    } else {
                        MyToast.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_UNBIND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudPrintInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_notice_unbind_success));
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_CLEARWAITCOUNT)) {
            //清空待打印数据
            DialogUtil.safeCloseDialog(mLoadDialog);
            CloudPrintClearDataBean msg = (CloudPrintClearDataBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudPrintInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    /*CloudPrintClearDataBean.Data data = msg.getData();
                    if(data!=null){
                        boolean clearStatus = data.getClearStatus();
                        if (clearStatus){
                            mTvWaitCloudPrintCount.setText("0条");
                            if(mCloudDetailBean!=null){
                                mCloudDetailBean.setWaitingPrintCount("0");
                            }
                        }
                    }else {
                        MyToast.showToastShort(getString(R.string.error_data));
                    }*/
                    String data = msg.getData();
                    if ("true".equals(data)) {
                        MyToast.showToastShort("清空成功");
                        mTvWaitCloudPrintCount.setText("0条");
                        if (mCloudDetailBean != null) {
                            mCloudDetailBean.setWaitingPrintCount("0");
                        }
                    } else {
                        MyToast.showToastShort("清空失败");
                    }

                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (mSelectDialog == null) {
                    mSelectDialog = new SelectDialog(this, getString(R.string.dialog_cloud_print_unbind_content), R.layout.select_common_dialog);
                    mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                        @Override
                        public void clickOk() {
                            unbindCloudDevice(mDeviceSn);
                        }
                    });
                }
                mSelectDialog.show();
                break;
            case R.id.rl_wait_cloud_print_count:
                //清空待待打印数据弹窗
                showClearPrintCountDialog();
                break;
            case R.id.rl_set_scan_order_dining_print:
                //扫码点餐小票打印
                Intent intent = new Intent(CloudPrintInfoActivity.this, CloudPrintModeActivity.class);
                intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mCloudDetailBean);
                startActivityForResult(intent, Constants.REQUEST_CLOUD_DEVICE_PRINT_DETAIL);
                break;
            case R.id.rl_set_pay_ticket_print:
                //支付小票打印设置
                Intent intentPay = new Intent(CloudPrintInfoActivity.this, CloudPrintTicketSetActivity.class);
                intentPay.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mCloudDetailBean);
                startActivityForResult(intentPay, Constants.REQUEST_CLOUD_DEVICE_PRINT_DETAIL);
                break;
        }
    }

    private void showClearPrintCountDialog() {
        CustomViewBottomDialog customViewBottomDialog = new CustomViewBottomDialog(this);
        customViewBottomDialog.setView(R.layout.dialog_cloud_print_clear_count);
        customViewBottomDialog.findViewById(R.id.tv_clear_print_data).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customViewBottomDialog.dismiss();
                if (!NetworkUtils.isNetworkAvailable(CloudPrintInfoActivity.this)) {
                    MyToast.showToastShort(getString(R.string.network_exception));
                    return;
                } else {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    map.put("sn", mDeviceSn);
                    map.put("storeMerchantId", mStoreID);
                    map.put("deviceId", mDeviceId);
                    ServerClient.newInstance(CloudPrintInfoActivity.this).clearCloudPrintWaitCount(CloudPrintInfoActivity.this, Constants.TAG_CLOUD_DEVICE_CLEARWAITCOUNT, map);
                }
            }
        });
        customViewBottomDialog.findViewById(R.id.tv_cancel_clear_data).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customViewBottomDialog.dismiss();
            }
        });
        customViewBottomDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CLOUD_DEVICE_PRINT_DETAIL) {
            CloudPrintDetailBean.DataBean dataBean = (CloudPrintDetailBean.DataBean) data.getSerializableExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL);
            mCloudDetailBean = dataBean;
        }
    }

    private void unbindCloudDevice(String code) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            map.put("operateType", 2);
            map.put("merchatId", MyApplication.getMechantId());
            map.put("purposeCode", "5");
            ServerClient.newInstance(MyApplication.getContext()).cloudBind(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_UNBIND, map);
        }
    }


    public void getDialogSuccess(String title) {
        mDialogSuccess = new CommonNoticeDialog(CloudPrintInfoActivity.this, title, getString(R.string.dialog_notice_button));
        mDialogSuccess.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                startActivity(new Intent(CloudPrintInfoActivity.this, DeviceListActivity.class));
                CloudPrintInfoActivity.this.finish();
            }
        });
        DialogHelper.resize(CloudPrintInfoActivity.this, mDialogSuccess);
        mDialogSuccess.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDialogSuccess != null) {
            mDialogSuccess.dismiss();
        }
        if (mSelectDialog != null) {
            mSelectDialog.dismiss();
        }
    }
}
