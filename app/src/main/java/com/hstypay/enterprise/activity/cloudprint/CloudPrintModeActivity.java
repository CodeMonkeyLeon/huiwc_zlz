package com.hstypay.enterprise.activity.cloudprint;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.SetUp;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


/**
 * @Author dean.zeng
 * @Description 扫码点餐模板
 * @Date 2020-02-06 15:47
 **/
public class CloudPrintModeActivity extends BaseActivity {

    private SafeDialog mLoadDialog;
    private ImageView mIvBack, mImgKitchenControl, mImgSettlement, mImgCustomer, mImgKitchen, mImgCountMerge;
    private TextView mTvTitle, mButton;
    private View mRlKitchenControl, mLlKitchenContainer;
    private CloudPrintDetailBean.DataBean mBean;
    private SetUp mSetUp;
    private boolean isOpen;//是否展开

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_order_control);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mImgKitchenControl = findViewById(R.id.iv_print_kitchen_control);
        mRlKitchenControl = findViewById(R.id.rl_kitchen_control);
        mLlKitchenContainer = findViewById(R.id.ll_kitchen_container);

        mImgSettlement = findViewById(R.id.iv_print_pay_order);
        mImgCustomer = findViewById(R.id.iv_print_client_order);
        mImgKitchen = findViewById(R.id.iv_print_kitchen_order);
        mImgCountMerge = findViewById(R.id.iv_print_kitchen_count);

        mBean = (CloudPrintDetailBean.DataBean) getIntent().getSerializableExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL);
        if (mBean != null) {
            mSetUp = mBean.getSetUpBean();
            mImgSettlement.setImageResource(!TextUtils.isEmpty(mSetUp.getSettlementPrint()) && mSetUp.getSettlementPrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            mImgCustomer.setImageResource(!TextUtils.isEmpty(mSetUp.getCustomerPrint()) && mSetUp.getCustomerPrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            mImgKitchen.setImageResource(!TextUtils.isEmpty(mSetUp.getKitchenPrint()) && mSetUp.getKitchenPrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            mImgCountMerge.setImageResource(!TextUtils.isEmpty(mSetUp.getCountMergePrint()) && mSetUp.getCountMergePrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);

        }

        mImgKitchenControl.setRotation(90);
        mTvTitle.setText(getString(R.string.tv_order_print_mode));
        mButton.setText(R.string.confirm);
        mImgKitchenControl.setImageResource(R.mipmap.ic_arrow_right);
    }

    private void initListener() {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mImgSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetUp.setSettlementPrint(mSetUp.getSettlementPrint().equals("1") ? "2" : "1");
                mImgSettlement.setImageResource(mSetUp.getSettlementPrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            }
        });
        mImgCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetUp.setCustomerPrint(mSetUp.getCustomerPrint().equals("1") ? "2" : "1");
                mImgCustomer.setImageResource(mSetUp.getCustomerPrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            }
        });
        mImgKitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetUp.setKitchenPrint(mSetUp.getKitchenPrint().equals("1") ? "2" : "1");
                mImgKitchen.setImageResource(mSetUp.getKitchenPrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            }
        });
        mImgCountMerge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetUp.setCountMergePrint(mSetUp.getCountMergePrint().equals("1") ? "2" : "1");
                mImgCountMerge.setImageResource(mSetUp.getCountMergePrint().equals("1") ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSn();
            }
        });
        mRlKitchenControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOpen = !isOpen;
                if (isOpen) {
                    mLlKitchenContainer.setVisibility(View.GONE);
                    mImgKitchenControl.setRotation(0);
                } else {
                    mLlKitchenContainer.setVisibility(View.VISIBLE);
                    mImgKitchenControl.setRotation(90);
                }
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_PRINT_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudPrintModeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_set_success));
                    break;
            }
        }
    }

    /**
     * 设置开关
     */
    private void updateSn() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", mBean.getSn());
            String setUp = new Gson().toJson(mSetUp);
            map.put("setUp", setUp);
            ServerClient.newInstance(MyApplication.getContext()).cloudPrintSet(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_PRINT_DETAIL, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void getDialogSuccess(String title) {
        showCommonNoticeDialog(CloudPrintModeActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent();
                SetUp setUp = (SetUp) mSetUp.clone();
                mBean.setSetUpBean(setUp);
                if (mBean != null) {
                    intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mBean);
                    setResult(RESULT_OK, intent);
                    CloudPrintModeActivity.this.finish();
                }
            }
        });
    }
}
