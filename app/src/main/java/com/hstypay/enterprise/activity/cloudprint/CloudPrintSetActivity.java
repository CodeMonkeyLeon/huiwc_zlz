package com.hstypay.enterprise.activity.cloudprint;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.SetUp;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author dean.zeng
 * @Description 云打印设备设置(没用了的页面)
 * @Date 2020-02-05 15:47
 **/
public class CloudPrintSetActivity extends BaseActivity {

    private SafeDialog mLoadDialog;
    private ImageView mIvBack, mImgSwitch;
    private TextView mTvTitle, mButton;
    private CloudPrintDetailBean.DataBean mBean;
    private SetUp mSetUp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_print_set);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mImgSwitch = (ImageView) findViewById(R.id.img_type_switch);
        mButton = findViewById(R.id.button_title);

        mBean = (CloudPrintDetailBean.DataBean) getIntent().getSerializableExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL);
        mSetUp = mBean.getSetUpBean();
        if (mSetUp.getPayPrint().equals("1")) {
            mImgSwitch.setImageResource(R.mipmap.ic_switch_open);
        } else {
            mImgSwitch.setImageResource(R.mipmap.ic_switch_close);
        }
//        mButton.setText(R.string.confirm);
        mTvTitle.setText(R.string.tv_order_print_set);

    }

    private void initListener() {
//        mButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cloudPrintSet();
//            }
//        });
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.rl_cloud_print_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CloudPrintSetActivity.this, CloudPrintModeActivity.class);
                intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mBean);
                startActivityForResult(intent, Constants.REQUEST_CLOUD_DEVICE_PRINT_DETAIL);
            }
        });
        mImgSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSetUp.getPayPrint().equals("2")) {
                    mSetUp.setPayPrint("1");
                    mImgSwitch.setImageResource(R.mipmap.ic_switch_open);
                } else {
                    mSetUp.setPayPrint("2");
                    mImgSwitch.setImageResource(R.mipmap.ic_switch_close);
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_PRINT_SET)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudPrintSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_set_success));
                    break;
            }
        }
    }

    /**
     * 设置开关
     */
    private void cloudPrintSet() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", mBean.getSn());
            String setUp = new Gson().toJson(mSetUp);
            map.put("setUp", setUp);
            ServerClient.newInstance(MyApplication.getContext()).cloudPrintSet(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_PRINT_SET, map);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CLOUD_DEVICE_PRINT_DETAIL) {
            CloudPrintDetailBean.DataBean dataBean = (CloudPrintDetailBean.DataBean) data.getSerializableExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL);
            mBean = dataBean;
            Intent intent = new Intent();
            SetUp setUp = (SetUp) mSetUp.clone();
            mBean.setSetUpBean(setUp);
            if (mBean != null) {
                intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mBean);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void getDialogSuccess(String title) {
        showCommonNoticeDialog(CloudPrintSetActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent();
                SetUp setUp = (SetUp) mSetUp.clone();
                mBean.setSetUpBean(setUp);
                if (mBean != null) {
                    intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mBean);
                    setResult(RESULT_OK, intent);
                    CloudPrintSetActivity.this.finish();
                }
            }
        });
    }

}
