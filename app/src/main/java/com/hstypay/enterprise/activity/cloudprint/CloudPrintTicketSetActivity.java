package com.hstypay.enterprise.activity.cloudprint;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.dialog.CustomViewBottomDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.bean.CloudPrintClearDataBean;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/9/16
 * @desc 云打印支付小票打印设置
 */
public class CloudPrintTicketSetActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout mRlSelectTicketType;//小票类型
    private LinearLayout mLlSetSwitchCloudPrint;//云打印开关设置
    private CloudPrintDetailBean.DataBean mBean;
    private LinearLayout mLlContentTicketSet;
    private ImageView mIvSwitchAutoPrint;
    private TextView mTvTicketTypeShow;
    private boolean isSwitchAutoPrintOpen = false;//当前的自动打印小票开关状态
    private String mConfigType = "-1";//当前的票据类型
    private SafeDialog mLoadDialog;
    private int mOperationType;//操作类型 1操作自动打印小票开关，2操作小票类型
    private ImageView mIvBack;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_pay_ticket_set);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initListener() {
        mRlSelectTicketType.setOnClickListener(this);
        mLlSetSwitchCloudPrint.setOnClickListener(this);
        mIvSwitchAutoPrint.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, getString(R.string.public_loading), false);
        mBean = (CloudPrintDetailBean.DataBean) getIntent().getSerializableExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL);
        if(mBean!=null){
            mLlContentTicketSet.setVisibility(View.VISIBLE);
            setSwitchView(mBean);
            setTicketTypeView(mBean);
        }
    }

    private void setSwitchView(CloudPrintDetailBean.DataBean dataBean){
        mIvSwitchAutoPrint.setImageResource("0".equals(dataBean.getEnabled())?R.mipmap.ic_switch_close:R.mipmap.ic_switch_open);
        isSwitchAutoPrintOpen = "0".equals(dataBean.getEnabled())?false:true;
    }

    private void setTicketTypeView(CloudPrintDetailBean.DataBean dataBean){
        String configType = dataBean.getConfigType();
        if ("1".equals(configType)){
            //商户存根
            mTvTicketTypeShow.setText(getResources().getString(R.string.tv_shcg));
        }else if ("2".equals(configType)){
            //商户存根+顾客存根
            mTvTicketTypeShow.setText(getResources().getString(R.string.tv_shcg_gkcy));
        }else {
            mTvTicketTypeShow.setText("");
        }
        mConfigType = configType;
    }

    private void initView() {
        TextView mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.tv_auto_print_ticket_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mRlSelectTicketType = findViewById(R.id.rl_select_ticket_type);
        mLlSetSwitchCloudPrint = findViewById(R.id.ll_set_switch_cloud_print);
        mLlContentTicketSet = findViewById(R.id.ll_content_ticket_set);
        mLlContentTicketSet.setVisibility(View.GONE);
        mIvSwitchAutoPrint = findViewById(R.id.iv_switch_auto_print);
        mTvTicketTypeShow = findViewById(R.id.tv_ticket_type_show);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_switch_auto_print:
                setSwitchAutoPrint(mBean.getConfigType(),isSwitchAutoPrintOpen?"0":"1",1);
                break;
            case R.id.rl_select_ticket_type:
                showSelectTicketTypeDialog();
                break;
            case R.id.ll_set_switch_cloud_print:
                //打印关联设置
                Intent intent = new Intent(CloudPrintTicketSetActivity.this, CloudPrintTicketTypeSetActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CLOUD_PRINT_SET);
                intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mBean);
                startActivity(intent);
                break;

            case R.id.iv_back:
                finish();
                break;

        }

    }

    /**
     * 设置云打印机的自助打印小票开关和票据类型
     * @param configType 票据类型 1 商户存根，2用户+商户存根
     * @param enabled 自助打印小票标志 0 关闭，1开启
     * @param operationType 操作类型 1操作自动打印小票开关，2操作小票类型
     */
    private void setSwitchAutoPrint(String configType,String enabled,int operationType) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            mOperationType = operationType;
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", mBean.getSn());
            map.put("configType",configType);
            map.put("enabled",enabled);
            ServerClient.newInstance(this).updatePrinterBysn(this, Constants.TAG_CLOUD_DEVICE_UPDATEPRINTERBYSN, map);
        }
    }

    /**
     * 选择小票类型弹窗
     */
    private void showSelectTicketTypeDialog() {
        CustomViewBottomDialog selectTicketTypeDialog = new CustomViewBottomDialog(this);
        selectTicketTypeDialog.setView(R.layout.dialog_cloud_print_select_ticket_type);
        TextView tv_cancel_ticket_type_dialog = selectTicketTypeDialog.findViewById(R.id.tv_cancel_ticket_type_dialog);
        tv_cancel_ticket_type_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTicketTypeDialog.dismiss();
            }
        });
        selectTicketTypeDialog.findViewById(R.id.tv_shcg_ticket_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTicketTypeDialog.dismiss();
                //商户存根
                if (!"1".equals(mConfigType)){
                    setSwitchAutoPrint("1",mBean.getEnabled(),2);
                }
            }
        });
        selectTicketTypeDialog.findViewById(R.id.tv_shcg_gkcy_ticket_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTicketTypeDialog.dismiss();
                //商户存根+顾客存根
                if (!"2".equals(mConfigType)){
                    setSwitchAutoPrint("2",mBean.getEnabled(),2);
                }
            }
        });
        selectTicketTypeDialog.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if(event.getTag().equals(Constants.TAG_CLOUD_DEVICE_UPDATEPRINTERBYSN)) {
            //云打印机设置
            DialogUtil.safeCloseDialog(mLoadDialog);
            BaseBean msg = (BaseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudPrintTicketSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (mOperationType==1){
                        mBean.setEnabled(isSwitchAutoPrintOpen?"0":"1");
                        setSwitchView(mBean);
                    }else if (mOperationType == 2){
                        if ("1".equals(mConfigType)){
                            mBean.setConfigType("2");
                        }
                        if ("2".equals(mConfigType)){
                            mBean.setConfigType("1");
                        }
                        setTicketTypeView(mBean);
                    }
                    break;
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        if (mBean!=null){
            Intent intent = new Intent();
            intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, mBean);
            setResult(RESULT_OK);
        }
    }
}
