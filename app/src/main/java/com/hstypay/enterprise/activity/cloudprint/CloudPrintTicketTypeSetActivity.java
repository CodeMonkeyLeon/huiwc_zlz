package com.hstypay.enterprise.activity.cloudprint;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.fragment.CloudPrintTicketCachierTypeFragment;
import com.hstypay.enterprise.fragment.CloudPrintTicketCodeTypeFragment;
import com.hstypay.enterprise.fragment.CloudPrintTicketDeviceTypeFragment;
import com.hstypay.enterprise.fragment.CloudPrintTicketTypeBaseFragment;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 云打印关联设置，云播报关联设置
 */
public class CloudPrintTicketTypeSetActivity extends BaseActivity {

    private EditText mEt_search_ticket_set;
    private RadioGroup mRg_select_ticket_type;
    public static final int BIND_TYPE_CODE = 1;//收款碼
    public static final int BIND_TYPE_DEVICE = 2;//收款設備
    public static final int BIND_TYPE_CACHIER = 3;//收銀員
    private List<CloudPrintTicketTypeBaseFragment> mFragments;
    private FragmentManager mSupportFragmentManager;
    private int mLastTabIndex = -1;
    private CloudPrintDetailBean.DataBean mDetailBean;
    private ImageView mIvBack;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_print_ticket_type_set);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mEt_search_ticket_set.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mFragments.get(mLastTabIndex).search(v.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });

        mEt_search_ticket_set.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = mEt_search_ticket_set.getText().length();
                if (length==0){
                    mFragments.get(mLastTabIndex).search(null);
                }
            }
        });

        mRg_select_ticket_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_receive_code_ticket://收款碼
                        selectFragment(0);
                        mEt_search_ticket_set.setText("");
                        mEt_search_ticket_set.setHint(getResources().getString(R.string.tv_search_code_type));
                        break;
                    case R.id.rb_receive_device_ticket://收款設備
                        selectFragment(1);
                        mEt_search_ticket_set.setText("");
                        mEt_search_ticket_set.setHint(getResources().getString(R.string.tv_search_device_type));
                        break;
                    case R.id.rb_receive_cashier_ticket://收銀人員
                        selectFragment(2);
                        mEt_search_ticket_set.setText("");
                        mEt_search_ticket_set.setHint(getResources().getString(R.string.tv_search_cashier_type));
                        break;
                }
            }
        });
    }

    private void selectFragment(int index) {
        if (mLastTabIndex == index){
            return;
        }
        FragmentTransaction fragmentTransaction = mSupportFragmentManager.beginTransaction();
        if (mLastTabIndex!=-1){
            CloudPrintTicketTypeBaseFragment lastFragment = mFragments.get(mLastTabIndex);
            fragmentTransaction.hide(lastFragment);
        }
        CloudPrintTicketTypeBaseFragment fragment = mFragments.get(index);
        if (!fragment.isAdded()){
            fragmentTransaction.add(R.id.fl_content_ticket_type,fragment);
        }else {
            fragmentTransaction.show(fragment);
        }
        mLastTabIndex = index;
        fragmentTransaction.commit();
    }

    private void initData() {
        mDetailBean = (CloudPrintDetailBean.DataBean) getIntent().getSerializableExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL);
        mSupportFragmentManager = getSupportFragmentManager();
        mFragments = new ArrayList<>();
        mFragments.add(new CloudPrintTicketCodeTypeFragment());
        mFragments.add(new CloudPrintTicketDeviceTypeFragment());
        mFragments.add(new CloudPrintTicketCachierTypeFragment());
        selectFragment(0);
    }

    private void initView() {
        TextView tv_title = findViewById(R.id.tv_title);
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_NAME_CLOUD_VOICE_SET.equals(mIntentName)){
            //云播报
            tv_title.setText(getResources().getString(R.string.tv_title_voice_type));
        }else if (Constants.INTENT_NAME_CLOUD_PRINT_SET.equals(mIntentName)){
            //云打印
            tv_title.setText(getResources().getString(R.string.tv_title_ticket_type));
        }
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEt_search_ticket_set = findViewById(R.id.et_search_ticket_set);
        mRg_select_ticket_type = findViewById(R.id.rg_select_ticket_type);
    }

    public CloudPrintDetailBean.DataBean getDetailBean() {
        return mDetailBean;
    }

    public String getIntentName() {
        return mIntentName;
    }
}
