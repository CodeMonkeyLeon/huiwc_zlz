package com.hstypay.enterprise.activity.cloundSound;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.CloudUserRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudDetailBean;
import com.hstypay.enterprise.bean.CloudUserBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class CloudChoiceActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvChoice;
    private TextView mTvStoreName, mTvPhoneNumber, mTvTitle, mButton;
    private RecyclerView mRecyclerView;
    private SafeDialog mLoadDialog;
    private CloudDetailBean.DataBean mCloudDetailBean;
    private List<CloudDetailBean.DataBean.UserListBean> mCloudUserList;
    private List<CloudDetailBean.DataBean.UserListBean> mAdapterUserList;
    private List<CloudUserBean> mCloudUserBean;
    private CloudDetailBean.DataBean.UserListBean mStoreBean;
    private RelativeLayout mRlStore;
    private CloudUserRecyclerAdapter mAdapter;
    private EditTextDelete mEtInput;
    private boolean hasStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_choice);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mRlStore.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(CloudChoiceActivity.this, getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtInput = (EditTextDelete) findViewById(R.id.et_input);
        mRlStore = (RelativeLayout) findViewById(R.id.rl_cloud_store);
        mTvStoreName = (TextView) findViewById(R.id.tv_store_name);
        mTvPhoneNumber = (TextView) findViewById(R.id.tv_phone_number);
        mIvChoice = (ImageView) findViewById(R.id.iv_cloud_choice);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mTvTitle.setText(R.string.title_cloud_choice);
        mButton.setText(R.string.ensure_check);
        mButton.setVisibility(MyApplication.getIsCasher() ? View.GONE : View.VISIBLE);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);
    }

    private void initData() {
        mCloudDetailBean = (CloudDetailBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_CLOUD_DEVICE_DETAIL);
        mCloudUserList = new ArrayList<>();
        mCloudUserList.addAll(mCloudDetailBean.getUserList());
        mCloudUserBean = new ArrayList<>();
        mAdapterUserList = new ArrayList<>();

        if (mCloudDetailBean != null && mCloudUserList != null && mCloudUserList.size() > 0) {
            mStoreBean = new CloudDetailBean.DataBean.UserListBean();
            for (int i = 0; i < mCloudUserList.size(); i++) {
                if (mCloudUserList.get(i).getRole() == 0) {
                    hasStore = true;
                    mStoreBean.setBind(mCloudUserList.get(i).getBind());
                    mStoreBean.setUserId(mCloudUserList.get(i).getUserId());
                    mStoreBean.setName(mCloudUserList.get(i).getName());
                    mStoreBean.setRole(mCloudUserList.get(i).getRole());
                    mStoreBean.setPhone(mCloudUserList.get(i).getPhone());
                    mTvStoreName.setText(mCloudUserList.get(i).getName());
                    mIvChoice.setVisibility(mCloudUserList.get(i).getBind() == 1 ? View.VISIBLE : View.INVISIBLE);
                }
                mRlStore.setVisibility(hasStore ? View.VISIBLE : View.GONE);
            }
            Iterator<CloudDetailBean.DataBean.UserListBean> it = mCloudUserList.iterator();
            while (it.hasNext()) {
                CloudDetailBean.DataBean.UserListBean userListBean = it.next();
                if (userListBean.getRole() == 0) {
                    it.remove();
                }
            }
            CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(MyApplication.getContext());
            mRecyclerView.setLayoutManager(layoutManager);
            for (int i = 0; i < mCloudUserList.size(); i++) {
                if (mCloudUserList.get(i).getRole()==4) {
                    mAdapterUserList.add(0,mCloudUserList.get(i));
                }else {
                    mAdapterUserList.add(mCloudUserList.get(i));
                }
            }
//            mAdapterUserList.addAll(mCloudUserList);
            mAdapter = new CloudUserRecyclerAdapter(MyApplication.getContext(), mAdapterUserList);
            if (!MyApplication.getIsCasher()) {
                mAdapter.setOnItemClickListener(new CloudUserRecyclerAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(String userId) {
                        for (int i = 0; i < mCloudUserList.size(); i++) {
                            if (mCloudUserList.get(i).getUserId().equals(userId)) {
                                if (mCloudUserList.get(i).getBind() == 1) {
                                    mCloudUserList.get(i).setBind(0);
                                } else {
                                    mCloudUserList.get(i).setBind(1);
                                }
                            }
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
            mRecyclerView.setAdapter(mAdapter);
        }

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mAdapterUserList.clear();
                    mAdapterUserList.addAll(getAdapterData(mCloudUserList, mEtInput.getText().toString().trim()));
                    mAdapter.notifyDataSetChanged();
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    mAdapterUserList.clear();
                    mAdapterUserList.addAll(mCloudUserList);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_CASHIER_SET)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudChoiceActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_set_success));
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (mCloudUserList != null) {
                    mCloudUserList.add(0, mStoreBean);
                    mCloudUserBean = setData(mCloudUserList);
                    cloudSet(mCloudDetailBean.getSn(), mCloudUserBean);
                }
                break;
            case R.id.rl_cloud_store:
                if (mStoreBean.getBind() == 1) {
                    mStoreBean.setBind(0);
                    mIvChoice.setVisibility(View.INVISIBLE);
                } else {
                    mStoreBean.setBind(1);
                    mIvChoice.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void cloudSet(String code, List<CloudUserBean> userIdList) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            map.put("merchantId", MyApplication.getMechantId());
            map.put("userIdList", userIdList);
            ServerClient.newInstance(MyApplication.getContext()).cloudSet(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_CASHIER_SET, map);
        }
    }

    private List<CloudUserBean> setData(List<CloudDetailBean.DataBean.UserListBean> userListBean) {
        List<CloudUserBean> list = new ArrayList<>();
        if (userListBean != null && userListBean.size() > 0) {
            for (int i = 0; i < userListBean.size(); i++) {
                CloudUserBean cloudUserBean = new CloudUserBean();
                cloudUserBean.setUserId(userListBean.get(i).getUserId());
                cloudUserBean.setOperateType(userListBean.get(i).getBind());
                list.add(cloudUserBean);
            }
        }
        return list;
    }

    private List<CloudDetailBean.DataBean.UserListBean> getAdapterData(List<CloudDetailBean.DataBean.UserListBean> userListBean, String search) {
        List<CloudDetailBean.DataBean.UserListBean> list = new ArrayList<>();
        if (userListBean != null && userListBean.size() > 0) {
            for (int i = 0; i < userListBean.size(); i++) {
                if ((userListBean.get(i).getPhone() != null && userListBean.get(i).getPhone().contains(search))
                        || (userListBean.get(i).getName() != null && userListBean.get(i).getName().contains(search))) {
                    list.add(userListBean.get(i));
                }
            }
        }
        return list;
    }

    public void getDialogSuccess(String title) {
        showCommonNoticeDialog(CloudChoiceActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent();
                if (mCloudDetailBean != null) {
                    mCloudDetailBean.setUserList(mCloudUserList);
                    intent.putExtra(Constants.RESULT_CLOUD_DEVICE_DETAIL, mCloudDetailBean);
                    setResult(RESULT_OK, intent);
                    CloudChoiceActivity.this.finish();
                }
            }
        });
    }
}
