package com.hstypay.enterprise.activity.cloundSound;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintTicketSetActivity;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintTicketTypeSetActivity;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.bean.CloudDetailBean;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 * 云音响详情及设置页
 */
public class CloudSetActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvSetType;
    private TextView mTvTitle, mTvDeviceType, mTvDeviceNo, mButton;
    private LinearLayout mRlSetChoice;
    private LinearLayout mLlContent;
    private SafeDialog mLoadDialog;
    private String mDeviceSn;
    private String mStoreID;
    private CommonNoticeDialog mDialogSuccess;
    private CloudDetailBean.DataBean mCloudDetailBean;
    private SelectDialog mSelectDialog;
    private TextView mTvCloudDeviceModel;
    private ImageView mIvCloudSetCancelpaySwitch;//支付取消防逃单开关
    private int mOperationSwitchType;//操作类型，1是播报收款类型，2是防逃单提醒,3是API交易播报
    private TextView mTvCloudDeviceStatus;//设备状态
    private RelativeLayout mRlCloudDeviceStatus;//rl设备状态
    private ImageView mIvCloudSetApiSwitch;//api交易播报开关

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_set);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(CloudSetActivity.this, getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvDeviceType = (TextView) findViewById(R.id.tv_cloud_device_type);
        mTvDeviceNo = (TextView) findViewById(R.id.tv_cloud_device_no);
        mIvSetType = (ImageView) findViewById(R.id.iv_cloud_set_type_switch);
        mRlSetChoice = (LinearLayout) findViewById(R.id.rl_cloud_set_choice);
        mLlContent = (LinearLayout) findViewById(R.id.ll_content);
        mTvCloudDeviceModel = findViewById(R.id.tv_cloud_device_model);
        mIvCloudSetCancelpaySwitch = findViewById(R.id.iv_cloud_set_cancelpay_switch);
        mTvCloudDeviceStatus = findViewById(R.id.tv_cloud_device_status);
        mRlCloudDeviceStatus = findViewById(R.id.rl_cloud_device_status);
        mIvCloudSetApiSwitch = findViewById(R.id.iv_cloud_set_api_switch);

        mTvTitle.setText(R.string.title_cloud_set);
        mButton.setText(R.string.btn_unbind);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mRlSetChoice.setOnClickListener(this);
        mIvSetType.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mIvCloudSetCancelpaySwitch.setOnClickListener(this);
        mIvCloudSetApiSwitch.setOnClickListener(this);
    }

    private void initData() {
        mDeviceSn = getIntent().getStringExtra(Constants.INTENT_DEVICE_SN);
        cloudDetail();
    }

    private void cloudDetail() {
        if (!NetworkUtils.isNetworkAvailable(CloudSetActivity.this)) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", MyApplication.getMechantId());
            if (!TextUtils.isEmpty(mDeviceSn)) {
                map.put("sn", mDeviceSn);
            }
            ServerClient.newInstance(CloudSetActivity.this).cloudDetail(CloudSetActivity.this, Constants.TAG_CLOUD_DEVICE_DETAIL, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CloudDetailBean msg = (CloudDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mCloudDetailBean = msg.getData();
                    if (msg.getData() != null) {
                        mLlContent.setVisibility(View.VISIBLE);
                        mTvDeviceNo.setText(msg.getData().getSn());
                        mTvDeviceType.setText(msg.getData().getCategoryName());
                        mTvCloudDeviceModel.setText(msg.getData().getDeviceModel());
                        String iotShowFlag = msg.getData().getIotShowFlag();
                        if ("1".equals(iotShowFlag)){
                            mRlCloudDeviceStatus.setVisibility(View.VISIBLE);
                            mTvCloudDeviceStatus.setText(msg.getData().getIotDeviceStatusDesc());
                        }else {
                            mRlCloudDeviceStatus.setVisibility(View.GONE);
                        }
                        if ("1".equals(msg.getData().getPayTypeBroadcast())) {
                            mIvSetType.setImageResource(R.mipmap.ic_switch_open);
                        } else {
                            mIvSetType.setImageResource(R.mipmap.ic_switch_close);
                        }
                        if ("1".equals(msg.getData().getConfigType())){
                            mIvCloudSetCancelpaySwitch.setImageResource(R.mipmap.ic_switch_open);
                        }else {
                            mIvCloudSetCancelpaySwitch.setImageResource(R.mipmap.ic_switch_close);
                        }
                        if ("1".equals(msg.getData().getApiTradeBroadcast())){
                            mIvCloudSetApiSwitch.setImageResource(R.mipmap.ic_switch_open);
                        }else {
                            mIvCloudSetApiSwitch.setImageResource(R.mipmap.ic_switch_close);
                        }
                    }else {
                        MyToast.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_UNBIND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_notice_unbind_success));
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_SET)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CloudSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (mOperationSwitchType ==1){
                        if ("1".equals(mCloudDetailBean.getPayTypeBroadcast())) {
                            //播报收款的开关关闭
                            mCloudDetailBean.setPayTypeBroadcast("0");
                            mIvSetType.setImageResource(R.mipmap.ic_switch_close);
                        } else {
                            mCloudDetailBean.setPayTypeBroadcast("1");
                            mIvSetType.setImageResource(R.mipmap.ic_switch_open);
                        }
                    }else if (mOperationSwitchType == 2){
                        if ("1".equals(mCloudDetailBean.getConfigType())) {
                            //防逃单提醒的开关关闭
                            mCloudDetailBean.setConfigType("0");
                            mIvCloudSetCancelpaySwitch.setImageResource(R.mipmap.ic_switch_close);
                        } else {
                            mCloudDetailBean.setConfigType("1");
                            mIvCloudSetCancelpaySwitch.setImageResource(R.mipmap.ic_switch_open);
                        }
                    }else if (mOperationSwitchType ==3){
                        if ("1".equals(mCloudDetailBean.getApiTradeBroadcast())) {
                            //播报api交易的开关关闭
                            mCloudDetailBean.setApiTradeBroadcast("0");
                            mIvCloudSetApiSwitch.setImageResource(R.mipmap.ic_switch_close);
                        } else {
                            mCloudDetailBean.setApiTradeBroadcast("1");
                            mIvCloudSetApiSwitch.setImageResource(R.mipmap.ic_switch_open);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (mSelectDialog == null) {
                    mSelectDialog = new SelectDialog(CloudSetActivity.this, getString(R.string.dialog_cloud_unbind_content), R.layout.select_common_dialog);
                    mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                        @Override
                        public void clickOk() {
                            unbindCloudDevice(mDeviceSn);
                        }
                    });
                }
                mSelectDialog.show();
                break;
            case R.id.iv_cloud_set_type_switch://收款类型云语音播报设置
                cloudSet(mDeviceSn,1);
                break;
            case R.id.iv_cloud_set_cancelpay_switch://防逃单云语音播报设置
                cloudSet(mDeviceSn,2);
                break;
            case R.id.iv_cloud_set_api_switch://API交易云语音播报设置
                cloudSet(mDeviceSn,3);
                break;
            case R.id.rl_cloud_set_choice:
                //关联设置
                Intent intent = new Intent(CloudSetActivity.this, CloudPrintTicketTypeSetActivity.class);
                /*CloudPrintDetailBean.DataBean bean = new CloudPrintDetailBean.DataBean();
                bean.setId(""+mCloudDetailBean.getId());
                bean.setStoreMerchantId(mCloudDetailBean.getStoreMerchantId());*/
                CloudPrintDetailBean.DataBean bean = new Gson().fromJson(new Gson().toJson(mCloudDetailBean),CloudPrintDetailBean.DataBean.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CLOUD_VOICE_SET);
                intent.putExtra(Constants.RESULT_CLOUD_PRINT_DEVICE_DETAIL, bean);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CLOUD_DEVICE_DETAIL) {
            CloudDetailBean.DataBean dataBean = (CloudDetailBean.DataBean) data.getSerializableExtra(Constants.RESULT_CLOUD_DEVICE_DETAIL);
            mCloudDetailBean = dataBean;
        }*/
    }

    private void unbindCloudDevice(String code) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", code);
            if (!TextUtils.isEmpty(mCloudDetailBean.getStoreMerchantId())) {
                map.put("storeMerchantId", mCloudDetailBean.getStoreMerchantId());
            }
            map.put("operateType", 2);
            map.put("merchatId", MyApplication.getMechantId());
            ServerClient.newInstance(MyApplication.getContext()).cloudBind(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_UNBIND, map);
        }
    }

    /**
     * @param sn 设备sn码
     * @param operationType 操作类型，1是播报收款类型，2是防逃单提醒,3是API交易播报
     */
    private void cloudSet(String sn,int operationType) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", sn);
            map.put("merchantId", MyApplication.getMechantId());
            if (1==operationType){
                //播报收款类型设置
                if ("1".equals(mCloudDetailBean.getPayTypeBroadcast())) {
                    //是开启则关闭
                    map.put("payTypeBroadcast", "0");
                } else {
                    //是关闭则开启
                    map.put("payTypeBroadcast", "1");
                }
                map.put("configType",mCloudDetailBean.getConfigType());
                map.put("apiTradeBroadcast", mCloudDetailBean.getApiTradeBroadcast());
            }else if (2==operationType){
                //防逃单提醒
                if ("1".equals(mCloudDetailBean.getConfigType())) {
                    //是开启则关闭
                    map.put("configType", "0");
                } else {
                    //是关闭则开启
                    map.put("configType", "1");
                }
                map.put("payTypeBroadcast",mCloudDetailBean.getPayTypeBroadcast());
                map.put("apiTradeBroadcast", mCloudDetailBean.getApiTradeBroadcast());
            }else if (3 == operationType){
                //API交易播报
                if ("1".equals(mCloudDetailBean.getApiTradeBroadcast())) {
                    //是开启则关闭
                    map.put("apiTradeBroadcast", "0");
                } else {
                    //是关闭则开启
                    map.put("apiTradeBroadcast", "1");
                }
                map.put("configType",mCloudDetailBean.getConfigType());
                map.put("payTypeBroadcast",mCloudDetailBean.getPayTypeBroadcast());
            }
            mOperationSwitchType = operationType;
            //ServerClient.newInstance(MyApplication.getContext()).updatePrinterBysn(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_SET, map);
            ServerClient.newInstance(MyApplication.getContext()).cloudSet(MyApplication.getContext(), Constants.TAG_CLOUD_DEVICE_SET, map);
        }
    }

    public void getDialogSuccess(String title) {
        mDialogSuccess = new CommonNoticeDialog(CloudSetActivity.this, title, getString(R.string.dialog_notice_button));
        mDialogSuccess.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                startActivity(new Intent(CloudSetActivity.this, DeviceListActivity.class));
                CloudSetActivity.this.finish();
            }
        });
        DialogHelper.resize(CloudSetActivity.this, mDialogSuccess);
        mDialogSuccess.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDialogSuccess != null) {
            mDialogSuccess.dismiss();
        }
        if (mSelectDialog != null) {
            mSelectDialog.dismiss();
        }
    }
}
