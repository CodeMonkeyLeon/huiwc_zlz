package com.hstypay.enterprise.activity.coupon;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoBean;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoData;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


public class CouponDetailActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvCouponPic;
    private TextView mTvTitle, mTvCouponCode, mTvCouponStatus, mTvCouponTitle, mTvError;
    private LinearLayout mLlEmpty, mLlBtn;
    private RelativeLayout mRlContent;
    private Button mBtnSubmit;
    private SafeDialog mLoadDialog, mLoadCheckDialog;
    private String mStoreId;
    private CouponHsInfoData mCouponInfoData;
    private String mError;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            DialogUtil.safeCloseDialog(mLoadCheckDialog);
            queryCoupon();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mLoadCheckDialog = getLoadDialog(this, UIUtils.getString(R.string.public_checking), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(getString(R.string.title_coupon_detail));

        mTvCouponCode = findViewById(R.id.tv_coupon_code);
        mTvCouponStatus = findViewById(R.id.tv_coupon_status);
        mIvCouponPic = findViewById(R.id.iv_coupon_pic);
        mTvCouponTitle = findViewById(R.id.tv_coupon_title);
        mTvError = findViewById(R.id.tv_error);
        mLlEmpty = findViewById(R.id.ll_empty);
        mRlContent = findViewById(R.id.rl_content);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mLlBtn = findViewById(R.id.ll_btn);
        setButtonEnable(mBtnSubmit, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    public void initData() {
        mCouponInfoData = (CouponHsInfoData) getIntent().getSerializableExtra("INTENT_COUPON_DETAIL");
        mError = getIntent().getStringExtra("INTENT_COUPON_DETAIL_ERROR");
        mStoreId = getIntent().getStringExtra(Constants.INTENT_STORE_ID);
        if (mCouponInfoData != null) {
            if (mCouponInfoData.getActivityName() == null && mCouponInfoData.getState() == null) {
                mRlContent.setVisibility(View.GONE);
                mLlEmpty.setVisibility(View.VISIBLE);
                mTvError.setText("返回数据异常");
            } else {
                mRlContent.setVisibility(View.VISIBLE);
                mLlEmpty.setVisibility(View.GONE);
                mTvCouponCode.setText("券号：" + mCouponInfoData.getCouponId());
                Picasso.get()
                        .load(mCouponInfoData.getActivityPicUrl())
                        .placeholder(R.mipmap.icon_coupon_general)
                        .error(R.mipmap.icon_coupon_general)
                        .into(mIvCouponPic);
                mTvCouponTitle.setText(mCouponInfoData.getActivityName());
                if (mCouponInfoData.getState() != null)
                    switch (mCouponInfoData.getState()) {
                        case "1":
                            mTvCouponStatus.setText("待使用");
                            mLlBtn.setVisibility(View.VISIBLE);
                            break;
                        case "2":
                            mTvCouponStatus.setText("已冻结");
                            mLlBtn.setVisibility(View.GONE);
                            break;
                        case "3":
                            mTvCouponStatus.setText("已过期");
                            mLlBtn.setVisibility(View.GONE);
                            break;
                        case "4":
                            mTvCouponStatus.setText("已使用");
                            mLlBtn.setVisibility(View.GONE);
                            break;
                        case "5":
                            mTvCouponStatus.setText("已退款");
                            mLlBtn.setVisibility(View.GONE);
                            break;
                    }
            }
        } else {
            mRlContent.setVisibility(View.GONE);
            mLlEmpty.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(mError))
                mTvError.setText(mError);
        }

    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_QUERY_COUPON_VERIFY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponHsInfoBean msg = (CouponHsInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(CouponDetailActivity.this, msg.getError().getMessage());
                            }
                        } else if ("internal error".equals(msg.getError().getCode())) {
                            DialogUtil.safeShowDialog(mLoadCheckDialog);
                            handler.sendEmptyMessageDelayed(1, 3000);
                        } else {
                            if (msg.getError().getMessage() != null) {
                                showCommonNoticeDialog(CouponDetailActivity.this, msg.getError().getMessage());
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        if (mCouponInfoData != null)
                            msg.getData().setActivityPicUrl(mCouponInfoData.getActivityPicUrl());
                        startActivity(new Intent(this, CouponVerifyDetailActivity.class)
                                .putExtra("INTENT_COUPON_VERIFY_DETAIL", msg.getData())
                                .putExtra(Constants.INTENT_NAME, "INTENT_COUPON_VERIFY_DETAIL"));
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.error_data));
                    }
                    break;
            }
        }
        if (event.getTag().equals("TAG_QUERY_COUPON_STATUS")) {
            DialogUtil.safeCloseDialog(mLoadCheckDialog);
            CouponHsInfoBean msg = (CouponHsInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                showCommonNoticeDialog(CouponDetailActivity.this, msg.getError().getMessage());
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        switch (msg.getData().getState()) {
                            case "1":
                                mTvCouponStatus.setText("待使用");
                                mLlBtn.setVisibility(View.VISIBLE);
                                showCommonNoticeDialog(this, "请重新核销");
                                break;
                            case "2":
                                mTvCouponStatus.setText("已冻结");
                                mLlBtn.setVisibility(View.GONE);
                                break;
                            case "3":
                                mTvCouponStatus.setText("已过期");
                                mLlBtn.setVisibility(View.GONE);
                                break;
                            case "4":
                                mTvCouponStatus.setText("已使用");
                                mLlBtn.setVisibility(View.GONE);
                                showCommonNoticeDialog(this, "核销成功", new CommonNoticeDialog.OnClickOkListener() {
                                    @Override
                                    public void onClickOk() {
                                        startActivity(new Intent(CouponDetailActivity.this, CouponVerifyListActivity.class));
                                    }
                                });
                                break;
                            case "5":
                                mTvCouponStatus.setText("已退款");
                                mLlBtn.setVisibility(View.GONE);
                                break;
                        }
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.error_data));
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                couponUse(mCouponInfoData.getCouponId());
                break;
        }
    }

    private void couponUse(String couponCode) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(CouponDetailActivity.this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("shopId", mStoreId);
            map.put("couponId", couponCode);
            ServerClient.newInstance(MyApplication.getContext()).couponUse(MyApplication.getContext(), Constants.TAG_QUERY_COUPON_VERIFY, map);
        }
    }

    /**
     * 查券详情
     */
    private void queryCoupon() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(CouponDetailActivity.this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadCheckDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("shopId", mStoreId);
            map.put("couponId", mCouponInfoData.getCouponId());
            ServerClient.newInstance(MyApplication.getContext()).queryCoupon(MyApplication.getContext(), "TAG_QUERY_COUPON_STATUS", map);
        }
    }
}
