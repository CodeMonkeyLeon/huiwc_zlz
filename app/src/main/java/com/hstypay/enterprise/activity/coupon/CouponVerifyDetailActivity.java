package com.hstypay.enterprise.activity.coupon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoData;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.squareup.picasso.Picasso;


public class CouponVerifyDetailActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvCouponPic;
    private TextView mTvTitle, mTvCouponTitle,mTvCouponOrderStatus, mTvCouponOrderNo, mTvCouponUsePhone,mTvCouponVerifyTime,mTvCouponVerifyStore;
    private String mIntentName;
    private CouponHsInfoData mCouponInfoData;
    private LinearLayout mLlCouponCode,mLlCouponOrderNo;
    private TextView mTvCouponCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_verify_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mIvCouponPic = findViewById(R.id.iv_coupon_pic);
        mTvCouponTitle = findViewById(R.id.tv_coupon_title);
        mTvCouponOrderStatus = findViewById(R.id.tv_coupon_order_status);
        mTvCouponOrderNo = findViewById(R.id.tv_coupon_order_no);
        mTvCouponUsePhone = findViewById(R.id.tv_coupon_use_phone);
        mTvCouponVerifyTime = findViewById(R.id.tv_coupon_verify_time);
        mTvCouponVerifyStore = findViewById(R.id.tv_coupon_verify_store);
        mLlCouponOrderNo = findViewById(R.id.ll_coupon_order_no);
        mLlCouponCode = findViewById(R.id.ll_coupon_code);
        mTvCouponCode = findViewById(R.id.tv_coupon_code);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    public void initData() {
        mCouponInfoData = (CouponHsInfoData) getIntent().getSerializableExtra("INTENT_COUPON_VERIFY_DETAIL");
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if ("INTENT_COUPON_VERIFY_DETAIL".equals(mIntentName)) {
            mTvTitle.setText(getString(R.string.tv_vip_verification_success));
        } else {
            mTvTitle.setText(getString(R.string.title_coupon_verify_detail));
        }
        if (mCouponInfoData != null) {
            Picasso.get()
                    .load(mCouponInfoData.getActivityPicUrl())
                    .placeholder(R.mipmap.icon_coupon_general)
                    .error(R.mipmap.icon_coupon_general)
                    .into(mIvCouponPic);
            mTvCouponTitle.setText(mCouponInfoData.getActivityName());
            mTvCouponOrderStatus.setText("核销成功");
            mTvCouponUsePhone.setText(mCouponInfoData.getUserPhone());
            mTvCouponVerifyTime.setText(DateUtil.formatTimeUtil(mCouponInfoData.getConsumeTime()));
            mTvCouponVerifyStore.setText(mCouponInfoData.getShopName());
            if (TextUtils.isEmpty(mCouponInfoData.getCouponId())){
                mLlCouponCode.setVisibility(View.GONE);
            } else {
                mTvCouponCode.setText(mCouponInfoData.getCouponId());
            }
            if (TextUtils.isEmpty(mCouponInfoData.getListId())){
                mLlCouponOrderNo.setVisibility(View.GONE);
            } else {
                mTvCouponOrderNo.setText(mCouponInfoData.getListId());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                if ("INTENT_COUPON_VERIFY_DETAIL".equals(mIntentName)) {
                    startActivity(new Intent(this, MainActivity.class));
                }
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if ("INTENT_COUPON_VERIFY_DETAIL".equals(mIntentName)) {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
    }
}
