package com.hstypay.enterprise.activity.coupon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.adapter.CouponVerifyAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoData;
import com.hstypay.enterprise.bean.coupon.CouponHsListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CouponVerifyListActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private TextView tv_title, mTvNull;
    private RecyclerView mRecyclerView;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private SafeDialog mLoadDialog;
    private List<CouponHsInfoData> mList;
    private CouponVerifyAdapter mAdapter;
    private RelativeLayout mRlBuyCoupon, mRlHotGoodCoupon;
    private View mViewBuyCoupon, mViewHotGoodCoupon;
    private LinearLayout mLlTab;
    private int type = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_list);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        mRlBuyCoupon = findViewById(R.id.rl_buy_coupon);
        mViewBuyCoupon = findViewById(R.id.view_buy_coupon);
        mRlHotGoodCoupon = findViewById(R.id.rl_hot_good_coupon);
        mViewHotGoodCoupon = findViewById(R.id.view_hot_good_coupon);
        mLlTab = findViewById(R.id.ll_tab);
        tv_title.setText(getString(R.string.title_verify_coupon));

        mTvNull = findViewById(R.id.tv_not_data);
        initRecyclerView();
        initSwipeRefreshLayout();

        if (MyApplication.isOpenMember() && MyApplication.isOpenFuncard()) {
            type = 2;
            mViewHotGoodCoupon.setVisibility(View.GONE);
        } else if (MyApplication.isOpenMember()) {
            type = 2;
            mLlTab.setVisibility(View.GONE);
        } else if (MyApplication.isOpenFuncard()) {
            type = 1;
            mLlTab.setVisibility(View.GONE);
        } else {
            mTvNull.setVisibility(View.VISIBLE);
            mLlTab.setVisibility(View.GONE);
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getReceiptList("15", "1", false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getReceiptList(pageSize + "", currentPage + "", false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        mRlBuyCoupon.setOnClickListener(this);
        mRlHotGoodCoupon.setOnClickListener(this);
    }

    private void initData() {
        if (!MyApplication.isOpenMember() && !MyApplication.isOpenFuncard())
            return;
        mList = new ArrayList<>();
        mAdapter = new CouponVerifyAdapter(MyApplication.getContext(), mList);
        mAdapter.setOnItemClickListener(new CouponVerifyAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                CouponHsInfoData dataEntity = mList.get(position);
                if (dataEntity != null) {
                    Intent intent = new Intent(CouponVerifyListActivity.this, CouponVerifyDetailActivity.class);
                    intent.putExtra("INTENT_COUPON_VERIFY_DETAIL", dataEntity);
                    startActivity(intent);
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        getReceiptList("15", "1", true);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.rl_buy_coupon:
                if (type != 2) {
                    type = 2;
                    mViewHotGoodCoupon.setVisibility(View.GONE);
                    mViewBuyCoupon.setVisibility(View.VISIBLE);
                    isRefreshed = true;
                    getReceiptList("15", "1", true);
                }
                break;
            case R.id.rl_hot_good_coupon:
                if (type != 1) {
                    type = 1;
                    mViewHotGoodCoupon.setVisibility(View.VISIBLE);
                    mViewBuyCoupon.setVisibility(View.GONE);
                    isRefreshed = true;
                    getReceiptList("15", "1", true);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void getReceiptList(String pageSize, String currentPage, boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("limit", pageSize);
            map.put("offset", currentPage);
            map.put("type", type);
            ServerClient.newInstance(MyApplication.getContext()).queryCouponList(MyApplication.getContext(), Constants.TAG_QUERY_COUPON_VERIFY_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_QUERY_COUPON_VERIFY_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponHsListBean msg = (CouponHsListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CouponVerifyListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mAdapter.notifyDataSetChanged();
                        if (isLoadmore) {
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            currentPage = 2;
            getReceiptList("15", "1", true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }
}
