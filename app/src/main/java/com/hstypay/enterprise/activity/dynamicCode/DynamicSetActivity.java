package com.hstypay.enterprise.activity.dynamicCode;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LinkEmployeeBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class DynamicSetActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mButton, mTvTitle, mTvDeviceType,mTvDeviceModel, mTvDeviceNo, mTvDeviceStore, mTvDeviceCashier;
    private RelativeLayout mRlSetChoice,mRlStore;
    private LinearLayout mLlContent,mLlCashier;
    private SafeDialog mLoadDialog;

    private SelectDialog mSelectDialog;
    private CommonNoticeDialog mDialogSuccess;
    private String mDeviceSn,mUserId;
    private CloudDetailBean.DataBean mDeviceInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_set);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(DynamicSetActivity.this, getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mTvDeviceType = findViewById(R.id.tv_device_type);
        mTvDeviceModel = findViewById(R.id.tv_device_model);
        mTvDeviceNo = findViewById(R.id.tv_device_no);
        mTvDeviceStore = findViewById(R.id.tv_device_store);
        mTvDeviceCashier = findViewById(R.id.tv_device_cashier);
        mRlSetChoice = findViewById(R.id.rl_device_set_choice);
        mLlContent = findViewById(R.id.ll_content);
        mLlCashier = findViewById(R.id.ll_cashier);
        mRlStore =  findViewById(R.id.rl_store);

        mTvTitle.setText(R.string.string_title_setting);
        mButton.setText(R.string.btn_unbind);
        if (MyApplication.getIsMerchant()) {
            mButton.setVisibility(View.VISIBLE);
        } else {
            mButton.setVisibility(View.INVISIBLE);
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mRlSetChoice.setOnClickListener(this);
        mButton.setOnClickListener(this);
    }

    private void initData() {
        mDeviceSn = getIntent().getStringExtra(Constants.INTENT_DEVICE_SN);
        mUserId = "";
        getDetail();
    }

    private void getDetail() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", MyApplication.getMechantId());
            if (!TextUtils.isEmpty(mDeviceSn)) {
                map.put("sn", mDeviceSn);
            }
            ServerClient.newInstance(DynamicSetActivity.this).cloudDetail(DynamicSetActivity.this, Constants.TAG_DYNAMIC_DEVICE_DETAIL, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_DYNAMIC_DEVICE_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CloudDetailBean msg = (CloudDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DynamicSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mDeviceInfo = msg.getData();
                    if (mDeviceInfo != null) {
                        mLlContent.setVisibility(View.VISIBLE);
                        if (!TextUtils.isEmpty(mDeviceInfo.getStoreMerchantId())) {
                            if ("HPAY_POS".equals(mDeviceInfo.getCategoryCode())){
                                mLlCashier.setVisibility(View.GONE);
                            } else {
                                mLlCashier.setVisibility(View.VISIBLE);
                            }
                            mRlStore.setVisibility(View.VISIBLE);
                        } else {
                            mRlStore.setVisibility(View.GONE);
                            mLlCashier.setVisibility(View.GONE);
                        }
                        mTvDeviceType.setText(mDeviceInfo.getCategoryName());
                        mTvDeviceModel.setText(mDeviceInfo.getDeviceModel());
                        mTvDeviceNo.setText(mDeviceInfo.getSn());
                        mTvDeviceStore.setText(mDeviceInfo.getStoreMerchantIdCnt());
                        mTvDeviceCashier.setText(R.string.tv_not_selected);
                        if (mDeviceInfo.getUserList() != null && mDeviceInfo.getUserList().size()>0){
                            for (CloudDetailBean.DataBean.UserListBean userListBean : mDeviceInfo.getUserList()) {
                                if (userListBean.getBind() == 1) {
                                    mTvDeviceCashier.setText(userListBean.getName());
                                    mUserId = userListBean.getUserId();
                                    break;
                                }
                            }
                        }
                    } else {
                        mLlContent.setVisibility(View.GONE);
                        MyToast.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_LINK_EMPLOYEE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LinkEmployeeBean msg = (LinkEmployeeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DynamicSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<LinkEmployeeBean.DataEntity> data = msg.getData();
                    if (data != null && data.size() > 0) {
                        Intent intent = new Intent(DynamicSetActivity.this, DynamicChoiceActivity.class);
                        intent.putExtra(Constants.INTENT_DEVICE_EMPLOYEE_LIST, (Serializable) data);
                        intent.putExtra(Constants.INTENT_DEVICE_SN, mDeviceInfo.getSn());
                        intent.putExtra(Constants.INTENT_USER_ID, mUserId);
                        startActivityForResult(intent, Constants.REQUEST_DEVICE_EMPLOYEE_LIST);
                    } else {
                        MyToast.showToastShort(getString(R.string.tv_moment_null));
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_DYNAMIC_SET_DEVICE_UNBIND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DynamicSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialogSuccess(getString(R.string.dialog_notice_unbind_success));
                    break;
            }
        }
    }


    public void getDialogSuccess(String title) {
        mDialogSuccess = new CommonNoticeDialog(DynamicSetActivity.this, title, getString(R.string.dialog_notice_button));
        mDialogSuccess.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
//                Constants.DEVICE_LIST_REFRESH_TAG=true;
                startActivity(new Intent(DynamicSetActivity.this, DeviceListActivity.class));
                DynamicSetActivity.this.finish();
            }
        });
        DialogHelper.resize(DynamicSetActivity.this, mDialogSuccess);
        mDialogSuccess.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_back:
                /*Intent intent = new Intent();
                intent.putExtra(Constants.RESULT_DEVICE_EMPLOYEE_LIST, mDeviceInfo);
                setResult(RESULT_OK, intent);*/
                finish();
                break;
            case R.id.rl_device_set_choice:
                linkEmployeeList(mDeviceInfo.getStoreMerchantId());
                break;
            case R.id.button_title:
                if (mSelectDialog == null) {
                    mSelectDialog = new SelectDialog(DynamicSetActivity.this, getString(R.string.dialog_receive_unbind_content), R.layout.select_common_dialog);
                    mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                        @Override
                        public void clickOk() {
                            mSelectDialog.dismiss();
                            unBindDevice();
                        }
                    });
                }
                mSelectDialog.show();
                break;
        }
    }

    private void unBindDevice() {

        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();

            if (mDeviceInfo != null) {
                if (!TextUtils.isEmpty(mDeviceInfo.getSn())) {
                    map.put("sn", mDeviceInfo.getSn());
                }
                if (!TextUtils.isEmpty(mDeviceInfo.getStoreMerchantId())) {
                    map.put("storeMerchantId", mDeviceInfo.getStoreMerchantId());
                }
            }
            map.put("operateType", 2);
            map.put("merchatId", MyApplication.getMechantId());
            map.put("purposeCode", "3");
            ServerClient.newInstance(MyApplication.getContext()).cloudBind(MyApplication.getContext(), Constants.TAG_DYNAMIC_SET_DEVICE_UNBIND, map);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_DEVICE_EMPLOYEE_LIST) {
            LinkEmployeeBean.DataEntity dataBean = (LinkEmployeeBean.DataEntity) data.getSerializableExtra(Constants.RESULT_DEVICE_EMPLOYEE_LIST);
            mUserId = dataBean.getUserId();
            mTvDeviceCashier.setText(dataBean.getName());
        }
    }

    private void linkEmployeeList(String storeId) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeMerchantId", storeId);
            ServerClient.newInstance(MyApplication.getContext()).linkEmployeeList(MyApplication.getContext(), Constants.TAG_LINK_EMPLOYEE_LIST, map);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mDialogSuccess != null) {
            mDialogSuccess.dismiss();
        }
        if (mSelectDialog != null) {
            mSelectDialog.dismiss();
        }
    }

}
