package com.hstypay.enterprise.activity.eleticket;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.ImageViewState;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author zeyu.kuang
 * @time 2020/12/26
 * @desc 绑定ukey盒子的服务详情和使用教程
 */
public class BindUkeyIntroduceFragment extends BaseFragment implements View.OnClickListener {

    private boolean isAddBtnView;
    private SubsamplingScaleImageView mImageViewServiceDetail;//加载长图iv
    private String mPicFileName = "";

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        View rootView = inflater.inflate(R.layout.fragment_bind_ukey_detail, container, false);
        initData(rootView);
        return rootView;
    }

    private void initData(View rootView) {
        LinearLayout ll_bind_ukey_boardcode = rootView.findViewById(R.id.ll_bind_ukey_boardcode);
        mImageViewServiceDetail = rootView.findViewById(R.id.imageView_sub);
        View btnView = View.inflate(getActivity(), R.layout.item_bind_ukey_device_btn, null);
        View btn_check_pass_app = btnView.findViewById(R.id.btn_check_pass_app);
        btn_check_pass_app.setOnClickListener(this);
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    Bundle bundle = getArguments();
                    int pageType = (int) bundle.get("pageType");
                    mImageViewServiceDetail.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CENTER_CROP);
                    if (pageType == BindUkeyTicketDeviceActivity.SERVICEDETAILPAGE) {
                        mPicFileName = "ukey_ticket_introduce.jpg";
                    } else if (pageType == BindUkeyTicketDeviceActivity.USEGUIDEPAGE) {
                        mPicFileName = "ukey_ticket_use_method.jpg";
                    }
                    String assetsCacheFile = getAssetsCacheFile(getActivity(), mPicFileName);
                    File resource = new File(assetsCacheFile);
                    if (resource.exists()) {
                        LogUtil.d("tagtag", "resource.exists()");
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 2;
                        Bitmap mBitmapIntroduce = BitmapFactory.decodeFile(assetsCacheFile, options);
                        float mFitWidthScaleIntroduce = getFitWidthScale(mBitmapIntroduce);
                        mImageViewServiceDetail.setMinScale(mFitWidthScaleIntroduce);//最小显示比例
                        mImageViewServiceDetail.setMaxScale(mFitWidthScaleIntroduce);//最大显示比例
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mImageViewServiceDetail.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                    @Override
                                    public void onGlobalLayout() {
                                        mImageViewServiceDetail.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                        if (!isAddBtnView) {
                                            isAddBtnView = true;
                                            ll_bind_ukey_boardcode.addView(btnView);
                                        }
                                    }
                                });
                                //mImageView.setImage(ImageSource.uri(Uri.fromFile(resource)), new ImageViewState(getFitWidthScale(resource), new PointF(0, 0), 0));
                                mImageViewServiceDetail.setImage((ImageSource.bitmap(mBitmapIntroduce)), new ImageViewState(mFitWidthScaleIntroduce, new PointF(0, 0), 0));
                            }
                        });
                    } else {
                        LogUtil.d("tagtag", "!!!resource.exists()");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.d("tagtag", "" + e.getMessage());
                }
            }
        }.start();
    }

    public String getAssetsCacheFile(Context context, String fileName) {
        File cacheFile = new File(context.getCacheDir(), fileName);
        if (cacheFile.exists()) {
            return cacheFile.getAbsolutePath();
        }
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            try {
                FileOutputStream outputStream = new FileOutputStream(cacheFile);
                try {
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = inputStream.read(buf)) > 0) {
                        outputStream.write(buf, 0, len);
                    }
                } finally {
                    outputStream.close();
                }
            } finally {
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cacheFile.getAbsolutePath();
    }

    // 计算缩放比例m1
    private float getFitWidthScale(File file) {
        WindowManager wm = getActivity().getWindowManager();
        float windowWidth = wm.getDefaultDisplay().getWidth();
        return windowWidth / getImageWidth(file);
    }

    // 计算缩放比例m2
    private float getFitWidthScale(Bitmap bitmap) {
        WindowManager wm = getActivity().getWindowManager();
        float windowWidth = wm.getDefaultDisplay().getWidth();
        return windowWidth / bitmap.getWidth();
    }

    private float getImageWidth(File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        return options.outWidth;
    }

    @Override
    public void onDestroy() {
        if (mImageViewServiceDetail != null) {
            mImageViewServiceDetail.recycle();
        }
        String assetsCacheFile = getAssetsCacheFile(getActivity(), mPicFileName);
        File resource = new File(assetsCacheFile);
        if (resource.exists()){
            resource.delete();
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_check_pass_app:
                Intent intent = new Intent(getActivity(), CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BIND_UKEY_DEVICE);
                startActivity(intent);
                getActivity().finish();
                break;
        }
    }
}
