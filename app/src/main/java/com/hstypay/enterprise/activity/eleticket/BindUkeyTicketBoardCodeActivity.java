package com.hstypay.enterprise.activity.eleticket;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/12/24
 * @desc 发票管理和绑定开票码牌
 */
public class BindUkeyTicketBoardCodeActivity extends BaseActivity implements View.OnClickListener {

    private Button mBtnTicketManager;//发票管理
    private Button mBtnBindTicketCode;//绑定牌码
    private SafeDialog mLoadDialog;
    private String store_Id = "";//门店编号

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_ukey_ticket_boardcode);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        findViewById(R.id.iv_back).setOnClickListener(this);
        mBtnTicketManager.setOnClickListener(this);
        mBtnBindTicketCode.setOnClickListener(this);
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
    }

    private void initView() {
        TextView mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_page_ticket);
        mBtnTicketManager = findViewById(R.id.btn_ticket_manager);
        mBtnBindTicketCode = findViewById(R.id.btn_bind_ticket_code);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_ticket_manager:
                //发票管理
                getManagerIndexUrl();
                break;
            case R.id.btn_bind_ticket_code:
                //扫码绑定开票码牌
                //先选择门店
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_ID, store_Id);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
        }
    }

    //获取发票管理后台H5首页url
    private void getManagerIndexUrl() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            ServerClient.newInstance(MyApplication.getContext()).getManagerIndexUrlEleTicket(MyApplication.getContext(), Constants.TAG_GET_MANAGER_INDEXURL_ELETICKET, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetManagerIndexUrlEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MANAGER_INDEXURL_ELETICKET)) {
            //获取发票管理后台H5首页url
            DialogUtil.safeCloseDialog(mLoadDialog);
            BaseBean<String> msg = (BaseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(BindUkeyTicketBoardCodeActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            showCommonNoticeDialog(BindUkeyTicketBoardCodeActivity.this, msg.getError().getMessage());
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        Intent intent = new Intent(this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, msg.getData());
                        intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                        startActivity(intent);
                    }
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            String storeName = shopBean.getStoreName();
            store_Id = shopBean.getStoreId();
            Intent intent = new Intent(BindUkeyTicketBoardCodeActivity.this, CaptureActivity.class);
            intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BIND_UKEY_BOARDCODE);
            intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
            intent.putExtra(Constants.INTENT_BIND_STORE_NAME, storeName);
            startActivity(intent);
        }
    }
}
