package com.hstypay.enterprise.activity.eleticket;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.utils.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/12/24
 * @desc  去绑定ukey盒子的服务详情和使用教程
 */
public class BindUkeyTicketDeviceActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    public static final int SERVICEDETAILPAGE = 1;//服务详情
    public static final int USEGUIDEPAGE = 2;//使用教程
    private RadioGroup mRgCheckTicketBoardcode;
    private View mViewLineServiceDetail;
    private View mViewLineIntroduce;
    private int mLastTabIndex = -1;
    private List<BaseFragment> mFragments;
    private FragmentManager mSupportFragmentManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_ukey_ticket_device);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        findViewById(R.id.iv_back).setOnClickListener(this);
        mRgCheckTicketBoardcode.setOnCheckedChangeListener(this);
    }

    private void initData() {
        mSupportFragmentManager = getSupportFragmentManager();
        mFragments = new ArrayList<>();
        BindUkeyIntroduceFragment bindUkeyServiceDetailFragment = new BindUkeyIntroduceFragment();
        Bundle bundleS = new Bundle();
        bundleS.putInt("pageType",SERVICEDETAILPAGE);
        bindUkeyServiceDetailFragment.setArguments(bundleS);
        BindUkeyIntroduceFragment bindUkeyUseGuideFragment = new BindUkeyIntroduceFragment();
        Bundle bundleU = new Bundle();
        bundleU.putInt("pageType",USEGUIDEPAGE);
        bindUkeyUseGuideFragment.setArguments(bundleU);
        mFragments.add(bindUkeyServiceDetailFragment);
        mFragments.add(bindUkeyUseGuideFragment);
        selectFragment(0);
    }

    private void selectFragment(int index) {
        if (mLastTabIndex == index){
            return;
        }
        FragmentTransaction fragmentTransaction = mSupportFragmentManager.beginTransaction();
        if (mLastTabIndex!=-1){
            BaseFragment lastFragment = mFragments.get(mLastTabIndex);
            fragmentTransaction.hide(lastFragment);
        }
        BaseFragment fragment = mFragments.get(index);
        if (!fragment.isAdded()){
            fragmentTransaction.add(R.id.fl_content_bind_boardcode,fragment);
        }else {
            fragmentTransaction.show(fragment);
        }
        mLastTabIndex = index;
        fragmentTransaction.commit();
    }


    private void initView() {
        TextView mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_page_ticket);
        mRgCheckTicketBoardcode = findViewById(R.id.rg_check_ticket_boardcode);
        mViewLineServiceDetail = findViewById(R.id.view_line_service_detail);
        mViewLineIntroduce = findViewById(R.id.view_line_introduce);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.rb_ukey_service_detail:
                mViewLineServiceDetail.setVisibility(View.VISIBLE);
                mViewLineIntroduce.setVisibility(View.GONE);
                selectFragment(0);
                break;
            case R.id.rb_ukey_use_introduce:
                mViewLineServiceDetail.setVisibility(View.GONE);
                mViewLineIntroduce.setVisibility(View.VISIBLE);
                selectFragment(1);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
