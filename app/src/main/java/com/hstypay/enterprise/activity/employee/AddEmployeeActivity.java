package com.hstypay.enterprise.activity.employee;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.BindShopActivity;
import com.hstypay.enterprise.activity.RoleListActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.RoleSelectBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author kuangzeyu
 * @time 2021/3/2
 * @desc 新增员工
 */
public class AddEmployeeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private EditTextDelete mEtEmployeeNameAdd;
    private TextView mEtSelectRoleAddEmployee;
    private TextView mEtSelectStoreAddEmployee;
    private EditTextDelete mEtPhoneAddEmployee;
    private Button mBtnCommitAddEmployee;
    private RelativeLayout mRlRoleAddEmployee;//选择角色
    private RelativeLayout mRlStoreAddEmployee;//选择所属门店
    private LinearLayout mLlRefundPermission;//退款权限
    private TreeMap<String, String> mStoreMap = new TreeMap<>();
    private static final int REQUEST_CODE_SHOP_ACTIVITY = 0x1000;//选择门店
    private static final int REQUEST_CODE_ROLE_ACTIVITY = 0x1001;//选择角色
    private String mRoleCode;//选择的角色code
    private String mStoreId;//收银员选择的门店ID
    private List<String> mList = new ArrayList<>();//店长选择的门店id集合
    private TextView mTvAddNotice;
    private ImageView mIvPwd1, mIvPwd2;
    private boolean isOpenEye1, isOpenEye2;
    private EditText mEtPwd1, mEtPwd2;
    private SafeKeyboard safeKeyboard;
    private LinearLayout ll_root;
    private LinearLayout mLlPwd;
    private CheckBox mCbRefund;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee_manager);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvPwd1.setOnClickListener(this);
        mIvPwd2.setOnClickListener(this);
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setButtonView();
            }
        };
        mEtEmployeeNameAdd.addTextChangedListener(textWatcher);
        mEtPhoneAddEmployee.addTextChangedListener(textWatcher);
        mEtSelectRoleAddEmployee.addTextChangedListener(textWatcher);
        mEtSelectStoreAddEmployee.addTextChangedListener(textWatcher);
        mEtPwd1.addTextChangedListener(textWatcher);
        mEtPwd2.addTextChangedListener(textWatcher);
        mBtnCommitAddEmployee.setOnClickListener(this);
        mRlRoleAddEmployee.setOnClickListener(this);
        mRlStoreAddEmployee.setOnClickListener(this);

        mCbRefund.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }

    private void initData() {
        String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_VALUE_STORE_ADD_EMPLOYEE.equals(intentName)) {
            //门店详情里的添加员工操作
            String storeName = getIntent().getStringExtra(Constants.INTENT_DETAIL_STORE_NAME);
            if (!TextUtils.isEmpty(storeName)) {
                mEtSelectStoreAddEmployee.setText(storeName);
                String storeID = getIntent().getStringExtra(Constants.INTENT_DETAIL_STORE_ID);
                mList.add(storeID);
                mStoreId = storeID;
                mRlStoreAddEmployee.setEnabled(false);
                findViewById(R.id.iv_store_add_employee).setVisibility(View.INVISIBLE);
            }
        }
    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.string_title_add_employee));
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mEtEmployeeNameAdd = findViewById(R.id.et_employee_name_add);
        mEtPhoneAddEmployee = findViewById(R.id.et_phone_add_employee);
        mBtnCommitAddEmployee = findViewById(R.id.btn_commit_add_employee);
        mRlStoreAddEmployee = findViewById(R.id.rl_store_add_employee);
        mEtSelectStoreAddEmployee = findViewById(R.id.et_select_store_add_employee);
        mRlRoleAddEmployee = findViewById(R.id.rl_role_add_employee);
        mEtSelectRoleAddEmployee = findViewById(R.id.et_select_role_add_employee);
        mLlRefundPermission = findViewById(R.id.ll_refund_permission);
        mCbRefund = findViewById(R.id.cb_refund);
        mTvAddNotice = findViewById(R.id.tv_add_notice);
        mTvAddNotice.setVisibility(MyApplication.isRiskWhite() == 1 ? View.GONE : View.VISIBLE);
        mLlPwd = findViewById(R.id.ll_pwd);
        mLlPwd.setVisibility(MyApplication.isRiskWhite() == 1 ? View.VISIBLE : View.GONE);
        mIvPwd1 = (ImageView) findViewById(R.id.iv_manager_pwd1);
        mIvPwd2 = (ImageView) findViewById(R.id.iv_manager_pwd2);
        mEtPwd1 = (EditText) findViewById(R.id.et_manager_pwd1);
        mEtPwd2 = (EditText) findViewById(R.id.et_manager_pwd2);
        ll_root = (LinearLayout) findViewById(R.id.ly_ed_cash_title);

        setButtonEnable(mBtnCommitAddEmployee, false);


        View rootView = findViewById(R.id.main_root);
        final LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtPwd1.getId(), mEtPwd1);
        safeKeyboard.putEditText(mEtPwd2.getId(), mEtPwd2);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_manager_pwd1:
                isOpenEye1 = setEye(isOpenEye1);
                setPwdVisible(isOpenEye1, mIvPwd1, mEtPwd1);
                break;
            case R.id.iv_manager_pwd2:
                isOpenEye2 = setEye(isOpenEye2);
                setPwdVisible(isOpenEye2, mIvPwd2, mEtPwd2);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_commit_add_employee:
                //添加员工
                addEmployee();
                break;
            case R.id.rl_store_add_employee:
                //请选择所属门店
                if (TextUtils.isEmpty(mEtSelectRoleAddEmployee.getText().toString().trim())) {
                    ToastHelper.showInfo("请先选择角色");
                    return;
                }
                if (Constants.ROLE_STORE_NICK_MANAGER.equals(mRoleCode)) {
                    //店长
                    Intent intent = new Intent(this, BindShopActivity.class);
                    intent.putExtra(Constants.INTENT_NAME, Constants.REQUEST_ADD_INTENT);
                    intent.putExtra(Constants.RESULT_ADD_MANAGER, mStoreMap);
                    startActivityForResult(intent, Constants.REQUEST_ADD_MANAGER);
                } else if (Constants.ROLE_STORE_NICK_CASHIER.equals(mRoleCode)) {
                    //收银员
                    Intent intentShop = new Intent(this, ShopActivity.class);
                    intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                    startActivityForResult(intentShop, Constants.REQUEST_CASHIER_SHOP_CODE);
                }
                /*Intent intent = new Intent(AddEmployeeActivity.this, ShopActivity.class);
                intent.putExtra(Constants.INTENT_STORE_ID,mStoreId);
                startActivityForResult(intent,REQUEST_CODE_SHOP_ACTIVITY);*/
                break;
            case R.id.rl_role_add_employee:
                //选择角色
                Intent intentRole = new Intent(AddEmployeeActivity.this, RoleListActivity.class);
                intentRole.putExtra(Constants.INTENT_STORE_ID, mRoleCode);
                startActivityForResult(intentRole, REQUEST_CODE_ROLE_ACTIVITY);
                break;
            default:
                break;
        }
    }

    private boolean setEye(boolean isOpen) {
        return isOpen ? false : true;
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    //添加员工
    private void addEmployee() {
        if (!StringUtils.managerName(mEtEmployeeNameAdd.getText().toString().trim())) {//用户名
            showCommonNoticeDialog(this, getString(R.string.regex_manager_name));
            mEtEmployeeNameAdd.setFocusable(true);
            mEtEmployeeNameAdd.setFocusableInTouchMode(true);
            mEtEmployeeNameAdd.requestFocus();
            return;
        }

        String pwd1 = mEtPwd1.getText().toString().trim();
        String pwd2 = mEtPwd2.getText().toString().trim();
        if (!TextUtils.isEmpty(pwd1)) {
            //if (!StringUtils.isLetterDigit(mEtEmpPwd1.getText().toString().trim())){
            if (pwd1.length() < 6) {
                mEtPwd1.setFocusable(true);
                mEtPwd1.setFocusableInTouchMode(true);
                mEtPwd1.requestFocus();
                showCommonNoticeDialog(AddEmployeeActivity.this, getString(R.string.pwd_prompting));
                return;
            }
        }

        if (!TextUtils.isEmpty(pwd2)) {
            //if (!StringUtils.isLetterDigit(mEtEmpPwd1.getText().toString().trim())){
            if (pwd2.length() < 6) {
                mEtPwd2.setFocusable(true);
                mEtPwd2.setFocusableInTouchMode(true);
                mEtPwd2.requestFocus();
                showCommonNoticeDialog(AddEmployeeActivity.this, getString(R.string.pwd_prompting));
                return;
            }
        }

        if (!TextUtils.isEmpty(pwd1) && !TextUtils.isEmpty(pwd2)) {
            if (!pwd1.equals(pwd2)) {
                showCommonNoticeDialog(this, getString(R.string.tx_pass_notdiff));
                return;
            }
        }


        if (Constants.ROLE_STORE_NICK_MANAGER.equals(mRoleCode)) {
            addManager();
        } else if (Constants.ROLE_STORE_NICK_CASHIER.equals(mRoleCode)) {
            //收银员
            addCashier();
        }
    }

    private void addManager() {
        //添加店长
        //店长
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(this, getString(R.string.network_exception));
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("realName", mEtEmployeeNameAdd.getText().toString().trim());
            map.put("telphone", mEtPhoneAddEmployee.getText().toString().trim());
            map.put("storeIds", mList);//门店id
            map.put("password", mEtPwd2.getText().toString().trim());
            if (mLlRefundPermission.getVisibility() == View.VISIBLE && mCbRefund.isChecked())
                map.put("opCode", "APPLY-REFUND");
            ServerClient.newInstance(this).addManager(this, Constants.TAG_ADD_MANAGER, map);
        }
    }

    //添加收银员
    private void addCashier() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(this, getString(R.string.network_exception));
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("realName", mEtEmployeeNameAdd.getText().toString().trim());//用户名
            map.put("loginPhone", mEtPhoneAddEmployee.getText().toString().trim());
            map.put("storeIds", mList);//门店id
            map.put("password", mEtPwd2.getText().toString().trim());
            if (mLlRefundPermission.getVisibility() == View.VISIBLE && mCbRefund.isChecked())
                map.put("opCode", "APPLY-REFUND");
            ServerClient.newInstance(this).addCashier(this, Constants.TAG_ADD_CASHIER_MANAGE, map);
        }
    }

    /**
     * 查询对应员工角色是否具备退款权限
     *
     * @param empType 员工类型 2：收银员 4：店长
     */
    private void userPermission(String empType) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(this, getString(R.string.network_exception));
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("empType", empType);
            ServerClient.newInstance(this).userPermission(this, "TAG_USER_PERMISSION" + this.getClass().getSimpleName(), map);
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据，添加店长返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddManager(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_ADD_MANAGER)) {
            dismissLoading();
            CashierBean msg = (CashierBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(AddEmployeeActivity.this, getString(R.string.operation_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    break;
            }
        } else if (event.getTag().equals("TAG_USER_PERMISSION" + this.getClass().getSimpleName())) {
            dismissLoading();
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().isApplyRefund()) {
                        mLlRefundPermission.setVisibility(View.VISIBLE);
                    } else {
                        mLlRefundPermission.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    //Eventbus接收数据，添加收银员返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddCashier(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_ADD_CASHIER_MANAGE)) {
            dismissLoading();
            CashierBean msg = (CashierBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ADD_CASHIER_MANAGE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ADD_CASHIER_MANAGE_TRUE:
                    /*CashierManageBean.DataBean cashier = null;
                    if (msg.getData() != null) {
                        cashier = new CashierManageBean.DataBean();
                        cashier.setRealName(msg.getData().getRealName());
                        cashier.setUserName(msg.getData().getUserName());
                        cashier.setUserId(msg.getData().getUserId());
                        List<StoresBean> stores = new ArrayList<>();
                        StoresBean storesBean = new StoresBean();
                        storesBean.setStoreName(mTvOpenAdress.getText().toString().trim());
                        storesBean.setStoreId(mStoreId);
                        stores.add(storesBean);
                        cashier.setStores(stores);
                    }
                    getDialog(UIUtils.getString(R.string.dialog_add_employee), cashier);*/
                    showCommonNoticeDialog(AddEmployeeActivity.this, getString(R.string.operation_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    //Eventbus接收数据，退款权限返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_ADD_EMPLOYEE)) {
            dismissLoading();
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_CASHIER_EDIT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(AddEmployeeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(AddEmployeeActivity.this, getString(R.string.add_info_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            //添加成功
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    private void setButtonView() {
        if (mLlPwd.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(mEtEmployeeNameAdd.getText().toString().trim())
                    || TextUtils.isEmpty(mEtPhoneAddEmployee.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSelectRoleAddEmployee.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSelectStoreAddEmployee.getText().toString().trim())
                    || TextUtils.isEmpty(mEtPwd1.getText().toString().trim())
                    || TextUtils.isEmpty(mEtPwd2.getText().toString().trim())) {
                setButtonEnable(mBtnCommitAddEmployee, false);
            } else {
                setButtonEnable(mBtnCommitAddEmployee, true);
            }
        } else {
            if (TextUtils.isEmpty(mEtEmployeeNameAdd.getText().toString().trim())
                    || TextUtils.isEmpty(mEtPhoneAddEmployee.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSelectRoleAddEmployee.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSelectStoreAddEmployee.getText().toString().trim())) {
                setButtonEnable(mBtnCommitAddEmployee, false);
            } else {
                setButtonEnable(mBtnCommitAddEmployee, true);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ROLE_ACTIVITY && resultCode == RESULT_OK) {
            //选择角色
            Bundle b = data.getExtras();
            RoleSelectBean dataBean = (RoleSelectBean) b.getSerializable(Constants.RESULT_ROLE_BEAN_INTENT);
            if (dataBean != null) {
                String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
                if (!Constants.INTENT_VALUE_STORE_ADD_EMPLOYEE.equals(intentName)) {
                    //不是门店详情里的添加员工操作
                    if (!dataBean.getRoleCode().equals(mRoleCode)) {
                        //切换了角色要清除门店，重新选门店，因为选门店时判断了不同的角色
                        mEtSelectStoreAddEmployee.setText("");
                        mStoreId = "";
                        mList.clear();
                    }
                }
                mEtSelectRoleAddEmployee.setText(dataBean.getRoleName());
                mRoleCode = dataBean.getRoleCode();
                userPermission(dataBean.getRoleId());
            }

        }

        if (requestCode == Constants.REQUEST_ADD_MANAGER && resultCode == RESULT_OK) {
            //店长选择门店
            mStoreMap = new TreeMap<>((HashMap) data.getSerializableExtra("data"));
            mList.clear();
            StringBuilder builder = new StringBuilder();
            if (mStoreMap != null && mStoreMap.size() > 0) {
                for (String storeId : mStoreMap.keySet()) {
                    mList.add(storeId);
                    String storeName = mStoreMap.get(storeId);
                    if (storeId.equals(mStoreMap.lastKey())) {
                        builder.append(storeName);
                    } else {
                        builder.append(storeName).append("/");
                    }
                }
            }
            mEtSelectStoreAddEmployee.setText(builder.toString());
        }

        if (requestCode == Constants.REQUEST_CASHIER_SHOP_CODE && resultCode == RESULT_OK) {
            //收银员选择门店
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            if (shopBean != null) {
                mEtSelectStoreAddEmployee.setText(shopBean.getStoreName());
                mStoreId = shopBean.getStoreId();
                mList.clear();
                mList.add(mStoreId);
            }

        }
    }
}
