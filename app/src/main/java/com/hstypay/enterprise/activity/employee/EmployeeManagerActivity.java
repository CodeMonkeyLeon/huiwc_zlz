package com.hstypay.enterprise.activity.employee;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.cashier.EmployeeInfoActivity;
import com.hstypay.enterprise.activity.manager.ManagerDetailActivity;
import com.hstypay.enterprise.adapter.EmployeeManagerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierDetailBean;
import com.hstypay.enterprise.bean.EmployeeManagerBean;
import com.hstypay.enterprise.bean.ManagerDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 员工管理列表页
 */
public class EmployeeManagerActivity extends BaseActivity implements View.OnClickListener {
    List<EmployeeManagerBean.EmployeeManagerData> mEmployeeManageItemBeanList = new ArrayList<>();
    private ImageView mIvBack, mIvButton;
    private EditTextDelete mEtInput;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerviewEmployeeManager;
    private EmployeeManagerAdapter mEmployeeManagerAdapter;
    private final int REQUEST_CODE_ADD_EMPLOYEE = 0x1001;//新增
    private final int REQUEST_CODE_DETAIL_EMPLOYEE = 0x1002;//收银员详情
    private final int REQUEST_CODE_DETAIL_STOREMANAGER = 0x1003;//店长详情

    private EmployeeManagerBean.EmployeeManagerData mClickedEmployeeManageItemBean;
    private SafeDialog mLoadDialog;
    public static final int mPageSize = 15;
    protected int mCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private String searchKey;
    private LinearLayout mTvNotData;
    private Button mBtnAddEmp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_manager);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.string_title_employee_manager));
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvButton = (ImageView) findViewById(R.id.iv_button);
        mIvButton.setVisibility(View.VISIBLE);

        mEtInput = findViewById(R.id.et_input);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mRecyclerviewEmployeeManager = findViewById(R.id.recyclerview_employee_manager);
        mRecyclerviewEmployeeManager.setLayoutManager(new LinearLayoutManager(this));
        mTvNotData = findViewById(R.id.ll_empty);
        mBtnAddEmp = findViewById(R.id.btn_add_emp);
        setButtonEnable(mBtnAddEmp, true);
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mEmployeeManagerAdapter = new EmployeeManagerAdapter(this);
        mRecyclerviewEmployeeManager.setAdapter(mEmployeeManagerAdapter);

        getDatas();
    }

    private void getDatas() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh || isLoadmore)) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mPageSize);
            map.put("currentPage", mCurrentPage);
            if (!TextUtils.isEmpty(searchKey)) {
                map.put("q", searchKey);
            }
            ServerClient.newInstance(MyApplication.getContext()).empList(MyApplication.getContext(), Constants.TAG_EMPLOYEE_LIST, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvButton.setOnClickListener(this);
        mBtnAddEmp.setOnClickListener(this);
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String input = mEtInput.getText().toString().trim();
                    if (!TextUtils.isEmpty(input)) {
                        searchKey = input;
                        mCurrentPage = 1;
                        isPullRefresh = true;
                        getDatas();
                    }
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    searchKey = "";
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getDatas();
                }
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getDatas();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    //mCurrentPage++;//要在获取数据成功后才能++，否则失败了又得--回去
                    getDatas();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

        mEmployeeManagerAdapter.setOnRecycleItemClickListener(new EmployeeManagerAdapter.OnRecycleItemClickListener() {
            @Override
            public void onItemClick(EmployeeManagerBean.EmployeeManagerData employeeManageItemBean) {
                mClickedEmployeeManageItemBean = employeeManageItemBean;
                int role = employeeManageItemBean.getRole();
                if (role == Constants.ROLE_STORE_MANAGER) {
                    //店长
                    clickStoreManagerItem(employeeManageItemBean);
                } else if (role == Constants.ROLE_STORE_CASHIER) {
                    //收银员
                    clickCashierItem(employeeManageItemBean);
                }
            }
        });
    }

    //点了店长
    private void clickStoreManagerItem(EmployeeManagerBean.EmployeeManagerData employeeManageItemBean) {
        String userId = employeeManageItemBean.getUserId();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("userId", userId);
            ServerClient.newInstance(MyApplication.getContext()).managerDetail(MyApplication.getContext(), Constants.TAG_MANAGER_DETAIL, map);
        } else {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
        }
    }

    //店长详情
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void storeManagerDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_MANAGER_DETAIL)) {
            ManagerDetailBean msg = (ManagerDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmployeeManagerActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        Intent intent = new Intent(this, ManagerDetailActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable(Constants.REQUEST_MANAGER_INTENT, msg.getData());   //传递一个user对象列表
                        intent.putExtras(mBundle);
                        startActivityForResult(intent, REQUEST_CODE_DETAIL_STOREMANAGER);
                    } else {
                        MyToast.showToastShort(ToastHelper.toStr(R.string.data_error));
                    }
                    break;
            }
            dismissLoading();
        }
    }


    //点了收银员
    private void clickCashierItem(EmployeeManagerBean.EmployeeManagerData employeeManageItemBean) {
        String userId = employeeManageItemBean.getUserId();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("userId", userId);
            ServerClient.newInstance(this).cashierDetail(this, Constants.TAG_CASHIER_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //收银员详情
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void cashierDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CASHIER_DETAIL)) {
            CashierDetailBean msg = (CashierDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.CASHIER_DETAIL_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmployeeManagerActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(EmployeeManagerActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CASHIER_DETAIL_TRUE:
                    CashierDetailBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        intent.putExtra("storeInfo", data);
                        /*if (mClickedEmployeeManageItemBean != null && !TextUtils.isEmpty(mClickedEmployeeManageItemBean.getPhone())) {
                            intent.putExtra("cashierUserName", mClickedEmployeeManageItemBean.getPhone());
                        } else {
                            intent.putExtra("cashierUserName", "");
                        }
                        if (mClickedEmployeeManageItemBean != null) {
                            intent.putExtra("userId", mClickedEmployeeManageItemBean.getUserId());
                        } else {
                            intent.putExtra("userId", "");
                        }*/
                        intent.setClass(EmployeeManagerActivity.this, EmployeeInfoActivity.class);
                        startActivityForResult(intent, REQUEST_CODE_DETAIL_EMPLOYEE);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpListManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_EMPLOYEE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            EmployeeManagerBean msg = (EmployeeManagerBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(EmployeeManagerActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData() != null && msg.getData().size() > 0) {
                            if (mCurrentPage == 1) {
                                mEmployeeManageItemBeanList.clear();
                            }
                            mEmployeeManageItemBeanList.addAll(msg.getData());
                            mEmployeeManagerAdapter.setData(mEmployeeManageItemBeanList);

                            mCurrentPage++;
                        } else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_nore));
                            } else {
                                mEmployeeManageItemBeanList.clear();//使后面会展示空视图
                                mEmployeeManagerAdapter.setData(mEmployeeManageItemBeanList);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_nore));
                        } else {
                            mEmployeeManageItemBeanList.clear();//使后面会展示空视图
                            mEmployeeManagerAdapter.setData(mEmployeeManageItemBeanList);
                        }
                    }
                    if (mEmployeeManageItemBeanList != null && mEmployeeManageItemBeanList.size() > 0) {
                        mTvNotData.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    } else {
                        mTvNotData.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                    }
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button://新增员工
            case R.id.btn_add_emp://新增员工
                Intent intent = new Intent(EmployeeManagerActivity.this, AddEmployeeActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_EMPLOYEE);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD_EMPLOYEE || requestCode == REQUEST_CODE_DETAIL_EMPLOYEE || requestCode == REQUEST_CODE_DETAIL_STOREMANAGER) {
            if (resultCode == RESULT_OK) {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getDatas();
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        }
    }
}
