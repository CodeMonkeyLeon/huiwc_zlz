package com.hstypay.enterprise.activity.facepay;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chinaums.mis.bean.ResponsePojo;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.dialog.ToastDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.PayResultActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PosOrderBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author dean.zeng
 * @Description 人脸支付
 * @Date 2020-06-28 14:02
 **/
public class FacePayActivity extends BaseActivity {


    private static String TAG = "FacePayActivity";

    /*刷脸金额key*/
    public static String KEY_AMOUNT_FACE_PAY = "key_amount_face_pay";
    /*刷脸订单对象*/
    public static String KEY_ORDER_BEAN = "key_order_bean";
    /*查询订单*/
    public static String TAG_SEARCH_ORDER_STATE_FACE_PAY = "TAG_SEARCH_ORDER_STATE_FACE_PAY";
    private TextView mTvTitle;
    private SafeDialog mLoadDialog;
    private PosOrderBean.DataEntity mBilldata;
    private String amount = "";
    private boolean retry = true;
    private ResponsePojo response = null;
    private ToastDialog mToastDialog;


    private CountDownTimer mCountDownTimer = new CountDownTimer(120000, 1000) {
        @Override
        public void onTick(long l) {
            mToastDialog.setContent(String.format("等待用户付款中…\n%ss", (int) l / 1000));
        }

        @Override
        public void onFinish() {
            UnionPayFacePayUtils.bankCancel();
            if (!FacePayActivity.this.isFinishing()) {
                mCountDownTimer.cancel();
                mToastDialog.dismiss();
                syncDialog();
            }
        }
    };
    private boolean cancelTask;
    private SelectDialog mSyncDialog;
    private Handler mHandler;
    private SelectDialog mSyncSelectDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_pay);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        mHandler = new Handler();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mTvTitle = findViewById(R.id.tv_title);
        TextView tvAmount = findViewById(R.id.tvAmount);
        mBilldata = (PosOrderBean.DataEntity) getIntent().getSerializableExtra(KEY_ORDER_BEAN);
        amount = mBilldata.getMoney();
        double totalMoney = new BigDecimal(amount).divide(new BigDecimal(100)).doubleValue();
        tvAmount.setText(DateUtil.formatMoneyUtil(totalMoney));
        mTvTitle.setText(getString(R.string.title_face_pay));
        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        facePay(mBilldata);
        mToastDialog = new ToastDialog(this);
        mToastDialog.show();
        mToastDialog.setContent("等待用户付款中…\n120s");
        mCountDownTimer.start();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.NOTIFY_FACE_PAY)) {//刷脸成功通知
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(FacePayActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(FacePayActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    remove();
                    mCountDownTimer.cancel();
                    mToastDialog.dismiss();
                    getOrderDetail(Constants.INTENT_NAME_FACE_PAYRESULT);
                    break;
            }
        }
        if (event.getTag().equals(Constants.INTENT_NAME_FACE_PAYRESULT) || event.getTag().equals("INTENT_NAME_FACE_PAYDETAIL")) {//查询订单
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        if (event.getTag().equals("INTENT_NAME_FACE_PAYDETAIL")) {
                            intent.putExtra(Constants.INTENT_BILL_DATA, data);
                            intent.putExtra(Constants.INTENT_NAME, "INTENT_NAME_FACE_PAYDETAIL");
                            intent.setClass(FacePayActivity.this, PayDetailActivity.class);
                        } else {
                            intent.putExtra("capture", data);
                            intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_FACE_PAYRESULT);
                            intent.setClass(this, PayResultActivity.class);
                        }
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    }

    /* 刷脸支付 */
    private void facePay(PosOrderBean.DataEntity bean) {
        UnionPayFacePayUtils.Builder builder = new UnionPayFacePayUtils.Builder(24, amount, bean.getOpUserId(), bean.getReqOrderNo());
        UnionPayFacePayUtils.bankDAO(builder, this, new UnionPayFacePayUtils.FaceCallBack() {
            @Override
            public void response(ResponsePojo responsePojo) {

                response = responsePojo;
                if (responsePojo != null && response.getRspCode().equals("00")) {
                    if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                        MyToast.showToastShort(getString(R.string.network_exception));
                        syncOrderState();
                    } else {
                        payYSFaceNotify(response);
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!FacePayActivity.this.isFinishing()) {
                                showCommonNoticeDialog(FacePayActivity.this, "null", response.getRspChin(), new CommonNoticeDialog.OnClickOkListener() {
                                    @Override
                                    public void onClickOk() {
                                        mCountDownTimer.cancel();
                                        mToastDialog.dismiss();
                                        syncDialog();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * 银商pos人脸支付查单
     */
    /*private void checkYSFacepayOrder() {
        DialogUtil.safeShowDialog(mLoadDialog);
        UnionPayFacePayUtils.Builder builder = new UnionPayFacePayUtils.Builder(43, "1", mBilldata.getOpUserId(), mBilldata.getReqOrderNo());
        UnionPayFacePayUtils.bankDAO(builder, this, responsePojo -> {
            runOnUiThread(() -> DialogUtil.safeCloseDialog(mLoadDialog));
            LogUtil.d("Jeremy--responsePojo====" + new Gson().toJson(responsePojo));
            if (responsePojo != null && responsePojo.getRspCode().equals("00")) {
                payYSFaceNotify(responsePojo);
            } else {
                runOnUiThread(() -> showCommonNoticeDialog(FacePayActivity.this, responsePojo.getRspChin(), new CommonNoticeDialog.OnClickOkListener() {
                    @Override
                    public void onClickOk() {
                        finish();
                    }
                }));
            }
        });
    }*/

    /*支付成功通知*/
    private void payYSFaceNotify(ResponsePojo response) {
        DialogUtil.safeShowDialog(mLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("timeEnd", Calendar.getInstance().get(Calendar.YEAR) + response.getTradeDate() + response.getTradeTime());
        map.put("transactionId", mBilldata.getOrderNo());
        map.put("outTradeNo", mBilldata.getOutTradeNo());
        map.put("totalFee", new BigDecimal(amount));
        map.put("storeMerchantId", mBilldata.getStoreMerchantId());
        map.put("payType", response.getBankName());
        map.put("voucherNo", response.getTraceNo());

        map.put("batchId", response.getBatchNo());

        map.put("refNo", response.getRefNo());

        String[] transMemo = response.getTransMemo().split("&");
        if (transMemo.length > 2) {
            map.put("thirdOrderNo", transMemo[1]);
        }
        if (transMemo.length > 7) {
            map.put("cardType", transMemo[7]);
        }
//        map.put("deviceInfo",HPAY_POS_EASYPAY_termid)
        LogUtil.e(TAG, map.toString());
        ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), Constants.NOTIFY_FACE_PAY, map);
    }


    private void getOrderDetail(String requestTag) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(mBilldata.getOrderNo())) {
                map.put("orderNo", mBilldata.getOrderNo());
            }
            ServerClient.newInstance(this).getOrderDetail(this, requestTag, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private long timeCount = 5;
    private int count = 1;
    private String dialogMessage = null;
    private NoticeDialog showDialog;

    private void syncOrderState() {
        if (count < 5) {
            timeCount = 5;
            count++;
            showDialog = new NoticeDialog(FacePayActivity.this, dialogMessage, "取消同步", R.layout.notice_dialog_check_order);
            showDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    cancelTask = true;
                    toSyncOrderStatus();
                    mHandler.removeCallbacks(myRunnable);
                }
            });
            DialogHelper.resize(FacePayActivity.this, showDialog);
            showDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            cancelTask = true;
            toSyncOrderStatus();
        }
    }

    private void toSyncOrderStatus() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            if (cancelTask) {
                showSyncResult();
            } else {
                syncOrderState();
            }
        } else {
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                payYSFaceNotify(response);
            }
        }
    }

    private void showSyncResult() {
        mSyncDialog = new SelectDialog(FacePayActivity.this, "订单状态同步失败", "取消", "继续同步", R.layout.select_common_dialog);
        mSyncDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                cancelTask = true;
                toSyncOrderStatus();
            }
        });
        mSyncDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                toMainActivity();
            }
        });
        DialogHelper.resize(FacePayActivity.this, mSyncDialog);
        mSyncDialog.show();
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && showDialog != null) {
                showDialog.setMessage(getString(R.string.dialog_order_sync) + timeCount
                        + getString(R.string.dialog_sync_start) + (count) + getString(R.string.dialog_sync_end));
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (showDialog != null && showDialog.isShowing()) {
                    showDialog.dismiss();
                }
                if (count == 5)
                    cancelTask = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toSyncOrderStatus();
                    }
                });
            }
        }
    };

    private void remove() {
        if (showDialog != null) {
            showDialog.dismiss();
        }
        if (mSyncDialog != null) {
            mSyncDialog.dismiss();
        }
        mHandler.removeCallbacks(myRunnable);
    }

    private void syncDialog() {
        mSyncSelectDialog = new SelectDialog(FacePayActivity.this, "支付失败，如有异议，请至订单详情页同步查询", "取消", "去查询", R.layout.select_common_dialog);
        mSyncSelectDialog.setOnClickOkListener(() -> getOrderDetail("INTENT_NAME_FACE_PAYDETAIL"));
        mSyncSelectDialog.setOnClickCancelListener(() -> {
            startActivity(new Intent(FacePayActivity.this, MainActivity.class));
            finish();
        });
        DialogHelper.resize(this, mSyncSelectDialog);
        mSyncSelectDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCountDownTimer.cancel();
        mToastDialog.dismiss();
        if (mSyncSelectDialog != null) {
            mSyncSelectDialog.dismiss();
        }
        remove();
    }

    private void toMainActivity() {
        Intent intent = new Intent(Constants.ACTION_RECEIVE_RESET_DATA);
        sendBroadcast(intent);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
