package com.hstypay.enterprise.activity.facepay;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.chinaums.mis.bank.BankDAO;
import com.chinaums.mis.bank.ICallBack;
import com.chinaums.mis.bean.RequestPojo;
import com.chinaums.mis.bean.ResponsePojo;
import com.chinaums.mis.bean.TransCfx;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

/**
 * @Author dean.zeng
 * @Description 银商刷脸支付工具类
 * @Date 2020-06-30 15:58
 **/
public class UnionPayFacePayUtils {

    private static String TAG = "UnionpayFacePayUtils";

    private static TransCfx transCFX;
    private static RequestPojo request;

    private static BankDAO bankDAO;


    /* 初始化刷脸基本参数 */
    private static void initFacePayValue() {
        transCFX = new TransCfx();
        transCFX.setSsl_on(2);
        transCFX.setIp("upos.chinaums.com");
        transCFX.setPort(16601);
        transCFX.setTpdu("6000300000");
        transCFX.setMchtId(MyApplication.getMechantId());
        transCFX.setTermId(Build.SERIAL);
        transCFX.setAuthSN("");
        transCFX.setTerm_info("123456789012345");
        transCFX.setSsl_sn(Build.SERIAL);
        transCFX.setSsl_cert("umscert.pem");
        transCFX.setPosConnMode(0);
    }

    /* 初始化刷脸Request参数 */
    private static void initRequestValue(Builder builder) {
        request = new RequestPojo();
        String operId = builder.operId;
        operId = repairZero(operId, 8, true);
        request.setOperId(operId);
        String sn = Build.SERIAL;
        sn = repairZero(sn, 8, true);
        request.setPosId(sn);
        request.setTransType(String.valueOf(builder.transType));
        String amount = repairZero(builder.amount, 12, false);
        request.setAmount(amount);
        StringBuilder transMemo = new StringBuilder("08&");
        transMemo.append(builder.reqOrderNo);
        transMemo.append("&");
        transMemo.append(MyApplication.getMerchantName());
        if (builder.transType == 30) {//人行人脸撤销
            transMemo.append("&");
            if (!TextUtils.isEmpty(builder.traceNO)){
                transMemo.append(builder.traceNO);
            }
        } else if (builder.transType == 31) {//微信人脸和扫码撤销
            transMemo.append("&");
            transMemo.append(builder.thirdOrderNo);
        } else if (builder.transType == 32) {//人行人脸退货
            transMemo.append("&");
            transMemo.append(builder.timeEnd);
            transMemo.append("&");
            transMemo.append(builder.refNo);
        } else if (builder.transType == 33) {//微信人脸和扫码退货
            transMemo.append("&");
            transMemo.append(builder.thirdOrderNo);
        }
        LogUtil.e(TAG, transMemo.toString());
        request.setTransMemo(transMemo.toString());
    }

    /* 刷脸支付相关 */
    public static void bankDAO(Builder builder, final Context context, final FaceCallBack callBack) {
        initFacePayValue();
        initRequestValue(builder);
        new Thread(new Runnable() {
            @Override
            public void run() {
                bankDAO = new BankDAO(context);
                bankDAO.getCallBack(new BackCall());

                ResponsePojo response = null;
                try {
                    response = bankDAO.bankall(transCFX, request);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                callBack.response(response);
            }
        }).start();
    }

    /*关闭刷脸页面*/
    public static void bankCancel(){
        if (bankDAO!=null){
            bankDAO.bankCancel();
        }
    }

    /*  字符串补0*/
    private static String repairZero(String str, int digit, boolean isLeft) {
        if (str == null) {
            str = "";
        }
        if (str.length() > 8) {
            return str.substring(0, 8);
        }
        digit = digit - str.length();
        StringBuilder sb = new StringBuilder();
        if (isLeft) {
            sb.append(str);
        }
        for (int i = 0; i < digit; i++) {
            sb.append(0);
        }
        if (!isLeft) {
            sb.append(str);
        }
        return sb.toString();
    }

    /**
     * 回调函数的实现
     *
     * @author lihuai
     */
    private static class BackCall implements ICallBack {

        @Override
        public void getCallBack(String stateCode, String stateTips) {
            LogUtil.e(TAG, "stateCode=" + stateCode + "|" + "stateTips=");
        }

    }

    /**
     * 银商刷脸回调接口
     */
    public interface FaceCallBack {
        void response(ResponsePojo response);
    }


    public static class Builder {
        int transType;
        String amount;
        String operId;
        String reqOrderNo;

        String traceNO;
        String thirdOrderNo;
        String timeEnd;
        String refNo;

        public Builder(int transType, String amount, String operId, String reqOrderNo) {
            this.transType = transType;
            this.amount = amount;
            this.operId = operId;
            this.reqOrderNo = reqOrderNo;
        }

        /**
         * 交易凭证号
         *
         * @param traceNO
         * @return
         */
        public Builder setTraceNO(String traceNO) {
            this.traceNO = traceNO;
            return this;
        }

        /**
         * 银商订单号
         *
         * @param thirdOrderNo
         * @return
         */
        public Builder setThirdOrderNo(String thirdOrderNo) {
            this.thirdOrderNo = thirdOrderNo;
            return this;
        }

        /**
         * 交易日期
         *
         * @param timeEnd
         * @return
         */
        public Builder setTimeEnd(String timeEnd) {
            this.timeEnd = DateUtil.formartTradeTime1(timeEnd);
            return this;
        }

        /**
         * 系统参考号
         *
         * @param refNo
         * @return
         */
        public Builder setRefNo(String refNo) {
            this.refNo = refNo;
            return this;
        }
    }
}
