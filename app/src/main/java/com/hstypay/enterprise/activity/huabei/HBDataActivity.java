package com.hstypay.enterprise.activity.huabei;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.QuestionActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.activity.receipt.InstructionActivity;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.paySite.DeviceTypeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.HbfqDataBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.SvcAppListBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DensityUtils;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HBDataActivity extends BaseActivity {
    private ImageView mIvBack, mIvInstruction, mIvFqCount, mIvFreePart, mIvDeleteNotice;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle, mTvDate, mTvMoney, mTvCount, mTvHbOpen, mTvOpen, mTvFqCount, mTvFreePart, mTvNoticeDetail;
    private ViewPager mPager;
    private LinearLayout mPointContainer, mLlFreeDetail, mLlNoticeDetail;
    private View mSelectedPoint;
    private RelativeLayout mRlSelectMonth, mRlHbfqQuestion, mRlFqCount, mRlFreePart, mRlRecyclerView;
    private List<View> mViews;
    private int mSpace;
    private ScrollView mSvContent;
    private TimePickerView pvTime;
    private SafeDialog mLoadDialog;
    private final static int[] PICS = new int[]{R.mipmap.img_huabei_banner01,
            R.mipmap.img_huabei_banner02,
            R.mipmap.img_huabei_banner03};
    private String mIntentName;
    private RecyclerView mRecyclerView;
    private boolean mCountSwitchArrow, mPartSwitchArrow;
    private Animation rotate;
    private List<DataEntity> mList, mHbCountList, mHbFreePartList;
    private String mId;
    private String mHbCount, mHbFreePart;
    private String mYear, mMonth;
    private DeviceTypeAdapter mAdapter;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private OnSingleClickListener mOnSingleClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hb_data);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
        initTimePicker();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mPager = (ViewPager) findViewById(R.id.guide_pager);
        mPointContainer = (LinearLayout) findViewById(R.id.guide_point_container);
        mSelectedPoint = findViewById(R.id.guide_selected_point);
        mRlSelectMonth = (RelativeLayout) findViewById(R.id.rl_select_month);
        mRlHbfqQuestion = (RelativeLayout) findViewById(R.id.rl_hbfq_question);
        mTvDate = (TextView) findViewById(R.id.tv_hbfq_date);
        mTvMoney = (TextView) findViewById(R.id.tv_hbfq_money);
        mTvCount = (TextView) findViewById(R.id.tv_hbfq_count);
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mIvInstruction = findViewById(R.id.iv_instruction);
        mRlRecyclerView = findViewById(R.id.rl_recycler_view);
        mRecyclerView = findViewById(R.id.recycler_view);
        mTvHbOpen = findViewById(R.id.tv_hb_open);
        mTvOpen = findViewById(R.id.tv_open);//开通按钮
        mLlFreeDetail = findViewById(R.id.ll_hbfq_interest_free_detail);
        mRlFqCount = findViewById(R.id.rl_select_fq_count);
        mTvFqCount = findViewById(R.id.tv_select_fq_count);
        mIvFqCount = findViewById(R.id.iv_select_fq_count);
        mRlFreePart = findViewById(R.id.rl_select_fee_part);
        mTvFreePart = findViewById(R.id.tv_select_fee_part);
        mIvFreePart = findViewById(R.id.iv_select_fee_part);
        mLlNoticeDetail = findViewById(R.id.ll_notice_open_detail);
        mTvNoticeDetail = findViewById(R.id.tv_notice_open_detail);
        mIvDeleteNotice = findViewById(R.id.iv_delete_notice);
        mButton.setText(R.string.btn_hbfq_material_download);
        mTvTitle.setText(R.string.title_hbfq_data);

        initSwipeRefreshLayout();
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setLoadmoreEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    getHbfqCollect(false);
                    if (!"2".equals(MyApplication.getHbOpenStatus()))
                        haveOpenSvcApp(false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mOnSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        finish();
                        break;
                    case R.id.button_title:
                        if (Constants.INTENT_USER_HUABEI.equals(mIntentName)) {
                            Intent intent = new Intent(HBDataActivity.this, RegisterActivity.class);
                            intent.putExtra(Constants.REGISTER_ALLOW_CLOSE, true);
                            intent.putExtra(Constants.REGISTER_INTENT, Constants.URL_BANNER_HUABEI);
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(HBDataActivity.this, HBMaterialActivity.class));
                        }
                        break;
                    case R.id.rl_select_month:
                        pvTime.show();
                        break;
                    case R.id.rl_hbfq_question:
                        Intent intent = new Intent(HBDataActivity.this, QuestionActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, Constants.URL_HUABEI_QUESTION);
                        startActivity(intent);
                        break;
                    case R.id.btnCancel:
                        pvTime.dismiss();
                        break;
                    case R.id.btnSubmit:
                        pvTime.returnData();
                        pvTime.dismiss();
                        getHbfqCollect(true);
                        break;
                    case R.id.rl_select_fq_count:
                        if (mPartSwitchArrow) {
                            closeArrow(3);
                        }
                        if (mCountSwitchArrow) {
                            closeArrow(2);
                        } else {
                            openArrow(2);
                        }
                        break;
                    case R.id.rl_select_fee_part:
                        if (mCountSwitchArrow) {
                            closeArrow(2);
                        }
                        if (mPartSwitchArrow) {
                            closeArrow(3);
                        } else {
                            openArrow(3);
                        }
                        break;
                    case R.id.iv_delete_notice:
                        mLlNoticeDetail.setVisibility(View.GONE);
                        break;
                    case R.id.tv_open:
                        if (TextUtils.isEmpty(MyApplication.getHbAppId())) {
                            querySvcAppList();
                        } else {
                            openFunction();
                        }
                        break;
                    case R.id.ll_hbfq_interest_free_detail:
                        startActivity(new Intent(HBDataActivity.this, InstructionActivity.class)
                                .putExtra(Constants.INTENT_OUT_URL, "file:///android_asset/instruction_hb.html"));
                        break;
                    case R.id.iv_instruction:
                        showCommonNoticeDialog(HBDataActivity.this, getString(R.string.tv_hb_money_instruction));
                        break;
                }
            }
        };
        mIvBack.setOnClickListener(mOnSingleClickListener);
        mButton.setOnClickListener(mOnSingleClickListener);
        mRlSelectMonth.setOnClickListener(mOnSingleClickListener);
        mRlHbfqQuestion.setOnClickListener(mOnSingleClickListener);
        mRlFqCount.setOnClickListener(mOnSingleClickListener);
        mRlFreePart.setOnClickListener(mOnSingleClickListener);
        mIvDeleteNotice.setOnClickListener(mOnSingleClickListener);
        mIvInstruction.setOnClickListener(mOnSingleClickListener/*new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                showCommonNoticeDialog(HBDataActivity.this, getString(R.string.tv_hb_money_instruction));
            }
        }*/);
        mTvOpen.setOnClickListener(mOnSingleClickListener/*new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (TextUtils.isEmpty(MyApplication.getHbAppId())) {
                    querySvcAppList();
                } else {
                    openFunction();
                }
            }
        }*/);
        mLlFreeDetail.setOnClickListener(mOnSingleClickListener/*new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                startActivity(new Intent(HBDataActivity.this, InstructionActivity.class)
                        .putExtra(Constants.INTENT_OUT_URL, "file:///android_asset/instruction_hb.html"));
            }
        }*/);
    }

    private void initData() {
        if ("2".equals(MyApplication.getHbOpenStatus())) {
            //已开通
            setView("2");
            mSvContent.setVisibility(View.VISIBLE);
        } else {
            haveOpenSvcApp(true);
        }
        mId = "";
        mHbCount = "";
        mHbFreePart = "";
        initListData();

        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new DeviceTypeAdapter(HBDataActivity.this, mList);
        mAdapter.setOnItemClickListener(new DeviceTypeAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mId = mList.get(position).getUserId();
                mAdapter.setSelected(mId);
                switch (mList.get(position).getType()) {
                    case 2:
                        mHbCount = mId;
                        mTvFqCount.setText(mList.get(position).getRealName());
                        break;
                    case 3:
                        mHbFreePart = mId;
                        mTvFreePart.setText(mList.get(position).getRealName());
                        break;
                }
                closeArrow(mList.get(position).getType());
                getHbfqCollect(true);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        //初始化图片数据
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_USER_HUABEI.equals(mIntentName)) {
            mButton.setText(getText(R.string.btn_hbfq_material_help));
        }
        mViews = new ArrayList<>();
        for (int i = 0; i < PICS.length; i++) {
            ImageView iv = new ImageView(this);
            //设置图片
            View view = LayoutInflater.from(this).inflate(R.layout.item_guide_hbfq, null);
            view.setBackgroundResource(R.color.white);
            ((ImageView) view.findViewById(R.id.guide_image)).setImageResource(PICS[i]);
            mViews.add(view);
            //给装点的容器动态加载点
            ImageView point = new ImageView(this);
            point.setBackgroundResource(R.drawable.shape_hbfq_banner_normal);
            point.setScaleType(ImageView.ScaleType.CENTER);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DensityUtils.dp2px(this, 7),
                    DensityUtils.dp2px(this, 4));
            params.leftMargin = DensityUtils.dp2px(this, 4);
            params.rightMargin = DensityUtils.dp2px(this, 4);

            mPointContainer.addView(point, params);
        }

        //给viewpager加载数据
        mPager.setAdapter(new GuideAdapter());//adapter--->list
        //设置viewpager滑动监听
        mPager.addOnPageChangeListener(new PagerListener());

        Calendar now = Calendar.getInstance();
        mYear = now.get(Calendar.YEAR) + "";
        mMonth = (now.get(Calendar.MONTH) + 1) + "";
        getHbfqCollect(true);
    }

    public void initListData() {
        mList = new ArrayList<>();
        mHbCountList = new ArrayList<>();
        DataEntity dataEntity1 = new DataEntity();
        dataEntity1.setType(2);
        dataEntity1.setRealName("全部");
        dataEntity1.setUserId("");
        DataEntity dataEntity2 = new DataEntity();
        dataEntity2.setType(2);
        dataEntity2.setRealName("3期");
        dataEntity2.setUserId("3");
        DataEntity dataEntity3 = new DataEntity();
        dataEntity3.setType(2);
        dataEntity3.setRealName("6期");
        dataEntity3.setUserId("6");
        DataEntity dataEntity4 = new DataEntity();
        dataEntity4.setType(2);
        dataEntity4.setRealName("12期");
        dataEntity4.setUserId("12");
        mHbCountList.add(dataEntity1);
        mHbCountList.add(dataEntity2);
        mHbCountList.add(dataEntity3);
        mHbCountList.add(dataEntity4);
        mHbFreePartList = new ArrayList<>();
        DataEntity dataEntity5 = new DataEntity();
        dataEntity5.setType(3);
        dataEntity5.setRealName("全部");
        dataEntity5.setUserId("");
        DataEntity dataEntity6 = new DataEntity();
        dataEntity6.setType(3);
        dataEntity6.setRealName("用户承担");
        dataEntity6.setUserId("1");
        DataEntity dataEntity7 = new DataEntity();
        dataEntity7.setType(3);
        dataEntity7.setRealName("商户承担");
        dataEntity7.setUserId("2");
        mHbFreePartList.add(dataEntity5);
        mHbFreePartList.add(dataEntity6);
        mHbFreePartList.add(dataEntity7);
    }

    private void getHbfqCollect(boolean isShowDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (isShowDialog)
                DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("year", mYear);
            map.put("month", mMonth);
            map.put("periodNum", mHbCount);
            map.put("feeUndertaker", mHbFreePart);
            ServerClient.newInstance(MyApplication.getContext()).hbfqCollect(MyApplication.getContext(), Constants.TAG_HBFQ_COLLECT, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        Calendar selectedDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        //时间选择器
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date, "yyyy-MM");
                Date now = new Date();
                if (DateUtil.getSelectDate(now, "yyyy-MM").equals(time)) {
                    mTvDate.setText("本月");
                } else {
                    mTvDate.setText(time);
                }
                Calendar calendarDate = Calendar.getInstance();
                calendarDate.setTime(date);
                mYear = calendarDate.get(Calendar.YEAR) + "";
                mMonth = (calendarDate.get(Calendar.MONTH) + 1) + "";
            }
        })
                .setLayoutRes(R.layout.layout_pickerview_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        Button btnCancel = v.findViewById(R.id.btnCancel);
                        Button btnSubmit = v.findViewById(R.id.btnSubmit);
                        btnCancel.setOnClickListener(mOnSingleClickListener);
                        btnSubmit.setOnClickListener(mOnSingleClickListener);
                    }
                })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, false, false, false, false})
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                .setRangDate(null, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年", "月", null, null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    /*@Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (Constants.INTENT_USER_HUABEI.equals(mIntentName)) {
                    Intent intent = new Intent(HBDataActivity.this, RegisterActivity.class);
                    intent.putExtra(Constants.REGISTER_ALLOW_CLOSE, true);
                    intent.putExtra(Constants.REGISTER_INTENT, Constants.URL_BANNER_HUABEI);
                    startActivity(intent);
                } else {
                    startActivity(new Intent(this, HBMaterialActivity.class));
                }
                break;
            case R.id.rl_select_month:
                pvTime.show();
                break;
            case R.id.rl_hbfq_question:
                Intent intent = new Intent(this, QuestionActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT, Constants.URL_HUABEI_QUESTION);
                startActivity(intent);
                break;
            case R.id.btnCancel:
                pvTime.dismiss();
                break;
            case R.id.btnSubmit:
                pvTime.returnData();
                pvTime.dismiss();
                break;
            case R.id.rl_select_fq_count:
                if (mPartSwitchArrow) {
                    closeArrow(3);
                }
                if (mCountSwitchArrow) {
                    closeArrow(2);
                } else {
                    openArrow(2);
                }
                break;
            case R.id.rl_select_fee_part:
                if (mCountSwitchArrow) {
                    closeArrow(2);
                }
                if (mPartSwitchArrow) {
                    closeArrow(3);
                } else {
                    openArrow(3);
                }
                break;
            case R.id.iv_delete_notice:
                mLlNoticeDetail.setVisibility(View.GONE);
                break;
        }
    }*/

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 2:
                mCountSwitchArrow = false;
                mIvFqCount.startAnimation(rotate);
                mRlRecyclerView.setVisibility(View.GONE);
                break;
            case 3:
                mPartSwitchArrow = false;
                mIvFreePart.startAnimation(rotate);
                mRlRecyclerView.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        mList.clear();
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 2:
                mCountSwitchArrow = true;
                mIvFqCount.startAnimation(rotate);
                mRlRecyclerView.setVisibility(View.VISIBLE);
                mList.addAll(mHbCountList);
                mAdapter.setSelected(mHbCount);
                break;
            case 3:
                mPartSwitchArrow = true;
                mIvFreePart.startAnimation(rotate);
                mRlRecyclerView.setVisibility(View.VISIBLE);
                mList.addAll(mHbFreePartList);
                mAdapter.setSelected(mHbFreePart);
                break;
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //此处可以正常获取getLeft()等
        //监听页面布局改变
        mPointContainer.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mSpace = mPointContainer.getChildAt(1).getLeft() - mPointContainer.getChildAt(0).getLeft();
                        mPointContainer.getViewTreeObserver()
                                .removeGlobalOnLayoutListener(this);
                    }
                });
    }

    private class PagerListener
            implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //滑动时的回调
            //     mSelectedPoint 设置他的marginLeft

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mSelectedPoint.getLayoutParams();
            //计算两点间的距离
            //            int space = DensityUtils.dp2px(GuideUI.this, 20);
            params.leftMargin = (int) (mSpace * position + mSpace * positionOffset + 0.5f);
            mSelectedPoint.setLayoutParams(params);
        }

        @Override
        public void onPageSelected(int position) {
            //页面选中时的回调
//            mPointContainer.setVisibility(position == (PICS.length - 1) ? View.GONE : View.VISIBLE);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            //滑动状态改变时的回调
        }
    }

    private class GuideAdapter
            extends PagerAdapter {

        @Override
        public int getCount() {
            if (mViews != null) {
                return mViews.size();
            }
            return 0;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mViews.get(position);
            //添加到容器中
            container.addView(view);
            //返回标记
            return view;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    /**
     * 应用ID列表查询
     */
    private void querySvcAppList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).querySvcAppList(MyApplication.getContext(), "TAG_HB_SVC_APP_LIST", null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 应用开通申请
     */
    private void openFunction() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("svcAppId", MyApplication.getHbAppId());
            ServerClient.newInstance(MyApplication.getContext()).openFunction(MyApplication.getContext(), "TAG_HB_OPEN", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 应用开通状态查询
     */
    private void haveOpenSvcApp(boolean isShowDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (isShowDialog)
                DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("appCode", "ALIPAY-ISTALLMENT");
            ServerClient.newInstance(MyApplication.getContext()).haveOpenSvcApp(MyApplication.getContext(), "TAG_HB_OPEN_STATUS", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_HBFQ_COLLECT)) {
            setRefreshState(500);
            DialogUtil.safeCloseDialog(mLoadDialog);
            HbfqDataBean msg = (HbfqDataBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg != null && msg.getError() != null) {
                        showCommonNoticeDialog(HBDataActivity.this, msg.getError().getMessage());
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mSvContent.setVisibility(View.VISIBLE);
                    if (msg.getData() != null) {
                        mTvMoney.setText(DateUtil.formatMoneyByLong(msg.getData().getTradeMoney()));
                        mTvCount.setText(msg.getData().getTradeCount());
                    } else {
                        mTvMoney.setText(R.string.tv_money_zero);
                        mTvCount.setText("0");
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_HB_SVC_APP_LIST")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SvcAppListBean msg = (SvcAppListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(HBDataActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        for (int i = 0; i < msg.getData().size(); i++) {
                            if ("CASH-POINT".equals(msg.getData().get(i).getAppCode())) {
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_CASH_POINT, msg.getData().get(i).getId());
                            }
                            if ("ALIPAY-ISTALLMENT".equals(msg.getData().get(i).getAppCode())) {
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_ALIPAY_ISTALLMENT, msg.getData().get(i).getId());
                                openFunction();
                            }
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_HB_OPEN")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                        if (msg.getError().getMessage() != null) {
                            getLoginDialog(HBDataActivity.this, msg.getError().getMessage());
                        }
                    } else {
                        if (msg.getError().getMessage() != null) {
                            MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(this, "开通申请成功");
                    setView("1");
                    mLlNoticeDetail.setVisibility(View.GONE);
                    break;
            }
        } else if (event.getTag().equals("TAG_HB_OPEN_STATUS")) {
            setRefreshState(500);
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                        if (msg.getError().getMessage() != null) {
                            getLoginDialog(HBDataActivity.this, msg.getError().getMessage());
                        }
                    } else {
                        if (msg.getError().getMessage() != null) {
                            MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_ALIPAY_ISTALLMENT_OPEN, msg.getData().getServiceStatus());
                        setView(MyApplication.getHbOpenStatus());
                        if (!TextUtils.isEmpty(msg.getData().getExamineRemark())) {
                            mLlNoticeDetail.setVisibility(View.VISIBLE);
                            mTvNoticeDetail.setText("审核不通过，原因：" + msg.getData().getExamineRemark());
                        } else {
                            mLlNoticeDetail.setVisibility(View.GONE);
                        }
                        mSvContent.setVisibility(View.VISIBLE);
                    } else {
                        ToastUtil.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
    }

    private void setRefreshState(long delay) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.finishRefresh();
            }
        }, delay);
    }

    private void setView(String status) {
        if (!TextUtils.isEmpty(status)) {
            switch (status) {
                case "0"://未开通
                    mTvHbOpen.setText("未开通");
                    mTvHbOpen.setVisibility(View.VISIBLE);
                    mTvOpen.setVisibility(MyApplication.getIsMerchant() ? View.VISIBLE : View.GONE);
                    break;
                case "1"://开通中
                    mTvHbOpen.setText("审核中");
                    mTvHbOpen.setVisibility(View.VISIBLE);
                    mTvOpen.setVisibility(View.GONE);
                    break;
                case "2"://已开通
                    mTvHbOpen.setText("已开通");
                    mTvHbOpen.setVisibility(View.VISIBLE);
                    mTvOpen.setVisibility(View.GONE);
                    break;
                default:
                    mTvHbOpen.setVisibility(View.GONE);
                    mTvOpen.setVisibility(View.GONE);
                    break;
            }
        }
    }
}
