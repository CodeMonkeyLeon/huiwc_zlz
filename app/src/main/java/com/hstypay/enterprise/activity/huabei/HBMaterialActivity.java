package com.hstypay.enterprise.activity.huabei;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

public class HBMaterialActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hb_material);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mBtnSubmit = findViewById(R.id.btn_submit);

        mTvTitle.setText(R.string.title_hbfq_material);
        setButtonEnable(mBtnSubmit, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    public void initData() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                Resources res = this.getResources();
                BitmapDrawable d = (BitmapDrawable) res.getDrawable(R.mipmap.img_huabei_poster);
                Bitmap bitmap = d.getBitmap();
                ScreenUtils.saveImageToGallery(this, bitmap);
                showCommonNoticeDialog(HBMaterialActivity.this, getString(R.string.dialog_copy_picture));
                break;
            default:
                break;
        }
    }
}
