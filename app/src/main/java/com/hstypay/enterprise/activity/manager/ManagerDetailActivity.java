package com.hstypay.enterprise.activity.manager;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.BindShopActivity;
import com.hstypay.enterprise.activity.ChangeNameActivity;
import com.hstypay.enterprise.activity.ResetPasswordActivity;
import com.hstypay.enterprise.activity.storeCode.StoreCodeListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierDetailBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.ManagerDetailBean;
import com.hstypay.enterprise.bean.StoresBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/24 10:09
 * @描述: 店长详情
 */
public class ManagerDetailActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private ImageView mIvBack, mIvForbidden, mIvStoreArrow;
    private Button mBtnSubmit;
    private Button mBtnResetPwd;
    private TextView mButton, mTvTitle, mTvManagerName, mTvCardID, mTvTel, mTvManagerStore, mTvForbiddenTitle, mTvEmpRole;
    private ImageView iv_pic_two, iv_pic_three;
    private RelativeLayout mRlManagerStore, mRlPhoto1, mRlPhoto2, mRlStaticCode, mRlManagerName;
    private LinearLayout mLlManagerCard, mLlManagerCardId;
    private boolean isEnable = false;
    private TreeMap<String, String> mStoreMap = new TreeMap<>();
    private TreeMap<String, String> mBindStore = new TreeMap<>();
    private List<String> mList = new ArrayList<>();
    private ManagerDetailBean.DataEntity mData;
    private SelectDialog mSelectDialog;
    private List<StoresBean> mStoresBean = new ArrayList<>();
    private SafeDialog mLoadDialog;
    private CheckBox mCbRefund;
    private TextView mTvActiveNotice;
    private LinearLayout mLlRefundPermission;
    private boolean isVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_info);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton.setVisibility(View.INVISIBLE);
        mTvTitle.setText(R.string.title_emp_detail);

        mRlManagerName = findViewById(R.id.rl_manager_name);
        mLlManagerCard = (LinearLayout) findViewById(R.id.ll_manager_card);
        mLlManagerCardId = (LinearLayout) findViewById(R.id.ll_manager_card_id);
        mRlPhoto1 = (RelativeLayout) findViewById(R.id.ly_pic_two);
        iv_pic_two = (ImageView) findViewById(R.id.iv_pic_two);
        mRlPhoto2 = (RelativeLayout) findViewById(R.id.ly_pic_three);
        iv_pic_three = (ImageView) findViewById(R.id.iv_pic_three);
        mTvManagerName = (TextView) findViewById(R.id.tv_manager_name);
        mTvCardID = (TextView) findViewById(R.id.tv_manager_card_id);
        mTvTel = (TextView) findViewById(R.id.tv_manager_tel);
        mTvEmpRole = (TextView) findViewById(R.id.tv_emp_role);
        mRlManagerStore = (RelativeLayout) findViewById(R.id.rl_manager_store);
        mTvForbiddenTitle = (TextView) findViewById(R.id.tv_forbidden_title);
        mTvManagerStore = (TextView) findViewById(R.id.tv_manager_store);
        mIvStoreArrow = (ImageView) findViewById(R.id.iv_open_forward);
        mRlStaticCode = (RelativeLayout) findViewById(R.id.rl_static_code);
        mIvForbidden = (ImageView) findViewById(R.id.iv_forbidden_switch);

        mCbRefund = findViewById(R.id.cb_refund);
        mTvActiveNotice = (TextView) findViewById(R.id.tv_active_notice);
        mLlRefundPermission = (LinearLayout) findViewById(R.id.ll_refund_permission);
        mBtnResetPwd = (Button) findViewById(R.id.btn_reset_pwd);
        mBtnSubmit = (Button) findViewById(R.id.btn_delete);

    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mIvForbidden.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mBtnResetPwd.setOnClickListener(this);
        mRlManagerStore.setOnClickListener(this);
        mRlStaticCode.setOnClickListener(this);
        mRlManagerName.setOnClickListener(this);
        mCbRefund.setOnCheckedChangeListener(this);
    }

    public void initData() {
        mData = (ManagerDetailBean.DataEntity) getIntent().getExtras().getSerializable(Constants.REQUEST_MANAGER_INTENT);
        if (mData != null) {
            /*if (StringUtils.isEmptyOrNull(mData.getIdCardFrontPhoto()) && StringUtils.isEmptyOrNull(mData.getIdCardBackPhoto())) {
                mLlManagerCard.setVisibility(View.GONE);
            } else {
                mLlManagerCard.setVisibility(View.VISIBLE);
                if (!StringUtils.isEmptyOrNull(mData.getIdCardFrontPhoto())) {
                    mRlPhoto1.setVisibility(View.VISIBLE);
                    Picasso.get().load(Constants.H5_BASE_URL + mData.getIdCardFrontPhoto()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(iv_pic_two);
                } else {
                    mRlPhoto1.setVisibility(View.GONE);
                }
                if (!StringUtils.isEmptyOrNull(mData.getIdCardBackPhoto())) {
                    mRlPhoto2.setVisibility(View.VISIBLE);
                    Picasso.get().load(Constants.H5_BASE_URL + mData.getIdCardBackPhoto()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(iv_pic_three);
                } else {
                    mRlPhoto2.setVisibility(View.GONE);
                }
            }*/
            mTvActiveNotice.setVisibility(mData.getActivateStatus() != 1 ? View.VISIBLE : View.GONE);
            mLlRefundPermission.setVisibility(mData.isRoleApplyRefund() ? View.VISIBLE : View.GONE);
            mTvEmpRole.setText("店长");
            mTvManagerName.setText(mData.getRealName());
            if (!TextUtils.isEmpty(mData.getIdCode())) {
                mLlManagerCardId.setVisibility(View.VISIBLE);
                mTvCardID.setText(mData.getIdCode());
            } else {
                mLlManagerCardId.setVisibility(View.GONE);
            }
            mTvTel.setText(mData.getTelphone());

            List<StoresBean> stores = mData.getStores();
            if (stores != null && stores.size() > 0) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < stores.size(); i++) {
                    mList.add(stores.get(i).getStoreId());
                    mStoreMap.put(stores.get(i).getStoreId(), stores.get(i).getStoreName());
                    mBindStore.put(stores.get(i).getStoreId(), stores.get(i).getStoreName());
                    if (i == stores.size() - 1) {
                        builder.append(stores.get(i).getStoreName());
                    } else {
                        builder.append(stores.get(i).getStoreName()).append("/");
                    }
                    mStoresBean.clear();
                    StoresBean storesBean = new StoresBean();
                    storesBean.setStoreId(stores.get(i).getStoreId());
                    storesBean.setStoreName(stores.get(i).getStoreName());
                    mStoresBean.add(storesBean);
                }
                if (StringUtils.getStrLength(builder.toString()) > 26) {
                    mTvManagerStore.setText(StringUtils.splitStr(builder.toString(), 24) + "...");
                } else {
                    mTvManagerStore.setText(builder.toString());
                }
            }
            mCbRefund.setChecked(mData.isApplyRefund());
            if (mData.isEnabled()) {
                mIvForbidden.setImageResource(R.mipmap.ic_switch_open);
                isEnable = true;
            } else {
                mIvForbidden.setImageResource(R.mipmap.ic_switch_close);
                isEnable = false;
            }
            setView(mData.getEditEnable());
        }
    }

    private void setView(int editEnable) {
        if (editEnable == 0) {
            mIvForbidden.setEnabled(false);
            mRlManagerStore.setEnabled(false);
            mCbRefund.setEnabled(false);
            mTvManagerStore.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvForbiddenTitle.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvEmpRole.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mTvTel.setTextColor(UIUtils.getColor(R.color.home_value_text));
            mIvStoreArrow.setVisibility(View.INVISIBLE);
            mBtnSubmit.setVisibility(View.GONE);
        } else {
            mIvForbidden.setEnabled(true);
            mRlManagerStore.setEnabled(true);
            mCbRefund.setEnabled(true);
            mTvManagerStore.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvForbiddenTitle.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvEmpRole.setTextColor(UIUtils.getColor(R.color.home_text));
            mTvTel.setTextColor(UIUtils.getColor(R.color.home_text));
            mIvStoreArrow.setVisibility(View.VISIBLE);
            mBtnSubmit.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_manager_store:
                Intent intent = new Intent(this, BindShopActivity.class);
                intent.putExtra(Constants.RESULT_EDIT_MANAGER, mStoreMap);
                intent.putExtra(Constants.REQUEST_MANAGER_BIND_STORE, mBindStore);
                startActivityForResult(intent, Constants.REQUEST_EDIT_MANAGER);
                break;
            case R.id.iv_forbidden_switch:
                //退款权限
                setForbiddenView();
                break;
            case R.id.btn_delete:
                showDeleteDialog();
                break;
            case R.id.rl_static_code:
                Intent intentCode = new Intent(this, StoreCodeListActivity.class);
                intentCode.putExtra(Constants.INTENT_USER_ID, mData.getUserId());
                startActivity(intentCode);
                break;
            case R.id.rl_manager_name:
                Intent intentName = new Intent(this, ChangeNameActivity.class);
                intentName.putExtra(Constants.INTENT_CHANGE_NAME, mTvManagerName.getText().toString().trim());
                startActivityForResult(intentName, Constants.REQUEST_CHANGE_NAME);
                break;
            case R.id.btn_reset_pwd:
                resetPassword(mData.getUserId());
                break;
            default:
                break;
        }
    }

    private void resetPassword(String phone) {
        Intent intent = new Intent(ManagerDetailActivity.this, ResetPasswordActivity.class);
        intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, phone);
        intent.putExtra(Constants.INTENT_NAME, "INTENT_EMP_RESET_PWD");
        startActivity(intent);
    }

    public void showDeleteDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(ManagerDetailActivity.this
                    , getString(R.string.dialog_delete_notice)
                    , getString(R.string.btnCancel)
                    , getString(R.string.ensure_check), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    delete();
                }
            });
            DialogHelper.resize(ManagerDetailActivity.this, mSelectDialog);
        }
        mSelectDialog.show();
    }

    private void submit() {
        if (!NetworkUtils.isNetworkAvailable(ManagerDetailActivity.this)) {
            showCommonNoticeDialog(ManagerDetailActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("userId", mData.getUserId());
            map.put("realName", mTvManagerName.getText().toString().trim());
            map.put("storeIds", mList);
            ServerClient.newInstance(ManagerDetailActivity.this).editManager(ManagerDetailActivity.this, Constants.TAG_EDIT_MANAGER, map);
        }
    }

    private void delete() {
        if (!NetworkUtils.isNetworkAvailable(ManagerDetailActivity.this)) {
            showCommonNoticeDialog(ManagerDetailActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("userId", mData.getUserId());
            map.put("storeIds", mList);
            ServerClient.newInstance(ManagerDetailActivity.this).deleteManager(ManagerDetailActivity.this, Constants.TAG_DELETE_MANAGER, map);
        }
    }

    private void setForbiddenView() {
        if (isEnable) {
            mIvForbidden.setImageResource(R.mipmap.ic_switch_close);
            isEnable = false;
            setForbidden(mData.getUserId(), isEnable);//开关关闭
        } else {
            mIvForbidden.setImageResource(R.mipmap.ic_switch_open);
            isEnable = true;
            setForbidden(mData.getUserId(), isEnable);//开关开启
        }
    }

    private void setForbidden(String userId, boolean enabled) {
        if (!NetworkUtils.isNetworkAvailable(ManagerDetailActivity.this)) {
            showCommonNoticeDialog(ManagerDetailActivity.this, getString(R.string.network_exception));
        } else {
            setResult(RESULT_OK);
            Map<String, Object> map = new HashMap<>();
            map.put("enabled", enabled);
            map.put("userId", userId);
            ServerClient.newInstance(ManagerDetailActivity.this).freezeManager(ManagerDetailActivity.this, Constants.TAG_FREEZE_MANAGER, map);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_EDIT_MANAGER && resultCode == RESULT_OK) {
            mStoreMap = new TreeMap<>((HashMap) data.getSerializableExtra("data"));
            mBindStore.clear();
            mBindStore.putAll(mStoreMap);
            mList.clear();
            mStoresBean.clear();
            StringBuilder builder = new StringBuilder();
            if (mStoreMap != null && mStoreMap.size() > 0) {
                for (String storeId : mStoreMap.keySet()) {
                    mList.add(storeId);
                    String storeName = mStoreMap.get(storeId);
                    if (storeId.equals(mStoreMap.lastKey())) {
                        builder.append(storeName);
                    } else {
                        builder.append(storeName).append("/");
                    }
                    StoresBean storesBean = new StoresBean();
                    storesBean.setStoreId(storeId);
                    storesBean.setStoreName(storeName);
                    mStoresBean.add(storesBean);
                }
            }
            mTvManagerStore.setText(builder.toString());
            submit();
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHANGE_NAME) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                String name = extras.getString(Constants.RESULT_CHANGE_NAME);
                mTvManagerName.setText(name);
                submit();
            }
        }
    }

    //Eventbus接收数据，退款权限返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_EDIT_MANAGER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_CASHIER_EDIT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ManagerDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ManagerDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
//                    showCommonNoticeDialog(ManagerDetailActivity.this, getString(R.string.edit_info_success));
                    mData.setRealName(mTvManagerName.getText().toString().trim());
                    mData.setStores(mStoresBean);
                    setResult(RESULT_OK);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_DELETE_MANAGER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ManagerDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ManagerDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(ManagerDetailActivity.this, getString(R.string.delete_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    break;
            }
        } else if (event.getTag().equals("TAG_MANAGER_PERMISSION")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierDetailBean msg = (CashierDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ManagerDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ManagerDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private String getStoreName(List<StoresBean> stores) {
        if (stores == null || stores.size() == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stores.size(); i++) {
            if (i == stores.size() - 1) {
                sb.append(stores.get(i).getStoreName());
            } else {
                sb.append(stores.get(i).getStoreName() + "/");
            }
        }
        return sb.toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_refund:
                if (isVisible)
                    getPermission("APPLY-REFUND", mData.getUserId(), mCbRefund);
                break;
        }
    }

    private void getPermission(String opCode, String userId, CheckBox checkBox) {
        if (!NetworkUtils.isNetworkAvailable(ManagerDetailActivity.this)) {
            showCommonNoticeDialog(ManagerDetailActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("opCode", opCode);
            map.put("userId", userId);
            if (checkBox.isChecked()) {
                map.put("openFlag", "1");
            } else {
                map.put("openFlag", "0");
            }
            ServerClient.newInstance(ManagerDetailActivity.this).cashierPermission(ManagerDetailActivity.this, "TAG_MANAGER_PERMISSION", map);
        }
    }

    /*private void finishActivity(String intentName) {
        Intent intent = new Intent();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(Constants.RESULT_MANAGER_DELETE, mData);
        mBundle.putString(Constants.INTENT_NAME, intentName);
        intent.putExtras(mBundle);
        setResult(RESULT_OK, intent);
        finish();
    }*/

    /*@Override
    public void onBackPressed() {
        finishActivity(Constants.INTENT_FINISH);
        super.onBackPressed();
    }*/
}
