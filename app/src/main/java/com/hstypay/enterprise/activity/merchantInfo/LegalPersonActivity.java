package com.hstypay.enterprise.activity.merchantInfo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.ChoiceDialog;
import com.hstypay.enterprise.Widget.LegalDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.Widget.TypeDialog;
import com.hstypay.enterprise.activity.bankcard.ChangeCompanyCardActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ChoiceBean;
import com.hstypay.enterprise.bean.ImageBean;
import com.hstypay.enterprise.bean.MerchantInfoBean;
import com.hstypay.enterprise.bean.QueryEnabledChangeBean;
import com.hstypay.enterprise.bean.SendMsgBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.ImageFactory;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

//法人信息
public class LegalPersonActivity extends BaseActivity implements View.OnClickListener {
    private MerchantInfoBean.DataBean mData;
    private ImageView iv_back;
    private ImageView iv_legal_name_delete;
    private EditText et_legal_name;
    private TextView tv_merchant_type;
    private ImageView iv_id_card_delete;
    private EditText et_id_card;
    private RelativeLayout rl_legal_valid;
    private TextView tv_legal_valid;
    private LinearLayout ll_choice;
    private ImageView iv_choice_contact;
    private ImageView iv_contact_name_delete;
    private EditText et_contact_name;
    private RelativeLayout rl_contact_id;
    private ImageView iv_contact_id_delete;
    private EditText et_contact_id;
    private RelativeLayout rl_contace_valid;
    private TextView tv_contact_valid;
    private Button btn_submit;
    private LegalDialog legalDialog;
    private TimePickerView pvTime;
    private boolean isChoice = false;
    private ImageView iv_legal_id_icon;
    private ImageView iv_legal_valid_icon;
    private TextView tv_contact_id_type;
    private RelativeLayout rl_merchant_type;
    private ImageView iv_legal_valid_icon1;
    private ImageView iv_contact_valid_icon;
    private TypeDialog typeDialog;
    private LinearLayout ll_license;
    private EditText et_license_id;
    private ImageView iv_license_delete;
    private RelativeLayout rl_license_valid_type;
    private TextView tv_license_valid_type;
    private ImageView iv_license_valid_icon;
    private TextView tv_license_date;
    private TextView tv_license_to;
    private TextView tv_license_valid_date;
    private TextView tv_license_valid_end;
    private int choiceDate = 0;
    private TextView tv_legal_date;
    private TextView tv_legal_start_date;
    private TextView tv_legal_to;
    private TextView tv_legal_end_date;
    private TextView tv_contact_date;
    private TextView tv_contact_start_date;
    private TextView tv_contact_to;
    private TextView tv_contact_end_date;
    private ImageView iv_contact_date_icon;
    private ImageView iv_license_date_icon;
    private ImageView iv_legal_date_icon;
    private SafeDialog mLoadDialog;
    private LinearLayout ll_legal_title;
    private int legalType;
    private boolean legalvalidType;
    private String startTime;
    private String endTime;
    private int idCardType;
    private boolean idCardValid;

    private String idCardStartTime;
    private String idCardEndTime;
    private ChoiceDialog legalIdDialog;

    private ImageView iv_camera_legal, iv_camera_contact;
    private int mOcrType;
    private String picSixPath;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private String mMerchantId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_person);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        mData = (MerchantInfoBean.DataBean) getIntent().getSerializableExtra("data");
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("法人信息");
        iv_legal_name_delete = findViewById(R.id.iv_legal_name_delete);
        et_legal_name = findViewById(R.id.et_legal_name);//法人姓名
        rl_merchant_type = findViewById(R.id.rl_merchant_type);
        tv_merchant_type = findViewById(R.id.tv_merchant_type);//证件类型
        iv_id_card_delete = findViewById(R.id.iv_id_card_delete);
        et_id_card = findViewById(R.id.et_id_card);//证件号码
        rl_legal_valid = findViewById(R.id.rl_legal_valid);
        tv_legal_valid = findViewById(R.id.tv_legal_valid);//法人有效期
        ll_choice = findViewById(R.id.ll_choice);//选择与联系人一致
        iv_choice_contact = findViewById(R.id.iv_choice_contact);
        iv_contact_name_delete = findViewById(R.id.iv_contact_name_delete);
        et_contact_name = findViewById(R.id.et_contact_name);//联系人姓名
        rl_contact_id = findViewById(R.id.rl_contact_id);//联系人证件类型
        iv_contact_id_delete = findViewById(R.id.iv_contact_id_delete);
        et_contact_id = findViewById(R.id.et_contact_id);//联系人证件号码
        rl_contace_valid = findViewById(R.id.rl_contace_valid);
        tv_contact_valid = findViewById(R.id.tv_contact_valid);//联系人有效期
        btn_submit = findViewById(R.id.btn_submit);
        iv_legal_id_icon = findViewById(R.id.iv_legal_id_icon);
        iv_legal_valid_icon = findViewById(R.id.iv_legal_valid_icon);
        tv_contact_id_type = findViewById(R.id.tv_contact_id_type);
        iv_legal_valid_icon1 = findViewById(R.id.iv_legal_valid_icon);
        iv_contact_valid_icon = findViewById(R.id.iv_contact_valid_icon);
        ll_legal_title = findViewById(R.id.ll_legal_title);

        ll_license = findViewById(R.id.ll_license);//营业执照
        et_license_id = findViewById(R.id.et_license_id);//营业执照编号
        iv_license_delete = findViewById(R.id.iv_license_delete);
        rl_license_valid_type = findViewById(R.id.rl_license_valid_type);//营业执照有效期类型
        tv_license_valid_type = findViewById(R.id.tv_license_valid_type);
        iv_license_valid_icon = findViewById(R.id.iv_license_valid_icon);
        tv_license_date = findViewById(R.id.tv_license_date);//证件日期
        //证件开始日期
        tv_license_valid_date = findViewById(R.id.tv_license_valid_date);
        tv_license_to = findViewById(R.id.tv_license_to);
        //证件结束日期
        tv_license_valid_end = findViewById(R.id.tv_license_valid_end);
        tv_legal_date = findViewById(R.id.tv_legal_date);//法人日期
        tv_legal_start_date = findViewById(R.id.tv_legal_start_date);//法人开始日期
        tv_legal_to = findViewById(R.id.tv_legal_to);
        tv_legal_end_date = findViewById(R.id.tv_legal_end_date);//法人结束日期

        tv_contact_date = findViewById(R.id.tv_contact_date);//联系人日期
        tv_contact_start_date = findViewById(R.id.tv_contact_start_date);//联系人开始日期
        tv_contact_to = findViewById(R.id.tv_contact_to);
        tv_contact_end_date = findViewById(R.id.tv_contact_end_date);//联系人结束日期
        iv_contact_date_icon = findViewById(R.id.iv_contact_date_icon);
        iv_license_date_icon = findViewById(R.id.iv_license_date_icon);
        iv_legal_date_icon = findViewById(R.id.iv_legal_date_icon);
        iv_camera_legal = findViewById(R.id.iv_camera_legal);
        iv_camera_contact = findViewById(R.id.iv_camera_contact);

        initTimePicker();

        mLoadDialog = LegalPersonActivity.this.getLoadDialog(LegalPersonActivity.this, UIUtils.getString(R.string.public_loading), false);
        setButtonEnable(btn_submit, true);
    }

    private void initEvent() {
        iv_back.setOnClickListener(this);
        iv_legal_name_delete.setOnClickListener(this);
        iv_id_card_delete.setOnClickListener(this);
        rl_legal_valid.setOnClickListener(this);
        ll_choice.setOnClickListener(this);
        iv_contact_name_delete.setOnClickListener(this);
        iv_contact_id_delete.setOnClickListener(this);
        rl_contace_valid.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        rl_license_valid_type.setOnClickListener(this);
        tv_license_valid_date.setOnClickListener(this);
        iv_license_delete.setOnClickListener(this);
        tv_license_valid_end.setOnClickListener(this);
        tv_legal_start_date.setOnClickListener(this);
        tv_legal_end_date.setOnClickListener(this);
        tv_contact_start_date.setOnClickListener(this);
        tv_contact_end_date.setOnClickListener(this);
        rl_merchant_type.setOnClickListener(this);
        rl_contact_id.setOnClickListener(this);
        iv_camera_legal.setOnClickListener(this);
        iv_camera_contact.setOnClickListener(this);
    }

    private void initData() {
        if (mData != null) {
            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
            if (mchDetail != null) {
                if (!StringUtils.isEmptyOrNull(mchDetail.getPrincipal())) {
                    et_legal_name.setText(mchDetail.getPrincipal());
                }

                if (!StringUtils.isEmptyOrNull(mchDetail.getIdCode())) {
                    //et_id_card.setText(DateUtil.idString(mchDetail.getIdCode()));
                    et_id_card.setText(mchDetail.getIdCode());
                }
                if (mchDetail.isIdCodeValidFlag()) {
                    //法人证件长期有效
                    tv_legal_valid.setText("长期");
                    tv_legal_date.setText("证件开始日期");
                    tv_legal_to.setVisibility(View.GONE);
                    tv_legal_end_date.setVisibility(View.GONE);
                    legalvalidType = true;

                } else {
                    //法人证件非长期
                    tv_legal_valid.setText("非长期");
                    tv_legal_date.setText("证件有效期");
                    legalvalidType = false;

                }
                switch (mchDetail.getIdCodeType()) {

                    case 1:
                        //身份证
                        tv_merchant_type.setText("身份证");
                        legalType = 1;
                        break;
                    case 2:
                        //护照
                        tv_merchant_type.setText("护照");
                        legalType = 2;
                        break;
                    case 3:
                        //港澳居民来往内地通行证
                        tv_merchant_type.setText("港澳居民来往内地通行证");
                        legalType = 3;
                        break;
                    case 4:
                        //台湾居民来往内地通行证
                        tv_merchant_type.setText("台湾居民来往内地通行证");
                        legalType = 4;
                        break;
                    default:
                        //其它
                        tv_merchant_type.setText("其它");
                        legalType = 99;
                        break;
                }

                if (mchDetail.getIdCodeValidBegin() != null) {
                    tv_legal_start_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getIdCodeValidBegin()));
                    startTime = mchDetail.getIdCodeValidBegin();
                }

                if (mchDetail.getIdCodeValidEnd() != null) {
                    tv_legal_end_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getIdCodeValidEnd()));
                    endTime = mchDetail.getIdCodeValidEnd();
                }

                //联系人
                if (mchDetail.getContactPersonName() != null) {
                    et_contact_name.setText(mchDetail.getContactPersonName());
                }

                switch (mchDetail.getIdCardType()) {
                    case 1:
                        //身份证
                        tv_contact_id_type.setText("身份证");
                        idCardType = 1;
                        break;
                    case 2:
                        //护照
                        tv_contact_id_type.setText("护照");
                        idCardType = 2;
                        break;
                    case 3:
                        //港澳居民来往内地通行证
                        tv_contact_id_type.setText("港澳居民来往内地通行证");
                        idCardType = 3;
                        break;
                    case 4:
                        //台湾居民来往内地通行证
                        tv_contact_id_type.setText("台湾居民来往内地通行证");
                        idCardType = 4;
                        break;
                    default:
                        //其它
                        tv_contact_id_type.setText("其它");
                        idCardType = 99;
                        break;
                }

                if (mchDetail.getIdCard() != null) {
                    //et_contact_id.setText(DateUtil.idString(mchDetail.getIdCard()));
                    et_contact_id.setText(mchDetail.getIdCard());
                }

                if (mchDetail.isIdCardValidFlag()) {
                    //长期有效
                    tv_contact_valid.setText("长期");
                    tv_contact_date.setText("证件开始日期");
                    tv_contact_to.setVisibility(View.GONE);
                    tv_contact_end_date.setVisibility(View.GONE);
                    idCardValid = true;
                } else {
                    //非长期
                    tv_contact_valid.setText("非长期");
                    tv_contact_date.setText("证件有效期");
                    idCardValid = false;
                }

                if (mchDetail.getIdCardValidBegin() != null) {

                    tv_contact_start_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getIdCardValidBegin()));
                    idCardStartTime = mchDetail.getIdCardValidBegin();
                }

                if (mchDetail.getIdCardValidEnd() != null) {

                    tv_contact_end_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getIdCardValidEnd()));
                    idCardEndTime = mchDetail.getIdCardValidEnd();
                }

            }

            switch (mData.getMchQueryStatus()) {

                case -1:
                    //初始商户
                    setExitEable(false);
                    break;
               /* case 1:
                    //审核通过
                    if (mData.getAttestationStatus() != 1 || mData.getTradeStatus() == 0) {
                        setExitEable(false);
                    } else if (isChangeInfo()) {
                        queryEnabledChange();
                    }
                    break;*/
                case 5:
                    //作废商户
                    setExitEable(false);
                    break;
               /* case 10:
                    //变更审核通过
                    setExitEable(false);
                    break;*/
                case 101:
                    //冻结商户
                    setExitEable(false);
                    break;
                //case 3://修改待确认
                default:
                    if (isChangeInfo()) {
                        queryEnabledChange();
                    }
                    break;
            }

        }

        setEdittextListener(et_legal_name, iv_legal_name_delete);
        //setEdittextListener(et_id_card, iv_id_card_delete);
        setEdittextListener(et_contact_name, iv_contact_name_delete);
        // setEdittextListener(et_contact_id, iv_contact_id_delete);
        setEdittextListener(et_license_id, iv_license_delete);


        et_id_card.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    iv_id_card_delete.setVisibility(View.VISIBLE);
                } else {
                    iv_id_card_delete.setVisibility(View.INVISIBLE);
                }
            }
        });
        et_id_card.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点

                    if (et_id_card.getText().length() > 0) {
                        iv_id_card_delete.setVisibility(View.VISIBLE);
                    } else {
                        iv_id_card_delete.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    iv_id_card_delete.setVisibility(View.INVISIBLE);
                }
            }
        });


        et_contact_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    iv_contact_id_delete.setVisibility(View.VISIBLE);
                } else {
                    iv_contact_id_delete.setVisibility(View.INVISIBLE);
                }
            }
        });
        et_contact_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点

                    if (et_contact_id.getText().length() > 0) {
                        iv_contact_id_delete.setVisibility(View.VISIBLE);
                    } else {
                        iv_contact_id_delete.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    iv_contact_id_delete.setVisibility(View.INVISIBLE);
                }
            }
        });


    }

    //是否是变更审核
    private boolean isChangeInfo() {
        if (mData == null) {
            return false;
        }
        /*if ((mData.getTradeStatus() == Constants.HPMerchantTradeAvailable || mData.getTradeStatus() == Constants.HPMerchantTradeSomeAvailable)
                && mData.getAttestationStatus() == Constants.HPMerchantAuthenticateStatusPass) {
            return true;
        }*/
        if (mData.getMchQueryStatus() == 1 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
            //case 1://审核通过
            //case 100://可交易
            //case 10://变更审核通过
            return true;
        }
        return false;
    }

    //验证商户资料是否允许修改
    private void queryEnabledChange() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            DialogUtil.safeShowDialog(mLoadDialog);
            map.put("merchantId", mMerchantId);
            ServerClient.newInstance(MyApplication.getContext()).queryEnabledChange(MyApplication.getContext(), Constants.TAG_QUERY_ENABLED_CHANGE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void setExitEable(boolean tag) {

        if (!tag) {

            btn_submit.setVisibility(View.GONE);
            et_legal_name.setEnabled(false);
            rl_merchant_type.setEnabled(false);
            et_id_card.setEnabled(false);
            rl_legal_valid.setEnabled(false);
            iv_legal_valid_icon1.setVisibility(View.GONE);
            ll_choice.setVisibility(View.GONE);

            et_contact_name.setEnabled(false);
            rl_contact_id.setEnabled(false);
            iv_legal_id_icon.setVisibility(View.GONE);
            rl_contace_valid.setEnabled(false);
            iv_legal_valid_icon.setVisibility(View.GONE);
            iv_contact_valid_icon.setVisibility(View.GONE);
            et_contact_id.setEnabled(false);

            et_contact_name.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_contact_id.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_id_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_valid.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_legal_name.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_merchant_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_id_card.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_legal_valid.setTextColor(getResources().getColor(R.color.tv_rb_color));


            tv_contact_start_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_start_date.setEnabled(false);
            tv_contact_end_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_end_date.setEnabled(false);
            tv_contact_to.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_license_id.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_contact_date_icon.setVisibility(View.GONE);
            et_license_id.setEnabled(false);
            iv_license_delete.setVisibility(View.GONE);
            rl_license_valid_type.setEnabled(false);
            tv_license_valid_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_license_valid_icon.setVisibility(View.GONE);
            tv_license_valid_date.setEnabled(false);
            tv_license_valid_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_to.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_valid_end.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_valid_end.setEnabled(false);
            iv_license_date_icon.setVisibility(View.GONE);
            tv_legal_start_date.setEnabled(false);
            tv_legal_start_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_legal_to.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_legal_end_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_legal_end_date.setEnabled(false);
            iv_legal_date_icon.setVisibility(View.GONE);
            iv_camera_contact.setVisibility(View.GONE);
            iv_camera_legal.setVisibility(View.GONE);
        }
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
      /*  Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -3);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));*/
        //时间选择器
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date);

                if (choiceDate > 0) {

                    switch (choiceDate) {

                        case 1:
                            //营业执照开始日期
                            tv_license_valid_date.setText(time);
                            break;
                        case 2:
                            //营业执照结束日期
                            tv_license_valid_end.setText(time);
                            break;
                        case 3:
                            //法人开始日期

                            if (!legalvalidType) {
                                if (!StringUtils.isEmptyOrNull(tv_legal_end_date.getText().toString())) {
                                    Date date2 = DateUtil.dateto(time);
                                    Date dateto = DateUtil.dateto(tv_legal_end_date.getText().toString());
                                    int compareTo = date2.compareTo(dateto);
                                    if (compareTo > 0) {
                                        ToastUtil.showToastShort("法人开始日期不能大于结束日期");
                                        return;
                                    }
                                }
                            }
                            tv_legal_start_date.setText(time);
                            startTime = time + " 00:00:00";
                            break;
                        case 4:
                            //法人结束日期
                            if (!legalvalidType) {
                                if (!StringUtils.isEmptyOrNull(tv_legal_start_date.getText().toString())) {
                                    Date date2 = DateUtil.dateto(time);
                                    Date dateto = DateUtil.dateto(tv_legal_start_date.getText().toString());
                                    int compareTo = date2.compareTo(dateto);
                                    if (compareTo <0) {
                                        ToastUtil.showToastShort("法人结束日期不能小于开始日期");
                                        return;
                                    }

                                }

                            }

                            tv_legal_end_date.setText(time);
                            endTime = time + " 00:00:00";
                            break;
                        case 5:
                            //联系人开始日期
                            if (!idCardValid) {
                                if (!StringUtils.isEmptyOrNull(tv_contact_end_date.getText().toString())) {
                                    Date date2 = DateUtil.dateto(time);
                                    Date dateto = DateUtil.dateto(tv_contact_end_date.getText().toString());
                                    int compareTo = date2.compareTo(dateto);
                                    if (compareTo >0) {
                                        ToastUtil.showToastShort("联系人开始日期不能大于结束日期");
                                        return;
                                    }
                                }
                            }
                            tv_contact_start_date.setText(time);
                            idCardStartTime = time + " 00:00:00";
                            break;
                        case 6:
                            //联系人结束日期
                            if (!idCardValid) {
                                if (!StringUtils.isEmptyOrNull(tv_contact_start_date.getText().toString())) {
                                    Date date2 = DateUtil.dateto(time);
                                    Date dateto = DateUtil.dateto(tv_contact_start_date.getText().toString());
                                    int compareTo = date2.compareTo(dateto);
                                    if (compareTo < 0) {
                                        ToastUtil.showToastShort("联系人人结束日期不能小于开始日期");
                                        return;
                                    }
                                }
                            }
                            tv_contact_end_date.setText(time);
                            idCardEndTime = time + " 00:00:00";
                            break;
                    }
                }

            }
        })
                .setLayoutRes(R.layout.layout_pickerview_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        Button btnCancel = v.findViewById(R.id.btnCancel);
                        Button btnSubmit = v.findViewById(R.id.btnSubmit);
                        btnCancel.setOnClickListener(LegalPersonActivity.this);
                        btnSubmit.setOnClickListener(LegalPersonActivity.this);
                    }
                })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                // .setRangDate(startDate, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年", "月", "日", null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_legal_name_delete:
                iv_legal_name_delete.setVisibility(View.GONE);
                et_legal_name.setText("");
                break;
            case R.id.iv_id_card_delete:
                iv_id_card_delete.setVisibility(View.GONE);
                et_id_card.setText("");
                break;
            case R.id.rl_legal_valid:
                //法人证件类型
                IDType(2);
                break;
            case R.id.ll_choice:
                //选择与法人信息保持一致
                choiceView();
                break;
            case R.id.iv_contact_name_delete:
                iv_contact_name_delete.setVisibility(View.GONE);
                et_contact_name.setText("");
                break;
            case R.id.iv_contact_id_delete:
                iv_contact_id_delete.setVisibility(View.GONE);
                et_contact_id.setText("");
                break;
            case R.id.rl_contace_valid:
                //联系人有效期类型
                IDType(3);
                break;
            case R.id.btn_submit:

                if (StringUtils.isEmptyOrNull(et_legal_name.getText().toString().trim())) {
                    ToastUtil.showToastShort("请输入法人姓名");
                    et_legal_name.setFocusable(true);
                    et_legal_name.setFocusableInTouchMode(true);
                    et_legal_name.requestFocus();
                    return;
                }

                if (et_id_card.getText().toString().trim().length() < 8) {
                    ToastUtil.showToastShort("请输入正确的法人证件号码");
                    et_id_card.setFocusable(true);
                    et_id_card.setFocusableInTouchMode(true);
                    et_id_card.requestFocus();
                    return;
                }
                if (!legalvalidType && StringUtils.isEmptyOrNull(tv_legal_end_date.getText().toString().trim())) {
                    ToastUtil.showToastShort("请选择法人证件有效期结束日期");
                    return;
                }
                if (StringUtils.isEmptyOrNull(et_contact_name.getText().toString().trim())) {
                    ToastUtil.showToastShort("请输入联系人姓名");
                    et_contact_name.setFocusable(true);
                    et_contact_name.setFocusableInTouchMode(true);
                    et_contact_name.requestFocus();
                    return;
                }
                if (et_contact_id.getText().toString().trim().length() < 8) {
                    ToastUtil.showToastShort("请输入正确的联系人证件号码");
                    et_contact_id.setFocusable(true);
                    et_contact_id.setFocusableInTouchMode(true);
                    et_contact_id.requestFocus();
                    return;
                }
                if (!idCardValid && StringUtils.isEmptyOrNull(tv_contact_end_date.getText().toString().trim())) {
                    ToastUtil.showToastShort("请选择联系人证件有效期结束日期");
                    return;
                }
                legalDialog("legal");
                break;
            case R.id.btnCancel:
                pvTime.dismiss();
                break;
            case R.id.btnSubmit:
                pvTime.returnData();
                pvTime.dismiss();
                break;
            case R.id.rl_license_valid_type:
                //营业执照有效期类型
                IDType(1);
                break;
            case R.id.tv_license_valid_date:
                //营业执照开始日期
                choiceDate = 1;
                pvTime.show();
                break;
            case R.id.tv_license_valid_end:
                //营业执照结束日期
                choiceDate = 2;
                pvTime.show();
                break;
            case R.id.iv_license_delete:
                iv_license_delete.setVisibility(View.GONE);
                et_license_id.setText("");
                break;
            case R.id.tv_legal_start_date:
                //法人开始日期
                choiceDate = 3;
                pvTime.show();
                break;
            case R.id.tv_legal_end_date:
                //法人结束日期
                choiceDate = 4;
                pvTime.show();
                break;

            case R.id.tv_contact_start_date:
                //联系人开始日期
                choiceDate = 5;
                pvTime.show();
                break;

            case R.id.tv_contact_end_date:
                //联系人结束日期
                choiceDate = 6;
                pvTime.show();
                break;
            case R.id.rl_merchant_type:
                //法人证件类型
                legalIdDialog(1);
                break;
            case R.id.rl_contact_id:
                //联系人证件类型
                legalIdDialog(2);
                break;
            case R.id.iv_camera_legal:
                //法人身份证OCR
                mOcrType = 1;
                choice();
                break;
            case R.id.iv_camera_contact:
                //联系人身份证OCR
                mOcrType = 2;
                choice();
                break;
        }

    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(LegalPersonActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    boolean results = PermissionUtils.checkPermissionArray(LegalPersonActivity.this, permissionArray);
                    if (results) {
                        startCamera();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_photo));
                    }
                } else {
                    (LegalPersonActivity.this).showCommonNoticeDialog(LegalPersonActivity.this, getString(R.string.tx_sd_pic));
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    boolean results = PermissionUtils.checkPermissionArray(LegalPersonActivity.this, permissionArray);
                    if (results) {
                        takeImg();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_photo));
                    }
                } else {
                    (LegalPersonActivity.this).showCommonNoticeDialog(LegalPersonActivity.this, getString(R.string.tx_sd_pic));
                }
            }
        });
        picPopupWindow.showAtLocation(tv_merchant_type, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    /*private void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(LegalPersonActivity.this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(LegalPersonActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(LegalPersonActivity.this, dialog);
        dialog.show();
    }

    private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(LegalPersonActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }*/

    private String imageUrl;
    private File tempFile;
    private Uri originalUri;

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";

            tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()) {
                tempFile.getParentFile().mkdirs();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(LegalPersonActivity.this,
                        Constants.FILE_PROVIDER, tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        /*Intent intent = new Intent();
        if (Build.VERSION.SDK_INT < 19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
        } else {
            intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,"image/*");
        }*/
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data == null || data.getData() == null) {
                        return;
                    }
                    String path = AppHelper.getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            picSixPath = path;
                            uploadImage(picSixPath, "identityPerImg");
                        }
                    }
                    break;
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = AppHelper.getPicPath(originalUri);
                    Bitmap bitmap_pci;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        picSixPath = pathPhoto;
                        uploadImage(picSixPath, "identityPerImg");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void uploadImage(String path, final String type) {
        if (!NetworkUtils.isNetworkAvailable(LegalPersonActivity.this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
            return;
        }
        DialogUtil.safeShowDialog(mLoadDialog);

        String url = Constants.BASE_URL + "merchant/upload/file";
        PostFormBuilder post = OkHttpUtils.post();
        String pathCompress = null;
        if (!TextUtils.isEmpty(path)) {
            //pathCompress = path.substring(0, path.lastIndexOf(".")) + "(1).jpg";
            pathCompress = AppHelper.getImageCacheDir(path);
            ImageFactory.compressPicture(path, pathCompress);
            post = post.addFile(type, type, new File(pathCompress));
        }
        final String finalPathCompress = pathCompress;
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                DialogUtil.safeCloseDialog(mLoadDialog);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(finalPathCompress);
            }

            @Override
            public void onResponse(String response, int id) {
                FileUtils.deleteFile(finalPathCompress);
                DialogUtil.safeCloseDialog(mLoadDialog);
                LogUtil.d("zhouwei", response);
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null) {
                    if (imageBean.isStatus()) {
                        ImageBean.DataEntity imageInfo = imageBean.getData();
                        if (imageInfo != null) {
                            ImageBean.DataEntity.IdCardEntity idCard = imageInfo.getIdCard();
                            if (idCard != null) {
                                String name = idCard.getName();//姓名
                                String number = idCard.getNumber();//sfz号码
                                if (mOcrType == 1) {
                                    //法人
                                    et_legal_name.setText(name);
                                    et_id_card.setText(number);
                                } else if (mOcrType == 2) {
                                    //联系人
                                    et_contact_name.setText(name);
                                    et_contact_id.setText(number);
                                }
                            }

                        }


                    } else {
                        if (imageBean.getError() != null && imageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(imageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void legalIdDialog(final int tag) {

        List<ChoiceBean> mList = new ArrayList<>();
        ChoiceBean bean = new ChoiceBean();
        bean.setId(1);
        bean.setName("身份证");
        ChoiceBean bean1 = new ChoiceBean();
        bean1.setId(2);
        bean1.setName("护照");
        ChoiceBean bean2 = new ChoiceBean();
        bean2.setId(3);
        bean2.setName("港澳居民来往内地通行证");
        ChoiceBean bean3 = new ChoiceBean();
        bean3.setId(4);
        bean3.setName("台湾居民来往内地通行证");
        mList.add(bean);
        mList.add(bean1);
        mList.add(bean2);
        mList.add(bean3);

        legalIdDialog = new ChoiceDialog(LegalPersonActivity.this, mList, new ChoiceDialog.HandleBtn() {
            @Override
            public void handleOkBtn(ChoiceBean s) {
                legalIdDialog.dismiss();

                if (s != null) {
                    tv_merchant_type.setText(s.getName());

                    switch (s.getId()) {

                        case 1:

                            if (tag == 1) {
                                //法人
                                tv_merchant_type.setText("身份证");
                                legalType = 1;
                            } else {

                                tv_contact_id_type.setText("身份证");
                                idCardType = 1;

                            }
                            break;
                        case 2:
                            if (tag == 1) {
                                tv_merchant_type.setText("护照");
                                legalType = 2;
                            } else {

                                tv_contact_id_type.setText("护照");
                                idCardType = 2;
                            }
                            break;
                        case 3:

                            if (tag == 1) {
                                legalType = 3;
                                tv_merchant_type.setText("港澳居民来往内地通行证");
                            } else {
                                tv_contact_id_type.setText("港澳居民来往内地通行证");
                                idCardType = 3;

                            }
                            break;
                        case 4:
                            if (tag == 1) {
                                tv_merchant_type.setText("台湾居民来往内地通行证");
                                legalType = 4;
                            } else {

                                tv_contact_id_type.setText("台湾居民来往内地通行证");
                                idCardType = 4;
                            }
                            break;
                    }

                }

            }
        });
        DialogHelper.resize(LegalPersonActivity.this, legalIdDialog);
        legalIdDialog.show();

    }

    private void IDType(final int type) {
        typeDialog = new TypeDialog(LegalPersonActivity.this, new TypeDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String s) {
                switch (type) {
                    case 1:
                        //营业执照有效期
                        if (!StringUtils.isEmptyOrNull(s)) {
                            if (s.equals("1")) {
                                //长期
                                tv_license_valid_type.setText("长期");
                                tv_license_date.setText("证件开始日期");
                                tv_license_to.setVisibility(View.GONE);
                                tv_license_valid_end.setVisibility(View.GONE);
                            } else {
                                tv_license_valid_type.setText("非长期");
                                tv_license_date.setText("证件有效期");
                                tv_license_to.setVisibility(View.VISIBLE);
                                tv_license_valid_end.setVisibility(View.VISIBLE);
                            }
                        }
                        break;
                    case 2:
                        //法人证件有效期
                        if (!StringUtils.isEmptyOrNull(s)) {
                            if (s.equals("1")) {
                                //长期
                                tv_legal_valid.setText("长期");
                                tv_legal_date.setText("证件开始日期");
                                tv_legal_to.setVisibility(View.GONE);
                                tv_legal_end_date.setVisibility(View.GONE);
                                legalvalidType = true;
                            } else {
                                tv_legal_valid.setText("非长期");
                                tv_legal_date.setText("证件有效期");
                                tv_legal_to.setVisibility(View.VISIBLE);
                                tv_legal_end_date.setVisibility(View.VISIBLE);
                                legalvalidType = false;
                            }
                        }
                        break;

                    case 3:
                        //联系人证件有效期
                        if (!StringUtils.isEmptyOrNull(s)) {
                            if (s.equals("1")) {
                                //长期
                                tv_contact_valid.setText("长期");
                                tv_contact_date.setText("证件开始日期");
                                tv_contact_to.setVisibility(View.GONE);
                                tv_contact_end_date.setVisibility(View.GONE);
                                idCardValid = true;
                            } else {
                                tv_contact_valid.setText("非长期");
                                tv_contact_date.setText("证件有效期");
                                tv_contact_to.setVisibility(View.VISIBLE);
                                tv_contact_end_date.setVisibility(View.VISIBLE);
                                idCardValid = false;
                            }
                        }
                        break;
                }
                typeDialog.dismiss();
            }
        });

        DialogHelper.resizeFull(LegalPersonActivity.this, typeDialog);
        typeDialog.show();

    }

    private void choiceView() {

        if (!isChoice) {
            isChoice = true;
            if (mData != null) {

                MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();

                if (mchDetail != null) {
                    if (!StringUtils.isEmptyOrNull(et_legal_name.getText().toString().trim())) {
                        et_contact_name.setText(et_legal_name.getText().toString().trim());
                    } else {

                        et_contact_name.setText("");
                    }

                    if (!StringUtils.isEmptyOrNull(et_id_card.getText().toString().trim())) {
                        // et_contact_id.setText(DateUtil.idString(mchDetail.getIdCode()));
                        et_contact_id.setText(et_id_card.getText().toString().trim());
                    } else {
                        et_contact_id.setText("");
                    }


                    if (legalvalidType) {
                        //法人证件长期有效
                        tv_contact_valid.setText("长期");
                        tv_contact_date.setText("证件开始日期");
                        tv_contact_to.setVisibility(View.GONE);
                        tv_contact_end_date.setVisibility(View.GONE);
                        idCardValid = true;

                    } else {
                        //法人证件非长期
                        tv_contact_valid.setText("非长期");
                        tv_contact_date.setText("证件有效期");
                        tv_contact_to.setVisibility(View.VISIBLE);
                        tv_contact_end_date.setVisibility(View.VISIBLE);
                        idCardValid = false;

                    }

                    switch (legalType) {

                        case 1:
                            //身份证
                            tv_contact_id_type.setText("身份证");
                            idCardType = 1;
                            break;
                        case 2:
                            //护照
                            tv_contact_id_type.setText("护照");
                            idCardType = 2;
                            break;
                        case 3:
                            //港澳居民来往内地通行证
                            tv_contact_id_type.setText("港澳居民来往内地通行证");
                            idCardType = 3;
                            break;
                        case 4:
                            //台湾居民来往内地通行证
                            tv_contact_id_type.setText("台湾居民来往内地通行证");
                            idCardType = 4;
                            break;
                        default:
                            //其它
                            tv_contact_id_type.setText("其它");
                            idCardType = 99;
                            break;
                    }


                    if (tv_legal_start_date.getText().toString().trim() != null) {

                        tv_contact_start_date.setText(tv_legal_start_date.getText().toString().trim());
                        idCardStartTime = mchDetail.getIdCodeValidBegin();
                    }

                    if (tv_legal_end_date.getText().toString().trim() != null) {

                        tv_contact_end_date.setText(tv_legal_end_date.getText().toString().trim());
                        idCardEndTime = mchDetail.getIdCodeValidEnd();
                    }
                }
                iv_choice_contact.setImageResource(R.mipmap.icon_selected);
                setEnable(false);
            }
        } else {

            isChoice = false;

            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();


            if (mchDetail != null) {
                if (!StringUtils.isEmptyOrNull(mchDetail.getContactPersonName())) {

                    et_contact_name.setText(mchDetail.getContactPersonName());
                } else {

                    et_contact_name.setText("");
                }

                switch (idCardType) {

                    case 1:
                        //身份证
                        tv_contact_id_type.setText("身份证");
                        idCardType = 1;
                        break;
                    case 2:
                        //护照
                        tv_contact_id_type.setText("护照");
                        idCardType = 2;
                        break;
                    case 3:
                        //港澳居民来往内地通行证
                        tv_contact_id_type.setText("港澳居民来往内地通行证");
                        idCardType = 3;
                        break;
                    case 4:
                        //台湾居民来往内地通行证
                        tv_contact_id_type.setText("台湾居民来往内地通行证");
                        idCardType = 4;
                        break;
                    default:
                        //其它
                        tv_contact_id_type.setText("其它");
                        idCardType = 99;
                        break;
                }

                if (mchDetail.getIdCard() != null) {

                    //et_contact_id.setText(DateUtil.idString(mchDetail.getIdCard()));
                    et_contact_id.setText(mchDetail.getIdCard());
                } else {

                    et_contact_id.setText("");
                }

                if (mchDetail.isIdCardValidFlag()) {
                    //长期有效
                    tv_contact_valid.setText("长期");
                    tv_contact_date.setText("证件开始日期");
                    tv_contact_to.setVisibility(View.GONE);
                    tv_contact_end_date.setVisibility(View.GONE);
                    idCardValid = true;
                } else {
                    //非长期
                    tv_contact_valid.setText("非长期");
                    tv_contact_date.setText("证件有效期");
                    tv_contact_to.setVisibility(View.VISIBLE);
                    tv_contact_end_date.setVisibility(View.VISIBLE);
                    idCardValid = false;
                }

                if (mchDetail.getIdCardValidBegin() != null) {

                    tv_contact_start_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getIdCardValidBegin()));
                    idCardStartTime = mchDetail.getIdCardValidBegin();
                }

                if (mchDetail.getIdCardValidEnd() != null) {
                    tv_contact_end_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getIdCardValidEnd()));
                    idCardEndTime = mchDetail.getIdCardValidEnd();
                }

            }

            iv_choice_contact.setImageResource(R.mipmap.not_choose_icon);

            setEnable(true);
        }

    }

    private void setEnable(boolean b) {

        if (!b) {
            et_contact_name.setEnabled(false);
            rl_contact_id.setEnabled(false);
            iv_legal_id_icon.setVisibility(View.GONE);
            rl_contace_valid.setEnabled(false);
            iv_contact_valid_icon.setVisibility(View.GONE);
            et_contact_id.setEnabled(false);
            iv_contact_name_delete.setVisibility(View.GONE);
            iv_contact_id_delete.setVisibility(View.GONE);
            et_contact_name.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_contact_id.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_id_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_valid.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_start_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_start_date.setEnabled(false);
            tv_contact_end_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_contact_end_date.setEnabled(false);
            tv_contact_to.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_contact_date_icon.setVisibility(View.GONE);


        } else {
            iv_legal_id_icon.setVisibility(View.VISIBLE);
            rl_contace_valid.setEnabled(true);
            iv_contact_valid_icon.setVisibility(View.VISIBLE);
            et_contact_id.setEnabled(true);
            et_contact_name.setEnabled(true);
            iv_contact_name_delete.setVisibility(View.GONE);
            iv_contact_id_delete.setVisibility(View.GONE);
            et_contact_name.setTextColor(getResources().getColor(R.color.color_ff333333));
            et_contact_id.setTextColor(getResources().getColor(R.color.color_ff333333));
            tv_contact_id_type.setTextColor(getResources().getColor(R.color.color_ff333333));
            tv_contact_valid.setTextColor(getResources().getColor(R.color.color_ff333333));


            tv_contact_start_date.setTextColor(getResources().getColor(R.color.color_ff333333));
            tv_contact_start_date.setEnabled(true);
            tv_contact_end_date.setTextColor(getResources().getColor(R.color.color_ff333333));
            tv_contact_end_date.setEnabled(true);
            tv_contact_to.setTextColor(getResources().getColor(R.color.color_ff333333));
            iv_contact_date_icon.setVisibility(View.VISIBLE);
        }
    }

    String content;

    private void legalDialog(final String type) {

        if (!type.equals("legal")) {
            content = "您已提交变更法人信息的操作，是否继续需要变更结算卡号？";
        } else {

            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();

            content = "您的操作有敏感信息更改，我们会发送短信验证码至" + mchDetail.getTelphone().replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
        }

        legalDialog = new LegalDialog(LegalPersonActivity.this, "法人信息更改验证",
                content, "确认", "取消"
                , type, new LegalDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String s) {

                if (type.equals("legal")) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        Map<String, Object> map = new HashMap<>();
                        mData.getMchDetail().setPrincipal(et_legal_name.getText().toString().trim());
                        map.put("principal", et_legal_name.getText().toString().trim());//法人姓名
                        mData.getMchDetail().setIdCodeType(legalType);//法人证件类型
                        map.put("idCodeType", legalType);//法人证件类型
                        mData.getMchDetail().setIdCode(et_id_card.getText().toString().trim());//法人证件类型
                        map.put("idCode", et_id_card.getText().toString().trim());
                        mData.getMchDetail().setIdCodeValidFlag(legalvalidType);//法人证件是否长期有效
                        map.put("idCodeValidFlag", legalvalidType);//法人证件是否长期有效
                        if (!legalvalidType) {
                            if (!StringUtils.isEmptyOrNull(endTime)) {
                                mData.getMchDetail().setIdCodeValidEnd(endTime);//法人证件有效期结束时间
                                map.put("idCodeValidEnd", endTime);//法人证件有效期结束时间
                            }
                        }

                        if (!StringUtils.isEmptyOrNull(startTime)) {
                            mData.getMchDetail().setIdCodeValidBegin(startTime);//法人证件有效期开始时间
                            map.put("idCodeValidBegin", startTime);//法人证件有效期开始时间
                        }
                        mData.getMchDetail().setContactPersonName(et_contact_name.getText().toString().trim());//联系人姓名
                        map.put("contactPersonName", et_contact_name.getText().toString().trim());//联系人姓名
                        if (!StringUtils.isEmptyOrNull(et_contact_id.getText().toString().trim())) {
                            mData.getMchDetail().setIdCard(et_contact_id.getText().toString().trim());//联系人证件号码
                            map.put("idCard", et_contact_id.getText().toString().trim());//联系人证件号码
                        }
                        mData.getMchDetail().setIdCardType(idCardType);//联系人证件类型
                        map.put("idCardType", idCardType);//联系人证件类型
                        if (!idCardValid) {
                            if (!StringUtils.isEmptyOrNull(idCardEndTime)) {
                                mData.getMchDetail().setIdCardValidEnd(idCardEndTime);//联系人证件有效期结束时间
                                map.put("idCardValidEnd", idCardEndTime);//联系人证件有效期结束时间
                            }
                        }
                        mData.getMchDetail().setIdCardValidFlag(idCardValid);//联系人证件是否长期有效
                        map.put("idCardValidFlag", idCardValid);//联系人证件是否长期有效

                        if (!StringUtils.isEmptyOrNull(idCardStartTime)) {
                            mData.getMchDetail().setIdCardValidBegin(idCardStartTime);//联系人证件有效期开始时间
                            map.put("idCardValidBegin", idCardStartTime);//联系人证件有效期开始时间
                        }
                        Map<String, Object> mapInfo = new HashMap<>();
                        mapInfo.put("smsCode", s);
                        mapInfo.put("merchantId", mData.getMerchantId());
                        mapInfo.put("detail", map);
                        legalDialog.dismiss();
                        DialogUtil.safeShowDialog(mLoadDialog);
                        ServerClient.newInstance(MyApplication.getContext()).legalInfo(MyApplication.getContext(), Constants.TAG_LEGAL_INFO, mapInfo);
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                    }
                } else {
                    legalDialog.dismiss();
                    MyApplication.isRefresh = true;
                    Intent intentCard = new Intent();
                    intentCard.putExtra(Constants.INTENT_BANK_DETAIL, mData);
                    intentCard.setClass(LegalPersonActivity.this, ChangeCompanyCardActivity.class);
                    startActivity(intentCard);
                    LegalPersonActivity.this.finish();
                }

            }

            @Override
            public void handleCancleBtn() {
                legalDialog.dismiss();

                if (type.equals("legal_success")) {

                    MyApplication.isRefresh = true;
                    LegalPersonActivity.this.finish();
                }
            }
        });
        legalDialog.setOnClickSend(new LegalDialog.OnClickSend() {
            @Override
            public void onClickSend() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
                    map.put("mobile", mchDetail.getTelphone());
                    map.put("smsBizType", "MCH_LEGAL_UPDATE");
//                    map.put("bizId" , mData.getMerchantId());
                    ServerClient.newInstance(MyApplication.getContext()).send(MyApplication.getContext(), Constants.TAG_SEND_MSG, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                }
            }
        });
        DialogHelper.resize(this, legalDialog);
        legalDialog.show();

    }


    private void setEdittextListener(final EditText editText, final ImageView imageView) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }

            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点

                    if (editText.getText().length() > 0) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {

        if (event.getTag().equals(Constants.TAG_SEND_MSG)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SendMsgBean msg = (SendMsgBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LegalPersonActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (legalDialog != null)
                        legalDialog.setTimerStart();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_LEGAL_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SendMsgBean msg = (SendMsgBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LegalPersonActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功

                    legalDialog("legal_success");
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_QUERY_ENABLED_CHANGE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            QueryEnabledChangeBean msg = (QueryEnabledChangeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.QUERY_ENABLED_CHANGE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LegalPersonActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUERY_ENABLED_CHANGE_TRUE://请求成功
                    QueryEnabledChangeBean.Data data = msg.getData();
                    if (!data.isEditMchBasic()) {
                        setExitEable(false);
                    }
                    break;
            }
        }

    }

}
