package com.hstypay.enterprise.activity.merchantInfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyGridView;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SelectImagePopupWindow;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.MerChantModifyBean;
import com.hstypay.enterprise.bean.MerchantInfoBean;
import com.hstypay.enterprise.bean.PictureBean;
import com.hstypay.enterprise.bean.QueryEnabledChangeBean;
import com.hstypay.enterprise.bean.UploadImageBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.ImageFactory;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.ImageUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.shehuan.niv.NiceImageView;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

/**
 * Created by admin on 2017/7/11.
 */

public class LicenseInfoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private TextView tv_title, mTvNotice;
    private Button mBtnEnsure;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    /**
     * 拍照保存照片的uri
     */
    private Uri originalUri = null;
    private File tempFile;
    private String ispicture = "1";
    private String picOnePath, picTowPath, picThreePath, picFourPath, picFivePath, picSixPath, picSevenPath, picEightPath, picNinePath, picTenPath, picElevenPath, picTwelvePath, picThirteenPath,
            picFourteenPath, picFifteenPath, picSixteenPath, picSeventeenPath, picEighteenPath, picNineteenPath, picTwentyPath, pictwentyOnePath, picTwentyTwoPath, picTwentyThreePath;
    /**
     * 1.营业执照 2.身份证正面 3.身份证背面 4.门头照 5.开户许可证 6.银行卡正面照 7.手持身份证照
     */
    boolean takepic = false;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE3 = 103;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE4 = 104;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE5 = 105;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 106;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE7 = 107;

    private static final int MERCHANT_PERSON = 108;
    private static final int MERCHANT_COMPANY_PUBLIC = 109;
    private static final int MERCHANT_COMPANY_PRIVATE = 110;
    private int mType = -1;

    private SelectDialog mDialog;
    private MerchantInfoBean.DataBean mData;
    private SafeDialog mLoadDialog;
    private TextView tv_pic_info;
    private MyGridView gv_activy;
    private NiceImageView imageView;
    private NiceImageView imageNew;
    private boolean isClick = true;
    private String basePath;
    private RelativeLayout rl_upload;
    private List<PictureBean> mList;
    private String mNoticeIdentificationFront, mNoticeIdentificationBack;
    private PictureBean mPictureBean;
    private String mMerchantId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license_info);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE3:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    if (mPictureBean != null && mPictureBean.getPicId() == 8) {
                        //变更申请函
                        ImageUtil.saveAssetsImage(MyApplication.getContext(), "accountChangePhoto.jpg");
                    } else if (mPictureBean != null && mPictureBean.getPicId() == 9) {
                        //第三方申请函
                        ImageUtil.saveAssetsImage(MyApplication.getContext(), "thirdAuthPhoto.jpg");
                    }
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    /*private void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(LicenseInfoActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }*/

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_pic_info = findViewById(R.id.tv_pic_info);
        mTvNotice = findViewById(R.id.tv_change_bank_card_notice);
        SpannableString spannableString = new SpannableString(getString(R.string.tv_pic_info));
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FD414B")), 0, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tv_pic_info.setText(spannableString);
        tv_title.setText(UIUtils.getString(R.string.title_merchant_photos));
        mBtnEnsure = findViewById(R.id.btn_submit);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        gv_activy = findViewById(R.id.gv_activy);
        setButtonEnable(mBtnEnsure, true);
    }


    private void initListener() {
        iv_back.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
    }

    private void initData() {
        mData = (MerchantInfoBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_NAME_MERCHANT_UPLOAD);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        //默认是可以修改的，以下是控制是否不可修改资料
        switch (mData.getMchQueryStatus()) {
            case -1:
                //初始商户
                setExitEable(false);
                break;
            /*case 1:
                //审核通过
                if (mData.getAttestationStatus() != 1 || mData.getTradeStatus() == 0) {
                    setExitEable(false);
                }else if (isChangeInfo()){

                }
                break;*/
            case 5:
                //作废商户
                setExitEable(false);
                break;
            /*case 10:
                //变更审核通过
                setExitEable(false);
                break;*/
            case 101:
                //冻结商户
                setExitEable(false);
                break;
            //case 3://修改待确认
            default:
                if (isChangeInfo()){
                    queryEnabledChange();
                }
                break;
        }
        /*if (mData.getMchQueryStatus() ==3) {
            mTvNotice.setVisibility(View.VISIBLE);
        } else {
            mTvNotice.setVisibility(View.GONE);
        }*/

        mList = new ArrayList<>();
        PictureBean idCard = new PictureBean();//法人证件正面照
        idCard.setPicId(2);
        PictureBean idFord = new PictureBean();//法人证件反面照
        idFord.setPicId(3);
        PictureBean handId = new PictureBean();//法人手持证件照
        handId.setPicId(7);
        handId.setPicName(getString(R.string.tv_identification_photo_hand));
        PictureBean bankCard = new PictureBean();//银行卡正面照
        bankCard.setPicId(6);
        bankCard.setPicName("银行卡正面照");
        PictureBean company = new PictureBean();//店铺门头照
        company.setPicId(4);
        company.setPicName("店铺门头照");
        PictureBean license = new PictureBean();//营业执照照片
        license.setPicId(1);
        license.setPicName("营业执照照片");
        PictureBean bkLicense = new PictureBean();//开户许可证
        bkLicense.setPicId(5);
        bkLicense.setPicName("开户许可证");
        PictureBean pic8 = new PictureBean();//变更申请函
        pic8.setPicId(8);
        pic8.setPicName("变更申请函");
        PictureBean pic9 = new PictureBean();//第三方授权函
        pic9.setPicId(9);
        pic9.setPicName("第三方授权函");
        PictureBean pic10 = new PictureBean();//工信网证明照
        pic10.setPicId(10);
        pic10.setPicName("工信网证明照");
        PictureBean pic11 = new PictureBean();//经营场所内景照一
        pic11.setPicId(11);
        pic11.setPicName("经营场所内景照一");
        PictureBean pic12 = new PictureBean();//经营场所内景照二
        pic12.setPicId(12);
        pic12.setPicName("经营场所内景照二");
        PictureBean pic13 = new PictureBean();//商户协议照
        pic13.setPicId(13);
        pic13.setPicName("商户协议照");
        PictureBean pic14 = new PictureBean();//收银台照
        pic14.setPicId(14);
        pic14.setPicName("收银台照");
        PictureBean pic15 = new PictureBean();//联系人证件正面照
        pic15.setPicId(15);
        pic15.setPicName("联系人证件正面照");
        PictureBean pic16 = new PictureBean();//联系人证件反面照
        pic16.setPicId(16);
        pic16.setPicName("联系人证件反面照");
        PictureBean pic17 = new PictureBean();//联系人手执证件照
        pic17.setPicId(17);
        pic17.setPicName("联系人手执证件照");
        PictureBean pic18 = new PictureBean();//其他图片一
        pic18.setPicId(18);
        pic18.setPicName("其他图片一");
        PictureBean pic19 = new PictureBean();//其他图片二
        pic19.setPicId(19);
        pic19.setPicName("其他图片二");
        PictureBean pic20 = new PictureBean();//其他图片三
        pic20.setPicId(20);
        pic20.setPicName("其他图片三");
        PictureBean pic21 = new PictureBean();//其他图片四
        pic21.setPicId(21);
        pic21.setPicName("其他图片四");
        PictureBean pic22 = new PictureBean();//单位证明函
        pic22.setPicId(22);
        pic22.setPicName("单位证明函");
        PictureBean pic23 = new PictureBean();//银行卡反面照
        pic23.setPicId(23);
        pic23.setPicName("银行卡反面照");

        MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
        String idCodePhotos;
        if (mchDetail.getIdCodeType() == 1) {
            idCodePhotos = mchDetail.getIdCardPhotos();
        } else {
            idCodePhotos = mchDetail.getCertificatePhoto();
        }
        if (!StringUtils.isEmptyOrNull(idCodePhotos)) {
            String[] split = idCodePhotos.split(",");
            if (split != null && split.length > 0) {
                if (split.length == 1) {
                    if (!TextUtils.isEmpty(split[0])) {
                        picTowPath = split[0].trim();
                        idCard.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                } else if (split.length == 2) {
                    if (!TextUtils.isEmpty(split[0])) {
                        picTowPath = split[0].trim();
                        idCard.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                    if (!TextUtils.isEmpty(split[1])) {
                        picThreePath = split[1].trim();
                        idFord.setUrl(Constants.H5_BASE_URL + split[1].trim());
                    }
                }
            }
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getHandIdcardPhoto())) {
            //手持身份证照片
            picSevenPath = mchDetail.getHandIdcardPhoto();
            handId.setUrl(Constants.H5_BASE_URL + mchDetail.getHandIdcardPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getBkCardPhoto())) {
            //银行卡
            picSixPath = mchDetail.getBkCardPhoto();
            bankCard.setUrl(Constants.H5_BASE_URL + mchDetail.getBkCardPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getCompanyPhoto())) {
            //门头照
            picFourPath = mchDetail.getCompanyPhoto();
            company.setUrl(Constants.H5_BASE_URL + mchDetail.getCompanyPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getLicensePhoto())) {
            //营业执照
            picOnePath = mchDetail.getLicensePhoto();
            license.setUrl(Constants.H5_BASE_URL + mchDetail.getLicensePhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getBkLicensePhoto())) {
            //开户许可证
            picFivePath = mchDetail.getBkLicensePhoto();
            bkLicense.setUrl(Constants.H5_BASE_URL + mchDetail.getBkLicensePhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getAccountChangePhoto())) {
            picEightPath = mchDetail.getAccountChangePhoto();
            pic8.setUrl(Constants.H5_BASE_URL + mchDetail.getAccountChangePhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getThirdAuthPhoto())) {
            picNinePath = mchDetail.getThirdAuthPhoto();
            pic9.setUrl(Constants.H5_BASE_URL + mchDetail.getThirdAuthPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getOrgPhoto())) {
            picTenPath = mchDetail.getOrgPhoto();
            pic10.setUrl(Constants.H5_BASE_URL + mchDetail.getOrgPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getBussinessPlacePhoto())) {
            picElevenPath = mchDetail.getBussinessPlacePhoto();
            pic11.setUrl(Constants.H5_BASE_URL + mchDetail.getBussinessPlacePhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getBussinessPlacePhoto2())) {
            picTwelvePath = mchDetail.getBussinessPlacePhoto2();
            pic12.setUrl(Constants.H5_BASE_URL + mchDetail.getBussinessPlacePhoto2());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getProtocolPhoto())) {
            picThirteenPath = mchDetail.getProtocolPhoto();
            pic13.setUrl(Constants.H5_BASE_URL + mchDetail.getProtocolPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getCashRegisterPhoto())) {
            picFourteenPath = mchDetail.getCashRegisterPhoto();
            pic14.setUrl(Constants.H5_BASE_URL + mchDetail.getCashRegisterPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getContactCertPhoto())) {
            String[] split = mchDetail.getContactCertPhoto().split(",");
            if (split != null && split.length > 0) {
                if (split.length == 1) {
                    if (!StringUtils.isEmptyOrNull(split[0])) {
                        picFifteenPath = split[0].trim();
                        pic15.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                } else if (split.length == 2) {
                    if (!StringUtils.isEmptyOrNull(split[0])) {
                        picFifteenPath = split[0].trim();
                        pic15.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[1])) {
                        picSixteenPath = split[1].trim();
                        pic16.setUrl(Constants.H5_BASE_URL + split[1].trim());
                    }
                }
            }
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getContactHandCertPhoto())) {
            picSeventeenPath = mchDetail.getContactHandCertPhoto();
            pic17.setUrl(Constants.H5_BASE_URL + mchDetail.getContactHandCertPhoto());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getOtherPhoto())) {
            String[] split = mchDetail.getOtherPhoto().split(",");
            if (split != null && split.length > 0) {
                if (split.length == 1) {
                    if (!StringUtils.isEmptyOrNull(split[0])) {
                        picEighteenPath = split[0].trim();
                        pic18.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                } else if (split.length == 2) {
                    if (!StringUtils.isEmptyOrNull(split[0])) {
                        picEighteenPath = split[0].trim();
                        pic18.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[1])) {
                        picNineteenPath = split[1].trim();
                        pic19.setUrl(Constants.H5_BASE_URL + split[1].trim());
                    }
                } else if (split.length == 3) {
                    if (!StringUtils.isEmptyOrNull(split[0])) {
                        picEighteenPath = split[0].trim();
                        pic18.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[1])) {
                        picNineteenPath = split[1].trim();
                        pic19.setUrl(Constants.H5_BASE_URL + split[1].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[2])) {
                        picTwentyPath = split[2].trim();
                        pic20.setUrl(Constants.H5_BASE_URL + split[2].trim());
                    }
                } else if (split.length == 4) {
                    if (!StringUtils.isEmptyOrNull(split[0])) {
                        picEighteenPath = split[0].trim();
                        pic18.setUrl(Constants.H5_BASE_URL + split[0].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[1])) {
                        picNineteenPath = split[1].trim();
                        pic19.setUrl(Constants.H5_BASE_URL + split[1].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[2])) {
                        picTwentyPath = split[2].trim();
                        pic20.setUrl(Constants.H5_BASE_URL + split[2].trim());
                    }
                    if (!StringUtils.isEmptyOrNull(split[3])) {
                        pictwentyOnePath = split[3].trim();
                        pic21.setUrl(Constants.H5_BASE_URL + split[3].trim());
                    }
                }
            }
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getCompanyProve())) {
            picTwentyTwoPath = mchDetail.getCompanyProve();
            pic22.setUrl(Constants.H5_BASE_URL + mchDetail.getCompanyProve());
        }
        if (!StringUtils.isEmptyOrNull(mchDetail.getBkCardPhotoBack())) {
            picTwentyThreePath = mchDetail.getBkCardPhotoBack();
            pic23.setUrl(Constants.H5_BASE_URL + mchDetail.getBkCardPhotoBack());
        }

        MerchantInfoBean.DataBean.BankAccountBean bankAccount = mData.getBankAccount();
        mList.add(pic8);
        mList.add(pic9);
        switch (mchDetail.getIdCodeType()) {//1:居民身份证 2:护照 3:港澳居民来往内地通行证 4:台湾居民来往内地通行证 99:其他
            case 1:
                idCard.setPicName(getString(R.string.tv_identification_photo_front));
                idFord.setPicName(getString(R.string.tv_identification_photo_back));
                mNoticeIdentificationFront = "请上传" + getString(R.string.tv_identification_photo_front);
                mNoticeIdentificationBack = "请上传" + getString(R.string.tv_identification_photo_back);
                break;
            case 2:
                idCard.setPicName(getString(R.string.tv_license_photo_front));
                mNoticeIdentificationFront = "请上传" + getString(R.string.tv_license_photo_front);
                mNoticeIdentificationBack = "";
                break;
            case 3:
                idCard.setPicName(getString(R.string.tv_gangao_photo_front));
                mNoticeIdentificationFront = "请上传" + getString(R.string.tv_gangao_photo_front);
                mNoticeIdentificationBack = "";
                break;
            case 4:
                idCard.setPicName(getString(R.string.tv_taiwan_photo_front));
                mNoticeIdentificationFront = "请上传" + getString(R.string.tv_taiwan_photo_front);
                mNoticeIdentificationBack = "";
                break;
            default:
                idCard.setPicName(getString(R.string.tv_other_photo_front));
                mNoticeIdentificationFront = "请上传" + getString(R.string.tv_other_photo_front);
                mNoticeIdentificationBack = "";
                break;
        }
        idCard.setMust(true);
        mList.add(idCard);
        if (mchDetail.getIdCodeType() == 1) {
            idFord.setMust(true);
            mList.add(idFord);
        }
        switch (mData.getOutMchType()) {
            case 1:
                //企业商户
                if (bankAccount.getAccountType() == 1) {
                    //企业
                    license.setMust(true);
                    company.setMust(true);
                    bkLicense.setMust(true);
                    mList.add(license);
                    mList.add(company);
                    mList.add(bkLicense);
                    mList.add(pic10);
                    mList.add(handId);
                    mList.add(pic11);
                    mList.add(pic12);
                    mList.add(pic13);
                    mList.add(pic14);
                } else {
                    //个人
                    license.setMust(true);
                    bankCard.setMust(true);
                    company.setMust(true);
                    mList.add(license);
                    mList.add(bankCard);
                    mList.add(company);
                    mList.add(pic10);
                    mList.add(handId);
                    mList.add(pic11);
                    mList.add(pic12);
                    mList.add(pic13);
                    mList.add(pic14);
                    mList.add(pic23);
                }
                break;
            case 2:
                //个体工商户
                license.setMust(true);
                bankCard.setMust(true);
                company.setMust(true);
                mList.add(license);
                mList.add(bankCard);
                mList.add(company);
                mList.add(pic10);
                mList.add(handId);
                mList.add(pic11);
                mList.add(pic12);
                mList.add(pic13);
                mList.add(pic14);
                mList.add(pic23);
                break;
            case 3:
                //个体经营者
                handId.setMust(true);
                bankCard.setMust(true);
                company.setMust(true);
                pic13.setMust(true);
                mList.add(bankCard);
                mList.add(company);
                mList.add(handId);
                mList.add(pic13);
                mList.add(pic11);
                mList.add(pic12);
                mList.add(pic14);
                mList.add(pic23);
                break;
            case 4:
                //事业单位
                if (bankAccount.getAccountType() == 1) {
                    //企业
                    license.setMust(true);
                    company.setMust(true);
                    pic22.setMust(true);
                    bkLicense.setMust(true);
                    mList.add(license);
                    mList.add(company);
                    mList.add(pic22);
                    mList.add(bkLicense);
                    mList.add(handId);
                    mList.add(pic11);
                    mList.add(pic12);
                    mList.add(pic14);
                } else {
                    license.setMust(true);
                    bankCard.setMust(true);
                    company.setMust(true);
                    pic22.setMust(true);
                    mList.add(license);
                    mList.add(bankCard);
                    mList.add(company);
                    mList.add(pic22);
                    mList.add(handId);
                    mList.add(pic11);
                    mList.add(pic12);
                    mList.add(pic14);
                    mList.add(pic23);
                }
                break;
        }
        mList.add(pic15);
        mList.add(pic16);
        mList.add(pic17);
        mList.add(pic18);
        mList.add(pic19);
        mList.add(pic20);
        mList.add(pic21);
        MyAdapter adapter = new MyAdapter(LicenseInfoActivity.this, mList);
        gv_activy.setAdapter(adapter);
    }

    //是否是变更审核
    private boolean isChangeInfo(){
        if (mData==null){
            return false;
        }

        /*if ((mData.getTradeStatus()== Constants.HPMerchantTradeAvailable ||mData.getTradeStatus() == Constants.HPMerchantTradeSomeAvailable )
                && mData.getAttestationStatus() == Constants.HPMerchantAuthenticateStatusPass){
            return true;
        }*/
        if (mData.getMchQueryStatus() == 1 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
            //case 1://审核通过
            //case 100://可交易
            //case 10://变更审核通过
            return true;//需要后端去控制是否不可修改
        }
        return false;
    }

    //验证商户资料是否允许修改
    private void queryEnabledChange() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            DialogUtil.safeShowDialog(mLoadDialog);
            map.put("merchantId", mMerchantId);
            ServerClient.newInstance(MyApplication.getContext()).queryEnabledChange(MyApplication.getContext(), Constants.TAG_QUERY_ENABLED_CHANGE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }


    private void setExitEable(boolean b) {
        mBtnEnsure.setVisibility(View.GONE);
        isClick = false;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //确认上传
            case R.id.btn_submit:
                submitData();
                break;
        }
    }

    private void submitData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            Map<String, String> mchMap = new HashMap<>();
            map.put("merchantId", mData.getMerchantId());
            map.put("merchantName", mData.getMerchantName());

            if (mData.getMchDetail().getIdCodeType() == 1) {
                if (!StringUtils.isEmptyOrNull(picTowPath) && !StringUtils.isEmptyOrNull(picThreePath)) {
                    mchMap.put("idCardPhotos", picTowPath + "," + picThreePath);
                } else {
                    if (StringUtils.isEmptyOrNull(picTowPath)) {
                        ToastUtil.showToastShort(mNoticeIdentificationFront);
                        return;
                    } else if (StringUtils.isEmptyOrNull(picThreePath)) {
                        ToastUtil.showToastShort(mNoticeIdentificationBack);
                        return;
                    }
                }
            } else {
                if (!StringUtils.isEmptyOrNull(picTowPath)) {
                    mchMap.put("certificatePhoto", picTowPath);
                } else {
                    ToastUtil.showToastShort(mNoticeIdentificationFront);
                    return;
                }
            }
            if (!StringUtils.isEmptyOrNull(picOnePath)) {
                mchMap.put("licensePhoto", picOnePath);
            } else {
                if (mData.getOutMchType() == 1 || mData.getOutMchType() == 2 || mData.getOutMchType() == 4) {
                    ToastUtil.showToastShort("请上传营业执照");
                    return;
                }
            }

            if (!StringUtils.isEmptyOrNull(picFourPath)) {
                mchMap.put("companyPhoto", picFourPath);
            } else {
                ToastUtil.showToastShort("请上传店铺门头照");
                return;
            }

            if (!StringUtils.isEmptyOrNull(picFivePath)) {
                mchMap.put("bkLicensePhoto", picFivePath);
            } else {
                if (mData.getBankAccount().getAccountType() == 1 && (mData.getOutMchType() == 1 || mData.getOutMchType() == 4)) {
                    ToastUtil.showToastShort("请上传开户许可证");
                    return;
                }
            }

            if (!StringUtils.isEmptyOrNull(picSixPath)) {
                mchMap.put("bkCardPhoto", picSixPath);
            } else {
                if (mData.getBankAccount().getAccountType() == 2 || mData.getOutMchType() == 2 || mData.getOutMchType() == 3) {
                    ToastUtil.showToastShort("请上传银行卡正面照");
                    return;
                }
            }

            if (!StringUtils.isEmptyOrNull(picSevenPath)) {
                mchMap.put("handIdcardPhoto", picSevenPath);
            }

            if (!StringUtils.isEmptyOrNull(picEightPath)) {
                mchMap.put("accountChangePhoto", picEightPath);
            }

            if (!StringUtils.isEmptyOrNull(picNinePath)) {
                mchMap.put("thirdAuthPhoto", picNinePath);
            }

            if (!StringUtils.isEmptyOrNull(picTenPath)) {
                mchMap.put("orgPhoto", picTenPath);
            }

            if (!StringUtils.isEmptyOrNull(picElevenPath)) {

                mchMap.put("bussinessPlacePhoto", picElevenPath);
            }

            if (!StringUtils.isEmptyOrNull(picTwelvePath)) {
                mchMap.put("bussinessPlacePhoto2", picTwelvePath);
            }

            if (!StringUtils.isEmptyOrNull(picThirteenPath)) {
                mchMap.put("protocolPhoto", picThirteenPath);
            }

            if (!StringUtils.isEmptyOrNull(picFourteenPath)) {
                mchMap.put("cashRegisterPhoto", picFourteenPath);
            }

            if (!StringUtils.isEmptyOrNull(picFifteenPath) && !StringUtils.isEmptyOrNull(picSixteenPath)) {
                mchMap.put("contactCertPhoto", picFifteenPath + "," + picSixteenPath);
            } else {
                if (!StringUtils.isEmptyOrNull(picFifteenPath)) {
                    mchMap.put("contactCertPhoto", picFifteenPath);
                } else if (!StringUtils.isEmptyOrNull(picSixteenPath)) {
                    mchMap.put("contactCertPhoto", "," + picSixteenPath);
                }
            }

            if (!StringUtils.isEmptyOrNull(picSeventeenPath)) {
                mchMap.put("contactHandCertPhoto", picSeventeenPath);
            } else {
                if (mData.getOutMchType() == 3) {
                    ToastUtil.showToastShort("请上传法人手持证件照");
                    return;
                }
            }

            StringBuilder otherPhotoBuilder = new StringBuilder();
            if (!StringUtils.isEmptyOrNull(picEighteenPath)) {
                otherPhotoBuilder.append(picEighteenPath).append(",");
            } else {
                otherPhotoBuilder.append("").append(",");
            }
            if (!StringUtils.isEmptyOrNull(picNineteenPath)) {
                otherPhotoBuilder.append(picNineteenPath).append(",");
            } else {
                otherPhotoBuilder.append("").append(",");
            }
            if (!StringUtils.isEmptyOrNull(picTwentyPath)) {
                otherPhotoBuilder.append(picTwentyPath).append(",");
            } else {
                otherPhotoBuilder.append("").append(",");
            }
            if (!StringUtils.isEmptyOrNull(pictwentyOnePath)) {
                otherPhotoBuilder.append(pictwentyOnePath);
            } else {
                otherPhotoBuilder.append("");
            }
            if (otherPhotoBuilder.toString().length() > 0) {
                mchMap.put("otherPhoto", otherPhotoBuilder.toString());
            }
            if (!StringUtils.isEmptyOrNull(picTwentyTwoPath)) {
                mchMap.put("companyProve", picTwentyTwoPath);
            } else {
                if (mData.getOutMchType() == 4) {
                    ToastUtil.showToastShort("请上传单位证明函");
                    return;
                }
            }
            if (!StringUtils.isEmptyOrNull(picTwentyThreePath)) {
                mchMap.put("bkCardPhotoBack", picTwentyThreePath);
            }
            map.put("detail", mchMap);
            LicenseInfoActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.safeShowDialog(mLoadDialog);
                }
            });

            ServerClient.newInstance(MyApplication.getContext()).submitMerchantInfo(MyApplication.getContext(), Constants.TAG_SUBMIT_MERCHANT_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }

    }

    private void choicePic(PictureBean bean) {
        final String status = Environment.getExternalStorageState();
        SelectImagePopupWindow popupWindow = new SelectImagePopupWindow(LicenseInfoActivity.this, bean, basePath, new SelectImagePopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        boolean results = PermissionUtils.checkPermissionArray(LicenseInfoActivity.this, permissionArray);
                        if (results) {
                            startCamera();
                        } else {
                            showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_photo));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        boolean results = PermissionUtils.checkPermissionArray(LicenseInfoActivity.this, permissionArray);
                        if (results) {
                            takeImg();
                        } else {
                            showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_photo));
                        }
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void delete(PictureBean be) {
                //删除图片
                initDelete(be);

            }

            @Override
            public void download(PictureBean bean) {
                mPictureBean = bean;
                try {
                    boolean results = PermissionUtils.checkPermissionArray(LicenseInfoActivity.this, permissionArray);
                    if (results) {
                        if (bean.getPicId() == 8) {
                            //变更申请函
                            ImageUtil.saveAssetsImage(MyApplication.getContext(), "accountChangePhoto.jpg");
                        } else if (bean.getPicId() == 9) {
                            //第三方申请函
                            ImageUtil.saveAssetsImage(MyApplication.getContext(), "thirdAuthPhoto.jpg");
                        }
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE3, permissionArray, getString(R.string.permission_content_storage));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        popupWindow.showAtLocation(tv_title, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    /*private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(LicenseInfoActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(LicenseInfoActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }*/

    private void initDelete(PictureBean bean) {

        if (bean != null) {

            switch (bean.getPicId()) {
                case 1:
                    picOnePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf1));
                        imageNew.postInvalidate();
                    }
                    break;
                case 2:
                    picTowPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf2));
                        imageNew.postInvalidate();
                    }
                    break;
                case 3:
                    picThreePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf3));
                        imageNew.postInvalidate();
                    }
                    break;
                case 4:
                    picFourPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf4));
                        imageNew.postInvalidate();
                    }
                    break;
                case 5:
                    picFivePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf5));
                        imageNew.postInvalidate();
                    }
                    break;
                case 6:
                    picSixPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf6));
                        imageNew.postInvalidate();
                    }
                    break;
                case 7:
                    picSevenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf7));
                        imageNew.postInvalidate();
                    }
                    break;
                case 8:
                    picEightPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 9:
                    picNinePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 10:
                    picTenPath = "";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf10));
                        imageNew.postInvalidate();
                    }
                    break;
                case 11:
                    picElevenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf11));
                        imageNew.postInvalidate();
                    }


                    break;
                case 12:
                    picTwelvePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf11));
                        imageNew.postInvalidate();
                    }
                    break;
                case 13:
                    picThirteenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf13));
                        imageNew.postInvalidate();
                    }
                    break;
                case 14:
                    picFourteenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 15:
                    picFifteenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf2));
                        imageNew.postInvalidate();
                    }
                    break;
                case 16:
                    picSixteenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf3));
                        imageNew.postInvalidate();
                    }
                    break;
                case 17:
                    picSeventeenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(LicenseInfoActivity.this.getResources().getDrawable(R.mipmap.upload_sf7));
                        imageNew.postInvalidate();
                    }
                    break;
                case 18:
                    picEighteenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 19:
                    picNineteenPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 20:
                    picTwentyPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 21:
                    pictwentyOnePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 22:
                    picTwentyTwoPath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 23:
                    picTwentyThreePath = "";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
            }
        }

    }


    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);

    }

    String imageUrl;

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";

            tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()){
                tempFile.getParentFile().mkdirs();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(LicenseInfoActivity.this, Constants.FILE_PROVIDER, tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        takepic = false;
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            if (ispicture.equals("1")) {
                                uploadimg(path);
                            } else if (ispicture.equals("2")) {
                                uploadimg(path);
                            } else if (ispicture.equals("3")) {
                                uploadimg(path);
                            } else if (ispicture.equals("4")) {
                                uploadimg(path);
                            } else if (ispicture.equals("5")) {
                                uploadimg(path);
                            } else if (ispicture.equals("6")) {
                                uploadimg(path);
                            } else if (ispicture.equals("7")) {
                                uploadimg(path);
                            } else if (ispicture.equals("8")) {
                                uploadimg(path);
                            } else if (ispicture.equals("9")) {
                                uploadimg(path);
                            } else if (ispicture.equals("10")) {
                                uploadimg(path);
                            } else if (ispicture.equals("11")) {
                                uploadimg(path);
                            } else if (ispicture.equals("12")) {
                                uploadimg(path);
                            } else if (ispicture.equals("13")) {
                                uploadimg(path);
                            } else if (ispicture.equals("14")) {
                                uploadimg(path);
                            } else if (ispicture.equals("15")) {
                                uploadimg(path);
                            } else if (ispicture.equals("16")) {
                                uploadimg(path);
                            } else if (ispicture.equals("17")) {
                                uploadimg(path);
                            } else if (ispicture.equals("18")) {
                                uploadimg(path);
                            } else if (ispicture.equals("19")) {
                                uploadimg(path);
                            } else if (ispicture.equals("20")) {
                                uploadimg(path);
                            } else if (ispicture.equals("21")) {
                                uploadimg(path);
                            } else if (ispicture.equals("22")) {
                                uploadimg(path);
                            } else if (ispicture.equals("23")) {
                                uploadimg(path);
                            }
                        }
                    }
                    break;
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = getPicPath(originalUri);
                    LogUtil.d("path=" + pathPhoto);
                    takepic = true;
                    Bitmap bitmap_pci;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        if (ispicture.equals("1")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("2")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("3")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("4")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("5")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("6")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("7")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("8")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("9")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("10")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("11")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("12")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("13")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("14")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("15")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("16")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("17")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("18")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("19")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("20")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("21")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("22")) {
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("23")) {
                            uploadimg(pathPhoto);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private String picPath;

    private void uploadimg(final String path) {

        if (!NetworkUtils.isNetworkAvailable(LicenseInfoActivity.this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
            return;
        }
        DialogUtil.safeShowDialog(mLoadDialog);

        String url = Constants.H5_BASE_URL + "/app/merchant/file/upload";
        PostFormBuilder post = OkHttpUtils.post();
        if (!TextUtils.isEmpty(path)) {
            picPath = AppHelper.getImageCacheDir(path);
            LogUtil.d("pic====" + path + ",,," + path);
            ImageFactory.compressPicture(path, picPath);
            post = post.addFile("license.jpg", "license", new File(picPath));
        }


        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                LogUtil.i("zhouwei", "onErroreee//..>>>" + e);
                DialogUtil.safeCloseDialog(mLoadDialog);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(picPath);

            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(picPath);
                DialogUtil.safeCloseDialog(mLoadDialog);
                Gson gson = new Gson();
                UploadImageBean uploadImageBean = gson.fromJson(response, UploadImageBean.class);
                if (uploadImageBean != null) {
                    if (uploadImageBean.isStatus()) {
                        UploadImageBean.DataEntity data = uploadImageBean.getData();
                        if (data != null) {
                            imagePath(data);
                            Bitmap bitmap = ImagePase.readBitmapFromStream(path);
                            setBitmapView(imageView, bitmap);
                        }
                    } else {
                        if (uploadImageBean.getError() != null && uploadImageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(uploadImageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void imagePath(UploadImageBean.DataEntity data) {

        int p = Integer.parseInt(ispicture);

        switch (p) {
            case 1:
                picOnePath = data.getLicense();
                break;
            case 2:
                picTowPath = data.getLicense();
                break;
            case 3:
                picThreePath = data.getLicense();
                break;
            case 4:
                picFourPath = data.getLicense();
                break;
            case 5:
                picFivePath = data.getLicense();
                break;
            case 6:
                picSixPath = data.getLicense();
                break;
            case 7:
                picSevenPath = data.getLicense();
                break;
            case 8:
                picEightPath = data.getLicense();
                break;
            case 9:
                picNinePath = data.getLicense();
                break;
            case 10:
                picTenPath = data.getLicense();
                break;
            case 11:
                picElevenPath = data.getLicense();
                break;
            case 12:
                picTwelvePath = data.getLicense();
                break;
            case 13:
                picThirteenPath = data.getLicense();
                break;
            case 14:
                picFourteenPath = data.getLicense();
                break;
            case 15:
                picFifteenPath = data.getLicense();
                break;
            case 16:
                picSixteenPath = data.getLicense();
                break;
            case 17:
                picSeventeenPath = data.getLicense();
                break;
            case 18:
                picEighteenPath = data.getLicense();
                break;
            case 19:
                picNineteenPath = data.getLicense();
                break;
            case 20:
                picTwentyPath = data.getLicense();
                break;
            case 21:
                pictwentyOnePath = data.getLicense();
                break;
            case 22:
                picTwentyTwoPath = data.getLicense();
                break;
            case 23:
                picTwentyThreePath = data.getLicense();
                break;
        }

    }


    /**
     * 返回图片地址
     *
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public String getPicPath(Uri originalUri) {
        ContentResolver mContentResolver = LicenseInfoActivity.this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        } else if ((originalUri + "").contains("/data")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        } else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                    && originalUri.toString().contains("documents")) {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                        mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column,
                                sel,
                                new String[]{id},
                                null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            } else {

                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }

        }
        return originalPath;
    }

    private void setButton(int type) {
        switch (type) {
            case MERCHANT_PERSON:
                if (StringUtils.isEmptyOrNull(picTowPath) && StringUtils.isEmptyOrNull(picThreePath) &&
                        StringUtils.isEmptyOrNull(picFourPath) && StringUtils.isEmptyOrNull(picSixPath) && StringUtils.isEmptyOrNull(picSevenPath)) {
                    mBtnEnsure.setVisibility(View.GONE);
                } else {
                    mBtnEnsure.setVisibility(View.VISIBLE);
                }
                break;
            case MERCHANT_COMPANY_PRIVATE:
                if (StringUtils.isEmptyOrNull(picOnePath) && StringUtils.isEmptyOrNull(picTowPath) && StringUtils.isEmptyOrNull(picThreePath) &&
                        StringUtils.isEmptyOrNull(picFourPath) && StringUtils.isEmptyOrNull(picSixPath)) {
                    mBtnEnsure.setVisibility(View.GONE);
                } else {
                    mBtnEnsure.setVisibility(View.VISIBLE);
                }
                break;
            case MERCHANT_COMPANY_PUBLIC:
                if (StringUtils.isEmptyOrNull(picOnePath) && StringUtils.isEmptyOrNull(picTowPath) && StringUtils.isEmptyOrNull(picThreePath) &&
                        StringUtils.isEmptyOrNull(picFourPath) && StringUtils.isEmptyOrNull(picFivePath)) {
                    mBtnEnsure.setVisibility(View.GONE);
                } else {
                    mBtnEnsure.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void setBitmapView(final NiceImageView iv, final Bitmap bitmap) {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (bitmap != null) {
                    iv.setImageBitmap(bitmap);
                    iv.setVisibility(View.VISIBLE);
                    iv.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.GONE);
                    }
                }

                setButton(mType);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }


    class MyAdapter extends BaseAdapter {

        private Context mContext;
        private List<PictureBean> mList;

        public MyAdapter(Context context, List<PictureBean> list) {

            this.mContext = context;
            this.mList = list;
        }

        @Override
        public int getCount() {

            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {


            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.image_gridview_item, null);
                holder = new ViewPayTypeHolder();
                holder.iv_pic = convertView.findViewById(R.id.iv_pic);
                holder.tv_pic_title = convertView.findViewById(R.id.tv_pic_title);
                holder.rl_upload = convertView.findViewById(R.id.rl_upload);
                holder.iv_upload_icon = convertView.findViewById(R.id.iv_upload_icon);
                holder.iv_pic_new = convertView.findViewById(R.id.iv_pic_new);
                convertView.setTag(holder);
            } else {
                holder = (ViewPayTypeHolder) convertView.getTag();
            }


            PictureBean bean = mList.get(position);

            if (bean != null) {

                if (bean.isMust()) {
                    SpannableString spantwo = new SpannableString("* " + bean.getPicName());
                    spantwo.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.red)), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    holder.tv_pic_title.setText(spantwo);
                } else {
                    if (bean.getPicName() != null) {
                        holder.tv_pic_title.setText(bean.getPicName());
                    }
                }


                switch (bean.getPicId()) {
                    case 1:
                        //营业执照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf1).error(R.mipmap.upload_sf1).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf1));
                        }
                        break;
                    case 2:
                        //身份证正面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf2).error(R.mipmap.upload_sf2).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf2));
                        }
                        break;
                    case 3:
                        //身份证反面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf3).error(R.mipmap.upload_sf3).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf3));
                        }
                        break;
                    case 4:
                        //门头照

                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf4).error(R.mipmap.upload_sf4).into(holder.iv_pic);

                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf4));
                        }
                        break;
                    case 5:
                        //开户许可证
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf5).error(R.mipmap.upload_sf5).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf5));
                        }
                        break;
                    case 6:
                        //银行卡
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf6).error(R.mipmap.upload_sf6).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf6));
                        }
                        break;
                    case 7:
                        //手持身份证
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf7).error(R.mipmap.upload_sf7).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf7));
                        }
                        break;

                    case 8:
                        //变更申请函
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }

                    case 9:
                        //第三方授权函
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 10:
                        //工信网证明照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf10).error(R.mipmap.upload_sf10).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf10));
                        }
                        break;
                    case 11:

                        //经营场所内景照一
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf11).error(R.mipmap.upload_sf11).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf11));
                        }

                        break;
                    case 12:

                        //经营场所内景照二
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf11).error(R.mipmap.upload_sf11).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf11));
                        }

                        break;
                    case 13:

                        //商户协议照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf13).error(R.mipmap.upload_sf13).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf13));
                        }

                        break;

                    case 14:

                        //收银台
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }

                        break;

                    case 15:

                        //联系人证件正面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf2).error(R.mipmap.upload_sf2).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf2));
                        }

                        break;

                    case 16:

                        //联系人证件反面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf3).error(R.mipmap.upload_sf3).into(holder.iv_pic);

                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf3));
                        }

                        break;

                    case 17:

                        //联系人手执身份证照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf7).error(R.mipmap.upload_sf7).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf7));
                        }
                        break;
                    case 18:
                        //其他图片一
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 19:
                        //其他图片二
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 20:
                        //其他图片三
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 21:
                        //其他图片四
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 22:
                        //单位证明函
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 23:
                        //银行卡反面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                }

                if (!isClick) {
                    holder.iv_pic.setEnabled(false);
                    holder.rl_upload.setEnabled(false);
                }
                setListener(bean, convertView);
            }

            return convertView;
        }

        private void setListener(final PictureBean bean, final View v) {

            holder.rl_upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageView = v.findViewById(R.id.iv_pic);
                    rl_upload = v.findViewById(R.id.rl_upload);
                    imageNew = v.findViewById(R.id.iv_pic_new);
                    basePath = "";
                    choiceImage(bean);
                }
            });

            holder.iv_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageView = view.findViewById(R.id.iv_pic);
                    rl_upload = v.findViewById(R.id.rl_upload);
                    imageNew = v.findViewById(R.id.iv_pic_new);
                    basePath = "";
                    choiceImage(bean);

                }
            });

        }

        private ViewPayTypeHolder holder;

        private void choiceImage(PictureBean bean) {

            switch (bean.getPicId()) {

                case 1:
                    //营业执照
                    ispicture = "1";
                    basePath = picOnePath;
                    break;
                case 2:
                    //身份证正面照
                    ispicture = "2";
                    basePath = picTowPath;
                    break;
                case 3:
                    //身份证反面照
                    ispicture = "3";
                    basePath = picThreePath;
                    break;
                case 4:
                    //门头照
                    ispicture = "4";
                    basePath = picFourPath;
                    break;
                case 5:
                    //开户许可证
                    ispicture = "5";
                    basePath = picFivePath;
                    break;
                case 6:
                    //银行卡
                    ispicture = "6";
                    basePath = picSixPath;
                    break;
                case 7:
                    //手持身份证
                    ispicture = "7";
                    basePath = picSevenPath;
                    break;
                case 8:
                    //变更申请函
                    ispicture = "8";
                    basePath = picEightPath;
                    break;
                case 9:
                    //第三方授权函
                    ispicture = "9";
                    basePath = picNinePath;
                    break;
                case 10:
                    //工信网证明照
                    ispicture = "10";
                    basePath = picTenPath;
                    break;
                case 11:
                    //经营场所内景照一
                    ispicture = "11";
                    basePath = picElevenPath;
                    break;
                case 12:
                    //经营场所内景照二
                    ispicture = "12";
                    basePath = picTwelvePath;
                    break;
                case 13:
                    //商户协议照
                    ispicture = "13";
                    basePath = picThirteenPath;
                    break;
                case 14:
                    //收银台
                    ispicture = "14";
                    basePath = picFourteenPath;
                    break;
                case 15:
                    //商户协议照
                    ispicture = "15";
                    basePath = picFifteenPath;
                    break;
                case 16:
                    //商户协议照
                    ispicture = "16";
                    basePath = picSixteenPath;
                    break;
                case 17:
                    //商户协议照
                    ispicture = "17";
                    basePath = picSeventeenPath;
                    break;
                case 18:
                    //商户协议照
                    ispicture = "18";
                    basePath = picEighteenPath;
                    break;
                case 19:
                    //商户协议照
                    ispicture = "19";
                    basePath = picNineteenPath;
                    break;
                case 20:
                    //商户协议照
                    ispicture = "20";
                    basePath = picTwentyPath;
                    break;
                case 21:
                    //商户协议照
                    ispicture = "21";
                    basePath = pictwentyOnePath;
                    break;
                case 22:
                    //商户协议照
                    ispicture = "22";
                    basePath = picTwentyTwoPath;
                    break;
                case 23:
                    //商户协议照
                    ispicture = "23";
                    basePath = picTwentyThreePath;
                    break;
            }

//            PermissionUtils.checkPermissionArray(LicenseInfoActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE2);
            choicePic(bean);
        }

        class ViewPayTypeHolder {
            NiceImageView iv_pic, iv_pic_new;
            ImageView iv_upload_icon;
            RelativeLayout rl_upload;
            TextView tv_pic_title;

        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }


    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {

        if (event.getTag().equals(Constants.TAG_SUBMIT_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerChantModifyBean msg = (MerChantModifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LicenseInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_TRUE://请求成功
                    if (msg.isStatus()) {
                        showCommonNoticeDialog(this, getString(R.string.submit_success), new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                MyApplication.isRefresh = true;
                                finish();
                            }
                        });
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_QUERY_ENABLED_CHANGE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            QueryEnabledChangeBean msg = (QueryEnabledChangeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.QUERY_ENABLED_CHANGE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(LicenseInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUERY_ENABLED_CHANGE_TRUE://请求成功
                    QueryEnabledChangeBean.Data data = msg.getData();
                    if (!data.isEditMchCredentials()){
                        setExitEable(false);
                    }
                    break;
            }
        }
    }
}