package com.hstypay.enterprise.activity.merchantInfo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.AddressSelector;
import com.hstypay.enterprise.Widget.BottomDialog;
import com.hstypay.enterprise.Widget.ChoiceDialog;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.ExtendEditText;
import com.hstypay.enterprise.Widget.LegalDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.Widget.TypeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.BasicBean;
import com.hstypay.enterprise.bean.ChoiceBean;
import com.hstypay.enterprise.bean.ImageBean;
import com.hstypay.enterprise.bean.IndustryBean;
import com.hstypay.enterprise.bean.MerChantModifyBean;
import com.hstypay.enterprise.bean.MerchantInfoBean;
import com.hstypay.enterprise.bean.QueryEnabledChangeBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.ImageFactory;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.qiezzi.choseviewlibrary.ChoseCityPicker;
import com.qiezzi.choseviewlibrary.bean.AddressBean;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

//基础信息
public class MerchantBasicActivity extends BaseActivity implements View.OnClickListener, AddressSelector.OnAddressSelectedListener, AddressSelector.OnDialogCloseListener {

    private ImageView iv_back;
    private MerchantInfoBean.DataBean mData;
    private Button btn_submit;
    private RelativeLayout rl_merchant_type;
    private TextView tv_merchant_type;
    private ImageView iv_all_name_delete;
    private EditText et_all_name;
    private ImageView iv_short_name_delete;
    private EditText et_short_name;
    private RelativeLayout rl_industry_type;
    private TextView tv_industry_type;
    private ImageView iv_email_delete;
    private EditText et_email;
    private ImageView iv_service_phone_delete;
    private EditText et_service_phone;
    private RelativeLayout rl_city;
    private TextView tv_merchant_city;
    private ImageView iv_address_delete;
    private EditText et_address;
    private SafeDialog mLoadDialog, mSubmitDialog;
    private ChoseCityPicker mCityPicker;

    private BottomDialog industryDialog;
    private ChoiceDialog dialog;
    private boolean isTag = false;
    private ImageView iv_base_type;
    private ImageView iv_base_location;
    private LegalDialog merchantDialog;
    private LinearLayout ll_license;
    private EditText et_license_id;
    private ImageView iv_license_delete;
    private ImageView iv_main_business_delete;
    private EditText et_main_business;
    private ImageView iv_register_capital_delete;
    private ExtendEditText et_register_capital;
    private RelativeLayout rl_license_valid_type;
    private TextView tv_license_valid_type;
    private TextView tv_license_date;
    private TextView tv_license_valid_date;
    private TextView tv_license_to;
    private TextView tv_license_valid_end;
    private TypeDialog typeDialog;
    private int choiceDate = 0;
    private TimePickerView pvTime;
    private ImageView iv_license_valid_icon;
    private ImageView iv_license_date_icon;
    private ImageView iv_camera;

    private int mchType;
    private int merType;
    private BasicBean basicBean = new BasicBean();
    private boolean validType;

    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    private String picSixPath;
    private String mMerchantId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_basic);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        mData = (MerchantInfoBean.DataBean) getIntent().getSerializableExtra("data");
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("基本信息");
        btn_submit = findViewById(R.id.btn_submit);
        rl_merchant_type = findViewById(R.id.rl_merchant_type);//商户类型
        tv_merchant_type = findViewById(R.id.tv_merchant_type);
        iv_all_name_delete = findViewById(R.id.iv_all_name_delete);
        et_all_name = findViewById(R.id.et_all_name);//商户全称
        iv_short_name_delete = findViewById(R.id.iv_short_name_delete);
        et_short_name = findViewById(R.id.et_short_name);//商户简称
        rl_industry_type = findViewById(R.id.rl_industry_type);
        tv_industry_type = findViewById(R.id.tv_industry_type);
        iv_email_delete = findViewById(R.id.iv_email_delete);
        et_email = findViewById(R.id.et_email);//联系邮箱
        iv_service_phone_delete = findViewById(R.id.iv_service_phone_delete);
        et_service_phone = findViewById(R.id.et_service_phone);//客服电话
        rl_city = findViewById(R.id.rl_city);
        tv_merchant_city = findViewById(R.id.tv_merchant_city);//所在城市
        iv_address_delete = findViewById(R.id.iv_address_delete);
        et_address = findViewById(R.id.et_address);//详细地址
        iv_base_type = findViewById(R.id.iv_base_type);
        iv_base_location = findViewById(R.id.iv_base_location);
        ll_license = findViewById(R.id.ll_license);//营业执照
        et_license_id = findViewById(R.id.et_license_id);
        iv_license_delete = findViewById(R.id.iv_license_delete);
        rl_license_valid_type = findViewById(R.id.rl_license_valid_type);//营业执照有效期类型
        tv_license_valid_type = findViewById(R.id.tv_license_valid_type);
        tv_license_date = findViewById(R.id.tv_license_date);
        tv_license_valid_date = findViewById(R.id.tv_license_valid_date);//营业执照开始日期
        tv_license_to = findViewById(R.id.tv_license_to);
        tv_license_valid_end = findViewById(R.id.tv_license_valid_end);//营业执照结束日期
        iv_license_valid_icon = findViewById(R.id.iv_license_valid_icon);
        iv_license_date_icon = findViewById(R.id.iv_license_date_icon);
        iv_camera = findViewById(R.id.iv_camera);
        iv_main_business_delete = findViewById(R.id.iv_main_business_delete);
        et_main_business = findViewById(R.id.et_main_business);
        iv_register_capital_delete = findViewById(R.id.iv_register_capital_delete);
        et_register_capital = findViewById(R.id.et_register_capital);

        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mSubmitDialog = getLoadDialog(this, UIUtils.getString(R.string.public_submitting), false);
        // isTag=true;
        initTimePicker();
        setButtonEnable(btn_submit, true);
    }

    private void initEvent() {
        iv_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        rl_merchant_type.setOnClickListener(this);
        iv_all_name_delete.setOnClickListener(this);
        iv_short_name_delete.setOnClickListener(this);
        rl_industry_type.setOnClickListener(this);
        iv_email_delete.setOnClickListener(this);
        iv_service_phone_delete.setOnClickListener(this);
        iv_main_business_delete.setOnClickListener(this);
        iv_register_capital_delete.setOnClickListener(this);
        rl_city.setOnClickListener(this);
        iv_address_delete.setOnClickListener(this);
        iv_license_delete.setOnClickListener(this);
        rl_license_valid_type.setOnClickListener(this);
        tv_license_valid_date.setOnClickListener(this);
        tv_license_valid_end.setOnClickListener(this);
        iv_camera.setOnClickListener(this);
        et_register_capital.setOnCommitTextListener(new ExtendEditText.OnCommitTextListener() {
            @Override
            public boolean onCommitText(CharSequence text, int newCursorPosition) {
                LogUtil.d("CharSequence=" + text + "," + newCursorPosition);
                if (et_register_capital.getText().toString().trim().length() == 0 && text.equals(".")) {
                    et_register_capital.setText("0.");
                } else {
                    if (et_register_capital.getText().toString().trim().length() == 1 && et_register_capital.getText().toString().trim().equals("0")) {
                        if (text.equals(".")) {
                            et_register_capital.setText(et_register_capital.getText().append(text));
                        } else {
                            et_register_capital.setText(text);
                        }
                    } else {
                        if (et_register_capital.getText().toString().contains(".")) {
                            if (!text.equals(".") && et_register_capital.getText().toString().trim().indexOf(".") != et_register_capital.getText().toString().trim().length() - 3)
                                et_register_capital.setText(et_register_capital.getText().append(text));
                        } else {
                            et_register_capital.setText(et_register_capital.getText().append(text));
                        }
                    }
                }
                et_register_capital.setSelection(et_register_capital.getText().toString().trim().length());
                return false;
            }

            @Override
            public boolean onDeleteClick() {
                return false;
            }
        });
    }

    private void initData() {
        if (mData != null) {
            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();

            if (mData.getMerchantName() != null) {
                et_all_name.setText(mData.getMerchantName());
            }
            if (mchDetail != null) {
                if (mchDetail.getMerchantShortName() != null) {
                    et_short_name.setText(mchDetail.getMerchantShortName());
                }
                if (mchDetail.getIndustryName() != null) {
                    tv_industry_type.setText(mchDetail.getIndustryName());
                }
                if (mchDetail.getEmail() != null) {

                    et_email.setText(mchDetail.getEmail());
                }
                if (mchDetail.getServicePhone() != null) {
                    et_service_phone.setText(mchDetail.getServicePhone());
                }
                if (mchDetail.getProvinceName() != null && mchDetail.getCityName() != null && mchDetail.getCountyName() != null) {
                    tv_merchant_city.setText(mchDetail.getProvinceName() + "/" + mchDetail.getCityName() + "/" + mchDetail.getCountyName());
                }
                if (mchDetail.getAddress() != null) {
                    et_address.setText(mchDetail.getAddress());
                }
                if (mchDetail.getLicenseCode() != null) {

                    et_license_id.setText(mchDetail.getLicenseCode());
                }
                basicBean.setIndustryId(mchDetail.getIndustryId());
                basicBean.setProvince(mchDetail.getProvince());
                basicBean.setCity(mchDetail.getCity());
                basicBean.setCounty(mchDetail.getCounty());

                if (mchDetail.getLicenseCode() != null) {
                    basicBean.setLicenseCode(mchDetail.getLicenseCode());
                }
                basicBean.setLicenseExpiredFlag(mchDetail.isLicenseExpiredFlag());
                basicBean.setLicenseExpiredBegin(mchDetail.getLicenseExpiredBegin());

                if (mchDetail.getLicenseExpiredDate() != null) {
                    basicBean.setLicenseExpiredDate(mchDetail.getLicenseExpiredDate());
                }
                if (!TextUtils.isEmpty(mchDetail.getCapital())) {
                    BigDecimal capital = new BigDecimal(mchDetail.getCapital()).divide(new BigDecimal(1000000)).setScale(2, RoundingMode.HALF_UP);
                    et_register_capital.setText(StringUtils.getNoMoreThanTwoDigits(capital.toString()));
                }
                if (mchDetail.getBusinScope() != null) {
                    et_main_business.setText(mchDetail.getBusinScope());
                }

                if (mchDetail.getLicenseExpiredDate() != null) {

                    tv_license_valid_end.setText(DateUtil.formartDateToYYMMDD(mchDetail.getLicenseExpiredDate()));
                }

                if (mchDetail.getLicenseExpiredBegin() != null) {
                    tv_license_valid_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getLicenseExpiredBegin()));
                }
                if (mchDetail.isLicenseExpiredFlag()) {
                    //长期
                    tv_license_valid_type.setText("长期");
                    tv_license_date.setText("证件开始日期");
                    tv_license_to.setVisibility(View.GONE);
                    tv_license_valid_end.setVisibility(View.GONE);
                    if (mchDetail.getLicenseExpiredBegin() != null) {
                        tv_license_valid_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getLicenseExpiredBegin()));
                        validType = true;
                    }
                } else {
                    tv_license_valid_type.setText("非长期");
                    tv_license_date.setText("证件有效期");
                    //非长期
                    if (mchDetail.getLicenseExpiredBegin() != null) {
                        tv_license_valid_date.setText(DateUtil.formartDateToYYMMDD(mchDetail.getLicenseExpiredBegin()));
                    }
                    if (mchDetail.getLicenseExpiredDate() != null) {

                        tv_license_valid_end.setText(DateUtil.formartDateToYYMMDD(mchDetail.getLicenseExpiredDate()));
                    }

                    validType = false;
                }

            }

            switch (mData.getOutMchType()) {
                case 1:
                    //企业商户
                    if (mData.getOutMchTypeCnt() != null) {
                        tv_merchant_type.setText(mData.getOutMchTypeCnt());
                    } else {
                        tv_merchant_type.setText("企业商户");
                    }

                    mchType = 1;

                    merType = 1;
                    break;
                case 2:
                    //个体工商户
                    if (mData.getOutMchTypeCnt() != null) {
                        tv_merchant_type.setText(mData.getOutMchTypeCnt());
                    } else {
                        tv_merchant_type.setText("个体工商户");
                    }
                    mchType = 2;
                    merType = 2;
                    break;
                case 3:
                    //个体经营者
                    if (mData.getOutMchTypeCnt() != null) {
                        tv_merchant_type.setText(mData.getOutMchTypeCnt());
                    } else {
                        tv_merchant_type.setText("个体经营者");
                    }
                    mchType = 3;
                    merType = 3;
                    ll_license.setVisibility(View.GONE);
                    break;
                case 4:
                    //事业单位
                    mchType = 4;
                    merType = 4;
                    if (mData.getOutMchTypeCnt() != null) {
                        tv_merchant_type.setText(mData.getOutMchTypeCnt());
                    } else {
                        tv_merchant_type.setText("事业单位");
                    }
                    break;
            }


            switch (mData.getMchQueryStatus()) {//examineStatus

                case -1:
                    //初始商户
                    setExitEable(false);
                    break;
                /*case 1:
                    //审核通过
                    if (mData.getAttestationStatus() != 1 || mData.getTradeStatus() == 0) {
                        setExitEable(false);
                    } else if (isChangeInfo()) {
                        queryEnabledChange();
                    }

                    break;*/
                case 5:
                    //作废商户
                    setExitEable(false);
                    break;
                /*case 10:
                    //变更审核通过
                    setExitEable(false);
                    break;*/
                case 101:
                    //冻结商户
                    setExitEable(false);
                    break;
                //case 3://修改待确认
                default:
                    if (isChangeInfo()) {
                        queryEnabledChange();
                    }
                    break;
            }


        }

        setEdittextListener(et_all_name, iv_all_name_delete);
        setEdittextListener(et_short_name, iv_short_name_delete);
        setEdittextListener(et_email, iv_email_delete);
        setEdittextListener(et_service_phone, iv_service_phone_delete);
        setEdittextListener(et_address, iv_address_delete);
        setEdittextListener(et_license_id, iv_license_delete);
        setEdittextListener(et_main_business, iv_main_business_delete);
        setEdittextListener(et_register_capital, iv_register_capital_delete);
    }

    //是否是变更审核
    private boolean isChangeInfo() {
        if (mData == null) {
            return false;
        }
        /*if ((mData.getTradeStatus() == Constants.HPMerchantTradeAvailable || mData.getTradeStatus() == Constants.HPMerchantTradeSomeAvailable)
                && mData.getAttestationStatus() == Constants.HPMerchantAuthenticateStatusPass) {
            return true;
        }*/
        if (mData.getMchQueryStatus() == 1 || mData.getMchQueryStatus() == 100 || mData.getMchQueryStatus() == 10) {
            //case 1://审核通过
            //case 100://可交易
            //case 10://变更审核通过
            return true;
        }
        return false;
    }

    //验证商户资料是否允许修改
    private void queryEnabledChange() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            DialogUtil.safeShowDialog(mLoadDialog);
            map.put("merchantId", mMerchantId);
            ServerClient.newInstance(MyApplication.getContext()).queryEnabledChange(MyApplication.getContext(), Constants.TAG_QUERY_ENABLED_CHANGE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }


    private void setExitEable(boolean tag) {

        if (!tag) {
            btn_submit.setVisibility(View.GONE);
            iv_base_type.setVisibility(View.GONE);
            rl_merchant_type.setEnabled(false);
            et_all_name.setEnabled(false);
            et_short_name.setEnabled(false);
            rl_industry_type.setEnabled(false);
            et_email.setEnabled(false);
            et_service_phone.setEnabled(false);
            rl_city.setEnabled(false);
            et_address.setEnabled(false);
            iv_base_location.setVisibility(View.GONE);
            et_main_business.setEnabled(false);
            et_register_capital.setEnabled(false);
            tv_merchant_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_all_name.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_short_name.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_industry_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_email.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_service_phone.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_merchant_city.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_address.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_main_business.setTextColor(getResources().getColor(R.color.tv_rb_color));
            et_register_capital.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_all_name_delete.setVisibility(View.GONE);
            et_license_id.setEnabled(false);
            iv_license_delete.setVisibility(View.GONE);
            rl_license_valid_type.setEnabled(false);
            tv_license_valid_date.setEnabled(false);
            tv_license_valid_end.setEnabled(false);
            iv_camera.setVisibility(View.GONE);

            tv_license_valid_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_license_valid_icon.setVisibility(View.GONE);
            tv_license_valid_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_to.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_valid_end.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_license_date_icon.setVisibility(View.GONE);
            et_license_id.setTextColor(getResources().getColor(R.color.tv_rb_color));
        }

    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
      /*  Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -3);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));*/
        //时间选择器
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date);
                if (choiceDate > 0) {
                    switch (choiceDate) {
                        case 1:
                            //营业执照开始日期
                            if (!validType) {
                                if (!StringUtils.isEmptyOrNull(tv_license_valid_end.getText().toString())) {
                                    Date date2 = DateUtil.dateto(time);
                                    Date dateto = DateUtil.dateto(tv_license_valid_end.getText().toString());
                                    int compareTo = date2.compareTo(dateto);
                                    if (compareTo > 0) {
                                        ToastUtil.showToastShort("开始日期不能大于结束日期");
                                        return;
                                    }
                                }
                            }
                            tv_license_valid_date.setText(time);
                            basicBean.setLicenseExpiredBegin(time + " 00:00:00");
                            break;
                        case 2:
                            //营业执照结束日期
                            if (!validType) {
                                if (!StringUtils.isEmptyOrNull(tv_license_valid_date.getText().toString())) {
                                    Date date2 = DateUtil.dateto(time);
                                    Date dateto = DateUtil.dateto(tv_license_valid_date.getText().toString());
                                    int compareTo = date2.compareTo(dateto);
                                    if (compareTo < 0) {
                                        ToastUtil.showToastShort("结束日期不能小于开始日期");
                                        return;
                                    }
                                }
                            }
                            tv_license_valid_end.setText(time);
                            basicBean.setLicenseExpiredDate(time + " 00:00:00");
                            break;

                    }
                }

            }
        })
                .setLayoutRes(R.layout.layout_pickerview_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        Button btnCancel = v.findViewById(R.id.btnCancel);
                        Button btnSubmit = v.findViewById(R.id.btnSubmit);
                        btnCancel.setOnClickListener(MerchantBasicActivity.this);
                        btnSubmit.setOnClickListener(MerchantBasicActivity.this);
                    }
                })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                // .setRangDate(startDate, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年", "月", "日", null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    private void setEdittextListener(final EditText editText, final ImageView imageView) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }

            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点
                    if (editText.getText().length() > 0) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                //提交
                if (StringUtils.isEmptyOrNull(tv_industry_type.getText().toString().trim())) {
                    ToastUtil.showToastShort("请输入行业类型");
                    return;
                }
                if (mchType != 3) {
                    if (StringUtils.isEmptyOrNull(et_license_id.getText().toString().trim())) {
                        ToastUtil.showToastShort("营业执照号不能为空");
                        et_address.setFocusable(true);
                        et_address.setFocusableInTouchMode(true);
                        et_address.requestFocus();
                        return;
                    }
                    if (!validType) {
                        if (StringUtils.isEmptyOrNull(tv_license_valid_end.getText().toString().trim())) {
                            ToastUtil.showToastShort("请选择营业执照结束日期");
                            return;
                        }
                        if (StringUtils.isEmptyOrNull(tv_license_valid_date.getText().toString().trim())) {
                            ToastUtil.showToastShort("请选择营业执照开始日期");
                            return;
                        }
                    }
                }
                if (StringUtils.isEmptyOrNull(et_all_name.getText().toString().trim())) {
                    ToastUtil.showToastShort("请输入商户全称");
                    et_all_name.setFocusable(true);
                    et_all_name.setFocusableInTouchMode(true);
                    et_all_name.requestFocus();
                    return;
                }
                if (StringUtils.isEmptyOrNull(et_short_name.getText().toString().trim())) {
                    ToastUtil.showToastShort("请输入商户简称");
                    et_short_name.setFocusable(true);
                    et_short_name.setFocusableInTouchMode(true);
                    et_short_name.requestFocus();
                    return;
                }

                if (StringUtils.isEmptyOrNull(et_email.getText().toString().trim())) {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.hint_email));
                    et_email.setFocusable(true);
                    et_email.setFocusableInTouchMode(true);
                    et_email.requestFocus();
                    return;
                }
                if (StringUtils.isEmptyOrNull(et_service_phone.getText().toString().trim())) {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.hint_service_phone));
                    et_service_phone.setFocusable(true);
                    et_service_phone.setFocusableInTouchMode(true);
                    et_service_phone.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(et_address.getText().toString().trim())) {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.regex_merchant_detail_address));
                    et_address.setFocusable(true);
                    et_address.setFocusableInTouchMode(true);
                    et_address.requestFocus();
                    return;
                }
                subDialog();
                break;
            case R.id.rl_merchant_type:
                //商户类型选择
                choiceDialog();
                break;
            case R.id.iv_all_name_delete:
                et_all_name.setText("");
                iv_all_name_delete.setVisibility(View.GONE);
                break;
            case R.id.iv_short_name_delete:
                et_short_name.setText("");
                iv_short_name_delete.setVisibility(View.GONE);
                break;
            case R.id.rl_industry_type:
                //行业类型选择
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    map.put("merchantClass", mchType);
                    ServerClient.newInstance(MyApplication.getContext()).getIndustryType(MyApplication.getContext(), Constants.TAG_GET_INDUSTRY_TYPE, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                }
                break;
            case R.id.iv_email_delete:
                et_email.setText("");
                iv_email_delete.setVisibility(View.GONE);
                break;
            case R.id.iv_service_phone_delete:
                et_service_phone.setText("");
                iv_service_phone_delete.setVisibility(View.GONE);
                break;
            case R.id.rl_city:
                //城市选择
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    ServerClient.newInstance(MyApplication.getContext()).getAddress(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_ADDRESS, null);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                }
                break;
            case R.id.iv_address_delete:
                et_address.setText("");
                iv_address_delete.setVisibility(View.GONE);
                break;
            case R.id.iv_license_delete:
                iv_license_delete.setVisibility(View.GONE);
                et_license_id.setText("");
                break;
            case R.id.iv_main_business_delete:
                et_main_business.setText("");
                iv_main_business_delete.setVisibility(View.GONE);
                break;
            case R.id.iv_register_capital_delete:
                et_register_capital.setText("");
                iv_register_capital_delete.setVisibility(View.GONE);
                break;
            case R.id.rl_license_valid_type:
                //营业执照有效期类型
                idType(1);
                break;
            case R.id.tv_license_valid_date:
                //营业执照开始日期
                choiceDate = 1;
                pvTime.show();
                break;
            case R.id.tv_license_valid_end:
                //营业执照结束日期
                choiceDate = 2;
                pvTime.show();
                break;
            case R.id.btnCancel:
                pvTime.dismiss();
                break;
            case R.id.btnSubmit:
                pvTime.returnData();
                pvTime.dismiss();
                break;
            case R.id.iv_camera:
                choice();
                break;
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(MerchantBasicActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    boolean results = PermissionUtils.checkPermissionArray(MerchantBasicActivity.this, permissionArray);
                    if (results) {
                        startCamera();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_photo));
                    }
                } else {
                    (MerchantBasicActivity.this).showCommonNoticeDialog(MerchantBasicActivity.this, getString(R.string.tx_sd_pic));
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    boolean results = PermissionUtils.checkPermissionArray(MerchantBasicActivity.this, permissionArray);
                    if (results) {
                        takeImg();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_photo));
                    }
                } else {
                    (MerchantBasicActivity.this).showCommonNoticeDialog(MerchantBasicActivity.this, getString(R.string.tx_sd_pic));
                }
            }
        });
        picPopupWindow.showAtLocation(tv_merchant_type, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private String imageUrl;
    private File tempFile;
    private Uri originalUri;

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";

            tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()) {
                tempFile.getParentFile().mkdirs();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(MerchantBasicActivity.this,
                        Constants.FILE_PROVIDER, tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        /*Intent intent = new Intent();
        if (Build.VERSION.SDK_INT < 19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
        } else {
            intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,"image/*");
        }*/
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data == null || data.getData() == null) {
                        return;
                    }
                    String path = AppHelper.getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            picSixPath = path;
                            uploadImage(picSixPath, "licenseImg");
                        }
                    }
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = AppHelper.getPicPath(originalUri);
                    Bitmap bitmap_pci;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        picSixPath = pathPhoto;
                        uploadImage(picSixPath, "licenseImg");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void uploadImage(String path, final String type) {
        if (!NetworkUtils.isNetworkAvailable(MerchantBasicActivity.this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
            return;
        }
        DialogUtil.safeShowDialog(mLoadDialog);

        String url = Constants.BASE_URL + "merchant/upload/file";
        PostFormBuilder post = OkHttpUtils.post();
        String pathCompress = null;
        if (!TextUtils.isEmpty(path)) {
            //pathCompress = path.substring(0, path.lastIndexOf(".")) + "(1).jpg";
            pathCompress = AppHelper.getImageCacheDir(path);
            ImageFactory.compressPicture(path, pathCompress);
            post = post.addFile(type, type, new File(pathCompress));
        }
        final String finalPathCompress = pathCompress;
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                DialogUtil.safeCloseDialog(mLoadDialog);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(finalPathCompress);
            }

            @Override
            public void onResponse(String response, int id) {
                FileUtils.deleteFile(finalPathCompress);
                DialogUtil.safeCloseDialog(mLoadDialog);
                LogUtil.d("zhouwei", response);
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null) {
                    if (imageBean.isStatus()) {
                        ImageBean.DataEntity imageInfo = imageBean.getData();
                        if (imageInfo != null) {
                            ImageBean.DataEntity.BusinessLicenseEntity businessLicense = imageInfo.getBusinessLicense();
                            if (businessLicense != null) {
                                String name = businessLicense.getName();//商户名称
                                String creditno = businessLicense.getCreditno();//营业执照号
                                String enddate = businessLicense.getEnddate();//长期
                                et_all_name.setText(name);
                                et_all_name.setSelection(et_all_name.getText().toString().trim().length());
                                et_license_id.setText(creditno);
                                et_license_id.setSelection(et_license_id.getText().toString().trim().length());
                                tv_license_valid_type.setText(enddate);
                                et_main_business.setText(businessLicense.getScope());
                                et_main_business.setSelection(et_main_business.getText().toString().trim().length());
                                et_register_capital.setText(StringUtils.getNoMoreThanTwoDigits(businessLicense.getRegcapital()));
                                et_register_capital.setSelection(et_register_capital.getText().toString().trim().length());
                            }
                        }


                    } else {
                        if (imageBean.getError() != null && imageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(imageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    /**
     * 获取应用详情页面intent
     *
     * @return
     */
    public static Intent getAppDetailSettingIntent(Context context) {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            localIntent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        return localIntent;
    }

    private void idType(final int type) {
        typeDialog = new TypeDialog(MerchantBasicActivity.this, new TypeDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String s) {
                switch (type) {
                    case 1:
                        //营业执照有效期
                        if (!StringUtils.isEmptyOrNull(s)) {
                            if (s.equals("1")) {
                                //长期
                                tv_license_valid_type.setText("长期");
                                tv_license_date.setText("证件开始日期");
                                tv_license_to.setVisibility(View.GONE);
                                tv_license_valid_end.setVisibility(View.GONE);
                                basicBean.setLicenseExpiredFlag(true);
                                validType = true;

                            } else {
                                tv_license_valid_type.setText("非长期");
                                tv_license_date.setText("证件有效期");
                                tv_license_to.setVisibility(View.VISIBLE);
                                tv_license_valid_end.setVisibility(View.VISIBLE);
                                basicBean.setLicenseExpiredFlag(false);
                                validType = false;
                            }
                        }
                        break;
                }
                typeDialog.dismiss();
            }
        });
        DialogHelper.resizeFull(MerchantBasicActivity.this, typeDialog);
        typeDialog.show();
    }


    private void subDialog() {


        merchantDialog = new LegalDialog(MerchantBasicActivity.this, "提示", "本次修改是否保存及提交？",
                "提交", "放弃", "merchant", new LegalDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String s) {

                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("merchantId", mData.getMerchantId());
                    map.put("merchantName", et_all_name.getText().toString().trim());
                    map.put("outMchType", mchType);
                    basicBean.setMerchantShortName(et_short_name.getText().toString().trim());
                    basicBean.setEmail(et_email.getText().toString().trim());
                    basicBean.setServicePhone(et_service_phone.getText().toString().trim());
                    basicBean.setAddress(et_address.getText().toString().trim());
                    basicBean.setBusinScope(et_main_business.getText().toString().trim());
                    if (TextUtils.isEmpty(et_register_capital.getText().toString().trim())) {
                        basicBean.setCapital(et_register_capital.getText().toString().trim());
                    } else {
                        BigDecimal capital = new BigDecimal(et_register_capital.getText().toString().trim()).multiply(new BigDecimal(1000000));
                        basicBean.setCapital(StringUtils.getNoMoreThanTwoDigits(capital.toString()));
                    }

                    if (mchType != 3) {
                        //非个体经营者
                        basicBean.setLicenseCode(et_license_id.getText().toString().trim());
                    }
                    map.put("detail", basicBean);
                    merchantDialog.dismiss();
                    DialogUtil.safeShowDialog(mSubmitDialog);
                    ServerClient.newInstance(MyApplication.getContext()).submitMerchantInfo(MyApplication.getContext(), Constants.TAG_SUBMIT_MERCHANT_INFO, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                }

            }


            @Override
            public void handleCancleBtn() {

                merchantDialog.dismiss();
            }
        });

        DialogHelper.resize(this, merchantDialog);
        merchantDialog.show();

    }

    private void choiceDialog() {

        List<ChoiceBean> mList = new ArrayList<>();
        ChoiceBean bean = new ChoiceBean();
        bean.setId(1);
        bean.setName("企业商户");
        ChoiceBean bean1 = new ChoiceBean();
        bean1.setId(2);
        bean1.setName("事业单位");
        ChoiceBean bean2 = new ChoiceBean();
        bean2.setId(3);
        bean2.setName("个体工商户");
        ChoiceBean bean3 = new ChoiceBean();
        bean3.setId(4);
        bean3.setName("个体经营者");
        mList.add(bean);
        mList.add(bean1);
        mList.add(bean2);
        mList.add(bean3);

        dialog = new ChoiceDialog(MerchantBasicActivity.this, mList, new ChoiceDialog.HandleBtn() {
            @Override
            public void handleOkBtn(ChoiceBean s) {
                dialog.dismiss();

                if (s != null) {
                    tv_merchant_type.setText(s.getName());

                    switch (s.getId()) {

                        case 1:
                            if (merType != 1) {

                                tv_industry_type.setText("");
                            }
                            ll_license.setVisibility(View.VISIBLE);
                            mchType = 1;

                            break;
                        case 2:
                            if (merType != 4) {

                                tv_industry_type.setText("");
                            }
                            ll_license.setVisibility(View.VISIBLE);
                            mchType = 4;
                            break;
                        case 3:
                            if (merType != 2) {

                                tv_industry_type.setText("");
                            }
                            ll_license.setVisibility(View.VISIBLE);
                            mchType = 2;
                            break;
                        case 4:
                            if (merType != 3) {

                                tv_industry_type.setText("");
                            }
                            ll_license.setVisibility(View.GONE);
                            mchType = 3;
                            break;
                    }

                }

            }
        });
        DialogHelper.resize(MerchantBasicActivity.this, dialog);
        dialog.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {

        //行业
        if (event.getTag().equals(Constants.TAG_GET_INDUSTRY_TYPE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            IndustryBean msg = (IndustryBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.GET_INDUSTRY_TYPE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantBasicActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_INDUSTRY_TYPE_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        showIndustryDiaog(msg);
                    }
                    break;
            }
            dismissLoading();
        }

        //省市区
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_ADDRESS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            AddressBean msg = (AddressBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.GET_ADDRESS_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantBasicActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_ADDRESS_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        area(msg);
                    }
                    break;
            }
        }


        if (event.getTag().equals(Constants.TAG_SUBMIT_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mSubmitDialog);
            MerChantModifyBean msg = (MerChantModifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantBasicActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_TRUE://请求成功
                    if (msg.isStatus()) {
                        showCommonNoticeDialog(this, getString(R.string.submit_success), new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                MyApplication.isRefresh = true;
                                finish();
                            }
                        });
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_QUERY_ENABLED_CHANGE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            QueryEnabledChangeBean msg = (QueryEnabledChangeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.QUERY_ENABLED_CHANGE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantBasicActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUERY_ENABLED_CHANGE_TRUE://请求成功
                    QueryEnabledChangeBean.Data data = msg.getData();
                    if (!data.isEditMchBasic()) {
                        setExitEable(false);
                    }
                    break;
            }
        }
    }


    private void area(AddressBean addressBean) {
        mCityPicker = new ChoseCityPicker(MerchantBasicActivity.this, addressBean);
        mCityPicker.setOnGetAddress(new ChoseCityPicker.OnGetAddress() {
            @Override
            public void getAddress(String province, String city, String area) {
                //获取省市区地址
                tv_merchant_city.setText(province + "/" + city + "/" + area);
            }
        });
        mCityPicker.setOnGetAddressCode(new ChoseCityPicker.OnGetAddressCode() {
            @Override
            public void getAddressCode(String province, String city, String area) {
                //获取省市区code
                if (province != null) {

                    basicBean.setProvince(province);
                }

                if (city != null) {

                    basicBean.setCity(city);
                }

                if (area != null) {

                    basicBean.setCounty(area);
                }

            }
        });
        mCityPicker.show();
    }

    private void showIndustryDiaog(IndustryBean industryBean) {
       /* if (industryDialog != null) {
            DialogHelper.resizeHeight(this, industryDialog, 0.6f);
            industryDialog.show();
        } else {*/
        industryDialog = new BottomDialog(this, industryBean);
        industryDialog.setOnAddressSelectedListener(this);
        industryDialog.setDialogDismisListener(this);
        industryDialog.setTextSize(16);//设置字体的大小
        industryDialog.setIndicatorBackgroundColor(R.color.theme_color);//设置指示器的颜色
        industryDialog.setTextSelectedColor(R.color.theme_color);//设置字体获得焦点的颜色
        industryDialog.setTextUnSelectedColor(R.color.tv_unchecked_color);//设置字体没有获得焦点的颜色
        DialogHelper.resizeHeight(this, industryDialog, 0.6f);
        industryDialog.show();
        //  }
    }

    String type;

    @Override
    public void onAddressSelected(IndustryBean.Industry1Entity industry1, IndustryBean.Industry1Entity.Industry2Entity industry2, IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity industry3) {

        if (industryDialog != null) {
            industryDialog.dismiss();
        }


        switch (mchType) {

            case 1:
                type = "企业商户";
                break;
            case 2:
                type = "个体工商户";
                break;
            case 3:
                type = "个体经营者";
                break;
            case 4:
                type = "事业单位";
                break;
        }

        if (industry1 != null) {
            tv_industry_type.setText(type + "-" + industry1.getIndustryName());
            basicBean.setIndustryId(industry1.getIndustryId());

            if (industry2 != null) {
                tv_industry_type.setText(type + "-" + industry1.getIndustryName() + "-" + industry2.getIndustryName());
                basicBean.setIndustryId(industry2.getIndustryId());

                if (industry3 != null) {
                    tv_industry_type.setText(type + "-" + industry1.getIndustryName() + "-" + industry2.getIndustryName() +
                            "-" + industry3.getIndustryName());
                    basicBean.setIndustryId(industry3.getIndustryId());
                }
            }

        }

    }

    @Override
    public void dialogclose() {
        if (industryDialog != null) {
            industryDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
