package com.hstypay.enterprise.activity.merchantInfo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.bankcard.ChangeCompanyCardActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.MerchantInfoBean;
import com.hstypay.enterprise.bean.SettleChangeCountBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class MerchantInfoActivity extends BaseActivity implements View.OnClickListener {

    public static final int INITCHANGEAVAILDCOUNT = 100;
    private ImageView iv_back;
    private String propValue = "审核失败";
    private String mMerchantTel, mMerchantId, mMerchantName;
    private TextView tv_title, tv_modify, mTvPayTypeStatus,mTvNotice;
    private SafeDialog mLoadDialog, mLoadPayTypeDialog;
    private MerchantInfoBean.DataBean mData;
    private TextView tv_merchant_name;
    private TextView tv_merchand_num;
    private RelativeLayout rl_basic_info;
    private RelativeLayout rl_person_info;
    private RelativeLayout rl_settle_info;
    private RelativeLayout rl_credentials_info;
    private RelativeLayout rl_pay_type_info;
    private ImageView iv_state;
    private TextView tv_state;
    private TextView tv_fail_info;
    private LinearLayout ll_fail;
    private TextView mTvCountChangeSettle;
    private int mChangeAcountCount= INITCHANGEAVAILDCOUNT;//剩余可修改次数,初始化为100表示可修改否不受改字段控制

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_info);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(UIUtils.getString(R.string.title_merchant_info));
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mLoadPayTypeDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        tv_merchant_name = findViewById(R.id.tv_merchant_name);
        tv_merchand_num = findViewById(R.id.tv_merchand_num);
        rl_basic_info = findViewById(R.id.rl_basic_info);//基础信息
        rl_person_info = findViewById(R.id.rl_person_info);//法人信息
        rl_settle_info = findViewById(R.id.rl_settle_info);//结算信息
        mTvCountChangeSettle = findViewById(R.id.tv_count_change_settle);//商户剩余修改结算信息次数
        rl_credentials_info = findViewById(R.id.rl_credentials_info);//证件信息
        rl_pay_type_info = findViewById(R.id.rl_pay_type_info);//支付类型
        iv_state = findViewById(R.id.iv_state);
        tv_state = findViewById(R.id.tv_state);
        tv_fail_info = findViewById(R.id.tv_fail_info);
        ll_fail = findViewById(R.id.ll_fail);
        tv_modify = findViewById(R.id.tv_modify);
        mTvNotice = findViewById(R.id.tv_change_bank_card_notice);
    }

    private void initEvent() {
        iv_back.setOnClickListener(this);
        rl_basic_info.setOnClickListener(this);
        rl_person_info.setOnClickListener(this);
        rl_settle_info.setOnClickListener(this);
        rl_credentials_info.setOnClickListener(this);
        rl_pay_type_info.setOnClickListener(this);
        tv_modify.setOnClickListener(this);
    }

    private void initData() {
        mMerchantTel = getIntent().getStringExtra(Constants.INTENT_MERCHANT_TEL);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mMerchantName = getIntent().getStringExtra(Constants.INTENT_MERCHANT_NAME);
        mTvPayTypeStatus = findViewById(R.id.tv_pay_type_status);//支付类型
        getData(mMerchantId);
    }

    private void getData(String merchantId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            ServerClient.newInstance(MyApplication.getContext()).getMerchantInfo(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_basic_info:
                //基本信息
                Intent intent = new Intent(MerchantInfoActivity.this, MerchantBasicActivity.class);
                intent.putExtra("data", mData);
                intent.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                startActivity(intent);
                break;
            case R.id.rl_person_info:
                //法人信息
                Intent legalIntent = new Intent(MerchantInfoActivity.this, LegalPersonActivity.class);
                legalIntent.putExtra("data", mData);
                legalIntent.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                startActivity(legalIntent);
                break;
            case R.id.rl_settle_info:
                //结算信息
                Intent intentCard = new Intent();
                intentCard.putExtra(Constants.INTENT_BANK_DETAIL, mData);
                intentCard.putExtra(Constants.INTENT_CHANGEACOUNTCOUNT,mChangeAcountCount);
                intentCard.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                //if (mData.getMerchantClass() == 1) {
                intentCard.setClass(MerchantInfoActivity.this, ChangeCompanyCardActivity.class);
                // } else if (mData.getMerchantClass() == 2) {
                //    intentCard.setClass(MerchantInfoActivity.this, ChangeCardActivity.class);
                // }
                // startActivityForResult(intentCard, Constants.REQUEST_MERCHANT_CARD);
//                startActivity(intentCard);
                startActivityForResult(intentCard, Constants.REQUEST_MERCHANT_CARD);
                break;
            case R.id.rl_credentials_info:
                //上传证件照
                Intent UPintent = new Intent(this, LicenseInfoActivity.class);
                UPintent.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                if (mData != null) {
                    UPintent.putExtra(Constants.INTENT_NAME_MERCHANT_UPLOAD, mData);
                }
                startActivityForResult(UPintent, Constants.REQUEST_UPLOAD_IMAGE);
                break;
            case R.id.rl_pay_type_info:
                //支付类型
                Intent intentPayType = new Intent(this, PayTypeActivity.class);
                intentPayType.putExtra(Constants.INTENT_MERCHANT_TEL, mMerchantTel);
                intentPayType.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                startActivity(intentPayType);
                break;
            case R.id.tv_modify:
                startActivity(new Intent(this, MerchantModifyListActivity.class));
                break;
        }
    }

    //查询商户剩余修改结算信息次数
    private void getSettleChangeCount() {
        if (isChangeInfo()){
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadPayTypeDialog);
                Map<String, Object> map = new HashMap<>();
                map.put("merchantId", mMerchantId);
                ServerClient.newInstance(MyApplication.getContext()).getSettleChangeCount(MyApplication.getContext(), Constants.TAG_GET_SETTLE_CHANGE_COUNT, map);
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
            }
        }
    }

    //是否是变更审核
    private boolean isChangeInfo(){
        if (mData==null){
            return false;
        }
        /*if ((mData.getTradeStatus()== Constants.HPMerchantTradeAvailable ||mData.getTradeStatus() == Constants.HPMerchantTradeSomeAvailable )
                && mData.getAttestationStatus() == Constants.HPMerchantAuthenticateStatusPass){
            return true;
        }*/
        if (mData.getMchQueryStatus() == 1 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
            //case 1://审核通过
            //case 100://可交易
            //case 10://变更审核通过
            return true;
        }
        return false;
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantInfoBean msg = (MerchantInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.GET_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_INFO_TRUE://请求成功
                    if (msg.getData() != null) {
                        mData = msg.getData();
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_WXREG_STATUS, mData.getWxRegStatus());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_ALIREG_STATUS, mData.getAliRegStatus());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, mData.getAttestationStatus());//商户认证状态
                        setView(mData);
                        getSettleChangeCount();
                    }
                    break;
            }
        }else if (event.getTag().equals(Constants.TAG_GET_SETTLE_CHANGE_COUNT)){
            DialogUtil.safeCloseDialog(mLoadPayTypeDialog);
            SettleChangeCountBean msg = (SettleChangeCountBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.GET_SETTLE_CHANGE_COUNT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_SETTLE_CHANGE_COUNT_TRUE://请求成功
                    if (msg.getData() != null) {
                        mChangeAcountCount = msg.getData();
                        if (mChangeAcountCount<0 ||mChangeAcountCount==0){
                            mTvCountChangeSettle.setText("本月修改机会已用完");
                        }else {
                            mTvCountChangeSettle.setText("本月可修改1次");

                        }
                    }
                    break;
            }
        }
    }

    private void setView(MerchantInfoBean.DataBean mData) {

        if (mData != null) {
            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
            if (mchDetail != null && mchDetail.getPrincipal() != null) {
                tv_merchant_name.setText(mchDetail.getPrincipal() + "-" + mData.getMerchantName());
            }

            if (mData.getMerchantId() != null) {
                tv_merchand_num.setText("商户编号： " + mData.getMerchantId());
            }
            if (TextUtils.isEmpty(mMerchantId)){
                mMerchantId = mData.getMerchantId();
            }
            switch (mData.getTradeStatus()) {
                case 0:
                    mTvPayTypeStatus.setText("不可交易");
                    break;
                case 10:
                    mTvPayTypeStatus.setText("可交易");
                    break;
                case 20:
                    mTvPayTypeStatus.setText("部分可交易");
                    break;
            }
            switch (mData.getMchQueryStatus()) {

                case -1:
                    //初始商户
                    iv_state.setImageResource(R.mipmap.checking_icon);
                    tv_state.setText("未进件");
                    break;
                case 0:
                    //审核中
                    iv_state.setImageResource(R.mipmap.checking_icon);
                    tv_state.setText("商户资料变更已提交，正在审核中");
                    break;
                case 1:
                    //审核通过
                    if (mData.getAttestationStatus() == 1 && mData.getTradeStatus() != 0){
                        iv_state.setImageResource(R.mipmap.icon_check_successful);
                        tv_state.setText("恭喜您，商户已可正常交易");
                    } else {
                        iv_state.setImageResource(R.mipmap.check_pass_icon);
                        tv_state.setText("恭喜您，商户已通过平台审核，正在报备支付机构");
                    }
                    break;
                case 2:
                    //审核不通过
                    iv_state.setImageResource(R.mipmap.check_fail_icon);
                    tv_state.setText("商户资料审核不通过");
                    if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                        ll_fail.setVisibility(View.VISIBLE);
                        String s = "失败原因为：" + mData.getExamineRemark();
                        SpannableString spantwo = new SpannableString(s);
                        spantwo.setSpan(new ForegroundColorSpan(Color.parseColor("#FD414B")), 6, s.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        tv_fail_info.setText(spantwo);

                    }
                    break;
                case 3:
                    //变更审核中
                    iv_state.setImageResource(R.mipmap.checking_icon);
                    tv_state.setText("商户资料变更已提交，正在审核中");
                    break;
                case 4:
                    //修改审核不通过
                    iv_state.setImageResource(R.mipmap.check_fail_icon);
                    tv_state.setText("商户资料审核不通过");
                    if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                        ll_fail.setVisibility(View.VISIBLE);
                        String s = "失败原因为：" + mData.getExamineRemark();
                        SpannableString spantwo = new SpannableString(s);
                        spantwo.setSpan(new ForegroundColorSpan(Color.parseColor("#FD414B")), 6, s.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        tv_fail_info.setText(spantwo);

                    }
                    break;
                case 5:
                    //作废商户
                    iv_state.setImageResource(R.mipmap.icon_invalid);
                    tv_state.setText("非常抱歉，商户已被废弃");
                    if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                        ll_fail.setVisibility(View.VISIBLE);
                        String s = "失败原因为：" + mData.getExamineRemark();
                        SpannableString spantwo = new SpannableString(s);
                        spantwo.setSpan(new ForegroundColorSpan(Color.parseColor("#FD414B")), 6, s.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        tv_fail_info.setText(spantwo);

                    }
                    break;
                case 6://待确认
                    iv_state.setImageResource(R.mipmap.checking_icon);
                    tv_state.setText("商户资料待确认");
                    break;
                case 10:
                    //变更审核通过
                    iv_state.setImageResource(R.mipmap.check_pass_icon);
                    tv_state.setText("恭喜您，商户已通过平台审核，正在报备支付机构");

                    break;
                case 100:
                    //可交易
                    iv_state.setImageResource(R.mipmap.icon_check_successful);
                    tv_state.setText("恭喜您，商户已可正常交易");
                    break;
                case 101:
                    //冻结商户
                    iv_state.setImageResource(R.mipmap.freeze_icon);
                    if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                        String s = "非常抱歉，商户被冻结，暂时不能进行交易或登录。冻结原因如下：" + mData.getExamineRemark();
                        SpannableString span = new SpannableString(s);
                        span.setSpan(UIUtils.getColor(R.color.tv_notice_red), 30, s.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        tv_state.setText(span);
                    } else {
                        tv_state.setText("商户已冻结");
                    }
                    break;

            }
            if (mData.getMchQueryStatus() ==3) {
                mTvNotice.setVisibility(View.VISIBLE);
            } else {
                mTvNotice.setVisibility(View.GONE);
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_UPLOAD_IMAGE && resultCode == RESULT_OK) {
            //mTvLoadResult.setText("上传成功");
//            Bundle extras = data.getExtras();
           /* UploadImageBean.DataEntity dataEntity = (UploadImageBean.DataEntity) extras.getSerializable(Constants.RESULT_UPLOAD_IMAGE_INTENT);
            if (!TextUtils.isEmpty(dataEntity.getStore())) {
                mData.setCompanyPhoto(dataEntity.getStore());
            }
            if (!TextUtils.isEmpty(dataEntity.getLicense())) {
                mData.setLicensePhoto(dataEntity.getLicense());
            }
            if (!TextUtils.isEmpty(dataEntity.getBkCardPhoto())) {
                mData.setBkCardPhoto(dataEntity.getBkCardPhoto());
            }
            if (!TextUtils.isEmpty(dataEntity.getHandIdcardPhoto())) {
                mData.setHandIdcardPhoto(dataEntity.getHandIdcardPhoto());
            }
            if (!TextUtils.isEmpty(dataEntity.getBkLicensePhoto())) {
                mData.setBkLicensePhoto(dataEntity.getBkLicensePhoto());
            }
            if (!TextUtils.isEmpty(dataEntity.getPicfilezm()) && !TextUtils.isEmpty(dataEntity.getPicfilefm())) {
                mData.setIdCardPhotos(dataEntity.getPicfilezm() + "," + dataEntity.getPicfilefm());
            } else if (!TextUtils.isEmpty(dataEntity.getPicfilezm())) {
                if (!TextUtils.isEmpty(mData.getIdCardPhotos())) {
                    String[] split = mData.getIdCardPhotos().split(",");
                    if (split != null && split.length > 0) {
                        if (split.length == 2) {
                            mData.setIdCardPhotos(dataEntity.getPicfilezm() + "," + split[1]);
                        }
                    }
                }
            } else if (!TextUtils.isEmpty(dataEntity.getPicfilefm())) {
                if (!TextUtils.isEmpty(mData.getIdCardPhotos())) {
                    String[] split = mData.getIdCardPhotos().split(",");
                    if (split != null && split.length > 0) {
                        if (split.length == 2) {
                            mData.setIdCardPhotos(split[0] + "," + dataEntity.getPicfilefm());
                        }
                    }
                }
            }*/
        }else if (requestCode == Constants.REQUEST_MERCHANT_CARD){
            getSettleChangeCount();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.isRefresh) {
            MyApplication.isRefresh = false;
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("merchantId", mMerchantId);
                ServerClient.newInstance(MyApplication.getContext()).getMerchantInfo(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_INFO, map);
            }
        }
    }


    public <T> T toBean(String arg0, Class clazz) {
        return (T) new Gson().fromJson(arg0, clazz);
    }
}
