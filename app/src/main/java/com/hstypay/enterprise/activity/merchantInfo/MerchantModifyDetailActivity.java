package com.hstypay.enterprise.activity.merchantInfo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.ModifyDetailAdapter;
import com.hstypay.enterprise.adapter.ModifyListAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ModifyRecordBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MerchantModifyDetailActivity extends BaseActivity implements View.OnClickListener {

    private SafeDialog mLoadDialog;
    private ImageView mIvBack,mIvState;
    private TextView mTvTitle,mTvState,mTvFailInfo;
    private LinearLayout mLlFail;
    private String mRecordId;
    private RecyclerView mRecyclerView;
    private List<ModifyRecordBean.DataBean.RecordList> mList;
    private ModifyDetailAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_modify_detail);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(UIUtils.getString(R.string.title_modify_detail));
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvState = findViewById(R.id.iv_state);
        mTvState = findViewById(R.id.tv_state);
        mTvFailInfo = findViewById(R.id.tv_fail_info);
        mLlFail = findViewById(R.id.ll_fail);
        mRecyclerView = findViewById(R.id.recyclerView);
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    private void initData() {
        mRecordId = getIntent().getStringExtra(Constants.INTENT_MODIFY_RECORD_ID);
        mList = new ArrayList<>();
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ModifyDetailAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        getData(mRecordId);
    }

    private void getData(String recordId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("recordId", recordId);
            ServerClient.newInstance(MyApplication.getContext()).getModifyRecordDetail(MyApplication.getContext(), Constants.TAG_MODIFY_RECORD_DETAIL, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_MODIFY_RECORD_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ModifyRecordBean msg = (ModifyRecordBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantModifyDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        setView(msg.getData().getState());
                        if (TextUtils.isEmpty(msg.getData().getReason())) {
                            mLlFail.setVisibility(View.GONE);
                        } else {
                            mTvFailInfo.setText("变更失败原因：\n" + msg.getData().getReason());
                            mLlFail.setVisibility(View.VISIBLE);
                        }
                        if (msg.getData().getRecordList()!=null && msg.getData().getRecordList().size()>0) {
                            mList.addAll(msg.getData().getRecordList());
                            mAdapter.notifyDataSetChanged();
                        }
                    } else {
                        MyToast.showToastShort(getString(R.string.data_error));
                    }
                    break;
            }
        }


    }

    private void setView(int status){
        switch (status) {
            case 1:
                mTvState.setText("变更成功");
                mIvState.setImageResource(R.mipmap.icon_check_successful);
                break;
            case 2:
                mTvState.setText("变更失败");
                mIvState.setImageResource(R.mipmap.check_fail_icon);
                break;
            case 11:
                mTvState.setText("变更审核中");
                mIvState.setImageResource(R.mipmap.checking_icon);
                break;
        }
    }
}
