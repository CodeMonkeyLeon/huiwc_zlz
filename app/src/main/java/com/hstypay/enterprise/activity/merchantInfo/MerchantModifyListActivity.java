package com.hstypay.enterprise.activity.merchantInfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.ModifyListAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ModifyRecordBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MerchantModifyListActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle, mTvNull;
    private RecyclerView mRecyclerView;
    private SafeDialog mLoadDialog;
    private List<ModifyRecordBean.DataBean.RecordList> mList;
    private ModifyListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_modify_list);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    private void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(UIUtils.getString(R.string.title_modify_record));
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mRecyclerView = findViewById(R.id.recyclerView);
        mTvNull = findViewById(R.id.tv_not_data);
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    private void initData() {
        mList = new ArrayList<>();
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ModifyListAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ModifyListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(MerchantModifyListActivity.this, MerchantModifyDetailActivity.class);
                intent.putExtra(Constants.INTENT_MODIFY_RECORD_ID, mList.get(position).getRecordId());
                startActivity(intent);
            }
        });
        getData();
    }

    private void getData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).getModifyRecordList(MyApplication.getContext(), Constants.TAG_MODIFY_RECORD_LIST, null);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_MODIFY_RECORD_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ModifyRecordBean msg = (ModifyRecordBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MerchantModifyListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().getRecordList() != null && msg.getData().getRecordList().size() > 0) {
                        mList.clear();
                        mList.addAll(msg.getData().getRecordList());
                        mAdapter.notifyDataSetChanged();
                        mTvNull.setVisibility(View.GONE);
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    }
}
