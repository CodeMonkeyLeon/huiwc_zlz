package com.hstypay.enterprise.activity.merchantInfo;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.PayTypeStatusAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PayTypeStatusBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 16:57
 * @描述: ${TODO}
 */

public class PayTypeActivity extends BaseActivity implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private TextView mTvNull, mTvTitle;
    private List<PayTypeStatusBean.PayTypeEntity> mList = new ArrayList<>();
    private PayTypeStatusAdapter mAdapter;
    private ImageView mIvBack;
    private SafeDialog mLoadDialog;
    private String mMerchantTel;
    private String mMerchantId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_type);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle.setText(UIUtils.getString(R.string.title_pay_type));
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mTvNull = (TextView) findViewById(R.id.tv_null);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
    }

    private void initData() {
        mMerchantTel = getIntent().getStringExtra(Constants.INTENT_MERCHANT_TEL);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new PayTypeStatusAdapter(PayTypeActivity.this, mList);
        mRecyclerView.setAdapter(mAdapter);
        getData(mMerchantId);
    }

    private void getData(String merchantId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String,Object> map = new HashMap<>();
            map.put("merchantId",merchantId);
            ServerClient.newInstance(MyApplication.getContext()).payTypeList(MyApplication.getContext(), Constants.TAG_PAY_TYPE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PAY_TYPE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayTypeStatusBean msg = (PayTypeStatusBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData()!= null && msg.getData().size()>0) {
                        mTvNull.setVisibility(View.GONE);
                        mList.clear();
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
