package com.hstypay.enterprise.activity.paySite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class AddPaySiteActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private EditText mEtSiteName;
    private Button mBtnSubmit;
    private TextView mTvTitle, mTvSiteCount, mTvStoreName;
    private SafeDialog mLoadDialog;
    private RelativeLayout mRlChoiceStore;
    private String mStoreId = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pay_site);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);

        mBtnSubmit = findViewById(R.id.btn_submit);
        mTvSiteCount = findViewById(R.id.tv_site_name_count);
        mEtSiteName = findViewById(R.id.et_site_name);
        mTvStoreName = findViewById(R.id.tv_store_name);
        mRlChoiceStore = findViewById(R.id.rl_choice_store);

        mTvTitle.setText(R.string.title_add_site);
        setButtonState();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mRlChoiceStore.setOnClickListener(this);

        mEtSiteName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mTvSiteCount.setText(s.length() + "/15");
                setButtonState();
            }
        });
    }

    private void setButtonState() {
        if (TextUtils.isEmpty(mEtSiteName.getText().toString().trim()) || TextUtils.isEmpty(mStoreId)) {
            setButtonEnable(mBtnSubmit, false);
        } else {
            if (!mBtnSubmit.isEnabled()) {
                setButtonEnable(mBtnSubmit, true);
            }
        }
    }

    private void initData() {

    }

    private void addPaySite() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("cashPointName", mEtSiteName.getText().toString().trim());
            map.put("storeId", mStoreId);
            ServerClient.newInstance(MyApplication.getContext()).addPaySite(MyApplication.getContext(), Constants.TAG_PAY_SITE_ADD, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                addPaySite();
                break;
            case R.id.rl_choice_store:
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                startActivityForResult(intentShop, Constants.REQUEST_CASHIER_SHOP_CODE);
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PAY_SITE_ADD)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddPaySiteActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialog(getString(R.string.dialog_add_success));
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CASHIER_SHOP_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                Bundle extras = data.getExtras();
                StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
                mTvStoreName.setText(shopBean.getStoreName());
                mStoreId = shopBean.getStoreId();
                setButtonState();
            }
        }
    }

    public void getDialog(String title) {
        showCommonNoticeDialog(AddPaySiteActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                setResult(RESULT_OK);
                AddPaySiteActivity.this.finish();
            }
        });
    }
}
