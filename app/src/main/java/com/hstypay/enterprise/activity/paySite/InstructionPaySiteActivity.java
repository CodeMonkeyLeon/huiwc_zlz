package com.hstypay.enterprise.activity.paySite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.SvcAppListBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class InstructionPaySiteActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle;
    private WebView webview;
    private Button mbtnSubmit;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction_pay_site);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mbtnSubmit = findViewById(R.id.btn_submit);
        mTvTitle.setText(getString(R.string.tv_site));
        webview = findViewById(R.id.webview);
        webview.loadUrl(getIntent().getStringExtra(Constants.INTENT_OUT_URL));

        setButtonEnable(mbtnSubmit, true);
        if (getIntent().getBooleanExtra("IS_NOT_SHOW_BUTTON", false) || !MyApplication.getIsMerchant()) {
            mbtnSubmit.setVisibility(View.GONE);
        } else {
            mbtnSubmit.setVisibility(View.VISIBLE);
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mbtnSubmit.setOnClickListener(this);
    }

    private void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                //开通接口
                if (TextUtils.isEmpty(MyApplication.getCashPointAppId())) {
                    querySvcAppList();
                } else {
                    openFunction();
                }
                break;
        }
    }

    /**
     * 应用ID列表查询
     */
    private void querySvcAppList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).querySvcAppList(MyApplication.getContext(), "TAG_PAY_SITE_SVC_APP_LIST", null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void openFunction() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("svcAppId", MyApplication.getCashPointAppId());
            ServerClient.newInstance(MyApplication.getContext()).openFunction(MyApplication.getContext(), "TAG_PAY_SITE_OPEN", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
         if (event.getTag().equals("TAG_PAY_SITE_SVC_APP_LIST")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SvcAppListBean msg = (SvcAppListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(InstructionPaySiteActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        for (int i = 0; i < msg.getData().size(); i++) {
                            if ("CASH-POINT".equals(msg.getData().get(i).getAppCode())) {
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_CASH_POINT, msg.getData().get(i).getId());
                                openFunction();
                            }
                            if ("ALIPAY-ISTALLMENT".equals(msg.getData().get(i).getAppCode())) {
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_ALIPAY_ISTALLMENT, msg.getData().get(i).getId());
                            }
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_PAY_SITE_OPEN")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                        if (msg.getError().getMessage() != null) {
                            getLoginDialog(InstructionPaySiteActivity.this, msg.getError().getMessage());
                        }
                    } else {
                        if (msg.getError().getMessage() != null) {
                            MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    haveOpenSvcApp();
                    break;
            }
        } else if (event.getTag().equals("TAG_PAY_SITE_OPEN_STATUS")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                        if (msg.getError().getMessage() != null) {
                            getLoginDialog(this, msg.getError().getMessage());
                        }
                    } else {
                        if (msg.getError().getMessage() != null) {
                            MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_CASH_POINT_OPEN, msg.getData().getServiceStatus());
                        if (MyApplication.getCashPointOpenStatus() != null) {
                            String title = "";
                            switch (MyApplication.getCashPointOpenStatus()) {
                                case "0":
                                    title = "未开通";
                                    break;
                                case "1":
                                    title = "开通中";
                                    break;
                                case "2":
                                    title = "开通成功";
                                    break;
                            }
                            showCommonNoticeDialog(this, title, () -> {
                                if ("2".equals(MyApplication.getCashPointOpenStatus())) {
                                    startActivity(new Intent(this, PaySiteListActivity.class));
                                }
                                finish();
                            });
                        }
                    } else {
                        ToastUtil.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
    }


    /**
     * 应用开通状态查询
     */
    private void haveOpenSvcApp() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("appCode", "CASH-POINT");
            ServerClient.newInstance(MyApplication.getContext()).haveOpenSvcApp(MyApplication.getContext(), "TAG_PAY_SITE_OPEN_STATUS", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }
}
