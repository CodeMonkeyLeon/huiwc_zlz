package com.hstypay.enterprise.activity.paySite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectSiteLinkDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.adapter.paySite.SiteDetailAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.bean.paySite.SiteDeviceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class PaySiteActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle, mTvStoreName, mTvSiteName, mTvQuery, mTvLink, mTvNull;
    private SafeDialog mLoadDialog;
    private String mStoreId = "";
    private String mStoreName = "";
    private RecyclerView mRecyclerView;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private String mCashPointId = "";
    private String mCashPointName = "";
    private boolean isRefreshed;
    private boolean isLoadmore;
    private SiteDetailAdapter mAdapter;
    private List<SiteDeviceBean> mList;
    private SelectSiteLinkDialog mDialog;
    private int mPosition;
    private int RESULTCODE;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_site);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);

        mTvSiteName = findViewById(R.id.tv_site_name);
        mTvStoreName = findViewById(R.id.tv_store_name);
        mTvQuery = findViewById(R.id.tv_query);
        mTvLink = findViewById(R.id.tv_link);
        mTvNull = findViewById(R.id.tv_null);

        mTvTitle.setText(R.string.title_site_detail);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mTvQuery.setOnClickListener(this);
        mTvLink.setOnClickListener(this);
    }

    private void initData() {
        mCashPointId = getIntent().getStringExtra(Constants.INTENT_SITE_ID);
        mCashPointName = getIntent().getStringExtra(Constants.INTENT_SITE_NAME);
        mStoreId = getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
        mStoreName = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_NAME);
        mList = new ArrayList<>();
        mAdapter = new SiteDetailAdapter(PaySiteActivity.this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemCancelClickListener(new SiteDetailAdapter.OnItemCancelClickListener() {
            @Override
            public void onItemCancelClick(int position) {
                mPosition = position;
                cancelSiteLink(mList.get(position).getDeviceId());
            }
        });
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            currentPage = 2;
            getSiteDevice(pageSize, 1, mCashPointId);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
        mTvSiteName.setText("收银点:"+mCashPointName);
        mTvStoreName.setText("门店:"+mStoreName);
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getSiteDevice(pageSize, 1, mCashPointId);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getSiteDevice(pageSize, currentPage, mCashPointId);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    private void getSiteDevice(int pageSize, int currentPage, String cashPointId) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadmore && !isRefreshed)
                DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            map.put("cashPointId", cashPointId);
            map.put("storeId", mStoreId);
            ServerClient.newInstance(MyApplication.getContext()).getSiteDevice(MyApplication.getContext(), Constants.TAG_SITE_DEVICE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void cancelSiteLink(String deviceId) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadmore && !isRefreshed)
                DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", mStoreId);
            map.put("deviceId", deviceId);
            ServerClient.newInstance(MyApplication.getContext()).cancelSiteLink(MyApplication.getContext(), Constants.TAG_CANCEL_LINK, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                setResult(RESULTCODE);
                finish();
                break;
            case R.id.tv_query:
                showSelectDialog(1);
                break;
            case R.id.tv_link:
                showSelectDialog(0);
                break;
        }
    }

    private void showSelectDialog(final int type) {
        mDialog = new SelectSiteLinkDialog(this);
        mDialog.setOnClickHardwareListener(new SelectSiteLinkDialog.OnClickHardwareListener() {
            @Override
            public void clickHardware() {
                if (type == 1) {
                    Intent intent = new Intent(PaySiteActivity.this, SiteDeviceListActivity.class);
                    intent.putExtra(Constants.INTENT_SITE_TYPE, "1");
                    intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
                    intent.putExtra(Constants.INTENT_SITE_ID, mCashPointId);
                    startActivityForResult(intent, Constants.REQUEST_CAPTURE_LINK_SITE);
                } else {
                    toCaptureSiteLink(Constants.INTENT_CAPTURE_SITE_DEVICE, "1");
                }
            }
        });
        mDialog.setOnClickQrcodeListener(new SelectSiteLinkDialog.OnClickQrcodeListener() {
            @Override
            public void clickQrcode() {
                if (type == 1) {
                    Intent intent = new Intent(PaySiteActivity.this, SiteDeviceListActivity.class);
                    intent.putExtra(Constants.INTENT_SITE_TYPE, "2");
                    intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
                    intent.putExtra(Constants.INTENT_SITE_ID, mCashPointId);
                    startActivityForResult(intent, Constants.REQUEST_CAPTURE_LINK_SITE);
                } else {
                    toCaptureSiteLink(Constants.INTENT_CAPTURE_SITE_QRCODE, "2");
                }
            }
        });
        DialogHelper.resize(this, mDialog);
        mDialog.show();
    }

    private void toCaptureSiteLink(String intentName, String deviceClass) {
        Intent intent = new Intent(PaySiteActivity.this, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_SITE_TYPE, deviceClass);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        intent.putExtra(Constants.INTENT_SITE_ID, mCashPointId);
        intent.putExtra(Constants.INTENT_SITE_NAME, mCashPointName);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        startActivityForResult(intent,Constants.REQUEST_CAPTURE_LINK_SITE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_SITE_DEVICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PaySiteActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getCashPointDetailList() != null && msg.getData().getCashPointDetailList().size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData().getCashPointDetailList());
                    } else {
                        if (isLoadmore) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            mAdapter.notifyDataSetChanged();
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_CANCEL_LINK)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PaySiteActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(PaySiteActivity.this, "已取消关联", new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            mList.remove(mPosition);
                            mAdapter.notifyDataSetChanged();
                            RESULTCODE = RESULT_OK;
                        }
                    });
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULTCODE);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CAPTURE_LINK_SITE) {
            RESULTCODE = RESULT_OK;
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                mList.clear();
                currentPage = 2;
                getSiteDevice(pageSize, 1, mCashPointId);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }
}
