package com.hstypay.enterprise.activity.paySite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.adapter.paySite.PaySiteChoiceAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.paySite.PaySiteBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class PaySiteChoiceActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private LinearLayout mLlList;
    private EditTextDelete mEtInput;
    private RecyclerView mRecyclerView;
    private TextView mButton, mTvTitle, mTvNull, mTvCancel, mTvEnsure;
    private List<StoreListBean.DataEntity> mStoreList;
    private List<StoreListBean.DataEntity> mOriginList;
    private List<PaySiteBean> mList;
    private SafeDialog mLoadDialog;
    private String mStoreId;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private CustomLinearLayoutManager mLinearLayoutManager;
    private PaySiteChoiceAdapter mAdapter;
    private int mPosition;
    private String mSiteId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_site_choice);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mTvCancel = findViewById(R.id.tv_cancel_link);
        mTvEnsure = findViewById(R.id.tv_ensure_link);
        mLlList = findViewById(R.id.ll_list);
        mEtInput = findViewById(R.id.et_input);
        mTvNull = findViewById(R.id.tv_null);
        mTvTitle.setText(getString(R.string.title_link_site));

        initRecyclerView();
        initSwipeRefreshLayout();

    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mTvCancel.setOnClickListener(this);
        mTvEnsure.setOnClickListener(this);

        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initData() {
        mStoreList = new ArrayList<>();
        mOriginList = new ArrayList<>();
        mList = new ArrayList<>();
        mStoreId = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_ID);
        List<PaySiteBean> paySiteList = (List<PaySiteBean>) getIntent().getSerializableExtra(Constants.INTENT_SITE_BINDING);
        if (paySiteList != null)
            mList.addAll(paySiteList);
        mAdapter = new PaySiteChoiceAdapter(PaySiteChoiceActivity.this, mList, mSiteId);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mTvEnsure.setEnabled(true);
                mTvEnsure.setBackgroundResource(R.color.theme_color);
                mPosition = position;
            }
        });
        /*if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            currentPage = 2;
            getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }*/
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getList(pageSize, currentPage, mEtInput.getText().toString().trim(), mStoreId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button:
                Intent intent = new Intent(PaySiteChoiceActivity.this, AddPaySiteActivity.class);
                startActivityForResult(intent, Constants.REQUEST_ADD_STORE);
                break;
            case R.id.tv_cancel_link:
                if ("INTENT_BIND_DEVICE".equals(getIntent().getStringExtra(Constants.INTENT_NAME))) {
                    bindReceiveSN(false);
                } else {
                    toCapture(false);
                }
                break;
            case R.id.tv_ensure_link:
                if ("INTENT_BIND_DEVICE".equals(getIntent().getStringExtra(Constants.INTENT_NAME))) {
                    bindReceiveSN(true);
                } else {
                    toCapture(true);
                }
                break;
        }
    }

    private void toCapture(boolean isLink) {
        Intent intent = new Intent(PaySiteChoiceActivity.this, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_USER_ID, getIntent().getStringExtra(Constants.INTENT_USER_ID));
        intent.putExtra(Constants.INTENT_BIND_STORE_ID, mStoreId);
        intent.putExtra(Constants.INTENT_BIND_STORE_NAME, getIntent().getStringExtra(Constants.INTENT_BIND_STORE_NAME));
        intent.putExtra(Constants.INTENT_NAME, getIntent().getStringExtra(Constants.INTENT_NAME));
        if (isLink) {
            intent.putExtra(Constants.INTENT_SITE_NAME, mList.get(mPosition).getCashPointName());
            intent.putExtra(Constants.INTENT_SITE_ID, mList.get(mPosition).getId());
        }
        startActivity(intent);
    }

    private void bindReceiveSN(boolean isLink) {
        if (!NetworkUtils.isNetworkAvailable(PaySiteChoiceActivity.this)) {
            ToastUtil.showToastShort(getString(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("sn", getIntent().getStringExtra(Constants.INTENT_DEVICE_SN));
            map.put("storeMerchantId", mStoreId);
            DialogUtil.safeShowDialog(mLoadDialog);
            if (isLink)
                map.put("cashPointId", mList.get(mPosition).getId());
            ServerClient.newInstance(MyApplication.getContext()).deviceBind(MyApplication.getContext(), "TAG_DEVICE_BIND_PAYSITECHOICEACTIVITY", map);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void getList(int pageSize, int currentPage, String search, String storeMerchantId, boolean showLoading) {
        //门店网络请求
        if (showLoading) {
            DialogUtil.safeShowDialog(mLoadDialog);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("search", search);
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeId", storeMerchantId);
        }
        ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_CHOICE_SITE_LIST, map);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_SITE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PaySiteChoiceActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getCashPointList() != null && msg.getData().getCashPointList().size() > 0) {
                        mButton.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData().getCashPointList());
                    } else {
                        if (isLoadmore) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            mAdapter.notifyDataSetChanged();
            isLoadmore = false;
            isRefreshed = false;
        }  else if (event.getTag().equals("TAG_DEVICE_BIND_PAYSITECHOICEACTIVITY")) {
            Info msg = (Info) event.getMsg();
            DialogUtil.safeCloseDialog(mLoadDialog);
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PaySiteChoiceActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PaySiteChoiceActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(PaySiteChoiceActivity.this, getString(R.string.dialog_notice_binding_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            startActivity(new Intent(PaySiteChoiceActivity.this, DeviceListActivity.class));
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setData(String search) {
        mStoreList.clear();
        if (TextUtils.isEmpty(search)) {
            mStoreList.addAll(mOriginList);
        } else {
            for (int i = 0; i < mOriginList.size(); i++) {
                if (mOriginList.get(i).getStoreName().contains(search)) {
                    mStoreList.add(mOriginList.get(i));
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_ADD_STORE) {
            mEtInput.setText("");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mEtInput.setText("");
    }
}
