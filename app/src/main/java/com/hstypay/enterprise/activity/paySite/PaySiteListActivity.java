package com.hstypay.enterprise.activity.paySite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.paySite.PaySiteAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.paySite.PaySiteBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class PaySiteListActivity extends BaseActivity implements View.OnClickListener, ShopRecyclerAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack, mIvArrow, mIvButton;
    private LinearLayout mLlList, mLlChoiceStore, mPopLayout;
    private EditTextDelete mEtInput, mEtStoreInput;
    private RecyclerView mRecyclerView, mStoreRecyclerView;
    private TextView mButton, mTvStoreName, mTvNull,mTvPaySiteInstruction;
    private boolean mSwitchArrow;
    private List<StoreListBean.DataEntity> mStoreList;
    private List<StoreListBean.DataEntity> mOriginList;
    private List<PaySiteBean> mList;
    private Animation rotate;
    private SafeDialog mLoadDialog;
    private String mStoreId;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private CustomLinearLayoutManager mLinearLayoutManager, mStoreLinearLayoutManager;
    private PaySiteAdapter mAdapter;
    private ShopRecyclerAdapter mStoreAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_site_list);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mIvButton = findViewById(R.id.iv_button);
        mTvStoreName = (TextView) findViewById(R.id.tv_store_name);
        mTvPaySiteInstruction = (TextView) findViewById(R.id.tv_pay_site_instruction);
        mIvArrow = findViewById(R.id.iv_arrow);
        mLlChoiceStore = (LinearLayout) findViewById(R.id.ll_choice_store);
        mLlList = (LinearLayout) findViewById(R.id.ll_list);
        mEtInput = findViewById(R.id.et_input);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mIvButton.setVisibility(View.VISIBLE);
        mButton.setVisibility(View.INVISIBLE);

        if (MyApplication.getIsCasher()) {
            mIvArrow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(MyApplication.getDefaultStoreName())) {
                if (MyApplication.getDefaultStoreName().length() > 10) {
                    mTvStoreName.setText(MyApplication.getDefaultStoreName().substring(0, 9) + "...");
                } else {
                    mTvStoreName.setText(MyApplication.getDefaultStoreName());
                }
            }
        } else {
            mTvStoreName.setText(getString(R.string.title_all_store));
//            mTvStoreName.setText(getString(R.string.tv_site_all_store));
        }

        initRecyclerView();
        initSwipeRefreshLayout();

        mPopLayout = (LinearLayout) findViewById(R.id.pop_layout);
        mEtStoreInput = findViewById(R.id.et_store_input);
        mStoreRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_store);
        mStoreLinearLayoutManager = new CustomLinearLayoutManager(this);
        mStoreRecyclerView.setLayoutManager(mStoreLinearLayoutManager);

        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mLlChoiceStore.setOnClickListener(this);
        mIvButton.setOnClickListener(this);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mTvPaySiteInstruction.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                startActivity(new Intent(PaySiteListActivity.this, InstructionPaySiteActivity.class)
                        .putExtra(Constants.INTENT_OUT_URL, "file:///android_asset/instruction_pay_site.html")
                        .putExtra("IS_NOT_SHOW_BUTTON",true));
            }
        });
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initData() {
        mStoreList = new ArrayList<>();
        mOriginList = new ArrayList<>();
        mList = new ArrayList<>();
        mStoreId = "";

        mAdapter = new PaySiteAdapter(PaySiteListActivity.this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new PaySiteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(PaySiteListActivity.this, PaySiteActivity.class);
                intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mList.get(position).getStoreMerchantId());
                intent.putExtra(Constants.INTENT_BIND_STORE_NAME, mList.get(position).getStoreName());
                intent.putExtra(Constants.INTENT_SITE_ID, mList.get(position).getId());
                intent.putExtra(Constants.INTENT_SITE_NAME, mList.get(position).getCashPointName());
                startActivityForResult(intent,Constants.REQUEST_ADD_STORE);
            }
        });

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            currentPage = 2;
            getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getList(pageSize, currentPage, mEtInput.getText().toString().trim(), mStoreId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button:
                Intent intent = new Intent(PaySiteListActivity.this, AddPaySiteActivity.class);
                startActivityForResult(intent, Constants.REQUEST_ADD_STORE);
                break;
            case R.id.ll_choice_store:
                if (mSwitchArrow) {
                    closeArrow();
                } else {
                    loadData();
                }
                break;
        }
    }

    public void closeArrow() {
        mEtStoreInput.setText("");
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        mSwitchArrow = false;
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mIvArrow.startAnimation(rotate);
        mPopLayout.setVisibility(View.GONE);
    }

    private void openArrow() {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        mSwitchArrow = true;
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mIvArrow.startAnimation(rotate);
        mPopLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIvArrow.clearAnimation();
    }

    private void loadData() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginList.clear();
            mStoreList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            map.put("merchantDataType", "1");
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_SITE_CHOICE_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getList(int pageSize, int currentPage, String search, String storeMerchantId, boolean showLoading) {
        //门店网络请求
        if (showLoading) {
            DialogUtil.safeShowDialog(mLoadDialog);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("search", search);
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeId", storeMerchantId);
        }
        ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_PAY_SITE_LIST, map);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_SITE_CHOICE_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PaySiteListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        dataEntity.setStoreName("全部门店");
                        dataEntity.setStoreId("");
                        mOriginList.add(dataEntity);
                        mOriginList.addAll(msg.getData());
                        mStoreList.addAll(mOriginList);
                        mStoreAdapter = new ShopRecyclerAdapter(PaySiteListActivity.this, mStoreList, mStoreId);
                        mStoreAdapter.setOnItemClickListener(this);
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        openArrow();
                    } else {
                        showCommonNoticeDialog(PaySiteListActivity.this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PAY_SITE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PaySiteListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getCashPointList() != null && msg.getData().getCashPointList().size() > 0) {
                        mLlList.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData().getCashPointList());
                    } else {
                        if (isLoadmore) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mLlList.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            mAdapter.notifyDataSetChanged();
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setData(String search) {
        mStoreList.clear();
        if (TextUtils.isEmpty(search)) {
            mStoreList.addAll(mOriginList);
        } else {
            for (int i = 0; i < mOriginList.size(); i++) {
                if (mOriginList.get(i).getStoreName().contains(search)) {
                    mStoreList.add(mOriginList.get(i));
                }
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        mStoreId = mStoreList.get(position).getStoreId();
        if (TextUtils.isEmpty(mStoreId)) {
            mTvStoreName.setText(getString(R.string.title_all_store));
//            mTvStoreName.setText(getString(R.string.tv_site_all_store));
        } else {
            String title = mStoreList.get(position).getStoreName();
            mTvStoreName.setText(StringUtils.ellipsisString(title, 20));
        }
        closeArrow();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            currentPage = 2;
            getList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_ADD_STORE) {
            mEtInput.setText("");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mEtInput.setText("");
    }
}
