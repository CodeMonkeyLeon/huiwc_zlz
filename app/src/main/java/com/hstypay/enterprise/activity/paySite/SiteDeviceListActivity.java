package com.hstypay.enterprise.activity.paySite;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.paySite.DeviceTypeAdapter;
import com.hstypay.enterprise.adapter.paySite.SiteDeviceAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.bean.paySite.SiteDeviceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class SiteDeviceListActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvDeviceType, mIvLinkState;
    private TextView mTvTitle, mTvLink, mTvNull, mTvDeviceType, mTvLinkState;
    private SafeDialog mLoadDialog;
    private LinearLayout mLlDeviceType, mLlLinkState;
    private RecyclerView mRecyclerView, mRvDeviceType, mRvLinkState;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private SiteDeviceAdapter mAdapter;
    private List<SiteDeviceBean> mList;
    private boolean mTypeSwitchArrow, mStateSwitchArrow;
    private Animation rotate;
    private String mTypeId = "";
    private String mStateId = "";
    private List<DataEntity> mTypeList;
    private List<DataEntity> mStateList;
    private SelectDialog mSelectDialog;
    private int count;
    private String mDeviceClass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_device_list);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);

        mTvLink = findViewById(R.id.tv_link);
        mTvNull = findViewById(R.id.tv_null);

        mLlDeviceType = findViewById(R.id.ll_device_type);
        mTvDeviceType = findViewById(R.id.tv_device_type);
        mIvDeviceType = findViewById(R.id.iv_device_type_arrow);

        mLlLinkState = findViewById(R.id.ll_link_state);
        mTvLinkState = findViewById(R.id.tv_link_state);
        mIvLinkState = findViewById(R.id.iv_link_state_arrow);

        mTvLink.setText("已选择0设备，确认关联");

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mTvLink.setOnClickListener(this);
        mLlDeviceType.setOnClickListener(this);
        mLlLinkState.setOnClickListener(this);
    }

    private void initData() {
        mDeviceClass = getIntent().getStringExtra(Constants.INTENT_SITE_TYPE);
        mList = new ArrayList<>();
        setData();
        mAdapter = new SiteDeviceAdapter(SiteDeviceListActivity.this, mList, mDeviceClass);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemCancelClickListener(new SiteDeviceAdapter.OnItemCancelClickListener() {
            @Override
            public void onItemCancelClick(int position, boolean isCheck) {
                if (isCheck) {
                    count++;
                } else {
                    count--;
                }
                mTvLink.setText("已选择" + count + "设备，确认关联");
                if (count > 0) {
                    mTvLink.setBackgroundColor(UIUtils.getColor(R.color.theme_color));
                    mTvLink.setEnabled(true);
                } else {
                    mTvLink.setBackgroundColor(UIUtils.getColor(R.color.theme_color_fifteen));
                    mTvLink.setEnabled(false);
                }
            }
        });
        DeviceTypeAdapter deviceTypeAdapter = new DeviceTypeAdapter(SiteDeviceListActivity.this, mTypeList);
        mRvDeviceType.setAdapter(deviceTypeAdapter);
        deviceTypeAdapter.setOnItemClickListener(new DeviceTypeAdapter.OnRecyclerViewItemClickListener() {

            @Override
            public void onItemClick(int position) {
                mTvDeviceType.setText(mTypeList.get(position).getRealName());
                mTypeId = mTypeList.get(position).getUserId();
                deviceTypeAdapter.setSelected(mTypeId);
                closeArrow(1);
                getSiteDeviceList();
            }
        });
        DeviceTypeAdapter deviceStateAdapter = new DeviceTypeAdapter(SiteDeviceListActivity.this, mStateList);
        mRvLinkState.setAdapter(deviceStateAdapter);
        deviceStateAdapter.setOnItemClickListener(new DeviceTypeAdapter.OnRecyclerViewItemClickListener() {

            @Override
            public void onItemClick(int position) {
                mTvLinkState.setText(mStateList.get(position).getRealName());
                mStateId = mStateList.get(position).getUserId();
                deviceStateAdapter.setSelected(mStateId);
                closeArrow(2);
                getSiteDeviceList();
            }
        });
        getSiteDeviceList();
        if ("1".equals(mDeviceClass)) {
            mTvTitle.setText(R.string.title_device_list);
            mTvDeviceType.setText("全部收款硬件");
        } else if ("2".equals(mDeviceClass)) {
            mTvTitle.setText(R.string.title_code_list);
            mTvDeviceType.setText("全部收款码");
        }
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRvDeviceType = findViewById(R.id.recyclerView_device_type);
        CustomLinearLayoutManager linearLayoutManager1 = new CustomLinearLayoutManager(this);
        mRvDeviceType.setLayoutManager(linearLayoutManager1);
        mRvLinkState = findViewById(R.id.recyclerView_link_state);
        CustomLinearLayoutManager linearLayoutManager2 = new CustomLinearLayoutManager(this);
        mRvLinkState.setLayoutManager(linearLayoutManager2);
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        mSwipeRefreshLayout.setLoadmoreEnable(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {

                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {

                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_link:
                if (isExistLinked()) {
                    showSelectDialog();
                } else {
                    linkDevice();
                }
                break;
            case R.id.ll_device_type:
                if (mStateSwitchArrow) {
                    closeArrow(2);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_link_state:
                if (mTypeSwitchArrow) {
                    closeArrow(1);
                }
                if (mStateSwitchArrow) {
                    closeArrow(2);
                } else {
                    openArrow(2);
                }
                break;
        }
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mTypeSwitchArrow = false;
                mIvDeviceType.startAnimation(rotate);
                mRvDeviceType.setVisibility(View.GONE);
                break;
            case 2:
                mStateSwitchArrow = false;
                mIvLinkState.startAnimation(rotate);
                mRvLinkState.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mTypeSwitchArrow = true;
                mIvDeviceType.startAnimation(rotate);
                mRvDeviceType.setVisibility(View.VISIBLE);
                break;
            case 2:
                mStateSwitchArrow = true;
                mIvLinkState.startAnimation(rotate);
                mRvLinkState.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_SITE_DEVICE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(SiteDeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getDeviceList() != null && msg.getData().getDeviceList().size() > 0) {
                        mList.clear();
                        mList.addAll(msg.getData().getDeviceList());
                        mAdapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                    } else {
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                        mTvNull.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_LINK_SITE_DEVICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(SiteDeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getDialog(getString(R.string.dialog_link_success));
                    break;
            }
        }
    }

    private void getSiteDeviceList() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID));
            map.put("deviceClass", mDeviceClass);
            map.put("cashPointId", getIntent().getStringExtra(Constants.INTENT_SITE_ID));
            if (!TextUtils.isEmpty(mStateId))
                map.put("bindFlag", mStateId);
            if (!TextUtils.isEmpty(mTypeId))
                map.put("deviceType", mTypeId);
            ServerClient.newInstance(MyApplication.getContext()).getSiteDeviceList(MyApplication.getContext(), Constants.TAG_SITE_DEVICE_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void linkDevice() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("cashPointId", getIntent().getStringExtra(Constants.INTENT_SITE_ID));
            map.put("deviceIds", getLinkDeviceId());
            map.put("deviceClass", mDeviceClass);
            ServerClient.newInstance(MyApplication.getContext()).linkDevice(MyApplication.getContext(), Constants.TAG_LINK_SITE_DEVICE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    public void getDialog(String title) {
        showCommonNoticeDialog(SiteDeviceListActivity.this, title, new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                setResult(RESULT_OK);
                SiteDeviceListActivity.this.finish();
            }
        });
    }

    public void showSelectDialog() {
        if (mSelectDialog == null) {
            String notice = "";
            if ("1".equals(mDeviceClass)) {
                notice = getString(R.string.dialog_link_site_notice);
            } else if ("2".equals(mDeviceClass)) {
                notice = getString(R.string.dialog_link_site_code_notice);
            }
            mSelectDialog = new SelectDialog(this, notice
                    , R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    linkDevice();
                }
            });
            DialogHelper.resize(this, mSelectDialog);
        }
        mSelectDialog.show();
    }

    private void setData() {
        mTypeList = new ArrayList<>();
        if ("1".equals(mDeviceClass)) {
            DataEntity dataEntity1 = new DataEntity();
            dataEntity1.setRealName("全部收款硬件");
            dataEntity1.setUserId("");
            DataEntity dataEntity2 = new DataEntity();
            dataEntity2.setRealName("简易pos");
            dataEntity2.setUserId("5");
            DataEntity dataEntity3 = new DataEntity();
            dataEntity3.setRealName("微收银插件");
            dataEntity3.setUserId("6");
            DataEntity dataEntity4 = new DataEntity();
            dataEntity4.setRealName("智能pos");
            dataEntity4.setUserId("7");
            mTypeList.add(dataEntity1);
            mTypeList.add(dataEntity2);
            mTypeList.add(dataEntity3);
            mTypeList.add(dataEntity4);
        } else if ("2".equals(mDeviceClass)) {
            DataEntity dataEntity1 = new DataEntity();
            dataEntity1.setRealName("全部收款码");
            dataEntity1.setUserId("");
            DataEntity dataEntity2 = new DataEntity();
            dataEntity2.setRealName("聚合码");
            dataEntity2.setUserId("1");
            DataEntity dataEntity3 = new DataEntity();
            dataEntity3.setRealName("银标码");
            dataEntity3.setUserId("2");
            DataEntity dataEntity4 = new DataEntity();
            dataEntity4.setRealName("升级版聚合码");
            dataEntity4.setUserId("3");
            DataEntity dataEntity5 = new DataEntity();
            dataEntity5.setRealName("花呗码");
            dataEntity5.setUserId("4");
            mTypeList.add(dataEntity1);
            mTypeList.add(dataEntity2);
            mTypeList.add(dataEntity3);
            mTypeList.add(dataEntity4);
            mTypeList.add(dataEntity5);
        }
        mStateList = new ArrayList<>();
        DataEntity dataEntity11 = new DataEntity();
        dataEntity11.setRealName("全部关联状态");
        dataEntity11.setUserId("");
        DataEntity dataEntity12 = new DataEntity();
        dataEntity12.setRealName("已关联");
        dataEntity12.setUserId("1");
        DataEntity dataEntity13 = new DataEntity();
        dataEntity13.setRealName("未关联");
        dataEntity13.setUserId("0");
        mStateList.add(dataEntity11);
        mStateList.add(dataEntity12);
        mStateList.add(dataEntity13);
    }

    private List<String> getLinkDeviceId() {
        List<String> deviceIdList = new ArrayList<>();
        for (SiteDeviceBean siteDeviceBean : mList) {
            if (siteDeviceBean.isChecked()) {
                deviceIdList.add(siteDeviceBean.getDeviceId());
            }
        }
        return deviceIdList;
    }

    private boolean isExistLinked() {
        for (SiteDeviceBean siteDeviceBean : mList) {
            if (siteDeviceBean.isChecked() && "1".equals(siteDeviceBean.getBindFlag())) {
                return true;
            }
        }
        return false;
    }
}
