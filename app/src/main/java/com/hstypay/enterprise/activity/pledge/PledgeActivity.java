package com.hstypay.enterprise.activity.pledge;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CardDetailBean;
import com.hstypay.enterprise.fragment.PledgeBillFragment;
import com.hstypay.enterprise.fragment.PledgeReportFragment;
import com.hstypay.enterprise.fragment.PrivateFragment;
import com.hstypay.enterprise.fragment.PublicFragment;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class PledgeActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private ImageView mIvBack;
    private TextView mTvTitle,mButton,mTvNotice;
    private RadioGroup mRgTitle;
    private RadioButton mRbBill, mRbReport;
    private PledgeBillFragment mPledgeBillFragment;
    private PledgeReportFragment mPledgeReportFragment;
    private Fragment[] mFragments;
    private Button mBtnSubmit;
    private ScrollView mScrollView;
    private int mIndex = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);

        mRgTitle =findViewById(R.id.rg_title);
        mRbBill = findViewById(R.id.rb_title_report_day);
        mRbReport = findViewById(R.id.rb_title_report_month);

        mScrollView = findViewById(R.id.sl_instruction);
        mTvNotice = findViewById(R.id.tv_pledge_refund_notice);
        mBtnSubmit = findViewById(R.id.btn_submit);

        mRbBill.setText(R.string.tv_detail);
        mRbReport.setText(R.string.tv_report);
        mTvTitle.setText(R.string.tx_pledge_pay);
        mButton.setVisibility(View.INVISIBLE);

        setButtonEnable(mBtnSubmit, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mRgTitle.setOnCheckedChangeListener(this);
    }

    public void initData() {
        initFragment();

        if (MyApplication.isHidePledgeRefund()) {
            mScrollView.setVisibility(View.GONE);
        } else {
            mScrollView.setVisibility(View.VISIBLE);
            mTvNotice.setText("注：\n" +
                    "1、押金收款订单必须进行退押金操作后才能退款给消费者或者结算给商家。\n" +
                    "2、押金退还成功后，剩余押金（即退款金额）将按原路返回至消费者银行卡或支付账户。");
        }
    }

    private void initFragment() {
        mPledgeBillFragment = new PledgeBillFragment();
        mPledgeReportFragment = new PledgeReportFragment();

        //添加到数组
        mFragments = new Fragment[]{mPledgeBillFragment, mPledgeReportFragment};
        //开启事务
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //添加首页
        ft.add(R.id.content, mPledgeBillFragment).commit();
        //默认设置为第0个
        setIndexSelected(0);
    }

    private void setIndexSelected(int index) {
        if (mIndex == index) {
            return;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        //隐藏
        ft.hide(mFragments[mIndex]);
        //判断是否添加
        if (!mFragments[index].isAdded()) {
            ft.add(R.id.content, mFragments[index]).show(mFragments[index]);
        } else {
            ft.show(mFragments[index]);
        }
        ft.commit();
        //再次赋值
        mIndex = index;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_HIDE_PLEDGE_REFUND + MyApplication.getUserId(), true);
                mScrollView.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_title_report_day:
                setIndexSelected(0);
                break;
            case R.id.rb_title_report_month:
                setIndexSelected(1);
                break;
        }
    }
}
