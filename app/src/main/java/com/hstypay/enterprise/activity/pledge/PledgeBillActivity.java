package com.hstypay.enterprise.activity.pledge;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.FilterPledgePopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDatePopupWindow;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.CashierActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.PledgeBillRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO} 没用到了的页面
 */

public class PledgeBillActivity extends BaseActivity implements View.OnClickListener {
    private RecyclerView mRvBill;
    private Button mBtnSubmit;
    private ImageView mIvFilter, mIvScan, mIvCashierArrow, mIvBack, mIvShopArrow;
    private LinearLayout mLlDate, mLlFilterTop, mLlShop, mLlCashier, mLlInput;
    private TextView mButton, mTvDate, mTvShop, mTvCashier, mTvNull, mTvTitle, mTvNotice;
    private SelectDatePopupWindow mDatePopupWindow;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private ScrollView mScrollView;

    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private PledgeBillRecyclerAdapter mBillRecyclerAdapter;
    private List<PledgeBillsBean.DataBean> billList;

    private String startTime;
    private String endTime;
    private String userId;
    private String storeMerchantId;
    private int[] apiProvider;
    private int[] tradeStatus;
    private List<Integer> tradeTypeList;
    private String otherType;
    private TimePickerView pvTime;
    private View mView;
    private OrderBroadcastReceiver mOrderBroadcastReceiver;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_bill);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        registerBroadcast();
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(PledgeBillActivity.this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mIvScan = (ImageView) findViewById(R.id.iv_scan);
        mLlDate = (LinearLayout) findViewById(R.id.ll_date);
        mTvDate = (TextView) findViewById(R.id.tv_date);
        mLlShop = (LinearLayout) findViewById(R.id.ll_shop);
        mTvShop = (TextView) findViewById(R.id.tv_shop);
        mLlCashier = (LinearLayout) findViewById(R.id.ll_cashier);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);
        mIvCashierArrow = (ImageView) findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = (ImageView) findViewById(R.id.iv_shop_arrow);
        mIvFilter = (ImageView) findViewById(R.id.iv_filter);
        mLlFilterTop = (LinearLayout) findViewById(R.id.bill_filter_top);
        mLlInput = (LinearLayout) findViewById(R.id.ly_input);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);

        mScrollView = findViewById(R.id.sl_instruction);
        mTvNotice = findViewById(R.id.tv_pledge_refund_notice);
        mBtnSubmit = findViewById(R.id.btn_submit);

        mTvTitle.setText(getString(R.string.title_pledge_refund));
        mButton.setText(getString(R.string.tv_report));

        initRecyclerView();
        initSwipeRefreshLayout();

        setButtonEnable(mBtnSubmit,true);
//        initTimePicker();
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -3);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        //时间选择器
        pvTime = new TimePickerView.Builder(PledgeBillActivity.this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date);
                /*if (DateUtil.isToday(time)) {
                    mTvDate.setText("今日");
                } else {*/
                mTvDate.setText(time);
                //}
                startTime = time + " 00:00:00";
                endTime = time + " 23:59:59";
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    billList.clear();
                    Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, otherType, tradeTypeList);
                    currentPage = 2;
                    getGetBills(map, true);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年", "月", "日", null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    private void initRecyclerView() {
        mRvBill = (RecyclerView) findViewById(R.id.bill_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(PledgeBillActivity.this);
        mRvBill.setLayoutManager(mLinearLayoutManager);
        mRvBill.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, otherType, tradeTypeList);
                    getGetBills(map, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    Map<String, Object> map = getRequestMap(pageSize + "", currentPage + "", startTime, endTime, userId,
                            storeMerchantId, apiProvider, tradeStatus, otherType, tradeTypeList);
                    getGetBills(map, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mLlDate.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlCashier.setOnClickListener(this);
        mIvScan.setOnClickListener(this);
        mIvFilter.setOnClickListener(this);
        mLlInput.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    public void initData() {
        mTvDate.setText(DateUtil.formatYYMD(System.currentTimeMillis()));
        initTimePicker();
        billList = new ArrayList<>();
        mBillRecyclerAdapter = new PledgeBillRecyclerAdapter(MyApplication.getContext(), billList);
        mBillRecyclerAdapter.setOnItemClickListener(new PledgeBillRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
//                MtaUtils.mtaId(PledgeBillActivity.this, "C002");
                PledgeBillsBean.DataBean dataBean = billList.get(position);
                if (dataBean != null) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        Map<String, Object> map = new HashMap<>();
                        map.put("authNo", dataBean.getAuthNo());
                        ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_BILL, map);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mRvBill.setAdapter(mBillRecyclerAdapter);
        initStore();
        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
        tradeStatus = new int[]{};
        apiProvider = null;
        tradeTypeList = null;
        otherType = null;
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> requestMap = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                    apiProvider, tradeStatus, otherType, tradeTypeList);
            getGetBills(requestMap, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

        if (MyApplication.isHidePledgeRefund()) {
            mScrollView.setVisibility(View.GONE);
        } else {
            mScrollView.setVisibility(View.VISIBLE);
            mTvNotice.setText("注：\n" +
                    "1、预授权订单必须进行退押金操作后才能退款给消费者或者结算给商家。\n" +
                    "2、押金退还成功后，剩余押金（即退款金额）将按原路返回至消费者银行卡或支付账户。");
        }
    }

    private void initStore() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() /*|| MyApplication.getIsManager()*/) {
            userId = "";
            storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
            if (TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""));
            }
        } else if (MyApplication.getIsCasher()) {
            storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            mIvCashierArrow.setVisibility(View.GONE);
            mTvCashier.setText(MyApplication.getRealName());
            mLlCashier.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            mLlShop.setEnabled(false);
            userId = MyApplication.getUserId();
        }
    }

    private Map<String, Object> getRequestMap(String pageSize, String currentPage, String startTime, String endTime,
                                              String userId, String storeMerchantId, int[] apiProvider, int[] tradeStatus, String otherType, List<Integer> tradeTypeList) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        if (apiProvider != null && apiProvider.length > 0)
            map.put("apiProviderList", apiProvider);
        if (tradeStatus != null && tradeStatus.length > 0)
            map.put("tradeStatusList", tradeStatus);
        if (tradeTypeList != null && tradeTypeList.size() > 0)
            map.put("tradeTypeList", tradeTypeList);
        if (!TextUtils.isEmpty(otherType)) {
            map.put("otherType", otherType);
        }
        return map;
    }

    public void getGetBills(Map<String, Object> map, boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_BILLS, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_BILLS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeBillActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        billList.addAll(msg.getData());
                        mBillRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        mBillRecyclerAdapter.notifyDataSetChanged();
                        if (isLoadmore) {
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_BILL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeBillActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        Intent intent = new Intent(PledgeBillActivity.this, PledgeDetailActivity.class);
                        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, msg.getData().get(0));
                        startActivity(intent);
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            billList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                startActivity(new Intent(PledgeBillActivity.this, PledgeReportActivity.class));
                break;
            case R.id.ll_date://选择日期
                pvTime.show();
                break;
            case R.id.ll_shop://选择店铺
                Intent intentShop = new Intent(PledgeBillActivity.this, ShopActivity.class);
                if (TextUtils.isEmpty(storeMerchantId)) {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                } else {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                }
                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BILL_SHOP);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.ll_cashier://选择收银员
                Intent intentCashier = new Intent(PledgeBillActivity.this, CashierActivity.class);
                intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                if (TextUtils.isEmpty(userId)) {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                } else {
                    intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                }
                intentCashier.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                break;
            case R.id.iv_filter://选择筛选条件
                showFilterPop();
                break;
            case R.id.iv_scan://扫一扫
                Intent intent = new Intent(PledgeBillActivity.this, CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_SCAN_PLEDGE);
                startActivity(intent);
                break;
            case R.id.ly_input://搜索
                startActivity(new Intent(PledgeBillActivity.this, PledgeBillFindActivity.class));
                break;
            case R.id.btn_submit:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_HIDE_PLEDGE_REFUND + MyApplication.getUserId(), true);
                mScrollView.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeMerchantId = shopBean.getStoreId();
            SpStayUtil.putString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, shopBean.getStoreId());
            SpStayUtil.putString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, shopBean.getStoreName());
            /*mTvCashier.setText("全部收银员");
            userId = null;*/
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText(getString(R.string.tv_all_user));
                userId = "";
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                billList.clear();
                Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                        apiProvider, tradeStatus, otherType, tradeTypeList);
                currentPage = 2;
                getGetBills(map, true);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }

        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                billList.clear();
                Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                        apiProvider, tradeStatus, otherType, tradeTypeList);
                currentPage = 2;
                getGetBills(map, true);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    public void showFilterPop() {
        FilterPledgePopupWindow popupWindow = new FilterPledgePopupWindow(PledgeBillActivity.this, apiProvider, tradeStatus, new FilterPledgePopupWindow.HandleBtn() {
            @Override
            public void handleOkBtn(int[] apiProvider, int[] tradeStatus) {
                Properties prop = new Properties();
                prop.setProperty("apiProviderList", apiToString(apiProvider));
                prop.setProperty("tradeStatusList", statusToString(tradeStatus));

                PledgeBillActivity.this.apiProvider = apiProvider;
                PledgeBillActivity.this.tradeStatus = tradeStatus;
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    billList.clear();
                    Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, otherType, tradeTypeList);
                    currentPage = 2;
                    getGetBills(map, true);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        });
        popupWindow.showAsDropDown(mLlFilterTop);
    }

    private String apiToString(int[] arr) {
        if (arr == null || arr.length == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                builder.append("微信支付 ");
            } else if (arr[i] == 2) {
                builder.append("支付宝 ");
            } else if (arr[i] == 4) {
                builder.append("QQ钱包 ");
            } else if (arr[i] == 5) {
                builder.append("银联支付 ");
            } else if (arr[i] == 10) {
                builder.append("刷卡支付 ");
            }
        }
        return builder.toString();
    }

    private String statusToString(int[] arr) {
        if (arr == null || arr.length == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                builder.append("未支付 ");
            } else if (arr[i] == 2) {
                builder.append("支付成功 ");
            } else if (arr[i] == 3) {
                builder.append("已关闭 ");
            } else if (arr[i] == 4) {
                builder.append("转入退款 ");
            }
        }
        return builder.toString();
    }

    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_PLEDGE_DATA);
        intentFilter.addAction(Constants.ACTION_PLEDGE_UNFREEZE_DATA);
        mOrderBroadcastReceiver = new OrderBroadcastReceiver();
        registerReceiver(mOrderBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mOrderBroadcastReceiver);
    }

    class OrderBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.ACTION_PLEDGE_DATA.equals(action)) {
                mHandler.sendEmptyMessageDelayed(1, 1000);
            } else if (Constants.ACTION_PLEDGE_UNFREEZE_DATA.equals(action)) {
                mHandler.sendEmptyMessage(1);
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        isRefreshed = true;
                        Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                                apiProvider, tradeStatus, otherType, tradeTypeList);
                        getGetBills(map, false);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    break;
            }
        }
    };
}
