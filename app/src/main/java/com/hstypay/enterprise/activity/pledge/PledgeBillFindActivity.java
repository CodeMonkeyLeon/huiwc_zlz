package com.hstypay.enterprise.activity.pledge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.PledgeBillRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PledgeBillFindActivity extends BaseActivity implements View.OnClickListener, PledgeBillRecyclerAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack;
    private RecyclerView mRvBill;
    private TextView mTvSearch;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private EditTextDelete mEtInput;

    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<PledgeBillsBean.DataBean> mList = new ArrayList<>();
    private PledgeBillRecyclerAdapter mAdapter;
    private ImageView mIvSearch;
    private TextView mTvNull;
    private String storeMerchantId;
    private SafeDialog mLoadDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_find);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvSearch = findViewById(R.id.button_title);
        mIvSearch = (ImageView) findViewById(R.id.iv_search);
        mEtInput = (EditTextDelete) findViewById(R.id.et_user_input);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        findViewById(R.id.rg_bill_search).setVisibility(View.GONE);

        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mEtInput.setFocusable(true);
        mEtInput.setFocusableInTouchMode(true);
        mEtInput.requestFocus();
        InputMethodManager inputManager = (InputMethodManager) mEtInput.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(mEtInput, 0);
        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvBill = (RecyclerView) findViewById(R.id.bill_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvBill.setLayoutManager(mLinearLayoutManager);
        mRvBill.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        mSwipeRefreshLayout.setRefreshEnable(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mTvSearch.setOnClickListener(this);
        mIvSearch.setOnClickListener(this);
    }

    public void initData() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() /*|| MyApplication.getIsManager()*/) {
            storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
        } else if (MyApplication.getIsCasher()) {
            storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
        }
        mList = new ArrayList<>();
        mAdapter = new PledgeBillRecyclerAdapter(PledgeBillFindActivity.this, mList);
        mRvBill.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mList.clear();
                    currentPage = 2;
                    loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });
    }

    private void loadData(String pageSize, String currentPage, String orderNo) {
        if (TextUtils.isEmpty(orderNo)) {
            MyToast.showToast(UIUtils.getString(R.string.et_bill_not_null), Toast.LENGTH_SHORT);
            return;
        } else {
            //门店网络请求
            if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            map.put("authNo", orderNo);
            ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_FIND_BILLS, map);
        }
    }

    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_FIND_BILLS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeBillFindActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mAdapter.notifyDataSetChanged();
                        if (isLoadmore) {
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_FIND_BILL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                   getLoginDialog(PledgeBillFindActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        Intent intent = new Intent(PledgeBillFindActivity.this, PledgeDetailActivity.class);
                        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, msg.getData().get(0));
                        startActivity(intent);
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_title:
                finish();
                break;
            case R.id.iv_search:
                mList.clear();
                currentPage = 2;
                loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
                break;

            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        PledgeBillsBean.DataBean dataBean = mList.get(position);
        if (dataBean != null) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                Map<String, Object> map = new HashMap<>();
                map.put("authNo", dataBean.getAuthNo());
                ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_FIND_BILL, map);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }

    }

    private OnItemSearchListener mOnItemSearchListener;

    public interface OnItemSearchListener {
        void onItemSearch(String text);
    }

    public void setOnItemSearchListener(OnItemSearchListener listener) {
        this.mOnItemSearchListener = listener;
    }
}