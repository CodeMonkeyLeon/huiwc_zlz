package com.hstypay.enterprise.activity.pledge;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class PledgeCheckActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle;
    private EditText mEtCheckPwd;
    private ImageView mIvEye;
    public static PledgeCheckActivity instance = null;
    private SafeDialog mLoadDialog;
    private PledgePayBean.DataBean mData;
    private SelectDialog dialogReverse;
    private boolean isOpenPwd;
    private SafeKeyboard safeKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtCheckPwd = (EditText) findViewById(R.id.et_login_pwd);
        mIvEye = (ImageView) findViewById(R.id.iv_eye_pwd);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvTitle.setText(R.string.title_authentication);
        mButton.setVisibility(View.INVISIBLE);

        View rootView = findViewById(R.id.main_root);
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtCheckPwd.getId(), mEtCheckPwd);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtCheckPwd.isFocused()) {
                    if (mEtCheckPwd.getText().toString().length() > 0) {
                        setButtonEnable(mBtnSubmit, true);
                    } else {
                        setButtonEnable(mBtnSubmit, false);
                    }
                }

            }
        });
        mEtCheckPwd.addTextChangedListener(editTextWatcher);
        setButtonEnable(mBtnSubmit, false);
    }

    public void initEvent() {
        mBtnSubmit.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mIvEye.setOnClickListener(this);
    }

    public void initData() {
        mData = (PledgePayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_PLEDGE_PAY_BEAN);
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return !isOpen;
    }

    //订单详情返回数据
    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_TO_PAY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeCheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PledgePayBean.DataBean data = msg.getData();
                    if (data != null) {
                        if (data.isNeedQuery()) {
                            checkPledgeState(data);
                        } else {
                            mData.setOptStatus("2");
                            mData.setSumPayMoney(data.getMoney());
                            mData.setSumFreeMoney(String.valueOf(getIntent().getLongExtra(Constants.INTENT_UNFREEZE_MONEY, 0)));
                            mData.setOpUserRealName(data.getOpUserRealName());
                            startVerify(mData);
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_AUTHENTICATION_PLEDGE_UNFREEZE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.AUTHENTICATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeCheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.AUTHENTICATION_TRUE:
                    long payMoney = getIntent().getLongExtra(Constants.INTENT_PLEDGE_PAY_MONEY, 0);
                    if (payMoney == 0) {
                        pledgeReverse(mData);
                    } else {
                        authPay(mData);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_PAY_SYNCHRONY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeCheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeCheckActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PledgePayBean.DataBean dataBean = msg.getData();
                    if (dataBean != null) {
                        mData.setTradeFinishTime(dataBean.getTradeFinishTime());
                        mData.setTradeStatus(dataBean.getTradeStatus());
                        mData.setMchId(dataBean.getMchId());
                        mData.setStoreMerchantId(dataBean.getStoreMerchantId());
                        mData.setStoreMerchantIdCnt(dataBean.getStoreMerchantIdCnt());
                        mData.setOpUserRealName(dataBean.getOpUserRealName());
                        String tradeStatus = mData.getTradeStatus();
                        mData.setTradeStatus(tradeStatus);
                        if ("1".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeCheckActivity.this, getString(R.string.tv_pledge_unfreeze_doing));
                        } else if ("3".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeCheckActivity.this, getString(R.string.tv_pledge_unfreeze_failed));
                        } else if ("2".equals(tradeStatus)) {
                            startVerify(mData);
                            //交易时间
                            if (!StringUtils.isEmptyOrNull(dataBean.getTradeFinishTime())) {
                                mData.setTradeFinishTime(dataBean.getTradeFinishTime());
                            }
                        } else {
                            showWindowDialog();
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_CHECK_REVERSE)) {
            dismissLoading();
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeCheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeCheckActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    //冲正成功
                    mData.setOptStatus("2");
                    mData.setTradeStatus("9");
                    mData.setSumPayMoney("0");
                    mData.setSumFreeMoney(String.valueOf(getIntent().getLongExtra(Constants.INTENT_UNFREEZE_MONEY, 0)));
                    startVerify(mData);
                    break;
            }
        }
    }

    private void showWindowDialog() {
        dialogReverse = new SelectDialog(PledgeCheckActivity.this, getString(R.string.tv_pledge_unfreeze_doing), getString(R.string.button_continue), getString(R.string.button_go_bill), R.layout.select_common_dialog);
        dialogReverse.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                startToBills();
            }
        });
        dialogReverse.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                checkPledgeState(mData);
            }
        });
        DialogHelper.resize(PledgeCheckActivity.this, dialogReverse);
        dialogReverse.show();
    }

    private void startVerify(PledgePayBean.DataBean data) {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, data);
        intent.putExtra(Constants.INTENT_UNFREEZE_MONEY, getIntent().getLongExtra(Constants.INTENT_UNFREEZE_MONEY, 0));
        intent.putExtra(Constants.INTENT_PLEDGE_PAY_MONEY, getIntent().getLongExtra(Constants.INTENT_PLEDGE_PAY_MONEY, 0));
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CHECK_VERIFY);
        intent.setClass(PledgeCheckActivity.this, PledgeVerifyActivity.class);
        startActivity(intent);
        finish();
    }

    private void startToBills() {
        Intent intent = new Intent();
        intent.setClass(PledgeCheckActivity.this, PledgeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                submit();
                break;
            case R.id.iv_eye_pwd:
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd, mIvEye, mEtCheckPwd);
                break;
            default:
                break;
        }
    }

    private void submit() {
        if (StringUtils.isEmptyOrNull(mEtCheckPwd.getText().toString().trim())) {
            showCommonNoticeDialog(PledgeCheckActivity.this, getString(R.string.pwd_not_null));
            return;
        }
        authentication();
    }

    private void authPay(PledgePayBean.DataBean data) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(data.getOutAuthNo())) {
                map.put("outAuthNo", data.getOutAuthNo());
            }
            if (!StringUtils.isEmptyOrNull(data.getAuthNo())) {
                map.put("authNo", data.getAuthNo());
            }
            map.put("money", getIntent().getLongExtra(Constants.INTENT_PLEDGE_PAY_MONEY, 0));
            map.put("password", mEtCheckPwd.getText().toString().trim());
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            map.put("storeMerchantId", data.getStoreMerchantId());
            ServerClient.newInstance(MyApplication.getContext()).authPay(MyApplication.getContext(), Constants.TAG_PLEDGE_TO_PAY, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void authentication() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("password", mEtCheckPwd.getText().toString().trim());
            ServerClient.newInstance(MyApplication.getContext()).authentication(MyApplication.getContext(), Constants.TAG_AUTHENTICATION_PLEDGE_UNFREEZE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void checkPledgeState(PledgePayBean.DataBean data) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (data.getAuthNo() != null) {
                map.put("authNo", data.getAuthNo());
                map.put("storeMerchantId", data.getStoreMerchantId());
                ServerClient.newInstance(MyApplication.getContext()).authOptquery(MyApplication.getContext(), Constants.TAG_PLEDGE_PAY_SYNCHRONY, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void pledgeReverse(PledgePayBean.DataBean data) {
        showNewLoading(true, getString(R.string.public_order_backout));
        Map<String, Object> map = new HashMap<>();
        map.put("outAuthNo", data.getOutAuthNo());
        map.put("storeMerchantId", data.getStoreMerchantId());
        ServerClient.newInstance(PledgeCheckActivity.this).authReverse(PledgeCheckActivity.this, Constants.TAG_PLEDGE_CHECK_REVERSE, map);
    }


    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboard.isShow()) {
                safeKeyboard.hideKeyboard();
                return false;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (safeKeyboard != null) {
            safeKeyboard.release();
            safeKeyboard = null;
        }
        super.onDestroy();
    }
}
