package com.hstypay.enterprise.activity.pledge;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.utils.CreateOneDiCodeUtil;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * 交易详情
 * Created by admin on 2017/7/3.
 */
public class PledgeDetailActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvTradeBarcode;
    private Button mBtnPrint,mButton;
    private ScrollView mSvContent;
    private TextView mTvTitle, mTvPledgeUnfreezeMoney, mTvPledgePayMoney, mTvPledgePayMoneyTitle, mTvPledgeUnfreezeMoneyTitle, mTvNotice, mTvTradeTitle;
    private TextView mTvPledgeMoney, mTvTradeTime, mTvTradeType, mTvTradeNo, mTvTradeState, mTvCashier, mTvPledgeOrderNo, mTvTradeCodeNumber;
    private PledgePayBean.DataBean mPledgeData;
    private PledgeBillsBean.DataBean mPledgeBillsBean;
    private LinearLayout mLlcashier, mLlTransactionId;
    private SafeDialog mLoadDialog;
    private Printer printer;
    private PosPrintUtil mPosPrintUtil;
    private IWoyouService woyouService;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;

    private AidlPrinter printerDev = null;
    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private SelectDialog mSelectDialog;

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service

    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    /**
     * 绑定商米打印服务
     */
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_detail);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
        initPosPrint();

    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        } else */if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mTvPledgePayMoneyTitle.setOnClickListener(this);
//        mTvPledgeUnfreezeMoneyTitle.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_pay_detail);
        mTvPledgeMoney = (TextView) findViewById(R.id.tv_pledge_money);//预授权金额
        mTvTradeTime = (TextView) findViewById(R.id.tv_order_time);//交易时间
        mTvTradeType = (TextView) findViewById(R.id.tv_pledge_trade_type);//支付方式
        mTvPledgeOrderNo = (TextView) findViewById(R.id.tv_pledge_order_no);//预授权单号
        mTvTradeNo = (TextView) findViewById(R.id.tv_trade_order_no);//微信订单号
        mTvTradeTitle = (TextView) findViewById(R.id.tv_trade_order_title);//微信订单号title
        mLlTransactionId = (LinearLayout) findViewById(R.id.ll_transaction_id);//微信订单号
        mTvTradeState = (TextView) findViewById(R.id.tv_trade_state);//交易状态
        mIvTradeBarcode = (ImageView) findViewById(R.id.iv_trade_barcode);
        mTvTradeCodeNumber = (TextView) findViewById(R.id.tv_trade_code_number);
        mTvPledgePayMoney = (TextView) findViewById(R.id.tv_pledge_pay_money);
        mTvPledgeUnfreezeMoney = (TextView) findViewById(R.id.tv_pledge_unfreeze_money);
        mTvPledgePayMoneyTitle = (TextView) findViewById(R.id.tv_pledge_pay_money_title);
        mTvPledgeUnfreezeMoneyTitle = (TextView) findViewById(R.id.tv_pledge_unfreeze_money_title);
        mTvNotice = (TextView) findViewById(R.id.tv_notice);
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mButton = (Button) findViewById(R.id.btn_pledge_refund);
        //收银员title
        mLlcashier = (LinearLayout) findViewById(R.id.ll_cashier_title);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);//收银员
        setButtonWhite(mBtnPrint);
        setButtonEnable(mButton,true);
    }

    public void initData() {
        getPrintActive();
        mPledgeBillsBean = (PledgeBillsBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_PLEDGE_PAY_BEAN);
        mPledgeData = new PledgePayBean.DataBean();
        mPledgeData.setMchId(mPledgeBillsBean.getMerchantId());
        mPledgeData.setStoreMerchantId(mPledgeBillsBean.getStoreMerchantId());
        mPledgeData.setStoreMerchantIdCnt(mPledgeBillsBean.getStoreMerchantIdCnt());
        mPledgeData.setOutAuthNo(mPledgeBillsBean.getOutAuthNo());
        mPledgeData.setOutTransactionId(mPledgeBillsBean.getTransactionId());
        mPledgeData.setAuthNo(mPledgeBillsBean.getAuthNo());
        mPledgeData.setApiProvider(mPledgeBillsBean.getApiProvider());
        mPledgeData.setTradeStatus(mPledgeBillsBean.getTradeState());
        mPledgeData.setMoney(mPledgeBillsBean.getMoney());
        mPledgeData.setSumPayMoney(mPledgeBillsBean.getSumPayMoney());
        mPledgeData.setTermNo(mPledgeBillsBean.getTermNo());
        if ("9".equals(mPledgeBillsBean.getTradeState())){
            mPledgeData.setSumFreeMoney(mPledgeBillsBean.getMoney());
        } else {
            mPledgeData.setSumFreeMoney(mPledgeBillsBean.getSumFreeMoney());
        }
        mPledgeData.setRemainMoney(mPledgeBillsBean.getRemainMoney());
        mPledgeData.setOptStatus(mPledgeBillsBean.getOptStatus());
        mPledgeData.setCashierName(mPledgeBillsBean.getCashierName());
        if (!TextUtils.isEmpty(mPledgeBillsBean.getTradeTime())) {
            mPledgeData.setTradeFinishTime(mPledgeBillsBean.getTradeTime());
        } else {
            mPledgeData.setTradeFinishTime(mPledgeBillsBean.getCreateTime());
        }

        if (mPledgeData != null) {
            setView(mPledgeData);
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void setView(PledgePayBean.DataBean pledgePayBean) {
        String money = pledgePayBean.getMoney();
        if (!StringUtils.isEmptyOrNull(money)) {
            mTvPledgeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Utils.Long.tryParse(money, 0) / 100d));
        }
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getAuthNo())) {
            mTvPledgeOrderNo.setText(pledgePayBean.getAuthNo());
        }
        //交易时间
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getTradeFinishTime())) {
            mTvTradeTime.setText(pledgePayBean.getTradeFinishTime());
        }

        int apiProvider = pledgePayBean.getApiProvider();
        mTvTradeType.setText(OrderStringUtil.getPledgeTradeTypeString(apiProvider));
        mTvTradeTitle.setText(OrderStringUtil.getPledgeTypeTitleString(apiProvider));
        /*switch (apiProvider) {
            case 1:
                mTvTradeType.setText("微信-押金支付");
                mTvTradeTitle.setText("微信支付单号");
                break;
            case 2:
                mTvTradeType.setText("支付宝-押金支付");
                mTvTradeTitle.setText("支付宝单号");
                break;
            case 3:
                mTvTradeType.setText("财付通-押金支付");
                mTvTradeTitle.setText("财付通单号");
                break;
            case 4:
                mTvTradeType.setText("QQ钱包-押金支付");
                mTvTradeTitle.setText("QQ钱包单号");
                break;
            case 5:
                mTvTradeType.setText("银联-押金支付");
                mTvTradeNo.setText("银联支付单号");
                break;
            case 10:
                mTvTradeType.setText("刷卡-押金支付");
                mTvTradeTitle.setText("第三方单号");
                break;
            default:
                mTvTradeType.setText("其他-押金支付");
                mTvTradeTitle.setText("第三方单号");
                break;
        }*/
        //交易单号
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getOutTransactionId())) {
            mTvTradeNo.setText(pledgePayBean.getOutTransactionId());
            mLlTransactionId.setVisibility(View.VISIBLE);
        } else {
            mLlTransactionId.setVisibility(View.GONE);
        }
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getSumPayMoney())) {
            mTvPledgePayMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Utils.Long.tryParse(pledgePayBean.getSumPayMoney(), 0) / 100d));
        } else {
            mTvPledgePayMoney.setText(getString(R.string.tx_mark) + "0.00");
        }

        if (!StringUtils.isEmptyOrNull(pledgePayBean.getSumFreeMoney())) {
            mTvPledgeUnfreezeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Utils.Long.tryParse(pledgePayBean.getSumFreeMoney(), 0) / 100d));
        } else {
            mTvPledgeUnfreezeMoney.setText(getString(R.string.tx_mark) + "0.00");
        }

        String tradeStatus = pledgePayBean.getTradeStatus();
        if ("1".equals(tradeStatus)) {
            //如果未支付隐藏第三支付单号
            if ("1".equals(tradeStatus)) {
                mTvTradeState.setText("未支付");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.item_left_color));
                mBtnPrint.setText("获取收款结果");
            }
        } else {
            if ("2".equals(tradeStatus)) {
                mBtnPrint.setVisibility(View.VISIBLE);
                mBtnPrint.setText("打印小票");
                mTvTradeState.setText(getString(R.string.tv_pledge_trade_state2));
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
            } else if ("5".equals(tradeStatus)) {
                mBtnPrint.setVisibility(View.VISIBLE);
                mBtnPrint.setText("打印小票");
                mTvTradeState.setText(getString(R.string.tv_trade_state5));
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.home_text));
            } else if ("3".equals(tradeStatus)) {
                mTvTradeState.setText("已关闭");
                mBtnPrint.setVisibility(View.GONE);
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.item_left_color));
            } else if ("9".equals(tradeStatus)) {
                mTvTradeState.setText("已撤销");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.item_left_color));
                mBtnPrint.setVisibility(View.GONE);
            } else {
                mTvTradeState.setText("未知");
                mTvTradeState.setTextColor(UIUtils.getColor(R.color.item_left_color));
                mBtnPrint.setVisibility(View.GONE);
            }

        }
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getAuthNo())) {
            mTvTradeCodeNumber.setText(pledgePayBean.getAuthNo());
        }
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getCashierName())) {
            mLlcashier.setVisibility(View.VISIBLE);
            mTvCashier.setText(pledgePayBean.getCashierName());
        } else {
            mLlcashier.setVisibility(View.GONE);
        }

        if ("2".equals(tradeStatus) && Utils.Long.tryParse(pledgePayBean.getRemainMoney(), 0) > 0) {
            mButton.setVisibility(View.VISIBLE);
            mTvNotice.setVisibility(View.VISIBLE);
        } else {
            mButton.setVisibility(View.GONE);
            mTvNotice.setVisibility(View.GONE);
        }
        //一维码
        if (!StringUtils.isEmptyOrNull(pledgePayBean.getAuthNo())) {
            mIvTradeBarcode.setVisibility(View.VISIBLE);
            mTvTradeCodeNumber.setVisibility(View.VISIBLE);
            WindowManager wm = this.getWindowManager();
            int width = wm.getDefaultDisplay().getWidth();
            final int w = (int) (width * 0.85);
            mIvTradeBarcode.setImageBitmap(CreateOneDiCodeUtil.createCode(pledgePayBean.getAuthNo(), w, 180));
            mTvTradeCodeNumber.setText(pledgePayBean.getAuthNo());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if (mPledgeData != null) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.RESULT_CASHIER_INTENT, mPledgeData.getTradeStatus());
                    setResult(RESULT_OK, intent);
                }
                finish();
                break;
            case R.id.btn_pledge_refund:
                //解冻
//                MtaUtils.mtaId(PledgeDetailActivity.this, "C005");
                unFreeze(mPledgeData);
                break;
            case R.id.blue_print:
                //判断是打印还是查询
                getPrint();
                break;
            case R.id.tv_pledge_pay_money_title:
                showCommonNoticeDialog(this,getString(R.string.dialog_pledge_pay_content));
                break;
            case R.id.tv_pledge_unfreeze_money_title:
                showCommonNoticeDialog(this,getString(R.string.dialog_pledge_unfreeze_content));
                break;
            default:
                break;
        }
    }

    private void getPrint() {
        if (mPledgeData == null)
            return;
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        }*/
        String tradeStatus = mPledgeData.getTradeStatus();
        if ("1".equals(tradeStatus)) {
            //发送网络请求
            checkOrder();
        } else {
            mPosPrintUtil.pledgePrint(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mPledgeData,false);
        }
    }

    private void checkOrder() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (mPledgeData.getAuthNo() != null) {
                DialogUtil.safeShowDialog(mLoadDialog);
                Map<String, Object> map = new HashMap<>();
                map.put("authNo", mPledgeData.getAuthNo());
                map.put("storeMerchantId", mPledgeData.getStoreMerchantId());
                ServerClient.newInstance(PledgeDetailActivity.this).queryAuthDetail(PledgeDetailActivity.this, Constants.TAG_PLEDGE_PAY_DETAIL, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void unFreeze(PledgePayBean.DataBean pledgeData) {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, pledgeData);
        intent.setClass(PledgeDetailActivity.this, PledgeUnfreezeActivity.class);
        startActivity(intent);
    }

    //Eventbus接收数据订单状态
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_PAY_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PledgePayBean.DataBean dataBean = msg.getData();
                    if (dataBean != null) {
                        mPledgeData.setTradeFinishTime(dataBean.getTradeFinishTime());
                        mPledgeData.setTradeStatus(dataBean.getTradeStatus());
                        mPledgeData.setMchId(dataBean.getMchId());
                        mPledgeData.setStoreMerchantId(dataBean.getStoreMerchantId());
                        mPledgeData.setStoreMerchantIdCnt(dataBean.getStoreMerchantIdCnt());
                        String tradeStatus = mPledgeData.getTradeStatus();
                        if ("1".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeDetailActivity.this, "该订单未支付");
                        } else if ("3".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeDetailActivity.this, "该订单已关闭");
                        } else if ("9".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeDetailActivity.this, "该订单已撤销");
                        } else if ("2".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeDetailActivity.this, "该订单交易成功");
                            //交易时间
                            if (!StringUtils.isEmptyOrNull(dataBean.getTradeFinishTime())) {
                                mPledgeData.setTradeFinishTime(dataBean.getTradeFinishTime());
                            }
                        } else {
                            showWindowRever();
                        }
                        setView(mPledgeData);
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PledgeDetailActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PledgeDetailActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void showWindowRever() {
        dismissLoading();
        mSelectDialog = new SelectDialog(PledgeDetailActivity.this, getString(R.string.tv_revers_prompt)
                , getString(R.string.revers_tx), getString(R.string.query_tx),R.layout.select_common_dialog);
        mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                checkOrder();
            }
        });
        mSelectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                if (!NetworkUtils.isNetworkAvailable(PledgeDetailActivity.this)) {
                    showCommonNoticeDialog(PledgeDetailActivity.this, ToastHelper.toStr(R.string.network_exception));
                } else {
                    pledgeReverse();
                }
            }
        });
        DialogHelper.resize(PledgeDetailActivity.this, mSelectDialog);
        mSelectDialog.show();
    }

    private void pledgeReverse() {
        showNewLoading(true, getString(R.string.public_order_backout));
        Map<String, Object> map = new HashMap<>();
        map.put("outAuthNo", mPledgeData.getOutAuthNo());
        map.put("storeMerchantId", mPledgeData.getStoreMerchantId());
        ServerClient.newInstance(PledgeDetailActivity.this).authReverse(PledgeDetailActivity.this, Constants.TAG_PLEDGE_REVERSE, map);
    }

    //Eventbus接收数据，订单详情冲正返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderReverse(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_REVERSE)) {
            dismissLoading();
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeDetailActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    //冲正成功
                    showNoticeDialog();
                    break;
            }
        }
    }

    private void showNoticeDialog(){
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(PledgeDetailActivity.this,getString(R.string.order_cz_success),"");
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent(Constants.ACTION_PLEDGE_DATA);
                sendBroadcast(intent);
                PledgeDetailActivity.this.finish();
            }
        });
        commonNoticeDialog.show();
    }
}
