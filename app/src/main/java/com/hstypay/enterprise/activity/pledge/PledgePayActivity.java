package com.hstypay.enterprise.activity.pledge;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SingleLineZoomTextView;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.HbfqRateBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class PledgePayActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private ImageView mIvback, mIvArrow;
    private LinearLayout num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, lay_point, lay_add, lay_clear, lay_delete, scan_pay;
    private TextView mButton, mTvTitle, mTvStoreName, mTvMoney;
    private SingleLineZoomTextView mTvCalculate;
    private Context context;
    private LinearLayout mTvChoiceStore, mLlHbfqDetail;
    private String mStoreId = "";
    private StringBuilder calculateBuilder;
    private double totalMoney;
    private String mPayRemark = "";
    private SafeDialog mLoadDialog;
    private ScrollView mSvHbfq;
    private TextView mTvSixMoney, mTvSixRate, mTvTwelveMoney, mTvTwelveRate;
    private RelativeLayout mRlSix, mRlTwelve;
    private CheckBox mCbHbfq;
    private boolean enable;
    private String hbFqNum = "6";
    private List<HbfqRateBean.DataBean> mRateData;
    private SelectDialog mSelectDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_pay);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        context = MyApplication.getContext();
        initView();
        initListener();
        initData();
    }

    private void initListener() {
        mIvback.setOnClickListener(this);
        mButton.setOnClickListener(this);
        num_0.setOnClickListener(this);
        num_1.setOnClickListener(this);
        num_2.setOnClickListener(this);
        num_3.setOnClickListener(this);
        num_4.setOnClickListener(this);
        num_5.setOnClickListener(this);
        num_6.setOnClickListener(this);
        num_7.setOnClickListener(this);
        num_8.setOnClickListener(this);
        num_9.setOnClickListener(this);
        lay_point.setOnClickListener(this);
        lay_add.setOnClickListener(this);
        lay_clear.setOnClickListener(this);
        lay_delete.setOnClickListener(this);
        scan_pay.setOnClickListener(this);
        if (!MyApplication.getIsCasher()) {
            mTvChoiceStore.setOnClickListener(this);
            mIvArrow.setVisibility(View.VISIBLE);
        } else {
            mIvArrow.setVisibility(View.GONE);
        }
        mCbHbfq.setOnClickListener(this);
        mCbHbfq.setOnCheckedChangeListener(this);
        mRlSix.setOnClickListener(this);
        mRlTwelve.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvback = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        num_0 = (LinearLayout) findViewById(R.id.num_0);
        num_1 = (LinearLayout) findViewById(R.id.num_1);
        num_2 = (LinearLayout) findViewById(R.id.num_2);
        num_3 = (LinearLayout) findViewById(R.id.num_3);
        num_4 = (LinearLayout) findViewById(R.id.num_4);
        num_5 = (LinearLayout) findViewById(R.id.num_5);
        num_6 = (LinearLayout) findViewById(R.id.num_6);
        num_7 = (LinearLayout) findViewById(R.id.num_7);
        num_8 = (LinearLayout) findViewById(R.id.num_8);
        num_9 = (LinearLayout) findViewById(R.id.num_9);
        lay_point = (LinearLayout) findViewById(R.id.lay_point);
        lay_add = (LinearLayout) findViewById(R.id.lay_add);
        lay_clear = (LinearLayout) findViewById(R.id.lay_clear);
        lay_delete = (LinearLayout) findViewById(R.id.lay_delete);
        scan_pay = (LinearLayout) findViewById(R.id.ly_to_scan_pay);
        mTvMoney = (TextView) findViewById(R.id.tv_money);
        mTvCalculate = (SingleLineZoomTextView) findViewById(R.id.tv_calculate);

        mTvChoiceStore = (LinearLayout) findViewById(R.id.tv_choice_store);
        mTvStoreName = (TextView) findViewById(R.id.tv_store_name);
        mIvArrow = findViewById(R.id.iv_arrow);

        mSvHbfq = (ScrollView) findViewById(R.id.sv_hbfq);
        mLlHbfqDetail = (LinearLayout) findViewById(R.id.ll_hbfq_detail);
        mCbHbfq = (CheckBox) findViewById(R.id.cb_hbfq);
        mRlSix = (RelativeLayout) findViewById(R.id.rl_six);
        mTvSixMoney = (TextView) findViewById(R.id.tv_six_money);
        mTvSixRate = (TextView) findViewById(R.id.tv_six_rate);
        mRlTwelve = (RelativeLayout) findViewById(R.id.rl_twelve);
        mTvTwelveMoney = (TextView) findViewById(R.id.tv_twelve_money);
        mTvTwelveRate = (TextView) findViewById(R.id.tv_twelve_rate);

        mTvTitle.setText(getString(R.string.tx_pledge_pay));
        mButton.setText(getString(R.string.tv_detail));
        mButton.setTextColor(UIUtils.getColor(R.color.theme_color));
        mButton.setCompoundDrawablePadding(UIUtils.dp2px(5));
        mButton.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_pledge_bill,0,0, 0);
//        mSvHbfq.setVisibility(MyApplication.isHuabeiWhite() ? View.VISIBLE : View.GONE);
        mSvHbfq.setVisibility(View.GONE);
        enable = false;
        setPayButton(enable);
    }

    private void initData() {
        calculateBuilder = new StringBuilder();
        mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
        if (!TextUtils.isEmpty(mStoreId)) {
            getStoreVoice(mStoreId);
        }
        /*if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(PledgePayActivity.this).hbfqRateList(PledgePayActivity.this, Constants.TAG_HBFQ_RATE_LIST, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        String storeName = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME);
        if (!TextUtils.isEmpty(storeName)) {
            mTvStoreName.setText(storeName);
        }
        mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
        if (TextUtils.isEmpty(mStoreId)) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).storePort(MyApplication.getContext(), Constants.STORE_PORT_TAG, null);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PUT_STORE)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error) + Constants.STORE_PORT_TAG);
                    break;
                case Constants.PUT_STORE_FALSE:
                    if (msg != null && msg.getError() != null) {
                        showCommonNoticeDialog(PledgePayActivity.this, msg.getError().getMessage());
                    }
                    break;
                case Constants.PUT_STORE_TRUE:
                    break;
            }
        }
        if (event.getTag().equals(Constants.STORE_PORT_TAG)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreBean bean = (StoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgePayActivity.this, bean.getError().getMessage());
                                }

                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (bean.getData().getStoreName() != null) {
                                String storeName = bean.getData().getStoreName();
                                if (!TextUtils.isEmpty(storeName)) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                                    mTvStoreName.setVisibility(View.VISIBLE);
                                    mTvStoreName.setText(storeName);
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, "");
                                    mTvStoreName.setVisibility(View.INVISIBLE);
                                }
                            }
                            if (!TextUtils.isEmpty(bean.getData().getStoreId())) {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, bean.getData().getStoreId());
                                mStoreId = bean.getData().getStoreId();
                                getStoreVoice(mStoreId);
                            } else {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, true);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                                mStoreId = "";
                            }
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.GETUI_PUSH_VOICE_LIST_TAG)) {
            VoiceBean msg = (VoiceBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgePayActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgePayActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (msg.getData().get(0).getPushClose() == 1) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, true);
                        } else {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, false);
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_HBFQ_RATE_LIST)) {
            dismissLoading();
            HbfqRateBean msg = (HbfqRateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg != null && msg.getError() != null) {
                        showCommonNoticeDialog(PledgePayActivity.this, msg.getError().getMessage());
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mRateData = msg.getData();
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                startActivity(new Intent(PledgePayActivity.this, PledgeActivity.class));
                break;
            case R.id.num_0:
                AppHelper.execVibrator(context);
                calculateProcess(0);
                break;
            case R.id.num_1:
                AppHelper.execVibrator(context);
                calculateProcess(1);
                break;
            case R.id.num_2:
                AppHelper.execVibrator(context);
                calculateProcess(2);
                break;
            case R.id.num_3:
                AppHelper.execVibrator(context);
                calculateProcess(3);
                break;
            case R.id.num_4:
                AppHelper.execVibrator(context);
                calculateProcess(4);
                break;
            case R.id.num_5:
                AppHelper.execVibrator(context);
                calculateProcess(5);
                break;
            case R.id.num_6:
                AppHelper.execVibrator(context);
                calculateProcess(6);
                break;
            case R.id.num_7:
                AppHelper.execVibrator(context);
                calculateProcess(7);
                break;
            case R.id.num_8:
                AppHelper.execVibrator(context);
                calculateProcess(8);
                break;
            case R.id.num_9:
                AppHelper.execVibrator(context);
                calculateProcess(9);
                break;
            //加号
            case R.id.lay_add:
                AppHelper.execVibrator(context);
                calculateProcess("+");
                break;
            //全删
            case R.id.lay_clear:
                AppHelper.execVibrator(context);
                if (calculateBuilder.toString().contains("+")) {
                    showDialog();
                } else {
                    clearData();
                }
                break;
            //点
            case R.id.lay_point:
                AppHelper.execVibrator(context);
                calculateProcess(".");
                break;
            //删除
            case R.id.lay_delete:
                AppHelper.execVibrator(context);
                deleteData();
                break;
            //支付
            case R.id.ly_to_scan_pay:
//                MtaUtils.mtaId(MyApplication.getContext(),"B002");
                if (MyApplication.isStoreNull()) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                } else {
                    AppHelper.execVibrator(context);
                    toCaptureActivity();
                }
                break;
            case R.id.rl_six:
                if (!"6".equals(hbFqNum)) {
                    mRlSix.setBackgroundResource(R.drawable.shape_rectangle_blue_four);
                    mTvSixMoney.setTextColor(UIUtils.getColor(R.color.white));
                    mTvSixRate.setTextColor(UIUtils.getColor(R.color.white));
                    mRlTwelve.setBackgroundResource(R.drawable.shape_rectangle_white_strike_four);
                    mTvTwelveMoney.setTextColor(UIUtils.getColor(R.color.tv_pay_total_money));
                    mTvTwelveRate.setTextColor(UIUtils.getColor(R.color.tv_pay_total_money));
                    hbFqNum = "6";
                }
                break;
            case R.id.rl_twelve:
                if (!"12".equals(hbFqNum)) {
                    mRlTwelve.setBackgroundResource(R.drawable.shape_rectangle_blue_four);
                    mTvTwelveMoney.setTextColor(UIUtils.getColor(R.color.white));
                    mTvTwelveRate.setTextColor(UIUtils.getColor(R.color.white));
                    mRlSix.setBackgroundResource(R.drawable.shape_rectangle_white_strike_four);
                    mTvSixMoney.setTextColor(UIUtils.getColor(R.color.tv_pay_total_money));
                    mTvSixRate.setTextColor(UIUtils.getColor(R.color.tv_pay_total_money));
                    hbFqNum = "12";
                }
                break;
            case R.id.cb_hbfq:
                if (!enable) {
                    mCbHbfq.setChecked(false);
                    MyToast.showToastShort(getString(R.string.et_hit_code_money));
                }
                break;
            //选择门店
            case R.id.tv_choice_store:
//                MtaUtils.mtaId(MyApplication.getContext(),"B003");
                Intent intentShop = new Intent(PledgePayActivity.this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
        }
        LogUtil.d("number_calculate==" + calculateBuilder.toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            String storeName = shopBean.getStoreName();
            String storeId = shopBean.getStoreId();
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, storeId);
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
            LogUtil.d("SP_DEFAULT_STORE_ID=" + storeId + ",,SP_DEFAULT_STORE_NAME=" + storeName);
            mStoreId = storeId;
            mTvStoreName.setText(storeName);
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeName", storeName);
                map.put("storeId", storeId);
                ServerClient.newInstance(MyApplication.getContext()).postStore(MyApplication.getContext(), Constants.TAG_PUT_STORE, map);
                getStoreVoice(mStoreId);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PAY_REMARK) {
            mPayRemark = data.getStringExtra(Constants.RESULT_PAY_REMARK);
        }
    }

    //扫描二维码
    private void toCaptureActivity() {
        if (TextUtils.isEmpty(mStoreId)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(context, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_CAPTURE_PLEDGE);
        if (mLlHbfqDetail.getVisibility() == View.VISIBLE) {
            intent.putExtra(Constants.INTENT_HBFQ, hbFqNum);
        }
        startActivity(intent);
    }

    public void clearData() {
        totalMoney = 0;
        calculateBuilder.delete(0, calculateBuilder.length());
        mTvMoney.setText(getString(R.string.tv_money_zero));
        mTvMoney.setTextColor(UIUtils.getColor(R.color.line_color));
        mTvCalculate.setText(getString(R.string.tv_money_zero));
        mTvCalculate.setTextColor(UIUtils.getColor(R.color.line_color));
        enable = false;
        setPayButton(enable);
    }

    private void deleteData() {
        if (calculateBuilder.length() > 0) {
            calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            mTvCalculate.setText(calculateBuilder.toString());
        }
        calculateResult();
    }

    private void calculateProcess(Object object) {
        if (object instanceof Integer) {
            if ((calculateBuilder.toString().lastIndexOf("0") == 0 && calculateBuilder.length() == 1) ||
                    (calculateBuilder.toString().lastIndexOf("0") == calculateBuilder.length() - 1
                            && calculateBuilder.toString().lastIndexOf("+") == calculateBuilder.length() - 2)) {
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            }
            if (calculateBuilder.toString().contains(".")) {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (calculateBuilder.length() >= 3)
                    if (pointIndex == calculateBuilder.length() - 3 &&
                            calculateBuilder.toString().lastIndexOf("+") != calculateBuilder.length() - 1) {
                        return;
                    }
            }
        }
        if (object instanceof String) {
            if (TextUtils.isEmpty(calculateBuilder)) {
                if (object.equals("+")) {
                    return;
                }
                if (object.equals(".")) {
                    calculateBuilder.append("0");
                }
            } else {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (pointIndex == calculateBuilder.length() - 1) {
                    if (object.equals("+")) {
                        calculateBuilder.append("0");
                    }
                }
                if (!calculateBuilder.toString().contains("+")) {
                    if (calculateBuilder.toString().contains(".") && object.equals(".")) {
                        return;
                    }
                } else {
                    int addIndex = calculateBuilder.toString().lastIndexOf("+");
                    if (addIndex == calculateBuilder.length() - 1) {
                        if (object.equals("+")) {
                            return;
                        }
                        if (object.equals(".")) {
                            calculateBuilder.append("0");
                        }
                    } else {
                        if (pointIndex > addIndex && object.equals(".")) {
                            return;
                        }
                    }
                }
            }
        }
        calculateBuilder.append(object);
        calculateResult();
        mTvCalculate.setText(calculateBuilder.toString());
    }

    private void calculateResult() {
        if (TextUtils.isEmpty(calculateBuilder)) {
            mTvMoney.setText(getString(R.string.tv_money_zero));
            mTvCalculate.setText(getString(R.string.tv_money_zero));
            mTvCalculate.setTextColor(UIUtils.getColor(R.color.line_color));
            enable = false;
            setPayButton(enable);
        } else {
            BigDecimal bigSum = null;
            if (calculateBuilder.toString().contains("+")) {
                String[] numbers = calculateBuilder.toString().split("\\+");
                if (numbers.length == 1) {
                    LogUtil.d("number1==", numbers[0]);
                    bigSum = new BigDecimal(numbers[0]);
                } else {
                    bigSum = new BigDecimal(0);
                    if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                        numbers[numbers.length - 1] = numbers[numbers.length - 1] + "0";
                    }
                    for (int i = 0; i < numbers.length; i++) {
                        LogUtil.d("number2==", numbers[i] + "--" + i);
                        BigDecimal big = new BigDecimal(numbers[i]);
                        bigSum = bigSum.add(big);
                        LogUtil.d("number3==", bigSum.doubleValue() + "");
                    }
                }
            } else {
                String calculateString = calculateBuilder.toString();
                if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                    bigSum = new BigDecimal(calculateString + "0");
                } else {
                    bigSum = new BigDecimal(calculateString);
                }
            }
            if (bigSum.doubleValue() > 300000) {
                MyToast.showToast(UIUtils.getString(R.string.tv_pay_exceed_money), Toast.LENGTH_SHORT);
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                return;
            }
            if (bigSum.doubleValue() <= 0) {
                enable = false;
            } else {
                enable = true;
            }
            setPayButton(enable);
            totalMoney = bigSum.doubleValue();
            mTvMoney.setText(DateUtil.formatPaseMoneyUtil(bigSum));
            setHbfqDetail(mRateData, totalMoney);
        }
        mTvMoney.setTextColor(UIUtils.getColor(enable ? R.color.tv_pay_total_money : R.color.line_color));
        mTvCalculate.setTextColor(TextUtils.isEmpty(calculateBuilder) ? UIUtils.getColor(R.color.line_color) : UIUtils.getColor(R.color.tv_pay_total_money));
    }

    private void setPayButton(boolean enable) {
        if (enable) {
            Drawable rectangleDefault = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            Drawable rectanglePressed = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectanglePressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
            scan_pay.setBackground(stateListDrawable);
            scan_pay.setEnabled(true);
        } else {
            Drawable rectangleUnable = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectangleUnable.setAlpha(102);
            scan_pay.setBackground(rectangleUnable);
            scan_pay.setEnabled(false);
        }
        if (!enable) {
            mCbHbfq.setChecked(false);
        }
    }

    public void showDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(PledgePayActivity.this, getString(R.string.dialog_notice_clear), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    clearData();
                }
            });
        }
        mSelectDialog.show();
    }

    private void getStoreVoice(String storeId) {
        Map<String, Object> voiceMap = new HashMap<>();
        voiceMap.put("storeMerchantId", storeId);
        String clientid = PushManager.getInstance().getClientid(getApplicationContext());
        if (!StringUtils.isEmptyOrNull(clientid)) {
            voiceMap.put("pushDeviceId", clientid);
        }
        voiceMap.put("client", Constants.REQUEST_CLIENT_APP);
        ServerClient.newInstance(MyApplication.getContext()).getuiPushVoiceList(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_LIST_TAG, voiceMap);
    }

    private void setHbfqDetail(List<HbfqRateBean.DataBean> listRate, double money) {
        if (listRate != null && listRate.size() > 0) {
            for (int i = 0; i < listRate.size(); i++) {
                HbfqRateBean.DataBean dataBean = listRate.get(i);
                BigDecimal eachPrin = new BigDecimal(DateUtil.formatMoneyInt(money)).divide(new BigDecimal(dataBean.getHbfqNum()), BigDecimal.ROUND_DOWN);
                BigDecimal totalFeeInDecimal = new BigDecimal(DateUtil.formatMoneyInt(money)).multiply(new BigDecimal(dataBean.getRate()));
                long totalFeeInLong = totalFeeInDecimal.setScale(0, BigDecimal.ROUND_HALF_EVEN).longValue();
                BigDecimal eachFee = BigDecimal.valueOf(totalFeeInLong).divide(new BigDecimal(dataBean.getHbfqNum()), BigDecimal.ROUND_DOWN);
                BigDecimal prinAndFee = eachFee.add(eachPrin);
                if ("6".equals(dataBean.getHbfqNum())) {
                    mTvSixRate.setText("手续费" + getString(R.string.tx_mark) + eachFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "/期");
                    mTvSixMoney.setText(getString(R.string.tx_mark) + prinAndFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "x" + dataBean.getHbfqNum() + "期");
                }
                if ("12".equals(dataBean.getHbfqNum())) {
                    mTvTwelveRate.setText("手续费" + getString(R.string.tx_mark) + eachFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "/期");
                    mTvTwelveMoney.setText(getString(R.string.tx_mark) + prinAndFee.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "x" + dataBean.getHbfqNum() + "期");
                }
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mLlHbfqDetail.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        if (!"6".equals(hbFqNum)) {
            mRlSix.setBackgroundResource(R.drawable.shape_rectangle_blue_four);
            mTvSixMoney.setTextColor(UIUtils.getColor(R.color.white));
            mTvSixRate.setTextColor(UIUtils.getColor(R.color.white));
            mRlTwelve.setBackgroundResource(R.drawable.shape_rectangle_white_strike_four);
            mTvTwelveMoney.setTextColor(UIUtils.getColor(R.color.tv_pay_total_money));
            mTvTwelveRate.setTextColor(UIUtils.getColor(R.color.tv_pay_total_money));
            hbFqNum = "6";
        }
    }
}
