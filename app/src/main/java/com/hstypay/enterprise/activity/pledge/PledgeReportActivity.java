package com.hstypay.enterprise.activity.pledge;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.DateStartEndPopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.CashierActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintPledgeSum;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO} 没用到了的页面
 */

public class PledgeReportActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout mLlData, mLlShop, mLlSalesman;
    private TextView mTvPledgeMoney, mTvPledgeCount, mTvPledgeFreezeMoney, mTvPledgeFreezeCount, mTvPledgePayMoney, mTvPledgePayCount, mTvPledgeNoFreezeMoney, mTvTitle;
    private ImageView mIvCashierArrow, mIvBack,mIvShopArrow;
    private String startTime;
    private String endTime;
    private String userId ;
    private String storeMerchantId;
    private List<ReportBean.DataEntity.ListEntity> mList;
    private String storeName;
    private String cashierName;
    private PosPrintUtil mPosPrintUtil;
    private Printer printer;
    private TextView tv_date;
    private Button mBtnPrint;
    private TextView mButton, mTvShop, mTvCashier, mTvNull;
    private ScrollView mSvReport;
    private PledgeReportBean.DataBean mInfo;
    private View mView;
    private IWoyouService woyouService;
    private ReportBroadcastReceiver mReportBroadcastReceiver;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private SafeDialog mLoadDialog;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    /**
     * 绑定商米打印服务
     */
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_report);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
        initPosPrint();
    }

    @Override
    public void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        } else */if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_REPORT_DATA);
        mReportBroadcastReceiver = new ReportBroadcastReceiver();
        registerReceiver(mReportBroadcastReceiver, intentFilter);
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
        unregisterReceiver(mReportBroadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyPosPrint();
    }

    private void destroyPosPrint() {
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(PledgeReportActivity.this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintPledgeSum.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
        }
    }

    public void initView() {
        mLoadDialog = getLoadDialog(PledgeReportActivity.this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);
        mLlData = (LinearLayout) findViewById(R.id.ll_date);
        mLlShop = (LinearLayout) findViewById(R.id.ll_shop);
        mTvShop = (TextView) findViewById(R.id.tv_shop);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);
        mLlSalesman = (LinearLayout) findViewById(R.id.ll_cashier);
        tv_date = (TextView) findViewById(R.id.tv_date);
        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
        setDateView(startTime, endTime);

        mTvPledgeMoney = (TextView) findViewById(R.id.tv_pledge_money);//预授权金额
        mTvPledgeCount = (TextView) findViewById(R.id.tv_pledge_count);//预授权笔数
        mTvPledgeFreezeMoney = (TextView) findViewById(R.id.tv_pledge_freeze_money);//累计解冻金额
        mTvPledgeFreezeCount = (TextView) findViewById(R.id.tv_pledge_freeze_count);//累计解冻笔数
        mTvPledgePayMoney = (TextView) findViewById(R.id.tv_pledge_pay_money);//累计转支付金额
        mTvPledgePayCount = (TextView) findViewById(R.id.tv_pledge_pay_count);//累计转支付笔数
        mTvPledgeNoFreezeMoney = (TextView) findViewById(R.id.tv_pledge_no_freeze_money);//未解冻金额

        mBtnPrint = (Button) findViewById(R.id.btn_print);
        mSvReport = (ScrollView) findViewById(R.id.sv_report);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mSvReport.setVisibility(View.GONE);
        mTvNull.setVisibility(View.GONE);
        mIvCashierArrow = (ImageView) findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = (ImageView) findViewById(R.id.iv_shop_arrow);

        mTvTitle.setText(getString(R.string.tv_report));
        mButton.setVisibility(View.INVISIBLE);
        setButtonWhite(mBtnPrint);

        storeName = getString(R.string.tv_shop);
        cashierName = getString(R.string.tv_all_user);
        if (MyApplication.getIsCasher()) {
            mIvCashierArrow.setVisibility(View.GONE);
            mTvCashier.setText(MyApplication.getRealName());
            userId = MyApplication.getUserId();
            mLlSalesman.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            storeMerchantId = MyApplication.getDefaultStore();
            mLlShop.setEnabled(false);
        } else {
            storeMerchantId = "";
            userId = "";
        }
    }

    private void setDateView(String startTime, String endTime) {
        try {
            if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formartDateToMMDD(endTime))) {
                tv_date.setText(DateUtil.formartDateToMMDD(startTime));
            } else {
                tv_date.setText(DateUtil.formartDateToMMDD(startTime) + " " + getString(R.string.tv_least) + " "
                        + DateUtil.formartDateToMMDD(endTime));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mLlData.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlSalesman.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
    }

    public void initData() {
        mList = new ArrayList<>();
        storeMerchantId = null;
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_REPORT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeReportBean msg = (PledgeReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeReportActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_REPORT_TRUE://请求成功
                    if (MyApplication.getIsCasher()) {
                        sendTodayDataBroadcast(PledgeReportActivity.this);
                    }
                    if (msg.getData() != null) {
                        mInfo = msg.getData();
                        mInfo.setCashierName(cashierName);
                        mInfo.setStoreName(storeName);
                        mTvNull.setVisibility(View.GONE);
                        mSvReport.setVisibility(View.VISIBLE);
                        setView(msg.getData());
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSvReport.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    private void showPopupDateWindow() {
        DateStartEndPopupWindow moneyPopupWindow = new DateStartEndPopupWindow(PledgeReportActivity.this, startTime, endTime, new DateStartEndPopupWindow.HandleTv() {
            @Override
            public void getDate(String startTime, String endTime) {
                PledgeReportActivity.this.startTime = startTime;
                PledgeReportActivity.this.endTime = endTime;
                setDateView(startTime, endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        });
        moneyPopupWindow.showAtLocation(mBtnPrint, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }

    public void getReport(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            ServerClient.newInstance(MyApplication.getContext()).getPledgeReport(MyApplication.getContext(), Constants.TAG_PLEDGE_REPORT, map);
        } else {
            mTvNull.setText("网络异常");
            mTvNull.setVisibility(View.VISIBLE);
            mSvReport.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_date:
                MtaUtils.mtaId(this, "1016");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    showPopupDateWindow();
                }
                break;
            case R.id.ll_shop:
                MtaUtils.mtaId(this, "1017");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    Intent intentShop = new Intent(PledgeReportActivity.this, ShopActivity.class);
                    if (TextUtils.isEmpty(storeMerchantId)) {
                        intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                    } else {
                        intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                    }
                    intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_COLLECT_SHOP);
                    intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                }
                break;
            case R.id.ll_cashier:
                MtaUtils.mtaId(this, "1018");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    Intent intentCashier = new Intent(PledgeReportActivity.this, CashierActivity.class);
                    if (TextUtils.isEmpty(userId)) {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                    } else {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                    }
                    intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                    intentCashier.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                }
                break;
            case R.id.btn_print:
                MtaUtils.mtaId(this, "1019");
                mInfo.setStoreMerchantId(storeMerchantId);
                bluePrint();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeName = shopBean.getStoreName();
            storeMerchantId = shopBean.getStoreId();
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText("全部收银员");
                cashierName = "全部收银员";
                userId = "";
            }
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            cashierName = cashierBean.getRealName();
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHOICE_DATE) {
            Bundle extras = data.getExtras();
            String time = extras.getString(Constants.RESULT_CHOICE_DATE_INTENT);
            if (!StringUtils.isEmptyOrNull(time)) {
                String t[] = time.split("\\|");
                startTime = t[0];
                endTime = t[1];
                setDateView(startTime, endTime);
                LogUtil.d("date=" + startTime + "///" + endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        }
    }

    private void bluetoothPrint(PledgeReportBean.DataBean info) {
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            showPrintDialog(1);
            return;
        }
        new Thread(){
            @Override
            public void run() {
                super.run();
                if (MyApplication.bluetoothSocket != null && MyApplication.bluetoothSocket.isConnected()) {
                    BluePrintUtil.printPledgeReportSum(info);
                } else {
                    if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
                        boolean isSucc = BluePrintUtil.blueConnent(1, PledgeReportActivity.this);
                        if (isSucc) {
                            BluePrintUtil.printPledgeReportSum(info);
                        }else {
                            if (BluetoothAdapter.getDefaultAdapter().isEnabled()){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showPrintDialog(1);
                                    }
                                });
                            }else {
                                //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                                LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                            }
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showPrintDialog(1);
                            }
                        });
                    }
                }
            }
        }.start();

    }

    public void showCommonNoticeDialog(Activity activity, String content) {
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, content, "");
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void bluePrint() {
        if (mInfo != null) {
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
            if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                mInfo.setStartTime(startTime);
                mInfo.setEndTime(endTime);
                long now = System.currentTimeMillis();
                if (now < DateUtil.getTime(endTime,"yyyy-MM-dd HH:mm:ss")) {
                    mInfo.setEndTime(DateUtil.formatTime(now, "yyyy-MM-dd HH:mm:ss"));
                } else {
                    mInfo.setEndTime(endTime);
                }
            } else {
                mInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                mInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
            mInfo.setCashierName(mTvCashier.getText().toString().trim());
            mInfo.setStoreName(mTvShop.getText().toString().trim());
            LogUtil.d("date===" + mInfo.getStartTime() + "//////" + mInfo.getEndTime() + "////" + mInfo.getPrintTime());
            if (AppHelper.getAppType() == 1) {
                mPosPrintUtil.printPledgeReport(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mInfo);
            } else if (AppHelper.getAppType() == 2) {
                bluetoothPrint(mInfo);
            }
        } else {
            showCommonNoticeDialog(PledgeReportActivity.this, getString(R.string.print_data_error));
        }
    }

    public void setView(PledgeReportBean.DataBean dataEntity) {
        if (dataEntity != null) {
            mTvPledgeMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumPreMoney() / 100d));
            mTvPledgeCount.setText(dataEntity.getCntPreMoney() + "");
            mTvPledgeFreezeMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumFreeMoney() / 100d));
            mTvPledgeFreezeCount.setText(dataEntity.getCntFreeMoney() + "");
            mTvPledgePayMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumPayMoney() / 100d));
            mTvPledgePayCount.setText(dataEntity.getCntPayMoney() + "");
            //mTvPledgeNoFreezeMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumNotFreeMoney() / 100d));
        }
    }

    class ReportBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        }
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

}
