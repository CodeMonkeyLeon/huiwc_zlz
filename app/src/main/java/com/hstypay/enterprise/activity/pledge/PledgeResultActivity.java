package com.hstypay.enterprise.activity.pledge;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.utils.CreateOneDiCodeUtil;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintPledgeYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.hstypay.enterprise.voice.VoiceUtils;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;


/**
 * 支付成功
 * Created by admin on 2017/7/3.
 */

public class PledgeResultActivity extends BaseActivity implements View.OnClickListener {
    private Button mBlue_print;
    private ScrollView mSvContent;
    private ImageView mIvBack, mIvPayStatus,mIvTradeBarcode;
    private Button mBtnComplete, mBtnPrint;
    private TextView mButton, mTvTitle, mTvPayStatus;
    private TextView mTvPledgeMoney, mTvTradeTime, mTvTradeType, mTvTradeTitle, mTvCashier, mTvPledgeOrderNo, mTvTradeNo,mTvTradeCodeNumber;
    private PledgePayBean.DataBean mPledgePayBean;
    private LinearLayout mLlCashier;
    private Printer printer;
    private PosPrintUtil mPosPrintUtil;
    private IWoyouService woyouService;
    private SafeDialog mLoadDialog;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;

    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private N900Device n900Device;

    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private String mIntentName;

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
            autoPrint();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("print----onServiceDisconnected");
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogUtil.d("print----onServiceConnected");
            woyouService = IWoyouService.Stub.asInterface(service);
            autoPrint();
        }
    };

    /**
     * 商米绑定打印服务
     **/
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 汇付绑定打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_result);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
        initPosPrint();
        if (!MyApplication.getIsCasher()) {
            sendTodayDataBroadcast(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
//            bindDeviceService();
            autoPrint();
        } else if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
            //autoPrint();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            autoPrint();
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
            autoPrint();
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintPledgeYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
            autoPrint();
        } else if ("a920".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("hdy".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        }

    }

    private void autoPrint() {
        if (Constants.INTENT_NAME_CAPTURE_PAYRESULT.equals(mIntentName)) {
            if (mPledgePayBean != null) {
                mPledgePayBean.setTradeStatus("2");
                mPledgePayBean.setTermNo(StringUtils.getDeviceInfo(AppHelper.getSN()));
                mPosPrintUtil.pledgePrint(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService,printerModule, mPledgePayBean,true);
            }
            mIntentName = "";
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mBlue_print.setOnClickListener(this);
        mBtnComplete.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvPayStatus = (ImageView) findViewById(R.id.logo_title);
        mTvPayStatus = (TextView) findViewById(R.id.tv_pay_status);
        mTvPledgeMoney = (TextView) findViewById(R.id.tv_pledge_money);//收款金额
        mTvTradeTime = (TextView) findViewById(R.id.tv_order_time);//交易时间
        mTvTradeType = (TextView) findViewById(R.id.tv_pledge_trade_type);//支付方式
        mTvTradeTitle = (TextView) findViewById(R.id.tv_trade_order_title);//支付方式单号title
        mTvPledgeOrderNo = (TextView) findViewById(R.id.tv_pledge_order_no);//支付方式单号
        mTvTradeNo = (TextView) findViewById(R.id.tv_trade_order_no);//平台订单号
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);//收银员
        mIvTradeBarcode = (ImageView) findViewById(R.id.iv_trade_barcode);
        mTvTradeCodeNumber = (TextView) findViewById(R.id.tv_trade_code_number);
        mBtnComplete = (Button) findViewById(R.id.btn_success);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mTvTitle.setText(R.string.title_pay_detail);
        mButton.setText(R.string.tv_refund);
        mBlue_print = (Button) findViewById(R.id.blue_print);
        mButton.setVisibility(View.INVISIBLE);
        mLlCashier = (LinearLayout) findViewById(R.id.ll_cashier_title);

        setButtonEnable(mBtnComplete,true);
        setButtonWhite(mBtnPrint);
    }

    public void initData() {
        getPrintActive();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mPledgePayBean = (PledgePayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_PLEDGE_PAY_BEAN);

        if (mPledgePayBean != null) {
            //交易单号
            if (!StringUtils.isEmptyOrNull(mPledgePayBean.getOutTransactionId())) {
                mTvTradeNo.setText(mPledgePayBean.getOutTransactionId());
            }
            //预授权单号
            if (!StringUtils.isEmptyOrNull(mPledgePayBean.getAuthNo())) {
                mTvPledgeOrderNo.setText(mPledgePayBean.getAuthNo());
            }

            //支付时间
            if (!StringUtils.isEmptyOrNull(mPledgePayBean.getTradeFinishTime())) {
                mTvTradeTime.setText(mPledgePayBean.getTradeFinishTime());
            }

            if (MyApplication.getIsMerchant() || MyApplication.getIsAdmin() || MyApplication.getIsManager()) {
                mLlCashier.setVisibility(View.GONE);
            } else {
                //收银员
                if (!StringUtils.isEmptyOrNull(mPledgePayBean.getOpUserRealName())) {
                    mTvCashier.setText(mPledgePayBean.getOpUserRealName());
                } else {
                    mLlCashier.setVisibility(View.GONE);
                }
            }
            int apiProvider = mPledgePayBean.getApiProvider();
            mTvTradeType.setText(OrderStringUtil.getPledgeTradeTypeString(apiProvider));
            mTvTradeTitle.setText(OrderStringUtil.getPledgeTypeTitleString(apiProvider));
            /*switch (apiProvider) {
                case 1:
                    mTvTradeType.setText("微信-押金支付");
                    mTvTradeTitle.setText("微信支付单号");
                    break;
                case 2:
                    mTvTradeType.setText("支付宝-押金支付");
                    mTvTradeTitle.setText("支付宝单号");
                    break;
                case 3:
                    mTvTradeType.setText("财付通-押金支付");
                    mTvTradeTitle.setText("财付通单号");
                    break;
                case 4:
                    mTvTradeType.setText("QQ钱包-押金支付");
                    mTvTradeTitle.setText("QQ钱包单号");
                    break;
                case 5:
                    mTvTradeType.setText("银联-押金支付");
                    mTvTradeTitle.setText("银联支付单号");
                    break;
                case 10:
                    mTvTradeType.setText("刷卡-押金支付");
                    mTvTradeTitle.setText("第三方单号");
                    break;
                default:
                    mTvTradeType.setText("其他-押金支付");
                    mTvTradeTitle.setText("第三方单号");
                    break;
            }*/
            //一维码
            if (!StringUtils.isEmptyOrNull(mPledgePayBean.getAuthNo())) {
                mIvTradeBarcode.setVisibility(View.VISIBLE);
                mTvTradeCodeNumber.setVisibility(View.VISIBLE);
                WindowManager wm = this.getWindowManager();
                int width = wm.getDefaultDisplay().getWidth();
                final int w = (int) (width * 0.85);
                mIvTradeBarcode.setImageBitmap(CreateOneDiCodeUtil.createCode(mPledgePayBean.getAuthNo(), w, 180));
                mTvTradeCodeNumber.setText(mPledgePayBean.getAuthNo());
            }
            String money = mPledgePayBean.getMoney();
            if (!StringUtils.isEmptyOrNull(money)) {
                mTvPledgeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Long.parseLong(money) / 100d));
                //改由推送语音播报
                /*String intent_name = getIntent().getStringExtra(Constants.INTENT_NAME);
                if (ConfigUtil.getuiEnable() && Constants.INTENT_NAME_CAPTURE_PAYRESULT.equals(intent_name) && MyApplication.isOpenVoice()) {
                    PushModel pushModel = new PushModel();
                    pushModel.setVoiceContent("旺旺，收款成功，" + (Long.parseLong(money) / 100d) + "元");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String moneyString = new BigDecimal(mPledgePayBean.getMoney()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).toString();
                            play(moneyString);
                        }
                    }).start();
                }*/
            }
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                /*finish();
                break;*/
            case R.id.btn_success:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            //打印小票
            case R.id.blue_print:
                getPrint();
                break;
            default:
                break;
        }
    }

    public void getPrint() {
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        } else */if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        if (mPledgePayBean != null) {
            mPledgePayBean.setTradeStatus("2");
            mPledgePayBean.setTermNo(StringUtils.getDeviceInfo(AppHelper.getSN()));
            mPosPrintUtil.pledgePrint(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService,printerModule, mPledgePayBean,false);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PledgeResultActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PledgeResultActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
    }

    public static void startActivity(Context mContext, PayBean order) {
        Intent it = new Intent();
        it.setClass(mContext, PledgeResultActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("order", order);
        mContext.startActivity(it);
    }

    private synchronized void play(final String str) {
        if (VoiceUtils.with(this).getIsPlay()) {
            LogUtil.d("正在播放语音 ");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Thread.sleep(100);//休眠0.1秒
                        play(str);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                    /**
                     * 要执行的操作
                     */
                }
            }.start();
        } else {
            LogUtil.d("不冲突");
            VoiceUtils.with(this).play(str, true);
        }
    }
}
