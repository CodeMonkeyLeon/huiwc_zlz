package com.hstypay.enterprise.activity.pledge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.EditCommonDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ApiRes;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.RefundPwd;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class PledgeUnfreezeActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private final static String TAG_QUERY_REFUND_PWD = "tag_query_refund_pwd";
    private ImageView mIvBack;
    private Button mBtnUnfreeze;
    private PledgePayBean.DataBean mData;
    private TextView mButton, mTvTitle, mTvPledgeOrderNo, mTvPledgeFreezeMoney, mTvPledgeUnfreezeMoney, mTvPledgeUnfreezeNotice;
    private EditText mEtPledgePayMoney;
    private LinearLayout mLlEditMoney, mLlConsume, mLlReverse;
    public static PledgeUnfreezeActivity instance = null;
    private EditCommonDialog mDialog;
    private SafeDialog mLoadDialog;
    private String editString;
    private long mPayMoney;
    private long mMoney;
    private RadioGroup mRgTitle;
    private RadioButton mRbLeft, mRbRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_unfreeze);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnUnfreeze = (Button) findViewById(R.id.btn_submit);

        mTvPledgeOrderNo = (TextView) findViewById(R.id.tv_pledge_order_no);
        mTvPledgeFreezeMoney = (TextView) findViewById(R.id.tv_pledge_freeze_money);//未解冻金额
        mEtPledgePayMoney = (EditText) findViewById(R.id.et_pledge_pay_money);//转支付金额
        mTvPledgeUnfreezeMoney = (TextView) findViewById(R.id.tv_pledge_unfreeze_money);//解冻金额
        mTvPledgeUnfreezeNotice = (TextView) findViewById(R.id.tv_pledge_unfreeze_notice);//说明
        mLlEditMoney = (LinearLayout) findViewById(R.id.ll_edit_money);
        mLlConsume = findViewById(R.id.ll_consume);
        mLlReverse = findViewById(R.id.ll_reverse);


        mRgTitle = findViewById(R.id.rg_title);
        mRbLeft = findViewById(R.id.rb_title_left);
        mRbRight = findViewById(R.id.rb_title_right);

        mTvTitle.setText(R.string.title_unfreeze);
        mButton.setVisibility(View.INVISIBLE);
        setButtonEnable(mBtnUnfreeze, false);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnUnfreeze.setOnClickListener(this);
        mRgTitle.setOnCheckedChangeListener(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 3);
                        mEtPledgePayMoney.setText(s);
                        mEtPledgePayMoney.setSelection(s.length());
                    }
                }
                if (s.toString().trim().equals(".")) {
                    s = "0" + s;
                    mEtPledgePayMoney.setText(s);
                    mEtPledgePayMoney.setSelection(2);
                }
                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        mEtPledgePayMoney.setText(s.subSequence(0, 1));
                        mEtPledgePayMoney.setSelection(1);
                        return;
                    }
                }
                if (Utils.Double.tryParse(s.toString().trim(), 0) > (new BigDecimal(mData.getMoney()).longValue() / 100d)) {
                    mEtPledgePayMoney.setText(s.subSequence(0, s.length() - 1));
                    mEtPledgePayMoney.setSelection(mEtPledgePayMoney.getText().toString().trim().length());
                    return;
                }
                long payMoney = BigDecimal.valueOf(Utils.Double.tryParse(mEtPledgePayMoney.getText().toString().trim(), -1)).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
                long money = new BigDecimal(mData.getRemainMoney()).longValue();
                if (mEtPledgePayMoney.getText().toString().trim().length() == 0) {
                    mTvPledgeUnfreezeMoney.setText("");
                } else {
                    mTvPledgeUnfreezeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil((money - payMoney) / 100d));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Utils.Double.tryParse(mEtPledgePayMoney.getText().toString().trim(), -1) > 0) {
                    setButtonEnable(mBtnUnfreeze, true);
                } else {
                    setButtonEnable(mBtnUnfreeze, false);
                }
            }
        };
        mEtPledgePayMoney.addTextChangedListener(textWatcher);
    }

    public void initData() {
        mData = (PledgePayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_PLEDGE_PAY_BEAN);
        mTvPledgeUnfreezeNotice.setText("1.预授权订单必须进行退押金操作后才能退款给消费者或者结算给商家，每笔预授权订单仅支持一次退押金操作；" +
                "\n2.退还金额=已收取押金-转支付金额；" +
                "\n3.转支付金额：转支付金额指交易金额，当日转支付金额会计算到当日交易金额中，并在下一个清分日结算给商户；" +
                "\n4.退押金操作完成后，剩余押金（即退款金额）将按原路返回至消费者银行卡或支付账户，到账时间以第三方为准；" +
                "\n5.转支付金额记录可在商户后台【交易管理-交易明细】处查询到对应的交易订单记录;" +
                "\n6.预授权订单冻结有效期为30天，30天后未退押金的订单将按原路全额退还给消费者。");
        if (mData != null) {
            if (!StringUtils.isEmptyOrNull(mData.getAuthNo())) {
                mTvPledgeOrderNo.setText(getText(R.string.tv_pledge_order_no) + "：" + mData.getAuthNo());
            }
            if (!TextUtils.isEmpty(mData.getMoney())) {
                mTvPledgeFreezeMoney.setText(getString(R.string.tv_pledge_max_money) + DateUtil.formatMoneyUtil(Utils.Long.tryParse(mData.getRemainMoney(), -1) / 100d) + "元");
            }
            mEtPledgePayMoney.setFocusable(true);
            mEtPledgePayMoney.setFocusableInTouchMode(true);
            mEtPledgePayMoney.requestFocus();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                MtaUtils.mtaId(PledgeUnfreezeActivity.this, "C006");
                submit();
                break;
            default:
                break;
        }
    }

    private void submit() {
        mMoney = new BigDecimal(mData.getRemainMoney()).longValue();
        if (mRbLeft.isChecked()) {
            if (!StringUtils.isNumberData(mEtPledgePayMoney.getText().toString().trim()) || Utils.Double.tryParse(mEtPledgePayMoney.getText().toString().trim(), -1) < 0) {
                MyToast.showToastShort(getString(R.string.regex_number_refund_money));
                return;
            }
            mPayMoney = BigDecimal.valueOf(Utils.Double.tryParse(mEtPledgePayMoney.getText().toString().trim(), -1)).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
            if (mPayMoney > mMoney) {
                MyToast.showToastShort(getString(R.string.regex_unfreeze_money_large));
                return;
            }
        } else {
            mPayMoney = 0;
        }
        queryRefundPwd();
    }

    public void showEditDialog(Activity activity, boolean isRefundPwd) {
        mDialog = new EditCommonDialog(activity, "验证身份", isRefundPwd ? "请输入退款密码" : "请输入密码", true, isRefundPwd, edit -> {
            if (!TextUtils.isEmpty(edit)) {
                editString = edit;
                if (mRbRight.isChecked()) {
                    authentication(edit);
                } else {
                    authPay(mData);
                }
            }
        }, null);
        DialogHelper.resize(activity, mDialog);
        mDialog.show();
    }

    private void authentication(String edit) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("password",  INITDES3Util.getSecretContent(this, edit));
            map.put("desFlag", 1);
            map.put("storeMerchantId", mData.getStoreMerchantId());
            map.put("outAuthNo", mData.getOutAuthNo());
            ServerClient.newInstance(MyApplication.getContext()).verifyRefundPwd(MyApplication.getContext(), Constants.TAG_AUTHENTICATION_PLEDGE_UNFREEZE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(TAG_QUERY_REFUND_PWD)) {//判断是否专用密码
            DialogUtil.safeCloseDialog(mLoadDialog);
            if (event.getCls().equals(Constants.ON_EVENT_TRUE)) {
                ApiRes<RefundPwd> res = (ApiRes<RefundPwd>) event.getMsg();
                RefundPwd refundPwd = res.getData();
                showEditDialog(this, refundPwd.getRefundCheckType() == 2);
            } else {
                failHandler(event);
            }
        }
        if (event.getTag().equals(Constants.TAG_AUTHENTICATION_PLEDGE_UNFREEZE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.AUTHENTICATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.AUTHENTICATION_TRUE:
                    /*if (mRbRight.isChecked()) {
//                        showReverseNotice();
                        pledgeReverse(mData);
                    } else {
                        authPay(mData);
                    }*/
                    mData.setOptStatus("2");
                    mData.setTradeStatus("9");
                    mData.setSumPayMoney("0");
                    mData.setSumFreeMoney(String.valueOf(mMoney - mPayMoney));
                    startVerify(mData);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_CHECK_REVERSE)) {
            dismissLoading();
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    //冲正成功
                    mData.setOptStatus("2");
                    mData.setTradeStatus("9");
                    mData.setSumPayMoney("0");
                    mData.setSumFreeMoney(String.valueOf(mMoney - mPayMoney));
                    startVerify(mData);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_TO_PAY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    PledgePayBean.DataBean data = msg.getData();
                    if (data != null) {
                        if (data.isNeedQuery()) {
                            checkPledgeState(data);
                        } else {
                            mData.setOptStatus("2");
                            mData.setSumPayMoney(data.getMoney());
                            mData.setSumFreeMoney(String.valueOf(mMoney - mPayMoney));
                            mData.setOpUserRealName(data.getOpUserRealName());
                            startVerify(mData);
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_PAY_SYNCHRONY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgePayBean msg = (PledgePayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(PledgeUnfreezeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PledgePayBean.DataBean dataBean = msg.getData();
                    if (dataBean != null) {
                        mData.setTradeFinishTime(dataBean.getTradeFinishTime());
                        mData.setTradeStatus(dataBean.getTradeStatus());
                        mData.setMchId(dataBean.getMchId());
                        mData.setStoreMerchantId(dataBean.getStoreMerchantId());
                        mData.setStoreMerchantIdCnt(dataBean.getStoreMerchantIdCnt());
                        mData.setOpUserRealName(dataBean.getOpUserRealName());
                        String tradeStatus = mData.getTradeStatus();
                        mData.setTradeStatus(tradeStatus);
                        if ("1".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeUnfreezeActivity.this, getString(R.string.tv_pledge_unfreeze_doing));
                        } else if ("3".equals(tradeStatus)) {
                            showCommonNoticeDialog(PledgeUnfreezeActivity.this, getString(R.string.tv_pledge_unfreeze_failed));
                        } else if ("2".equals(tradeStatus)) {
                            startVerify(mData);
                            //交易时间
                            if (!StringUtils.isEmptyOrNull(dataBean.getTradeFinishTime())) {
                                mData.setTradeFinishTime(dataBean.getTradeFinishTime());
                            }
                        } else {
                            showWindowDialog();
                        }
                    }
                    break;
            }
        }
    }

    private void startVerify(PledgePayBean.DataBean data) {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, data);
        intent.putExtra(Constants.INTENT_UNFREEZE_MONEY, mMoney - mPayMoney);
        intent.putExtra(Constants.INTENT_PLEDGE_PAY_MONEY, mPayMoney);
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CHECK_VERIFY);
        intent.setClass(PledgeUnfreezeActivity.this, PledgeVerifyActivity.class);
        startActivity(intent);
        finish();
    }

    private void showReverseNotice() {
        SelectDialog selectDialog = new SelectDialog(PledgeUnfreezeActivity.this, "确定全额退？", R.layout.select_common_dialog);
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                pledgeReverse(mData);
            }
        });
        selectDialog.show();
    }

    private void pledgeReverse(PledgePayBean.DataBean data) {
        showNewLoading(true, getString(R.string.public_order_backout));
        Map<String, Object> map = new HashMap<>();
        map.put("outAuthNo", data.getOutAuthNo());
        map.put("storeMerchantId", data.getStoreMerchantId());
        ServerClient.newInstance(PledgeUnfreezeActivity.this).authReverse(PledgeUnfreezeActivity.this, Constants.TAG_PLEDGE_CHECK_REVERSE, map);
    }

    private void authPay(PledgePayBean.DataBean data) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!StringUtils.isEmptyOrNull(data.getOutAuthNo())) {
                map.put("outAuthNo", data.getOutAuthNo());
            }
            if (!StringUtils.isEmptyOrNull(data.getAuthNo())) {
                map.put("authNo", data.getAuthNo());
            }
            map.put("money", mPayMoney);
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            if (!TextUtils.isEmpty(StringUtils.getDeviceInfo(AppHelper.getSN()))) {
                map.put("opDeviceId", StringUtils.getDeviceInfo(AppHelper.getSN()));
            }
            map.put("password",  INITDES3Util.getSecretContent(this, editString));
            map.put("desFlag", 1);
            map.put("storeMerchantId", data.getStoreMerchantId());
            ServerClient.newInstance(MyApplication.getContext()).authPay2(MyApplication.getContext(), Constants.TAG_PLEDGE_TO_PAY, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void checkPledgeState(PledgePayBean.DataBean data) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (data.getAuthNo() != null) {
                map.put("authNo", data.getAuthNo());
                map.put("storeMerchantId", data.getStoreMerchantId());
                ServerClient.newInstance(MyApplication.getContext()).authOptquery(MyApplication.getContext(), Constants.TAG_PLEDGE_PAY_SYNCHRONY, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void showWindowDialog() {
        SelectDialog dialog = new SelectDialog(PledgeUnfreezeActivity.this, getString(R.string.tv_pledge_unfreeze_doing)
                , getString(R.string.button_go_bill), getString(R.string.button_continue), R.layout.select_common_dialog);
        dialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                startToBills();
            }
        });
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                //继续查询
                checkPledgeState(mData);
            }
        });
    }

    private void startToBills() {
        Intent intent = new Intent();
        intent.setClass(PledgeUnfreezeActivity.this, PledgeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_title_left:
                mEtPledgePayMoney.setText("");
                mLlConsume.setVisibility(View.VISIBLE);
                mLlReverse.setVisibility(View.GONE);
                break;
            case R.id.rb_title_right:
                mTvPledgeUnfreezeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Utils.Long.tryParse(mData.getRemainMoney(), -1) / 100d));
                mLlConsume.setVisibility(View.GONE);
                mLlReverse.setVisibility(View.VISIBLE);
                setButtonEnable(mBtnUnfreeze, true);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefund(NoticeEvent event) {

    }

    /**
     * 查询退款密码
     */
    private void queryRefundPwd() {
        DialogUtil.safeShowDialog(mLoadDialog);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", mData.getStoreMerchantId());
            ServerClient.newInstance(this).queryRefundPwd(this, TAG_QUERY_REFUND_PWD, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

    }
}
