package com.hstypay.enterprise.activity.pledge;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

public class PledgeVerifyActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvRefundStatus;
    private Button mBtnPrint, mBtnComplete;
    private TextView mButton, mTvTitle, mTvPayMoney, mTvApplyDate, mTvConductor, mTvPledgeCode, mTvRefundMoney, mTvRefundStatus, mTvRefundMoneyNotice;
    private LinearLayout mLlPayMoney, mLlRefundMoney;
    private PledgePayBean.DataBean mPledgeData;
    private SafeDialog mLoadDialog;
    private PosPrintUtil mPosPrintUtil;
    private Printer printer;
    private IWoyouService woyouService;
    private PrintUtils mPrintUtils;
    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private String mIntentName;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
            autoPrint();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
            autoPrint();
        }
    };

    /**
     * 绑定商米打印服务
     **/
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge_verify);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
        initPosPrint();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if (Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
//            bindDeviceService();
            autoPrint();
        } else if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
            //autoPrint();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if (Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            autoPrint();
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
            autoPrint();
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
            autoPrint();
        } else if ("a920".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("hdy".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        }
        if (AppHelper.getAppType() == 2 && MyApplication.autoPrint()) {
            autoPrint();
        }
    }

    private void autoPrint() {
        if (Constants.INTENT_NAME_CHECK_VERIFY.equals(mIntentName)) {
            if (mPledgeData != null) {
                mPledgeData.setTermNo(StringUtils.getDeviceInfo(AppHelper.getSN()));
                mPosPrintUtil.pledgePrint(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mPledgeData, true);
            }
            mIntentName = "";
        }
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mTvPayMoney = (TextView) findViewById(R.id.tv_pledge_pay_money);
        mTvRefundMoney = (TextView) findViewById(R.id.tv_pledge_refund_money);
        mTvApplyDate = (TextView) findViewById(R.id.tv_apply_date);
        mTvConductor = (TextView) findViewById(R.id.tv_pledge_unfreeze_conductor);
        mTvPledgeCode = (TextView) findViewById(R.id.tv_pledge_order_no);
        mLlPayMoney = (LinearLayout) findViewById(R.id.ll_pledge_pay_money);
        mLlRefundMoney = (LinearLayout) findViewById(R.id.ll_pledge_refund_money);
        mBtnPrint = (Button) findViewById(R.id.blue_print);

        mIvRefundStatus = findViewById(R.id.iv_refund_status);
        mTvRefundStatus = findViewById(R.id.tv_refund_status);
        mTvRefundMoneyNotice = findViewById(R.id.tv_refund_money);
        mBtnComplete = (Button) findViewById(R.id.btn_complete);

        mTvTitle.setText(R.string.title_unfreeze);
        mButton.setVisibility(View.INVISIBLE);
        setButtonWhite(mBtnPrint);
        setButtonEnable(mBtnComplete, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mBtnComplete.setOnClickListener(this);
    }

    public void initData() {
        getPrintActive();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mPledgeData = (PledgePayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_PLEDGE_PAY_BEAN);
        if (mPledgeData != null) {
            String money = mPledgeData.getSumPayMoney();
            if (!StringUtils.isEmptyOrNull(money)) {
                mTvPayMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Long.parseLong(money) / 100d));
            }
            long payMoney = getIntent().getLongExtra(Constants.INTENT_PLEDGE_PAY_MONEY, 0);
            long refundMoney = getIntent().getLongExtra(Constants.INTENT_UNFREEZE_MONEY, 0);
            if (payMoney <= 0) {//撤销
                mLlPayMoney.setVisibility(View.GONE);
                mTvRefundMoneyNotice.setText(getString(R.string.tv_pledge_unfreeze_money)
                        + DateUtil.formatMoneyUtil(refundMoney / 100d)
                        + "元原路返回至支付账户中，具体到账时间以个各银行为准");
                mTvRefundStatus.setText(R.string.tv_pledge_reverse_success);
            } else {
                if (refundMoney > 0) {
                    mTvRefundMoneyNotice.setText("消费金额"
                            + DateUtil.formatMoneyUtil(payMoney / 100d)
                            + "元将在下个结算日结算给商户，"
                            + getString(R.string.tv_pledge_unfreeze_money)
                            + DateUtil.formatMoneyUtil(refundMoney / 100d)
                            + "元原路返回至支付账户中，具体到账时间以个各银行为准");
                    mTvRefundStatus.setText(R.string.tv_pledge_refund_success);
                } else {
                    mTvRefundMoneyNotice.setText("消费金额"
                            + DateUtil.formatMoneyUtil(payMoney / 100d)
                            + "元将在下个结算日结算给商户，"
                            + getString(R.string.tv_pledge_unfreeze_money)
                            + DateUtil.formatMoneyUtil(refundMoney / 100d)
                            + "元");
                    mTvRefundStatus.setText(R.string.tv_pledge_refund_success);
                }
            }
            if (refundMoney > 0) {
                mTvRefundMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(refundMoney / 100d));
            } else {
                mLlRefundMoney.setVisibility(View.GONE);
            }

            mTvApplyDate.setText(DateUtil.formatTime(System.currentTimeMillis()));
            if (!StringUtils.isEmptyOrNull(mPledgeData.getOpUserRealName())) {
                mTvConductor.setText(mPledgeData.getOpUserRealName());
            } else {
                mTvConductor.setText(MyApplication.getRealName());
            }

            if (!StringUtils.isEmptyOrNull(mPledgeData.getAuthNo())) {
                mTvPledgeCode.setText(mPledgeData.getAuthNo());
            }
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
            case R.id.btn_complete:
                finish();
                break;
            case R.id.blue_print:
                getPrint();
                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        Intent intent = new Intent(Constants.ACTION_PLEDGE_UNFREEZE_DATA);
        sendBroadcast(intent);
        startActivity(new Intent(PledgeVerifyActivity.this, PledgeActivity.class));
    }

    public void getPrint() {
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        }else */
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        if (mPledgeData != null) {
            mPledgeData.setTermNo(StringUtils.getDeviceInfo(AppHelper.getSN()));
            mPosPrintUtil.pledgePrint(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mPledgeData, false);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderState(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(PledgeVerifyActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(PledgeVerifyActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /*if (keyCode == KeyEvent.KEYCODE_BACK) {
            PledgeVerifyActivity.this.finish();
            Intent intent = new Intent(Constants.ACTION_ORDER_REFUND_DATA);
            sendBroadcast(intent);
        }*/
        return super.onKeyDown(keyCode, event);
    }
}
