package com.hstypay.enterprise.activity.receipt;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.ReceiptFilterPopupWindow;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.calendar.CalendarListPopupWindow;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.adapter.ReceiptAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReceiptDetailBean;
import com.hstypay.enterprise.bean.ReceiptListBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReceiptActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back, mIvButton;
    private TextView tv_title, mTvDate, mTvStore, mTvNull;
    private ImageView iv_filter, mIVStoreArrow;
    private LinearLayout mLlDate;
    private LinearLayout mLlStore;
    private Button btn_create_receipt;
    private ReceiptFilterPopupWindow popupWindow;
    private String storeMerchantId;
    private String storeMerchantName = "全部门店";
    private int[] apiProvider;
    private int[] tradeStatus;
    private CalendarListPopupWindow calendarListPopupWindow;
    private String startTime = ""/*DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00"*/;
    private String endTime = ""/*DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59"*/;
    private RecyclerView mRecyclerView;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private SafeDialog mLoadDialog;
    private List<ReceiptDetailBean> billList;
    private ReceiptAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
        setButtonEnable(btn_create_receipt, true);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getString(R.string.tv_receipt));
        iv_filter = findViewById(R.id.iv_filter);
        mIvButton = findViewById(R.id.iv_button);
        mLlDate = findViewById(R.id.ll_date);
        mTvDate = findViewById(R.id.tv_date);
        mLlStore = findViewById(R.id.ll_store);
        mTvStore = findViewById(R.id.tv_store);
        mIVStoreArrow = findViewById(R.id.iv_store_arrow);
        btn_create_receipt = findViewById(R.id.btn_create_receipt);
        mTvNull = findViewById(R.id.tv_not_data);

        mIvButton.setVisibility(View.VISIBLE);
        mIvButton.setImageResource(R.mipmap.icon_code_question);
      /* View viewLine= findViewById(R.id.view_title_line);
        viewLine.setVisibility(View.GONE);*/
        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getReceiptList("15", "1", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getReceiptList(pageSize + "", currentPage + "", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        iv_filter.setOnClickListener(this);
        mLlDate.setOnClickListener(this);
        mLlStore.setOnClickListener(this);
        mIvButton.setOnClickListener(this);
        btn_create_receipt.setOnClickListener(this);
    }

    private void initData() {
        tradeStatus = new int[]{};
        billList = new ArrayList<>();
        mAdapter = new ReceiptAdapter(MyApplication.getContext(), billList);
        mAdapter.setOnItemClickListener(new ReceiptAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                ReceiptDetailBean dataEntity = billList.get(position);
                if (dataEntity != null) {
                    Intent intent = new Intent(ReceiptActivity.this, ReceiptDetailActivity.class);
                    intent.putExtra(Constants.INTENT_RECEIPT_DETAIL, dataEntity);
                    startActivity(intent);
                    /*if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        Map<String, Object> map = new HashMap<>();

                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }*/
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        initStore();
        getReceiptList("15", "1", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, true);
    }

    private void initStore() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() /*|| MyApplication.getIsManager()*/) {
            storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
            if (TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""))) {
                mTvStore.setText("全部门店");
            } else {
                mTvStore.setText(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""));
            }
        } else if (MyApplication.getIsCasher()) {
            storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvStore.setText("全部门店");
            } else {
                mTvStore.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            mTvStore.setEnabled(false);
            mIVStoreArrow.setVisibility(View.GONE);
        }
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_filter:
                //筛选
                showFilterPop();
                break;
            case R.id.ll_date:
                //全部日期
                showDateSelector();
                break;
            case R.id.ll_store:
                //全部门店
                Intent intentShop = new Intent(this, ShopActivity.class);
                if (TextUtils.isEmpty(storeMerchantId)) {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                } else {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                }
                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BILL_SHOP);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.btn_create_receipt:
                //创建收款单
                startActivity(new Intent(ReceiptActivity.this, ReciptPayActivity.class));
                break;
            case R.id.iv_button:
                startActivity(new Intent(ReceiptActivity.this, InstructionActivity.class)
                        .putExtra(Constants.INTENT_OUT_URL,"file:///android_asset/instruction_receipt.html"));
                break;
        }
    }

    private void showDateSelector() {
        if (calendarListPopupWindow == null) {
            calendarListPopupWindow = new CalendarListPopupWindow(ReceiptActivity.this, 0, new CalendarListPopupWindow.HandleBtn() {
                @Override
                public void handleOkBtn(String start, String end) {
                    startTime = start + " 00:00:00";
                    endTime = end + " 23:59:59";
                    LogUtil.d("aaaaaa=" + start + "--" + end);
                    if (DateUtil.formartYMDToYYMDDot(start).equals(DateUtil.formartYMDToYYMDDot(end))) {
                        mTvDate.setText(DateUtil.formartYMDToYYMDDot(start));
                    } else {
                        mTvDate.setText(DateUtil.formartYMDToYYMDDot(start) + "-" + DateUtil.formartYMDToYYMDDot(end));
                    }
                    if (mTvDate.getText().toString().trim().length() > 20) {
                        mTvStore.setText(StringUtils.omitString(storeMerchantName, 3));
                    } else if (mTvDate.getText().toString().trim().length() >= 10) {
                        mTvStore.setText(StringUtils.omitString(storeMerchantName, 6));
                    } else {
                        mTvStore.setText(storeMerchantName);
                    }
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        billList.clear();
                        currentPage = 2;
                        getReceiptList("15", "1", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            });
        }
        calendarListPopupWindow.showAsDropDown(mLlDate);
    }

    public void getReceiptList(String pageSize, String currentPage, String startTime, String endTime, String storeMerchantId, int[] apiProvider, int[] tradeStatus, boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            map.put("startTime", startTime);
            map.put("endTime", endTime);
            if (!TextUtils.isEmpty(storeMerchantId))
                map.put("storeMerchantId", storeMerchantId);
            if (apiProvider != null && apiProvider.length > 0)
                map.put("apiProviderList", apiProvider);
            if (tradeStatus != null && tradeStatus.length > 0)
                map.put("receiptStatusList", tradeStatus);
            ServerClient.newInstance(MyApplication.getContext()).getReceiptList(MyApplication.getContext(), Constants.TAG_RECEIPT_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_RECEIPT_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReceiptListBean msg = (ReceiptListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ReceiptActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        billList.addAll(msg.getData().getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mAdapter.notifyDataSetChanged();
                        if (isLoadmore) {
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            billList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void showFilterPop() {
        if (popupWindow == null) {
            popupWindow = new ReceiptFilterPopupWindow(ReceiptActivity.this, apiProvider, tradeStatus, new ReceiptFilterPopupWindow.HandleBtn() {
                @Override
                public void handleOkBtn(int[] apiProvider, int[] tradeStatus) {
                    ReceiptActivity.this.apiProvider = apiProvider;
                    ReceiptActivity.this.tradeStatus = tradeStatus;
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        billList.clear();
                        currentPage = 2;
                        getReceiptList("15", "1", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            });
        }
        popupWindow.showAsDropDown(mLlDate);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
//            mTvStore.setText(shopBean.getStoreName());
            storeMerchantName = shopBean.getStoreName();
            storeMerchantId = shopBean.getStoreId();
            if (mTvDate.getText().toString().trim().length() > 20) {
                mTvStore.setText(StringUtils.omitString(storeMerchantName, 3));
            } else if (mTvDate.getText().toString().trim().length() >= 10) {
                mTvStore.setText(StringUtils.omitString(storeMerchantName, 6));
            } else {
                mTvStore.setText(storeMerchantName);
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                billList.clear();
                currentPage = 2;
                getReceiptList("15", "1", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, true);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            billList.clear();
            currentPage = 2;
            getReceiptList("15", "1", startTime, endTime, storeMerchantId, apiProvider, tradeStatus, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }
}
