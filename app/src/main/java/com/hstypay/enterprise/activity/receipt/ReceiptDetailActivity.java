package com.hstypay.enterprise.activity.receipt;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.share.sdk.openapi.APAPIFactory;
import com.alipay.share.sdk.openapi.APMediaMessage;
import com.alipay.share.sdk.openapi.APWebPageObject;
import com.alipay.share.sdk.openapi.IAPApi;
import com.alipay.share.sdk.openapi.SendMessageToZFB;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.dialog.ShareDialog;
import com.hstypay.enterprise.activity.setting.ChangePwdActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.ReceiptBean;
import com.hstypay.enterprise.bean.ReceiptDetailBean;
import com.hstypay.enterprise.bean.ReceiptListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ReceiptDetailActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvReceiptStatus,mIvButton;
    private TextView mTvTitle, mTvTradeMoney, mTvReceiptStatus, mTvPayeeName, mTvReceiptMoney, mTvReceiptExplain, mTvReceiptOrderNo, mTvReceiptCreateTime, mTvReceiptPayWay, mTvReceiptPayTime, mTvReceiptCloseTime;
    private LinearLayout mLlTradeMoney, mLlReceiptStatus,mLlReceiptMoney, mLlReceiptExplain, mLlReceiptPayWay, mLlReceiptPayTime, mLlReceiptCloseTime;
    private Button mBtnSubmit;
    private ReceiptDetailBean mDataEntity;
    private String mIntentName;
    private SafeDialog mLoadDialog;
    private SelectDialog mSelectDialog;
    private ScrollView mSvContent;
    private IAPApi api;
    private ShareDialog shareDialog;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
//            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
//            DialogUtil.safeCloseDialog(dialog);
//            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_TRUE));
//            MyToast.showToastShort("分享成功");
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
//            DialogUtil.safeCloseDialog(dialog);
//            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_FALSE));
//            MyToast.showToastShort("分享失败");
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
//            DialogUtil.safeCloseDialog(dialog);
//            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_FALSE));
//            MyToast.showToastShort("分享取消");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
        setButtonWhite(mBtnSubmit);
        api = APAPIFactory.createZFBApi(getApplicationContext(), Constants.ALIPAY_SHARE_ID, false);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mIvButton = findViewById(R.id.iv_button);
        mTvTitle.setText(getString(R.string.tv_receipt));
        mIvButton.setImageResource(R.mipmap.icon_general_expect);

        mSvContent = findViewById(R.id.sv_content);
        mLlTradeMoney = findViewById(R.id.ll_trade_money);
        mTvTradeMoney = findViewById(R.id.tv_trade_money);
        mLlReceiptStatus = findViewById(R.id.ll_receipt_status);
        mIvReceiptStatus = findViewById(R.id.iv_receipt_status);
        mTvReceiptStatus = findViewById(R.id.tv_receipt_status);

        mTvPayeeName = findViewById(R.id.tv_payee_name);
        mLlReceiptMoney = findViewById(R.id.ll_receipt_money);
        mTvReceiptMoney = findViewById(R.id.tv_receipt_money);
        mLlReceiptExplain = findViewById(R.id.ll_receipt_explain);
        mTvReceiptExplain = findViewById(R.id.tv_receipt_explain);
        mLlReceiptPayWay = findViewById(R.id.ll_receipt_pay_way);
        mTvReceiptPayWay = findViewById(R.id.tv_receipt_pay_way);
        mTvReceiptOrderNo = findViewById(R.id.tv_receipt_order_no);
        mTvReceiptCreateTime = findViewById(R.id.tv_receipt_create_time);
        mLlReceiptPayTime = findViewById(R.id.ll_receipt_pay_time);
        mTvReceiptPayTime = findViewById(R.id.tv_receipt_pay_time);
        mLlReceiptCloseTime = findViewById(R.id.ll_receipt_close_time);
        mTvReceiptCloseTime = findViewById(R.id.tv_receipt_close_time);

        mBtnSubmit = findViewById(R.id.btn_receipt_complete);
        if (AppHelper.getAppType() == 1 || AppHelper.getApkType() != 0) {
            mIvButton.setVisibility(View.GONE);
        } else {
            mIvButton.setVisibility(View.VISIBLE);
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mIvButton.setOnClickListener(this);
    }

    private void initData() {
        mDataEntity = (ReceiptDetailBean) getIntent().getSerializableExtra(Constants.INTENT_RECEIPT_DETAIL);
        getReceiptDetail();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
    }

    private void setView(ReceiptDetailBean dataEntity) {
        mSvContent.setVisibility(View.VISIBLE);
        if (Constants.INTENT_RECEIPT_DETAIL.equals(mIntentName)) {
            mBtnSubmit.setText(getString(R.string.btn_complete));
            mBtnSubmit.setTextColor(UIUtils.getColor(R.color.theme_color));
            mLlTradeMoney.setVisibility(View.VISIBLE);
            mLlReceiptMoney.setVisibility(View.GONE);
            mLlReceiptStatus.setVisibility(View.GONE);
            mIvButton.setVisibility(View.VISIBLE);
        } else {
            if (dataEntity.getReceiptStatus() == 1 || dataEntity.getReceiptStatus() == 2) {
                mBtnSubmit.setText(getString(R.string.btn_close_receipt));
                mBtnSubmit.setVisibility(View.VISIBLE);
                mIvButton.setVisibility(View.VISIBLE);
            } else {
                mBtnSubmit.setVisibility(View.INVISIBLE);
                mBtnSubmit.setText(getString(R.string.btn_close_receipt));
                mIvButton.setVisibility(View.GONE);
            }
            mBtnSubmit.setTextColor(UIUtils.getColor(R.color.home_text));
            mLlTradeMoney.setVisibility(View.GONE);
            mLlReceiptStatus.setVisibility(View.VISIBLE);
        }
        mTvTradeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getTradeMoney() / 100d));
        switch (dataEntity.getReceiptStatus()) {
            case 1:
                mTvReceiptStatus.setText(UIUtils.getString(R.string.tv_receipt_wait_check));
                mIvReceiptStatus.setImageResource(R.mipmap.icon_wait_check);
                mTvReceiptMoney.setTextColor(UIUtils.getColor(R.color.tv_notice_red));
                break;
            case 2:
                mTvReceiptStatus.setText(UIUtils.getString(R.string.tv_receipt_checked));
                mIvReceiptStatus.setImageResource(R.mipmap.icon_checked);
                mTvReceiptMoney.setTextColor(UIUtils.getColor(R.color.tv_notice_red));
                break;
            case 3:
                mTvReceiptStatus.setText(UIUtils.getString(R.string.tv_receipt_closed));
                mIvReceiptStatus.setImageResource(R.mipmap.icon_closed);
                mLlReceiptCloseTime.setVisibility(View.VISIBLE);
                mTvReceiptCloseTime.setText(dataEntity.getUpdateTime());
                mTvPayeeName.setTextColor(UIUtils.getColor(R.color.home_value_text));
                mTvReceiptMoney.setTextColor(UIUtils.getColor(R.color.home_value_text));
                mTvReceiptExplain.setTextColor(UIUtils.getColor(R.color.home_value_text));
                mTvReceiptOrderNo.setTextColor(UIUtils.getColor(R.color.home_value_text));
                mTvReceiptCreateTime.setTextColor(UIUtils.getColor(R.color.home_value_text));
                mTvReceiptCloseTime.setTextColor(UIUtils.getColor(R.color.home_value_text));
                break;
            case 4:
                mTvReceiptStatus.setText(UIUtils.getString(R.string.tv_receipt_success));
                mIvReceiptStatus.setImageResource(R.mipmap.icon_success);
                mLlReceiptPayTime.setVisibility(View.VISIBLE);
                mTvReceiptPayTime.setText(dataEntity.getUpdateTime());
                mLlReceiptPayWay.setVisibility(View.VISIBLE);
                mTvReceiptPayWay.setText(OrderStringUtil.getTradeTypeString(dataEntity.getApiProvider()));
                mTvReceiptMoney.setTextColor(UIUtils.getColor(R.color.tv_notice_red));
                break;
            default:
                mTvReceiptStatus.setText(UIUtils.getString(R.string.tv_receipt_wait_check));
                mIvReceiptStatus.setImageResource(R.mipmap.icon_wait_check);
                break;
        }

        mTvPayeeName.setText(dataEntity.getStoreMerchantName());
        mTvReceiptMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getTradeMoney() / 100d));
        if (!TextUtils.isEmpty(dataEntity.getAttach())) {
            mLlReceiptExplain.setVisibility(View.VISIBLE);
            mTvReceiptExplain.setText(dataEntity.getAttach());
        }
        mTvReceiptOrderNo.setText(dataEntity.getOutTradeNo());
        mTvReceiptCreateTime.setText(dataEntity.getCreateTime());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_back:
                startActivity(new Intent(ReceiptDetailActivity.this, ReceiptActivity.class));
                finish();
                break;
            case R.id.btn_receipt_complete:
                if (Constants.INTENT_RECEIPT_DETAIL.equals(mIntentName)) {
                    startActivity(new Intent(ReceiptDetailActivity.this, ReceiptActivity.class));
                    finish();
                } else {
                    showCloseDialog();
                }
                break;
            case R.id.iv_button:
                shareDialog();
                break;
        }
    }

    private void getReceiptClose() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("tokenId", mDataEntity.getTokenId());
            ServerClient.newInstance(MyApplication.getContext()).getReceiptClose(MyApplication.getContext(), Constants.TAG_RECEIPT_CLOSE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getReceiptDetail() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("tokenId", mDataEntity.getTokenId());
            ServerClient.newInstance(MyApplication.getContext()).getReceiptDetail(MyApplication.getContext(), Constants.TAG_RECEIPT_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_RECEIPT_CLOSE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info bean = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(ReceiptDetailActivity.this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ReceiptDetailActivity.this, bean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getReceiptDetail();
                    break;
            }

        }
        if (event.getTag().equals(Constants.TAG_RECEIPT_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReceiptBean bean = (ReceiptBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(ReceiptDetailActivity.this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ReceiptDetailActivity.this, bean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (bean.getData() != null) {
                        setView(bean.getData());
                    } else {
                        MyToast.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }

        }

    }

    private void showCloseDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(ReceiptDetailActivity.this, getString(R.string.dialog_receipt_close)
                    , getString(R.string.btnCancel), getString(R.string.dialog_btn_close_receipt), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    getReceiptClose();
                }
            });
            DialogHelper.resize(ReceiptDetailActivity.this, mSelectDialog);
        }
        mSelectDialog.show();
    }


    private void shareDialog() {
        if (shareDialog == null) {
            shareDialog = new ShareDialog(ReceiptDetailActivity.this, new ShareDialog.HandleBtn() {
                @Override
                public void handleOkBtn(String s) {
                    if (!TextUtils.isEmpty(s)) {
                        shareDialog.dismiss();
                        if (s.equals("wx")) {
                            //分享到微信
                            if(AppHelper.isWeixinAvilible(MyApplication.getContext())) {
                                shareWeb(SHARE_MEDIA.WEIXIN, mDataEntity);
                            }else {
                                MyToast.showToastShort(UIUtils.getString(R.string.wechat_info));
                            }
                        } else {
                            //分享到支付宝
                            //是否安装支付宝
                            boolean isZFBInstalled = api.isZFBAppInstalled();
                            //设备是否支持分享
                            boolean isZFBSupportApi = api.isZFBSupportAPI();
                            if (isZFBInstalled) {
                                if (isZFBSupportApi) {
                                    aliShare(mDataEntity);
                                } else {
                                    MyToast.showToastShort("该设备不支持支付宝分享");
                                }
                            } else {
                                MyToast.showToastShort(UIUtils.getString(R.string.alipay_info));
                            }
                        }
                    }
                }
            });
            DialogHelper.resizeFull(ReceiptDetailActivity.this, shareDialog);
        }
        shareDialog.show();
    }


    private void shareWeb(SHARE_MEDIA platform, ReceiptDetailBean dataEntity) {
        Drawable drawable = getResources().getDrawable(R.mipmap.pic_home_receipt);
        BitmapDrawable bd = (BitmapDrawable) drawable;
        UMImage thumb = new UMImage(ReceiptDetailActivity.this, drawBg4Bitmap(UIUtils.getColor(R.color.white),bd.getBitmap()));
        UMWeb web = new UMWeb(dataEntity.getPayUrl());
        web.setDescription(dataEntity.getStoreMerchantName()+getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getTradeMoney() / 100d)+"收款单");
        web.setTitle("汇旺财收款单");
        web.setThumb(thumb);
        new ShareAction(ReceiptDetailActivity.this)
                .withMedia(web)
                .setPlatform(platform)
                .setCallback(shareListener).share();
    }

    private void aliShare(ReceiptDetailBean data) {
        //组装文本消息内容对象
       /* APTextObject textObject = new APTextObject();
        textObject.text = "alipay_share";
        //组装分享消息对象
        APMediaMessage mediaMessage = new APMediaMessage();
        mediaMessage.mediaObject = textObject;
        //将分享消息对象包装成请求对象
        SendMessageToZFB.Req req = new SendMessageToZFB.Req();
        req.message = mediaMessage;
        //发送请求
        api.sendReq(req);*/

        APWebPageObject webPageObject = new APWebPageObject();
        webPageObject.webpageUrl = data.getPayUrl();

////初始化APMediaMessage ，组装分享消息对象
        APMediaMessage webMessage = new APMediaMessage();
        webMessage.mediaObject = webPageObject;
        webMessage.title = "汇旺财收款单";
        webMessage.description = data.getStoreMerchantName()+getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(data.getTradeMoney() / 100d)+"收款单";
        Drawable drawable = getResources().getDrawable(R.mipmap.pic_home_receipt);
        BitmapDrawable bd = (BitmapDrawable) drawable;
        webMessage.setThumbImage(drawBg4Bitmap(UIUtils.getColor(R.color.white),bd.getBitmap()));
//网页缩略图的分享支持bitmap和url两种方式，直接通过bitmap传递时bitmap最大为32K
//a）url方式
//webMessage.thumbUrl = "http://www.yoururl.com/thumb.jpg";
//b）Bitmap方式
//webMessage.setThumbImage(bitmap);
//bitmap.recycle();
        //  webMessage.thumbUrl = "http://www.yoururl.com/thumb.jpg";

//将分享消息对象包装成请求对象
        SendMessageToZFB.Req webReq = new SendMessageToZFB.Req();
        webReq.message = webMessage;
        webReq.transaction = "WebShare" + String.valueOf(System.currentTimeMillis());
//修改请求消息对象的scene场景值为ZFBSceneTimeLine
//9.9.5版本之后的支付宝不需要传此参数，用户会在跳转进支付宝后选择分享场景（好友、动态等）
        webReq.scene = SendMessageToZFB.Req.ZFBSceneTimeLine;

//发送请求
        api.sendReq(webReq);
    }

    public static Bitmap drawBg4Bitmap(int color, Bitmap orginBitmap) {
        Paint paint = new Paint();
        paint.setColor(color);
        Bitmap bitmap = Bitmap.createBitmap(orginBitmap.getWidth(),
                orginBitmap.getHeight(), orginBitmap.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(0, 0, orginBitmap.getWidth(), orginBitmap.getHeight(), paint);
        canvas.drawBitmap(orginBitmap, 0, 0, paint);
        return bitmap;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }


    /**
     * 监听返回键
     */
    @Override
    public void onBackPressed() {
        startActivity(new Intent(ReceiptDetailActivity.this, ReceiptActivity.class));
        finish();
    }
}
