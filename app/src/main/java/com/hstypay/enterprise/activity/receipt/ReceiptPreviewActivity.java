package com.hstypay.enterprise.activity.receipt;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.share.sdk.openapi.APAPIFactory;
import com.alipay.share.sdk.openapi.APMediaMessage;
import com.alipay.share.sdk.openapi.APWebPageObject;
import com.alipay.share.sdk.openapi.IAPApi;
import com.alipay.share.sdk.openapi.SendMessageToZFB;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.dialog.ShareDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReceiptBean;
import com.hstypay.enterprise.bean.ReceiptDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ReceiptPreviewActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private Button mBtnSubmit;
    private TextView mTvTItle, mTvReceiptMoney, mTvPayeeName, mTvReceiptExplain;
    private String mReceiptExplain;
    private double doubleMoney;
    private LinearLayout mLlReceiptExplain;
    private String mStoreId;
    private ShareDialog shareDialog;
    private IAPApi api;
    public static ReceiptPreviewActivity instants = null;
    private SafeDialog mLoadDialog;
    private String money;
    private SafeDialog dialog;
    private boolean isShareClicked;
    private boolean isReceiptCreated;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
//            DialogUtil.safeShowDialog(dialog);
//            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_TRUE));
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
//            DialogUtil.safeCloseDialog(dialog);
//            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_TRUE));
//            MyToast.showToastShort("分享成功");
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
//            DialogUtil.safeCloseDialog(dialog);
            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_FALSE));
            MyToast.showToastShort("分享失败");
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
//            DialogUtil.safeCloseDialog(dialog);
//            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_FALSE));
//            MyToast.showToastShort("分享取消");
        }
    };
    private ReceiptDetailBean mData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_preview);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        instants = this;
        initView();
        initData();
        initListener();
        setButtonEnable(mBtnSubmit,true);
        //创建工具对象实例，此处的APPID为上文提到的，申请应用生效后，在应用详情页中可以查到的支付宝应用唯一标识
        api = APAPIFactory.createZFBApi(getApplicationContext(), Constants.ALIPAY_SHARE_ID, false);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTItle = findViewById(R.id.tv_title);
        mTvTItle.setText(getString(R.string.tv_receipt));
        mBtnSubmit = findViewById(R.id.btn_send_receipt);
        mTvReceiptMoney = findViewById(R.id.tv_receipt_money);//收款金额
        mTvPayeeName = findViewById(R.id.tv_payee_name);//收款方
        mLlReceiptExplain = findViewById(R.id.ll_receipt_explain);//收款说明
        mTvReceiptExplain = findViewById(R.id.tv_receipt_explain);//收款说明
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    private void initData() {
        mReceiptExplain = getIntent().getStringExtra(Constants.INTENT_PAY_REMARK);//收款说明
        doubleMoney = getIntent().getDoubleExtra(Constants.INTENT_PAY_MONEY, 0);  //金额
        money = String.valueOf(BigDecimal.valueOf(doubleMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
        String receipt_name = getIntent().getStringExtra(Constants.RECEIPT_STORE_NAME);//门店名
        mStoreId = getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID); //门店Id

        if (!TextUtils.isEmpty(receipt_name)) {
            mTvPayeeName.setText(receipt_name);
        }
        if (doubleMoney > 0) {
            mTvReceiptMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(doubleMoney));
        }
        if (!TextUtils.isEmpty(mReceiptExplain)) {
            mTvReceiptExplain.setText(mReceiptExplain);
            mLlReceiptExplain.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_send_receipt:
                //发送收款单
                if (!isReceiptCreated) {
                    getReceiptCreate();
                } else {
                    if (mData != null) {
                        shareDialog();
                    } else {
                        showCommonNoticeDialog(ReceiptPreviewActivity.this, "数据有误，请回到上一页重新发起", new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                finish();
                            }
                        });
                    }
                }
                break;
        }
    }

    private void getReceiptCreate() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("tradeMoney", money);
            map.put("storeMerchantId", mStoreId);
            if (!TextUtils.isEmpty(mReceiptExplain))
                map.put("attach", mReceiptExplain);
            ServerClient.newInstance(MyApplication.getContext()).getReceiptCreate(MyApplication.getContext(), Constants.TAG_RECEIPT_CREATE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_RECEIPT_CREATE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReceiptBean bean = (ReceiptBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(ReceiptPreviewActivity.this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ReceiptPreviewActivity.this, bean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    isReceiptCreated = true;
                    if (bean != null && bean.getData() != null) {
                        mData = bean.getData();
                        shareDialog();
                    }
                    break;
            }
        }
    }

    private void shareDialog() {
        if (shareDialog == null) {
            shareDialog = new ShareDialog(ReceiptPreviewActivity.this, new ShareDialog.HandleBtn() {
                @Override
                public void handleOkBtn(String s) {
                    if (!TextUtils.isEmpty(s)) {
                        shareDialog.dismiss();
                        if (s.equals("wx")) {
                            //分享到微信
                            if(AppHelper.isWeixinAvilible(MyApplication.getContext())) {
                                isShareClicked = true;
                                shareWeb(SHARE_MEDIA.WEIXIN, mData);
                            }else {
                                MyToast.showToastShort(UIUtils.getString(R.string.wechat_info));
                            }
                        } else {
                            //分享到支付宝
                            //是否安装支付宝
                            boolean isZFBInstalled = api.isZFBAppInstalled();
                            //设备是否支持分享
                            boolean isZFBSupportApi = api.isZFBSupportAPI();
                            if (isZFBInstalled) {
                                if (isZFBSupportApi) {
                                    isShareClicked = true;
                                    aliShare(mData);
                                } else {
                                    MyToast.showToastShort("该设备不支持支付宝分享");
                                }
                            } else {
                                MyToast.showToastShort(UIUtils.getString(R.string.alipay_info));
                            }
                        }
                    }
                }
            });
            DialogHelper.resizeFull(ReceiptPreviewActivity.this, shareDialog);
        }
        shareDialog.show();
    }


    private void shareWeb(SHARE_MEDIA platform, ReceiptDetailBean dataEntity) {
        Drawable drawable = getResources().getDrawable(R.mipmap.pic_home_receipt);
        BitmapDrawable bd = (BitmapDrawable) drawable;
        UMImage thumb = new UMImage(ReceiptPreviewActivity.this, drawBg4Bitmap(UIUtils.getColor(R.color.white),bd.getBitmap()));
        UMWeb web = new UMWeb(dataEntity.getPayUrl());
        web.setDescription(dataEntity.getStoreMerchantName()+getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(doubleMoney)+"收款单");
        web.setTitle("汇旺财收款单");
        web.setThumb(thumb);
        new ShareAction(ReceiptPreviewActivity.this)
                .withMedia(web)
                .setPlatform(platform)
                .setCallback(shareListener).share();
    }

    private void aliShare(ReceiptDetailBean data) {
        //组装文本消息内容对象
       /* APTextObject textObject = new APTextObject();
        textObject.text = "alipay_share";
        //组装分享消息对象
        APMediaMessage mediaMessage = new APMediaMessage();
        mediaMessage.mediaObject = textObject;
        //将分享消息对象包装成请求对象
        SendMessageToZFB.Req req = new SendMessageToZFB.Req();
        req.message = mediaMessage;
        //发送请求
        api.sendReq(req);*/

        APWebPageObject webPageObject = new APWebPageObject();
        webPageObject.webpageUrl = data.getPayUrl();

////初始化APMediaMessage ，组装分享消息对象
        APMediaMessage webMessage = new APMediaMessage();
        webMessage.mediaObject = webPageObject;
        webMessage.title = "汇旺财收款单";
        webMessage.description = data.getStoreMerchantName()+getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(doubleMoney)+"收款单";
        Drawable drawable = getResources().getDrawable(R.mipmap.pic_home_receipt);
        BitmapDrawable bd = (BitmapDrawable) drawable;
        webMessage.setThumbImage(drawBg4Bitmap(UIUtils.getColor(R.color.white),bd.getBitmap()));
//网页缩略图的分享支持bitmap和url两种方式，直接通过bitmap传递时bitmap最大为32K
//a）url方式
//webMessage.thumbUrl = "http://www.yoururl.com/thumb.jpg";
//b）Bitmap方式
//webMessage.setThumbImage(bitmap);
//bitmap.recycle();
        //  webMessage.thumbUrl = "http://www.yoururl.com/thumb.jpg";

//将分享消息对象包装成请求对象
        SendMessageToZFB.Req webReq = new SendMessageToZFB.Req();
        webReq.message = webMessage;
        webReq.transaction = "WebShare" + String.valueOf(System.currentTimeMillis());
//修改请求消息对象的scene场景值为ZFBSceneTimeLine
//9.9.5版本之后的支付宝不需要传此参数，用户会在跳转进支付宝后选择分享场景（好友、动态等）
        webReq.scene = SendMessageToZFB.Req.ZFBSceneTimeLine;

//发送请求
        EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_TRUE));
        api.sendReq(webReq);
    }

    public static Bitmap drawBg4Bitmap(int color, Bitmap orginBitmap) {
        Paint paint = new Paint();
        paint.setColor(color);
        Bitmap bitmap = Bitmap.createBitmap(orginBitmap.getWidth(),
                orginBitmap.getHeight(), orginBitmap.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(0, 0, orginBitmap.getWidth(), orginBitmap.getHeight(), paint);
        canvas.drawBitmap(orginBitmap, 0, 0, paint);
        return bitmap;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        DialogUtil.safeCloseDialog(dialog);
        if (isShareClicked) {
            Intent intent = new Intent(ReceiptPreviewActivity.this, ReceiptDetailActivity.class);
            intent.putExtra(Constants.INTENT_RECEIPT_DETAIL, mData);
            intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_RECEIPT_DETAIL);
            startActivity(intent);
        }
        isShareClicked = false;
    }
/*
    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent(ReceiptPreviewActivity.this, ReceiptDetailActivity.class);
        intent.putExtra(Constants.INTENT_RECEIPT_DETAIL, mData);
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_RECEIPT_DETAIL);
        startActivity(intent);
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }
}
