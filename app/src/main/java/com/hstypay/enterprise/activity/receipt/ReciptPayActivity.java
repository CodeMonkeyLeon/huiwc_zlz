package com.hstypay.enterprise.activity.receipt;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.dialog.DialogAttachInfo;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ReciptPayActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private TextView tv_title;
    private TextView tv_add_explain, tv_explain_data;
    private LinearLayout ll_choice_store;
    private TextView tv_receipt_store_name, tv_money;
    private LinearLayout num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, num_0, lay_delete, lay_clear, lay_point, lay_add, ly_to_scan_pay;
    private Context context;
    private StringBuilder calculateBuilder;
    private boolean enable;
    private double totalMoney;
    private String mStoreId = "";
    private String mStoreName = "";
    private String mAttach = "";

    public static ReciptPayActivity instants = null;
    private DialogAttachInfo mAttachInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //StatusBarUtil.setImmersiveStatusBar(this,true);
        setContentView(R.layout.activity_receipt_pay);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        instants = this;
        context = this;
        initView();
        initData();
        initListener();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getString(R.string.tv_receipt));
        tv_add_explain = findViewById(R.id.tv_add_explain);//添加收款说明
        tv_explain_data = findViewById(R.id.tv_explain_data);//添加收款数据
        ll_choice_store = findViewById(R.id.ll_choice_store);//选择门店
        tv_receipt_store_name = findViewById(R.id.tv_receipt_store_name);//门店名
        tv_money = findViewById(R.id.tv_money);//金额
        num_1 = findViewById(R.id.num_1);//1
        num_2 = findViewById(R.id.num_2);//2
        num_3 = findViewById(R.id.num_3);//3
        lay_delete = findViewById(R.id.lay_delete);//删除
        num_4 = findViewById(R.id.num_4);//4
        num_5 = findViewById(R.id.num_5);//5
        num_6 = findViewById(R.id.num_6);//6
        lay_clear = findViewById(R.id.lay_clear);//清空
        num_7 = findViewById(R.id.num_7);//7
        num_8 = findViewById(R.id.num_8);//8
        num_9 = findViewById(R.id.num_9);//9
        num_0 = findViewById(R.id.num_0);//0
        lay_point = findViewById(R.id.lay_point);//点
        lay_add = findViewById(R.id.lay_add);//加
        ly_to_scan_pay = findViewById(R.id.ly_to_scan_pay);//预览
        enable = false;
        setPayButton(enable);
    }

    private void initListener() {

        iv_back.setOnClickListener(this);
        tv_add_explain.setOnClickListener(this);
        if (!MyApplication.getIsCasher()) {
            ll_choice_store.setOnClickListener(this);
        }
        num_0.setOnClickListener(this);
        num_1.setOnClickListener(this);
        num_2.setOnClickListener(this);
        num_3.setOnClickListener(this);
        num_4.setOnClickListener(this);
        num_5.setOnClickListener(this);
        num_6.setOnClickListener(this);
        num_7.setOnClickListener(this);
        num_8.setOnClickListener(this);
        num_9.setOnClickListener(this);
        lay_point.setOnClickListener(this);
        lay_add.setOnClickListener(this);
        lay_clear.setOnClickListener(this);
        lay_delete.setOnClickListener(this);
        ly_to_scan_pay.setOnClickListener(this);
    }

    private void initData() {
        calculateBuilder = new StringBuilder();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
        mStoreName = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME);
        if (!TextUtils.isEmpty(this.mStoreName)) {
            tv_receipt_store_name.setText(this.mStoreName);
        }
        if (TextUtils.isEmpty(mStoreId)) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(ReciptPayActivity.this).storePort(ReciptPayActivity.this, Constants.STORE_PORT_TAG, null);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_explain:
                //添加收款说明
                explain();
                break;
            case R.id.ll_choice_store:
                //选择门店
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.lay_clear:
                //清空
                AppHelper.execVibrator(context);
                //  tv_money.setText("");
                totalMoney = 0;
                calculateBuilder.delete(0, calculateBuilder.length());
                tv_money.setText(getString(R.string.tv_money_zero));
                tv_money.setTextColor(UIUtils.getColor(R.color.line_color));
                enable = false;
                setPayButton(enable);
                break;
            case R.id.num_0:
                AppHelper.execVibrator(context);
                calculateProcess(0);
                break;
            case R.id.num_1:
                AppHelper.execVibrator(context);
                calculateProcess(1);
                break;
            case R.id.num_2:
                AppHelper.execVibrator(context);
                calculateProcess(2);
                break;
            case R.id.num_3:
                AppHelper.execVibrator(context);
                calculateProcess(3);
                break;
            case R.id.num_4:
                AppHelper.execVibrator(context);
                calculateProcess(4);
                break;
            case R.id.num_5:
                AppHelper.execVibrator(context);
                calculateProcess(5);
                break;
            case R.id.num_6:
                AppHelper.execVibrator(context);
                calculateProcess(6);
                break;
            case R.id.num_7:
                AppHelper.execVibrator(context);
                calculateProcess(7);
                break;
            case R.id.num_8:
                AppHelper.execVibrator(context);
                calculateProcess(8);
                break;
            case R.id.num_9:
                AppHelper.execVibrator(context);
                calculateProcess(9);
                break;
            case R.id.lay_point:
                //点
                AppHelper.execVibrator(context);
                calculateProcess(".");
                break;
            case R.id.lay_add:
                //加
                AppHelper.execVibrator(context);
                calculateProcess("+");
                break;
            case R.id.ly_to_scan_pay:
                if (TextUtils.isEmpty(tv_receipt_store_name.getText().toString().trim())) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                    return;
                }
                if (TextUtils.isEmpty(mStoreId)) {
                    MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
                    return;
                }
                Intent intent = new Intent(context, ReceiptPreviewActivity.class);
                intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
                intent.putExtra(Constants.RECEIPT_STORE_NAME, mStoreName);
                intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
                intent.putExtra(Constants.INTENT_PAY_REMARK, mAttach);
                startActivity(intent);
                //预览
                break;
            case R.id.lay_delete:
                //删除
                AppHelper.execVibrator(context);
                if (calculateBuilder.length() > 0) {
                    calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                }
                calculateResult();
                break;
        }
    }

    private void explain() {
        if (mAttachInfo == null) {
            mAttachInfo = new DialogAttachInfo(ReciptPayActivity.this, new DialogAttachInfo.HandleBtn() {
                @Override
                public void handleOkBtn(String s) {
                    mAttach = s;
                    if (s.length() > 18) {
                        tv_explain_data.setText(s.substring(0, 17) + "...");
                    } else {
                        tv_explain_data.setText(s);
                    }
                    if (s.length() == 0){
                        tv_add_explain.setText(getString(R.string.btn_add_receipt_explain));
                    }else {
                        tv_add_explain.setText(getString(R.string.btn_active_modify));
                    }
                    tv_explain_data.setVisibility(View.VISIBLE);
                }

                @Override
                public void handleCancleBtn() {

                }
            });
            DialogHelper.resize(ReciptPayActivity.this, mAttachInfo);
        }
        mAttachInfo.setContent(mAttach);
        mAttachInfo.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mAttachInfo.showKeyboard();
            }
        }, 200);

    }


    private void calculateProcess(Object object) {
        if (object instanceof Integer) {
            if ("0".equals(object.toString()) && (calculateBuilder.toString().equals("0.0"))) {
                return;
            }
            if ((calculateBuilder.toString().lastIndexOf("0") == 0 && calculateBuilder.length() == 1) ||
                    (calculateBuilder.toString().lastIndexOf("0") == calculateBuilder.length() - 1
                            && calculateBuilder.toString().lastIndexOf("+") == calculateBuilder.length() - 2)) {
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            }
            if (calculateBuilder.toString().contains(".")) {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (calculateBuilder.length() >= 3)
                    if (pointIndex == calculateBuilder.length() - 3 &&
                            calculateBuilder.toString().lastIndexOf("+") != calculateBuilder.length() - 1) {
                        return;
                    }
            }
        }
        if (object instanceof String) {
            if (TextUtils.isEmpty(calculateBuilder)) {
                if (object.equals("+")) {
                    return;
                }
                if (object.equals(".")) {
                    calculateBuilder.append("0");
                }
            } else {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (pointIndex == calculateBuilder.length() - 1) {
                    if (object.equals("+")) {
                        calculateBuilder.append("0");
                    }
                }
                if (!calculateBuilder.toString().contains("+")) {
                    if (calculateBuilder.toString().contains(".") && object.equals(".")) {
                        return;
                    }
                } else {
                    int addIndex = calculateBuilder.toString().lastIndexOf("+");
                    if (addIndex == calculateBuilder.length() - 1) {
                        if (object.equals("+")) {
                            return;
                        }
                        if (object.equals(".")) {
                            calculateBuilder.append("0");
                        }
                    } else {
                        if (pointIndex > addIndex && object.equals(".")) {
                            return;
                        }
                    }
                }
            }
        }
        calculateBuilder.append(object);
        calculateResult();
    }


    private void calculateResult() {
        if (TextUtils.isEmpty(calculateBuilder)) {

            tv_money.setText(getString(R.string.tv_money_zero));
            enable = false;
            setPayButton(enable);
        } else {
            BigDecimal bigSum = null;
            if (calculateBuilder.toString().contains("+")) {
                String[] numbers = calculateBuilder.toString().split("\\+");
                if (numbers.length == 1) {
                    LogUtil.d("number1==", numbers[0]);
                    bigSum = new BigDecimal(numbers[0]);
                } else {
                    bigSum = new BigDecimal(0);
                    if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                        numbers[numbers.length - 1] = numbers[numbers.length - 1] + "0";
                    }
                    for (int i = 0; i < numbers.length; i++) {
                        LogUtil.d("number2==", numbers[i] + "--" + i);
                        BigDecimal big = new BigDecimal(numbers[i]);
                        bigSum = bigSum.add(big);
                        LogUtil.d("number3==", bigSum.doubleValue() + "");
                    }
                }
            } else {
                String calculateString = calculateBuilder.toString();
                if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                    bigSum = new BigDecimal(calculateString + "0");
                } else {
                    bigSum = new BigDecimal(calculateString);
                }
            }
            if (bigSum.doubleValue() > 300000) {
                MyToast.showToast(UIUtils.getString(R.string.tv_pay_exceed_money), Toast.LENGTH_SHORT);
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                return;
            }
            if (bigSum.doubleValue() <= 0) {
                enable = false;
            } else {
                enable = true;
            }
            setPayButton(enable);
            totalMoney = bigSum.doubleValue();
            tv_money.setText(DateUtil.formatPaseMoneyUtil(bigSum));
        }
        tv_money.setTextColor(UIUtils.getColor(enable ? R.color.tv_pay_total_money : R.color.line_color));
        // tv_calculate.setTextColor(TextUtils.isEmpty(calculateBuilder) ? UIUtils.getColor(R.color.line_color) : UIUtils.getColor(R.color.tv_pay_total_money));
    }

    private void setPayButton(boolean enable) {
        if (enable) {
            Drawable rectangleDefault = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            Drawable rectanglePressed = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectanglePressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
            ly_to_scan_pay.setBackground(stateListDrawable);
            ly_to_scan_pay.setEnabled(true);
        } else {
            Drawable rectangleUnable = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectangleUnable.setAlpha(102);
            ly_to_scan_pay.setBackground(rectangleUnable);
            ly_to_scan_pay.setEnabled(false);
        }

    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.STORE_PORT_TAG)) {
            StoreBean bean = (StoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(ReciptPayActivity.this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ReciptPayActivity.this, bean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (bean.getData().getStoreName() != null) {
                                String storeName = bean.getData().getStoreName();
                                if (!TextUtils.isEmpty(storeName)) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                                    tv_receipt_store_name.setText(storeName);
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, "");

                                }
                            }
                            if (!TextUtils.isEmpty(bean.getData().getStoreId())) {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, bean.getData().getStoreId());
                                mStoreId = bean.getData().getStoreId();
                                getStoreVoice(mStoreId);
                            } else {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, true);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                                mStoreId = "";
                            }
                        }
                    }
                    break;
            }
            dismissLoading();
        }


        if (event.getTag().equals(Constants.GETUI_PUSH_VOICE_LIST_TAG)) {
            VoiceBean msg = (VoiceBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ReciptPayActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (msg.getData().get(0).getPushClose() == 1) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, true);
                        } else {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, false);
                        }
                        LogUtil.d("SP_STORE_VOICE---" + MyApplication.isOpenVoice());
                    }
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mStoreId = shopBean.getStoreId();
            mStoreName = shopBean.getStoreName();
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, mStoreId);
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, mStoreName);
            if (mStoreName.length() > 10) {
                tv_receipt_store_name.setText(mStoreName.substring(0, 9) + "...");
            } else {
                tv_receipt_store_name.setText(mStoreName);
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeName", mStoreName);
                map.put("storeId", mStoreId);
                ServerClient.newInstance(MyApplication.getContext()).postStore(MyApplication.getContext(), Constants.TAG_PUT_STORE, map);
                getStoreVoice(mStoreId);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    private void getStoreVoice(String storeId) {
        Map<String, Object> voiceMap = new HashMap<>();
        voiceMap.put("storeMerchantId", storeId);
        String clientid = PushManager.getInstance().getClientid(this.getApplicationContext());
        if (!StringUtils.isEmptyOrNull(clientid)) {
            voiceMap.put("pushDeviceId", clientid);
        }
        voiceMap.put("client", Constants.REQUEST_CLIENT_APP);
        ServerClient.newInstance(MyApplication.getContext()).getuiPushVoiceList(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_LIST_TAG, voiceMap);
    }
}
