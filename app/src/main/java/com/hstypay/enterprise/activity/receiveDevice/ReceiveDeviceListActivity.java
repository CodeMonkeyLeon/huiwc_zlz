package com.hstypay.enterprise.activity.receiveDevice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.BindingCodeActivity;
import com.hstypay.enterprise.adapter.ReceiveDeviceAdapter;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreDevicesBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hstypay.enterprise.R.id.et_input;

public class ReceiveDeviceListActivity extends BaseActivity implements View.OnClickListener, ShopRecyclerAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack, mIvArrow;
    private Button mBtnSubmit;
    private TextView mButton, mTvStoreName, mTvReceiveDeviceCart, mTvNull;
    private ReceiveDeviceAdapter mAdapter;
    private List<StoreListBean.DataEntity> mList;
    private List<StoreListBean.DataEntity> mOriginList;
    private List<StoreDevicesBean.DataEntity> mDeviceList;
    private RecyclerView mRvDevices, mStoreRecyclerView;
    private CustomLinearLayoutManager mLinearLayoutManager, mStoreLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mLlDeviceScan, mPopLayout, mLlDeviceList, mLlChoiceStore;
    private EditTextDelete mEtInput, mEtStoreInput;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private int pageSize = 15;
    private int currentPage = 2;
    private boolean isNotFirstRequest;
    private boolean mSwitchArrow;
    private Animation rotate;
    private ShopRecyclerAdapter mStoreAdapter;
    private String mStoreId;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_device_list);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);

        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mLlDeviceScan = (LinearLayout) findViewById(R.id.ll_devices_scan);
        mLlDeviceList = (LinearLayout) findViewById(R.id.ll_device_list);
        mLlChoiceStore = (LinearLayout) findViewById(R.id.ll_choice_store);
        mTvReceiveDeviceCart = (TextView) findViewById(R.id.tv_receive_device_cart);
        mTvStoreName = findViewById(R.id.tv_store_name);
        mIvArrow = (ImageView) findViewById(R.id.iv_arrow);
        mEtInput = (EditTextDelete) findViewById(et_input);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);

        if (MyApplication.getIsCasher()) {
            mIvArrow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(MyApplication.getDefaultStoreName())) {
                if (MyApplication.getDefaultStoreName().length() > 10) {
                    mTvStoreName.setText(MyApplication.getDefaultStoreName().substring(0, 9) + "...");
                } else {
                    mTvStoreName.setText(MyApplication.getDefaultStoreName());
                }
            }
        } else {
            mTvStoreName.setText(getString(R.string.title_all_store));
//            mTvStoreName.setText(getString(R.string.tv_receive_device_all_store));
        }
        SpannableStringBuilder spannableString = new SpannableStringBuilder();
        spannableString.append(getString(R.string.tv_device_service) + getString(R.string.tv_receive_device_cart));
        /*AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(UIUtils.dp2px(17));
        spannableString.setSpan(absoluteSizeSpan, 10, 23, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);*/
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if (AppHelper.getAppType() == 2) {
                    AppHelper.call(ReceiveDeviceListActivity.this, getString(R.string.contact_service_telephone));
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                /*ds.setColor(ContextCompat.getColor(MyApplication.getContext(), R.color.theme_color));            //设置可以点击文本部分的颜色
                ds.setUnderlineText(false);            //设置该文本部分是否显示超链接形式的下划线
                ds.clearShadowLayer();*/
            }
        };
        spannableString.setSpan(clickableSpan, 10, 23, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        mTvReceiveDeviceCart.setText(spannableString);
        mTvReceiveDeviceCart.setMovementMethod(LinkMovementMethod.getInstance());
        avoidHintColor(mTvReceiveDeviceCart);

        mButton.setText(getString(R.string.btn_bind_code_ensure));
        setButtonEnable(mBtnSubmit, true);

        initRecyclerView();
        initSwipeRefreshLayout();
        mPopLayout = (LinearLayout) findViewById(R.id.pop_layout);
        mEtStoreInput = (EditTextDelete) findViewById(R.id.et_store_input);
        mStoreRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_store);
        mStoreLinearLayoutManager = new CustomLinearLayoutManager(this);
        mStoreRecyclerView.setLayoutManager(mStoreLinearLayoutManager);

        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        if (!MyApplication.getIsCasher()) {
            mLlChoiceStore.setOnClickListener(this);
        }
        mBtnSubmit.setOnClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    MtaUtils.mtaId(ReceiveDeviceListActivity.this, "R003");
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mDeviceList.clear();
                        currentPage = 2;
                        receiveDeviceList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mDeviceList.clear();
                        receiveDeviceList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
    }

    public void initData() {
        mList = new ArrayList<>();
        mOriginList = new ArrayList<>();
        mDeviceList = new ArrayList<>();
        mStoreId = "";

        mAdapter = new ReceiveDeviceAdapter(ReceiveDeviceListActivity.this, mDeviceList);
        mRvDevices.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ReceiveDeviceAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(ReceiveDeviceListActivity.this, ReceiveDeviceSetActivity.class);
                intent.putExtra(Constants.INTENT_RECEIVE_DEVICE_INFO, mDeviceList.get(position));
                startActivity(intent);
            }
        });
        if (!NetworkUtils.isNetworkAvailable(ReceiveDeviceListActivity.this)) {
            showCommonNoticeDialog(ReceiveDeviceListActivity.this, getString(R.string.network_exception));
        } else {
            mDeviceList.clear();
            receiveDeviceList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
        }

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
    }

    private void initRecyclerView() {
        mRvDevices = (RecyclerView) findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvDevices.setLayoutManager(mLinearLayoutManager);
        mRvDevices.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                //触发刷新数据任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    currentPage = 1;
                    receiveDeviceList(pageSize, currentPage, mEtInput.getText().toString().trim(), mStoreId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                //触发加载更多任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    receiveDeviceList(pageSize, currentPage, mEtInput.getText().toString().trim(), mStoreId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    private void receiveDeviceList(int pageSize, int currentPage, String search, String storeMerchantId, boolean showLoading) {
        Map<String, Object> map = new HashMap<>();
        if (showLoading) {
            DialogUtil.safeShowDialog(mLoadDialog);
        }
        if (!TextUtils.isEmpty(search)) {
            map.put("search", search);
        }
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeMerchantId", storeMerchantId);
        }
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("purposeCode", "0");
        ServerClient.newInstance(MyApplication.getContext()).storeDeviceList(MyApplication.getContext(), Constants.TAG_DEVICE_LIST, map);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                MtaUtils.mtaId(ReceiveDeviceListActivity.this, "R002");
                Intent intentScan = new Intent(ReceiveDeviceListActivity.this, BindingCodeActivity.class);
                intentScan.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_DEVICE_CAPTURE);
                intentScan.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivity(intentScan);
                break;
            case R.id.button_title:
                MtaUtils.mtaId(ReceiveDeviceListActivity.this, "R002");
                Intent intentButton = new Intent(ReceiveDeviceListActivity.this, BindingCodeActivity.class);
                intentButton.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_DEVICE_CAPTURE);
                intentButton.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivity(intentButton);
                break;
            case R.id.ll_choice_store:
                if (mSwitchArrow) {
                    closeArrow();
                } else {
                    loadData();
                }
                break;
            default:
                break;
        }
    }

    //Eventbus接收数据，语言播报列表
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_DEVICE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreDevicesBean msg = (StoreDevicesBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ReceiveDeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mLlDeviceList.setVisibility(View.VISIBLE);
                        mButton.setVisibility(View.VISIBLE);
                        mLlDeviceScan.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mDeviceList.addAll(msg.getData());
                        if (mDeviceList.size() == 0) {
                            if (!isNotFirstRequest) {
                                mLlDeviceList.setVisibility(View.GONE);
                                mLlDeviceScan.setVisibility(View.VISIBLE);
                                mButton.setVisibility(View.INVISIBLE);
                            } else {
                                if (isLoadmore) {
                                    mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                                    mTvNull.setVisibility(View.GONE);
                                    MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                                } else {
                                    mSwipeRefreshLayout.setVisibility(View.GONE);
                                    mTvNull.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    } else {
                        if (!isNotFirstRequest) {
                            mLlDeviceList.setVisibility(View.GONE);
                            mLlDeviceScan.setVisibility(View.VISIBLE);
                            mButton.setVisibility(View.INVISIBLE);
                        } else {
                            if (isLoadmore) {
                                mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                                mTvNull.setVisibility(View.GONE);
                                MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                            } else {
                                mSwipeRefreshLayout.setVisibility(View.GONE);
                                mTvNull.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    break;
            }
            mAdapter.notifyDataSetChanged();
            isLoadmore = false;
            isRefreshed = false;
            isNotFirstRequest = true;
        }
        if (event.getTag().equals(Constants.TAG_DEVICE_BIND)) {
            dismissLoading();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (!NetworkUtils.isNetworkAvailable(ReceiveDeviceListActivity.this)) {
                        return;
                    } else {
                        mDeviceList.clear();
                        receiveDeviceList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_RECEIVE_DEVICE_CHOICE_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ReceiveDeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ReceiveDeviceListActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        dataEntity.setStoreName("全部门店");
                        dataEntity.setStoreId("");
                        mOriginList.add(dataEntity);
                        mOriginList.addAll(msg.getData());
                        mList.addAll(mOriginList);
                        mStoreAdapter = new ShopRecyclerAdapter(ReceiveDeviceListActivity.this, mList, mStoreId);
                        mStoreAdapter.setOnItemClickListener(this);
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        openArrow();
                    } else {
                        showCommonNoticeDialog(ReceiveDeviceListActivity.this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mDeviceList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void avoidHintColor(View view) {
        if (view instanceof TextView)
            ((TextView) view).setHighlightColor(getResources().getColor(android.R.color.transparent));
    }

    public void closeArrow() {
        mEtStoreInput.setText("");
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        mSwitchArrow = false;
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mIvArrow.startAnimation(rotate);
        mPopLayout.setVisibility(View.GONE);
    }

    private void openArrow() {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        mSwitchArrow = true;
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mIvArrow.startAnimation(rotate);
        mPopLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIvArrow.clearAnimation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_DEVICE_CAPTURE && resultCode == RESULT_OK) {
            mDeviceList.clear();
            currentPage = 1;
            if (!NetworkUtils.isNetworkAvailable(ReceiveDeviceListActivity.this)) {
                showCommonNoticeDialog(ReceiveDeviceListActivity.this, getString(R.string.network_exception));
            } else {
                receiveDeviceList(pageSize, currentPage, mEtInput.getText().toString().trim(), mStoreId, true);
            }
        }
    }

    private void loadData() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginList.clear();
            mList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            map.put("merchantDataType", "1");
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_RECEIVE_DEVICE_CHOICE_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void setData(String search) {
        mList.clear();
        if (TextUtils.isEmpty(search)) {
            mList.addAll(mOriginList);
        } else {
            for (int i = 0; i < mOriginList.size(); i++) {
                if (mOriginList.get(i).getStoreName().contains(search)) {
                    mList.add(mOriginList.get(i));
                }
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        mStoreId = mList.get(position).getStoreId();
        if (TextUtils.isEmpty(mStoreId)) {
            mTvStoreName.setText(getString(R.string.title_all_store));
//            mTvStoreName.setText(getString(R.string.tv_receive_device_all_store));
        } else {
            String title = mList.get(position).getStoreName();
            if (title.length() > 10) {
                mTvStoreName.setText(title.substring(0, 9) + "...");
            } else {
                mTvStoreName.setText(title);
            }
        }
        closeArrow();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mDeviceList.clear();
            receiveDeviceList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Constants.DEVICE_LIST_REFRESH_TAG){

            Constants.DEVICE_LIST_REFRESH_TAG=false;
            if (!NetworkUtils.isNetworkAvailable(ReceiveDeviceListActivity.this)) {
                showCommonNoticeDialog(ReceiveDeviceListActivity.this, getString(R.string.network_exception));
            } else {
                mEtInput.setText("");
                currentPage=2;
                mDeviceList.clear();
                receiveDeviceList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, true);
            }

        }

    }
}
