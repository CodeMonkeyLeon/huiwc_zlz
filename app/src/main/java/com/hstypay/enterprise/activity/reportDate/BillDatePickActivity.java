package com.hstypay.enterprise.activity.reportDate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.ReportData.MonthAdapter;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.DateEntity;
import com.hstypay.enterprise.bean.MonthEntity;
import com.hstypay.enterprise.bean.ReportSelectDayBean;
import com.hstypay.enterprise.bean.SelectDateBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author MiTa
 * @date 2017/12/21.
 */
public class BillDatePickActivity extends BaseActivity implements MonthAdapter.OnMonthChildClickListener {

    private final int CALENDAR_TODAY = 77;
    public final static String KEY_HIDE_MONEY = "key_hide_money";
    private RecyclerView mRvCalendar;
    private TextView mTvTitle, mTvHintData;
    private ImageView mIvBack;
    private MonthAdapter adapter;
    private List<MonthEntity> monthList = new ArrayList<>();

    private int year, month, day;
    private int nowDay;
    private int lastDateSelect = -1, lastMonthSelect = -1;
    private ReportSelectDayBean mSelectDayBean;
    private SafeDialog mLoadDialog;
    private boolean hideMoney;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        getViews();
        initListener();
        initData();
//        initCalendarRv();
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mSelectDayBean = (ReportSelectDayBean) getIntent().getSerializableExtra(Constants.INTENT_REPORT_DAY_DATA);
        lastMonthSelect = getIntent().getIntExtra(Constants.INTENT_PARENT_POSITION, -1);
        lastDateSelect = getIntent().getIntExtra(Constants.INTENT_CHILD_POSITION, -1);
        hideMoney = getIntent().getBooleanExtra(KEY_HIDE_MONEY, false);

        DialogUtil.safeShowDialog(mLoadDialog);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Calendar calendar = Calendar.getInstance();
                day = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.add(Calendar.MONTH, -6);
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                nowDay = day;
                calendar.set(year, month, 1);
                for (int i = 0; i < 7; i++) {
                    List<DateEntity> deList = new ArrayList<>();
                    MonthEntity monthEntity = new MonthEntity();
                    int maxDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                    int empty = calendar.get(Calendar.DAY_OF_WEEK);
                    empty = empty == 1 ? 0 : empty - 1;
                    for (int j = 0; j < empty; j++) {
                        DateEntity de = new DateEntity();
                        de.setType(1);
                        deList.add(de);
                    }
                    for (int j = 1; j <= maxDayOfMonth; j++) {
                        DateEntity de = new DateEntity();
                        if (i == 6) {
                            de.setType(j > nowDay ? 4 : 0);
                            if (j > nowDay) {
                                break;
                            }
                        } else {
                            de.setType(0);
                        }
               /* if (i == 6 && nowDay == j) {
                    de.setDate(CALENDAR_TODAY);
                } else {
                    de.setDate(j);
                }*/
                        de.setDate(j);
                        Calendar cal = Calendar.getInstance();
                        cal.set(year, month, j);
                        de.setParentPos(i);
                        de.setDesc(getString(R.string.tx_mark) + "0.00");
                       /* if (mSelectDayBean != null && mSelectDayBean.getData() != null && mSelectDayBean.getData().size() > 0) {
                            for (int n = 0; n < mSelectDayBean.getData().size(); n++) {
                                List<ReportSelectDayBean.DataBean.DaysBean> days = mSelectDayBean.getData().get(n).getDays();
                                if (days != null && days.size() > 0) {
                                    for (int m = 0; m < days.size(); m++) {
                                        if (DateUtil.formatDate(cal.getTime()).equals(days.get(m).getPayTradeTime())) {
                                            de.setDesc(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(days.get(m).getNetFee()));
                                            break;
                                        }
                                    }
                                }
                            }
                        }*/
                        deList.add(de);
                        /*de.setParentPos(i);
                        de.setDesc(Lunar.getLunarDate(year, month + 1, j));
                        deList.add(de);*/
                    }
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH) + 1;
                    monthEntity.setTitle(DateUtil.formatDate(calendar.getTime(), "yyyy-MM"));
//                    monthEntity.setTitle(year + "年" + month + "月");
                    monthEntity.setYear(year);
                    monthEntity.setList(deList);
                    monthList.add(monthEntity);

                    calendar.add(Calendar.MONTH, 1);
                }
                if (lastMonthSelect == -1)
                    lastMonthSelect = monthList.size() - 1;
                if (lastDateSelect == -1)
                    lastDateSelect = monthList.get(monthList.size() - 1).getList().size() - 1;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initCalendarRv();
                        DialogUtil.safeCloseDialog(mLoadDialog);
                    }
                });
            }
        }).start();
    }

    private void initCalendarRv() {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRvCalendar.setLayoutManager(llm);

        adapter = new MonthAdapter(this, monthList, hideMoney);
        adapter.setChildClickListener(this);
        mRvCalendar.setAdapter(adapter);
        mRvCalendar.scrollToPosition(lastMonthSelect+1);

        monthList.get(lastMonthSelect).getList().get(lastDateSelect).setType(8);
        adapter.notifyItemChanged(lastMonthSelect);
    }

    private void getViews() {
        mTvTitle = findViewById(R.id.tv_title);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle.setText(R.string.tv_select_date);
        mRvCalendar = (RecyclerView) findViewById(R.id.rv_calendar);
    }

    private void initListener() {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onMonthClick(int parentPos, int pos) {
        /*if (parentPos == lastMonthSelect && pos == lastDateSelect) {
            return;
        }*/
        monthList.get(parentPos).getList().get(pos).setType(8);
        adapter.notifyItemChanged(parentPos);
        if (lastDateSelect != -1) {
            monthList.get(lastMonthSelect).getList().get(lastDateSelect).setType(0);
            adapter.notifyItemChanged(lastMonthSelect);
        }
        lastMonthSelect = parentPos;
        lastDateSelect = pos;
        Calendar calendar = DateUtil.getSelectCalender(monthList.get(parentPos).getTitle(), "yyy-MM");
        calendar.set(Calendar.DAY_OF_MONTH, monthList.get(parentPos).getList().get(pos).getDate());
        LogUtil.d("aaaaaaaa=" + lastMonthSelect + "--" + monthList.get(parentPos).getYear() + "-" + DateUtil.formatDate(calendar.getTime(), "yyyy-MM-dd") + "--" + monthList.get(parentPos).getYear() + "-" + monthList.get(parentPos).getList().get(pos).getDate());
        SelectDateBean selectDateBean = new SelectDateBean();
        selectDateBean.setParentPos(lastMonthSelect);
        selectDateBean.setChildPos(lastDateSelect);
        selectDateBean.setStartTime(DateUtil.formatDate(calendar.getTime(), "yyyy-MM-dd"));
        selectDateBean.setEndTime(DateUtil.formatDate(calendar.getTime(), "yyyy-MM-dd"));
        Intent intent = new Intent();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(Constants.RESULT_SELECT_DATE, selectDateBean);   //传递一个user对象列表
        intent.putExtras(mBundle);
        setResult(RESULT_OK, intent);
        finish();
    }

}
