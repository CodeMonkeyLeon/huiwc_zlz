package com.hstypay.enterprise.activity.reportDate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.adapter.ReportData.ChoiceMonthAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportSelectMonthBean;
import com.hstypay.enterprise.bean.SelectDateBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class ChoiceMonthActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mButton;
    private RecyclerView mRecyclerView;
    private ReportSelectMonthBean mSelectMonthBean;
    private int mParentPos = -1;
    private int mChildPos = -1;
    private ChoiceMonthAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_week_month);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.tv_select_date);
        mButton.setVisibility(View.INVISIBLE);

        mRecyclerView = findViewById(R.id.recyclerView);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    public void initData() {
        mSelectMonthBean = (ReportSelectMonthBean) getIntent().getSerializableExtra(Constants.INTENT_REPORT_MONTH_DATA);
        mParentPos = getIntent().getIntExtra(Constants.INTENT_PARENT_POSITION, -1);
        mChildPos = getIntent().getIntExtra(Constants.INTENT_CHILD_POSITION, -1);
        if (mSelectMonthBean != null && mSelectMonthBean.getData() != null && mSelectMonthBean.getData().size() > 0) {
            if (mParentPos == -1)
                mParentPos = mSelectMonthBean.getData().size() - 1;
            if (mChildPos == -1)
                mChildPos = mSelectMonthBean.getData().get(mSelectMonthBean.getData().size() - 1).getMonths().size() - 1;
            mSelectMonthBean.getData().get(mParentPos).getMonths().get(mChildPos).setSelected(true);
            CustomLinearLayoutManager customLinearLayoutManager = new CustomLinearLayoutManager(this);
            mRecyclerView.setLayoutManager(customLinearLayoutManager);
            mAdapter = new ChoiceMonthAdapter(ChoiceMonthActivity.this, mSelectMonthBean.getData());
            mAdapter.setChildClickListener(new ChoiceMonthAdapter.OnChildClickListener() {

                @Override
                public void onMonthClick(int parentPos, int pos) {
                    if (parentPos == mParentPos && pos == mChildPos) {
                        return;
                    }
                    mSelectMonthBean.getData().get(parentPos).getMonths().get(pos).setSelected(true);
                    mAdapter.notifyItemChanged(parentPos);
                    if (mChildPos != -1) {
                        mSelectMonthBean.getData().get(mParentPos).getMonths().get(mChildPos).setSelected(false);
                        mAdapter.notifyItemChanged(mParentPos);
                    }
                    mParentPos = parentPos;
                    mChildPos = pos;
                    Intent intent = new Intent();
                    Bundle mBundle = new Bundle();
                    String payTradeMonth = mSelectMonthBean.getData().get(parentPos).getMonths().get(pos).getPayTradeMonth();
                    SelectDateBean selectDateBean = new SelectDateBean();
                    selectDateBean.setParentPos(mParentPos);
                    selectDateBean.setChildPos(mChildPos);
                    selectDateBean.setStartTime(DateUtil.getMonthFirst(payTradeMonth, "yyyy-MM"));
                    selectDateBean.setEndTime(DateUtil.getMonthLastDay(payTradeMonth, "yyyy-MM"));
                    mBundle.putSerializable(Constants.RESULT_SELECT_DATE, selectDateBean);
                    intent.putExtras(mBundle);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.scrollToPosition(mChildPos);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
