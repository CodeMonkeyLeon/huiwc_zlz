package com.hstypay.enterprise.activity.reportDate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.adapter.ReportData.ChoiceWeekAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportSelectWeekBean;
import com.hstypay.enterprise.bean.SelectDateBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class ChoiceWeekActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mButton;
    private RecyclerView mRecyclerView;
    private ReportSelectWeekBean mSelectWeekBean;
    private int mParentPos = -1;
    private int mChildPos = -1;
    private ChoiceWeekAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_week_month);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.tv_select_date);
        mButton.setVisibility(View.INVISIBLE);

        mRecyclerView = findViewById(R.id.recyclerView);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    public void initData() {
        mSelectWeekBean = (ReportSelectWeekBean) getIntent().getSerializableExtra(Constants.INTENT_REPORT_WEEK_DATA);
        mParentPos = getIntent().getIntExtra(Constants.INTENT_PARENT_POSITION, -1);
        mChildPos = getIntent().getIntExtra(Constants.INTENT_CHILD_POSITION, -1);
        if (mSelectWeekBean != null && mSelectWeekBean.getData() != null && mSelectWeekBean.getData().size() > 0) {
            if (mParentPos == -1)
                mParentPos = mSelectWeekBean.getData().size() - 1;
            if (mChildPos == -1)
                mChildPos = mSelectWeekBean.getData().get(mSelectWeekBean.getData().size() - 1).getWeeks().size() - 1;
            mSelectWeekBean.getData().get(mParentPos).getWeeks().get(mChildPos).setSelected(true);
            CustomLinearLayoutManager customLinearLayoutManager = new CustomLinearLayoutManager(this);
            mRecyclerView.setLayoutManager(customLinearLayoutManager);
            mAdapter = new ChoiceWeekAdapter(ChoiceWeekActivity.this, mSelectWeekBean.getData());
            mAdapter.setChildClickListener(new ChoiceWeekAdapter.OnChildClickListener() {

                @Override
                public void onMonthClick(int parentPos, int pos) {
                    if (parentPos == mParentPos && pos == mChildPos) {
                        return;
                    }
                    mSelectWeekBean.getData().get(parentPos).getWeeks().get(pos).setSelected(true);
                    mAdapter.notifyItemChanged(parentPos);
                    if (mChildPos != -1) {
                        mSelectWeekBean.getData().get(mParentPos).getWeeks().get(mChildPos).setSelected(false);
                        mAdapter.notifyItemChanged(mParentPos);
                    }
                    mParentPos = parentPos;
                    mChildPos = pos;
                    Intent intent = new Intent();
                    Bundle mBundle = new Bundle();
                    String payTradeWeekScope = mSelectWeekBean.getData().get(parentPos).getWeeks().get(pos).getPayTradeWeekScope();
                    String[] dates = payTradeWeekScope.split(",");
                    SelectDateBean selectDateBean = new SelectDateBean();
                    selectDateBean.setParentPos(mParentPos);
                    selectDateBean.setChildPos(mChildPos);
                    selectDateBean.setStartTime(dates[0]);
                    selectDateBean.setEndTime(dates[1]);
                    mBundle.putSerializable(Constants.RESULT_SELECT_DATE, selectDateBean);   //传递一个user对象列表
                    intent.putExtras(mBundle);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.scrollToPosition(mParentPos);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
