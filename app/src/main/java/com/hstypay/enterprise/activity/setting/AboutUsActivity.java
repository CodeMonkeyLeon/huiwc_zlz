package com.hstypay.enterprise.activity.setting;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.BuildConfig;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.dialog.CommonConfirmDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ProgressBean;
import com.hstypay.enterprise.bean.UpdateVersionBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.service.DownLoadService;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.VersionUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AboutUsActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;//test master
    private TextView mButton, mTvTitle;
    private TextView mTvVersionName, mTvVersionTitle;
    private TextView mTvVersion;
    private RelativeLayout mRlVersionCheck;
    private String mVersionName;
    private int mVersionCode;
    private Intent mService;
    private UpdateVersionBean.DataBean mData;
    private Uri apkurl;
    private CommonConfirmDialog mCommonConfirmDialog;
    private ImageView mIvVersionCheck;
    private Intent mMService;
    private SelectDialog mUpdateDialog;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvVersionCheck = (ImageView) findViewById(R.id.iv_version_check);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvVersionTitle = (TextView) findViewById(R.id.tv_version_title);
        mTvVersionName = (TextView) findViewById(R.id.tv_version_name);
        mTvVersion = (TextView) findViewById(R.id.tv_ver);
        mRlVersionCheck = (RelativeLayout) findViewById(R.id.rl_version_check);
        mTvTitle.setText(R.string.title_about_us);

        if (AppHelper.getAppType() == 1) {
            mIvVersionCheck.setVisibility(View.GONE);
            mTvVersionTitle.setText(UIUtils.getString(R.string.tv_version_old));
        } else if (AppHelper.getAppType() == 2) {
            mIvVersionCheck.setVisibility(SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE) ? View.VISIBLE : View.GONE);
            mTvVersionTitle.setText(SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE) ?
                    UIUtils.getString(R.string.tv_version_old) : UIUtils.getString(R.string.tv_version_new));
        }
        mButton.setVisibility(View.INVISIBLE);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        if (ConfigUtil.updateVersionEnable()) {
            mRlVersionCheck.setOnClickListener(this);
        }
    }

    public void initData() {
        mVersionName = VersionUtils.getVersionName(MyApplication.getContext());
        mTvVersionName.setText(mVersionName);
        mTvVersion.setText(MyApplication.getContext().getString(R.string.app_name) + " " + mVersionName);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_version_check:
                if (ConfigUtil.updateVersionEnable()) {
                    if (AppHelper.isServiceRunning(AboutUsActivity.this, "com.hstypay.enterprise.service.DownLoadService")) {
                        MyToast.showToastShort(getString(R.string.update_downloading));
                    } else {
                        checkVersion();
                    }
                }
                if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    showUpdateDialog();
                }
                break;
            default:
                break;
        }
    }

    private void checkVersion() {
        // 进行网络判断
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(AboutUsActivity.this, getString(R.string.network_exception));
        } else {
            //发起网络请求
            //showNewLoading(true,getString(R.string.show_new_version_loading));
            mVersionCode = VersionUtils.getVersionCode(AboutUsActivity.this);
            Map<String, Object> map = new HashMap<>();
            if (AppHelper.getAppType() == 1) {
                map.put("client", Constants.REQUEST_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("client", Constants.REQUEST_CLIENT_APP);
            }
            map.put("versionCode", mVersionCode);
            map.put("appChannel", ConfigUtil.getAppChannel());
            map.put("appType", "HWC");
            map.put("currentVersion", VersionUtils.getVersionName(MyApplication.getContext()));
            ServerClient.newInstance(AboutUsActivity.this).updateVersion2(AboutUsActivity.this, Constants.UPDATE_VERSION_TAG, map);
        }
    }

    /*private void showNotice(final int requestCode, final String[] permissionArray, String content) {
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(AboutUsActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(AboutUsActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }

    private void showDialog(String content) {
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(AboutUsActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    installDownloadApk(mData);
                } else {
                    showDialog(getString(R.string.permission_set_content_update));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    //Eventbus接收数据,版本更新
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateVersion(NoticeEvent event) {
        if (event.getTag().equals(Constants.UPDATE_VERSION_TAG)) {
            UpdateVersionBean msg = (UpdateVersionBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AboutUsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mData = msg.getData();
                    if (mData != null) {
                        updateVersion(mData);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private void updateVersion(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (VersionUtils.getVersionName(MyApplication.getContext()).equals(data.getVersionName())) {
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, false);
                MyToast.showToastShort("已是最新版本");
                return;
            }
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                update(data);
            }
            /*if (data.getVersionCode() > 0) {
                if (data.getVersionCode() == mVersionCode) {
                    mTvVersionTitle.setText(UIUtils.getString(R.string.tv_version_new));
                    mIvVersionCheck.setVisibility(View.GONE);
                    showCommonNoticeDialog(AboutUsActivity.this, getString(R.string.show_new_version));
                    return;
                }
                if (data.getVersionCode() > mVersionCode) {
                    if (AppHelper.getApkType() == 0) {
                        boolean results = PermissionUtils.checkPermissionArray(AboutUsActivity.this, permissionArray);
                        if (results) {
                            update(data);
                        } else {
                            showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_update));
                        }
                    }
                }
            }*/
        }
    }

    private void update(UpdateVersionBean.DataBean data) {
        //进行版本升级
        mTvVersionTitle.setText(UIUtils.getString(R.string.tv_version_old));
        String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
        if (!versionName.equals(data.getVersionName())) {
            //如何服务器版本大于保存的版本，直接下载最新的版本
            //启动服务
            showUpgradeInfoDialog(data, new ComDialogListener(data), true);
            //}
        } else {
            String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
            if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, new File(string));
                apkurl = contentUri;
                showUpgradeInfoDialog(data, new ComDialogListener(data), true);
            }
        }
    }

    private void showUpdateDialog() {
        if (mUpdateDialog == null) {
            mUpdateDialog = new SelectDialog(AboutUsActivity.this, getString(R.string.dialog_to_update), getString(R.string.btnCancel), getString(R.string.btnContinue), R.layout.select_dialog);
            mUpdateDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    PackageManager packageManager = getPackageManager();
                    Intent intent = packageManager.getLaunchIntentForPackage("com.pax.ipp.stos");
                    if (intent == null) {
                        Toast.makeText(AboutUsActivity.this, "未找到应用市场", Toast.LENGTH_LONG).show();
                    } else {
                        startActivity(intent);
                    }
                }
            });
        }
        mUpdateDialog.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void showUpgradeInfoDialog(UpdateVersionBean.DataBean result, CommonConfirmDialog.ConfirmListener listener, boolean isWifi) {
        mCommonConfirmDialog = new CommonConfirmDialog(AboutUsActivity.this, listener, result, isWifi);
        if (!mCommonConfirmDialog.isShowing()) {
            mCommonConfirmDialog.show();
        }
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpdateVersionBean.DataBean result;

        public ComDialogListener(UpdateVersionBean.DataBean result) {
            this.result = result;
        }

        @Override
        public void ok() {
            boolean results = PermissionUtils.checkPermissionArray(AboutUsActivity.this, permissionArray);
            if (results) {
                if (apkurl != null) {
                    openAPKFile();
                } else {
                    installDownloadApk(result);
                }
            } else {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_update));
            }
        }

        @Override
        public void cancel() {
            if (mService != null) {
                stopService(mService);
            }
        }
    }

    private void installDownloadApk(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                //进行版本升级
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, true);
                String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
                mService = new Intent(AboutUsActivity.this, DownLoadService.class);
                mService.putExtra("appurl", data.getAppUrl());
                mService.putExtra("versionName", data.getVersionName());
                mService.putExtra(Constants.INTENT_NAME, Constants.UPDATE_PROGRESS_TAG);
                if (!versionName.equals(data.getVersionName())) {
                    //如何服务器版本大于保存的版本，直接下载最新的版本
                    //启动服务
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            startService(mService);
                        }
                    }).start();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(false);
                    }
                } else {
                    String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
                    if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, new File(string));
                        apkurl = contentUri;
                        showUpgradeInfoDialog(data, new ComDialogListener(data), true);
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                startService(mService);
                            }
                        }).start();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(false);
                        }
                    }
                }

            }
        } else {
            LogUtil.d("版本升级---11111");
        }

    }

    //Eventbus接收数据,服务返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateService(NoticeEvent event) {
        if (event.getTag().equals(Constants.UPDATE_PROGRESS_TAG)) {
            switch (event.getCls()) {
                case Constants.UPDATE_DOWNLOAD_SUCCESS:
                    File file = (File) event.getMsg();
                    if (file != null && file.isFile()) {
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, file);
                        apkurl = contentUri;
                        openAPKFile();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(true);
                        }
                    }
                    break;
                case Constants.UPDATE_DOWNLOAD_FAILED:
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(true);
                        MyToast.showToast("下载失败，请重新下载！", Toast.LENGTH_SHORT);
                    }
                    break;
                case Constants.UPDATE_DOWNLOADING:
                    ProgressBean progressBean = (ProgressBean) event.getMsg();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setProgress(progressBean);
                    }
                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUpdateDialog != null) {
            if (mUpdateDialog.isShowing()) {
                mUpdateDialog.dismiss();
            }
        }
    }

    /**
     * 打开安装包
     */
    private void openAPKFile() {
        String filePath = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
        String mimeDefault = "application/vnd.android.package-archive";
        File apkFile = null;
        if (!TextUtils.isEmpty(filePath) && !TextUtils.isEmpty(Uri.parse(filePath).getPath())) {
            apkFile = new File(filePath);
        }
        if (apkFile == null) {
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //兼容7.0
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //这里牵涉到7.0系统中URI读取的变更
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), Constants.FILE_PROVIDER, new File(filePath));
                intent.setDataAndType(contentUri, mimeDefault);
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                        return;
                    }
                }
            } else {
                intent.setDataAndType(Uri.fromFile(apkFile), mimeDefault);
            }
            if (getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                //如果APK安装界面存在，携带请求码跳转。使用forResult是为了处理用户 取消 安装的事件。外面这层判断理论上来说可以不要，但是由于国内的定制，这个加上还是比较保险的
                startActivity(intent);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到设置-允许安装未知来源-页面
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {
        //后面跟上包名，可以直接跳转到对应APP的未知来源权限设置界面。使用startActivityForResult 是为了在关闭设置界面之后，获取用户的操作结果，然后根据结果做其他处理
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, Constants.REQUEST_INSTALL_PACKAGES);
    }

    /**
     * 功用：未知来源权限弹窗
     * 说明：8.0系统中升级APK时，如果跳转到了 未知来源权限设置界面，并且用户没用允许该权限，会弹出此窗口
     */
    private void showUnKnowResourceDialog() {
        SelectDialog dialog = new SelectDialog(AboutUsActivity.this, "未知来源权限设置界面", R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_INSTALL_PACKAGES) {
            if (resultCode == RESULT_OK) {
                openAPKFile();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        showUnKnowResourceDialog();
                    }
                }
            }
        }
    }
}
