package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.LoginActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.keyboard.SafeKeyboard;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ChangePwdActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvEyeOld, mIvEyeNew, mIvEyeEnsure;
    private TextView mButton, mTvTitle;
    private EditText mEtOldPwd, mEtNewPwd1, mEtNewPwd2;
    private Button mBtnSubmit;
    private SelectDialog mSelectDialog;
    private boolean isOpenOld, isOpenNew, isOpenEnsure;
    private SafeKeyboard safeKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvEyeOld = (ImageView) findViewById(R.id.iv_eye_old);
        mIvEyeNew = (ImageView) findViewById(R.id.iv_eye_new);
        mIvEyeEnsure = (ImageView) findViewById(R.id.iv_eye_ensure);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mTvTitle.setText(R.string.title_change_password);
        mButton.setVisibility(View.INVISIBLE);

        mEtOldPwd = (EditText) findViewById(R.id.et_old_pwd);
        mEtNewPwd1 = (EditText) findViewById(R.id.et_new_pwd1);
        mEtNewPwd2 = (EditText) findViewById(R.id.et_new_pwd2);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);

        View rootView = findViewById(R.id.main_root);
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboard = new SafeKeyboard(getApplicationContext(), keyboardContainer,
                R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId(), rootView);
        safeKeyboard.putEditText(mEtOldPwd.getId(), mEtOldPwd);
        safeKeyboard.putEditText(mEtNewPwd1.getId(), mEtNewPwd1);
        safeKeyboard.putEditText(mEtNewPwd2.getId(), mEtNewPwd2);

        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        if (!StringUtils.isEmptyOrNull(mEtOldPwd.getText().toString().trim())
                && !StringUtils.isEmptyOrNull(mEtNewPwd1.getText().toString().trim())
                && !StringUtils.isEmptyOrNull(mEtNewPwd2.getText().toString().trim())) {
            setButtonEnable(mBtnSubmit, true);
        } else {
            setButtonEnable(mBtnSubmit, false);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvEyeOld.setOnClickListener(this);
        mIvEyeNew.setOnClickListener(this);
        mIvEyeEnsure.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (!StringUtils.isEmptyOrNull(mEtOldPwd.getText().toString().trim())
                        && !StringUtils.isEmptyOrNull(mEtNewPwd1.getText().toString().trim())
                        && !StringUtils.isEmptyOrNull(mEtNewPwd2.getText().toString().trim())) {
                    setButtonEnable(mBtnSubmit, true);
                } else {
                    setButtonEnable(mBtnSubmit, false);
                }
            }
        });
        mEtOldPwd.addTextChangedListener(editTextWatcher);
        mEtNewPwd1.addTextChangedListener(editTextWatcher);
        mEtNewPwd2.addTextChangedListener(editTextWatcher);
    }

    public void initData() {

    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.LOGIN_CHANGE_PWD_TAG)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.LOGIN_CHANGE_PWD_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ChangePwdActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ChangePwdActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.LOGIN_CHANGE_PWD_TRUE:
                    dismissLoading();
                    showCommonNoticeDialog(ChangePwdActivity.this, getString(R.string.change_pwd_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            SpUtil.removeKey(Constants.SKEY);
                            SpUtil.removeAll();
                            startActivity(new Intent(ChangePwdActivity.this, LoginActivity.class));
                            ChangePwdActivity.this.finish();
                            MyApplication.getInstance().finishAllActivity();
                        }
                    });
                    break;
            }
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //提交
            case R.id.btn_submit:
                submit();
                break;
            case R.id.iv_eye_old:
                isOpenOld = setEye(isOpenOld);
                setPwdVisible(isOpenOld, mIvEyeOld, mEtOldPwd);
                break;
            case R.id.iv_eye_new:
                isOpenNew = setEye(isOpenNew);
                setPwdVisible(isOpenNew, mIvEyeNew, mEtNewPwd1);
                break;
            case R.id.iv_eye_ensure:
                isOpenEnsure = setEye(isOpenEnsure);
                setPwdVisible(isOpenEnsure, mIvEyeEnsure, mEtNewPwd2);
                break;
            default:
                break;
        }
    }

    private void submit() {

        if (StringUtils.isEmptyOrNull(mEtOldPwd.getText().toString().trim()) || StringUtils.isEmptyOrNull(mEtNewPwd1.getText().toString().trim()) || StringUtils.isEmptyOrNull(mEtNewPwd2.getText().toString().trim())) {
            showCommonNoticeDialog(ChangePwdActivity.this, getString(R.string.pwd_not_null));
            return;
        }

        if (!mEtNewPwd1.getText().toString().trim().equals(mEtNewPwd2.getText().toString().trim())) {
            mEtNewPwd2.setFocusable(true);
            mEtNewPwd2.setFocusableInTouchMode(true);
            mEtNewPwd2.requestFocus();
            showCommonNoticeDialog(ChangePwdActivity.this, getString(R.string.tx_pass_notdiff));
            return;
        }

//        if (!StringUtils.isLetterDigit(mEtNewPwd1.getText().toString().trim())){
        if (mEtNewPwd1.getText().toString().trim().length() < 6) {
            mEtNewPwd1.setFocusable(true);
            mEtNewPwd1.setFocusableInTouchMode(true);
            mEtNewPwd1.requestFocus();
            showCommonNoticeDialog(ChangePwdActivity.this, getString(R.string.pwd_prompting));
            return;
        }

//        if (!StringUtils.isLetterDigit(mEtNewPwd2.getText().toString().trim())){
        if (mEtNewPwd2.getText().toString().trim().length() < 6) {
            mEtNewPwd2.setFocusable(true);
            mEtNewPwd2.setFocusableInTouchMode(true);
            mEtNewPwd2.requestFocus();
            showCommonNoticeDialog(ChangePwdActivity.this, getString(R.string.pwd_prompting));
            return;
        }

        if (mEtOldPwd.getText().toString().trim().equals(mEtNewPwd1.getText().toString().trim())) {
            mEtNewPwd1.setFocusable(true);
            mEtNewPwd1.setFocusableInTouchMode(true);
            mEtNewPwd1.requestFocus();
            showCommonNoticeDialog(ChangePwdActivity.this, getString(R.string.tx_pass_not_same));
            return;
        }

        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(ChangePwdActivity.this, getString(R.string.tv_modify_pass), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    Map<String, Object> map = new HashMap<>();
                    map.put("oldPassword", INITDES3Util.getSecretContent(ChangePwdActivity.this, mEtOldPwd.getText().toString().trim()));
                    map.put("newPassword", INITDES3Util.getSecretContent(ChangePwdActivity.this, mEtNewPwd2.getText().toString().trim()));
                    map.put("desFlag", 1);
                    showNewLoading(true, getString(R.string.change_pwd));
                    ServerClient.newInstance(ChangePwdActivity.this).changePwd(ChangePwdActivity.this, Constants.LOGIN_CHANGE_PWD_TAG, map);
                }
            });
            DialogHelper.resize(ChangePwdActivity.this, mSelectDialog);
        }
        mSelectDialog.show();
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.ic_login_password_visible);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.ic_login_password_hiding);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return isOpen ? false : true;
    }

    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboard.isShow()) {
                safeKeyboard.hideKeyboard();
                return false;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (safeKeyboard != null) {
            safeKeyboard.release();
            safeKeyboard = null;
        }
        super.onDestroy();
    }
}
