package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.LoginActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.EditTextWatcher;
import com.hstypay.enterprise.utils.INITDES3Util;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class ChangeTelActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private LinearLayout mLlVoiceCode;
    private TextView mTvTitle, mTvPhone,mTvNotice, mTvGetCode, mTvVoiceButton, mBtnTitle;
    private ImageView mIvClean;
    private EditText mEtTelephone, mEtCode;
    private Button mBtnSubmit;
    private SafeDialog mLoadDialog;
    private NoticeDialog mNoticeDialog;
    private TimeCount time;

    private static final int GET_MESSAGE_CODE = 1;
    private static final int GET_VOICE_CODE = 2;

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_id:
                    if (hasFocus) {
                        if (mEtTelephone.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_tel);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mTvPhone = (TextView) findViewById(R.id.tv_telephone);
        mTvNotice = (TextView) findViewById(R.id.tv_change_tel_notice);
        mIvClean = (ImageView) findViewById(R.id.iv_clean_input);
        mEtTelephone = (EditText) findViewById(R.id.et_id);//手机号
        mEtCode = (EditText) findViewById(R.id.et_code);
        mTvGetCode = (TextView) findViewById(R.id.tv_get_code);
        mTvVoiceButton = (TextView) findViewById(R.id.tv_phone_verify);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mLlVoiceCode = findViewById(R.id.ll_voice_code);
        setButtonEnable(mBtnSubmit,false);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mTvGetCode.setOnClickListener(this);
        mTvVoiceButton.setOnClickListener(this);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtTelephone.isFocused()) {
                    if (mEtTelephone.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                }

                if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())
                        && !StringUtils.isEmptyOrNull(mEtCode.getText().toString())) {
                    setButtonEnable(mBtnSubmit,true);
                } else {
                    setButtonEnable(mBtnSubmit,false);
                }
            }
        });
        mEtCode.addTextChangedListener(editTextWatcher);
        mEtTelephone.addTextChangedListener(editTextWatcher);
        mEtCode.setOnFocusChangeListener(listener);
        mEtTelephone.setOnFocusChangeListener(listener);
    }

    private void initData() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        String telephone = getIntent().getStringExtra(Constants.INTENT_CHANGE_BANK);
        if (Constants.INTENT_CHANGE_BANK.equals(mIntentName)){
            mTvTitle.setText(R.string.title_authentication);
            mTvNotice.setVisibility(View.GONE);
            mTvPhone.setVisibility(View.GONE);
            mEtTelephone.setText(telephone);
            mEtTelephone.setEnabled(false);
            mLlVoiceCode.setVisibility(View.GONE);
            mEtCode.setFocusable(true);
            mEtCode.setFocusableInTouchMode(true);
            mEtCode.requestFocus();
        }else {
            String loginPhone = MyApplication.getLoginPhone();
            if (TextUtils.isEmpty(loginPhone)) {
                mTvTitle.setText(UIUtils.getString(R.string.binding_tel));
                mTvPhone.setVisibility(View.GONE);
                mTvNotice.setText(UIUtils.getString(R.string.binding_tel_notice));
            }else{
                mTvPhone.setText(UIUtils.getString(R.string.change_telephone)+loginPhone);
                mTvNotice.setText(UIUtils.getString(R.string.change_tel_notice));
                mTvTitle.setText(UIUtils.getString(R.string.change_tel));
            }
            mEtTelephone.setEnabled(true);
            mLlVoiceCode.setVisibility(View.VISIBLE);
        }
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_BINDING_MSG) || event.getTag().equals(Constants.TAG_CHANGE_BANK_CODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://验证码返回成功
                    loadGetCode();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_BINDING_VOICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(ChangeTelActivity.this,getString(R.string.dialog_notice_voice_tx), getString(R.string.dialog_notice_voice_after));
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_BINDING)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.GET_BINDING_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_BINDING_TRUE:
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE, mEtTelephone.getText().toString().trim());
                    CommonNoticeDialog dialog = new CommonNoticeDialog(ChangeTelActivity.this,UIUtils.getString(R.string.dialog_notice_binding_success)
                            , UIUtils.getString(R.string.dialog_notice_button));
                    dialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            finish();
                        }
                    });
                    DialogHelper.resize(ChangeTelActivity.this, dialog);
                    dialog.show();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CHANGE_BINGING)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    CommonNoticeDialog dialog = new CommonNoticeDialog(ChangeTelActivity.this,UIUtils.getString(R.string.dialog_notice_binding_success)
                            , UIUtils.getString(R.string.dialog_notice_button));
                    dialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            stayRecord();
                            startActivity(new Intent(ChangeTelActivity.this,LoginActivity.class));
                            finish();
                        }
                    });
                    DialogHelper.resize(ChangeTelActivity.this, dialog);
                    dialog.show();
                    break;
            }
        }
    }

    private void stayRecord() {
        String loginRecord = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_RECORD);
        Gson gson = new Gson();
        List<String> records = new ArrayList<>();
        if (!TextUtils.isEmpty(loginRecord)) {
            records = gson.fromJson(loginRecord, new TypeToken<ArrayList<String>>() {
            }.getType());
            Iterator<String> it = records.iterator();
            while (it.hasNext()) {
                String account = it.next();
                if (mEtTelephone.getText().toString().trim().equals(account)) {
                    it.remove();
                }
            }
            if (records.size() == 3) {
                records.remove(2);
            }
        }
        records.add(0, mEtTelephone.getText().toString().trim());
        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_RECORD, gson.toJson(records));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean_input:
                mEtTelephone.setText("");
                mIvClean.setVisibility(View.GONE);
                break;
            case R.id.tv_get_code:
                //获取验证码
                if (Constants.INTENT_CHANGE_BANK.equals(mIntentName)){
                    getChangeBankMsg();
                }else {
                    getCode(GET_MESSAGE_CODE);
                }
                break;
            //下一步
            case R.id.btn_submit:
                if (Constants.INTENT_CHANGE_BANK.equals(mIntentName)){
                    Intent intent = new Intent();
                    intent.putExtra(Constants.RESULT_CHANGE_BANK, mEtCode.getText().toString().trim());
                    setResult(RESULT_OK, intent);
                    finish();
                }else {
                    bindingTel();
                }
                break;
            case R.id.tv_phone_verify:
                getCode(GET_VOICE_CODE);
                break;
        }
    }

    private void bindingTel() {
        if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString().trim())) {
            if (mEtTelephone.getText().toString().trim().length() != 11) {
                MyToast.showToastShort(UIUtils.getString(R.string.tv_phone_num_error));
                return;
            }
        }else {
            MyToast.showToastShort(UIUtils.getString(R.string.phone_not_null));
        }
        /*if (!StringUtils.isEmptyOrNull(mEtCode.getText().toString().trim())) {
            if (mEtCode.getText().toString().trim().length() != 6) {
                MyToast.showToastShort(UIUtils.getString(R.string.code_error));
                return;
            }
        }*/
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", mEtTelephone.getText().toString().trim());
            map.put("code", mEtCode.getText().toString().trim());
            String loginPhone = MyApplication.getLoginPhone();
            if (TextUtils.isEmpty(loginPhone)) {
                ServerClient.newInstance(MyApplication.getContext()).bindingTel(MyApplication.getContext(), Constants.TAG_GET_BINDING, map);
            }else{
                String loginPwd = getIntent().getStringExtra(Constants.INTENT_LOGIN_PWD);
                map.put("password",  INITDES3Util.getSecretContent(this, loginPwd));
                map.put("desFlag", 1);
                ServerClient.newInstance(MyApplication.getContext()).changeBindingTel(MyApplication.getContext(), Constants.TAG_CHANGE_BINGING, map);
            }
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void getCode(int type) {
        if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())) {
            if (mEtTelephone.getText().toString().length() != 11) {
                MyToast.showToastShort(UIUtils.getString(R.string.tv_phone_num_error));
                return;
            }
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.phone_not_null));
            return;
        }
        if (type == GET_MESSAGE_CODE) {
            getMessageCode();
        } else if (type == GET_VOICE_CODE) {
            getVoiceCode();
        }

    }

    private void getChangeBankMsg() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            ServerClient.newInstance(MyApplication.getContext()).getChangeBankMsg(MyApplication.getContext(), Constants.TAG_CHANGE_BANK_CODE, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void getMessageCode() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", mEtTelephone.getText().toString().trim());
            ServerClient.newInstance(MyApplication.getContext()).getBindingMsg(MyApplication.getContext(), Constants.TAG_GET_BINDING_MSG, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void getVoiceCode() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", mEtTelephone.getText().toString().trim());
            ServerClient.newInstance(MyApplication.getContext()).getBindingVoice(MyApplication.getContext(), Constants.TAG_GET_BINDING_VOICE, map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    private void loadGetCode() {
        Countdown();
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();

    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            mTvGetCode.setEnabled(true);
            mTvGetCode.setClickable(true);
            mTvGetCode.setText(R.string.bt_code_get);
            mTvGetCode.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            mTvGetCode.setEnabled(false);
            mTvGetCode.setClickable(false);
            mTvGetCode.setTextColor(getResources().getColor(R.color.user_edit_color));
            mTvGetCode.setText(getString(R.string.verify_code_count_down) + millisUntilFinished / 1000 + "s");
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
