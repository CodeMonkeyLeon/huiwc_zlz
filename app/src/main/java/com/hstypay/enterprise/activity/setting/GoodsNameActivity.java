package com.hstypay.enterprise.activity.setting;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class GoodsNameActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle;
    private EditTextDelete mEtGoodsName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_name);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        initView();
        initEvent();
        initData();
    }

    public void initView(){
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtGoodsName = (EditTextDelete) findViewById(R.id.et_goods_name);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);

        String goodsName = getIntent().getStringExtra(Constants.INTENT_GET_GOODS_NAME);
        if(!StringUtils.isEmptyOrNull(goodsName)){
            mEtGoodsName.setText(goodsName);
        }
        mEtGoodsName.setSelection(mEtGoodsName.length());
        if (TextUtils.isEmpty(mEtGoodsName.getText().toString().trim())){
            mBtnSubmit.setEnabled(false);
            mBtnSubmit.setBackgroundResource(R.drawable.btn_unsure_bg);
            mBtnSubmit.setTextColor(UIUtils.getColor(R.color.bt_enable));
        }else{
            mBtnSubmit.setEnabled(true);
            mBtnSubmit.setBackgroundResource(R.drawable.selector_btn_register);
            mBtnSubmit.setTextColor(UIUtils.getColor(R.color.white));
        }
        mTvTitle.setText(R.string.title_goods_name);
        mButton.setVisibility(View.INVISIBLE);
        setButtonEnable(mBtnSubmit,false);
    }
    public void initEvent(){
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

            mEtGoodsName.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
                @Override
                public void onEditChanged(boolean isClear) {
                    if (isClear) {
                        setButtonEnable(mBtnSubmit,false);
                    } else {
                        if (!mBtnSubmit.isEnabled()) {
                            setButtonEnable(mBtnSubmit,true);
                        }
                    }
                }
            });
        }

    public void initData(){

    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangeGoodsName(NoticeEvent event) {
        if (event.getTag().equals(Constants.GOODS_NAME_TAG)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    break;
                case Constants.GOODS_NAME_FALSE:
                    dismissLoading();
                    showCommonNoticeDialog(GoodsNameActivity.this,"",msg.getError().getMessage());
                    break;
                case Constants.GOODS_NAME_TRUE:
                    dismissLoading();
                    SpUtil.putString(MyApplication.getContext(),Constants.SP_GOODS_NAME,mEtGoodsName.getText().toString().trim());
                    showCommonNoticeDialog(GoodsNameActivity.this, getString(R.string.add_goods_name_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                submit();
                break;
            default:
                break;
        }
    }

    private void submit() {

        if(StringUtils.isEmptyOrNull(mEtGoodsName.getText().toString())){
            MyToast.showToastShort(ToastHelper.toStr(R.string.goods_name_not_null));
            return;
        }

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true,getString(R.string.public_submitting));
            Map<String,Object> map=new HashMap<>();
            map.put("payGoodsName",mEtGoodsName.getText().toString().trim());
            ServerClient.newInstance(GoodsNameActivity.this).goodsName(GoodsNameActivity.this, Constants.GOODS_NAME_TAG,map);
        } else {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
        }
    }
}
