package com.hstypay.enterprise.activity.setting;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class InstructionsActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mButton, mTvTitle;
    private WebView mWvInstruction;
    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    @SuppressLint("JavascriptInterface")
    public void initView(){
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mWvInstruction = (WebView) findViewById(R.id.webview);
        mTvTitle.setText(R.string.title_instructions);
        mButton.setVisibility(View.INVISIBLE);

    }
    public void initEvent(){
        mIvBack.setOnClickListener(this);
    }

    public void initData(){
        Intent intent = getIntent();
        mUrl = intent.getStringExtra(Constants.INTENT_INSTRUCTION);
        mWvInstruction.getSettings().setJavaScriptEnabled(true);
        WebSettings settings = mWvInstruction.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }

        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setAppCacheEnabled(true);//是否使用缓存
        mWvInstruction.setWebChromeClient(new InstructionsActivity.myWebChromeClien());
        mWvInstruction.setWebViewClient(new InstructionsActivity.MyWebViewClient());
        mWvInstruction.getSettings().setDomStorageEnabled(true);
        mWvInstruction.setWebViewClient(new InstructionsActivity.MyWebViewClient());
        mWvInstruction.loadUrl(mUrl);

    }


    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error)
        {
            // super.onReceivedSslError(view, handler, error);
            handler.proceed();

        }

        /** {@inheritDoc} */

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            try
            {
                showNewLoading(true, getString(R.string.public_loading));
            }
            catch (Exception e)
            {
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            /*// showNewLoading(true, getString(R.string.public_loading));
            //调用拨号程序
            if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:"))
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }*/
            return super.shouldOverrideUrlLoading(view, url);
        }
        @Override
        public void onPageFinished(WebView view, String url)
        {
            dismissLoading();
            super.onPageFinished(view, url);
        }
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            LogUtil.i("zhouwei", "onReceivedError " + description + ",errorCode-->" + errorCode);

            dismissLoading();
        }


    }

    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_BACK)
        {

            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            default:
                break;
        }
    }
}
