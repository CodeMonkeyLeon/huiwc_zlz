package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.OrderShopAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderPrintSetActivity extends BaseActivity implements View.OnClickListener, OrderShopAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack, mIvButton;
    private TextView mButton, mTvTitle, mTvNotice;
    private RecyclerView mRvVoiceShop;
    List<VoiceBean.DataBean> mList = new ArrayList<>();
    private OrderShopAdapter mAdapter;
    Boolean multShop[];
    private VoiceBean.DataBean mDataBean;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_notice);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvButton = (ImageView) findViewById(R.id.iv_button);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNotice = (TextView) findViewById(R.id.tv_notice);
        mRvVoiceShop = (RecyclerView) findViewById(R.id.rv_voice_shop);
        mRvVoiceShop.setLayoutManager(new LinearLayoutManager(this));

        mTvNotice.setText(R.string.setting_order_print);
        mTvTitle.setText(R.string.title_order_print);
        mButton.setVisibility(View.INVISIBLE);
        mIvButton.setVisibility(View.VISIBLE);
        mIvButton.setImageResource(R.mipmap.voice_help_icon);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvButton.setOnClickListener(this);
    }

    public void initData() {
        if (!NetworkUtils.isNetworkAvailable(OrderPrintSetActivity.this)) {
            showCommonNoticeDialog(OrderPrintSetActivity.this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (AppHelper.getAppType() == 1) {
                map.put("client", Constants.REQUEST_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("client", Constants.REQUEST_CLIENT_APP);
            }

            String clientid = PushManager.getInstance().getClientid(this.getApplicationContext());
            if (!StringUtils.isEmptyOrNull(clientid)) {
                map.put("pushDeviceId", clientid);
            }
            ServerClient.newInstance(OrderPrintSetActivity.this).orderPushList(OrderPrintSetActivity.this, Constants.TAG_PUSH_ORDER_LIST, map);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button:
                Intent intent = new Intent(OrderPrintSetActivity.this, InstructionsActivity.class);
                intent.putExtra(Constants.INTENT_INSTRUCTION, Constants.URL_VOICE_INSTRUCTION);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    //Eventbus接收数据，语言播报列表
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PUSH_ORDER_LIST)) {
            VoiceBean msg = (VoiceBean) event.getMsg();
            DialogUtil.safeCloseDialog(mLoadDialog);
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(OrderPrintSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(OrderPrintSetActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<VoiceBean.DataBean> data = msg.getData();
                    if (data != null && data.size() > 0) {
                        mList.addAll(data);
                        multShop = new Boolean[mList.size()];
                        for (int i = 0; i < multShop.length; i++) {
                            multShop[i] = false;
                        }
                    }
                    mAdapter = new OrderShopAdapter(OrderPrintSetActivity.this, mList);
                    mRvVoiceShop.setAdapter(mAdapter);
                    mAdapter.setOnItemClickListener(this);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PUSH_ORDER_SET)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(OrderPrintSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(OrderPrintSetActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onItemClick(int position) {
        mDataBean = mList.get(position);
        if (mDataBean != null) {
            pushVoiceSetting(mDataBean.getPushClose() + "");
        }

    }

    private void pushVoiceSetting(String pushClose) {
        if (!NetworkUtils.isNetworkAvailable(OrderPrintSetActivity.this)) {
            showCommonNoticeDialog(OrderPrintSetActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();

            if (mDataBean != null) {
                if (!StringUtils.isEmptyOrNull(mDataBean.getStoreMerchantId())) {
                    map.put("storeMerchantId", mDataBean.getStoreMerchantId());
                }
            }
            map.put("pushClose", pushClose);
            if (AppHelper.getAppType() == 1) {
                map.put("client", Constants.REQUEST_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("client", Constants.REQUEST_CLIENT_APP);
            }
            String clientid = PushManager.getInstance().getClientid(this.getApplicationContext());
            if (!StringUtils.isEmptyOrNull(clientid)) {
                map.put("pushDeviceId", clientid);
            }
            ServerClient.newInstance(OrderPrintSetActivity.this).orderPushSet(OrderPrintSetActivity.this, Constants.TAG_PUSH_ORDER_SET, map);
        }
    }
}
