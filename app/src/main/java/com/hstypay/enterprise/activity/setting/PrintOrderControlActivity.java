package com.hstypay.enterprise.activity.setting;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class PrintOrderControlActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private ImageView mIvPrintPayOrder,mIvPrintClientOrder,mIvPrintKitchenOrder,mIvPrintKitchenCount,mIvPrintKitchenControl;
    private RelativeLayout mRlPrintPayOrder,mRlPrintClientOrder,mRlPrintKitchenOrder,mRlPrintKitchenCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_order_control);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView(){
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mRlPrintPayOrder = (RelativeLayout) findViewById(R.id.rl_print_pay_order);
        mIvPrintPayOrder = (ImageView) findViewById(R.id.iv_print_pay_order);
        mRlPrintClientOrder = (RelativeLayout) findViewById(R.id.rl_print_client_order);
        mIvPrintClientOrder = (ImageView) findViewById(R.id.iv_print_client_order);
        mRlPrintKitchenOrder = (RelativeLayout) findViewById(R.id.rl_print_kitchen_order);
        mIvPrintKitchenOrder = (ImageView) findViewById(R.id.iv_print_kitchen_order);
        mRlPrintKitchenCount = (RelativeLayout) findViewById(R.id.rl_print_kitchen_count);
        mIvPrintKitchenCount = (ImageView) findViewById(R.id.iv_print_kitchen_count);
        mIvPrintKitchenControl = (ImageView) findViewById(R.id.iv_print_kitchen_control);
        mIvPrintPayOrder.setImageResource(MyApplication.isPrintPayOrder()?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
        mIvPrintClientOrder.setImageResource(MyApplication.isPrintClientOrder()?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
        mIvPrintKitchenOrder.setImageResource(MyApplication.isPrintKitchenOrder()?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
        mIvPrintKitchenCount.setImageResource(MyApplication.isPrintKitchenCount()?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);

        Bitmap bitmap = ((BitmapDrawable)getResources().getDrawable(R.mipmap.ic_arrow_right)).getBitmap();
        Matrix matrix  = new Matrix();
        matrix.setRotate(90);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
        mIvPrintKitchenControl.setImageBitmap(createBitmap);

        mTvTitle.setText(R.string.title_print_order_control);
    }
    public void initEvent(){
        mIvBack.setOnClickListener(this);
        mRlPrintPayOrder.setOnClickListener(this);
        mRlPrintClientOrder.setOnClickListener(this);
        mRlPrintKitchenOrder.setOnClickListener(this);
        mRlPrintKitchenCount.setOnClickListener(this);
    }

    public void initData(){
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_print_pay_order:
                setToggle(!MyApplication.isPrintPayOrder(),mIvPrintPayOrder,Constants.SP_PRINT_PAY_ORDER);
                break;
            case R.id.rl_print_client_order:
                setToggle(!MyApplication.isPrintClientOrder(),mIvPrintClientOrder,Constants.SP_PRINT_CLIENT_ORDER);
                break;
            case R.id.rl_print_kitchen_order:
                setToggle(!MyApplication.isPrintKitchenOrder(),mIvPrintKitchenOrder,Constants.SP_PRINT_KITCHEN_ORDER);
                break;
            case R.id.rl_print_kitchen_count:
                setToggle(!MyApplication.isPrintKitchenCount(),mIvPrintKitchenCount,Constants.SP_PRINT_KITCHEN_COUNT);
                break;
            default:
                break;
        }
    }
    public void setToggle(boolean isChecked,ImageView imageView ,String spString){
        imageView.setImageResource(isChecked?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
        SpStayUtil.putBoolean(MyApplication.getContext(),spString,isChecked);
    }
}
