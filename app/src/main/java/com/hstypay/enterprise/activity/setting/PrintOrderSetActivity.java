package com.hstypay.enterprise.activity.setting;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class PrintOrderSetActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle;
    private ImageView mIvOrderPushMsg;
    private boolean isLinked = true;
    private RelativeLayout mRlReceptionPrint,mRlKitchenPrint,mRlOrderPrintSet,mRlOrderPrintControl;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private String[] permissionArray = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_order_set);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView(){
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvOrderPushMsg = (ImageView) findViewById(R.id.iv_order_push_msg);
        mIvOrderPushMsg.setImageResource(isLinked?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);

        mRlOrderPrintSet = findViewById(R.id.rl_order_print_set);
        mRlOrderPrintControl = findViewById(R.id.rl_order_print_control);
        mRlReceptionPrint = findViewById(R.id.rl_reception_print);
        mRlKitchenPrint = findViewById(R.id.rl_kitchen_print);

        mTvTitle.setText(R.string.title_print_order_set);
    }

    public void initEvent(){
        mIvBack.setOnClickListener(this);
        mIvOrderPushMsg.setOnClickListener(this);
        mRlOrderPrintSet.setOnClickListener(this);
        mRlOrderPrintControl.setOnClickListener(this);
        mRlReceptionPrint.setOnClickListener(this);
        mRlKitchenPrint.setOnClickListener(this);
    }

    public void initData(){
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_order_push_msg:
                setLinkToggle(isLinked);
                break;
            case R.id.rl_order_print_set:
                startActivity(new Intent(this, OrderPrintSetActivity.class));
                break;
            case R.id.rl_order_print_control:
                startActivity(new Intent(this, PrintOrderControlActivity.class));
                break;
            case R.id.rl_reception_print:
                boolean result1 = PermissionUtils.checkPermissionArray(PrintOrderSetActivity.this, permissionArray);
                if (!result1) {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_bluetooth));
                } else {
                    startBluetoothSetActivity(2);
                }
                /*Intent receptionIntent = new Intent(this, BluetoothSetActivity.class);
                receptionIntent.putExtra(Constants.INTENT_PRINT_DEVICE_TYPE,2);
                startActivity(receptionIntent);*/
                break;
            case R.id.rl_kitchen_print:
                boolean result2 = PermissionUtils.checkPermissionArray(PrintOrderSetActivity.this, permissionArray);
                if (!result2) {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_bluetooth));
                } else {
                    startBluetoothSetActivity(3);
                }
                /*Intent kitchenIntent = new Intent(this, BluetoothSetActivity.class);
                kitchenIntent.putExtra(Constants.INTENT_PRINT_DEVICE_TYPE,3);
                startActivity(kitchenIntent);*/
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    showDialog(getString(R.string.permission_set_content_bluetooth));
                } else {
                    startBluetoothSetActivity(2);
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    showDialog(getString(R.string.permission_set_content_bluetooth));
                } else {
                    startBluetoothSetActivity(3);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void setLinkToggle(boolean isChecked){
//        isChecked = !this.isLinked;
        isLinked = !isChecked;
        mIvOrderPushMsg.setImageResource(isLinked?R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }
}
