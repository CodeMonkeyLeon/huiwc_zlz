package com.hstypay.enterprise.activity.setting;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.BluetoothPopupWindow;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.PrintTypePopupWindow;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;

public class PrintPaySetActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvButton, mIvAutoSwitch;
    private TextView mButton, mTvTitle, mTvPrintType, mTvPrinterName;
    private RelativeLayout mRlPrintType, mRlSelecPrinter;
    private boolean mPrintTwo;
    private BluetoothPopupWindow mBluetoothPopupWindow;
    private SelectDialog selectDialog;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private String[] permissionArray = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_pay_set);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mIvButton = findViewById(R.id.iv_button);
        mButton = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.tv_pay_print_set);
        mButton.setText(R.string.tv_print_help);
        mIvButton.setVisibility(View.INVISIBLE);

        mIvAutoSwitch = findViewById(R.id.iv_auto_print_switch);
        mRlPrintType = findViewById(R.id.rl_print_type);
        mTvPrintType = findViewById(R.id.tv_print_type);
        mRlSelecPrinter = findViewById(R.id.rl_select_printer);
        mTvPrinterName = findViewById(R.id.tv_printer_name);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mRlPrintType.setOnClickListener(this);
        mRlSelecPrinter.setOnClickListener(this);
        mIvAutoSwitch.setOnClickListener(this);
    }

    public void initData() {
        mPrintTwo = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO);
        mTvPrintType.setText(mPrintTwo ? R.string.tv_print_type_two : R.string.tv_print_type_one);
        if (!TextUtils.isEmpty(MyApplication.getBluetoothName(1))) {
            mTvPrinterName.setText(MyApplication.getBluetoothName(1));
        } else {
            mTvPrinterName.setText("");
        }
        mIvAutoSwitch.setImageResource(MyApplication.autoPrint() ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                Intent intent = new Intent(PrintPaySetActivity.this, InstructionsActivity.class);
                intent.putExtra(Constants.INTENT_INSTRUCTION, Constants.URL_BLUETOOTH_INSTRUCTION);
                startActivity(intent);
                break;
            case R.id.rl_print_type:
                showPrintTypePopup();
                break;
            case R.id.rl_select_printer:
                boolean result1 = PermissionUtils.checkPermissionArray(PrintPaySetActivity.this, permissionArray);
                if (!result1) {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_bluetooth));
                } else {
                    setPayBluetooth();
                }
                break;
            case R.id.iv_auto_print_switch:
                if (MyApplication.autoPrint()) {
                    SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_AUTO_PRINT, false);
                    mIvAutoSwitch.setImageResource(R.mipmap.ic_switch_close);
                } else {
                    SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_AUTO_PRINT, true);
                    mIvAutoSwitch.setImageResource(R.mipmap.ic_switch_open);
                }
                break;
            default:
                break;
        }
    }

    private void setPayBluetooth(){
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {//说明手机不带蓝牙模块
            showCommonNoticeDialog(PrintPaySetActivity.this, getString(R.string.tx_blue_no_device), new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    PrintPaySetActivity.this.finish();
                }
            });
            return;
        }
        if (!adapter.isEnabled()) { //蓝牙没有打开
            openBlue(false);
        } else {
            showBluetoothPopup();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    showDialog(getString(R.string.permission_set_content_bluetooth));
                } else {
                    setPayBluetooth();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void showPrintTypePopup() {
        PrintTypePopupWindow popupWindow = new PrintTypePopupWindow(this, new PrintTypePopupWindow.OnSelectPrintTypeListener() {
            @Override
            public void onSelectPrintType() {
                mPrintTwo = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO);
                mTvPrintType.setText(mPrintTwo ? R.string.tv_print_type_two : R.string.tv_print_type_one);
            }
        });
        popupWindow.showAtLocation(mRlPrintType, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void showBluetoothPopup() {
        mBluetoothPopupWindow = new BluetoothPopupWindow(this, 1, new BluetoothPopupWindow.OnConnectPrinterListener() {
            @Override
            public void onConnectPrinter() {
                mTvPrinterName.setText(MyApplication.getBluetoothName(1));
            }
        });
        mBluetoothPopupWindow.showAtLocation(mRlPrintType, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                showBluetoothPopup();
            } else if (resultCode == RESULT_CANCELED) {
                showCommonNoticeDialog(PrintPaySetActivity.this, getString(R.string.tx_blue_open_fail), new CommonNoticeDialog.OnClickOkListener() {
                    @Override
                    public void onClickOk() {
                        PrintPaySetActivity.this.finish();
                    }
                });
            }
        }
    }

    private void openBlue(final boolean isFinish) {
        if (selectDialog == null) {
            selectDialog = new SelectDialog(PrintPaySetActivity.this, UIUtils.getString(R.string.tx_blue_no_open),
                    UIUtils.getString(R.string.btnCancel), UIUtils.getString(R.string.to_open), R.layout.select_common_dialog);
            selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    if (isFinish) {
                        finish();
                    }
                }
            });
            selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(mIntent, 1);
                }
            });
            DialogHelper.resize(PrintPaySetActivity.this, selectDialog);
        }
        selectDialog.show();
    }
}
