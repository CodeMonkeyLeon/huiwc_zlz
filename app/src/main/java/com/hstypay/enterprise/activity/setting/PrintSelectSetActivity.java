package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class PrintSelectSetActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mButton;
    private boolean isLinked = false;
    private boolean isAutoPrint = false;
    private RelativeLayout mRlPayPrintSet, mRlOrderPrintSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_select_set);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButton = findViewById(R.id.button_title);

        mRlPayPrintSet = (RelativeLayout) findViewById(R.id.rl_pay_print_set);
        mRlOrderPrintSet = (RelativeLayout) findViewById(R.id.rl_order_print_set);

        mButton.setVisibility(View.INVISIBLE);
        mTvTitle.setText(R.string.title_print_set);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlPayPrintSet.setOnClickListener(this);
        mRlOrderPrintSet.setOnClickListener(this);
    }

    public void initData() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button:
                startActivity(new Intent(PrintSelectSetActivity.this, PrintTypeActivity.class));
                break;
            case R.id.rl_pay_print_set:
                Intent payIntent = new Intent(this, PrintPaySetActivity.class);
                payIntent.putExtra(Constants.INTENT_PRINT_DEVICE_TYPE, 1);
                startActivity(payIntent);
                break;
            case R.id.rl_order_print_set:
                startActivity(new Intent(PrintSelectSetActivity.this, PrintOrderSetActivity.class));
                break;
            case R.id.btn_about_print:
                Intent intent = new Intent(PrintSelectSetActivity.this, InstructionsActivity.class);
                intent.putExtra(Constants.INTENT_INSTRUCTION, Constants.URL_BLUETOOTH_INSTRUCTION);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
