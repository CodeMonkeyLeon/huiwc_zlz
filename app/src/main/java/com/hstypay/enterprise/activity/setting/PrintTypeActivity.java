package com.hstypay.enterprise.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class PrintTypeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvTypeOne, mIvTypeTwo;
    private TextView mTvTitle, mButton;
    private RelativeLayout mRlTypeOne, mRlTypeTwo;
    private boolean printTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_type);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mTvTitle.setText(R.string.title_print_type_set);
        mButton.setVisibility(View.INVISIBLE);

        mRlTypeOne = (RelativeLayout) findViewById(R.id.rl_print_type_one);
        mRlTypeTwo = (RelativeLayout) findViewById(R.id.rl_print_type_two);
        mIvTypeOne = (ImageView) findViewById(R.id.iv_print_type_one);
        mIvTypeTwo = (ImageView) findViewById(R.id.iv_print_type_two);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlTypeOne.setOnClickListener(this);
        mRlTypeTwo.setOnClickListener(this);
    }

    public void initData() {
        setImageView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                PrintTypeActivity.this.finish();
                break;
            case R.id.rl_print_type_one:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO, false);
                setImageView();
                break;
            case R.id.rl_print_type_two:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO, true);
                setImageView();
                break;
            default:
                break;
        }
    }

    private void setImageView() {
        String metaData = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        boolean defValue = Constants.WJY_YINSHANG.equals(metaData) || Constants.WJY_WIZARPOS.equals(metaData);
        printTwo = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO, defValue);
        if (printTwo) {
            mIvTypeTwo.setImageResource(R.mipmap.ic_radio_selected);
            mIvTypeOne.setImageResource(R.mipmap.ic_radio_default);
        } else {
            mIvTypeOne.setImageResource(R.mipmap.ic_radio_selected);
            mIvTypeTwo.setImageResource(R.mipmap.ic_radio_default);
        }
    }
}
