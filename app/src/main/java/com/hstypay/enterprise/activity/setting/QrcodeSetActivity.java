package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.SettingActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.QrcodeSetDetailBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class QrcodeSetActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvQrcodeRemarkSwitch;
    private TextView mTvTitle, mBtnTitle;
    private EditTextDelete mEtQrcodeRemark;
    private Button mBtnSubmit;
    private SafeDialog mLoadDialog;
    private int mSwitch;
    private String mStoreId;
    private LinearLayout mLlContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_set);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnTitle.setVisibility(View.INVISIBLE);
        mTvTitle.setText(R.string.title_qrcode_set);

        mLlContent = findViewById(R.id.ll_content);
        mIvQrcodeRemarkSwitch = findViewById(R.id.iv_qrcode_remark_switch);
        mEtQrcodeRemark = findViewById(R.id.et_qrcode_remark);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        setButtonEnable(mBtnSubmit, true);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mIvQrcodeRemarkSwitch.setOnClickListener(this);
    }

    private void initData() {
        QrcodeSetDetailBean.DataEntity data = (QrcodeSetDetailBean.DataEntity) getIntent().getSerializableExtra("INTENT_QRCODE_SET_DETAIL");
        if (data != null) {
            mSwitch = data.getFixedQrCodeSwitchStatus();
            mIvQrcodeRemarkSwitch.setImageResource(data.getFixedQrCodeSwitchStatus() == 1 ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
//            mEtQrcodeRemark.setText(data.getPrompt() == null ? "" : data.getPrompt());
            mEtQrcodeRemark.setSelection(mEtQrcodeRemark.getText().length());
        }
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_QRCODE_SET")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(QrcodeSetActivity.this, getString(R.string.dialog_set_success), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                qrcodeSet();
                break;
            case R.id.iv_qrcode_remark_switch:
                if (mSwitch == 1) {
                    mIvQrcodeRemarkSwitch.setImageResource(R.mipmap.ic_switch_close);
                    mSwitch = 0;
                } else {
                    mIvQrcodeRemarkSwitch.setImageResource(R.mipmap.ic_switch_open);
                    mSwitch = 1;
                }
                break;
        }
    }

    private void qrcodeSet() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("fixedQrCodeStatus", mSwitch);
            ServerClient.newInstance(MyApplication.getContext()).fixedQRCodeSet(MyApplication.getContext(), "TAG_QRCODE_SET", map);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
