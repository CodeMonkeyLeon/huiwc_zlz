package com.hstypay.enterprise.activity.setting;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.ShortNameBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ShortNameActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvCheckStatus, mIvClean;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle, mTvNameTitle, mTvNotice, mTvReason;
    private EditText mEtName;
    private LinearLayout mLlNotice;
    private ShortNameBean.DataEntity mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_name);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvNameTitle = (TextView) findViewById(R.id.tv_name_title);
        mEtName = (EditText) findViewById(R.id.et_name);

        mLlNotice = (LinearLayout) findViewById(R.id.ll_merchant_notice);
        mIvCheckStatus = (ImageView) findViewById(R.id.iv_check_status);
        mIvClean = (ImageView) findViewById(R.id.iv_store_name_clean);
        mTvNotice = (TextView) findViewById(R.id.tv_merchant_notice);
        mTvReason = (TextView) findViewById(R.id.tv_merchant_reason);
        mButton.setVisibility(View.INVISIBLE);
        setButtonEnable(mBtnSubmit,false);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mIvClean.setOnClickListener(this);

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    mIvClean.setVisibility(View.GONE);
                    setButtonEnable(mBtnSubmit,false);
                } else {
                    mIvClean.setVisibility(View.VISIBLE);
                    setButtonEnable(mBtnSubmit,true);
                }
            }
        });
    }

    public void initData() {
        mData = (ShortNameBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_SHORT_NAME_DETAL);
        SpUtil.putInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE, mData.getMerchantClass());
        if (mData.getMerchantClass() == 1) {//企业
            mTvTitle.setText(UIUtils.getString(R.string.short_name));
            mTvNameTitle.setText(UIUtils.getString(R.string.short_name));
            mEtName.setHint(R.string.hint_short_name);
        } else if (mData.getMerchantClass() == 2) {//个人
            mTvTitle.setText(UIUtils.getString(R.string.shop_name));
            mTvNameTitle.setText(UIUtils.getString(R.string.shop_name));
            mEtName.setHint(R.string.hint_whole_name);
        }

        if (!StringUtils.isEmptyOrNull(mData.getMerchantShortName())) {
            mEtName.setText(mData.getMerchantShortName());
        }
        mEtName.setSelection(mEtName.length());

        if (mData.getExamineStatus() == 0) {
            mIvClean.setVisibility(View.GONE);
            mEtName.setEnabled(false);
            mEtName.setTextColor(UIUtils.getColor(R.color.tv_not_change));
            mIvCheckStatus.setImageResource(R.mipmap.ic_examine);
            mTvNotice.setText(UIUtils.getString(R.string.tx_checking));
            mTvReason.setVisibility(View.GONE);
            mBtnSubmit.setVisibility(View.GONE);
        } else if (mData.getExamineStatus() == 1) {
            mLlNotice.setVisibility(View.GONE);
        } else if (mData.getExamineStatus() == 2) {
            mIvCheckStatus.setImageResource(R.mipmap.ic_failed);
            mTvReason.setText(mData.getExamineRemark());
            mTvNotice.setText(UIUtils.getString(R.string.notice_failed_short_tx));
            mBtnSubmit.setText(UIUtils.getString(R.string.submit));
        }
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHANGE_SHORT_NAME)) {
            dismissLoading();
            Info info = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.GOODS_NAME_TAG);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(ShortNameActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(ShortNameActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_SHORT_NAME, mEtName.getText().toString().trim());
                    showCommonNoticeDialog(ShortNameActivity.this, getString(R.string.dialog_title_short_name), getString(R.string.dialog_content_short_name), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                submit();
                break;
            case R.id.iv_store_name_clean:
                mEtName.setText("");
                break;
            default:
                break;
        }
    }

    private void submit() {
        if (mEtName.getText().toString().trim().contains(" ")) {
            showCommonNoticeDialog(ShortNameActivity.this, getString(R.string.merchant_name));
            return;
        }

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_submitting));
            Map<String, Object> map = new HashMap<>();
            map.put("merchantName", mEtName.getText().toString().trim());
            ServerClient.newInstance(ShortNameActivity.this).changeShortName(ShortNameActivity.this, Constants.TAG_CHANGE_SHORT_NAME, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }
}
