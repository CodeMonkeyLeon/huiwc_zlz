package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.StoreSettingCountBean;
import com.hstypay.enterprise.fragment.VoiceSetReceiveFragment;
import com.hstypay.enterprise.fragment.VoiceSetDaoJiaFragment;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class VoiceSetActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mButton;
    private SafeDialog mLoadDialog;
    private VoiceSetReceiveFragment mVoiceSetReceiveFragment;
    private VoiceSetDaoJiaFragment mVoiceSetDaojiaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_set);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(R.string.voice_notice);
        mButton = findViewById(R.id.button_title);
        mButton.setVisibility(View.VISIBLE);
        mButton.setText(getResources().getString(R.string.voice_set_use_help));
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mLoadDialog = getLoadDialog(VoiceSetActivity.this, getString(R.string.public_loading), false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        mVoiceSetReceiveFragment = (VoiceSetReceiveFragment) Fragment.instantiate(this, VoiceSetReceiveFragment.class.getName());
        transaction.replace(R.id.fl_voice_set_collection, mVoiceSetReceiveFragment);
        mVoiceSetDaojiaFragment = (VoiceSetDaoJiaFragment) Fragment.instantiate(this, VoiceSetDaoJiaFragment.class.getName());
        transaction.replace(R.id.fl_voice_set_daojia, mVoiceSetDaojiaFragment);
        transaction.commitNow();
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
    }

    public void initData() {
        initPusherVoiceDevice();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                Intent intent = new Intent(VoiceSetActivity.this, InstructionsActivity.class);
                intent.putExtra(Constants.INTENT_INSTRUCTION, Constants.URL_VOICE_INSTRUCTION);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (Constants.GETUI_PUSH_VOICE_TAG.equals(event.getTag()) ) {//初始化语音推送设备的返回
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VoiceSetActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(VoiceSetActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    getStoreSettingCount();
                    break;
                default:
                    break;
            }
        }else if (Constants.GETUI_STORE_COUNT_TAG.equals(event.getTag())){//查询语音播报门店选择数量的返回
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreSettingCountBean countBean = (StoreSettingCountBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (countBean.getError() != null) {
                        if (countBean.getError().getCode() != null) {
                            if (countBean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (countBean.getError().getMessage() != null) {
                                    getLoginDialog(VoiceSetActivity.this, countBean.getError().getMessage());
                                }
                            } else {
                                if (countBean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(VoiceSetActivity.this, countBean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    StoreSettingCountBean.DataBean data = countBean.getData();
                    if (data!=null){
                        String voiceStoreCount = data.getVoice();
                        String voiceDaoJiaStoreCount = data.getVoiceDaoJia();
                        mVoiceSetReceiveFragment.setSelectStoreCountView(voiceStoreCount);
                        mVoiceSetDaojiaFragment.setSelectStoreCountView(voiceDaoJiaStoreCount);
                    }else {
                        MyToast.showToastShort(getString(R.string.data_error));
                    }
                    break;
                default:
                    break;
            }

        }
    }

    //获取语音播报门店选择数量
    private void getStoreSettingCount() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(VoiceSetActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (AppHelper.getAppType() == 1) {
                map.put("deviceType", Constants.REQUEST_CLIENT_POS);
            } else if (AppHelper.getAppType() == 2) {
                map.put("deviceType", Constants.REQUEST_CLIENT_APP);
            }
            String clientId = PushManager.getInstance().getClientid(MyApplication.getContext());
            map.put("deviceNo", clientId);
            ServerClient.newInstance(MyApplication.getContext()).getStoreSettingCount(MyApplication.getContext(), Constants.GETUI_STORE_COUNT_TAG, map);
        }
    }

    //初始化语音推送设备
    private void initPusherVoiceDevice() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(VoiceSetActivity.this, getString(R.string.network_exception));
        } else {
            String clientId = PushManager.getInstance().getClientid(MyApplication.getContext());
            if (clientId != null) {
                DialogUtil.safeShowDialog(mLoadDialog);
                SpUtil.putString(MyApplication.getContext(), Constants.GETUI_CLIENT_ID, clientId);
                Map<String, Object> map = new HashMap<>();
                if (AppHelper.getAppType() == 1) {
                    map.put("client", Constants.REQUEST_CLIENT_POS);
                } else if (AppHelper.getAppType() == 2) {
                    map.put("client", Constants.REQUEST_CLIENT_APP);
                }
                map.put("pushDeviceId", clientId);
                LogUtil.i("Jeremy", "login=client==" + clientId);
                ServerClient.newInstance(MyApplication.getContext()).getuiPushVoice(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_TAG, map);
            } else {
                showCommonNoticeDialog(VoiceSetActivity.this, getString(R.string.error_push_client));
            }
        }
    }

}
