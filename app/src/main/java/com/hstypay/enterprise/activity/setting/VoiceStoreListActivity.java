package com.hstypay.enterprise.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.VoiceShopAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.igexin.sdk.PushManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 语音播报门店列表页面
 * */
public class VoiceStoreListActivity extends BaseActivity implements View.OnClickListener, VoiceShopAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private TextView mButton, mTvTitle;
    private RecyclerView mRvVoiceShop;
    private List<VoiceBean.DataBean> mList = new ArrayList<>();

    private VoiceShopAdapter mAdapter;
    private int mPosition;
    private int mIntentType;//个推类型 1-收款语音推送；2-到家语音订单推送

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_notice);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mRvVoiceShop = (RecyclerView) findViewById(R.id.rv_voice_shop);
        mRvVoiceShop.setLayoutManager(new LinearLayoutManager(this));

        mTvTitle.setText(R.string.voice_notice);
        mButton.setText(R.string.ensure_check);
        mButton.setVisibility(View.INVISIBLE);

    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
    }

    public void initData() {
        mIntentType = getIntent().getIntExtra(Constants.INTENT_TYPE_SELECT_STORE_VOICE,-1);
        mAdapter = new VoiceShopAdapter(VoiceStoreListActivity.this, mList,mIntentType);
        mRvVoiceShop.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);

        getVoiceStoreList();
    }

    /**
     * 获取语音门店选择列表
     */
    private void getVoiceStoreList() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(this, getString(R.string.network_exception));
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            if (mIntentType == Constants.INTENT_VALUE_VOICE_COLLECTION ){
                //收款语音推送列表
                if (AppHelper.getAppType() == 1) {
                    map.put("client", Constants.REQUEST_CLIENT_POS);
                } else if (AppHelper.getAppType() == 2) {
                    map.put("client", Constants.REQUEST_CLIENT_APP);
                }

                String clientId = PushManager.getInstance().getClientid(getApplicationContext());
                if (!StringUtils.isEmptyOrNull(clientId)) {
                    map.put("pushDeviceId", clientId);
                }
                ServerClient.newInstance(MyApplication.getContext()).getuiPushVoiceList(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_LIST_TAG, map);
            }else if (mIntentType == Constants.INTENT_VALUE_VOICE_ORDER_DAOJIA ){
                // 到家语音订单推送列表
                if (AppHelper.getAppType() == 1) {
                    map.put("deviceType", Constants.REQUEST_CLIENT_POS);
                } else if (AppHelper.getAppType() == 2) {
                    map.put("deviceType", Constants.REQUEST_CLIENT_APP);
                }

                String clientId = PushManager.getInstance().getClientid(getApplicationContext());
                if (!StringUtils.isEmptyOrNull(clientId)) {
                    map.put("deviceNo", clientId);
                }
                map.put("pushType","2");//2-到家语音订单个推
                ServerClient.newInstance(MyApplication.getContext()).getVoicePushStoreList(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_STORESETTINGLIST_TAG, map);
            }else {
                dismissLoading();
            }

        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //推送语音播报门店列表的返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStoreList(NoticeEvent event) {
        if (event.getTag().equals(Constants.GETUI_PUSH_VOICE_LIST_TAG)||event.getTag().equals(Constants.GETUI_PUSH_VOICE_STORESETTINGLIST_TAG)) {//收款语音播报的门店列表
            dismissLoading();
            VoiceBean msg = (VoiceBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<VoiceBean.DataBean> dataBeanList = msg.getData();
                    mList.clear();
                    mList.addAll(dataBeanList);
                    mAdapter.notifyDataSetChanged();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        mPosition = position;
        if (mList.get(position).getPushClose() == 1) {
            pushVoiceSetting(0);
        }else {
            pushVoiceSetting(1);
        }
    }

    /**
     * 开启或关闭选择的门店的推送语音
     * @param pushClose 0关闭，1开启
     */
    private void pushVoiceSetting(int pushClose) {
        if (!NetworkUtils.isNetworkAvailable(VoiceStoreListActivity.this)) {
            showCommonNoticeDialog(VoiceStoreListActivity.this, getString(R.string.network_exception));
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            String storeMerchantId = mList.get(mPosition).getStoreMerchantId();
            map.put("storeMerchantId", !StringUtils.isEmptyOrNull(storeMerchantId)?storeMerchantId:"");

            if (mIntentType == Constants.INTENT_VALUE_VOICE_COLLECTION ){
                //收款语音播报门店开关设置
                map.put("pushClose", pushClose);
                if (AppHelper.getAppType() == 1) {
                    map.put("client", Constants.REQUEST_CLIENT_POS);
                } else if (AppHelper.getAppType() == 2) {
                    map.put("client", Constants.REQUEST_CLIENT_APP);
                }
                String clientid = PushManager.getInstance().getClientid(this.getApplicationContext());
                map.put("pushDeviceId", !StringUtils.isEmptyOrNull(clientid)?clientid:"");
                ServerClient.newInstance(VoiceStoreListActivity.this).getuiPushVoiceSet(VoiceStoreListActivity.this, Constants.GETUI_PUSH_VOICE_SET_TAG, map);
            }else if (mIntentType == Constants.INTENT_VALUE_VOICE_ORDER_DAOJIA ){
                //到家订单语音播报门店开关设置
                map.put("id",mList.get(mPosition).getId());//上一个接口返回的参数id
                String clientid = PushManager.getInstance().getClientid(this.getApplicationContext());
                map.put("deviceNo", !StringUtils.isEmptyOrNull(clientid)?clientid:"");
                if (AppHelper.getAppType() == 1) {
                    map.put("deviceType", Constants.REQUEST_CLIENT_POS);
                } else if (AppHelper.getAppType() == 2) {
                    map.put("deviceType", Constants.REQUEST_CLIENT_APP);
                }
                map.put("pushType",2);//个推类型；1-收款个推；2-到家语音订单个推
                ServerClient.newInstance(VoiceStoreListActivity.this).pushUserSwitch(VoiceStoreListActivity.this, Constants.GETUI_PUSH_VOICE_PUSHUSERSWITCH_TAG, map);
            }else {
                dismissLoading();
            }

        }
    }
    //开启或关闭选择门店的推送语音设置
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPushVoiceSet(NoticeEvent event) {
        if (event.getTag().equals(Constants.GETUI_PUSH_VOICE_SET_TAG)||event.getTag().equals(Constants.GETUI_PUSH_VOICE_PUSHUSERSWITCH_TAG)) {
            Info msg = (Info) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.GETUI_PUSH_VOICE_SET_TAG);
                    break;
                case Constants.ADD_CASHIER_MANAGE_FALSE:
                    dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VoiceStoreListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(VoiceStoreListActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ADD_CASHIER_MANAGE_TRUE:
                    if (mList.get(mPosition).getPushClose() == 1) {
                        mList.get(mPosition).setPushClose(0);
                    } else {
                        mList.get(mPosition).setPushClose(1);
                    }
                    mAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        if (mList.size()==0){
            //门店列表获取失败，则不要改变上个页面选择门店的个数
            intent.putExtra(Constants.INTENT_VOICE_SELECT_STORE_COUNT,Constants.SELECT_STORE_COUNT_DEFAULT);
        }else {
            intent.putExtra(Constants.INTENT_VOICE_SELECT_STORE_COUNT,getStoreCount(mList));
        }
        setResult(RESULT_OK, intent);
        super.finish();
    }

    /**
     * 计算选中的门店个数
     * */
    private int getStoreCount(List<VoiceBean.DataBean> data) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getPushClose() == 1) {
                count++;
            }
        }
        return count;
    }
}
