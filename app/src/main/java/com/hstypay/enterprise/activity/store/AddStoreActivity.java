package com.hstypay.enterprise.activity.store;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.activity.StoreGroupListActivity;
import com.hstypay.enterprise.activity.StoreManagerListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ImageBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LocationBean;
import com.hstypay.enterprise.bean.ManagerListBean;
import com.hstypay.enterprise.bean.StoreDetailBean;
import com.hstypay.enterprise.bean.StoreGroupBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.FileUtils;
import com.hstypay.enterprise.utils.ImageFactory;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LocationUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.qiezzi.choseviewlibrary.ChoseCityPicker;
import com.qiezzi.choseviewlibrary.bean.AddressBean;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/24 10:09
 * @描述: 添加门店、编辑门店
 */
public class AddStoreActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvNameClean, mIvLocation, mIvAddressClean;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle, mTvStoreCity;
    private boolean isChecked = false;
    private EditText mEtStoreName, mEtStoreAddress;//详细地址
    private RelativeLayout mRlArea;

    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    public static final int REQUEST_CODE_SELECT_GROUP = 0x1003;
    public static final int REQUEST_CODE_SELECT_MANAGER = 0x1004;

    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private String[] permissionLocationArray = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE3 = 103;
    private File tempFile;
    private Uri originalUri;
    private String picFourPath, imageUrl;
    private boolean takepic = false;
    private String picFourPath1;
    private RelativeLayout ly_phone_four;
    private LinearLayout ly_four;
    private SelectDialog mDialog;
    private ChoseCityPicker mCityPicker;
    private StoreDetailBean.DataEntity mData = new StoreDetailBean.DataEntity();
//    private StoreDetailBean.DataEntity mStoreDetailData;
    private ImageView mIvPictureHeadStore;
    private RelativeLayout mRlSelectGroup;//rl所属分组
    private TextView mTvBelongGroup;//tv所属分组
    private String selectedGroupId = "";//选中的分组id
    private RelativeLayout mRlSelectStoreManager;//rl所属店长
    private TextView mTvStoreManager;//tv所属店长
    private String selectedStoreManagerId = "";//选中的店长id
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_store);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        mIntentName = intent.getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_NAME_EDIT_STORE_DETAIL.equals(mIntentName)){
            //是门店编辑
            mTvTitle.setText(R.string.title_edit_store);
            mData = (StoreDetailBean.DataEntity) intent.getSerializableExtra(Constants.RESULT_SHOP_BEAN_INTENT);
//            mData.setCompanyPhoto(mStoreDetailData.getCompanyPhoto());
//            mData.setProvince(mStoreDetailData.getProvince());
//            mData.setCity(mStoreDetailData.getCity());
//            mData.setCounty(mStoreDetailData.getCounty());
//            mData.setStoreId(mStoreDetailData.getStoreId());

            mEtStoreName.setText(mData.getStoreName());
            mTvStoreCity.setText(mData.getProvinceCnt() + " " + mData.getCityCnt() + " " + mData.getCountyCnt());
            mEtStoreAddress.setText(mData.getAddress());
            mTvBelongGroup.setText(mData.getGroupName());
            selectedGroupId = mData.getStoreGroup();
            mTvStoreManager.setText(mData.getUserIdCnt());
            selectedStoreManagerId = mData.getUserId();
            if (!StringUtils.isEmptyOrNull(mData.getCompanyPhoto())) {
                mIvPictureHeadStore.setVisibility(View.VISIBLE);
                Picasso.get().load(Constants.H5_BASE_URL + mData.getCompanyPhoto()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mIvPictureHeadStore);
            }
        }
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_add_store);
        mButton.setVisibility(View.INVISIBLE);

        mBtnSubmit = (Button) findViewById(R.id.btn_save);
        mEtStoreName = (EditText) findViewById(R.id.et_store_name);
        mEtStoreAddress = (EditText) findViewById(R.id.et_store_address);
        mTvStoreCity = (TextView) findViewById(R.id.tv_store_city_content);
        mIvNameClean = (ImageView) findViewById(R.id.iv_store_name_clean);
        mIvLocation = (ImageView) findViewById(R.id.iv_store_city_location);
        mIvAddressClean = (ImageView) findViewById(R.id.iv_store_address_clean);
        mRlArea = (RelativeLayout) findViewById(R.id.rl_area);
        mRlSelectGroup = findViewById(R.id.rl_select_group);
        mTvBelongGroup = findViewById(R.id.tv_belong_group);
        mRlSelectStoreManager = findViewById(R.id.rl_select_store_manager);
        mTvStoreManager = findViewById(R.id.tv_store_manager);

        ly_phone_four = (RelativeLayout) findViewById(R.id.ly_phone_four);
        ly_four = (LinearLayout) findViewById(R.id.ll_preview_photo_store);
        mIvPictureHeadStore = findViewById(R.id.iv_picture_head_store);
        setButtonEnable(mBtnSubmit,false);
    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mIvNameClean.setOnClickListener(this);
        mIvLocation.setOnClickListener(this);
        mRlArea.setOnClickListener(this);
        mIvAddressClean.setOnClickListener(this);
        ly_phone_four.setOnClickListener(this);
        mRlSelectGroup.setOnClickListener(this);
        mRlSelectStoreManager.setOnClickListener(this);

        setEdittextListener(mEtStoreName, mIvNameClean);
        setEdittextListener(mEtStoreAddress, mIvAddressClean);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_select_store_manager:
                //选择所属店长
                selectStoreManager();
                break;
            case R.id.rl_select_group:
                //选择所属分组
                selectStoreGroup();
                break;
            case R.id.iv_store_name_clean:
                mEtStoreName.setText("");
                break;
            case R.id.iv_store_address_clean:
                mEtStoreAddress.setText("");
                break;
            case R.id.iv_store_city_location:
                boolean results = PermissionUtils.checkPermissionArray(AddStoreActivity.this, permissionLocationArray);
                if (results) {
                    location();
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE3, permissionLocationArray, getString(R.string.permission_content_location));
                }
                break;
            case R.id.rl_area:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    loadDialog(AddStoreActivity.this, "加载中...");
                    ServerClient.newInstance(MyApplication.getContext()).getAddress(MyApplication.getContext(), Constants.TAG_GET_ADDRESS, null);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.ly_phone_four://拍照
                choice();
                break;
            case R.id.btn_save://保存
                submit();
                break;
            default:
                break;
        }
    }

    //选择所属店长
    private void selectStoreManager() {
        Intent intentGroup = new Intent(this, StoreManagerListActivity.class);
        intentGroup.putExtra(Constants.INTENT_STORE_USER_ID,selectedStoreManagerId);
        intentGroup.putExtra(Constants.INTENT_NAME,Constants.INTENT_VALUE_ADD_STORE);
        startActivityForResult(intentGroup,REQUEST_CODE_SELECT_MANAGER);
    }

    //选择分组
    private void selectStoreGroup() {
        Intent intentGroup = new Intent(this, StoreGroupListActivity.class);
        intentGroup.putExtra(Constants.INTENT_GROUP_ID,selectedGroupId);
        intentGroup.putExtra(Constants.INTENT_NAME,Constants.INTENT_VALUE_ADD_STORE);
        startActivityForResult(intentGroup,REQUEST_CODE_SELECT_GROUP);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE3:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    location();
                } else {
                    showDialog(getString(R.string.permission_set_content_location));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(AddStoreActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    boolean results = PermissionUtils.checkPermissionArray(AddStoreActivity.this, permissionArray);
                    if (results) {
                        startCamera();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_store_photo));
                    }
                } else {
                    ToastHelper.showInfo(AddStoreActivity.this, ToastHelper.toStr(R.string.tx_sd_pic));
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    boolean results = PermissionUtils.checkPermissionArray(AddStoreActivity.this, permissionArray);
                    if (results) {
                        takeImg();
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_store_photo));
                    }
                } else {
                    showCommonNoticeDialog(AddStoreActivity.this, getString(R.string.tx_sd_pic));
                }
            }
        });
        picPopupWindow.showAtLocation(mTvTitle, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";

            tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()){
                tempFile.getParentFile().mkdirs();
            }
            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(AddStoreActivity.this,
                        Constants.FILE_PROVIDER, tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UploadImage() {
        if (!NetworkUtils.isNetworkAvailable(AddStoreActivity.this)) {
            showCommonNoticeDialog(AddStoreActivity.this, getString(R.string.network_exception));
            return;
        }
        showNewLoading(true, getString(R.string.public_uploading));

        String url = Constants.BASE_URL + "merchant/upload/file";
        PostFormBuilder post = OkHttpUtils.post();
        if (!TextUtils.isEmpty(picFourPath)) {
//            picFourPath1 = picFourPath.substring(0, picFourPath.lastIndexOf(".")) + "(1).jpg";
            picFourPath1 = AppHelper.getImageCacheDir(picFourPath);
            ImageFactory.compressPicture(picFourPath, picFourPath1);
            post = post.addFile("storeImg", "storeImg", new File(picFourPath1));
        }
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                dismissLoading();
                MyToast.showToastShort("上传失败");
                FileUtils.deleteFile(picFourPath1);
            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(picFourPath1);
                dismissLoading();
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null) {
                    if (imageBean.isStatus()) {
                        ImageBean.DataEntity imageInfo = imageBean.getData();
                        mData.setCompanyPhoto(imageInfo.getStoreImg());
                        setButtonView();
                    } else {
                        if (imageBean.getError() != null && imageBean.getError().getMessage() != null) {
                            MyToast.showToastShort(imageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void submit() {

        if (TextUtils.isEmpty(mTvStoreCity.getText().toString().trim())) {
            MyToast.showToastShort(UIUtils.getString(R.string.regex_store_area));
            return;
        }

        if (TextUtils.isEmpty(mEtStoreAddress.getText().toString().trim())) {
            MyToast.showToastShort(UIUtils.getString(R.string.regex_store_address));
            mEtStoreAddress.setFocusable(true);
            mEtStoreAddress.setFocusableInTouchMode(true);
            mEtStoreAddress.requestFocus();
            return;
        }

        if (!NetworkUtils.isNetworkAvailable(AddStoreActivity.this)) {
            showCommonNoticeDialog(AddStoreActivity.this, getString(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("companyPhoto", mData.getCompanyPhoto());
            map.put("storeName", mEtStoreName.getText().toString().trim());
            map.put("province", mData.getProvince());//实际是provinceCode
            map.put("city", mData.getCity());
            map.put("county", mData.getCounty());
            map.put("address", mEtStoreAddress.getText().toString().trim());
            map.put("storeGroup", TextUtils.isEmpty(selectedGroupId)?0:selectedGroupId);
            map.put("userId", selectedStoreManagerId);

            showNewLoading(true, getString(R.string.public_loading));
            if (Constants.INTENT_NAME_EDIT_STORE_DETAIL.equals(mIntentName)){
                //是编辑门店信息
                map.put("storeId", mData.getStoreId());
                ServerClient.newInstance(AddStoreActivity.this).editStore(AddStoreActivity.this, Constants.TAG_STORE_EDIT, map);

            }else {
                //是新增门店
                ServerClient.newInstance(AddStoreActivity.this).addStore(AddStoreActivity.this, Constants.TAG_ADD_STORE, map);
            }
        }
    }

    //Eventbus接收数据，退款权限返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_ADD_STORE) ||event.getTag().equals(Constants.TAG_STORE_EDIT)) {
            dismissLoading();
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    LogUtil.i("zhouwei", getString(R.string.net_error) + Constants.TAG_CASHIER_EDIT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddStoreActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(AddStoreActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (event.getTag().equals(Constants.TAG_ADD_STORE) ){
                        //添加门店
                        showCommonNoticeDialog(AddStoreActivity.this, getString(R.string.operation_success), new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                //添加成功
                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                    }else if (event.getTag().equals(Constants.TAG_STORE_EDIT)){
                        //编辑门店信息  ?要不要更新门店ID
                        showCommonNoticeDialog(AddStoreActivity.this, getString(R.string.operation_success), new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                //修改成功
//                                mData = (StoreDetailBean.DataEntity) data.getSerializableExtra(Constants.RESULT_SHOP_BEAN_INTENT);
                                mData.setStoreName(mEtStoreName.getText().toString().trim());
                                mData.setAddress(mEtStoreAddress.getText().toString().trim());
                                if (!TextUtils.isEmpty(selectedGroupId)){
                                    mData.setStoreGroup(selectedGroupId);
                                    mData.setGroupName(mTvBelongGroup.getText().toString().trim());
                                }
                                if (!TextUtils.isEmpty(selectedStoreManagerId)){
                                    mData.setUserId(selectedStoreManagerId);
                                    mData.setUserIdCnt(mTvStoreManager.getText().toString().trim());
                                }
                                Intent intentData = new Intent();
                                intentData.putExtra(Constants.RESULT_SHOP_BEAN_INTENT,mData);
                                setResult(RESULT_OK,intentData);
                                finish();
                            }
                        });
                    }

                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_ADDRESS)) {
            AddressBean msg = (AddressBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_ADDRESS_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddStoreActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_ADDRESS_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        area(msg);
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_QUEST_LOCATION)) {
            LocationBean msg = (LocationBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.QUEST_LOCATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(AddStoreActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUEST_LOCATION_TRUE://请求成功
                    if (msg.getData() != null) {
                        mTvStoreCity.setText(msg.getData().getProvince() + " " + msg.getData().getCity() + " " + msg.getData().getDistrict());
                        mData.setProvince(msg.getData().getProvinceCode());
                        mData.setCity(msg.getData().getCitycode());
                        mData.setCounty(msg.getData().getAdcode());
                        mData.setProvinceCnt(msg.getData().getProvince());
                        mData.setCityCnt(msg.getData().getCity());
                        mData.setCountyCnt(msg.getData().getDistrict());
                        setButtonView();
                    }
                    break;
            }

            dismissLoading();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void area(AddressBean addressBean) {
        mCityPicker = new ChoseCityPicker(AddStoreActivity.this, addressBean);
        mCityPicker.setOnGetAddress(new ChoseCityPicker.OnGetAddress() {
            @Override
            public void getAddress(String province, String city, String area) {
                //获取省市区地址
                mTvStoreCity.setText(province + " " + city + " " + area);
                setButtonView();
                mData.setProvinceCnt(province);
                mData.setCityCnt(city);
                mData.setCountyCnt(area);
            }
        });
        mCityPicker.setOnGetAddressCode(new ChoseCityPicker.OnGetAddressCode() {
            @Override
            public void getAddressCode(String province, String city, String area) {
                //获取省市区code
                mData.setProvince(province);
                mData.setCity(city);
                mData.setCounty(area);
            }
        });
        mCityPicker.show();
    }

    private void location() {
        LocationUtil.getInstance().getCNBylocation(this);
        String lng = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOCATION_LONGITUDE);
        String lat = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOCATION_LATITUDE);
        if (!TextUtils.isEmpty(lng) && !TextUtils.isEmpty(lat)) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                showNewLoading(true, getString(R.string.location_area));
                Map<String, Object> map = new HashMap();
                map.put("longitude", lng);
                map.put("dimensionality", lat);
                ServerClient.newInstance(MyApplication.getContext()).questLocation(MyApplication.getContext(), Constants.TAG_QUEST_LOCATION, map);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }else {
            showCommonNoticeDialog(AddStoreActivity.this,getString(R.string.location_exception));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SELECT_GROUP || requestCode == REQUEST_CODE_SELECT_MANAGER){
            //所属分组、店长
            //mBundle.putSerializable(Constants.RESULT_STORE_GROUP_BEAN_INTENT, dataBean);   //传递一个user对象列表
            if (resultCode == RESULT_OK){
                Bundle bundle = data.getExtras();
                if (requestCode == REQUEST_CODE_SELECT_GROUP){
                    StoreGroupBean.StoreGroupItemData storeGroupItemData = (StoreGroupBean.StoreGroupItemData) bundle.getSerializable(Constants.RESULT_STORE_GROUP_BEAN_INTENT);
                    if (storeGroupItemData!=null){
                        mTvBelongGroup.setText(storeGroupItemData.getGroupName());
                        selectedGroupId = storeGroupItemData.getGroupId();
                    }else {
                        mTvBelongGroup.setText("");
                        selectedGroupId = "";
                    }

                }else if (requestCode == REQUEST_CODE_SELECT_MANAGER){
                    ManagerListBean.DataEntity.itemData itemData = (ManagerListBean.DataEntity.itemData) bundle.getSerializable(Constants.STORE_MANAGER_BEAN_INTENT);
                    if (itemData!=null){
                        mTvStoreManager.setText(itemData.getEmpName());
                        selectedStoreManagerId = itemData.getUserId();
                    }else {
                        mTvStoreManager.setText("");
                        selectedStoreManagerId = "";
                    }
                }

            }
        }else {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    //获取相册图片
                    case REQUEST_CODE_TAKE_PICTURE:
                        if (data.getData() == null) {
                            return;
                        }
                        String path = AppHelper.getPicPath(data.getData());
                        Bitmap bitmap = null;
                        if (path != null) {
                            takepic = false;
                            bitmap = ImagePase.readBitmapFromStream(path);
                            if (bitmap != null) {
                                picFourPath = path;
                                ly_four.setVisibility(View.GONE);
                                mIvPictureHeadStore.setVisibility(View.GONE);
                                ly_phone_four.setBackground(new BitmapDrawable(bitmap));
                                UploadImage();
                            }
                        }
                        break;

                    // 拍照
                    case REQUEST_CODE_CAPTURE_PICTURE:
                        String pathPhoto = AppHelper.getPicPath(originalUri);
                        takepic = true;
                        Bitmap bitmap_pci;
                        File file = new File(pathPhoto);
                        if (!file.exists()) {
                            File f = new File(imageUrl);
                            if (f.exists()) {
                                pathPhoto = imageUrl;
                            }
                        }
                        bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                        if (bitmap_pci != null) {
                            picFourPath = pathPhoto;
                            ly_four.setVisibility(View.GONE);
                            mIvPictureHeadStore.setVisibility(View.GONE);
                            ly_phone_four.setBackground(new BitmapDrawable(bitmap_pci));
                            UploadImage();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

    }

    private void setEdittextListener(final EditText editText, final ImageView imageView) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }
                setButtonView();
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点
                    if (editText.getText().length() > 0) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void setButtonView() {
        if (TextUtils.isEmpty(mEtStoreName.getText().toString().trim())
                || TextUtils.isEmpty(mEtStoreAddress.getText().toString().trim())
                || TextUtils.isEmpty(mTvStoreCity.getText().toString())
                || TextUtils.isEmpty(mData.getCompanyPhoto())) {
            setButtonEnable(mBtnSubmit,false);
        } else {
            setButtonEnable(mBtnSubmit,true);
        }
    }
}
