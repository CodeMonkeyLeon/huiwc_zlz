package com.hstypay.enterprise.activity.store;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author kuangzeyu
 * @time 2021/2/27
 * @desc 门店详情
 */
public class StoreDetailActivity2 extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvStoreNameDetail;//门店名称
    private TextView mTvStoreManagerDetail;//所属店长
    private TextView mTvStoreGroupDetail;//所属分组
    private ImageView iv_picture_head_store;//店铺照
    private RelativeLayout mRlEmployeeNumDetail;//rl员工数量
    private TextView mTvEmployeeNumDetail;//员工数量
    private TextView mTvTitleFreezeStore;//冻结tv
    private ImageView mIvFreezeStoreDetail;//冻结iv
    private TextView mTvStoreCity;//所在地
    private TextView mTvStoreDetailAddress;//详细地址
    private StoreDetailBean.DataEntity mData;
    private boolean isChecked = false;//冻结状态
    private SelectDialog mDialogInfo;
    private TextView mButtonTitle;
    private static final int REQUESTCODE_EDIT_STORE = 100;//编辑门店
    private static final int REQUESTCODE_EMPLOYEE = 101;//员工管理
    private String mStoreId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_detail2);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        mData = (StoreDetailBean.DataEntity) getIntent().getSerializableExtra(Constants.RESULT_SHOP_BEAN_INTENT);
        initView();
        initData();
        initListener();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvFreezeStoreDetail.setOnClickListener(this);
        mButtonTitle.setOnClickListener(this);
        mRlEmployeeNumDetail.setOnClickListener(this);
    }

    private void initData() {
        if (mData != null) {
            mStoreId = mData.getStoreId();
            mTvStoreNameDetail.setText(mData.getStoreName());
            mTvStoreCity.setText(mData.getProvinceCnt() + " " + mData.getCityCnt() + " " + mData.getCountyCnt());
            mTvStoreDetailAddress.setText(mData.getAddress());
            mTvStoreManagerDetail.setText(mData.getUserIdCnt());
            mTvStoreGroupDetail.setText(mData.getGroupName());
            mTvEmployeeNumDetail.setText(mData.getEmpTotal()+"名");
            if (!StringUtils.isEmptyOrNull(mData.getCompanyPhoto())) {
                Picasso.get().load(Constants.H5_BASE_URL + mData.getCompanyPhoto()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(iv_picture_head_store);
            }

            if (mData.getEditEnable() == 0) {
                //mLlFreeze.setVisibility(View.GONE);
                mIvFreezeStoreDetail.setEnabled(false);
                mTvTitleFreezeStore.setTextColor(UIUtils.getColor(R.color.tv_not_change));
            } else {
                //mLlFreeze.setVisibility(View.VISIBLE);
                mIvFreezeStoreDetail.setEnabled(true);
                mTvTitleFreezeStore.setTextColor(UIUtils.getColor(R.color.home_text));
                if (mData.getActivateStatus() == 2) {
                    isChecked = true;
                    mIvFreezeStoreDetail.setImageResource(R.mipmap.ic_switch_open);
                } else {
                    isChecked = false;
                    mIvFreezeStoreDetail.setImageResource(R.mipmap.ic_switch_close);
                }
            }

        }
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        TextView mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText("门店详情");
        mButtonTitle = findViewById(R.id.button_title);
        mButtonTitle.setText("编辑");

        mTvStoreNameDetail = findViewById(R.id.tv_store_name_detail);
        mTvStoreManagerDetail = findViewById(R.id.tv_store_manager_detail);
        mTvStoreGroupDetail = findViewById(R.id.tv_store_group_detail);
        mTvStoreCity = findViewById(R.id.tv_store_city);
        mTvStoreDetailAddress = findViewById(R.id.tv_store_detail_address);
        iv_picture_head_store = findViewById(R.id.iv_picture_head_store);
        mTvEmployeeNumDetail = findViewById(R.id.tv_employee_num_detail);
        mTvTitleFreezeStore = findViewById(R.id.tv_title_freeze_store);
        mIvFreezeStoreDetail = findViewById(R.id.iv_freeze_store_detail);
        mRlEmployeeNumDetail = findViewById(R.id.rl_employee_num_detail);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_freeze_store_detail://冻结
                showSetDialog(StoreDetailActivity2.this);
                setFreezeSwitch();
                break;
            case R.id.button_title://编辑门店信息
                Intent intent  = new Intent(StoreDetailActivity2.this,AddStoreActivity.class);
                intent.putExtra(Constants.INTENT_NAME,Constants.INTENT_NAME_EDIT_STORE_DETAIL);
                intent.putExtra(Constants.RESULT_SHOP_BEAN_INTENT,mData);
                startActivityForResult(intent,REQUESTCODE_EDIT_STORE);
                break;
            case R.id.rl_employee_num_detail://员工管理列表
                Intent intentEmployee  = new Intent(StoreDetailActivity2.this,StoreEmployeeActivity.class);
                intentEmployee.putExtra(Constants.INTENT_DETAIL_STORE_ID,mData.getStoreId());
                intentEmployee.putExtra(Constants.INTENT_DETAIL_STORE_NAME,mData.getStoreName());
                startActivityForResult(intentEmployee,REQUESTCODE_EMPLOYEE);
                break;
        }
    }


    //设置冻结的弹窗选择
    public void showSetDialog(final Activity context) {
        mDialogInfo = new SelectDialog(context, getString(R.string.show_freeze), getString(R.string.btnCancel), getString(R.string.btnOk), R.layout.select_common_dialog);
        mDialogInfo.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                setFreeze(mData.getStoreId(), isChecked);
            }
        });
        mDialogInfo.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                setFreezeSwitch();
            }
        });
        DialogHelper.resize(context, mDialogInfo);
        mDialogInfo.show();
    }

    //冻结
    private void setFreeze(String storeId, boolean isChecked) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(this, getString(R.string.network_exception));
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("storeId", storeId);
            if (isChecked) {
                map.put("activateStatus", 2);
            } else {
                map.put("activateStatus", 1);
            }
            ServerClient.newInstance(this).storeFreeze(this, Constants.TAG_STORE_FREEZE, map);
        }
    }

    private void setFreezeSwitch() {
        if (isChecked) {
            isChecked = false;
            mIvFreezeStoreDetail.setImageResource(R.mipmap.ic_switch_close);
            mData.setActivateStatus(1);
        } else {
            isChecked = true;
            mIvFreezeStoreDetail.setImageResource(R.mipmap.ic_switch_open);
            mData.setActivateStatus(2);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUESTCODE_EDIT_STORE || requestCode == REQUESTCODE_EMPLOYEE){
            //编辑门店、员工后返回
            if (resultCode == RESULT_OK){
                setResult(RESULT_OK);
//                mData = (StoreDetailBean.DataEntity) data.getSerializableExtra(Constants.RESULT_SHOP_BEAN_INTENT);
//                initData();
                getStoreDetailData();
            }
        }
    }

    //更新门店详情信息
    private void getStoreDetailData(){
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String,Object> map = new HashMap<>();
            map.put("storeId",mStoreId);
            ServerClient.newInstance(MyApplication.getContext()).storeDetail(MyApplication.getContext(), Constants.TAG_STORE_EMPLOYEE_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetStoreDetailData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_STORE_EMPLOYEE_DETAIL)) {
            dismissLoading();
            StoreDetailBean msg = (StoreDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreDetailActivity2.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mData = msg.getData();
                        initData();
                    }else {
                        MyToast.showToastShort(UIUtils.getString(R.string.data_error));
                    }
                    break;
            }
        }
    }
}
