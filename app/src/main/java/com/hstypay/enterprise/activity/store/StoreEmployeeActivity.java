package com.hstypay.enterprise.activity.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.cashier.EmployeeInfoActivity;
import com.hstypay.enterprise.activity.employee.AddEmployeeActivity;
import com.hstypay.enterprise.activity.employee.EmployeeManagerActivity;
import com.hstypay.enterprise.activity.manager.ManagerDetailActivity;
import com.hstypay.enterprise.activity.receiveDevice.ReceiveDeviceChoiceActivity;
import com.hstypay.enterprise.activity.receiveDevice.ReceiveDeviceSetActivity;
import com.hstypay.enterprise.adapter.StoreEmployeeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierDetailBean;
import com.hstypay.enterprise.bean.EmployeeManagerBean;
import com.hstypay.enterprise.bean.LinkEmployeeBean;
import com.hstypay.enterprise.bean.ManagerDetailBean;
import com.hstypay.enterprise.bean.StoreEmployeeBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 门店的员工管理列表页
 */
public class StoreEmployeeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack,mIvButton;
    private EditTextDelete mEt_input;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerviewEmployeeStore;
    private StoreEmployeeAdapter mStoreEmployeeAdapter;
    private String mStoreID;
    private SafeDialog mLoadDialog;
    List<StoreEmployeeBean.Employee> employees =  new ArrayList<>();
    private final int REQUEST_CODE_DETAIL_EMPLOYEE = 0x1002;//收银员详情
    private final int REQUEST_CODE_DETAIL_STOREMANAGER = 0x1003;//店长详情
    private final int REQUEST_CODE_ADD_EMPLOYEE = 0x1004;//新增员工

    private String mStoreName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_employee);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvButton.setOnClickListener(this);

        mEt_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchData();
                    return true;
                }
                return false;
            }
        });
        mEt_input.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    mStoreEmployeeAdapter.setDatas(employees);
                }
            }
        });

        mStoreEmployeeAdapter.setOnRecycleViewItemClickListener(new StoreEmployeeAdapter.OnRecycleViewItemClickListener() {
            @Override
            public void onItemClick(StoreEmployeeBean.Employee employee) {
                int role = employee.getRole();
                if (role == Constants.ROLE_STORE_MANAGER){
                    //店长
                    clickStoreManagerItem(employee);
                }else if (role == Constants.ROLE_STORE_CASHIER){
                    //收银员
                    clickCashierItem(employee);
                }
            }
        });
    }

    //点了店长
    private void clickStoreManagerItem(StoreEmployeeBean.Employee employeeManageItemBean) {
        String userId = employeeManageItemBean.getUserId();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("userId", userId);
            ServerClient.newInstance(MyApplication.getContext()).managerDetail(MyApplication.getContext(), Constants.TAG_MANAGER_DETAIL, map);
        } else {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
        }
    }
    //店长详情
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void storeManagerDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_MANAGER_DETAIL)) {
            ManagerDetailBean msg = (ManagerDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        Intent intent = new Intent(this, ManagerDetailActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable(Constants.REQUEST_MANAGER_INTENT, msg.getData());   //传递一个user对象列表
                        intent.putExtras(mBundle);
                        startActivityForResult(intent,REQUEST_CODE_DETAIL_STOREMANAGER);
                    } else {
                        MyToast.showToastShort(ToastHelper.toStr(R.string.data_error));
                    }
                    break;
            }
            dismissLoading();
        }
    }


    //点了收银员
    private void clickCashierItem(StoreEmployeeBean.Employee employeeManageItemBean) {
        String userId = employeeManageItemBean.getUserId();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("userId", userId);
            ServerClient.newInstance(this).cashierDetail(this, Constants.TAG_CASHIER_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //收银员详情
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void cashierDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CASHIER_DETAIL)) {
            CashierDetailBean msg = (CashierDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.CASHIER_DETAIL_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(StoreEmployeeActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CASHIER_DETAIL_TRUE:
                    CashierDetailBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        intent.putExtra("storeInfo", data);
                        /*if (mClickedEmployeeManageItemBean != null && !TextUtils.isEmpty(mClickedEmployeeManageItemBean.getPhone())) {
                            intent.putExtra("cashierUserName", mClickedEmployeeManageItemBean.getPhone());
                        } else {
                            intent.putExtra("cashierUserName", "");
                        }
                        if (mClickedEmployeeManageItemBean != null) {
                            intent.putExtra("userId", mClickedEmployeeManageItemBean.getUserId());
                        } else {
                            intent.putExtra("userId", "");
                        }*/
                        intent.setClass(StoreEmployeeActivity.this, EmployeeInfoActivity.class);
                        startActivityForResult(intent, REQUEST_CODE_DETAIL_EMPLOYEE);
                    }
                    break;
            }
            dismissLoading();
        }
    }


    //模糊查询
    private void searchData() {
        String searchKey = mEt_input.getText().toString().trim();
        if (!TextUtils.isEmpty(searchKey) && employees.size()>0){
            List<StoreEmployeeBean.Employee> searchEmps =  new ArrayList<>();
            for (StoreEmployeeBean.Employee employee:employees){
                if (employee.getEmployeeName().contains(searchKey) || employee.getTelPhone().contains(searchKey)){
                    searchEmps.add(employee);
                }
            }

            if (searchEmps.size()>0){
                List<StoreEmployeeBean.Employee> storeData = new ArrayList<>();//店长list
                List<StoreEmployeeBean.Employee> cashierData = new ArrayList<>();//收银员list
                for (StoreEmployeeBean.Employee dataEntity : searchEmps){
                    int role = dataEntity.getRole();
                    if (Constants.ROLE_STORE_MANAGER == role){
                        //店长
                        storeData.add(dataEntity);
                    }else if (Constants.ROLE_STORE_CASHIER == role){
                        //收银员
                        cashierData.add(dataEntity);
                    }

                }

                List<StoreEmployeeBean.Employee> employees =  new ArrayList<>();
                if (storeData.size()>0){
                    StoreEmployeeBean.Employee title1 = new StoreEmployeeBean.Employee();
                    title1.setTitleName("店长");
                    employees.add(title1);
                    for (int i=0;i<storeData.size();i++){
                        StoreEmployeeBean.Employee dataEntity = storeData.get(i);
                        StoreEmployeeBean.Employee storeEmployee = new StoreEmployeeBean.Employee();
                        storeEmployee.setEmployeeName(dataEntity.getEmployeeName());
                        storeEmployee.setTelPhone(dataEntity.getTelPhone());
                        storeEmployee.setShowLine(i!=(storeData.size()-1));
                        storeEmployee.setRole(dataEntity.getRole());
                        storeEmployee.setUserId(dataEntity.getUserId());
                        employees.add(storeEmployee);
                    }
                }

                if (cashierData.size()>0){
                    StoreEmployeeBean.Employee title2 = new StoreEmployeeBean.Employee();
                    title2.setTitleName("收银员");
                    employees.add(title2);
                    for (int i=0;i<cashierData.size();i++){
                        StoreEmployeeBean.Employee dataEntity = cashierData.get(i);
                        StoreEmployeeBean.Employee cashierEmployee = new StoreEmployeeBean.Employee();
                        cashierEmployee.setEmployeeName(dataEntity.getEmployeeName());
                        cashierEmployee.setTelPhone(dataEntity.getTelPhone());
                        cashierEmployee.setShowLine(i!=(cashierData.size()-1));
                        cashierEmployee.setRole(dataEntity.getRole());
                        cashierEmployee.setUserId(dataEntity.getUserId());
                        employees.add(cashierEmployee);
                    }
                }
                mStoreEmployeeAdapter.setDatas(employees);
            }

        }
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, getString(R.string.public_loading), false);
        mStoreID = getIntent().getStringExtra(Constants.INTENT_DETAIL_STORE_ID);
        mStoreName = getIntent().getStringExtra(Constants.INTENT_DETAIL_STORE_NAME);
        mStoreEmployeeAdapter = new StoreEmployeeAdapter(this);
        mRecyclerviewEmployeeStore.setAdapter(mStoreEmployeeAdapter);

        linkEmployeeList(mStoreID);
    }

    private void linkEmployeeList(String storeId) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("storeMerchantId", storeId);
            ServerClient.newInstance(MyApplication.getContext()).linkEmployeeList(MyApplication.getContext(), Constants.TAG_LINK_EMPLOYEE_LIST, map);
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LINK_EMPLOYEE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LinkEmployeeBean msg = (LinkEmployeeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreEmployeeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    employees.clear();
                    List<LinkEmployeeBean.DataEntity> data = msg.getData();
                    if (data != null && data.size() > 0) {
                        List<LinkEmployeeBean.DataEntity> storeData = new ArrayList<>();//店长list
                        List<LinkEmployeeBean.DataEntity> cashierData = new ArrayList<>();//收银员list
                        for (LinkEmployeeBean.DataEntity dataEntity : data){
                            int role = dataEntity.getRole();
                            if (Constants.ROLE_STORE_MANAGER == role){
                                //店长
                                storeData.add(dataEntity);
                            }else if (Constants.ROLE_STORE_CASHIER == role){
                                //收银员
                                cashierData.add(dataEntity);
                            }

                        }
                        if (storeData.size()>0){
                            StoreEmployeeBean.Employee title1 = new StoreEmployeeBean.Employee();
                            title1.setTitleName("店长");
                            employees.add(title1);
                            for (int i=0;i<storeData.size();i++){
                                LinkEmployeeBean.DataEntity dataEntity = storeData.get(i);
                                StoreEmployeeBean.Employee storeEmployee = new StoreEmployeeBean.Employee();
                                storeEmployee.setEmployeeName(dataEntity.getName());
                                storeEmployee.setTelPhone(dataEntity.getPhone());
                                storeEmployee.setShowLine(i!=(storeData.size()-1));
                                storeEmployee.setRole(dataEntity.getRole());
                                storeEmployee.setUserId(dataEntity.getUserId());
                                employees.add(storeEmployee);
                            }
                        }

                        if (cashierData.size()>0){
                            StoreEmployeeBean.Employee title2 = new StoreEmployeeBean.Employee();
                            title2.setTitleName("收银员");
                            employees.add(title2);
                            for (int i=0;i<cashierData.size();i++){
                                LinkEmployeeBean.DataEntity dataEntity = cashierData.get(i);
                                StoreEmployeeBean.Employee cashierEmployee = new StoreEmployeeBean.Employee();
                                cashierEmployee.setEmployeeName(dataEntity.getName());
                                cashierEmployee.setTelPhone(dataEntity.getPhone());
                                cashierEmployee.setShowLine(i!=(cashierData.size()-1));
                                cashierEmployee.setRole(dataEntity.getRole());
                                cashierEmployee.setUserId(dataEntity.getUserId());
                                employees.add(cashierEmployee);
                            }
                        }
                        mStoreEmployeeAdapter.setDatas(employees);
                    } else {
                        MyToast.showToastShort(getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
    }


    private void initView() {
        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(getResources().getString(R.string.string_title_store_employee));
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvButton = (ImageView) findViewById(R.id.iv_button);
        mIvButton.setVisibility(View.VISIBLE);

        mEt_input = findViewById(R.id.et_input);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setLoadmoreEnable(false);
        mSwipeRefreshLayout.setRefreshEnable(false);
        mRecyclerviewEmployeeStore = findViewById(R.id.recyclerview_employee_store);
        mRecyclerviewEmployeeStore.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button://新增员工
                Intent intent = new Intent(StoreEmployeeActivity.this, AddEmployeeActivity.class);
                intent.putExtra(Constants.INTENT_NAME,Constants.INTENT_VALUE_STORE_ADD_EMPLOYEE);
                intent.putExtra(Constants.INTENT_DETAIL_STORE_ID,mStoreID);
                intent.putExtra(Constants.INTENT_DETAIL_STORE_NAME,mStoreName);
                startActivityForResult(intent,REQUEST_CODE_ADD_EMPLOYEE);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( requestCode == REQUEST_CODE_DETAIL_EMPLOYEE || requestCode == REQUEST_CODE_DETAIL_STOREMANAGER || requestCode == REQUEST_CODE_ADD_EMPLOYEE){
            if (resultCode == RESULT_OK){
                setResult(RESULT_OK);
                linkEmployeeList(mStoreID);
            }
        }
    }
}
