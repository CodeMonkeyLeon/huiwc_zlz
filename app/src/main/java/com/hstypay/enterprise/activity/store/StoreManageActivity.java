package com.hstypay.enterprise.activity.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.StoreManageAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreDetailBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 门店管理
 */
public class StoreManageActivity extends BaseActivity implements View.OnClickListener, StoreManageAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack,mIvButton;
    private TextView mButton, mTvTitle, mTvNull;
    private RecyclerView mRvShop;
    private EditTextDelete mEtInput;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private StoreManageAdapter mAdapter;
    private String intentName;
    private int clickPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_manage);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        currentPage = 2;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mIvButton = (ImageView) findViewById(R.id.iv_button);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mRvShop.setLayoutManager(new LinearLayoutManager(this));
        mEtInput = findViewById(R.id.et_input);
        mTvTitle.setText(R.string.string_store_manage);
        mButton.setVisibility(View.INVISIBLE);
        mIvButton.setVisibility(View.VISIBLE);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        //mSwipeRefreshLayout.setRefreshEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO 触发刷新数据任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    loadData("15", "1", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                //TODO 触发加载更多任务
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(pageSize + "", currentPage + "", mEtInput.getText().toString().trim());
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvButton.setOnClickListener(this);

        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mList.clear();
                        currentPage = 2;
                        loadData("15", "1", mEtInput.getText().toString().trim());
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    mList.clear();
                    setHeader(intentName, mList);
                    loadData("15", "1", "");
                }
            }
        });
    }

    public void initData() {
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mList = new ArrayList<>();
        mAdapter = new StoreManageAdapter(StoreManageActivity.this, mList);
        mAdapter.setOnItemClickListener(this);
        mRvShop.setAdapter(mAdapter);
        mRvShop.setVisibility(View.GONE);
        setHeader(intentName, mList);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            loadData(pageSize + "", "1", mEtInput.getText().toString().trim());
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadData(String pageSize, String currentPage, String storeName) {
        if (Utils.Integer.tryParse(currentPage, 0) == 1 && !isRefreshed) {
            showNewLoading(true, getString(R.string.public_loading));
        }
        //门店网络请求
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        if (!TextUtils.isEmpty(storeName)) {
            map.put("storeName", storeName);
        }
        ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_CHOICE_STORE, map);
    }

    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_STORE)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreManageActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mSwipeRefreshLayout.setLoadmoreEnable(true);
                        mRvShop.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            mRvShop.setVisibility(View.VISIBLE);
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mRvShop.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setLoadmoreEnable(false);
                        }
                    }
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_STORE_DETAIL)) {
            dismissLoading();
            StoreDetailBean msg = (StoreDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreManageActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        Intent intent = new Intent(this,StoreDetailActivity2.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable(Constants.RESULT_SHOP_BEAN_INTENT, msg.getData());   //传递一个user对象列表
                        mBundle.putInt(Constants.REQUEST_STORE_INTENT, clickPosition);
                        intent.putExtras(mBundle);
                        startActivityForResult(intent,Constants.REQUEST_DETAIL_STORE);
                    }else {
                        MyToast.showToastShort(UIUtils.getString(R.string.data_error));
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            setHeader(intentName, mList);
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button:
                Intent intent = new Intent(StoreManageActivity.this,AddStoreActivity.class);
                startActivityForResult(intent,Constants.REQUEST_ADD_STORE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        MtaUtils.mtaId(StoreManageActivity.this,"M002");
        clickPosition = position;
        String storeId = mList.get(position).getStoreId();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String,Object> map = new HashMap<>();
            map.put("storeId",storeId);
            ServerClient.newInstance(MyApplication.getContext()).storeDetail(MyApplication.getContext(), Constants.TAG_STORE_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    public void setHeader(String name, List<StoreListBean.DataEntity> list) {
        if (Constants.INTENT_NAME_BILL_SHOP.equals(name) || Constants.INTENT_NAME_COLLECT_SHOP.equals(name)) {
            StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
            dataEntity.setStoreName("全部门店");
            dataEntity.setStoreId("");
            list.add(dataEntity);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == Constants.REQUEST_EDIT_STORE && resultCode == RESULT_OK){
            int clickPosition = data.getIntExtra(Constants.REQUEST_STORE_INTENT, -1);
            if (clickPosition != -1) {
                mList.get(clickPosition).setExamineStatus(0);
                mAdapter.notifyDataSetChanged();
            }
        }*/
        if ((requestCode == Constants.REQUEST_ADD_STORE || requestCode == Constants.REQUEST_DETAIL_STORE )&& resultCode == RESULT_OK){
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                mList.clear();
                currentPage = 2;
                loadData("15", "1", mEtInput.getText().toString().trim());
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }
}
