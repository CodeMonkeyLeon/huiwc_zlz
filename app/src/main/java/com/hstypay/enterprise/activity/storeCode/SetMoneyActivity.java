package com.hstypay.enterprise.activity.storeCode;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.EditDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 18:11
 * @描述: ${TODO}
 */

public class SetMoneyActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvClean;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle, mTvAddRemark;
    private EditText mEtMoney;
    private EditDialog mDialog;
    private String editString;
    private String setRemark = "";
    private double moneyLimit;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_money);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtMoney = findViewById(R.id.et_set_money);
        mIvClean = findViewById(R.id.iv_clean);
        mTvAddRemark = findViewById(R.id.tv_add_remark);

        mTvTitle.setText(R.string.title_set_money);
        mButton.setVisibility(View.INVISIBLE);
        setButtonEnable(mBtnSubmit, true);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mTvAddRemark.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mEtMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 1) {
                    if (s.length() >= 2 && s.toString().indexOf('0') == 0 && !s.toString().startsWith("0.")) {
                        mEtMoney.setText(s.toString().substring(0, s.toString().length() - 1));
                    }
                    if (s.toString().indexOf('.') == 0) {
                        s = "0.";
                        mEtMoney.setText("0.");
                    }
                    if (Double.parseDouble(s.toString()) <= 300000) {
                        if (s.toString().indexOf('.') > -1) {
                            if (s.toString().length() > s.toString().indexOf('.') + 3) {
                                mEtMoney.setText(s.toString().substring(0, s.toString().indexOf('.') + 3));
                            }
                        }
                    } else {
                        mEtMoney.setText(s.toString().substring(0, s.toString().length() - 1));
                        MyToast.showToast(UIUtils.getString(R.string.tv_pay_exceed_money), Toast.LENGTH_SHORT);
                    }
                    String money = mEtMoney.getText().toString().trim();
                    /*if (money.equals("0") || money.equals("0.") || money.equals("0.0") || money.equals("0.00")) {
                        setButtonEnable(mBtnSubmit, false);
                    } else {*/
//                        setButtonEnable(mBtnSubmit, true);
//                    }
                    mIvClean.setVisibility(View.VISIBLE);
                } else {
//                    setButtonEnable(mBtnSubmit, false);
                    mIvClean.setVisibility(View.INVISIBLE);
                }
                mEtMoney.setSelection(mEtMoney.getText().toString().trim().length());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void initData() {
        String limit = SpUtil.getString(MyApplication.getContext(), Constants.SP_MONEY_LIMIT);
        moneyLimit = Utils.Double.tryParse(limit, 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_remark:
                MtaUtils.mtaId(SetMoneyActivity.this, "J006");
                showEditDialog(this);
                break;
            case R.id.btn_submit:
                MtaUtils.mtaId(SetMoneyActivity.this, "J005");
                Intent intent = new Intent();
                intent.putExtra(Constants.REQUESTCODE_SETMONEY_REMARK, setRemark);
                intent.putExtra(Constants.REQUESTCODE_SETMONEY_MONEY, mEtMoney.getText().toString().trim());
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.iv_clean:
                mEtMoney.setText("");
                break;
            default:
                break;
        }
    }

    public void showEditDialog(Activity activity) {
        mDialog = new EditDialog(activity, editString, new EditDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String edit) {
                if (!TextUtils.isEmpty(edit)) {
                    mTvAddRemark.setText(edit);
                    editString = edit;
                    setRemark = edit;
                }
            }
        }, null);
        DialogHelper.resize(activity, mDialog);
        mDialog.show();
    }
}
