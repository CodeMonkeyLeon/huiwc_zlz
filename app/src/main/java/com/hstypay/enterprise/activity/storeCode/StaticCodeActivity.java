package com.hstypay.enterprise.activity.storeCode;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class StaticCodeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvQrLogo, mIvLogoWechat, mIvLogoAlipay, mIvLogoQQ, mIvLogoUnion, mIvLogoHuabei, mIvLogoJd, mIvLogoFuka;
    private Button mBtnConfirm;
    private TextView mButton, mTvTitle, mTvTitleWechat, mTvTitleAlipay, mTvTitleUnion, mTvTitleQQ, mTvTitleHuabei, mTvTitleJd, mTvTitleFuka;
    private TextView mTvMoney, mTvCodeRemark, mTvCodeNo, mTvCompany, mTvScan;
    private ImageView mIvQrCode;
    private RelativeLayout mRlQrLogo;
    private static final int SETMONEY = 0;
    private static final int CLEARMONEY = 1;
    private int buttonState = SETMONEY;
    private LinearLayout mLlBitmap;
    private Bitmap bitmap;
    private String mStoreMerchantId;
    private String mQrcode;
    private String mUserId;
    private java.text.DecimalFormat mDecimalFormat = new java.text.DecimalFormat("#");
    private StoreCodeListBean.DataBeanX.DataBean mQrCodeStore;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_qrcode);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMoney = (TextView) findViewById(R.id.tv_set_money);//设置的金额
        mTvCodeRemark = (TextView) findViewById(R.id.tv_code_remark);//收款备注
        mTvCodeNo = (TextView) findViewById(R.id.tv_code_no);//二维码编号
        mTvCompany = (TextView) findViewById(R.id.tv_company);//店名
        mTvScan = (TextView) findViewById(R.id.tv_scan);//扫一扫字样
        mIvQrCode = (ImageView) findViewById(R.id.img_qr_code);//二维码图片
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mLlBitmap = (LinearLayout) findViewById(R.id.ll_qrcode_bitmap);

        mRlQrLogo = findViewById(R.id.rl_qr_logo);
        mIvQrLogo = findViewById(R.id.img_qr_logo);
        mIvLogoWechat = findViewById(R.id.iv_logo_wechat);
        mIvLogoAlipay = findViewById(R.id.iv_logo_alipay);
        mIvLogoUnion = findViewById(R.id.iv_logo_union);
        mIvLogoQQ = findViewById(R.id.iv_logo_qq);
        mIvLogoHuabei = findViewById(R.id.iv_logo_huabei);
        mIvLogoJd = findViewById(R.id.iv_logo_jd);
        mIvLogoFuka = findViewById(R.id.iv_logo_fuka);

        mTvTitleWechat = findViewById(R.id.tv_title_wechat);
        mTvTitleAlipay = findViewById(R.id.tv_title_alipay);
        mTvTitleUnion = findViewById(R.id.tv_title_union);
        mTvTitleQQ = findViewById(R.id.tv_title_qq);
        mTvTitleHuabei = findViewById(R.id.tv_title_huabei);
        mTvTitleJd = findViewById(R.id.tv_title_jd);
        mTvTitleFuka = findViewById(R.id.tv_title_fuka);

        mTvTitle.setText(R.string.title_qrcode);
        mButton.setText(R.string.title_set_money);
        setButtonEnable(mBtnConfirm, true);
    }

    public void initEvent() {
        mButton.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mBtnConfirm.setOnClickListener(this);
    }

    public void initData() {
        Intent intent = getIntent();
        mQrCodeStore = (StoreCodeListBean.DataBeanX.DataBean) intent.getSerializableExtra(Constants.INTENT_QRCODE_STORE);
//        mQrCodeInfo = (CodeListBean.DataEntity.QrcodeEntity) intent.getSerializableExtra(Constants.INTENT_QRCODE_INFO);
        mStoreMerchantId = mQrCodeStore.getStoreId();
        String storeName = mQrCodeStore.getStoreName();
        mQrcode = mQrCodeStore.getQrcode();
        String showId = mQrCodeStore.getId() + "";
        mUserId = SpUtil.getString(MyApplication.getContext(), Constants.USER_ID, "");
        setQrcode(mQrCodeStore, mUserId, "");
        mTvCodeNo.setText("NO." + showId);
        mTvCompany.setText(storeName);
        if (mQrCodeStore.getQrType() == 2) {
            mButton.setVisibility(View.INVISIBLE);
        }

        if (mQrCodeStore != null) {
            if (mQrCodeStore.getApiProvider() != null) {
                for (int i = 0; i < mQrCodeStore.getApiProvider().size(); i++) {
                    if ("1".equals(mQrCodeStore.getApiProvider().get(i))) {
                        mIvLogoWechat.setVisibility(View.VISIBLE);
                        mTvTitleWechat.setVisibility(View.VISIBLE);
                    }
                    if ("2".equals(mQrCodeStore.getApiProvider().get(i))) {
                        mIvLogoAlipay.setVisibility(View.VISIBLE);
                        mTvTitleAlipay.setVisibility(View.VISIBLE);
                    }
                    if ("4".equals(mQrCodeStore.getApiProvider().get(i))) {
                        mIvLogoQQ.setVisibility(View.VISIBLE);
                        mTvTitleQQ.setVisibility(View.VISIBLE);
                    }
                    if ("5".equals(mQrCodeStore.getApiProvider().get(i))) {
                        mIvLogoUnion.setVisibility(View.VISIBLE);
                        mTvTitleUnion.setVisibility(View.VISIBLE);
                    }
                    if ("6".equals(mQrCodeStore.getApiProvider().get(i))) {
                        mIvLogoJd.setVisibility(View.VISIBLE);
                        mTvTitleJd.setVisibility(View.VISIBLE);
                    }
                    if ("11".equals(mQrCodeStore.getApiProvider().get(i))) {
                        mIvLogoFuka.setVisibility(View.VISIBLE);
                        mTvTitleFuka.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                mIvLogoWechat.setVisibility(View.VISIBLE);
                mTvTitleWechat.setVisibility(View.VISIBLE);
                mIvLogoAlipay.setVisibility(View.VISIBLE);
                mTvTitleAlipay.setVisibility(View.VISIBLE);
                mIvLogoUnion.setVisibility(View.VISIBLE);
                mTvTitleUnion.setVisibility(View.VISIBLE);
            }
        } else {
            mIvLogoWechat.setVisibility(View.VISIBLE);
            mTvTitleWechat.setVisibility(View.VISIBLE);
            mIvLogoAlipay.setVisibility(View.VISIBLE);
            mTvTitleAlipay.setVisibility(View.VISIBLE);
            mIvLogoUnion.setVisibility(View.VISIBLE);
            mTvTitleUnion.setVisibility(View.VISIBLE);
        }
    }

    public void setQrcode(StoreCodeListBean.DataBeanX.DataBean qrCodeStore, String userId, String money) {
        String codeMsg;
        if (qrCodeStore.getQrType() == 2) {
            try {
                String qrUrl = qrCodeStore.getQrUrl();
                String regex = "attach=";
                if (!TextUtils.isEmpty(qrUrl) && qrUrl.contains(regex)) {
                    String urlBefore = qrUrl.substring(0, qrUrl.indexOf(regex) + regex.length());
                    String urlAfter = qrUrl.substring(qrUrl.indexOf(regex) + regex.length());
                    String urlAfterDecode = URLDecoder.decode(urlAfter, "UTF-8");
                    String[] urlAfterSplit = urlAfterDecode.split("&");
                    StringBuilder urlAfterBuilder = new StringBuilder();
                    if (urlAfterSplit.length > 0) {
                        for (int i = 0; i < urlAfterSplit.length; i++) {
                            if (urlAfterSplit[i].contains("op_user_id=")) {
                                urlAfterBuilder.append("op_user_id=").append(userId);
                            } else {
                                urlAfterBuilder.append(urlAfterSplit[i]);
                            }
                            if (i != urlAfterSplit.length - 1) {
                                urlAfterBuilder.append("&");
                            }
                        }
                        urlAfter = URLEncoder.encode(urlAfterBuilder.toString(), "UTF-8");
                    }
                    qrUrl = urlBefore + urlAfter;
                }
                codeMsg = qrUrl;
            } catch (UnsupportedEncodingException e) {
                showCommonNoticeDialog(StaticCodeActivity.this, "二维码解码错误");
                e.printStackTrace();
                return;
            }
        } /*else if (qrCodeStore.getQrType() == 3 && !TextUtils.isEmpty(qrCodeStore.getWxappid())) {
            if (TextUtils.isEmpty(money)) {
                codeMsg = Constants.H5_STATIC_QRCODE + "wxappid=" + qrCodeStore.getWxappid() + "&storeMerchantId=" + qrCodeStore.getStoreId() + "&userId=" + userId + "&qrcode=" + qrCodeStore.getQrcode();
            } else {
                codeMsg = Constants.H5_STATIC_QRCODE + "wxappid=" + qrCodeStore.getWxappid() + "&storeMerchantId=" + mStoreMerchantId +
                        "&userId=" + mUserId + "&qrcode=" + mQrcode + "&money=" + mDecimalFormat.format(Double.parseDouble(money) * 100);
            }
        }*/ else {
            if (TextUtils.isEmpty(money)) {
                codeMsg = qrCodeStore.getQrUrl();
//                codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + qrCodeStore.getStoreId() + "&userId=" + userId + "&qrcode=" + qrCodeStore.getQrcode();
            } else {
                codeMsg = qrCodeStore.getQrUrl() + "&money=" + mDecimalFormat.format(Double.parseDouble(money) * 100);
//                codeMsg = Constants.H5_STATIC_QRCODE + "storeMerchantId=" + mStoreMerchantId +
//                        "&userId=" + mUserId + "&qrcode=" + mQrcode + "&money=" + mDecimalFormat.format(Double.parseDouble(money) * 100);
            }
        }

        if (ConfigUtil.getConfig()){
            if (codeMsg.contains("?")){
                //codeMsg已有参数
                codeMsg = codeMsg+"&source="+Constants.APP_CODE + Constants.ORG_ID;
            }else {
                //codeMsg里没有参数
                codeMsg = codeMsg+"?source="+Constants.APP_CODE + Constants.ORG_ID;
            }
        }
        try {
            bitmap = MaxCardManager.getInstance().create2DCode(codeMsg,
                    DisplayUtil.dip2Px(StaticCodeActivity.this, 400),
                    DisplayUtil.dip2Px(StaticCodeActivity.this, 400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
        mIvQrCode.setImageBitmap(bitmap);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                MtaUtils.mtaId(StaticCodeActivity.this, "J004");
                if (buttonState == SETMONEY) {
                    startActivityForResult(new Intent(StaticCodeActivity.this, SetMoneyActivity.class), Constants.REQUESTCODE_SETMONEY);
                } else if (buttonState == CLEARMONEY) {
                    mTvMoney.setVisibility(View.GONE);
                    mTvCodeRemark.setVisibility(View.GONE);
                    mTvScan.setVisibility(View.VISIBLE);
                    mButton.setText("设置金额");
                    buttonState = SETMONEY;
                    setQrcode(mQrCodeStore, mUserId, "");
                }
                break;
            case R.id.btn_confirm:
                MtaUtils.mtaId(StaticCodeActivity.this, "J003");
                boolean results = PermissionUtils.checkPermissionArray(StaticCodeActivity.this, permissionArray);
                if (results) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(mLlBitmap));
                    showCommonNoticeDialog(StaticCodeActivity.this, getString(R.string.dialog_copy_picture));
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(mLlBitmap));
                    showCommonNoticeDialog(StaticCodeActivity.this, getString(R.string.dialog_copy_picture));
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUESTCODE_SETMONEY && resultCode == RESULT_OK) {
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY))) {
                LogUtil.d(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY));
                String money = data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY);
                mTvMoney.setText(getString(R.string.tx_mark) + StringUtils.formatMoney(money, 2));
                mTvMoney.setVisibility(View.VISIBLE);
                buttonState = CLEARMONEY;
                mButton.setText("清除金额");
                setQrcode(mQrCodeStore, mUserId, money);
            }
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK))) {
                mTvCodeRemark.setText("备注：" + data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK));
                mTvCodeRemark.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY)) || !TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK))) {
                mTvScan.setVisibility(View.INVISIBLE);
            }
        }
    }
}
