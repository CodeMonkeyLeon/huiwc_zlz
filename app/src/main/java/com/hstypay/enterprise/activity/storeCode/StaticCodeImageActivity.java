package com.hstypay.enterprise.activity.storeCode;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.EmpEnsureActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LinkEmployeeBean;
import com.hstypay.enterprise.bean.QrImageBean;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class StaticCodeImageActivity extends BaseActivity {
    private ImageView mIvBack;
    private Button mBtnConfirm, mBtnUnbind;
    private TextView mButton, mTvTitle;
    private TextView mTvMoney, mTvCodeRemark;
    private TextView mTvStore, mTvCashier;
    private RelativeLayout mRlStore, mRlCashier;
    private ImageView mIvQrCode;
    private static final int SETMONEY = 0;
    private static final int CLEARMONEY = 1;
    private int buttonState = SETMONEY;
    private LinearLayout mLlBitmap, mLlProperty;
    private Bitmap bitmap;
    private String mStoreMerchantId;
    private String mQrcode;
    private String mUserId;
    private java.text.DecimalFormat mDecimalFormat = new java.text.DecimalFormat("#");
    private StoreCodeListBean.DataBeanX.DataBean mQrCodeStore;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private SafeDialog mLoadDialog;
    private String mIntentName;
    private double mPayMoney;
    private String mPayRemark;
    private String mStoreId;
    private String mCashierId;
    private SelectDialog mSelectDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_qrcode_image);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMoney = (TextView) findViewById(R.id.tv_set_money);//设置的金额
        mTvCodeRemark = (TextView) findViewById(R.id.tv_code_remark);//收款备注

        mIvQrCode = (ImageView) findViewById(R.id.iv_qrcode);//二维码图片
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mLlBitmap = (LinearLayout) findViewById(R.id.ll_qrcode_bitmap);

        mLlProperty = findViewById(R.id.ll_property);
        mRlStore = findViewById(R.id.rl_store);
        mRlCashier = findViewById(R.id.rl_cashier);
        mTvStore = findViewById(R.id.tv_store);
        mTvCashier = findViewById(R.id.tv_cashier);
        mBtnUnbind = findViewById(R.id.btn_unbind);

        mTvTitle.setText(R.string.title_qrcode);
        mButton.setText(R.string.title_set_money);
        setButtonEnable(mBtnConfirm, true);
        setButtonWhite(mBtnUnbind);
    }

    public void initEvent() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        finish();
                        break;
                    case R.id.button_title:
                        MtaUtils.mtaId(StaticCodeImageActivity.this, "J004");
                        if (buttonState == SETMONEY) {
                            startActivityForResult(new Intent(StaticCodeImageActivity.this, SetMoneyActivity.class), Constants.REQUESTCODE_SETMONEY);
                        } else if (buttonState == CLEARMONEY) {
                            mTvMoney.setVisibility(View.GONE);
                            mTvCodeRemark.setVisibility(View.GONE);
                            mButton.setText("设置金额");
                            buttonState = SETMONEY;
                            Picasso.get().load(Constants.H5_BASE_URL + mQrCodeStore.getQrLogo())
                                    .error(R.mipmap.large_img_gen_placeholder)
                                    .placeholder(R.mipmap.large_img_gen_placeholder).into(mIvQrCode);
                        }
                        break;
                    case R.id.btn_confirm:
                        if ("INTENT_CHANGE_QRCODE".equals(mIntentName)) {//从支付页跳转
                            Intent intent = new Intent(StaticCodeImageActivity.this, CaptureActivity.class);
                            intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreMerchantId);
                            intent.putExtra(Constants.INTENT_PAY_MONEY, mPayMoney);
                            intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
                            startActivity(intent);
                            finish();
                        } else {
                            MtaUtils.mtaId(StaticCodeImageActivity.this, "J003");
                            boolean results = PermissionUtils.checkPermissionArray(StaticCodeImageActivity.this, permissionArray);
                            if (results) {
                                ScreenUtils.saveImageToGallery(StaticCodeImageActivity.this, ScreenUtils.createViewBitmap(mLlBitmap));
                                showCommonNoticeDialog(StaticCodeImageActivity.this, getString(R.string.dialog_copy_picture));
                            } else {
                                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                            }
                        }
                        break;
                    case R.id.rl_store:
                        Intent intentShop = new Intent(StaticCodeImageActivity.this, ShopActivity.class);
                        intentShop.putExtra(Constants.INTENT_NAME, "INTENT_QRCODE_STORE");
                        intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                        intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                        startActivityForResult(intentShop, Constants.REQUEST_CASHIER_SHOP_EDIT_CODE);
                        break;
                    case R.id.rl_cashier:
                        Intent intentCashier = new Intent(StaticCodeImageActivity.this, EmpEnsureActivity.class);
                        intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, mStoreId);
                        if (TextUtils.isEmpty(mCashierId)) {
                            intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                        } else {
                            intentCashier.putExtra(Constants.INTENT_CASHIER_ID, mCashierId);
                        }
                        startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                        break;
                    case R.id.btn_unbind:
                        showUnbindDialog();
                        break;
                    default:
                        break;
                }
            }
        };
        mButton.setOnClickListener(onSingleClickListener);
        mIvBack.setOnClickListener(onSingleClickListener);
        mBtnConfirm.setOnClickListener(onSingleClickListener);
        mBtnConfirm.setOnClickListener(onSingleClickListener);
        mRlStore.setOnClickListener(onSingleClickListener);
        mRlCashier.setOnClickListener(onSingleClickListener);
        mBtnUnbind.setOnClickListener(onSingleClickListener);
    }

    public void initData() {
        Intent intent = getIntent();
        mIntentName = intent.getStringExtra(Constants.INTENT_NAME);
        if ("INTENT_CHANGE_QRCODE".equals(mIntentName) || Constants.INTENT_NAME_POS_QRCODE.equals(mIntentName)) {//从支付页跳转
            mPayMoney = intent.getDoubleExtra(Constants.INTENT_PAY_MONEY, 0);
            String money = String.valueOf(BigDecimal.valueOf(mPayMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            mStoreMerchantId = intent.getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
            mPayRemark = intent.getStringExtra(Constants.INTENT_PAY_REMARK);
            if(!TextUtils.isEmpty(mPayRemark)) {
                mTvCodeRemark.setText("备注：" + mPayRemark);
                mTvCodeRemark.setVisibility(View.VISIBLE);
            }
            getMoneyQrcode(money, null, mStoreMerchantId);
            mButton.setVisibility(View.INVISIBLE);
            mBtnConfirm.setText(R.string.change_scan);
            if (Constants.INTENT_NAME_POS_QRCODE.equals(mIntentName)) {
                mBtnConfirm.setVisibility(View.INVISIBLE);
            }
            mLlProperty.setVisibility(View.GONE);
            mBtnUnbind.setVisibility(View.INVISIBLE);
            mTvMoney.setText(getString(R.string.tx_mark) + StringUtils.formatMoney(mPayMoney + "", 2));
            mTvMoney.setVisibility(View.VISIBLE);
        } else {
            mQrCodeStore = (StoreCodeListBean.DataBeanX.DataBean) intent.getSerializableExtra(Constants.INTENT_QRCODE_STORE);
            if (mQrCodeStore == null)
                return;
            mStoreMerchantId = mQrCodeStore.getStoreId();
            mQrcode = mQrCodeStore.getQrcode();
            mUserId = SpUtil.getString(MyApplication.getContext(), Constants.USER_ID, "");
            Picasso.get().load(Constants.H5_BASE_URL + mQrCodeStore.getQrLogo())
                    .error(R.mipmap.large_img_gen_placeholder)
                    .placeholder(R.mipmap.large_img_gen_placeholder).into(mIvQrCode);
            if (mQrCodeStore.getQrType() == 2) {
                mButton.setVisibility(View.INVISIBLE);
            }
            mTvStore.setText(mQrCodeStore.getStoreName());
            if (TextUtils.isEmpty(mQrCodeStore.getEmpName())) {
                mTvCashier.setText("");
            } else {
                mTvCashier.setText(mQrCodeStore.getEmpName());
            }
            mStoreId = mQrCodeStore.getStoreId();
            mCashierId = mQrCodeStore.getUserId();
            if (MyApplication.getIsCasher()) {
                mLlProperty.setVisibility(View.GONE);
                mBtnUnbind.setVisibility(View.INVISIBLE);
            }
            mBtnConfirm.setText(R.string.tx_static_save_photo);
        }
    }

    private void showUnbindDialog() {
        StringBuilder content = new StringBuilder();
        content.append("解绑后，收款码将不能收款");
        if (mQrCodeStore != null) {
            content.append("\n确认将").append(mQrCodeStore.getId()).append("收款码与");
            if (mQrCodeStore.getEmpName() != null) {
                content.append(mQrCodeStore.getEmpName());
            } else if (mQrCodeStore.getStoreName() != null) {
                content.append(mQrCodeStore.getStoreName());
            }
            content.append("解绑");
        }
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(StaticCodeImageActivity.this, content.toString(), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    unbindQrcode(mQrCodeStore.getQrcode());
                }
            });
        }
        mSelectDialog.show();
    }

    private void getMoneyQrcode(String money, String qrId, String storeId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(money))
                map.put("money", money);
            if (!TextUtils.isEmpty(qrId))
                map.put("id", qrId);
            if (!TextUtils.isEmpty(storeId))
                map.put("storeId", storeId);
            if (!TextUtils.isEmpty(mPayRemark))
                map.put("remark", mPayRemark);
            ServerClient.newInstance(MyApplication.getContext()).getMoneyQrcode(MyApplication.getContext(), "TAG_QUERY_MONEY_QRCODE", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void changeBindQrcode(String qrcode, String storeMerchantId, String userId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("qrcode", qrcode);
            if (!TextUtils.isEmpty(storeMerchantId))
                map.put("storeMerchantId", storeMerchantId);
            if (!TextUtils.isEmpty(userId))
                map.put("userId", userId);
            ServerClient.newInstance(MyApplication.getContext()).changeBindQrcode(MyApplication.getContext(), "TAG_CHANGE_BIND_QRCODE", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void unbindQrcode(String qrcode) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("qrcode", qrcode);
            ServerClient.newInstance(MyApplication.getContext()).unbindQrcode(MyApplication.getContext(), "TAG_UNBIND_QRCODE", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(mLlBitmap));
                    showCommonNoticeDialog(StaticCodeImageActivity.this, getString(R.string.dialog_copy_picture));
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUESTCODE_SETMONEY && resultCode == RESULT_OK) {
            String money = "0";
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY))) {
                LogUtil.d(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY));
                money = data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY);
                mTvMoney.setText(getString(R.string.tx_mark) + StringUtils.formatMoney(money, 2));
                mTvMoney.setVisibility(View.VISIBLE);
                buttonState = CLEARMONEY;
                mButton.setText("清除金额");
            }
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK))) {
                mPayRemark = data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK);
                mTvCodeRemark.setText("备注：" + data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK));
                mTvCodeRemark.setVisibility(View.VISIBLE);
            }
            getMoneyQrcode(mDecimalFormat.format(Double.parseDouble(money) * 100), mQrCodeStore.getId() + "", null);
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_SHOP_EDIT_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvStore.setText(shopBean.getStoreName());
            mStoreId = shopBean.getStoreId();
            mTvCashier.setText("");
            mCashierId = "";
            changeBindQrcode(mQrCodeStore.getQrcode(), mStoreId, mCashierId);
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            Bundle extras = data.getExtras();
            LinkEmployeeBean.DataEntity cashierBean = (LinkEmployeeBean.DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            if (cashierBean != null && cashierBean.isSelected()) {
                mTvCashier.setText(cashierBean.getName());
                mCashierId = cashierBean.getUserId();
            } else {
                mTvCashier.setText("");
                mCashierId = "";
            }
            changeBindQrcode(mQrCodeStore.getQrcode(), mStoreId, mCashierId);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_QUERY_MONEY_QRCODE")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            QrImageBean msg = (QrImageBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StaticCodeImageActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (!TextUtils.isEmpty(msg.getData()) && ImagePase.base64ToBitmap(msg.getData()) != null) {
                        mIvQrCode.setImageBitmap(ImagePase.base64ToBitmap(msg.getData()));
                    } else {
                        mIvQrCode.setImageResource(R.mipmap.large_img_gen_placeholder);
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_CHANGE_BIND_QRCODE") || event.getTag().equals("TAG_UNBIND_QRCODE")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StaticCodeImageActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    String title = "换绑成功";
                    CommonNoticeDialog.OnClickOkListener onClickOkListener = null;
                    if (event.getTag().equals("TAG_UNBIND_QRCODE")) {
                        title = "解绑成功";
                        onClickOkListener = new CommonNoticeDialog.OnClickOkListener() {
                            @Override
                            public void onClickOk() {
                                Intent intent = new Intent(StaticCodeImageActivity.this, StoreCodeListActivity.class);
                                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_STATIC_CODE);
                                startActivity(intent);
                            }
                        };
                    }
                    showCommonNoticeDialog(StaticCodeImageActivity.this, title, onClickOkListener);
                    break;
            }
        }
    }
}
