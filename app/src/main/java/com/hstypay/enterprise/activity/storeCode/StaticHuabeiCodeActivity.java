package com.hstypay.enterprise.activity.storeCode;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class StaticHuabeiCodeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvQrLogo;
    private Button mBtnConfirm;
    private TextView mButton, mTvTitle, mTvStaticTitle;
    private TextView mTvMoney, mTvCodeRemark, mTvCodeNo, mTvCompany;
    private ImageView mIvQrCode;
    private RelativeLayout mRlQrLogo,mRlQrcode;
    private static final int SETMONEY = 0;
    private static final int CLEARMONEY = 1;
    private int buttonState = SETMONEY;
    private LinearLayout mLlBitmap;
    private Bitmap bitmap;
    private String mStoreMerchantId;
    private String mQrcode;
    private String mUserId;
    private java.text.DecimalFormat mDecimalFormat = new java.text.DecimalFormat("#");
    private StoreCodeListBean.DataBeanX.DataBean mQrCodeStore;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_huabei_qrcode);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMoney = (TextView) findViewById(R.id.tv_set_money);//设置的金额
        mTvStaticTitle = (TextView) findViewById(R.id.tv_static_title);//标题
        mTvCodeRemark = (TextView) findViewById(R.id.tv_code_remark);//收款备注
        mTvCodeNo = (TextView) findViewById(R.id.tv_code_no);//二维码编号
        mTvCompany = (TextView) findViewById(R.id.tv_company);//店名
        mIvQrCode = (ImageView) findViewById(R.id.img_qr_code);//二维码图片
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mLlBitmap = (LinearLayout) findViewById(R.id.ll_qrcode_bitmap);
        mRlQrLogo = findViewById(R.id.rl_qr_logo);
        mIvQrLogo = findViewById(R.id.img_qr_logo);
        mRlQrcode = findViewById(R.id.rl_qrcode);

        mTvTitle.setText(R.string.title_qrcode);
        mButton.setText(R.string.title_set_money);
        mButton.setVisibility(View.INVISIBLE);
        setButtonEnable(mBtnConfirm, true);
    }

    public void initEvent() {
        mButton.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mBtnConfirm.setOnClickListener(this);
    }

    public void initData() {
        Intent intent = getIntent();
        mQrCodeStore = (StoreCodeListBean.DataBeanX.DataBean) intent.getSerializableExtra(Constants.INTENT_QRCODE_STORE);
        mStoreMerchantId = mQrCodeStore.getStoreId();
        String storeName = mQrCodeStore.getStoreName();
        mQrcode = mQrCodeStore.getQrcode();
        String showId = mQrCodeStore.getId() + "";
        mUserId = SpUtil.getString(MyApplication.getContext(), Constants.USER_ID, "");
        setQrcode(mQrCodeStore, mUserId, "");
        mTvCodeNo.setText("NO." + showId);
        mTvCompany.setText(storeName);
        if (mQrCodeStore.getQrType() == 2) {
            mButton.setVisibility(View.INVISIBLE);
        }
    }

    public void setQrcode(StoreCodeListBean.DataBeanX.DataBean qrCodeStore, String userId, String money) {
        String codeMsg;
        if (qrCodeStore.getQrType() == 2) {
            try {
                String qrUrl = qrCodeStore.getQrUrl();
                String regex = "attach=";
                if (!TextUtils.isEmpty(qrUrl) && qrUrl.contains(regex)) {
                    String urlBefore = qrUrl.substring(0, qrUrl.indexOf(regex) + regex.length());
                    String urlAfter = qrUrl.substring(qrUrl.indexOf(regex) + regex.length());
                    String urlAfterDecode = URLDecoder.decode(urlAfter, "UTF-8");
                    String[] urlAfterSplit = urlAfterDecode.split("&");
                    StringBuilder urlAfterBuilder = new StringBuilder();
                    if (urlAfterSplit.length > 0) {
                        for (int i = 0; i < urlAfterSplit.length; i++) {
                            if (urlAfterSplit[i].contains("op_user_id=")) {
                                urlAfterBuilder.append("op_user_id=").append(userId);
                            } else {
                                urlAfterBuilder.append(urlAfterSplit[i]);
                            }
                            if (i != urlAfterSplit.length - 1) {
                                urlAfterBuilder.append("&");
                            }
                        }
                        urlAfter = URLEncoder.encode(urlAfterBuilder.toString(), "UTF-8");
                    }
                    qrUrl = urlBefore + urlAfter;
                }
                codeMsg = qrUrl;
            } catch (UnsupportedEncodingException e) {
                showCommonNoticeDialog(StaticHuabeiCodeActivity.this, "二维码解码错误");
                e.printStackTrace();
                return;
            }
        } else {
            if (TextUtils.isEmpty(money)) {
                codeMsg = qrCodeStore.getQrUrl();
            } else {
                codeMsg = qrCodeStore.getQrUrl() + "&money=" + mDecimalFormat.format(Double.parseDouble(money) * 100);
            }
        }

        if (ConfigUtil.getConfig()){
            if (codeMsg.contains("?")){
                //codeMsg已有参数
                codeMsg = codeMsg+"&source="+Constants.APP_CODE + Constants.ORG_ID;
            }else {
                //codeMsg里没有参数
                codeMsg = codeMsg+"?source="+Constants.APP_CODE + Constants.ORG_ID;
            }
        }

        try {
            bitmap = MaxCardManager.getInstance().create2DCode(codeMsg,
                    DisplayUtil.dip2Px(StaticHuabeiCodeActivity.this, 400),
                    DisplayUtil.dip2Px(StaticHuabeiCodeActivity.this, 400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
        mIvQrCode.setImageBitmap(bitmap);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                MtaUtils.mtaId(StaticHuabeiCodeActivity.this, "J004");
                if (buttonState == SETMONEY) {
                    startActivityForResult(new Intent(StaticHuabeiCodeActivity.this, SetMoneyActivity.class), Constants.REQUESTCODE_SETMONEY);
                } else if (buttonState == CLEARMONEY) {
                    mTvMoney.setVisibility(View.GONE);
                    mTvCodeRemark.setVisibility(View.GONE);
                    mButton.setText("设置金额");
                    buttonState = SETMONEY;
                    mTvStaticTitle.setText(R.string.tv_static_title);
                    mRlQrcode.setBackgroundResource(R.mipmap.img_qr_huabei);
                    setQrcode(mQrCodeStore, mUserId, "");
                }
                break;
            case R.id.btn_confirm:
                MtaUtils.mtaId(StaticHuabeiCodeActivity.this, "J003");
                boolean results = PermissionUtils.checkPermissionArray(StaticHuabeiCodeActivity.this, permissionArray);
                if (results) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(mLlBitmap));
                    showCommonNoticeDialog(StaticHuabeiCodeActivity.this,getString(R.string.dialog_copy_picture));
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(mLlBitmap));
                    showCommonNoticeDialog(StaticHuabeiCodeActivity.this,getString(R.string.dialog_copy_picture));
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUESTCODE_SETMONEY && resultCode == RESULT_OK) {
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY))) {
                mRlQrcode.setBackgroundResource(R.mipmap.img_qr_huabei_money);
                mTvStaticTitle.setText(R.string.tv_static_update_title);
                LogUtil.d(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY));
                String money = data.getStringExtra(Constants.REQUESTCODE_SETMONEY_MONEY);
                mTvMoney.setText(getString(R.string.tx_mark) + StringUtils.formatMoney(money, 2));
                mTvMoney.setVisibility(View.VISIBLE);
                buttonState = CLEARMONEY;
                mButton.setText("清除金额");
                setQrcode(mQrCodeStore, mUserId, money);
            }
            if (!TextUtils.isEmpty(data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK))) {
                mTvCodeRemark.setText("备注：" + data.getStringExtra(Constants.REQUESTCODE_SETMONEY_REMARK));
                mTvCodeRemark.setVisibility(View.VISIBLE);
            }
        }
    }
}
