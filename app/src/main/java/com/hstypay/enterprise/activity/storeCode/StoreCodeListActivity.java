package com.hstypay.enterprise.activity.storeCode;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.BindingCodeActivity;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.CommonRecyclerAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.StoreCodeListAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.StoresBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/6/29.
 */
public class StoreCodeListActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvQuestion, mIvShopArrow, mIvCashierArrow, mIvTypeArrow, mIvBack;
    private LinearLayout mLlDeviceList, mShopPopLayout, mCashierPopLayout, mTypePopLayout, mLlShop, mLlCashier, mLlType;
    private EditTextDelete mEtInput, mEtStoreInput, mEtCashierInput;
    private RecyclerView mRecyclerView, mStoreRecyclerView, mCashierRecyclerView, mTypeRecyclerView;
    private TextView mButton, mTvTitle, mTvNull, mTvShop, mTvCashier, mTvType;
    private boolean mShopSwitchArrow, mCashierSwitchArrow, mTypeSwitchArrow;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DataEntity> mCashierList;
    private List<DataEntity> mOriginCashierList;
    private List<DataEntity> mTypeList;
    private List<StoreCodeListBean.DataBeanX.DataBean> mStoreCodeList;
    private Animation rotate;
    private SafeDialog mLoadDialog;
    private SafeDialog mStatusLoadDialog;
    private String mStoreId = "";
    private String mCashierId = "";
    private String mTypeId = "";
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private boolean isNotFirstRequest;
    private CustomLinearLayoutManager mLinearLayoutManager, mStoreLinearLayoutManager;
    private StoreCodeListAdapter mAdapter;
    private ShopRecyclerAdapter mStoreAdapter;
    private CashierRecyclerAdapter mCashierAdapter;
    private int updateStatus = -1;
    private String mUserId = "";
    private List<StoresBean> mStores;
    private NoticeDialog mNoticeDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_code_list);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvShopArrow = (ImageView) findViewById(R.id.iv_shop_arrow);
        mIvCashierArrow = (ImageView) findViewById(R.id.iv_cashier_arrow);
        mIvTypeArrow = (ImageView) findViewById(R.id.iv_type_arrow);
        mLlDeviceList = (LinearLayout) findViewById(R.id.ll_cloud_device_list);
        mEtInput = (EditTextDelete) findViewById(R.id.et_input);
        mTvNull = (TextView) findViewById(R.id.tv_null);
        mIvQuestion = findViewById(R.id.iv_question);

        mLlShop = findViewById(R.id.ll_shop);
        mTvShop = findViewById(R.id.tv_shop);
        mLlCashier = findViewById(R.id.ll_cashier);
        mTvCashier = findViewById(R.id.tv_cashier);
        mLlType = findViewById(R.id.ll_type);
        mTvType = findViewById(R.id.tv_type);

        mButton.setText(getString(R.string.btn_bind_code_ensure));
        mTvTitle.setText(getString(R.string.tv_store_code));
        mButton.setVisibility(View.VISIBLE);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mStoreCodeList.clear();
                        currentPage = 2;
                        storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mStoreCodeList.clear();
                        currentPage = 2;
                        storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        initRecyclerView();
        initSwipeRefreshLayout();

        mShopPopLayout = (LinearLayout) findViewById(R.id.shop_pop_layout);
        mEtStoreInput = (EditTextDelete) findViewById(R.id.et_store_input);
        mStoreRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_store);
        mStoreLinearLayoutManager = new CustomLinearLayoutManager(this);
        mStoreRecyclerView.setLayoutManager(mStoreLinearLayoutManager);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);

        mCashierPopLayout = (LinearLayout) findViewById(R.id.cashier_pop_layout);
        mEtCashierInput = (EditTextDelete) findViewById(R.id.et_cashier_input);
        CustomLinearLayoutManager cashierLinearLayoutManager = new CustomLinearLayoutManager(this);
        mCashierRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_cashier);
        mCashierRecyclerView.setLayoutManager(cashierLinearLayoutManager);
        mEtCashierInput.setClearImage(R.mipmap.ic_search_clear);

        mTypePopLayout = (LinearLayout) findViewById(R.id.type_pop_layout);
        CustomLinearLayoutManager typeLinearLayoutManager = new CustomLinearLayoutManager(this);
        mTypeRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_type);
        mTypeRecyclerView.setLayoutManager(typeLinearLayoutManager);

    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mIvQuestion.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlCashier.setOnClickListener(this);
        mLlType.setOnClickListener(this);
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mUserId = extras.getString(Constants.INTENT_USER_ID);
            mStores = (List<StoresBean>) extras.getSerializable(Constants.INTENT_USER_STORE);
        }
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mCashierList = new ArrayList<>();
        mOriginCashierList = new ArrayList<>();
        mTypeList = new ArrayList<>();
        mStoreCodeList = new ArrayList<>();
        mStoreId = "";
        mCashierId = "";
        mTypeId = "";

//        mStoreAdapter = new ShopRecyclerAdapter(StoreCodeListActivity.this, mShopList, mStoreId);
//        mCashierAdapter = new CashierRecyclerAdapter(StoreCodeListActivity.this, mCashierList, mCashierId);
        mAdapter = new StoreCodeListAdapter(StoreCodeListActivity.this, mStoreCodeList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new StoreCodeListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent;
               /* if (mStoreCodeList.get(position).getQrType() == 3 && !TextUtils.isEmpty(mStoreCodeList.get(position).getWxappid())) {
                    intent = new Intent(StoreCodeListActivity.this, StaticUpdateCodeActivity.class);
                } else if (mStoreCodeList.get(position).getQrType() == 4) {
                    intent = new Intent(StoreCodeListActivity.this, StaticHuabeiCodeActivity.class);
                } else {
                    intent = new Intent(StoreCodeListActivity.this, StaticCodeActivity.class);
                }*/
                intent = new Intent(StoreCodeListActivity.this, StaticCodeImageActivity.class);
                intent.putExtra(Constants.INTENT_QRCODE_STORE, mStoreCodeList.get(position));
                startActivity(intent);
            }
        });

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtCashierInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData(mEtCashierInput.getText().toString().trim());
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtCashierInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData("");
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        initStore();

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mStoreCodeList.clear();
            currentPage = 2;
            storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
        if (!TextUtils.isEmpty(mUserId)) {
            mLlShop.setVisibility(View.GONE);
            mLlCashier.setVisibility(View.GONE);
        } else {
            mLlShop.setVisibility(View.VISIBLE);
            mLlCashier.setVisibility(View.VISIBLE);
        }
    }

    private void initStore() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() || MyApplication.getIsManager()) {
            mStoreId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
            if (TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""));
            }
            mCashierId = mUserId;
        } else if (MyApplication.getIsCasher()) {
            mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            mLlShop.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            if (TextUtils.isEmpty(MyApplication.getOpCodeBill())) {
                mLlCashier.setEnabled(false);
                mIvCashierArrow.setVisibility(View.GONE);
            }
            mTvCashier.setText(MyApplication.getRealName());
            mCashierId = MyApplication.getUserId();
        }
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    storeCodeList(pageSize, currentPage, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                MtaUtils.mtaId(MyApplication.getContext(), "J007");
                Intent intent = new Intent(StoreCodeListActivity.this, BindingCodeActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BIND);
                intent.putExtra(Constants.INTENT_USER_ID, mUserId);
                intent.putExtra(Constants.INTENT_USER_STORE, (Serializable) mStores);
                startActivity(intent);
                break;
            case R.id.ll_shop:
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(3);
                }
                if (mShopSwitchArrow) {
                    closeArrow(1);
                } else {
                    loadShopData();
                }
                break;
            case R.id.ll_cashier:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(3);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                } else {
                    loadCashierData();
                }
                break;
            case R.id.ll_type:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(3);
                } else {
                    loadTypeData();
                    CommonRecyclerAdapter commonRecyclerAdapter = new CommonRecyclerAdapter(StoreCodeListActivity.this, mTypeList, mTypeId);
                    commonRecyclerAdapter.setOnItemClickListener(new CommonRecyclerAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            mTypeId = mTypeList.get(position).getUserId();
                            mTvType.setText(mTypeList.get(position).getRealName());
                            closeArrow(3);
                            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                mStoreCodeList.clear();
                                currentPage = 2;
                                storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, true);
                            } else {
                                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                            }
                        }
                    });
                    mTypeRecyclerView.setAdapter(commonRecyclerAdapter);
                    openArrow(3);
                }
                break;
            case R.id.iv_question:
                showNoticeDialog();
                break;
        }
    }

    private void showNoticeDialog() {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(StoreCodeListActivity.this, null, null, R.layout.notice_dialog_code_instruction);
            DialogHelper.resize(StoreCodeListActivity.this, mNoticeDialog, 0.8f);
        }
        mNoticeDialog.show();
    }

    public void closeArrow(int condition) {

        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mShopSwitchArrow = false;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mEtCashierInput.setText("");
                mCashierSwitchArrow = false;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.GONE);
                break;
            case 3:
                mTypeSwitchArrow = false;
                mIvTypeArrow.startAnimation(rotate);
                mTypePopLayout.setVisibility(View.GONE);
                break;
        }

    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mShopSwitchArrow = true;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.VISIBLE);
                break;
            case 2:
                mCashierSwitchArrow = true;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.VISIBLE);
                break;
            case 3:
                mTypeSwitchArrow = true;
                mIvTypeArrow.startAnimation(rotate);
                mTypePopLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIvShopArrow.clearAnimation();
        mIvCashierArrow.clearAnimation();
        mIvTypeArrow.clearAnimation();
    }

    private void loadShopData() {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginShopList.clear();
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            map.put("merchantDataType", "1");
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_STORE_CODE_CHOICE_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadCashierData() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginCashierList.clear();
            mCashierList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(mStoreId)) {
                map.put("storeId", mStoreId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), "TAG_STORE_CODE_CASHIER", map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private List<DataEntity> loadTypeData() {
        if (mTypeList == null) {
            mTypeList = new ArrayList<>();
        }
        mTypeList.clear();
        DataEntity dataEntity1 = new DataEntity();
        dataEntity1.setRealName("全部类型");
        dataEntity1.setUserId("");
        DataEntity dataEntity2 = new DataEntity();
        dataEntity2.setRealName("聚合码");
        dataEntity2.setUserId("1");
        DataEntity dataEntity3 = new DataEntity();
        dataEntity3.setRealName("银标码");
        dataEntity3.setUserId("2");
        DataEntity dataEntity4 = new DataEntity();
        dataEntity4.setRealName("升级版聚合码");
        dataEntity4.setUserId("3");
        DataEntity dataEntity5 = new DataEntity();
        dataEntity5.setRealName("花呗码");
        dataEntity5.setUserId("4");
        mTypeList.add(dataEntity1);
        mTypeList.add(dataEntity2);
        mTypeList.add(dataEntity3);
        mTypeList.add(dataEntity4);
        mTypeList.add(dataEntity5);
        return mTypeList;
    }

    private void storeCodeList(int pageSize, int currentPage, String search, String storeMerchantId, String cashierId, String qrType, boolean showLoading) {
        //门店网络请求
        if (showLoading) {
            DialogUtil.safeShowDialog(mLoadDialog);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("searchName", search);
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeId", storeMerchantId);
        }
        if (!TextUtils.isEmpty(cashierId)) {
            map.put("userId", mCashierId);
        }
        if (!TextUtils.isEmpty(qrType)) {
            map.put("qrType", mTypeId);
        }
        ServerClient.newInstance(MyApplication.getContext()).qrcodeList(MyApplication.getContext(), Constants.TAG_STORE_CODE_LIST, map);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_STORE_CODE_CHOICE_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreCodeListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        dataEntity.setStoreName(getString(R.string.tv_shop));
                        dataEntity.setStoreId("");
                        mOriginShopList.add(dataEntity);
                        mOriginShopList.addAll(msg.getData());
                        mShopList.addAll(mOriginShopList);
                        mStoreAdapter = new ShopRecyclerAdapter(StoreCodeListActivity.this, mShopList, mStoreId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                mStoreId = mShopList.get(position).getStoreId();
                                mCashierId = "";
                                mTvShop.setText(mShopList.get(position).getStoreName());
                                mTvCashier.setText(getString(R.string.tv_all_user));
                                closeArrow(1);
                                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                    mStoreCodeList.clear();
                                    currentPage = 2;
                                    storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, true);
                                } else {
                                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                                }
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        openArrow(1);
                    } else {
                        showCommonNoticeDialog(StoreCodeListActivity.this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals("TAG_STORE_CODE_CASHIER")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreCodeListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        DataEntity dataEntity = new DataEntity();
                        dataEntity.setRealName(getString(R.string.tv_all_user));
                        dataEntity.setUserId("");
                        mOriginCashierList.add(dataEntity);
                        mOriginCashierList.addAll(msg.getData());
                        mCashierList.addAll(mOriginCashierList);
                        mCashierAdapter = new CashierRecyclerAdapter(StoreCodeListActivity.this, mCashierList);
                        mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                mCashierId = mCashierList.get(position).getUserId();
                                mTvCashier.setText(mCashierList.get(position).getRealName());
                                closeArrow(2);
                                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                    mStoreCodeList.clear();
                                    currentPage = 2;
                                    storeCodeList(pageSize, 1, mEtInput.getText().toString().trim(), mStoreId, mCashierId, mTypeId, true);
                                } else {
                                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                                }
                            }
                        });
                        mCashierRecyclerView.setAdapter(mCashierAdapter);
                        mCashierAdapter.setSelected(mCashierId);
                        openArrow(2);
                    } else {
                        showCommonNoticeDialog(StoreCodeListActivity.this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_STORE_CODE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreCodeListBean msg = (StoreCodeListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(StoreCodeListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                        mStoreCodeList.addAll(msg.getData().getData());
                    } else {
                        if (isLoadmore) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            mAdapter.notifyDataSetChanged();
            isLoadmore = false;
            isRefreshed = false;
        }
    }


    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mStoreCodeList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            mShopList.addAll(mOriginShopList);
        } else {
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    private void setCashierData(String search) {
        mCashierList.clear();
        if (TextUtils.isEmpty(search)) {
            mCashierList.addAll(mOriginCashierList);
        } else {
            for (int i = 0; i < mOriginCashierList.size(); i++) {
                if (mOriginCashierList.get(i).getRealName().contains(search)) {
                    mCashierList.add(mOriginCashierList.get(i));
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_NAME_STATIC_CODE.equals(intentName)) {
            mEtInput.setText("");
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            mUserId = extras.getString(Constants.INTENT_USER_ID);
        if (!TextUtils.isEmpty(mUserId)) {
            mLlShop.setVisibility(View.GONE);
            mLlCashier.setVisibility(View.GONE);
        } else {
            mLlShop.setVisibility(View.VISIBLE);
            mLlCashier.setVisibility(View.VISIBLE);
        }
    }


}
