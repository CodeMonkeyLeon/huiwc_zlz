package com.hstypay.enterprise.activity.tinycashier;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.DeviceUserRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActivationCode;
import com.hstypay.enterprise.bean.CashierItem;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LinkEmployeeBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hstypay.enterprise.activity.tinycashier.ActivationCodeDetailsActivity.KEY_ACTIVATION_CODE;

/**
 * Created by admin on 2017/6/29.
 */
public class CashierChoiceActivity extends BaseActivity implements View.OnClickListener, DeviceUserRecyclerAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle, mButton;
    private RecyclerView mRecyclerView;
    private SafeDialog mLoadDialog;
    private List<LinkEmployeeBean.DataEntity> mDeviceEmployeeList;
    private List<LinkEmployeeBean.DataEntity> mAdapterUserList;
    private DeviceUserRecyclerAdapter mAdapter;
    private CommonNoticeDialog mDialogSuccess;
    private EditTextDelete mEtInput;
    private ActivationCode mActivationCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_choice);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(CashierChoiceActivity.this, getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtInput = (EditTextDelete) findViewById(R.id.et_input);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mEtInput.setClearImage(R.mipmap.ic_search_clear);

        mTvTitle.setText(R.string.tv_device_cashier_set);
        mButton.setVisibility(View.INVISIBLE);
    }

    private void initData() {
        mDeviceEmployeeList = new ArrayList<>();
        mAdapterUserList = new ArrayList<>();
        List<CashierItem> cashierItems = (List<CashierItem>) getIntent().getSerializableExtra(Constants.INTENT_DEVICE_EMPLOYEE_LIST);
        mActivationCode = (ActivationCode) getIntent().getSerializableExtra(KEY_ACTIVATION_CODE);
        if (cashierItems != null && cashierItems.size() > 0) {
            for (int i = 0; i < cashierItems.size(); i++) {
                CashierItem item = cashierItems.get(i);
                LinkEmployeeBean.DataEntity dataEntity = new LinkEmployeeBean.DataEntity();
                if (cashierItems.get(i).getUserId().equals(mActivationCode.getUserId())) {
                    dataEntity.setBind(1);
                }
                dataEntity.setName(item.getName());
                dataEntity.setPhone(item.getPhone());
                dataEntity.setRole(item.getRole());
                dataEntity.setUserId(item.getUserId());
                if (cashierItems.get(i).getRole() == 4) {
                    mDeviceEmployeeList.add(0, dataEntity);
                } else {
                    mDeviceEmployeeList.add(dataEntity);
                }
            }

            if (mDeviceEmployeeList != null && mDeviceEmployeeList.size() > 0) {
                CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(MyApplication.getContext());
                mRecyclerView.setLayoutManager(layoutManager);
                mAdapterUserList.addAll(mDeviceEmployeeList);
                mAdapter = new DeviceUserRecyclerAdapter(MyApplication.getContext(), mAdapterUserList);
                if (!MyApplication.getIsCasher()) {
                    mAdapter.setOnItemClickListener(this);
                }
                mRecyclerView.setAdapter(mAdapter);
            }
        }

        mEtInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mAdapterUserList.clear();
                mAdapterUserList.addAll(getAdapterData(mDeviceEmployeeList, mEtInput.getText().toString().trim()));
                mAdapter.notifyDataSetChanged();
                return true;
            }
            return false;
        });
        mEtInput.setOnEditChangedListener(isClear -> {
            if (isClear) {
                mAdapterUserList.clear();
                mAdapterUserList.addAll(mDeviceEmployeeList);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LINK_EMPLOYEE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CashierChoiceActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    LinkEmployeeBean.DataEntity dataEntity = new LinkEmployeeBean.DataEntity();
                    for (int i = 0; i < mDeviceEmployeeList.size(); i++) {
                        if (!TextUtils.isEmpty(mDeviceEmployeeList.get(i).getUserId()) && mDeviceEmployeeList.get(i).getUserId().equals(mActivationCode.getUserId())) {
                            mDeviceEmployeeList.get(i).setBind(1);
                            dataEntity = mDeviceEmployeeList.get(i);
                        } else {
                            mDeviceEmployeeList.get(i).setBind(0);
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                    getDialogSuccess(getString(R.string.dialog_set_success), dataEntity);
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //返回
            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void userBind(String id, String userId) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", id);
            map.put("userId", userId);
            ServerClient.newInstance(MyApplication.getContext()).activationCodeUserBind(MyApplication.getContext(), Constants.TAG_LINK_EMPLOYEE, map);
        }
    }

    private List<LinkEmployeeBean.DataEntity> getAdapterData(List<LinkEmployeeBean.DataEntity> userListBean, String search) {
        List<LinkEmployeeBean.DataEntity> list = new ArrayList<>();
        if (userListBean != null && userListBean.size() > 0) {
            for (int i = 0; i < userListBean.size(); i++) {
                if ((userListBean.get(i).getPhone() != null && userListBean.get(i).getPhone().contains(search))
                        || (userListBean.get(i).getName() != null && userListBean.get(i).getName().contains(search))) {
                    list.add(userListBean.get(i));
                }
            }
        }
        return list;
    }

    public void getDialogSuccess(String title, final LinkEmployeeBean.DataEntity dataEntity) {
        mDialogSuccess = new CommonNoticeDialog(CashierChoiceActivity.this, title, getString(R.string.dialog_notice_button));
        mDialogSuccess.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                CashierItem cashierItem = new CashierItem(dataEntity.getName(), dataEntity.getPhone(),
                        dataEntity.getRole(), dataEntity.getUserId());
                Intent intent = new Intent();
                intent.putExtra(Constants.RESULT_DEVICE_EMPLOYEE_LIST, cashierItem);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        DialogHelper.resize(CashierChoiceActivity.this, mDialogSuccess);
        mDialogSuccess.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDialogSuccess != null) {
            mDialogSuccess.dismiss();
        }
    }

    @Override
    public void onItemClick(String userId) {
        mActivationCode.setUserId(userId);
        userBind(String.valueOf(mActivationCode.getId()), mActivationCode.getUserId());
    }
}
