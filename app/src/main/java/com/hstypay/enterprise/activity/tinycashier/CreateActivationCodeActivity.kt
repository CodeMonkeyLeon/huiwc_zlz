package com.hstypay.enterprise.activity.tinycashier

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.view.View
import com.hstypay.enterprise.R
import com.hstypay.enterprise.Widget.dialog.ActivationCodeHelpDialog
import com.hstypay.enterprise.activity.ShopActivity
import com.hstypay.enterprise.app.MyApplication
import com.hstypay.enterprise.bean.StoreListBean
import com.hstypay.enterprise.commonlib.base.AppActivity
import com.hstypay.enterprise.utils.Constants
import com.hstypay.enterprise.utils.SpUtil
import com.hstypay.enterprise.utils.StringUtils
import com.hstypay.enterprise.utils.TextInputHelper
import com.hstypay.enterprise.viewmodel.CreateActivationCodeViewMode
import kotlinx.android.synthetic.main.activity_create_activation_code.*
import kotlinx.android.synthetic.main.title_report.*

/**
 * @Author dean.zeng
 * @Description  微收银激活码创建
 * @Date 2020-07-17 15:50
 **/
class CreateActivationCodeActivity : AppActivity<CreateActivationCodeViewMode>() {


    private lateinit var storeMerchantId: String

    companion object {
        const val REQUEST_SHOP_BEAN_CODE = 0x92
    }

    private lateinit var textInputHelper: TextInputHelper

    override fun initData() {
        iv_back.setOnClickListener { finish() }
        tv_title.text = "创建激活码"
        iv_button.setImageResource(R.mipmap.question_circle)
        iv_button.visibility = View.VISIBLE
        iv_button.setOnClickListener {
            ActivationCodeHelpDialog(this).show()
        }
        tvMerchantName.text= SpUtil.getString(MyApplication.getContext(), Constants.SP_SHORT_NAME)
        llStoreName.setOnClickListener {
            storeMerchantId = if (!this::storeMerchantId.isInitialized) {
                ""
            } else {
                storeMerchantId
            }
            startActivityForResult(Intent(this, ShopActivity::class.java)
                    .putExtra(Constants.INTENT_STORE_ID, storeMerchantId), REQUEST_SHOP_BEAN_CODE)
        }
        textInputHelper = TextInputHelper(btnSubmit)
        textInputHelper.addViews(tvStoreMerchantName, tvStoreMerchantShortName)
        btnSubmit.setOnClickListener {
            if (StringUtils.managerName10People(tvStoreMerchantShortName.text.toString())) {
                mViewModel.addActivationCode(storeMerchantId, tvStoreMerchantShortName.text.toString())
            } else {
                showCommonNoticeDialog(this, getString(R.string.regex_manager_name_10_People))
            }
        }


    }


    override fun onDestroy() {
        super.onDestroy()
        textInputHelper.removeViews()
    }

    override fun initView() {
        mViewModel.successLiveData.observe(this, Observer {//创建成功
            showModal("创建成功") {
                val intent = Intent()
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })
    }

    override fun getLayoutId(): Int = R.layout.activity_create_activation_code


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SHOP_BEAN_CODE && data != null) {
            val extras = data.extras
            val shopBean = extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT) as StoreListBean.DataEntity
            tvStoreMerchantName.text = shopBean.storeName
            storeMerchantId = shopBean.storeId
        }
    }

}