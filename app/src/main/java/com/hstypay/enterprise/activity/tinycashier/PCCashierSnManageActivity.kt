package com.hstypay.enterprise.activity.tinycashier

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import com.hstypay.enterprise.R
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager
import com.hstypay.enterprise.Widget.MyToast
import com.hstypay.enterprise.adapter.ActivationCodeAdapter
import com.hstypay.enterprise.adapter.CommonRecyclerAdapter
import com.hstypay.enterprise.adapter.DataEntity
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter
import com.hstypay.enterprise.app.MyApplication
import com.hstypay.enterprise.bean.ActivationCode
import com.hstypay.enterprise.bean.StoreList
import com.hstypay.enterprise.bean.StoreListBean
import com.hstypay.enterprise.commonlib.base.AppActivity
import com.hstypay.enterprise.utils.*
import com.hstypay.enterprise.viewmodel.PCCashierSnManageViewModel
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout.SHSOnRefreshListener
import kotlinx.android.synthetic.main.activity_create_activation_code.*
import kotlinx.android.synthetic.main.activity_pc_cashier_sn_manage.*
import kotlinx.android.synthetic.main.title_report.*

/**
 * @Author dean.zeng
 * @Description PC收银激活码管理
 * @Date 2020-07-16 16:56
 **/
class PCCashierSnManageActivity : AppActivity<PCCashierSnManageViewModel>() {


    private var currentPage = 2
    private val mState = arrayOf("全部状态", "未激活", "已激活")
    private lateinit var mStoreList: MutableList<StoreList>

    private var isRefreshed = false//刷新过

    private var isLoadmore = false //加载更多


    companion object {
        const val REQUEST_ACTIVATION_CODE_REFRESH = 0x9a
    }

    //激活码列表
    private val mAdapter = ActivationCodeAdapter(arrayListOf())

    private val mRotate by lazy {
        val rotate = AnimationUtils.loadAnimation(this, R.anim.rotate)
        rotate.interpolator = LinearInterpolator()
        rotate.fillAfter = true
        rotate
    }
    private lateinit var mStoreId: String
    private lateinit var commonRecyclerAdapter: CommonRecyclerAdapter
    private lateinit var storeAdapter: ShopRecyclerAdapter

    private var mStateId = -1

    private val mRotateReverse by lazy {
        val rotateReverse = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse)
        rotateReverse.interpolator = LinearInterpolator()
        rotateReverse.fillAfter = true
        rotateReverse
    }


    override fun initData() {
        iv_back.setOnClickListener { finish() }
        tv_title.setText(R.string.pc_cashier_sn_manage)
        button_title.setText(R.string.create)
        button_title.setOnClickListener {
            startActivityForResult(Intent(this, CreateActivationCodeActivity::class.java), REQUEST_ACTIVATION_CODE_REFRESH)
        }
        ll_shop.setOnClickListener {
            if (type_pop_layout.visibility == View.VISIBLE) {
                closeArrow(2)
            }
            if (shop_pop_layout.visibility == View.VISIBLE) {
                closeArrow(1)
            } else {
                MyApplication.getBluePrintSetting()
                if (this::mStoreList.isInitialized) {
                    initShopList(mStoreList)
                    openArrow(1)
                } else {
                    mViewModel.findStores()
                }
            }
        }
        ll_type.setOnClickListener {
            if (shop_pop_layout.visibility == View.VISIBLE) {
                closeArrow(1)
            }
            if (type_pop_layout.visibility == View.VISIBLE) {
                closeArrow(2)
            } else {
                openArrow(2)
            }
        }
        et_store_input.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                setShopData(et_store_input.text.toString().trim { it <= ' ' })
                return@OnEditorActionListener true
            }
            false
        })
        et_store_input.setOnEditChangedListener { isClear ->
            if (isClear) {
                setShopData("")
            }
        }
        val stateList = mutableListOf<DataEntity>()
        mState.forEachIndexed { index, s ->
            val item = DataEntity()
            item.realName = s
            item.userId = (index - 1).toString()
            stateList.add(item)
        }
        commonRecyclerAdapter = CommonRecyclerAdapter(this, stateList, mStateId.toString())
        commonRecyclerAdapter.setOnItemClickListener { position ->
            mStateId = stateList[position].userId.toInt()
            tv_type.text = stateList[position].realName
            closeArrow(2)
            onRefreshData()
        }
        recyclerView_type.layoutManager = LinearLayoutManager(this)
        recyclerView_type.adapter = commonRecyclerAdapter
        initSwipeRefreshLayout()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mAdapter
        mStoreId = SpStayUtil.getString(this, MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "")
        onRefreshData()
        mAdapter.setOnItemClickListener { adapter, view, position ->
            val item = mAdapter.getItem(position)
            item?.let {
                startActivity(Intent(this, ActivationCodeDetailsActivity::class.java)
                        .putExtra(ActivationCodeDetailsActivity.KEY_ACTIVATION_CODE, item))
            }
        }

    }


    private fun setShopData(search: String) {
        if (TextUtils.isEmpty(search)) {
            if (this::mStoreList.isInitialized)
                initShopList(mStoreList)
        } else {
            val list = mStoreList.filter { it.storeName.contains(search) }
            initShopList(list)
        }
    }


    override fun initView() {
        mViewModel.storeList.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                showModal(getString(R.string.tv_moment_null))
            } else {
                val shoreAll = StoreList(mStoreId, getString(R.string.tv_shop))
                mStoreList = mutableListOf()
                mStoreList.add(shoreAll)
                mStoreList.addAll(it)
                initShopList(mStoreList)
                openArrow(1)
            }
        })
        mViewModel.activationCodeList.observe(this, Observer {
            if (it == null) {
                setErrorState(isRefreshed, isLoadmore, 500)
            } else {
                setSuccessState(isRefreshed, isLoadmore, 200)
                if (it.isNotEmpty()) {
                    swipeRefreshLayout.visibility = View.VISIBLE
                    tv_null.visibility = View.GONE
                    mAdapter.addData(it)
                } else {
                    if (isLoadmore) {
                        swipeRefreshLayout.visibility = View.VISIBLE
                        tv_null.visibility = View.GONE
                        MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT)
                    } else {
                        swipeRefreshLayout.setVisibility(View.GONE)
                        tv_null.setVisibility(View.VISIBLE)
                    }
                }
            }
            isLoadmore = false
            isRefreshed = false
        })
    }


    private fun initShopList(it: List<StoreList>) {
        val storeCodeList = mutableListOf<StoreListBean.DataEntity>()
        it.forEach {
            val item = StoreListBean().DataEntity()
            item.storeId = it.storeId
            item.storeName = it.storeName
            storeCodeList.add(item)
        }
        storeAdapter = ShopRecyclerAdapter(this, storeCodeList, mStoreId)

        storeAdapter.setOnItemClickListener(ShopRecyclerAdapter.OnRecyclerViewItemClickListener { position ->
            mStoreId = mStoreList[position].storeId
            tv_shop.text = mStoreList[position].storeName
            onRefreshData()
            closeArrow(1)
        })
        recyclerView_store.layoutManager = LinearLayoutManager(this)
        recyclerView_store.layoutManager = CustomLinearLayoutManager(this)
        et_store_input.setClearImage(R.mipmap.ic_search_clear)
        recyclerView_store.adapter = storeAdapter
    }

    override fun getLayoutId(): Int = R.layout.activity_pc_cashier_sn_manage


    fun closeArrow(condition: Int) {
        when (condition) {
            1 -> {
                et_store_input.setText("")
                iv_shop_arrow.startAnimation(mRotateReverse)
                shop_pop_layout.visibility = View.GONE
            }
            2 -> {
                iv_type_arrow.startAnimation(mRotateReverse)
                type_pop_layout.visibility = View.GONE
            }
        }
    }

    private fun openArrow(condition: Int) {
        when (condition) {
            1 -> {
                iv_shop_arrow.startAnimation(mRotate)
                shop_pop_layout.visibility = View.VISIBLE
            }
            2 -> {
                iv_type_arrow.startAnimation(mRotate)
                type_pop_layout.visibility = View.VISIBLE
            }
        }
    }


    private fun initSwipeRefreshLayout() {
        if (AppHelper.getSwipeRefresh()) {
            swipeRefreshLayout.setFooterView(R.layout.refresh_view)
            swipeRefreshLayout.setHeaderView(R.layout.refresh_view)
        }
        swipeRefreshLayout.setOnRefreshListener(object : SHSOnRefreshListener {
            override fun onRefresh() {//刷新
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    onRefreshData()
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT)
                    swipeRefreshLayout.postDelayed({ swipeRefreshLayout.finishRefresh() }, 500)
                }
            }

            override fun onLoading() {//加载
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true
                    getActivationData(currentPage)
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT)
                    swipeRefreshLayout.postDelayed({ swipeRefreshLayout.finishLoadmore() }, 500)
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            override fun onRefreshPulStateChange(percent: Float, state: Int) {
                when (state) {
                    SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT -> swipeRefreshLayout.setRefreshViewText("下拉刷新")
                    SHSwipeRefreshLayout.OVER_TRIGGER_POINT -> swipeRefreshLayout.setRefreshViewText("松开刷新")
                    SHSwipeRefreshLayout.START -> swipeRefreshLayout.setRefreshViewText("正在刷新")
                }
            }

            override fun onLoadmorePullStateChange(percent: Float, state: Int) {
                when (state) {
                    SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT ->                         //textView.setText("上拉加载");
                        swipeRefreshLayout.setLoaderViewText("上拉加载")
                    SHSwipeRefreshLayout.OVER_TRIGGER_POINT ->                         //textView.setText("松开加载");
                        swipeRefreshLayout.setLoaderViewText("松开加载")
                    SHSwipeRefreshLayout.START ->                         //textView.setText("正在加载");
                        swipeRefreshLayout.setLoaderViewText("正在加载")
                }
            }
        })
    }


    private fun onRefreshData() {
        isRefreshed = true
        currentPage = 2
        getActivationData(1)
    }


    private fun getActivationData(currentPage: Int) {
        mViewModel.getActivationData("15", currentPage.toString(), mStateId.toString(), mStoreId)
    }


    private fun setErrorState(isRefreshed: Boolean, isLoadmore: Boolean, delay: Long) {
        if (isRefreshed) {
            swipeRefreshLayout.postDelayed({ swipeRefreshLayout.finishRefresh() }, delay)
        }
        if (isLoadmore) {
            swipeRefreshLayout.postDelayed({ swipeRefreshLayout.finishLoadmore() }, delay)
        }
    }

    private fun setSuccessState(isRefreshed: Boolean, isLoadmore: Boolean, delay: Long) {
        if (isRefreshed) {
            mAdapter.setNewData(arrayListOf<ActivationCode>())
            currentPage = 2
            swipeRefreshLayout.postDelayed({ swipeRefreshLayout.finishRefresh() }, delay)
        }
        if (isLoadmore) {
            swipeRefreshLayout.postDelayed({
                currentPage++
                swipeRefreshLayout.finishLoadmore()
            }, delay)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_ACTIVATION_CODE_REFRESH) {
            if (this::commonRecyclerAdapter.isInitialized) {
                mStateId = -1
                tv_type.text = mState[0]
                commonRecyclerAdapter.setUserId("-1")
                commonRecyclerAdapter.notifyDataSetChanged()
            }

            if (this::storeAdapter.isInitialized) {
                mStoreId = SpStayUtil.getString(this, MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "")
                tv_shop.text = getString(R.string.tv_shop)
                storeAdapter.setStoreId(mStoreId)
                storeAdapter.notifyDataSetChanged()
            }
            onRefreshData()
        }
    }

}