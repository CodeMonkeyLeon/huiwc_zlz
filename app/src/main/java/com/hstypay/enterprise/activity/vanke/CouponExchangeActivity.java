package com.hstypay.enterprise.activity.vanke;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.bean.vanke.CouponListBean;
import com.hstypay.enterprise.bean.vanke.VipInfoBean;
import com.hstypay.enterprise.bean.vanke.VipInfoData;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class CouponExchangeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvVankeVip, mIvVankeCoupon;
    private TextView mTvTitle, mBtnTitle;
    private Button mBtnSubmit;
    private ScrollView mSvVankeCoupon;
    private RelativeLayout mRlVankeVip, mRlVankeCoupon;
    private EditText mEtVankeVip;
    private CheckBox mCbMaoku, mCbMeituan;
    private SafeDialog mLoadDialog;
    private String mStoreId;
    private String mInterProviderType = "2";//目前默认只有印享星才有兑换券
    private VipInfoData mVipInfo;
    private List<CouponInfoData> mCouponList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_exchange);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mSvVankeCoupon = findViewById(R.id.sv_vanke_coupon);
        mRlVankeVip = findViewById(R.id.rl_vanke_vip);
        mRlVankeCoupon = findViewById(R.id.rl_vanke_coupon);
        mEtVankeVip = findViewById(R.id.et_vanke_vip);
        mCbMaoku = findViewById(R.id.cb_coupon_maoku);
        mCbMeituan = findViewById(R.id.cb_coupon_meituan);
        mIvVankeVip = findViewById(R.id.iv_vanke_vip);
        mIvVankeCoupon = findViewById(R.id.iv_vanke_coupon);
        mBtnSubmit = findViewById(R.id.btn_submit);

        mTvTitle.setText(R.string.tx_coin_certificate);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvVankeVip.setOnClickListener(this);
        mIvVankeCoupon.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mEtVankeVip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    mBtnSubmit.setBackgroundResource(R.color.theme_color_fifty);
                    mBtnSubmit.setEnabled(false);
                } else {
                    mBtnSubmit.setBackgroundResource(R.color.theme_color);
                    mBtnSubmit.setEnabled(true);
                }
            }
        });
    }

    private void initData() {
        mCouponList = new ArrayList<>();
        mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVipInfo = null;
        mCouponList.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_vanke_vip:
                capture(Constants.INTENT_VANKE_VIP);
                break;
            case R.id.btn_submit:
                getUserInfo(mEtVankeVip.getText().toString().trim());
                break;
        }
    }

    private void getUserInfo(String vipNo) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("mallCardNo", vipNo);
            map.put("storeMerchantId", mStoreId);
            ServerClient.newInstance(MyApplication.getContext()).getUserInfo(MyApplication.getContext(), "TAG_EXCHANGE_VIP_INFO", map);
        }
    }

    private void getCanUseByMallCardNo(VipInfoData vipInfo) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("mallCardNo", vipInfo.getMallCardNo());
            map.put("bizUid", vipInfo.getBizUid());
            map.put("storeMerchantId", mStoreId);
            if (mInterProviderType != null && !"-1".equals(mInterProviderType))
                map.put("interProviderType", mInterProviderType);
            map.put("couponType", 3);
            ServerClient.newInstance(MyApplication.getContext()).getCanUseByMallCardNo(MyApplication.getContext(), "TAG_EXCHANGE_VIP_COUPON", map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_EXCHANGE_VIP_INFO")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            VipInfoBean msg = (VipInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mVipInfo = msg.getData();
                    if (mVipInfo != null) {
                        getCanUseByMallCardNo(mVipInfo);
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.error_vip_info));
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_EXCHANGE_VIP_COUPON")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponListBean msg = (CouponListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<CouponInfoData> couponList = msg.getData();
                    if (couponList != null && couponList.size() > 0) {
                        mCouponList.addAll(couponList);
                    }
                    toOrderCouponInfo();
                    break;
            }
        }
    }

    private void toOrderCouponInfo() {
        if (TextUtils.isEmpty(mStoreId)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(this, OrderCouponInfoActivity.class);
        intent.putExtra(Constants.INTENT_VANKE_COUPON, (Serializable) mCouponList);
        intent.putExtra(Constants.INTENT_VANKE_VIP, mVipInfo);
        intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
        intent.putExtra(Constants.INTENT_COUPON_TYPE, 3);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        startActivity(intent);
    }

    private void capture(String intentName) {
        if (TextUtils.isEmpty(mStoreId)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }

        Intent intent = new Intent(this, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        intent.putExtra(Constants.INTENT_VIP_NUMBER, mEtVankeVip.getText().toString().trim());
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        intent.putExtra(Constants.INTENT_COUPON_TYPE, 3);
        if (mRlVankeCoupon.getVisibility() == View.VISIBLE) {
            intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
        }
        startActivity(intent);
    }
}
