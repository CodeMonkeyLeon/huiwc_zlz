package com.hstypay.enterprise.activity.vanke;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.vanke.CouponReverseBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class CouponVerifyActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvVerify;
    private TextView mTvTitle, mBtnTitle, mTvVerify, mTvVerifyNotice;
    private Button mBtnBackHome, mBtnContinue;
    private SafeDialog mLoadDialog;
    private int count;
    private String mStoreId;
    private boolean mUsedCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_verify);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mIvVerify = findViewById(R.id.iv_verify);
        mTvVerify = findViewById(R.id.tv_verify);
        mTvVerifyNotice = findViewById(R.id.tv_verify_notice);
        mBtnBackHome = findViewById(R.id.btn_back_home);
        mBtnContinue = findViewById(R.id.btn_continue);

    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnBackHome.setOnClickListener(this);
        mBtnContinue.setOnClickListener(this);
    }

    private void initData() {
        String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        String errorString = getIntent().getStringExtra(Constants.INTENT_COUPON_CANCEL_FAILED);
        mStoreId = getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
        mUsedCoupon = getIntent().getBooleanExtra(Constants.INTENT_USED_COUPON,false);//是否使用了优惠券
        switch (intentName) {
            case Constants.INTENT_COUPON_VERIFY_SUCCESS:
                mTvTitle.setText(R.string.title_coupon_verify_success);
                mTvVerify.setText(R.string.tv_coupon_verify_success);
                mIvVerify.setImageResource(R.mipmap.icon_success);
                mTvVerifyNotice.setVisibility(View.INVISIBLE);
                break;
            case Constants.INTENT_COUPON_CANCEL_SUCCESS:
                mIvVerify.setImageResource(R.mipmap.icon_success);
                if (mUsedCoupon) {
                    mTvTitle.setText(R.string.title_coupon_reverse_success);
                    mTvVerify.setText(R.string.tv_coupon_reverse_success);
                    mTvVerifyNotice.setText(R.string.tv_coupon_reverse_success_notice);
                } else {
                    mTvTitle.setText("优惠撤销成功");
                    mTvVerify.setText("所有优惠撤销成功");
                    mTvVerifyNotice.setText("");
                }
                break;
            case Constants.INTENT_COUPON_CANCEL_FAILED:
                if (mUsedCoupon) {
                    mTvTitle.setText(R.string.title_coupon_reverse_success);
                    mTvVerify.setText(R.string.tv_coupon_reverse_success);
                    mTvVerifyNotice.setText(R.string.tv_coupon_reverse_success_notice);
                } else {
                    mTvTitle.setText("优惠撤销失败");
                    mTvVerify.setText("优惠撤销失败");
                    mTvVerifyNotice.setText("");
                }
                mIvVerify.setImageResource(R.mipmap.icon_check_failed);
                mTvVerifyNotice.setVisibility(View.VISIBLE);
                mTvVerifyNotice.setText(errorString);
                if (getIntent().getBooleanExtra(Constants.INTENT_NOT_NEED_REVERSE_COUPON, false)) {
                    mBtnContinue.setVisibility(View.GONE);
                } else {
                    mBtnContinue.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                toMainActivity();
                break;
            case R.id.btn_back_home:
                toMainActivity();
                break;
            case R.id.btn_continue:
                cancelUse(getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO));
                break;
        }
    }

    private void cancelUse(String outTradeNo) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("outTradeNo", outTradeNo);
            map.put("storeMerchantId", mStoreId);
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            ServerClient.newInstance(MyApplication.getContext()).cancelUse(MyApplication.getContext(), Constants.TAG_COUPON_CONTINUE_CANCEL, map);
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_COUPON_CONTINUE_CANCEL)) {
            count++;
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponReverseBean msg = (CouponReverseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    mTvTitle.setText(R.string.title_coupon_reverse_failed);
                    mIvVerify.setImageResource(R.mipmap.icon_check_failed);
                    mTvVerify.setText(R.string.tv_coupon_reverse_failed);
                    if (count >= 3) {
                        mBtnContinue.setVisibility(View.GONE);
                        mTvVerifyNotice.setText(R.string.tv_verify_failed_notice);
                    } else {
                        mTvVerifyNotice.setText(R.string.net_error);
                        mBtnContinue.setVisibility(View.VISIBLE);
                    }
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    mTvTitle.setText(R.string.title_coupon_reverse_failed);
                                    mIvVerify.setImageResource(R.mipmap.icon_check_failed);
                                    mTvVerify.setText(R.string.tv_coupon_reverse_failed);
                                    if (count >= 3) {
                                        mBtnContinue.setVisibility(View.GONE);
                                        mTvVerifyNotice.setText(R.string.tv_verify_failed_notice);
                                    } else {
                                        mTvVerifyNotice.setText(msg.getError().getMessage());
                                        mBtnContinue.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    CouponReverseBean.CouponReverseData couponReverseData = msg.getData();
                    if (couponReverseData != null) {
                        if (couponReverseData.getReverseResult() == 0) {
                            mTvTitle.setText(R.string.title_coupon_reverse_success);
                            mIvVerify.setImageResource(R.mipmap.icon_success);
                            mTvVerify.setText(R.string.tv_coupon_reverse_success);
                            mTvVerifyNotice.setText(R.string.tv_coupon_reverse_success_notice);
                        } else {
                            mTvTitle.setText(R.string.title_coupon_reverse_failed);
                            mIvVerify.setImageResource(R.mipmap.icon_check_failed);
                            mTvVerify.setText(R.string.tv_coupon_reverse_failed);
                            if (couponReverseData.isNeedReverse()) {
                                if (count >= 3) {
                                    mBtnContinue.setVisibility(View.GONE);
                                    mTvVerifyNotice.setText(R.string.tv_verify_failed_notice);
                                } else {
                                    mTvVerifyNotice.setText("");
                                    mBtnContinue.setVisibility(View.VISIBLE);
                                }
                            } else {
                                mTvTitle.setText(R.string.title_coupon_reverse_failed);
                                mIvVerify.setImageResource(R.mipmap.icon_check_failed);
                                mTvVerify.setText(R.string.tv_coupon_reverse_failed);
                                mBtnContinue.setVisibility(View.GONE);
                                mTvVerifyNotice.setText(R.string.tv_verify_failed_notice);
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        toMainActivity();
        super.onBackPressed();
    }

    private void toMainActivity() {
        Intent intent = new Intent(Constants.ACTION_RECEIVE_RESET_DATA);
        sendBroadcast(intent);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
