package com.hstypay.enterprise.activity.vanke;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.PayResultActivity;
import com.hstypay.enterprise.adapter.vanke.CouponInfoAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Coupon;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.vanke.CouponCheckBean;
import com.hstypay.enterprise.bean.vanke.CouponCheckData;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.bean.vanke.CouponVerifyBean;
import com.hstypay.enterprise.bean.vanke.VipInfoData;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class OrderCouponInfoActivity extends BaseActivity {
    private ImageView mIvBack;
    private CheckBox mCbVipRight;
    private TextView mTvTitle, mBtnTitle, mTvVipName, mTvVipPhone, mTvVipBirthday, mTvVipPoint, mTvVipLevel, mTvAddCoupon;
    private TextView mTvOrderMoney, mTvCouponMoney, mTvCutMoney, mTvVipRightMoney, mTvActualMoney, mTvVerify;
    private SafeDialog mLoadDialog;
    private RecyclerView mRecyclerView;
    private LinearLayout mLlTop, mLlCouponMoney;
    private RelativeLayout mRlVipRight;
    private List<CouponInfoData> mCouponList;
    private List<CouponInfoData> mCouponCheckList;
    private CouponInfoAdapter mAdapter;
    private VipInfoData mVipInfo;
    private String mPayRemark, mInterProviderType;
    private int mCouponType;//券类型
    private double doubleMoney;
    private double couponMoney;
    private double vipRightMoney;
    private double cutMoney;
    private double actualMoney;
    private long totalCouponMoney;
    private String money;
    private String mStoreId;
    private int mPosition = -1;
    private CouponCheckData mCouponCheckData;
    private boolean isClickCheckbox;
    private int clickType = 0;//默认
    private boolean isAddCoupon;//添加券

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_coupon_info);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_coupon_info);
        mBtnTitle.setVisibility(View.INVISIBLE);

        mTvVipName = findViewById(R.id.tv_vip_name);
        mTvVipPhone = findViewById(R.id.tv_vip_phone);
        mTvVipBirthday = findViewById(R.id.tv_vip_birthday);
        mTvVipPoint = findViewById(R.id.tv_vip_point);
        mTvVipLevel = findViewById(R.id.tv_vip_level);
        mTvAddCoupon = findViewById(R.id.tv_add_coupon);
        mTvOrderMoney = findViewById(R.id.tv_order_money);
        mTvCouponMoney = findViewById(R.id.tv_coupon_money);
        mTvCutMoney = findViewById(R.id.tv_cut_money);
        mTvVipRightMoney = findViewById(R.id.tv_vip_right_money);
        mTvActualMoney = findViewById(R.id.tv_actual_money);
        mTvVerify = findViewById(R.id.tv_verify);
        mRecyclerView = findViewById(R.id.recyclerView);
        mCbVipRight = findViewById(R.id.cb_vip_right);
        mLlTop = findViewById(R.id.ll_top);
        mLlCouponMoney = findViewById(R.id.ll_coupon_money);
        mRlVipRight = findViewById(R.id.rl_vip_right);
    }

    private void initListener() {
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        finish();
                        break;
                    case R.id.tv_add_coupon:
                        Intent intent = new Intent(OrderCouponInfoActivity.this, CaptureActivity.class);
                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_ADD_VANKE_COUPON);
                        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
                        intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
                        startActivityForResult(intent, Constants.REQUEST_VANKE_COUPON);
                        break;
                    case R.id.cb_vip_right:
                        mCouponCheckList.clear();
                        for (int i = 0; i < mCouponList.size(); i++) {
                            if (mCouponList.get(i).isChecked() || ("1".equals(mInterProviderType) && mCouponList.get(i).getNeedUseCount() >= 0)) {
                                mCouponCheckList.add(mCouponList.get(i));
                            }
                        }
                        isClickCheckbox = true;
                        getPayInfo();
                        break;
                    case R.id.tv_verify:
                        mCouponCheckList.clear();
                        for (int i = 0; i < mCouponList.size(); i++) {
                            if (mCouponList.get(i).isChecked() || mCouponList.get(i).getNeedUseCount() > 0) {
                                mCouponCheckList.add(mCouponList.get(i));
                            }
                        }
                        if (totalCouponMoney > 0) {
                            batchUseByVCodes();
                        } else {
                            toOrderInfoActivity("", false);
                        }
                        break;
                }

            }
        };
        mIvBack.setOnClickListener(onSingleClickListener);
        mTvVerify.setOnClickListener(onSingleClickListener);
        mTvAddCoupon.setOnClickListener(onSingleClickListener);
        mCbVipRight.setOnClickListener(onSingleClickListener);
    }

    private void initData() {
        mCouponList = new ArrayList<>();
        mCouponCheckList = new ArrayList<>();
        List<CouponInfoData> couponList = (List<CouponInfoData>) getIntent().getSerializableExtra(Constants.INTENT_VANKE_COUPON);
        mPayRemark = getIntent().getStringExtra(Constants.INTENT_PAY_REMARK);//备注
        doubleMoney = getIntent().getDoubleExtra(Constants.INTENT_PAY_MONEY, 0);//金额
        mStoreId = getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
        mInterProviderType = getIntent().getStringExtra(Constants.INTENT_COUPON_PROVIDER_TYPE);
        mCouponType = getIntent().getIntExtra(Constants.INTENT_COUPON_TYPE,-1);
        money = String.valueOf(BigDecimal.valueOf(doubleMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
        if (couponList != null && couponList.size() > 0)
            mCouponList.addAll(couponList);
        if (mInterProviderType == null || "-1".equals(mInterProviderType)) {
            mTvAddCoupon.setVisibility(View.INVISIBLE);
        } else {
            mTvAddCoupon.setVisibility(View.VISIBLE);
            if ("2".equals(mInterProviderType)) {//印享星不支持添加单张券
                mTvAddCoupon.setAlpha(0.4f);
                mTvAddCoupon.setEnabled(false);
            } else {
                mTvAddCoupon.setAlpha(1);
                mTvAddCoupon.setEnabled(true);
            }
        }
        if ("1".equals(mInterProviderType))
            mCouponCheckList.addAll(couponList);
        LinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CouponInfoAdapter(MyApplication.getContext(), mCouponList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new CouponInfoAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mPosition = position;
                CouponInfoData data = mCouponList.get(position);
                mCouponCheckList.clear();
                for (int i = 0; i < mCouponList.size(); i++) {
                    CouponInfoData item = mCouponList.get(i);
                    if (item.isChecked() && !data.getCouponID().equals(item.getCouponID())) {
                        item.setCheckFlag(1);
                        mCouponCheckList.add(item);
                    }
                }
                if (!data.isChecked()) {
                    data.setCheckFlag(2);
                    mCouponCheckList.add(data);
                }
                getPayInfo();
            }

            @Override
            public void onItemAddClick(int position) {
                mPosition = position;
                clickType = 1;//点击加号
                mCouponList.get(position).setNeedUseCount(mCouponList.get(position).getNeedUseCount() + 1);
                mAdapter.notifyDataSetChanged();
                getPayInfo();
            }

            @Override
            public void onItemReduceClick(int position) {
                mPosition = position;
                clickType = 2;//点击减号
                mCouponList.get(position).setNeedUseCount(mCouponList.get(position).getNeedUseCount() - 1);
                mAdapter.notifyDataSetChanged();
                getPayInfo();
            }
        });
        mTvOrderMoney.setText(getString(R.string.tx_mark) + DateUtil.formatPaseMoneyUtil(doubleMoney));
        mVipInfo = (VipInfoData) getIntent().getSerializableExtra(Constants.INTENT_VANKE_VIP);
        /*if (mVipInfo != null || (mCouponCheckList != null && mCouponCheckList.size() > 0 && couponCount(mCouponCheckList))) {
            getUserInfo();
            getPayInfo();
            mLlTop.setVisibility(View.VISIBLE);
//            isClickCheckbox = false;
            if (mVipInfo.getCardDiscount() == 1000) {
                mLlVipRight.setVisibility(View.INVISIBLE);
                mCbVipRight.setChecked(false);
//                isClickCheckbox = false;
            }
        } else {
            setCouponNull();
            mLlTop.setVisibility(View.GONE);
//            isClickCheckbox = false;
            mTvVerify.setText(R.string.tv_pay);
        }*/
        if (mVipInfo == null) {
            mLlTop.setVisibility(View.GONE);
        } else {
            if (mCouponType == 3 || mVipInfo.getCardDiscount() >= 1000)
                mRlVipRight.setVisibility(View.INVISIBLE);
            getUserInfo();
        }
        getPayInfo();
    }

    private void getPayInfo() {
        /*if (mVipInfo == null) {
            if (mCouponCheckList.size() == 0 || !couponCount(mCouponCheckList)) {
                setCouponNull();
                if (mPosition != -1) {
                    mCouponList.get(mPosition).setChecked(false);
                    mAdapter.notifyDataSetChanged();
                }
                mTvVerify.setText(R.string.tv_pay);
            } else {
                checkByVCode();
            }
        } else {
            if (!mCbVipRight.isChecked() && (mCouponCheckList.size() == 0 || !couponCount(mCouponCheckList))) {
                setCouponNull();
                if (mPosition != -1) {
                    mCouponList.get(mPosition).setChecked(false);
                    mAdapter.notifyDataSetChanged();
                }
                isClickCheckbox = false;
                mTvVerify.setText(R.string.tv_pay);
            } else {
                checkByVCode();
            }
        }*/
        if ((mVipInfo == null || !mCbVipRight.isChecked()) && (mCouponCheckList.size() == 0 || !couponCount(mCouponCheckList))) {
            if ("1".equals(mInterProviderType))
                isAddCoupon = false;
            setCouponNull();
            if (mPosition != -1) {
                mCouponList.get(mPosition).setChecked(false);
            }
            mAdapter.notifyDataSetChanged();
            isClickCheckbox = false;
            clickType = 0;
            mTvVerify.setText(R.string.tv_pay);
        } else {
            checkByVCode();
        }
    }

    /*@Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_add_coupon:
                Intent intent = new Intent(this, CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_ADD_VANKE_COUPON);
                intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
                intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
                startActivityForResult(intent, Constants.REQUEST_VANKE_COUPON);
                break;
            case R.id.cb_vip_right:
                mCouponCheckList.clear();
                for (int i = 0; i < mCouponList.size(); i++) {
                    if (mCouponList.get(i).isChecked() || ("1".equals(mInterProviderType) && mCouponList.get(i).getNeedUseCount() >= 0)) {
                        mCouponCheckList.add(mCouponList.get(i));
                    }
                }
                isClickCheckbox = true;
                getPayInfo();
                break;
        }
    }*/

    private void setCouponNull() {
        mTvCouponMoney.setText(getString(R.string.tx_mark_coupon) + getString(R.string.tv_money_zero));
        mTvCutMoney.setText(getString(R.string.tx_mark_coupon) + getString(R.string.tv_money_zero));
        mTvVipRightMoney.setText(getString(R.string.tx_mark_coupon) + getString(R.string.tv_money_zero));
        mTvActualMoney.setText(getString(R.string.tx_mark) + DateUtil.formatPaseMoneyUtil(doubleMoney));
        actualMoney = doubleMoney;
        couponMoney = 0;
        cutMoney = 0;
        vipRightMoney = 0;
        totalCouponMoney = 0;
    }

    private void getUserInfo() {
        if (mVipInfo != null) {
            if (!TextUtils.isEmpty(mVipInfo.getUserName())) {
                mTvVipName.setText("会员：" + mVipInfo.getUserName());
            } else if (!TextUtils.isEmpty(mVipInfo.getNickName())) {
                mTvVipName.setText("会员：" + mVipInfo.getNickName());
            } else {
                mTvVipName.setText("会员：");
            }
            if (!TextUtils.isEmpty(mVipInfo.getMobile())) {
                mTvVipPhone.setText("手机：" + mVipInfo.getMobile());
            } else {
                mTvVipPhone.setText("手机：");
            }
            if (!TextUtils.isEmpty(mVipInfo.getBirthday())) {
                mTvVipBirthday.setText("生日：" + mVipInfo.getBirthday());
            } else {
                mTvVipBirthday.setText("生日：");
            }
            mTvVipPoint.setText("积分：" + mVipInfo.getScore());
            BigDecimal cardDiscount = BigDecimal.valueOf(mVipInfo.getCardDiscount()).divide(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP);
            if (cardDiscount.doubleValue() == 10) {
                mTvVipLevel.setText(mVipInfo.getMallCardName());
            } else {
                mTvVipLevel.setText(mVipInfo.getMallCardName() + "(" + cardDiscount.toString() + "折)");
            }
        }
    }

    private void checkByVCode() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            setCheckedFail();
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("couponInfoList", mCouponCheckList);
            map.put("orderAmount", money);
            map.put("storeMerchantId", mStoreId);
            if (mVipInfo != null) {
                map.put("mallCardNo", mVipInfo.getMallCardNo());
                map.put("bizUid", mVipInfo.getBizUid());
                map.put("memberDiscount", mVipInfo.getCardDiscount());
                map.put("memberLevel", mVipInfo.getMallCardLevel());
                if (mCbVipRight.isChecked()) {
                    map.put("isdiscount", true);
                } else {
                    map.put("isdiscount", false);
                }
            }
            ServerClient.newInstance(MyApplication.getContext()).checkByVCode(MyApplication.getContext(), Constants.TAG_COUPON_CHECK, map);
        }
    }

    private void batchUseByVCodes() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("couponInfoList", mCouponCheckList);
            map.put("orderAmount", money);
            map.put("storeMerchantId", mStoreId);
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            if (mVipInfo != null) {
                map.put("mallCardNo", mVipInfo.getMallCardNo());
                map.put("bizUid", mVipInfo.getBizUid());
                map.put("memberDiscount", mVipInfo.getCardDiscount());
                map.put("memberLevel", mVipInfo.getMallCardLevel());
                map.put("mallCardName", mVipInfo.getMallCardName());
                map.put("mobile", mVipInfo.getMobile());
                if (mCbVipRight.isChecked()) {
                    map.put("isdiscount", true);
                } else {
                    map.put("isdiscount", false);
                }
            }
            ServerClient.newInstance(MyApplication.getContext()).batchUseByVCodes(MyApplication.getContext(), Constants.TAG_COUPON_VERIFY, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_COUPON_CHECK)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponCheckBean msg = (CouponCheckBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    setCheckedFail();
//                    isClickCheckbox = false;
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(OrderCouponInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(OrderCouponInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    setCheckedFail();
//                    isClickCheckbox = false;
                    break;
                case Constants.ON_EVENT_TRUE:
                    mCouponCheckData = msg.getData();
                    if (mCouponCheckData != null) {
                        List<CouponInfoData> filterCouponInfoList = mCouponCheckData.getFilterCouponInfoList();
                        if (filterCouponInfoList == null) {
                            filterCouponInfoList = Collections.EMPTY_LIST;
                        }
                        for (CouponInfoData couponInfoData : mCouponList) {//判断元素是否需要勾选
                            couponInfoData.setChecked(containsItem(couponInfoData, filterCouponInfoList));
                        }
                        mAdapter.notifyDataSetChanged();
                        mTvCouponMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyByLong(mCouponCheckData.getCouponMoney()));
                        mTvCutMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyByLong(mCouponCheckData.getReduceMoney()));
                        mTvVipRightMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyByLong(mCouponCheckData.getDiscountMoney()));
                        mTvActualMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(mCouponCheckData.getPaymentMoney()));
                        couponMoney = new BigDecimal(mCouponCheckData.getCouponMoney()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                        vipRightMoney = new BigDecimal(mCouponCheckData.getDiscountMoney()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                        cutMoney = new BigDecimal(mCouponCheckData.getReduceMoney()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                        actualMoney = new BigDecimal(mCouponCheckData.getPaymentMoney()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                        totalCouponMoney = mCouponCheckData.getCouponMoney() + mCouponCheckData.getReduceMoney() + mCouponCheckData.getDiscountMoney();
                        isClickCheckbox = false;
                        clickType = 0;
                        if ("1".equals(mInterProviderType))
                            isAddCoupon = false;
                    } else {
                        setCheckedFail();
                    }
//                    isClickCheckbox = false;
                    break;
            }
            /*if (!mCbVipRight.isChecked() && mCouponCheckList.size() == 0) {
                mTvVerify.setText(R.string.tv_pay);
            } else {
                mTvVerify.setText(R.string.tv_verify);
            }*/
            isClickCheckbox = false;
            mTvVerify.setText(totalCouponMoney > 0 ? R.string.tv_verify : R.string.tv_pay);
        }
        if (event.getTag().equals(Constants.TAG_COUPON_VERIFY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponVerifyBean msg = (CouponVerifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    mTvVerify.setText(R.string.tv_verify_continue);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError().getCode() != null) {
                        if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                            if (msg.getError().getMessage() != null) {
                                getLoginDialog(OrderCouponInfoActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                showCommonNoticeDialog(OrderCouponInfoActivity.this, msg.getError().getMessage());
                            }
                        }
                    }
                    mTvVerify.setText(R.string.tv_verify_continue);
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (actualMoney > 0) {
                        if(msg.getData() != null) {
                            if (TextUtils.isEmpty(msg.getData().getCouponInfoList())) {
                                toOrderInfoActivity(msg.getData().getOutTradeNo(), false);
                            } else {
                                toOrderInfoActivity(msg.getData().getOutTradeNo(), true);
                            }
                        } else {
                            toOrderInfoActivity("", false);
                        }
                    } else {
//                        toCouponVerify(Constants.INTENT_COUPON_VERIFY_SUCCESS);
                        PayBean.DataBean data = new PayBean.DataBean();
                        data.setRealMoney(BigDecimal.valueOf(actualMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue());
                        data.setMoney(new BigDecimal(money).longValue());
                        data.setMchDiscountsMoney(new BigDecimal(totalCouponMoney).longValue());
                        List<Coupon> couponList = new ArrayList<>();
                        if (couponMoney > 0) {
                            Coupon coupon = new Coupon();
                            coupon.setCouponName("优惠金额");
                            coupon.setParValue(BigDecimal.valueOf(couponMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue());
                            couponList.add(coupon);
                        }
                        if (cutMoney > 0) {
                            Coupon coupon = new Coupon();
                            coupon.setCouponName("立减金额");
                            coupon.setParValue(BigDecimal.valueOf(cutMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue());
                            couponList.add(coupon);
                        }
                        if (vipRightMoney > 0) {
                            Coupon coupon = new Coupon();
                            coupon.setCouponName("会员权益");
                            coupon.setParValue(BigDecimal.valueOf(vipRightMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).longValue());
                            couponList.add(coupon);
                        }
                        data.setCouponInfoList(couponList);
                        data.setCouponInfoData(mCouponCheckList);
                        if (!TextUtils.isEmpty(msg.getData().getCouponInfoList())) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<CouponInfoData>>() {
                            }.getType();
                            List<CouponInfoData> list = gson.fromJson(msg.getData().getCouponInfoList(), type);
                            if (list != null && list.size() > 0) {
                                data.setCouponGroupInfoData(list);
                            }
                        }
                        data.setTradeTime(DateUtil.formatTime(System.currentTimeMillis()));
                        if (mInterProviderType != null) {
                            if ("1".equals(mInterProviderType)) {
                                data.setApiProvider(1112);
                            } else if ("2".equals(mInterProviderType)) {
                                data.setApiProvider(1113);
                            } else {
                                data.setApiProvider(1111);
                            }
                        }
                        data.setTradeState(2);
                        data.setTransactionId(msg.getData().getOutTradeNo());
                        data.setCashierName(MyApplication.getRealName());
                        Intent intent = new Intent();
                        intent.putExtra("capture", data);
                        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_CAPTURE_PAYRESULT);
                        intent.setClass(OrderCouponInfoActivity.this, PayResultActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    }

    /**
     * 判断是否包含指定元素
     *
     * @param couponInfoData
     * @param filterCouponInfoList
     */
    private boolean containsItem(CouponInfoData couponInfoData, List<CouponInfoData> filterCouponInfoList) {
        for (CouponInfoData infoData : filterCouponInfoList) {
            if (couponInfoData.getCouponID() !=null && couponInfoData.getCouponID().equals(infoData.getCouponID())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断聚合券张数是否为零
     */
    private boolean couponCount(List<CouponInfoData> couponInfoList) {
        for (CouponInfoData infoData : couponInfoList) {
            if (infoData.getInterProviderType() != 1)
                return true;
            if (infoData.getNeedUseCount() > 0) {
                return true;
            }
        }
        return false;
    }

    //银商订单详情页面
    private void toOrderInfoActivity(String outTradeNo,boolean usedCoupon) {
        Intent intent = new Intent(this, OrderPayInfoActivity.class);
        intent.putExtra(Constants.INTENT_PAY_MONEY, doubleMoney);
        if (mCbVipRight.isChecked() || mCouponCheckList.size() > 0) {
            intent.putExtra(Constants.INTENT_USED_COUPON, usedCoupon);
            intent.putExtra(Constants.INTENT_COUPON_MONEY, couponMoney);
            intent.putExtra(Constants.INTENT_CUT_MONEY, cutMoney);
            intent.putExtra(Constants.INTENT_OUT_TRADE_NO, outTradeNo);
            intent.putExtra(Constants.INTENT_VIP_RIGHT_MONEY, vipRightMoney);
            intent.putExtra(Constants.INTENT_TOTAL_COUPON_MONEY, totalCouponMoney);
        }
        intent.putExtra(Constants.INTENT_ACTUAL_MONEY, actualMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
        if (mVipInfo != null) {//猫酷会员
            intent.putExtra(OrderPayInfoActivity.KEY_VANKE_VIP, mVipInfo);
        }
        startActivity(intent);
    }

    private void setCheckedFail() {
        if (isClickCheckbox) {
            if (mCbVipRight.isChecked()) {
                mCbVipRight.setChecked(false);
            } else {
                mCbVipRight.setChecked(true);
            }
        }
        switch (clickType) {
            case 1://点击加号
                if (mPosition != -1) {
                    mCouponList.get(mPosition).setNeedUseCount(mCouponList.get(mPosition).getNeedUseCount() - 1);
                }
                break;
            case 2://点击减号
                if (mPosition != -1) {
                    mCouponList.get(mPosition).setNeedUseCount(mCouponList.get(mPosition).getNeedUseCount() + 1);
                }
                break;
        }
        if (!mCbVipRight.isChecked() && (mCouponCheckList.size() == 0 || !couponCount(mCouponCheckList))) {
            mTvVerify.setText(R.string.tv_pay);
        } else {
            mTvVerify.setText(R.string.tv_verify);
        }
        isClickCheckbox = false;
        clickType = 0;
        if ("1".equals(mInterProviderType) && isAddCoupon) {
            mCouponCheckList.remove(0);
            mCouponList.remove(0);
            isAddCoupon = false;
        }
        mAdapter.notifyDataSetChanged();
    }

    private void toCouponVerify(String intentName) {
        Intent intent = new Intent(OrderCouponInfoActivity.this, CouponVerifyActivity.class);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_VANKE_COUPON) {
            if (data == null)
                return;
            CouponInfoData couponInfoData = (CouponInfoData) data.getSerializableExtra(Constants.RESULT_VANKE_COUPON);
            if (couponInfoData != null) {
                boolean isExist = false;
                for (int i = 0; i < mCouponList.size(); i++) {
                    if (mCouponList.get(i).getCouponCode().equals(couponInfoData.getCouponCode())) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    mCouponList.add(0, couponInfoData);
                    if (couponInfoData.getInterProviderType() == 1) {
                        isAddCoupon = true;
                        mCouponCheckList.add(0, couponInfoData);
                        getPayInfo();
                    }
                } else {
                    showCommonNoticeDialog(OrderCouponInfoActivity.this, "该优惠券已存在");
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
