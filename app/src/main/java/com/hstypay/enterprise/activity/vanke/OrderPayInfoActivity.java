package com.hstypay.enterprise.activity.vanke;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.facepay.FacePayActivity;
import com.hstypay.enterprise.adapter.PayAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ApiResult;
import com.hstypay.enterprise.bean.EasypayInfo;
import com.hstypay.enterprise.bean.FunctionBean;
import com.hstypay.enterprise.bean.FuxunBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PosAccountBean;
import com.hstypay.enterprise.bean.PosOrderBean;
import com.hstypay.enterprise.bean.QueryPayTypeBean;
import com.hstypay.enterprise.bean.YinshangPosBean;
import com.hstypay.enterprise.bean.vanke.BizDetailBean;
import com.hstypay.enterprise.bean.vanke.CouponReverseBean;
import com.hstypay.enterprise.bean.vanke.VipInfoData;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.UmsPayUtils;
import com.ums.anypay.service.IOnTransEndListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class OrderPayInfoActivity extends BaseActivity implements View.OnClickListener, PayAdapter.OnRecyclerViewItemClickListener {

    //支付成功通知结果
    private final static String NOTIFY_PAY_SUCCESS = "order_pay_info_notify_success";
    public final static String KEY_VANKE_VIP = "key_vanke_vip";
    private ImageView mIvBack;
    private RecyclerView mRecyclerView;
    private TextView mTvTitle, mBtnTitle, mTvOrderMoney, mTvCouponMoney, mTvCutMoney, mTvVipRightMoney, mTvActualMoney;
    private LinearLayout mLlCouponMoney, mLlCutMoney, mLlVipRightMoney;
    private SafeDialog mLoadDialog, mSyncLoadDialog;
    private String mPayRemark;
    private double totalMoney;
    private double couponMoney;
    private double cutMoney;
    private double vipRightMoney;
    private double actualMoney;
    private boolean mUsedCoupon;
    private long totalCouponMoney;
    private String money;
    private String mStoreId;
    private String mOutTradeNo;
    private boolean notNeedReverse;
    private PayAdapter mAdapter;
    private List<FunctionBean> mFunctionList;
    //创建订单
    private String mThirdApiCode;
    //银商pos福卡刷卡支付
    private PosOrderBean.DataEntity mBilldata;
    private boolean retry = true;
    private VipInfoData mVipInfo;//猫酷会员信息
    private Handler mHandler;
    private boolean cancelTask;
    private SelectDialog mSyncDialog;
    private YinshangPosBean mYinshangPosBean;
    private SelectDialog mGetCardPayResultDialog;

    private Intent mResultData;
    private PosAccountBean.DataEntity mPosAccountBean;
    private FuxunBean mFuxunBean;
    private boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_pay_info);
        MyApplication.getInstance().addActivity(this);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        /*if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))
            bindCUPService();*/
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mSyncLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_order_sync), false);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mBtnTitle = findViewById(R.id.button_title);
        mTvTitle = findViewById(R.id.tv_title);

        mBtnTitle.setVisibility(View.INVISIBLE);

        mTvOrderMoney = findViewById(R.id.tv_order_money);
        mTvCouponMoney = findViewById(R.id.tv_coupon_money);
        mTvCutMoney = findViewById(R.id.tv_cut_money);
        mTvVipRightMoney = findViewById(R.id.tv_vip_right_money);
        mTvActualMoney = findViewById(R.id.tv_actual_money);

        mLlCouponMoney = findViewById(R.id.ll_coupon_money);
        mLlCutMoney = findViewById(R.id.ll_cut_money);
        mLlVipRightMoney = findViewById(R.id.ll_vip_right_money);
        mRecyclerView = findViewById(R.id.recyclerView);
        mTvTitle.setText(R.string.title_pay_info);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
    }

    private void initData() {
        mHandler = new Handler();
        mUsedCoupon = getIntent().getBooleanExtra(Constants.INTENT_USED_COUPON, false);//是否使用了优惠券
        mPayRemark = getIntent().getStringExtra(Constants.INTENT_PAY_REMARK);//备注
        totalMoney = getIntent().getDoubleExtra(Constants.INTENT_PAY_MONEY, 0);//金额
        couponMoney = getIntent().getDoubleExtra(Constants.INTENT_COUPON_MONEY, 0);//优惠金额
        cutMoney = getIntent().getDoubleExtra(Constants.INTENT_CUT_MONEY, 0);//立减金额
        vipRightMoney = getIntent().getDoubleExtra(Constants.INTENT_VIP_RIGHT_MONEY, 0);//权益金额
        actualMoney = getIntent().getDoubleExtra(Constants.INTENT_ACTUAL_MONEY, 0);//实付金额
        totalCouponMoney = getIntent().getLongExtra(Constants.INTENT_TOTAL_COUPON_MONEY, 0);//优惠总金额
        money = String.valueOf(BigDecimal.valueOf(totalMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
        mStoreId = getIntent().getStringExtra(Constants.INTENT_QRCODE_STOREMERCHANTID);
        mOutTradeNo = getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO);
        mVipInfo = (VipInfoData) getIntent().getSerializableExtra(KEY_VANKE_VIP);
        mTvOrderMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(totalMoney));
        if (couponMoney > 0) {
            mLlCouponMoney.setVisibility(View.VISIBLE);
            mTvCouponMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyUtil(couponMoney));
        } else {
            mLlCouponMoney.setVisibility(View.GONE);
        }
        if (cutMoney > 0) {
            mLlCutMoney.setVisibility(View.VISIBLE);
            mTvCutMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyUtil(cutMoney));
        } else {
            mLlCutMoney.setVisibility(View.GONE);
        }
        if (vipRightMoney > 0) {
            mLlVipRightMoney.setVisibility(View.VISIBLE);
            mTvVipRightMoney.setText(getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyUtil(vipRightMoney));
        } else {
            mLlVipRightMoney.setVisibility(View.GONE);
        }
        mTvActualMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(actualMoney));
        if (!TextUtils.isEmpty(mOutTradeNo)) {
            MyToast.showToastShort("核销成功，请继续完成支付");
        }
        mFunctionList = new ArrayList<>();
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new PayAdapter(OrderPayInfoActivity.this, mFunctionList);
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        if (MyApplication.isBcardOpen()) {
            setFunc(true);
        } else {
            setFunc(false);
            if (!DateUtil.getDate().equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_YINSHANG_SIGN_DATE)) && checkApiProvider(10)) {
                getPayCardAccount();
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void setFunc(boolean isYinshangOpen) {
        mFunctionList.clear();
        FunctionBean scanPay = new FunctionBean(R.mipmap.icon_vanke_scan_pay, 0, getString(R.string.tv_scan_pay));
        FunctionBean unionCardPay = new FunctionBean(R.mipmap.icon_vanke_union_card_pay, 1, getString(R.string.tv_union_card_pay));
        FunctionBean fukaPay = new FunctionBean(R.mipmap.icon_vanke_fuka_card_pay, 2, getString(R.string.tv_fuka_card_pay));
        FunctionBean facePay = new FunctionBean(R.mipmap.icon_vanke_face_pay, 3, getString(R.string.tv_face_pay));
        mFunctionList.add(scanPay);
        if (isYinshangOpen && checkApiProvider(10)) {
            mFunctionList.add(unionCardPay);
        }
        /*if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mFunctionList.add(fukaPay);
            mFunctionList.add(facePay);
        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mFunctionList.add(fukaPay);
        }*/
        if (checkApiProvider(11))
            mFunctionList.add(fukaPay);
        if (checkApiProvider(13) || checkApiProvider(15))
            mFunctionList.add(facePay);
    }

    /**
     * 获取POS账号密码
     */
    private void getPayCardAccount() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.HPAY_CARD_YINSHANG);
            } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.HUIFUTIANXIA);
            } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.ESAYCARD);
            }
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("sn", AppHelper.getYinshangSN());
            } else {
                map.put("sn", AppHelper.getDeviceId());
            }
            ServerClient.newInstance(MyApplication.getContext()).getPayCardThirdChannelId(MyApplication.getContext(), Constants.TAG_PAY_POS_ACCOUNT, map);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finishActivity();
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finishActivity();
    }

    private void finishActivity() {
        if (TextUtils.isEmpty(mOutTradeNo)) {
            finish();
        } else {
            if (isChecked) {
                showCheckCancelCoupon();
            } else {
                showCancelCoupon();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (!TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), "BCARD_ORDER_NO"))) {//如果不为空，说明没有走onActivityResult方法,弹框提示刷卡单需要同步
            getPayResult();
        }*/
    }

    private void getPayResult() {
        if (mGetCardPayResultDialog == null) {
            mGetCardPayResultDialog = new SelectDialog(this, "您有一笔刷卡订单未获取到收款结果，是否立即获取结果？", "忽略", "获取", R.layout.select_common_dialog);
            mGetCardPayResultDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    queryOrder("TAG_AUTO_ORDER_PAY_DETAIL");
                }
            });
            mGetCardPayResultDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
                }
            });
            DialogHelper.resize(this, mGetCardPayResultDialog, 0.9f);
        }
        mGetCardPayResultDialog.show();
    }

    private void showCancelCoupon() {
        SelectDialog selectDialog = new SelectDialog(OrderPayInfoActivity.this, getString(R.string.dialog_coupon_cancel_context1)
                , getString(R.string.dialog_coupon_cancel_context2), getString(R.string.btn_coupon_cancel)
                , getString(R.string.btn_coupon_continue), R.layout.select_common_dialog);
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                cancelUse(getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO));
            }
        });
        selectDialog.show();
    }

    private void showCheckCancelCoupon() {
        SelectDialog selectDialog = new SelectDialog(OrderPayInfoActivity.this, "是否撤销优惠"
                , "请先核对订单是否支付成功；\n" +
                "若支付成功，请点击\"返回首页\"；\n" +
                "否则请点击\"撤销\"，会撤销该订单使用的优惠券", "撤销"
                , "返回首页", R.layout.select_common_dialog);
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                cancelUse(getIntent().getStringExtra(Constants.INTENT_OUT_TRADE_NO));
            }
        });
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                toMainActivity();
            }
        });
        selectDialog.show();
    }

    //扫描二维码
    private void toCaptureActivity() {
        if (TextUtils.isEmpty(mStoreId)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(OrderPayInfoActivity.this, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_ACTUAL_MONEY, actualMoney);
        intent.putExtra(Constants.INTENT_OUT_TRADE_NO, mOutTradeNo);
        intent.putExtra(Constants.INTENT_TOTAL_COUPON_MONEY, totalCouponMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        intent.putExtra(KEY_VANKE_VIP, mVipInfo);
        startActivityForResult(intent, Constants.REQUEST_POS_RECEIVE_SCAN);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(NOTIFY_PAY_SUCCESS)) {//支付成功通知
            DialogUtil.safeCloseDialog(mSyncLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(OrderPayInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(OrderPayInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    remove();
                    toMainActivity();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_POS_FACE_PAY)) {//创建下单
            DialogUtil.safeCloseDialog(mLoadDialog);
            PosOrderBean msg = (PosOrderBean) event.getMsg();
            ApiResult.ErrorBean errorBean = null;
            if (msg.getError() != null) {
                errorBean = new ApiResult.ErrorBean(msg.getError().getCode(), msg.getError().getMessage(), msg.getError().getArgs());
            }
            ApiResult<PosOrderBean.DataEntity> res = new ApiResult(errorBean, msg.getLogId(), msg.isStatus(), msg.getData());
            if (event.getCls().equals(Constants.ON_EVENT_TRUE) && msg.getData() != null) {
                mBilldata = msg.getData();
//                mBilldata.setCreateTime(DateUtil.formatTime(System.currentTimeMillis()));
                if (mThirdApiCode.equals(Constants.ESAYCARD)) {//易生刷卡
                    SpStayUtil.putString(MyApplication.getContext(), "BCARD_ORDER_NO", mBilldata.getOrderNo());
                    startActivityWizarposCard(new BigDecimal(msg.getData().getMoney()).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).toString(), msg.getData().getOrderNo());
                } else if (mThirdApiCode.equals(Constants.HPAY_CARD_YINSHANG)) {//刷卡
                    SpStayUtil.putString(MyApplication.getContext(), "BCARD_ORDER_NO", mBilldata.getOrderNo());
                    startYinshangCard(msg.getData().getMoney(), msg.getData().getOrderNo());
                } else if (mThirdApiCode.equals(Constants.UNIONPAY_FUKA_BCARD)) {//银商福卡
                    UmsPayUtils.Builder builder = new UmsPayUtils.Builder(UmsPayUtils.Type.Consumption,
                            msg.getData().getOrderNo());
                    builder.setAmt(msg.getData().getMoney());
                    UmsPayUtils.callTrans(this, builder, new UmsPayUtils.UmsCallBack() {
                        @Override
                        public void response(YinshangPosBean posBean) {
                            if (posBean != null) {
                                if ("0".equals(posBean.getResultCode())) {
                                    mYinshangPosBean = posBean;
                                    if (mYinshangPosBean.getTransData() != null && "00".equals(mYinshangPosBean.getTransData().getResCode())) {
                                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                            MyToast.showToastShort(getString(R.string.network_exception));
                                            syncOrderState();
                                        } else {
                                            payYinshangNotify(mYinshangPosBean);
                                        }
                                    } else {
                                        syncDialog(mYinshangPosBean.getTransData().getResDesc());
                                    }
                                    /*if (posBean.getTransData() != null && "00".equals(posBean.getTransData().getResCode())) {
                                        startActivity(new Intent(OrderPayInfoActivity.this, MainActivity.class));
                                    }*/
                                } else {
                                    showCommonNoticeDialog(OrderPayInfoActivity.this, null, posBean.getResultMsg());
                                }
                            } else {
                                showCommonNoticeDialog(OrderPayInfoActivity.this, "支付回调数据有误，请确认支付结果！");
                            }
                        }
                    });
                } else if (mThirdApiCode.equals(Constants.UNIONPAY_WIZARD_FUKA_BCARD)) {//慧银福卡
                    if (MainActivity.instance.getCupService() != null) {
                        if (TextUtils.isEmpty(StringUtils.formatFukaMoney(msg.getData().getMoney()))) {
                            showCommonNoticeDialog(OrderPayInfoActivity.this, "输入金额有误！");
                        } else {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    SpStayUtil.putString(MyApplication.getContext(), "BCARD_ORDER_NO", mBilldata.getOrderNo());
                                    wizarFukaPay(msg);
                                }
                            }).start();
                        }
                    }
                } else {//刷脸
                    startActivity(new Intent(this, FacePayActivity.class)
                            .putExtra(FacePayActivity.KEY_ORDER_BEAN, msg.getData()));
                }
            } else {
                errorHandle(event, res);
            }
        } else if (event.getTag().equals(Constants.TAG_COUPON_CANCEL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponReverseBean msg = (CouponReverseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    CouponReverseBean.CouponReverseData couponReverseData = msg.getData();
                    if (couponReverseData != null) {
                        if (couponReverseData.getReverseResult() == 0) {
                            toCouponVerify(Constants.INTENT_COUPON_CANCEL_SUCCESS, "");
                        } else {
                            if (couponReverseData.isNeedReverse()) {
                                notNeedReverse = false;
                                toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, "");
                            } else {
                                notNeedReverse = true;
                                toCouponVerify(Constants.INTENT_COUPON_CANCEL_FAILED, couponReverseData.getReverseMessage());
                            }
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_PAY_POS_ACCOUNT)) {
            PosAccountBean msg = (PosAccountBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mPosAccountBean = msg.getData();
                    if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        startYinshangSign(mPosAccountBean.getThirdMerchantId());
                    } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        startActivityWizarposMerInfo();
                    }
                    if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        startYinshangSign(mPosAccountBean.getThirdMerchantId());
                    }
                    break;
            }
        }
        if (event.getTag().equals("TAG_ORDER_PAY_DETAIL") || event.getTag().equals("TAG_AUTO_ORDER_PAY_DETAIL")) {//查询订单
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.INTENT_BILL_DATA, data);
                        if (!TextUtils.isEmpty(mOutTradeNo)) {
                            intent.putExtra(Constants.INTENT_OUT_TRADE_NO, mOutTradeNo);
                            intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
                            intent.putExtra(Constants.INTENT_USED_COUPON, mUsedCoupon);
                        }
                        intent.putExtra(Constants.INTENT_NAME, "INTENT_NAME_FACE_PAYDETAIL");
                        if (event.getTag().equals("TAG_AUTO_ORDER_PAY_DETAIL")) {
                            //传个标识，直接同步
                            intent.putExtra(Constants.INTENT_NAME, "TAG_AUTO_ORDER_DETAIL");
                        }
                        intent.setClass(OrderPayInfoActivity.this, PayDetailActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    break;
            }
        }
    }

    /**
     * 慧银-福卡支付
     *
     * @param msg
     */
    private void wizarFukaPay(PosOrderBean msg) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("AppID", Constants.WJYS_APPID);
            map.put("AppName", "WJYS");
            map.put("ItemID", "WJY02");
            map.put("CustomerOID", msg.getData().getOrderNo());
            map.put("trxId", msg.getData().getOrderNo());
            map.put("TransType", 1);
            map.put("TransAmount", StringUtils.formatFukaMoney(msg.getData().getMoney()));
            if (!TextUtils.isEmpty(msg.getData().getOrderNo())) {
                if (msg.getData().getOrderNo().length() > 16) {
                    map.put("TransIndexCode", msg.getData().getOrderNo().substring(msg.getData().getOrderNo().length() - 16));
                } else {
                    map.put("TransIndexCode", msg.getData().getOrderNo());
                }
            }
            map.put("ReqTransDate", DateUtil.formatYYMD(System.currentTimeMillis()));
            map.put("ReqTransTime", DateUtil.formatDateToHHmmss(DateUtil.formatTime(System.currentTimeMillis())));
            LogUtil.d("Jeremy-pay-json==" + new Gson().toJson(map));
            String cash = MainActivity.instance.getCupService().payCash(new Gson().toJson(map));
            LogUtil.d("Jeremy-pay==" + cash);
            mFuxunBean = new Gson().fromJson(cash, FuxunBean.class);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mFuxunBean != null) {
                        SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
                        if ("00".equals(mFuxunBean.getRespCode())) {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                payFukaNotify(mFuxunBean);
                            }
                        } else {
                            syncDialog(mFuxunBean.getRespDesc());
                        }
                    } else {
                        showCommonNoticeDialog(OrderPayInfoActivity.this, "支付回调数据有误，请确认支付结果！");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 银商签到
     *
     * @param thirdMerchantId
     */
    private void startYinshangSign(final String thirdMerchantId) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "公共资源";
        String transAppId = "签到";
        JSONObject transData = new JSONObject();
        try {
            transData.put("appId", APPID);//appId
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
                SpUtil.putString(MyApplication.getContext(), Constants.SP_YINSHANG_SIGN_DATE, DateUtil.getDate());
                YinshangPosBean yinshangPosBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                if (yinshangPosBean.getTransData() != null) {
                    if (yinshangPosBean.getTransData().getTerminalNo() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, yinshangPosBean.getTransData().getTerminalNo());
                    }
                    if (thirdMerchantId != null && thirdMerchantId.equals(yinshangPosBean.getTransData().getMerchantNo())) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, true);
                        setFunc(true);
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, false);
                        setFunc(false);
                        MyToast.showToastShort("第三方商户号有误！(" + thirdMerchantId + ")与(" + yinshangPosBean.getTransData().getMerchantNo() + ")不一致");
                    }
                } else {
                    MyToast.showToastShort("签到失败！");
                }
            }
        };
        com.ums.AppHelper.callTrans(OrderPayInfoActivity.this, transAppName, transAppId, transData, listener);
    }

    private void toCouponVerify(String intentName, String errorString) {
        Intent intent = new Intent(OrderPayInfoActivity.this, CouponVerifyActivity.class);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        intent.putExtra(Constants.INTENT_OUT_TRADE_NO, mOutTradeNo);
        intent.putExtra(Constants.INTENT_USED_COUPON, mUsedCoupon);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, mStoreId);
        intent.putExtra(Constants.INTENT_NOT_NEED_REVERSE_COUPON, notNeedReverse);
        if (!TextUtils.isEmpty(errorString)) {
            intent.putExtra(Constants.INTENT_COUPON_CANCEL_FAILED, errorString);
        }
        startActivity(intent);
    }

    private void cancelUse(String outTradeNo) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            showCommonNoticeDialog(this, ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("outTradeNo", outTradeNo);
            map.put("storeMerchantId", mStoreId);
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
            map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
            ServerClient.newInstance(MyApplication.getContext()).cancelUse(MyApplication.getContext(), Constants.TAG_COUPON_CANCEL, map);
        }
    }

    /**
     * 唤起银商刷卡支付
     */
    private void startYinshangCard(String money, String orderNo) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "全民惠";
        String transAppId = "消费";
        JSONObject transData = new JSONObject();
        try {
            transData.put("amt", money);//金额
            transData.put("appId", APPID);//appId
            transData.put("isNeedPrintReceipt", false);//交易结束后自动打单
            transData.put("extOrderNo", orderNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtil.d("Jeremy-request--" + transData.toString());
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
                LogUtil.d("Jeremy-" + reslutmsg);
                mYinshangPosBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                if (mYinshangPosBean != null) {
                    if ("0".equals(mYinshangPosBean.getResultCode())) {
                        if (mYinshangPosBean.getTransData() != null && "00".equals(mYinshangPosBean.getTransData().getResCode())) {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                payYinshangNotify(mYinshangPosBean);
                            }
                        } else {
                            syncDialog(mYinshangPosBean.getTransData().getResDesc());
                        }
                    } else {
                        showCommonNoticeDialog(OrderPayInfoActivity.this, mYinshangPosBean.getResultMsg());
                    }
                } else {
                    showCommonNoticeDialog(OrderPayInfoActivity.this, "支付回调数据有误，请确认支付结果！");
                }
                /*if (mYinshangPosBean.getTransData() != null && "00".equals(mYinshangPosBean.getTransData().getResCode())) {
                    startActivity(new Intent(OrderPayInfoActivity.this, MainActivity.class));
                }*/
            }
        };
        com.ums.AppHelper.callTrans(this, transAppName, transAppId, transData, listener);
    }

    /*创建订单*/
    private void createOrder(String money) {
        DialogUtil.safeShowDialog(mLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("money", money);
        map.put("storeMerchantId", MyApplication.getDefaultStore());
        map.put("thirdApiCode", mThirdApiCode);
        if (!TextUtils.isEmpty(mOutTradeNo) || mVipInfo != null) {
            BizDetailBean bizDetailBean = new BizDetailBean();
            bizDetailBean.setIs_biz(1);
            bizDetailBean.setBiz_type(4);
            bizDetailBean.setBiz_fee(totalCouponMoney);
            if (mVipInfo != null) {
                bizDetailBean.setCard_no(mVipInfo.getMallCardNo());
                bizDetailBean.setUid(mVipInfo.getBizUid());
                bizDetailBean.setMobile(mVipInfo.getMobile());
                bizDetailBean.setInterProviderType(mVipInfo.getInterProviderType());
            }
            Gson gson = new Gson();
            String bizDetial = gson.toJson(bizDetailBean);
            map.put("bizDetail", bizDetial);
            if (!TextUtils.isEmpty(mOutTradeNo)) {
                map.put("outTradeNo", mOutTradeNo);
            }
        }
        String deviceInfo = StringUtils.getDeviceInfo(AppHelper.getDeviceInfo());
        if (!TextUtils.isEmpty(deviceInfo)) {
            map.put("deviceInfo", deviceInfo);
        }
        if (!TextUtils.isEmpty(AppHelper.getIPAddress(MyApplication.getContext()))) {
            map.put("mchCreateIp", AppHelper.getIPAddress(MyApplication.getContext()));
        }
        if (!TextUtils.isEmpty(StringUtils.getDeviceInfo(AppHelper.getSN()))) {
            map.put("opDeviceId", StringUtils.getDeviceInfo(AppHelper.getSN()));
        }
        ServerClient.newInstance(MyApplication.getContext()).bCardPayment(MyApplication.getContext(), Constants.TAG_POS_FACE_PAY, map);
    }


    /*错误处理*/
    private void errorHandle(NoticeEvent event, ApiResult res) {
        if (event.getCls().equals(Constants.MSG_NET_ERROR)) {
            MyToast.showToastShort(getString(R.string.net_error));
        } else if (event.getCls().equals(Constants.ON_EVENT_TRUE) && res.getData() == null) {
            ToastHelper.showInfo(this, getString(R.string.error_arouse_bcard_pay));
        } else if (res.getError() != null) {
            if (res.getError().getCode() != null) {
                if (res.getError().getCode().equals(MyApplication.getFreeLogin())) {
                    if (res.getError().getMessage() != null) {
                        getLoginDialog(this, res.getError().getMessage());
                    }
                } else {
                    if (res.getError().getMessage() != null) {
                        ToastHelper.showInfo(this, res.getError().getMessage());
                    }
                }
            }
        }
    }


    /*支付成功通知*/
    private void payYinshangNotify(YinshangPosBean bean) {
        if (bean != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(bean.getTransData().getAmt())) {
                map.put("totalFee", new BigDecimal(bean.getTransData().getAmt()).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            }
            map.put("cardType", bean.getTransData().getCardType());
            if (mBilldata != null) {
                map.put("storeMerchantId", mBilldata.getStoreMerchantId());
                map.put("transactionId", mBilldata.getOrderNo());
                map.put("outTradeNo", mBilldata.getOutTradeNo());
            }
            map.put("payType", bean.getTransData().getCardType());
            map.put("timeEnd", DateUtil.formartTradeTime(bean.getTransData().getDate() + " " + bean.getTransData().getTime(), "yyyy/MM/dd HH:mm:ss"));
            map.put("voucherNo", bean.getTransData().getTraceNo());
            map.put("batchId", bean.getTransData().getBatchNo());
            map.put("refNo", bean.getTransData().getRefNo());
            map.put("payCardId", bean.getTransData().getCardNo());
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), NOTIFY_PAY_SUCCESS, map);
        }
    }


    @Override
    public void onItemClick(int position) {
        switch (position) {
            case 0:
                if (MyApplication.isStoreNull()) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                } else {
                    AppHelper.execVibrator(OrderPayInfoActivity.this);
                    toCaptureActivity();
                }
                break;
            case 1:
                if (MyApplication.isStoreNull()) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                } else {
                    AppHelper.execVibrator(OrderPayInfoActivity.this);
                    createOrderByThirdApiCode(ConfigUtil.getThirdApiCode());
                }
                break;
            case 2:
                if (MyApplication.isStoreNull()) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                } else {
                    AppHelper.execVibrator(OrderPayInfoActivity.this);
                    if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        if (!DateUtil.getDate().equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE))) {
                            wizardFukaSign();
                        } else {
                            createOrderByThirdApiCode(Constants.UNIONPAY_WIZARD_FUKA_BCARD);
                        }
                    } else {
                        createOrderByThirdApiCode(Constants.UNIONPAY_FUKA_BCARD);
                    }
                }
                break;
            case 3:
                if (MyApplication.isStoreNull()) {
                    MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                } else {
                    AppHelper.execVibrator(OrderPayInfoActivity.this);
                    createOrderByThirdApiCode(Constants.WECHAT_FACEPAY);
                }
                break;
        }
    }

    private void createOrderByThirdApiCode(String thirdApiCode) {
        if (totalMoney <= 0) {
            MyToast.showToastShort(getString(R.string.please_input_amount));
        } else {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                mThirdApiCode = thirdApiCode;
                createOrder(money);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }

        }
    }

    /**
     * 福卡签到
     */
    private void wizardFukaSign() {
        Map<String, Object> map = new HashMap<>();
        map.put("AppID", Constants.WJYS_APPID);
        map.put("AppName", "WJYS");
        map.put("OptCode", "01");
        map.put("OptPass", "0000");
        try {
            String login = MainActivity.instance.getCupService().login(new Gson().toJson(map));
            LogUtil.d("Jeremy—login==" + login);
            FuxunBean fuxunBean = new Gson().fromJson(login, FuxunBean.class);
            if (fuxunBean != null) {
                if ("00".equals(fuxunBean.getRespCode())) {//签到成功
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_FUKA_SIGN_DATE, DateUtil.getDate());
                    createOrderByThirdApiCode(Constants.UNIONPAY_WIZARD_FUKA_BCARD);
                } else {
                    showCommonNoticeDialog(this, TextUtils.isEmpty(fuxunBean.getRespDesc()) ? "签到失败！" : fuxunBean.getRespDesc());
                }
            } else {
                showCommonNoticeDialog(this, "签到失败！");
            }
        } catch (Exception e) {
            showCommonNoticeDialog(this, "签到失败！");
            e.printStackTrace();
        }
    }

    private long timeCount = 5;
    private int count = 1;
    private String dialogMessage = null;
    private NoticeDialog showDialog;

    private void syncOrderState() {
        if (count < 5) {
            timeCount = 5;
            count++;
            showDialog = new NoticeDialog(OrderPayInfoActivity.this, dialogMessage, "取消同步", R.layout.notice_dialog_check_order);
            showDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    cancelTask = true;
                    toSyncOrderStatus();
                    mHandler.removeCallbacks(myRunnable);
                }
            });
            DialogHelper.resize(OrderPayInfoActivity.this, showDialog);
            showDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            cancelTask = true;
            toSyncOrderStatus();
        }
    }

    private void toSyncOrderStatus() {
        if (!NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            if (cancelTask) {
                showSyncResult();
            } else {
                syncOrderState();
            }
        } else {
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                payYinshangNotify(mYinshangPosBean);
            } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        && Constants.UNIONPAY_WIZARD_FUKA_BCARD.equals(mThirdApiCode)) {
                    payFukaNotify(mFuxunBean);
                } else {
                    payNotify(mResultData);
                }
            }
        }
    }

    private void showSyncResult() {
        mSyncDialog = new SelectDialog(OrderPayInfoActivity.this, "订单状态同步失败", "取消", "继续同步", R.layout.select_common_dialog);
        mSyncDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                cancelTask = true;
                toSyncOrderStatus();
            }
        });
        mSyncDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
//                toMainActivity();
                //如果使用了优惠，需要调撤券接口
                isChecked = true;
                finishActivity();
            }
        });
        DialogHelper.resize(OrderPayInfoActivity.this, mSyncDialog);
        mSyncDialog.show();
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && showDialog != null) {
                showDialog.setMessage(getString(R.string.dialog_order_sync) + timeCount
                        + getString(R.string.dialog_sync_start) + (count) + getString(R.string.dialog_sync_end));
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (showDialog != null && showDialog.isShowing()) {
                    showDialog.dismiss();
                }
                if (count == 5)
                    cancelTask = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toSyncOrderStatus();
                    }
                });
            }
        }
    };

    private void remove() {
        if (showDialog != null) {
            showDialog.dismiss();
        }
        if (mSyncDialog != null) {
            mSyncDialog.dismiss();
        }
        mHandler.removeCallbacks(myRunnable);
    }

    private void queryOrder(String tag) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if ("TAG_AUTO_ORDER_PAY_DETAIL".equals(tag)) {
                map.put("orderNo", SpStayUtil.getString(MyApplication.getContext(), "BCARD_ORDER_NO"));
            } else {
                if (!StringUtils.isEmptyOrNull(mBilldata.getOrderNo())) {
                    map.put("orderNo", mBilldata.getOrderNo());
                }
            }
            ServerClient.newInstance(MyApplication.getContext()).getOrderDetail(MyApplication.getContext(), tag, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void syncDialog(String resDesc) {
        String title;
        if (TextUtils.isEmpty(resDesc)) {
            title = "支付失败，如有异议，请至订单详情页同步查询";
        } else {
            title = resDesc + "，如有异议，请至订单详情页同步查询";
        }
        SelectDialog syncSelectDialog = new SelectDialog(OrderPayInfoActivity.this, title, "取消", "去查询", R.layout.select_common_dialog);
        syncSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                queryOrder("TAG_ORDER_PAY_DETAIL");
            }
        });
        DialogHelper.resize(this, syncSelectDialog);
        syncSelectDialog.show();
    }

    private void toMainActivity() {
        Intent intent = new Intent(Constants.ACTION_RECEIVE_RESET_DATA);
        sendBroadcast(intent);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    /**
     * 唤起慧银刷卡支付
     */
    private void startActivityWizarposCard(String money, String orderNo) {
        LogUtil.d("money====" + money);
        Map<String, String> map = new HashMap<>();
        map.put("option", "consume");
        map.put("paytype", "card");
        map.put("amount", money);
        map.put("orderNo", orderNo);
        startActivityForResult(map, Constants.REQUEST_BCARD_PAY);
    }

    /**
     * 慧银pos获取商户信息
     */
    private void startActivityWizarposMerInfo() {
        Map<String, String> map = new HashMap<>();
        map.put("option", "getMerInfo");
        startActivityForResult(map, Constants.REQUEST_POS_MERCHANT_INFO);
    }

    private void startActivityForResult(Map<String, String> map, int requestCode) {
        unbindDeviceService();
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("net.worthtech.worthcasher", "net.worthtech.worthcasher.activity.PayService"));
        for (Map.Entry<String, String> arg : map.entrySet()) {
            intent.putExtra(arg.getKey(), arg.getValue());
        }
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_BCARD_PAY) {
            if (data != null) {
                try {
                    LogUtil.e("REQUEST_BCARD_PAY", URLDecoder.decode(data.toURI(), "UTF-8"));
                } catch (Exception e) {

                }
                String message;
                String responseCode;
                if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    message = data.getStringExtra("respmsg");
                    responseCode = data.getStringExtra("result");
                    mResultData = data;
                    if ("00".equals(responseCode)) {//支付成功
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(this, getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            payNotify(data);
                        }
                    } else {
                        syncDialog(message);
                    }
                } else {
                    message = data.getStringExtra("message");
                    responseCode = data.getStringExtra("responseCode");
                    if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                        ToastHelper.showInfo(this, message);
                    }
                }
                SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
            } else {
                showCommonNoticeDialog(this, "支付回调数据有误，请确认支付结果！");
            }
        } else if (requestCode == Constants.REQUEST_POS_MERCHANT_INFO) {
            if (data != null) {
                try {
                    LogUtil.e("REQUEST_POS_MERCHANT_INFO", URLDecoder.decode(data.toURI(), "UTF-8"));
                } catch (Exception e) {

                }
                String mer_name = data.getStringExtra("mer_name");
                String mer_id = data.getStringExtra("mer_id");
                String ter_id = data.getStringExtra("ter_id");
                if (!TextUtils.isEmpty(ter_id)) {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, ter_id);
                } else {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, "");
                }
                if (mer_id.equals(mPosAccountBean.getThirdMerchantId())) {
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, true);
                } else {
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, false);
                    MyToast.showToastShort("第三方商户号有误！(" + mPosAccountBean.getThirdMerchantId() + ")与(" + mer_id + ")不一致");
                }
            }
        }
    }

    /**
     * 易生刷卡支付通知
     *
     * @param data
     */
    private void payNotify(Intent data) {
        if (data != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", data.getStringExtra("orderNo"));
            if (!TextUtils.isEmpty(data.getStringExtra("amount"))) {
                map.put("totalFee", new BigDecimal(data.getStringExtra("amount")).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            }
            map.put("cardType", data.getStringExtra("paytype"));
            if (mBilldata != null) {
                map.put("storeMerchantId", mBilldata.getStoreMerchantId());
            }
            map.put("payType", data.getStringExtra("paytype"));
            String printInfo = data.getStringExtra("printInfo");
            if (!TextUtils.isEmpty(printInfo)) {
                Gson gson = new Gson();
                EasypayInfo easypayInfo = gson.fromJson(printInfo, EasypayInfo.class);
                map.put("timeEnd", DateUtil.formartTradeTime(easypayInfo.getDate_time()));
                map.put("voucherNo", easypayInfo.getTrace());
                map.put("batchId", easypayInfo.getBatch_no());
                map.put("refNo", easypayInfo.getRefer_no());
                map.put("payCardId", easypayInfo.getCard_no());
            }
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), NOTIFY_PAY_SUCCESS, map);
        }
    }

    /**
     * 易生福卡支付通知
     *
     * @param data
     */
    private void payFukaNotify(FuxunBean data) {
        if (data != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", data.getTrxID());
            if (!TextUtils.isEmpty(data.getTransAmount())) {
                map.put("totalFee", new BigDecimal(data.getTransAmount()));
            }
//            map.put("cardType", data.getStringExtra("paytype"));
            if (mBilldata != null) {
                map.put("storeMerchantId", mBilldata.getStoreMerchantId());
                map.put("timeEnd", DateUtil.formartWizardFukaTradeTime(data.getTransDate() + data.getTransTime(), mBilldata.getCreateTime()));
            }
            map.put("payType", data.getTransType());
            map.put("voucherNo", data.getCertNum());
            map.put("batchId", data.getBatchNum());
            map.put("refNo", data.getReferCode());
            map.put("payCardId", data.getCardNum());
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), NOTIFY_PAY_SUCCESS, map);
        }
    }

    //判断支付方式是否开通
    private boolean checkApiProvider(int apiProvider) {
        /*
        支付方式, 1:微信;2:支付宝;3:财付通;4:qq钱包;5:银联;6:世明;10:刷卡;11:福卡;12:饭卡支付;13:银联POS刷脸支付;14:猫酷支付;15:微信POS刷脸支付;16:美团点评支付;17:数字人民币;18:印享星券支付;99:未知;
        * */
        String allApiProvider = SpUtil.getString(MyApplication.getContext(), Constants.SP_ALL_APIPROVIDERS);
        if (!TextUtils.isEmpty(allApiProvider)) {
            List<QueryPayTypeBean.Entity> apiProviders = new Gson().fromJson(allApiProvider, new TypeToken<List<QueryPayTypeBean.Entity>>() {
            }.getType());
            if (apiProviders != null && apiProviders.size() > 0) {
                for (QueryPayTypeBean.Entity entity : apiProviders) {
                    if (apiProvider == entity.getApiProvider())
                        return true;
                }
            }
        }
        return false;
    }
}
