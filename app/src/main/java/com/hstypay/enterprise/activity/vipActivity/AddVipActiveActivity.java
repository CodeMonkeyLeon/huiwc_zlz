package com.hstypay.enterprise.activity.vipActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.DateDialog;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.VipActiveBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: 添加收银员页面
 */
public class AddVipActiveActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private ImageView mIvBack, mIvStartTimeDelete, mIvEndTimeDelete, mIvActiveDelete1, mIvActiveDelete2, mIvActiveDelete3, mIvActiveDelete4, mIvActiveAdd;
    private Button mBtnSubmit;
    private TextView mButton, mTvTitle, mTvStartTime, mTvEndTime;
    private int mActiveType;
    private int mActiveItemType = 1;
    private EditTextDelete mEtActiveName;
    private RadioGroup mRgActiveType;
    private RadioButton mRbActiveGift, mRbActiveReduce, mRbActiveDiscount;
    private RelativeLayout mRlActive1, mRlActive2, mRlActive3, mRlActive4;
    private EditText mEtMoney1, mEtMoney2, mEtMoney3, mEtMoney4, mEtSale1, mEtSale2, mEtSale3, mEtSale4, mEtActiveRemark;
    private TextView mTvMoney1, mTvMoney2, mTvMoney3, mTvMoney4, mTvActiveType1, mTvActiveType2, mTvActiveType3, mTvActiveType4, mTvYuanDisCount1, mTvYuanDisCount2, mTvYuanDisCount3, mTvYuanDisCount4;
    private ArrayList<TextWatcher> mListeners = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vip_active);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_add_vip_active);
        mButton.setVisibility(View.INVISIBLE);

        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mEtActiveName = (EditTextDelete) findViewById(R.id.et_active_name);
        mTvStartTime = (TextView) findViewById(R.id.tv_active_start_time);
        mTvEndTime = (TextView) findViewById(R.id.tv_active_end_time);
        mIvStartTimeDelete = (ImageView) findViewById(R.id.iv_start_time_delete);
        mIvEndTimeDelete = (ImageView) findViewById(R.id.iv_end_time_delete);

        mRgActiveType = (RadioGroup) findViewById(R.id.rg_active_type);
        mRbActiveGift = (RadioButton) findViewById(R.id.rb_active_gift);
        mRbActiveReduce = (RadioButton) findViewById(R.id.rb_active_reduce);
        mRbActiveDiscount = (RadioButton) findViewById(R.id.rb_active_discount);

        mRlActive1 = (RelativeLayout) findViewById(R.id.rl_active1);
        mRlActive2 = (RelativeLayout) findViewById(R.id.rl_active2);
        mRlActive3 = (RelativeLayout) findViewById(R.id.rl_active3);
        mRlActive4 = (RelativeLayout) findViewById(R.id.rl_active4);

        mEtMoney1 = (EditText) findViewById(R.id.et_recharge_money1);
        mEtMoney2 = (EditText) findViewById(R.id.et_recharge_money2);
        mEtMoney3 = (EditText) findViewById(R.id.et_recharge_money3);
        mEtMoney4 = (EditText) findViewById(R.id.et_recharge_money4);

        mEtSale1 = (EditText) findViewById(R.id.et_recharge_sale1);
        mEtSale2 = (EditText) findViewById(R.id.et_recharge_sale2);
        mEtSale3 = (EditText) findViewById(R.id.et_recharge_sale3);
        mEtSale4 = (EditText) findViewById(R.id.et_recharge_sale4);

        mEtActiveRemark = (EditText) findViewById(R.id.et_tv_active_remark);

        mTvMoney1 = (TextView) findViewById(R.id.tv_recharge_money1);
        mTvMoney2 = (TextView) findViewById(R.id.tv_recharge_money2);
        mTvMoney3 = (TextView) findViewById(R.id.tv_recharge_money3);
        mTvMoney4 = (TextView) findViewById(R.id.tv_recharge_money4);

        mTvActiveType1 = (TextView) findViewById(R.id.tv_active_type_text1);
        mTvActiveType2 = (TextView) findViewById(R.id.tv_active_type_text2);
        mTvActiveType3 = (TextView) findViewById(R.id.tv_active_type_text3);
        mTvActiveType4 = (TextView) findViewById(R.id.tv_active_type_text4);

        mTvYuanDisCount1 = (TextView) findViewById(R.id.tv_yuan_discount1);
        mTvYuanDisCount2 = (TextView) findViewById(R.id.tv_yuan_discount2);
        mTvYuanDisCount3 = (TextView) findViewById(R.id.tv_yuan_discount3);
        mTvYuanDisCount4 = (TextView) findViewById(R.id.tv_yuan_discount4);

        mIvActiveAdd = (ImageView) findViewById(R.id.iv_add_active_item);
        mIvActiveDelete1 = (ImageView) findViewById(R.id.iv_active_delete1);
        mIvActiveDelete2 = (ImageView) findViewById(R.id.iv_active_delete2);
        mIvActiveDelete3 = (ImageView) findViewById(R.id.iv_active_delete3);
        mIvActiveDelete4 = (ImageView) findViewById(R.id.iv_active_delete4);
    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

        mTvStartTime.setOnClickListener(this);
        mTvEndTime.setOnClickListener(this);
        mIvStartTimeDelete.setOnClickListener(this);
        mIvEndTimeDelete.setOnClickListener(this);

        mIvActiveAdd.setOnClickListener(this);
        mIvActiveDelete1.setOnClickListener(this);
        mIvActiveDelete2.setOnClickListener(this);
        mIvActiveDelete3.setOnClickListener(this);
        mIvActiveDelete4.setOnClickListener(this);

        mRgActiveType.setOnCheckedChangeListener(this);
        addEditListener(mEtActiveName);
        addMoneyTextWatcher(mEtMoney1, mEtMoney2, mEtMoney3, mEtMoney4);
    }

    public void initData() {
        mTvStartTime.setText(DateUtil.formatsYYYYMD(System.currentTimeMillis()));
        mTvEndTime.setText(DateUtil.getAfterThirtyDays());
        mActiveType = getIntent().getIntExtra(Constants.INTENT_ACTIVE_TYPE, 0);
        if (mActiveType == 3) {
            mRbActiveGift.setVisibility(View.GONE);
            mRbActiveReduce.setChecked(true);
            mActiveItemType = 2;
        }
        setView(mActiveType, mActiveItemType);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_active_start_time:
                DateDialog startTimeDialog = new DateDialog(AddVipActiveActivity.this, R.style.ThemeDialog, System.currentTimeMillis());
                startTimeDialog.setOnClickOkListener(new DateDialog.OnClickOkListener() {
                    @Override
                    public void clickOk(String date) {
                        mTvStartTime.setText(date + " 00:00");
                        if (!TextUtils.isEmpty(mTvEndTime.getText().toString().trim())
                                && DateUtil.getTime(mTvStartTime.getText().toString().trim(), "yyyy-MM-dd HH:mm") > DateUtil.getTime(mTvEndTime.getText().toString().trim(), "yyyy-MM-dd HH:mm")-1) {
                            mTvEndTime.setText(date + " 24:00");
                        }
                        setButton();
                    }
                });
                startTimeDialog.show();
                break;
            case R.id.tv_active_end_time:
                long minDate;
                if (TextUtils.isEmpty(mTvStartTime.getText().toString().trim())) {
                    minDate = System.currentTimeMillis();
                } else {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = sdf.parse(mTvStartTime.getText().toString().trim());
                        minDate = date.getTime();
                    } catch (ParseException e) {
                        minDate = System.currentTimeMillis();
                        e.printStackTrace();
                    }
                }
                DateDialog endTimeDialog = new DateDialog(AddVipActiveActivity.this, R.style.ThemeDialog, minDate);
                endTimeDialog.setOnClickOkListener(new DateDialog.OnClickOkListener() {
                    @Override
                    public void clickOk(String date) {
                        mTvEndTime.setText(date + " 24:00");
                        setButton();
                    }
                });
                endTimeDialog.show();
                break;
            case R.id.iv_start_time_delete:
                mTvStartTime.setText("");
                mBtnSubmit.setBackgroundResource(R.color.btn_active_preview_unable);
                mBtnSubmit.setEnabled(false);
                break;
            case R.id.iv_end_time_delete:
                mTvEndTime.setText("");
                mBtnSubmit.setBackgroundResource(R.color.btn_active_preview_unable);
                mBtnSubmit.setEnabled(false);
                break;
            case R.id.iv_add_active_item:
                if (visibleItem(mRlActive1, mRlActive2, mRlActive3, mRlActive4) == 4) {
                    MyToast.showToastShort(getString(R.string.toast_active_item_count));
                    return;
                }
                addActiveItem(mRlActive1, mRlActive2, mRlActive3, mRlActive4);
                setButton();
                break;
            case R.id.iv_active_delete1:
                if (visibleItem(mRlActive1, mRlActive2, mRlActive3, mRlActive4) <= 1) {
                    MyToast.showToastShort(getString(R.string.toast_active_item_least));
                    return;
                }
                mRlActive1.setVisibility(View.GONE);
                mEtMoney1.setText("");
                mEtSale1.setText("");
                setButton();
                break;
            case R.id.iv_active_delete2:
                if (visibleItem(mRlActive1, mRlActive2, mRlActive3, mRlActive4) <= 1) {
                    MyToast.showToastShort(getString(R.string.toast_active_item_least));
                    return;
                }
                mRlActive2.setVisibility(View.GONE);
                mEtMoney2.setText("");
                mEtSale2.setText("");
                setButton();
                break;
            case R.id.iv_active_delete3:
                if (visibleItem(mRlActive1, mRlActive2, mRlActive3, mRlActive4) <= 1) {
                    MyToast.showToastShort(getString(R.string.toast_active_item_least));
                    return;
                }
                mRlActive3.setVisibility(View.GONE);
                mEtMoney3.setText("");
                mEtSale3.setText("");
                setButton();
                break;
            case R.id.iv_active_delete4:
                if (visibleItem(mRlActive1, mRlActive2, mRlActive3, mRlActive4) <= 1) {
                    MyToast.showToastShort(getString(R.string.toast_active_item_least));
                    return;
                }
                mRlActive4.setVisibility(View.GONE);
                mEtMoney4.setText("");
                mEtSale4.setText("");
                setButton();
                break;
            case R.id.btn_submit:
                MtaUtils.mtaId(AddVipActiveActivity.this,"G004");
                submitData();
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_active_gift:
                mActiveItemType = 1;
                break;
            case R.id.rb_active_reduce:
                mActiveItemType = 2;
                break;
            case R.id.rb_active_discount:
                mActiveItemType = 3;
                break;
        }
        setView(mActiveType, mActiveItemType);
    }

    private void setView(int activeType, int activeItemType) {
        if (activeType == 2) {
            if (activeItemType == 1) {
                setText(getString(R.string.tv_active_text_recharge), mTvMoney1, mTvMoney2, mTvMoney3, mTvMoney4);
                setText(getString(R.string.tv_active_text_gift), mTvActiveType1, mTvActiveType2, mTvActiveType3, mTvActiveType4);
                setText(getString(R.string.tv_active_text_yuan), mTvYuanDisCount1, mTvYuanDisCount2, mTvYuanDisCount3, mTvYuanDisCount4);
            } else if (activeItemType == 2) {
                setText(getString(R.string.tv_active_text_recharge), mTvMoney1, mTvMoney2, mTvMoney3, mTvMoney4);
                setText(getString(R.string.tv_active_text_reduce), mTvActiveType1, mTvActiveType2, mTvActiveType3, mTvActiveType4);
                setText(getString(R.string.tv_active_text_yuan), mTvYuanDisCount1, mTvYuanDisCount2, mTvYuanDisCount3, mTvYuanDisCount4);
            } else if (activeItemType == 3) {
                setText(getString(R.string.tv_active_text_recharge_fill), mTvMoney1, mTvMoney2, mTvMoney3, mTvMoney4);
                setText("", mTvActiveType1, mTvActiveType2, mTvActiveType3, mTvActiveType4);
                setText(getString(R.string.tv_active_text_discount), mTvYuanDisCount1, mTvYuanDisCount2, mTvYuanDisCount3, mTvYuanDisCount4);
            }
        } else if (activeType == 3) {
            if (activeItemType == 2) {
                setText(getString(R.string.tv_active_text_consume), mTvMoney1, mTvMoney2, mTvMoney3, mTvMoney4);
                setText(getString(R.string.tv_active_text_reduce), mTvActiveType1, mTvActiveType2, mTvActiveType3, mTvActiveType4);
                setText(getString(R.string.tv_active_text_yuan), mTvYuanDisCount1, mTvYuanDisCount2, mTvYuanDisCount3, mTvYuanDisCount4);
            } else if (activeItemType == 3) {
                setText(getString(R.string.tv_active_text_consume_fill), mTvMoney1, mTvMoney2, mTvMoney3, mTvMoney4);
                setText("", mTvActiveType1, mTvActiveType2, mTvActiveType3, mTvActiveType4);
                setText(getString(R.string.tv_active_text_discount), mTvYuanDisCount1, mTvYuanDisCount2, mTvYuanDisCount3, mTvYuanDisCount4);
            }
        }
        setText("", mEtMoney1, mEtMoney2, mEtMoney3, mEtMoney4, mEtSale1, mEtSale2, mEtSale3, mEtSale4);
        onlyOne(mRlActive1, mRlActive2, mRlActive3, mRlActive4);
        removeTextWatcher(mEtSale1, mEtSale2, mEtSale3, mEtSale4);
        if (activeItemType == 3) {
            addDiscountTextWatcher(mEtSale1, mEtSale2, mEtSale3, mEtSale4);
        } else {
            addMoneyTextWatcher(mEtSale1, mEtSale2, mEtSale3, mEtSale4);
        }
    }

    private void setText(String string, TextView... tvs) {
        for (TextView tv : tvs) {
            tv.setText(string);
        }
    }

    private void addActiveItem(RelativeLayout... rls) {
        for (RelativeLayout rl : rls) {
            if (rl.getVisibility() == View.GONE) {
                rl.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    private void onlyOne(RelativeLayout rl1, RelativeLayout... rls) {
        for (RelativeLayout rl : rls) {
            if (rl.getVisibility() == View.VISIBLE) {
                rl.setVisibility(View.GONE);
            }
        }
        if (rl1.getVisibility() == View.GONE) {
            rl1.setVisibility(View.VISIBLE);
        }
    }

    private int visibleItem(RelativeLayout... rls) {
        int count = 0;
        for (RelativeLayout rl : rls) {
            if (rl.getVisibility() == View.VISIBLE) {
                count++;
            }
        }
        return count;
    }

    private void setButton() {
        boolean infoNotNull;
        boolean itemNotEmpty1;
        boolean itemNotEmpty2;
        boolean itemNotEmpty3;
        boolean itemNotEmpty4;
        if (!TextUtils.isEmpty(mEtActiveName.getText().toString().trim())
                && !TextUtils.isEmpty(mTvStartTime.getText().toString().trim())
                && !TextUtils.isEmpty(mTvEndTime.getText().toString().trim())) {
            infoNotNull = true;
        } else {
            infoNotNull = false;
        }
        if (mRlActive1.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(mEtMoney1.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSale1.getText().toString().trim())) {
                itemNotEmpty1 = false;
            } else {
                itemNotEmpty1 = true;
            }
        } else {
            itemNotEmpty1 = true;
        }
        if (mRlActive2.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(mEtMoney2.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSale2.getText().toString().trim())) {
                itemNotEmpty2 = false;
            } else {
                itemNotEmpty2 = true;
            }
        } else {
            itemNotEmpty2 = true;
        }
        if (mRlActive3.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(mEtMoney3.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSale3.getText().toString().trim())) {
                itemNotEmpty3 = false;
            } else {
                itemNotEmpty3 = true;
            }
        } else {
            itemNotEmpty3 = true;
        }
        if (mRlActive4.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(mEtMoney4.getText().toString().trim())
                    || TextUtils.isEmpty(mEtSale4.getText().toString().trim())) {
                itemNotEmpty4 = false;
            } else {
                itemNotEmpty4 = true;
            }
        } else {
            itemNotEmpty4 = true;
        }
        if (infoNotNull && itemNotEmpty1 && itemNotEmpty2 && itemNotEmpty3 && itemNotEmpty4) {
            mBtnSubmit.setBackgroundResource(R.drawable.selector_btn_active_preview);
            mBtnSubmit.setEnabled(true);
        } else {
            mBtnSubmit.setBackgroundResource(R.color.btn_active_preview_unable);
            mBtnSubmit.setEnabled(false);
        }
    }

    private void addEditListener(EditText... ets) {
        for (EditText et : ets) {
            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    setButton();
                }
            });
        }
    }

    private void submitData() {
        VipActiveBean activeBean = new VipActiveBean();
        activeBean.setActiveType(mActiveType);
        activeBean.setActiveItemType(mActiveItemType);
        activeBean.setActiveName(mEtActiveName.getText().toString().trim());
        activeBean.setStartTime(DateUtil.getTime(mTvStartTime.getText().toString().trim(), "yyyy-MM-dd HH:mm"));
        activeBean.setEndTime(DateUtil.getTime(mTvEndTime.getText().toString().trim(), "yyyy-MM-dd HH:mm"));
        activeBean.setActiveRemark(mEtActiveRemark.getText().toString().trim());

        List<VipActiveBean.ActiveItemBean> activeItem = new ArrayList<>();
        if (mRlActive1.getVisibility() == View.VISIBLE) {
            VipActiveBean.ActiveItemBean itemBean = new VipActiveBean().new ActiveItemBean();
            itemBean.setMoney(mEtMoney1.getText().toString().trim());
            itemBean.setSale(mEtSale1.getText().toString().trim());
            activeItem.add(itemBean);
        }
        if (mRlActive2.getVisibility() == View.VISIBLE) {
            VipActiveBean.ActiveItemBean itemBean = new VipActiveBean().new ActiveItemBean();
            itemBean.setMoney(mEtMoney2.getText().toString().trim());
            itemBean.setSale(mEtSale2.getText().toString().trim());
            activeItem.add(itemBean);
        }
        if (mRlActive3.getVisibility() == View.VISIBLE) {
            VipActiveBean.ActiveItemBean itemBean = new VipActiveBean().new ActiveItemBean();
            itemBean.setMoney(mEtMoney3.getText().toString().trim());
            itemBean.setSale(mEtSale3.getText().toString().trim());
            activeItem.add(itemBean);
        }
        if (mRlActive4.getVisibility() == View.VISIBLE) {
            VipActiveBean.ActiveItemBean itemBean = new VipActiveBean().new ActiveItemBean();
            itemBean.setMoney(mEtMoney4.getText().toString().trim());
            itemBean.setSale(mEtSale4.getText().toString().trim());
            activeItem.add(itemBean);
        }
        activeBean.setActiveItem(activeItem);
        Intent intent = new Intent(AddVipActiveActivity.this, VipActivePreviewActivity.class);
        intent.putExtra(Constants.INTENT_ACTIVE_DATA, activeBean);
        intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_ADD_VIP_ACTIVE);
        startActivity(intent);
    }

    private void addDiscountTextWatcher(EditText... ets) {
        for (EditText et : ets) {
            setFloatEditText(et);
        }
    }

    private void addMoneyTextWatcher(EditText... ets) {
        for (EditText et : ets) {
            setIntEditText(et);
        }
    }

    private void setFloatEditText(final EditText editText) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 1) {
                        s = s.toString().subSequence(0,
                                s.toString().indexOf(".") + 2);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().equals(".")) {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }
                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        editText.setText(s.subSequence(1, 2));
                        editText.setSelection(1);
                        return;
                    }
                }
                if (Utils.Double.tryParse(s.toString().trim(),0)>=10){
                    editText.setText(s.subSequence(0, 1));
                    editText.setSelection(1);
                    return;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setButton();
            }
        };
        editText.addTextChangedListener(textWatcher);
        mListeners.add(textWatcher);
    }

    private void setIntEditText(final EditText editText) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.toString().contains(".")) {
                    s = s.toString().subSequence(0,
                            s.toString().indexOf("."));
                    editText.setText(s);
                    editText.setSelection(s.length());
                }
                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        editText.setText(s.subSequence(1, 2));
                        editText.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setButton();
            }
        };
        editText.addTextChangedListener(textWatcher);
        mListeners.add(textWatcher);
    }

    private void removeTextWatcher(EditText... ets) {
        if (mListeners != null && mListeners.size() > 0) {
            for (EditText et : ets) {
                for (TextWatcher listener : mListeners) {
                    et.removeTextChangedListener(listener);
                }
            }
            mListeners.clear();
        }
    }
}
