package com.hstypay.enterprise.activity.vipActivity;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectSendPopupWindow;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveRuleBean;
import com.hstypay.enterprise.bean.MaterialBean;
import com.hstypay.enterprise.bean.VipActiveBean;
import com.hstypay.enterprise.bean.VipActiveItem;
import com.hstypay.enterprise.bean.VipActiveListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

public class MaterialActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnDownLoad;
    private TextView mButton, mTvTitle;
    private RelativeLayout mRlTypeOne, mRlTypeTwo;
    private View mViewTypeOne, mViewTypeTwo;
    private WebView mWebviewTypeOne, mWebviewTypeTwo;
    private ProgressBar mPgTypeOne, mPgTypeTwo;
    private VipActiveListBean.DataEntity.DataListEntity mActiveInfo;
    private VipActiveBean mVipActiveBean;
    private SafeDialog dialog;
    private List<File> mFiles;
    private List<Uri> uris = new ArrayList<>();
    private boolean request1;
    private boolean request2;
    private int type = 1;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }
    };
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);

        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_material);
        mButton.setVisibility(View.INVISIBLE);

        mRlTypeOne = (RelativeLayout) findViewById(R.id.rl_material_type_one);
        mRlTypeTwo = (RelativeLayout) findViewById(R.id.rl_material_type_two);
        mViewTypeOne = findViewById(R.id.view_type_one);
        mViewTypeTwo = findViewById(R.id.view_type_two);
        mBtnDownLoad = (Button) findViewById(R.id.btn_download);

        mWebviewTypeOne = (WebView) findViewById(R.id.web_view_type_one);
        mWebviewTypeTwo = (WebView) findViewById(R.id.web_view_type_two);
        mPgTypeOne = (ProgressBar) findViewById(R.id.progressBar);

        WebSettings webSettings1 = mWebviewTypeOne.getSettings();
        WebSettings webSettings2 = mWebviewTypeTwo.getSettings();
        setZoom(webSettings1);
        setZoom(webSettings2);
        setSettings(webSettings1);
        setSettings(webSettings2);
        setWebChromeClient(mWebviewTypeOne);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlTypeOne.setOnClickListener(this);
        mRlTypeTwo.setOnClickListener(this);
        mBtnDownLoad.setOnClickListener(this);
    }

    public void initData() {
        mFiles = new ArrayList<>();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (Constants.INTENT_ACTIVE_EDIT.equals(mIntentName)) {
            mVipActiveBean = (VipActiveBean) getIntent().getSerializableExtra(Constants.INTENT_EDIT_ACTIVE_INFO);
            if (mVipActiveBean.getActiveType() == 2) {
                mWebviewTypeOne.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_ONE + "1");
                mWebviewTypeTwo.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_ONE + "2");
            } else if (mVipActiveBean.getActiveType() == 3) {
                mWebviewTypeOne.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_TWO + "1");
                mWebviewTypeTwo.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_TWO + "2");
            }
        } else {
            mActiveInfo = (VipActiveListBean.DataEntity.DataListEntity) getIntent().getSerializableExtra(Constants.INTENT_MATERIAL_ACTIVE_INFO);
            if (mActiveInfo.getScene_type() == 2) {
                mWebviewTypeOne.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_ONE + "1");
                mWebviewTypeTwo.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_ONE + "2");
            } else if (mActiveInfo.getScene_type() == 3) {
                mWebviewTypeOne.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_TWO + "1");
                mWebviewTypeTwo.loadUrl(Constants.URL_VIP_ACTIVE_PREVIEW_TWO + "2");
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if (Constants.INTENT_ACTIVE_EDIT.equals(mIntentName)) {
                    startActivity(new Intent(MaterialActivity.this, VipActiveActivity.class));
                }
                finish();
                break;
            case R.id.rl_material_type_one:
                if (mWebviewTypeOne.getVisibility() == View.GONE) {
                    mViewTypeOne.setVisibility(View.VISIBLE);
                    mViewTypeTwo.setVisibility(View.INVISIBLE);
                    mWebviewTypeOne.setVisibility(View.VISIBLE);
                    mWebviewTypeTwo.setVisibility(View.GONE);
                    type = 1;
                }
                break;
            case R.id.rl_material_type_two:
                if (mWebviewTypeTwo.getVisibility() == View.GONE) {
                    mViewTypeOne.setVisibility(View.INVISIBLE);
                    mViewTypeTwo.setVisibility(View.VISIBLE);
                    mWebviewTypeTwo.setVisibility(View.VISIBLE);
                    mWebviewTypeOne.setVisibility(View.GONE);
                    type = 2;
                }
                break;
            case R.id.btn_download:
                if (Constants.INTENT_ACTIVE_EDIT.equals(mIntentName)) {
                    createEditMaterial(mVipActiveBean);
                } else {
                    createMaterial(mActiveInfo);
                }
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CREATE_MATERIAL)) {
            dismissLoading();
            MaterialBean msg = (MaterialBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(MaterialActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        /*if (request1 && request2) {
                            clickSendIcon(mFiles);
                        } else {*/
                        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                            if (type == 1) {
                                DownLoadImages(msg.getData().getPosterStyle1Pic(), msg.getData().getTableStickerStyle1Pic());
                            } else {
                                DownLoadImages(msg.getData().getPosterStyle2Pic(), msg.getData().getTableStickerStyle2Pic());
                            }
                        } else {
                            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                        }
                        //}
                    } else {
                        MyToast.showToastShort(getString(R.string.data_error));
                    }
                    break;
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }


    private void clickSendIcon(final List<File> files) {
        SelectSendPopupWindow picPopupWindow = new SelectSendPopupWindow(MaterialActivity.this, new SelectSendPopupWindow.HandleIcon() {

            @Override
            public void sendToFriends() {
                if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
                    Intent intent = new Intent();
                    ComponentName comp;
                    comp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareToTimeLineUI");
                    intent.putExtra("Kdescription", "分享朋友圈的图片说明");
                    intent.setComponent(comp);
                    intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intent.setType("image/*");

                    ArrayList<Uri> imageUris = new ArrayList<>();
                    for (File file : files) {
                        LogUtil.d("files=====" + file);
                        //imageUris.add(Uri.fromFile(f));
                        // 判断版本大于等于7.0
                        Uri data = getImageContentUri(MyApplication.getContext(), file.getAbsoluteFile());
                        LogUtil.d("files==uri===" + data);
                        imageUris.add(data);
                    }

                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
                    startActivity(intent);
                }
            }

            @Override
            public void sendToWechat() {
                if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
                    Intent intent = new Intent();
                    ComponentName comp;
                    comp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI");
                    intent.putExtra("Kdescription", "分享朋友圈的图片说明");
                    intent.setComponent(comp);
                    intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intent.setType("image/*");

                    ArrayList<Uri> imageUris = new ArrayList<>();
                    for (File file : files) {
                        LogUtil.d("files=====" + file);
                        //imageUris.add(Uri.fromFile(f));
                        // 判断版本大于等于7.0
                        Uri data = getImageContentUri(MyApplication.getContext(), file.getAbsoluteFile());
                        LogUtil.d("files==uri===" + data);
                        imageUris.add(data);
                    }

                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
                    startActivity(intent);
                } else {
                    MyToast.showToastShort("请先安装微信app");
                }
            }

            @Override
            public void sendToQQ() {

                if (AppHelper.isQQClientAvailable(MyApplication.getContext())) {
                    Intent intent = new Intent();
                    ComponentName comp;
                    comp = new ComponentName("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity");
                    intent.putExtra("Kdescription", "分享朋友圈的图片说明");
                    intent.setComponent(comp);
                    intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intent.setType("image/*");

                    ArrayList<Uri> imageUris = new ArrayList<>();
                    for (File file : files) {
                        LogUtil.d("files=====" + file);
                        //imageUris.add(Uri.fromFile(f));
                        // 判断版本大于等于7.0
                        Uri data = getImageContentUri(MyApplication.getContext(), file.getAbsoluteFile());
                        LogUtil.d("files==uri===" + data);
                        imageUris.add(data);
                    }

                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
                    startActivity(intent);
                } else {
                    MyToast.showToastShort("请先安装QQ应用");
                }
            }

            @Override
            public void sendToAblum() {
                showCommonNoticeDialog(MaterialActivity.this, getString(R.string.dialog_copy_picture));
            }

            @Override
            public void sendToEmail() {

            }
        });

        picPopupWindow.showAtLocation(mBtnDownLoad, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void ShareImage(SHARE_MEDIA platform, String image) {
        UMImage pic = new UMImage(MaterialActivity.this, image);
        //pic.setThumb(new UMImage(MaterialActivity.this, thumb));
        new ShareAction(MaterialActivity.this).withMedia(pic).setPlatform(platform).setCallback(shareListener).withText("").share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }

    public void setZoom(WebSettings settings) {
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
    }

    private void setSettings(WebSettings webSettings) {
        webSettings.setAllowFileAccess(true);
        String ua = webSettings.getUserAgentString();
        webSettings.setUserAgentString(ua + AppHelper.getUserAgent(MyApplication.getContext()));
        //设置为可调用js方法
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        webSettings.setJavaScriptEnabled(true);

    }

    public void setWebChromeClient(WebView wvResgister) {
        wvResgister.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mPgTypeOne.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    mPgTypeOne.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    mPgTypeOne.setProgress(newProgress);//设置进度值
                }
            }

        });
    }

    private void createMaterial(VipActiveListBean.DataEntity.DataListEntity activeInfo) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("activityId", activeInfo.getActivity_id());
            map.put("activityName", activeInfo.getName());
            map.put("beginDate", DateUtil.formatTime(activeInfo.getBegin_date()));
            map.put("endDate", DateUtil.formatTime(activeInfo.getEnd_date()));
            map.put("activityType", activeInfo.getActivity_type() + "");
            map.put("activityScene", activeInfo.getScene_type());
            Gson gson = new Gson();
            List<ActiveRuleBean> ruleList = new ArrayList<>();
            VipActiveItem[] vipActiveItems = gson.fromJson(activeInfo.getRule(), VipActiveItem[].class);
            if (vipActiveItems != null && vipActiveItems.length > 0) {
                for (int i = 0; i < vipActiveItems.length; i++) {
                    ActiveRuleBean itemBean = new ActiveRuleBean();
                    itemBean.setConditionAmount(new BigDecimal(vipActiveItems[i].getConditionAmount().longValue()));
                    if (activeInfo.getActivity_type() == 3) {
                        itemBean.setDiscount(new BigDecimal(vipActiveItems[i].getDiscountVar().multiply(new BigDecimal(100)).longValue()));
                    } else {
                        itemBean.setDiscount(new BigDecimal(vipActiveItems[i].getDiscountVar().longValue()));
                    }
                    ruleList.add(itemBean);
                }
            }
            //String rule = gson.toJson(ruleList);
            map.put("ruleList", ruleList.toArray());
            ServerClient.newInstance(MyApplication.getContext()).createMaterialImage(MyApplication.getContext(), Constants.TAG_CREATE_MATERIAL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

    }

    private void createEditMaterial(VipActiveBean vipActiveBean) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("activityId", vipActiveBean.getActivityId());
            map.put("activityName", vipActiveBean.getActiveName());
            map.put("beginDate", DateUtil.formatTime(vipActiveBean.getStartTime()));
            map.put("endDate", DateUtil.formatTime(vipActiveBean.getEndTime()));
            map.put("activityType", vipActiveBean.getActiveItemType() + "");
            map.put("activityScene", vipActiveBean.getActiveType());
            List<ActiveRuleBean> ruleList = new ArrayList<>();
            List<VipActiveBean.ActiveItemBean> activeItem = vipActiveBean.getActiveItem();
            if (activeItem != null && activeItem.size() > 0) {
                for (int i = 0; i < activeItem.size(); i++) {
                    ActiveRuleBean itemBean = new ActiveRuleBean();
                    itemBean.setConditionAmount(new BigDecimal(activeItem.get(i).getMoney()).multiply(new BigDecimal(100)));
                    itemBean.setDiscount(new BigDecimal(activeItem.get(i).getSale()).multiply(new BigDecimal(100)));
                    ruleList.add(itemBean);
                }
            }
            map.put("ruleList", ruleList.toArray());
            ServerClient.newInstance(MyApplication.getContext()).createMaterialImage(MyApplication.getContext(), Constants.TAG_CREATE_MATERIAL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

    }

    private void DownLoadImages(String url1, String url2) {
        mFiles.clear();
        String absolutePath = "";
        try {
            absolutePath = AppHelper.getCacheDir();
        } catch (Exception e) {
            e.printStackTrace();
            MyToast.showToastShort("SD卡不存在");
            return;
        }
        if (TextUtils.isEmpty(url1) || TextUtils.isEmpty(url2)) {
            MyToast.showToastShort("图片地址有误！");
            dismissLoading();
            return;
        }
        if (!url1.contains("/") || !url2.contains("/")) {
            MyToast.showToastShort("图片地址有误！");
            dismissLoading();
            return;
        }
        String fileName1 = url1.substring(url1.lastIndexOf("/"));
        String fileName2 = url2.substring(url2.lastIndexOf("/"));
        File file1 = new File(absolutePath, fileName1);
        File file2 = new File(absolutePath, fileName2);
        if (file1.exists() && file2.exists()) {
            if (AppHelper.getApkType()==0) {
                mFiles.add(file1);
                mFiles.add(file2);
                clickSendIcon(mFiles);
            }else {
                showCommonNoticeDialog(MaterialActivity.this,"图片下载到本地");
            }
        } else {
            if (!file1.exists()) {
                showNewLoading(true, getString(R.string.public_loading));
                OkHttpUtils.post().url(Constants.H5_BASE_URL + url1).build().
                        connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new FileCallBack(absolutePath, fileName1) {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        MyToast.showToastShort(getString(R.string.toast_download_image_failed));
                        dismissLoading();
                        request1 = false;
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        LogUtil.d("files==length1==" + response.length());
                        saveImage(response.getAbsolutePath());
                        mFiles.add(response);
                        request1 = true;
                        if (request1 && request2) {
                            if (AppHelper.getApkType()==0) {
                                clickSendIcon(mFiles);
                            }else {
                                showCommonNoticeDialog(MaterialActivity.this,"图片下载到本地");
                            }
                            dismissLoading();
                        }
                    }
                });
            } else {
                request1 = true;
            }
            if (!file2.exists()) {
                showNewLoading(true, getString(R.string.public_loading));
                OkHttpUtils.post().url(Constants.H5_BASE_URL + url2).build().
                        connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new FileCallBack(absolutePath, fileName2) {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        MyToast.showToastShort(getString(R.string.toast_download_image_failed));
                        dismissLoading();
                        request2 = false;
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        LogUtil.d("files==length2==" + response.length());
                        saveImage(response.getAbsolutePath());
                        mFiles.add(response);
                        request2 = true;
                        if (request1 && request2) {
                            if(AppHelper.getApkType()==0) {
                                clickSendIcon(mFiles);
                            }else {
                                showCommonNoticeDialog(MaterialActivity.this,"图片下载到本地");
                            }
                            dismissLoading();
                        }
                    }
                });
            } else {
                request2 = true;
            }
        }
    }


    private Uri saveImage(String imagePath) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATA, imagePath);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        Uri uri = MyApplication.getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        //MyApplication.getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, /*Uri.parse("file://" + path_export)*/uri));//path_export是你导出的文件路径
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(imagePath));
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
        return uri;
    }


    private File uri2File(Uri uri) {
        String img_path;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor actualimagecursor = managedQuery(uri, proj, null,
                null, null);
        if (actualimagecursor == null) {
            img_path = uri.getPath();
        } else {
            int actual_image_column_index = actualimagecursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            actualimagecursor.moveToFirst();
            img_path = actualimagecursor
                    .getString(actual_image_column_index);
        }
        File file = new File(img_path);
        return file;
    }


    public static Uri getImageContentUri(Context context, java.io.File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID}, MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }

    }
}
