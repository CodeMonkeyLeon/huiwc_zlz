package com.hstypay.enterprise.activity.vipActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.adapter.VipActiveAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.VipActiveBean;
import com.hstypay.enterprise.bean.VipActiveInfoBean;
import com.hstypay.enterprise.bean.VipActiveItem;
import com.hstypay.enterprise.bean.VipActiveListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OperationUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 门店单选列表
 */
public class VipActiveActivity extends BaseActivity implements View.OnClickListener, VipActiveAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack,mIvButton;
    private Button mBtnAddActive;
    private TextView mButton, mTvTitle;
    private RecyclerView mRvActive;
    private RelativeLayout mRlActiveEmpty;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int offset;
    private int limit = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<VipActiveListBean.DataEntity.DataListEntity> mList = new ArrayList<>();
    private VipActiveAdapter mAdapter;
    private String intentName;
    private int storeDateType;
    private int clickPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_active);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvButton = (ImageView) findViewById(R.id.iv_button);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_vip_active);
        mButton.setVisibility(View.INVISIBLE);


        mBtnAddActive = (Button) findViewById(R.id.btn_add_active);
        mRlActiveEmpty = (RelativeLayout) findViewById(R.id.rl_active_empty);

        if (MyApplication.getIsMerchant()){
            mIvButton.setVisibility(View.VISIBLE);
            mBtnAddActive.setVisibility(View.VISIBLE);
        }else {
            mIvButton.setVisibility(View.INVISIBLE);
            mBtnAddActive.setVisibility(View.INVISIBLE);
        }
        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvActive = (RecyclerView) findViewById(R.id.active_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvActive.setLayoutManager(mLinearLayoutManager);
        mRvActive.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    offset = 0;
                    loadData(limit,offset);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    loadData(limit , offset);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvButton.setOnClickListener(this);
        mBtnAddActive.setOnClickListener(this);
    }

    public void initData() {
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        String storeId = getIntent().getStringExtra(Constants.INTENT_STORE_ID);
        storeDateType = getIntent().getIntExtra(Constants.INTENT_STORE_DATA_TYPE, 0);
        mList = new ArrayList<>();
        mAdapter = new VipActiveAdapter(VipActiveActivity.this, mList, storeId);
        mAdapter.setOnItemClickListener(this);
        mRvActive.setAdapter(mAdapter);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            loadData(limit,offset);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadData(int limit, int offset) {
        //门店网络请求
        Map<String, Object> map = new HashMap<>();
        map.put("limit", limit);
        map.put("offset", offset);
        if (storeDateType == 1) {
            map.put("merchantDataType", storeDateType);
        }
        ServerClient.newInstance(MyApplication.getContext()).vipActiveList(MyApplication.getContext(), Constants.TAG_VIP_ACTIVE_LIST, map);
    }

    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VIP_ACTIVE_LIST)) {
            VipActiveListBean msg = (VipActiveListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipActiveActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 200);
                    if (msg.getData() != null && msg.getData().getDataList() != null && msg.getData().getDataList().size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mRlActiveEmpty.setVisibility(View.GONE);
                        mList.addAll(msg.getData().getDataList());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mRlActiveEmpty.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mRlActiveEmpty.setVisibility(View.VISIBLE);
                        }
                    }
                    offset = mList.size();
                    break;
            }
            dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_VIP_ACTIVE_DETAIL)) {
            VipActiveInfoBean msg = (VipActiveInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipActiveActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (MyApplication.getIsMerchant()) {
                            Intent intent = new Intent(VipActiveActivity.this, VipActiveDetailActivity.class);
                            intent.putExtra(Constants.INTENT_ACTIVE_INFO, msg.getData());
                            intent.putExtra(Constants.INTENT_ACTIVE_DETAIL, clickPosition);
                            startActivityForResult(intent, Constants.REQUEST_ACTIVE_DETAIL);
                        }else {
                            Intent intent = new Intent(VipActiveActivity.this, VipActivePreviewActivity.class);
                            intent.putExtra(Constants.INTENT_ACTIVE_DATA, setPreviewInfo(msg.getData()));
                            intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_ACTIVE_LIST);
                            startActivity(intent);
                        }
                    } else {
                        MyToast.showToastShort(getString(R.string.data_error));
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_VIP_ACTIVE_CREATE_ENABLE)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipActiveActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg!=null && msg.getData()!=null) {
                        boolean data = (boolean) msg.getData();
                        if (data){
                            showDialog();
                        }else {
                            startActivity(new Intent(VipActiveActivity.this, VipActiveTypeActivity.class));
                        }
                    }else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_button:
            case R.id.btn_add_active:
                MtaUtils.mtaId(VipActiveActivity.this,"G002");
                if (createEnable(mList)){
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        showNewLoading(true, getString(R.string.public_loading));
                        Map<String,Object> map = new HashMap<>();
                        map.put("states","2,3,5");
                        ServerClient.newInstance(MyApplication.getContext()).vipActiveCreateEnable(MyApplication.getContext(), Constants.TAG_VIP_ACTIVE_CREATE_ENABLE, map);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }else {
                    MtaUtils.mtaId(VipActiveActivity.this,"G011");
                    showDialog();
                }
                //startActivity(new Intent(VipActiveActivity.this, VipActiveTypeActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemDownloadClick(int position) {
        MtaUtils.mtaId(VipActiveActivity.this,"G009");
        Intent intent = new Intent(VipActiveActivity.this, MaterialActivity.class);
        intent.putExtra(Constants.INTENT_MATERIAL_ACTIVE_INFO, mList.get(position));
        startActivity(intent);
    }

    @Override
    public void onItemDetailClick(int position) {
        MtaUtils.mtaId(VipActiveActivity.this,"G010");
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            clickPosition = position;
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("activeId", mList.get(position).getActivity_id());
            ServerClient.newInstance(MyApplication.getContext()).vipActiveInfo(MyApplication.getContext(), Constants.TAG_VIP_ACTIVE_DETAIL, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_ACTIVE_DETAIL) {
            Bundle extras = data.getExtras();
            VipActiveInfoBean.DataEntity infoBean = (VipActiveInfoBean.DataEntity) extras.getSerializable(Constants.RESULT_ACTIVE_DETAIL);
            mList.get(clickPosition).setState(infoBean.getState());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            offset = 0;
            mList.clear();
            loadData(limit,offset);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private VipActiveBean setPreviewInfo(VipActiveInfoBean.DataEntity dataEntity){
        VipActiveBean activeBean = new VipActiveBean();
        activeBean.setActiveType(dataEntity.getSceneType());
        activeBean.setActiveItemType(dataEntity.getActivityType());
        activeBean.setActiveName(dataEntity.getActivityName());
        activeBean.setStartTime(dataEntity.getBeginDate());
        activeBean.setEndTime(dataEntity.getEndDate());
        activeBean.setActiveRemark(dataEntity.getInstructions());

        List<VipActiveBean.ActiveItemBean> activeItem = new ArrayList<>();
        Gson gson = new Gson();
        VipActiveItem[] vipActiveItems = gson.fromJson(dataEntity.getRule(), VipActiveItem[].class);
        if (vipActiveItems!=null && vipActiveItems.length>0) {
            for (int i = 0; i < vipActiveItems.length; i++) {
                VipActiveBean.ActiveItemBean itemBean = new VipActiveBean().new ActiveItemBean();
                itemBean.setMoney(OperationUtil.div(vipActiveItems[i].getConditionAmount(),100));
                if (dataEntity.getActivityType() ==3){
                    itemBean.setSale(vipActiveItems[i].getDiscountVar().toString());
                }else {
                    itemBean.setSale(OperationUtil.div(vipActiveItems[i].getDiscountVar(), 100));
                }
                activeItem.add(itemBean);
            }
        }
        activeBean.setActiveItem(activeItem);
        return activeBean;
    }

    private boolean createEnable(List<VipActiveListBean.DataEntity.DataListEntity> list){
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getState() == 2 || list.get(i).getState() == 3 || list.get(i).getState() == 5){
                return false;
            }
        }
        return true;
    }

    private void showDialog() {
        NoticeDialog noticeDialog = new NoticeDialog(VipActiveActivity.this
                , getString(R.string.dialog_active_unable), "", R.layout.notice_dialog_common);
        noticeDialog.show();
    }
}
