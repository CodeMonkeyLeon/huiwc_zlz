package com.hstypay.enterprise.activity.vipActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.adapter.ActiveItemAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.VipActiveBean;
import com.hstypay.enterprise.bean.VipActiveInfoBean;
import com.hstypay.enterprise.bean.VipActiveItem;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OperationUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: 添加收银员页面
 */
public class VipActiveDetailActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private ImageView mIvBack;
    private Button mBtnLeft, mBtnRight;
    private TextView mButton, mTvTitle, mTvActiveStatus, mTvActiveName, mTvActiveDate, mTvActiveItemType, mTvActiveRemark;
    private TextView mTvDataMoney, mTvDataCount, mTvActivePeople, mTvActiveCount, mTvDataMoneyTitle, mTvDataCountTitle;
    private RecyclerView mRecyclerView;
    private LinearLayout mLlActiveRemark, mLlActiveData, mLlActiveInfo, mLlBottom;
    private RadioGroup mRgActiveDetailType;

    private VipActiveInfoBean.DataEntity mInfoBean;
    private static final int ACTIVE_NOT_START = 2;//1-初始 2-未开始 3-进行中 4-已完成 5-已暂停 6-已关闭
    private static final int ACTIVE_PROCESS = 3;
    private static final int ACTIVE_OVER = 4;
    private static final int ACTIVE_PAUSE = 5;
    private static final int ACTIVE_CLOSE = 6;
    private int tempState;
    private int mClickPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_active_detail);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_vip_active_detail);
        mButton.setVisibility(View.INVISIBLE);

        mLlBottom = (LinearLayout) findViewById(R.id.ll_bottom);
        mBtnLeft = (Button) findViewById(R.id.btn_active_left);
        mBtnRight = (Button) findViewById(R.id.btn_active_right);

        mTvActiveStatus = (TextView) findViewById(R.id.tv_active_status);
        mRgActiveDetailType = (RadioGroup) findViewById(R.id.rg_active_detail_type);

        mTvActiveName = (TextView) findViewById(R.id.tv_active_name);
        mTvActiveDate = (TextView) findViewById(R.id.tv_active_date);
        mTvActiveItemType = (TextView) findViewById(R.id.tv_active_item_type);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mLlActiveRemark = (LinearLayout) findViewById(R.id.ll_active_remark);
        mTvActiveRemark = (TextView) findViewById(R.id.tv_active_remark);

        mTvDataMoney = (TextView) findViewById(R.id.tv_active_data_money);
        mTvDataMoneyTitle = (TextView) findViewById(R.id.tv_data_money_title);
        mTvDataCount = (TextView) findViewById(R.id.tv_active_data_count);
        mTvDataCountTitle = (TextView) findViewById(R.id.tv_data_count_title);
        mTvActivePeople = (TextView) findViewById(R.id.tv_active_people);
        mTvActiveCount = (TextView) findViewById(R.id.tv_active_count);

        mLlActiveData = (LinearLayout) findViewById(R.id.ll_active_data);
        mLlActiveInfo = (LinearLayout) findViewById(R.id.ll_active_info);
    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnLeft.setOnClickListener(this);
        mBtnRight.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mRgActiveDetailType.setOnCheckedChangeListener(this);
    }

    public void initData() {
        mInfoBean = (VipActiveInfoBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_ACTIVE_INFO);
        mClickPosition = getIntent().getIntExtra(Constants.INTENT_ACTIVE_DETAIL, 0);
        setButton(mInfoBean.getState());

        mTvActivePeople.setText(mInfoBean.getParticipationUserNum() + "");
        mTvActiveCount.setText(mInfoBean.getParticipationNum() + "");
        if (mInfoBean.getPayAmountTotal() != null) {
            mTvDataMoney.setText(DateUtil.formatMoneyUtil(mInfoBean.getPayAmountTotal().divide(BigDecimal.valueOf(100d)).doubleValue()));
        }
        mTvDataCount.setText(mInfoBean.getParticipationNum() + "");

        mTvActiveName.setText(mInfoBean.getActivityName());
        mTvActiveDate.setText(DateUtil.formatYYMD(mInfoBean.getBeginDate()) + " 至 " + DateUtil.formatYYMD(mInfoBean
                .getEndDate() - 1000));
        switch (mInfoBean.getActivityType()) {//活动类型 1.满赠 2.满减 3.折扣
            case 1:
                mTvActiveItemType.setText(getString(R.string.tv_type_active_gift));
                break;
            case 2:
                mTvActiveItemType.setText(getString(R.string.tv_type_active_reduce));
                break;
            case 3:
                mTvActiveItemType.setText(getString(R.string.tv_type_active_discount));
                break;
        }
        if (!TextUtils.isEmpty(mInfoBean.getInstructions())) {
            mTvActiveRemark.setText(mInfoBean.getInstructions());
        } else {
            mLlActiveRemark.setVisibility(View.GONE);
        }
        if (mInfoBean.getSceneType() == 2) {
            mTvDataMoneyTitle.setText(getString(R.string.tx_vip_recharge_money));
            mTvDataCountTitle.setText(getString(R.string.tx_vip_recharge_count));
        } else if (mInfoBean.getSceneType() == 3) {
            mTvDataMoneyTitle.setText(getString(R.string.tx_vip_consume_money));
            mTvDataCountTitle.setText(getString(R.string.tx_vip_consume_count));
        }
        LinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ActiveItemAdapter adapter = new ActiveItemAdapter(MyApplication.getContext(), setInfo(mInfoBean));
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                Intent intent = new Intent();
                Bundle mBundle = new Bundle();
                mBundle.putSerializable(Constants.RESULT_ACTIVE_DETAIL, mInfoBean);
                intent.putExtras(mBundle);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.btn_active_left:
                SelectDialog leftDialog = new SelectDialog(VipActiveDetailActivity.this, getString(R.string.dialog_active_over), R.layout.select_dialog);
                leftDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                    @Override
                    public void clickOk() {
                        editActive(mInfoBean.getActivityId(), 3);
                        tempState = ACTIVE_CLOSE;
                    }
                });
                leftDialog.show();
                break;
            case R.id.btn_active_right:
                if (mInfoBean != null) {
                    if (ACTIVE_NOT_START == mInfoBean.getState()) {
                        Intent intentEdit = new Intent(VipActiveDetailActivity.this, EditVipActiveActivity.class);
                        intentEdit.putExtra(Constants.INTENT_ACTIVE_INFO, mInfoBean);
                        startActivity(intentEdit);
                    } else {
                        String dialogText = "";
                        if (mInfoBean.getState() == ACTIVE_PAUSE) {
                            dialogText = getString(R.string.dialog_active_recover);
                        } else if (mInfoBean.getState() == ACTIVE_PROCESS) {
                            dialogText = getString(R.string.dialog_active_pause);
                        }
                        SelectDialog rightDialog = new SelectDialog(VipActiveDetailActivity.this, dialogText, R.layout.select_dialog);
                        rightDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                            @Override
                            public void clickOk() {
                                if (mInfoBean.getState() == ACTIVE_PAUSE) {
                                    editActive(mInfoBean.getActivityId(), 2);
                                    tempState = ACTIVE_PROCESS;
                                } else if (mInfoBean.getState() == ACTIVE_PROCESS) {
                                    editActive(mInfoBean.getActivityId(), 1);
                                    tempState = ACTIVE_PAUSE;
                                }
                            }
                        });
                        rightDialog.show();
                    }
                } else {
                    showCommonNoticeDialog(VipActiveDetailActivity.this, getString(R.string.data_error));
                }
                break;
           /* case R.id.button_title:
                if (mInfoBean!=null) {
                    Intent intentEdit = new Intent(VipActiveDetailActivity.this, EditVipActiveActivity.class);
                    intentEdit.putExtra(Constants.INTENT_ACTIVE_INFO, mInfoBean);
                    startActivity(intentEdit);
                }else {
                    showCommonNoticeDialog(VipActiveDetailActivity.this,getString(R.string.data_error));
                }
                break;*/
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_active_data:
                mLlActiveData.setVisibility(View.VISIBLE);
                mLlActiveInfo.setVisibility(View.GONE);
                break;
            case R.id.rb_active_info:
                mLlActiveData.setVisibility(View.GONE);
                mLlActiveInfo.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setButton(int activeStatus) {
        switch (activeStatus) {
            case ACTIVE_NOT_START:
                mTvActiveStatus.setText(getString(R.string.tv_active_not_start));
                mTvActiveStatus.setBackgroundResource(R.color.tv_active_status_bg);
                mBtnLeft.setVisibility(View.GONE);
                mBtnRight.setText(getString(R.string.tv_active_edit));
                break;
            case ACTIVE_PROCESS:
                mTvActiveStatus.setText(getString(R.string.tv_active_process));
                mTvActiveStatus.setBackgroundResource(R.color.tv_active_status_bg);
                mBtnLeft.setVisibility(View.GONE);
                mBtnRight.setText(getString(R.string.btn_active_pause));
                break;
            case ACTIVE_PAUSE:
                mTvActiveStatus.setText(getString(R.string.tv_active_pause));
                mTvActiveStatus.setBackgroundResource(R.color.tv_active_status_bg);
                mBtnRight.setText(getString(R.string.btn_active_recover));
                mBtnLeft.setVisibility(View.VISIBLE);
                break;
            case ACTIVE_OVER:
                mTvActiveStatus.setText(getString(R.string.tv_active_over));
                mTvActiveStatus.setBackgroundResource(R.color.tv_active_title);
                mLlBottom.setVisibility(View.GONE);
                break;
            case ACTIVE_CLOSE:
                mTvActiveStatus.setText(getString(R.string.tv_active_close));
                mTvActiveStatus.setBackgroundResource(R.color.tv_active_title);
                mLlBottom.setVisibility(View.GONE);
                break;
        }
    }

    private void editActive(Number activeId, int state) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
            map.put("activeId", activeId);
            map.put("state", state);//活动状态：1- 暂停2- 恢复3- 关闭
            ServerClient.newInstance(MyApplication.getContext()).vipActiveStatusEdit(MyApplication.getContext(), Constants.TAG_VIP_ACTIVE_STATUS_EDIT, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VIP_ACTIVE_STATUS_EDIT)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipActiveDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    switch (tempState) {
                        case ACTIVE_CLOSE:
                            mTvActiveStatus.setText(getString(R.string.tv_active_close));
                            mInfoBean.setState(ACTIVE_CLOSE);
                            break;
                        case ACTIVE_PAUSE:
                            mTvActiveStatus.setText(getString(R.string.tv_active_pause));
                            mInfoBean.setState(ACTIVE_PAUSE);
                            break;
                        case ACTIVE_PROCESS:
                            mTvActiveStatus.setText(getString(R.string.tv_active_process));
                            mInfoBean.setState(ACTIVE_PROCESS);
                            break;
                    }
                    setButton(mInfoBean.getState());
                    break;
            }
            dismissLoading();

        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(Constants.RESULT_ACTIVE_DETAIL, mInfoBean);
        intent.putExtras(mBundle);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    private VipActiveBean setInfo(VipActiveInfoBean.DataEntity dataEntity) {
        VipActiveBean activeBean = new VipActiveBean();
        activeBean.setActiveType(dataEntity.getSceneType());
        activeBean.setActiveItemType(dataEntity.getActivityType());
        activeBean.setActiveName(dataEntity.getActivityName());
        activeBean.setStartTime(dataEntity.getBeginDate());
        activeBean.setEndTime(dataEntity.getEndDate());
        activeBean.setActiveRemark(dataEntity.getInstructions());

        List<VipActiveBean.ActiveItemBean> activeItem = new ArrayList<>();
        Gson gson = new Gson();
        VipActiveItem[] vipActiveItems = gson.fromJson(dataEntity.getRule(), VipActiveItem[].class);
        if (vipActiveItems != null && vipActiveItems.length > 0) {
            for (int i = 0; i < vipActiveItems.length; i++) {
                VipActiveBean.ActiveItemBean itemBean = new VipActiveBean().new ActiveItemBean();
                itemBean.setMoney(OperationUtil.div(vipActiveItems[i].getConditionAmount(), 100));
                if (dataEntity.getActivityType() == 3) {
                    itemBean.setSale(vipActiveItems[i].getDiscountVar() + "");
                } else {
                    itemBean.setSale(OperationUtil.div(vipActiveItems[i].getDiscountVar(), 100));
                }
                activeItem.add(itemBean);
            }
        }
        activeBean.setActiveItem(activeItem);
        return activeBean;
    }
}
