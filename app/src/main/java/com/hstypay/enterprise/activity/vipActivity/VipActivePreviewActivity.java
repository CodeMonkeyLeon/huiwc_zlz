package com.hstypay.enterprise.activity.vipActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.adapter.ActiveItemAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CommonBean;
import com.hstypay.enterprise.bean.VipActiveBean;
import com.hstypay.enterprise.bean.VipActiveItem;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: 添加收银员页面
 */
public class VipActivePreviewActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnLeft, mBtnRight;
    private TextView mButton, mTvTitle, mTvActiveName, mTvActiveDate, mTvActiveItemType, mTvActiveRemark;
    private RecyclerView mRecyclerView;
    private LinearLayout mLlActiveRemark;
    private VipActiveBean mVipActiveBean;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private LinearLayout mLlBottom;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_active_preview);
        StatusBarUtil.setImmersiveStatusBar(this,true);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.title_vip_active_preview);
        mButton.setVisibility(View.INVISIBLE);

        mBtnLeft = (Button) findViewById(R.id.btn_active_left);
        mBtnRight = (Button) findViewById(R.id.btn_active_right);

        mTvActiveName = (TextView) findViewById(R.id.tv_active_name);
        mTvActiveDate = (TextView) findViewById(R.id.tv_active_date);
        mTvActiveItemType = (TextView) findViewById(R.id.tv_active_item_type);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mLlActiveRemark = (LinearLayout) findViewById(R.id.ll_active_remark);
        mTvActiveRemark = (TextView) findViewById(R.id.tv_active_remark);
        mLlBottom = (LinearLayout) findViewById(R.id.ll_bottom);
    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnLeft.setOnClickListener(this);
        mBtnRight.setOnClickListener(this);
    }

    public void initData() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mVipActiveBean = (VipActiveBean) getIntent().getSerializableExtra(Constants.INTENT_ACTIVE_DATA);
        if (Constants.INTENT_ADD_VIP_ACTIVE.equals(mIntentName) || Constants.INTENT_EDIT_VIP_ACTIVE.equals(mIntentName)) {
            mLlBottom.setVisibility(View.VISIBLE);
        } else if (Constants.INTENT_VIP_ACTIVE_LIST.equals(mIntentName)) {
            mLlBottom.setVisibility(View.GONE);
        }
        if (mVipActiveBean != null) {
            mTvActiveName.setText(mVipActiveBean.getActiveName());
            mTvActiveDate.setText(DateUtil.formatYYMD(mVipActiveBean.getStartTime()) + " 至 " + DateUtil.formatYYMD(mVipActiveBean.getEndTime()-1000));
            switch (mVipActiveBean.getActiveItemType()) {
                case 1:
                    mTvActiveItemType.setText(getString(R.string.tv_type_active_gift));
                    break;
                case 2:
                    mTvActiveItemType.setText(getString(R.string.tv_type_active_reduce));
                    break;
                case 3:
                    mTvActiveItemType.setText(getString(R.string.tv_type_active_discount));
                    break;
            }
            mLinearLayoutManager = new CustomLinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLinearLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            ActiveItemAdapter adapter = new ActiveItemAdapter(MyApplication.getContext(), mVipActiveBean);
            mRecyclerView.setAdapter(adapter);

            if (TextUtils.isEmpty(mVipActiveBean.getActiveRemark())) {
                mLlActiveRemark.setVisibility(View.GONE);
            } else {
                mLlActiveRemark.setVisibility(View.VISIBLE);
                mTvActiveRemark.setText(mVipActiveBean.getActiveRemark());
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_active_left:
                finish();
                break;
            case R.id.btn_active_right:
                MtaUtils.mtaId(VipActivePreviewActivity.this,"G005");
                submit();
                break;
            default:
                break;
        }
    }

    private void showDownLoadDialog() {
        MtaUtils.mtaId(VipActivePreviewActivity.this,"G007");
        SelectDialog selectDialog = new SelectDialog(VipActivePreviewActivity.this
                , getString(R.string.dialog_active_download), R.layout.select_dialog);
        selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                MtaUtils.mtaId(VipActivePreviewActivity.this,"G008");
                Intent intent = new Intent(VipActivePreviewActivity.this, MaterialActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_ACTIVE_EDIT);
                intent.putExtra(Constants.INTENT_EDIT_ACTIVE_INFO, mVipActiveBean);
                startActivity(intent);
            }
        });
        selectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                Intent intent = new Intent(VipActivePreviewActivity.this, VipActiveActivity.class);
                startActivity(intent);
            }
        });
        selectDialog.show();
    }

    private void submit() {
        Gson gson = new Gson();
        Map<String, Object> map = new HashMap<>();
        map.put("name", mVipActiveBean.getActiveName());
        map.put("beginDate", mVipActiveBean.getStartTime());
        map.put("endDate", mVipActiveBean.getEndTime());
        map.put("sceneType", mVipActiveBean.getActiveType());
        map.put("activityType", mVipActiveBean.getActiveItemType());
        map.put("instructions", mVipActiveBean.getActiveRemark());
        List<VipActiveItem> vipActiveItems = new ArrayList<>();
        StringBuilder rule = new StringBuilder();
        for (int i = 0; i < mVipActiveBean.getActiveItem().size(); i++) {
            VipActiveItem vipActiveItem = new VipActiveItem();
            if (mVipActiveBean.getActiveItemType() == 3) {
                BigDecimal conditionAmount = new BigDecimal(new BigDecimal(mVipActiveBean.getActiveItem().get(i).getMoney()).multiply(new BigDecimal(100)).longValue());
                vipActiveItem.setConditionAmount(conditionAmount);
                vipActiveItem.setDiscountVar(new BigDecimal(mVipActiveBean.getActiveItem().get(i).getSale()).setScale(1, BigDecimal.ROUND_DOWN));
                if (mVipActiveBean.getActiveType() == 2) {
                    rule.append(getString(R.string.tv_active_text_recharge_fill))
                            .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getMoney(), 0))
                            .append(getString(R.string.tv_active_text_symbol))
                            .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getSale(), 0))
                            .append(getString(R.string.tv_active_text_discount)).append(";");
                } else if (mVipActiveBean.getActiveType() == 3) {
                    rule.append(getString(R.string.tv_active_text_consume_fill))
                            .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getMoney(), 0))
                            .append(getString(R.string.tv_active_text_symbol))
                            .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getSale(), 0))
                            .append(getString(R.string.tv_active_text_discount)).append(";");
                }
            } else {
                BigDecimal conditionAmount = new BigDecimal(new BigDecimal(mVipActiveBean.getActiveItem().get(i).getMoney()).multiply(new BigDecimal(100)).longValue());
                BigDecimal discountVar = new BigDecimal(new BigDecimal(mVipActiveBean.getActiveItem().get(i).getSale()).multiply(new BigDecimal(100)).longValue());
                vipActiveItem.setConditionAmount(conditionAmount);
                vipActiveItem.setDiscountVar(discountVar);
                if (mVipActiveBean.getActiveItemType() == 1) {
                    if (mVipActiveBean.getActiveType() == 2) {
                        rule.append(getString(R.string.tv_active_text_recharge))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getMoney(), 0))
                                .append(getString(R.string.tv_active_text_symbol))
                                .append(getString(R.string.tv_active_text_gift))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getSale(), 0))
                                .append(getString(R.string.tv_active_text_yuan)).append(";");
                    } else if (mVipActiveBean.getActiveType() == 3) {
                        rule.append(getString(R.string.tv_active_text_consume))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getMoney(), 0))
                                .append(getString(R.string.tv_active_text_symbol))
                                .append(getString(R.string.tv_active_text_gift))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getSale(), 0))
                                .append(getString(R.string.tv_active_text_yuan)).append(";");
                    }
                } else if (mVipActiveBean.getActiveItemType() == 2) {
                    if (mVipActiveBean.getActiveType() == 2) {
                        rule.append(getString(R.string.tv_active_text_recharge))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getMoney(), 0))
                                .append(getString(R.string.tv_active_text_symbol))
                                .append(getString(R.string.tv_active_text_reduce))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getSale(), 0))
                                .append(getString(R.string.tv_active_text_yuan)).append(";");
                    } else if (mVipActiveBean.getActiveType() == 3) {
                        rule.append(getString(R.string.tv_active_text_consume_fill))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getMoney(), 0))
                                .append(getString(R.string.tv_active_text_symbol))
                                .append(getString(R.string.tv_active_text_reduce))
                                .append(Utils.Double.tryParse(mVipActiveBean.getActiveItem().get(i).getSale(), 0))
                                .append(getString(R.string.tv_active_text_yuan)).append(";");
                    }
                }
            }
            vipActiveItems.add(vipActiveItem);
        }
        map.put("ruleStr", rule.toString());
        map.put("rule", gson.toJson(vipActiveItems));
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            showNewLoading(true, getString(R.string.public_loading));
            if (Constants.INTENT_EDIT_VIP_ACTIVE.equals(mIntentName)) {
                map.put("activityId", mVipActiveBean.getActivityId());
                ServerClient.newInstance(MyApplication.getContext()).vipActiveEdit(MyApplication.getContext(), Constants.TAG_VIP_ACTIVE_EDIT, map);
            } else {
                ServerClient.newInstance(MyApplication.getContext()).vipActiveCreate(MyApplication.getContext(), Constants.TAG_VIP_ACTIVE_CREATE, map);
            }
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VIP_ACTIVE_CREATE) || event.getTag().equals(Constants.TAG_VIP_ACTIVE_EDIT)) {
            CommonBean msg = (CommonBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipActivePreviewActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        Number activityId = msg.getData();
                        mVipActiveBean.setActivityId(activityId);
                        if (Constants.INTENT_EDIT_VIP_ACTIVE.equals(mIntentName)) {
                            showDialog(getString(R.string.edit_info_success));
                        }else {
                            showDialog(getString(R.string.dialog_active_success));
                        }
                    } else {
                        MyToast.showToastShort(getString(R.string.data_error));
                    }
                    break;
            }
            dismissLoading();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
