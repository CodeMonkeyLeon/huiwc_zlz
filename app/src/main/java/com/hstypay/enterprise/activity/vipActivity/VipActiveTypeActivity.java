package com.hstypay.enterprise.activity.vipActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.MyPagerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DensityUtils;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.MtaUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: 添加收银员页面
 */
public class VipActiveTypeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnRecharge,mBtnConsume;
    private List<View> list;
    private ViewPager mViewPager;
    private LinearLayout mViewPagerContainer,mPointContainer;
    private View mSelectedPoint;
    private RelativeLayout mRlPointContainer;
    private int mSpace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_active_type);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }
    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mPointContainer = (LinearLayout) findViewById(R.id.guide_point_container);
        mSelectedPoint = findViewById(R.id.guide_selected_point);
        mRlPointContainer = (RelativeLayout) findViewById(R.id.guide_point);
        //ViewPager的父布局

        mViewPagerContainer = (LinearLayout) findViewById(R.id.viewPager_layout);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setOffscreenPageLimit(2); // viewPager的缓存页数
        mViewPager.setPageMargin(DisplayUtil.dip2Px(MyApplication.getContext(),20)); // 设置各页面的间距
        // 将父节点Layout事件分发给viewpager，否则只能滑动中间的一个view对象
        mViewPagerContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mViewPager.dispatchTouchEvent(event);
            }
        });
        //设置ViewPager的滑动监听,必须在滑动的时候刷新界面才能看到效果
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //必须刷新页面,才能看到效果
                if (mViewPagerContainer != null) {
                    mViewPagerContainer.invalidate();
                }
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mSelectedPoint.getLayoutParams();
                //计算两点间的距离
                params.leftMargin = (int) (mSpace * position + mSpace * positionOffset + 0.5f);
                mSelectedPoint.setLayoutParams(params);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    public void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnRecharge.setOnClickListener(this);
        mBtnConsume.setOnClickListener(this);
    }

    public void initData() {
        list = new ArrayList<>();
        View viewRecharge = View.inflate(this,R.layout.layout_active_recharge,null);
        View viewConsume = View.inflate(this,R.layout.layout_active_consume,null);
        mBtnRecharge = (Button) viewRecharge.findViewById(R.id.btn_active_recharge);
        mBtnConsume = (Button) viewConsume.findViewById(R.id.btn_active_consume);
        list.add(viewRecharge);
        list.add(viewConsume);
        //设置适配器
        mViewPager.setAdapter(new MyPagerAdapter(list));

        for (int i = 0; i < 2; i++) {
            ImageView point = new ImageView(this);
            point.setBackgroundResource(R.drawable.active_type_dot_normal);
            point.setScaleType(ImageView.ScaleType.CENTER);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DensityUtils.dp2px(this, 6),
                    DensityUtils.dp2px(this, 6));
            if (i != 0) {
                params.leftMargin = DensityUtils.dp2px(this, 10);
            }
            mPointContainer.addView(point, params);
        }

        //监听页面布局改变
        mPointContainer.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mSpace = mPointContainer.getChildAt(1).getLeft() - mPointContainer.getChildAt(0).getLeft();
                        mPointContainer.getViewTreeObserver()
                                .removeGlobalOnLayoutListener(this);
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_active_recharge:
                MtaUtils.mtaId(VipActiveTypeActivity.this,"G003");
                Intent intentRecharge = new Intent(VipActiveTypeActivity.this,AddVipActiveActivity.class);
                intentRecharge.putExtra(Constants.INTENT_ACTIVE_TYPE,2);//会员卡充值
                startActivity(intentRecharge);
                break;
            case R.id.btn_active_consume:
                MtaUtils.mtaId(VipActiveTypeActivity.this,"G003");
                Intent intentConsume = new Intent(VipActiveTypeActivity.this,AddVipActiveActivity.class);
                intentConsume.putExtra(Constants.INTENT_ACTIVE_TYPE,3);//会员卡消费
                startActivity(intentConsume);
                break;
            default:
                break;
        }
    }
}
