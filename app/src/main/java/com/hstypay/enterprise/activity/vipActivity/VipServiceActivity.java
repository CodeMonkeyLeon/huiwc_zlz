package com.hstypay.enterprise.activity.vipActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.fragment.VipFragment;
import com.hstypay.enterprise.utils.StatusBarUtil;

/**
 * @author kuangzeyu
 * @time 2021/3/22
 * @desc 会员服务页，原来的会员首页VipFragment
 */
public class VipServiceActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_service);
        StatusBarUtil.setTranslucentStatus(this);

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = supportFragmentManager.beginTransaction();
        ft.replace(R.id.fl_conent_vip_service,new VipFragment());
        ft.commit();
    }
}
