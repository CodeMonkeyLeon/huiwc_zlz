package com.hstypay.enterprise.activity.vipCard;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DensityUtils;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ShareVipActivity extends BaseActivity implements View.OnClickListener {
    private String mCodeUrl;
    private Bitmap mQRCode;
    private ImageView mIvBack, mIvShareCode;
    private TextView mButton, mTvMerchantName;
    private LinearLayout mLlShareWecaht, mLlShareFrients, mLlShareWeibo, mLlShareQQ, mLlShareQQSpace;
    private RelativeLayout mRlShareCode, mRlShareIcon;
    private SafeDialog dialog;
    private UMWeb web;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_QQ = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_SPACE = 103;
    private Bitmap mViewBitmap;
    private CommonNoticeDialog mDialogInfo;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_vip);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mIvShareCode = (ImageView) findViewById(R.id.iv_share_qrcode);
        mTvMerchantName = (TextView) findViewById(R.id.tv_code_merchant_name);
        mLlShareWecaht = (LinearLayout) findViewById(R.id.ll_share_wechat);
        mLlShareFrients = (LinearLayout) findViewById(R.id.ll_share_friends);
        mLlShareWeibo = (LinearLayout) findViewById(R.id.ll_share_weibo);
        mLlShareQQ = (LinearLayout) findViewById(R.id.ll_share_qq);
        mLlShareQQSpace = (LinearLayout) findViewById(R.id.ll_share_qq_space);
        mRlShareCode = (RelativeLayout) findViewById(R.id.rl_share_code);
        mRlShareIcon = (RelativeLayout) findViewById(R.id.rl_share_icon);
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);

        if (AppHelper.getAppType() == 1 || AppHelper.getApkType() != 0) {
            mRlShareIcon.setVisibility(View.INVISIBLE);
            mButton.setVisibility(View.INVISIBLE);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mLlShareWecaht.setOnClickListener(this);
        mLlShareFrients.setOnClickListener(this);
        mLlShareWeibo.setOnClickListener(this);
        mLlShareQQ.setOnClickListener(this);
        mLlShareQQSpace.setOnClickListener(this);
    }

    public void initData() {
        mTvMerchantName.setText(MyApplication.getMerchantName());
//        mCodeUrl = getIntent().getStringExtra(Constants.INTENT_CODE_URL);
        if (!NetworkUtils.isNetworkAvailable(ShareVipActivity.this)) {
            showCommonNoticeDialog(ShareVipActivity.this, getString(R.string.network_exception));
        } else {
            showNewLoading(true, getString(R.string.public_loading));
            if (MyApplication.getIsMerchant() || MyApplication.getIsCasher()) {
                ServerClient.newInstance(ShareVipActivity.this).queryDrawMember(ShareVipActivity.this, Constants.TAG_MEMBER_URL, null);
            } else {
                String storeId = getIntent().getStringExtra(Constants.INTENT_VIP_STORE_ID);
                Map<String, Object> map = new HashMap<>();
                map.put("cardType", "MEMBER_CARD");
                map.put("storeID", storeId);
                ServerClient.newInstance(ShareVipActivity.this).queryStoreVipCode(ShareVipActivity.this, Constants.TAG_STORE_MEMBER_URL, map);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (!TextUtils.isEmpty(mCodeUrl)) {
            mViewBitmap = ScreenUtils.createViewBitmap(mRlShareCode);
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareVipActivity.this, permissionArray);
                    if (results) {
                        ScreenUtils.saveImageToGallery(this, mViewBitmap);
                        getDialogSuccess(getString(R.string.dialog_copy_picture));
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                    }
                }
                break;
            case R.id.ll_share_wechat:
                MtaUtils.mtaId(ShareVipActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    ShareImage(SHARE_MEDIA.WEIXIN, mViewBitmap, mViewBitmap);
                }
                break;
            case R.id.ll_share_friends:
                MtaUtils.mtaId(ShareVipActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    ShareImage(SHARE_MEDIA.WEIXIN_CIRCLE, mViewBitmap, mViewBitmap);
                }
                break;
            case R.id.ll_share_weibo:
                break;
            case R.id.ll_share_qq:
                MtaUtils.mtaId(ShareVipActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareVipActivity.this, permissionArray);
                    if (results) {
                        ShareImage(SHARE_MEDIA.QQ, mViewBitmap, mViewBitmap);
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_QQ, permissionArray, getString(R.string.permission_content_share));
                    }
                }
                break;
            case R.id.ll_share_qq_space:
                MtaUtils.mtaId(ShareVipActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareVipActivity.this, permissionArray);
                    if (results) {
                        ShareImage(SHARE_MEDIA.QZONE, mViewBitmap, mViewBitmap);
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_SPACE, permissionArray, getString(R.string.permission_content_share));
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ScreenUtils.saveImageToGallery(this, mViewBitmap);
                    getDialogSuccess(getString(R.string.dialog_copy_picture));
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_QQ:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ShareImage(SHARE_MEDIA.QQ, mViewBitmap, mViewBitmap);
                } else {
                    showDialog(getString(R.string.permission_set_content_share));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_SPACE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ShareImage(SHARE_MEDIA.QZONE, mViewBitmap, mViewBitmap);
                } else {
                    showDialog(getString(R.string.permission_set_content_share));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void ShareImage(SHARE_MEDIA platform, Bitmap image, Bitmap thumb) {
        UMImage pic = new UMImage(ShareVipActivity.this, image);
        pic.setThumb(new UMImage(ShareVipActivity.this, thumb));
        new ShareAction(ShareVipActivity.this).withMedia(pic).setPlatform(platform).setCallback(shareListener).withText("识别二维码领取会员卡").share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }

    public void getDialogSuccess(String title) {
        if (mDialogInfo == null) {
            mDialogInfo = new CommonNoticeDialog(ShareVipActivity.this, title, getString(R.string.dialog_notice_button));
            DialogHelper.resize(ShareVipActivity.this, mDialogInfo);
        }
        mDialogInfo.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_MEMBER_URL)) {
            dismissLoading();
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ShareVipActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ShareVipActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mCodeUrl = msg.getData().getMemerUrl();
                        if (msg.getData().getRetCode()) {
                            if (!TextUtils.isEmpty(mCodeUrl)) {
                                /*mRlShareIcon.setVisibility(View.VISIBLE);
                                mButton.setVisibility(View.VISIBLE);*/
                                try {
                                    mQRCode = MaxCardManager.getInstance().create2DCode(mCodeUrl,
                                            DensityUtils.dip2px(ShareVipActivity.this, 190),
                                            DensityUtils.dip2px(ShareVipActivity.this, 190));
                                } catch (WriterException e) {
                                    e.printStackTrace();
                                }
                                mIvShareCode.setImageBitmap(mQRCode);
                            } else {
                                mRlShareIcon.setVisibility(View.INVISIBLE);
                                mButton.setVisibility(View.INVISIBLE);
                                showCommonNoticeDialog(ShareVipActivity.this, getString(R.string.toast_not_open_member));
                            }
                        } else {
                            mRlShareIcon.setVisibility(View.INVISIBLE);
                            mButton.setVisibility(View.INVISIBLE);
                            if (!TextUtils.isEmpty(msg.getData().getMessage())) {
                                showCommonNoticeDialog(ShareVipActivity.this, msg.getData().getMessage());
                            }
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_STORE_MEMBER_URL)) {
            dismissLoading();
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(ShareVipActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(ShareVipActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mCodeUrl = (String) msg.getData();
                        if (!TextUtils.isEmpty(mCodeUrl)) {
                            try {
                                mQRCode = MaxCardManager.getInstance().create2DCode(mCodeUrl,
                                        DensityUtils.dip2px(ShareVipActivity.this, 190),
                                        DensityUtils.dip2px(ShareVipActivity.this, 190));
                            } catch (WriterException e) {
                                e.printStackTrace();
                            }
                            mIvShareCode.setImageBitmap(mQRCode);
                        } else {
                            mRlShareIcon.setVisibility(View.INVISIBLE);
                            mButton.setVisibility(View.INVISIBLE);
                            showCommonNoticeDialog(ShareVipActivity.this, getString(R.string.toast_not_open_member));
                        }
                    } else {
                        mRlShareIcon.setVisibility(View.INVISIBLE);
                        mButton.setVisibility(View.INVISIBLE);
                    }
                    break;
            }
        }
    }
}
