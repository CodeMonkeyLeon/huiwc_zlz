package com.hstypay.enterprise.activity.vipCard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.ShadowLayout;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


public class VipCardActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout mLlVerification, mLlRecharge, mLlCountVerification;
    private ImageView mIvBack;
    private RelativeLayout mRlVipCode;
    private TextView mTvTitle;
    private Button mButtonTitle;
    private String mStoreId;
    private ShadowLayout mSlVerification;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_card);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mRlVipCode = (RelativeLayout) findViewById(R.id.rl_vip_code);
        mLlVerification = (LinearLayout) findViewById(R.id.ll_vip_verification);
        mLlCountVerification = (LinearLayout) findViewById(R.id.ll_vip_count_verification);
        mLlRecharge = (LinearLayout) findViewById(R.id.ll_vip_recharge);

        mSlVerification = (ShadowLayout) findViewById(R.id.sl_vip_verification);

    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlVipCode.setOnClickListener(this);
        mLlVerification.setOnClickListener(this);
        mLlCountVerification.setOnClickListener(this);
        mLlRecharge.setOnClickListener(this);
        mTvTitle.setOnClickListener(this);
    }

    public void initData() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        String storeName = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME);
        if (!TextUtils.isEmpty(storeName)) {
            mTvTitle.setVisibility(View.VISIBLE);
            mTvTitle.setText(storeName);
        }
        mStoreId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
        if (TextUtils.isEmpty(mStoreId)) {
            showNewLoading(true, getString(R.string.public_loading));
            ServerClient.newInstance(VipCardActivity.this).storePort(VipCardActivity.this, Constants.STORE_VIP_PORT_TAG, null);
        }
        if (!NetworkUtils.isNetworkAvailable(VipCardActivity.this)) {
            showCommonNoticeDialog(VipCardActivity.this, getString(R.string.network_exception));
            return;
        } else {
            loadDialog(VipCardActivity.this, getString(R.string.public_loading));
            ServerClient.newInstance(VipCardActivity.this).vipPayWhite(VipCardActivity.this, Constants.TAG_VIP_PAY_WHITE, null);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.STORE_VIP_PORT_TAG)) {
            StoreBean bean = (StoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    getLoginDialog(VipCardActivity.this, bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    showCommonNoticeDialog(VipCardActivity.this, bean.getError().getMessage());
                                }

                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (bean.getData().getStoreName() != null) {
                                String storeName = bean.getData().getStoreName();
                                if (!TextUtils.isEmpty(storeName)) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                                    mTvTitle.setVisibility(View.VISIBLE);
                                    mTvTitle.setText(storeName);
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, "");
                                    mTvTitle.setVisibility(View.INVISIBLE);
                                }
                            }
                            if (!TextUtils.isEmpty(bean.getData().getStoreId())) {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, bean.getData().getStoreId());
                                mStoreId = bean.getData().getStoreId();
                            } else {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, true);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                                mStoreId = "";
                            }
                        }
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_VIP_CARD_STORE_LIST)) {
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCardActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData().size() == 0) {
                            showNoStoreDialog();
                        } else if (msg.getData().size() == 1) {
                            Intent intent = new Intent(VipCardActivity.this, ShareVipActivity.class);
                            intent.putExtra(Constants.INTENT_VIP_STORE_ID, msg.getData().get(0).getStoreId());
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(VipCardActivity.this, VipListActivity.class);
                            intent.putExtra(Constants.INTENT_VIP_STORE_LIST, msg);
                            startActivity(intent);
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_VIP_PAY_WHITE)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCardActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mSlVerification.setVisibility(msg.getData().isWhiteList()?View.VISIBLE:View.GONE);
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private void showNoStoreDialog() {
        NoticeDialog noticeDialog = new NoticeDialog(VipCardActivity.this
                , getString(R.string.tv_no_store), "", R.layout.notice_dialog_common);
        noticeDialog.show();
    }

    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }


    @Override
    protected void onPause() {
        super.onPause();
        dismissLoading();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_vip_code:
                if (MyApplication.getIsMerchant() || MyApplication.getIsCasher()) {
                    startActivity(new Intent(VipCardActivity.this, ShareVipActivity.class));
                } else {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("pageSize", 1000);
                        map.put("currentPage", 1);
                        ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_VIP_CARD_STORE_LIST, map);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
                break;
            case R.id.ll_vip_recharge:
                Intent intentRecharge = new Intent(VipCardActivity.this, VipCardSearchActivity.class);
                intentRecharge.putExtra(Constants.INTENT_VIP_TRADE_TYPE, 1);
                startActivity(intentRecharge);
                break;
            case R.id.ll_vip_verification:
                Intent intentVerification = new Intent(VipCardActivity.this, VipCardSearchActivity.class);
                intentVerification.putExtra(Constants.INTENT_VIP_TRADE_TYPE, 0);
                startActivity(intentVerification);
                break;
            case R.id.ll_vip_count_verification:
                Intent intentCount = new Intent(VipCardActivity.this, CaptureActivity.class);
                intentCount.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_COUNT_VERIFICATION);
                startActivity(intentCount);
                break;
            case R.id.tv_title:
                Intent intentShop = new Intent(this, ShopActivity.class);
                intentShop.putExtra(Constants.INTENT_STORE_ID, mStoreId);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            String storeName = shopBean.getStoreName();
            String storeId = shopBean.getStoreId();
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, storeId);
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
            mStoreId = storeId;
            mTvTitle.setText(storeName);
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeName", storeName);
                map.put("storeId", storeId);
                ServerClient.newInstance(MyApplication.getContext()).postStore(MyApplication.getContext(), Constants.TAG_PUT_STORE, map);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }
}
