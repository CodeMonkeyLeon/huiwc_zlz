package com.hstypay.enterprise.activity.vipCard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MoneyPopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.activity.bill.BillActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.VipCardListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MoneyFormat;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class VipCardInfoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mButtonTitle, mTvTitle;
    private Button mBtnVerification;
    private ScrollView mSvVipInfo;
    private int mTradeType;
    private TextView mTvVipCode, mTvVipPhone, mTvVipName, mTvVipBirthday, mTvVipLevel, mTvVipBalance,
            mTvVipConsume, mTvVipIntegral, mTvVipStatus, mTvVipInfo, mTvVipRegisterDate;
    private VipCardListBean.DataEntity mVipCardInfo;

    private boolean isTagThreeOrFive = true; //标示 默认是3次，false是5次7秒
    private long timeCount = 5;
    private int count = 0;
    private int countFive = 0;
    private String dialogMessage = null;
    private NoticeDialog mNoticeDialog;
    private int timeTotal;//轮询总时长，50s
    private Handler mHandler;
    private boolean isCehckOrderStatus;
    private SelectDialog mDialogReverse;
    private PayBean.DataBean mPayBean;
    private CommonNoticeDialog mDialogInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_card_info);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);

        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButtonTitle = findViewById(R.id.button_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mSvVipInfo = (ScrollView) findViewById(R.id.sv_vip_info);
        mBtnVerification = (Button) findViewById(R.id.btn_vip_verification);
        mButtonTitle.setVisibility(View.INVISIBLE);

        mTvVipCode = (TextView) findViewById(R.id.tv_vip_code);
        mTvVipPhone = (TextView) findViewById(R.id.tv_vip_tel);
        mTvVipName = (TextView) findViewById(R.id.tv_vip_name);
        mTvVipBirthday = (TextView) findViewById(R.id.tv_vip_birthday);
        mTvVipLevel = (TextView) findViewById(R.id.tv_vip_level);
        mTvVipBalance = (TextView) findViewById(R.id.tv_vip_balance);
        mTvVipConsume = (TextView) findViewById(R.id.tv_vip_consume);
        mTvVipIntegral = (TextView) findViewById(R.id.tv_vip_integral);
        mTvVipStatus = (TextView) findViewById(R.id.tv_vip_status);
        mTvVipInfo = (TextView) findViewById(R.id.tv_vip_info);
        mTvVipRegisterDate = (TextView) findViewById(R.id.tv_vip_register_date);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtnVerification.setOnClickListener(this);
    }

    public void initData() {
        mHandler = new Handler();
        mVipCardInfo = (VipCardListBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_VIP_CARD_INFO);
        mTradeType = getIntent().getIntExtra(Constants.INTENT_VIP_TRADE_TYPE, 0);//0:核销 1:充值
        if (mTradeType == 1) {
            isCehckOrderStatus = false;
            mBtnVerification.setText(getString(R.string.btn_vip_recharge));
        } else {
            isCehckOrderStatus = true;
            mBtnVerification.setText(getString(R.string.btn_vip_verification));
        }
        if (mVipCardInfo != null) {
            if (!TextUtils.isEmpty(mVipCardInfo.getMembersNo())) {
                mTvVipCode.setText(mVipCardInfo.getMembersNo());
            } else {
                mTvVipCode.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getMobile())) {
                mTvVipPhone.setText(mVipCardInfo.getMobile());
            } else {
                mTvVipPhone.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getName())) {
                mTvVipName.setText(mVipCardInfo.getName());
            } else {
                mTvVipName.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getBirthday())) {
                mTvVipBirthday.setText(mVipCardInfo.getBirthday());
            } else {
                mTvVipBirthday.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getMembersGrade())) {
                mTvVipLevel.setText(getString(R.string.tv_vip_level_before) + mVipCardInfo.getMembersGrade());
            } else {
                mTvVipLevel.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getAccountBalance())) {
                mTvVipBalance.setText(MoneyFormat.getInstance().formatMoney(mVipCardInfo.getAccountBalance()));
            } else {
                mTvVipBalance.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getCountConsume())) {
                mTvVipConsume.setText(MoneyFormat.getInstance().formatMoney(mVipCardInfo.getCountConsume()));
            } else {
                mTvVipConsume.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getIntegralBalance())) {
                mTvVipIntegral.setText(mVipCardInfo.getIntegralBalance());
            } else {
                mTvVipIntegral.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getState())) {

                mTvVipStatus.setText(mVipCardInfo.getState());
            } else {
                mTvVipStatus.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getActivatedState())) {

                switch (mVipCardInfo.getActivatedState()) {
                    case "1":
                        mTvVipInfo.setText(getString(R.string.tv_vip_status1));
                        break;
                    case "2":
                        mTvVipInfo.setText(getString(R.string.tv_vip_status2));
                        break;
                    case "3":
                        mTvVipInfo.setText(getString(R.string.tv_vip_status3));
                        break;
                    case "4":
                        mTvVipInfo.setText(getString(R.string.tv_vip_status4));
                        break;
                    case "5":
                        mTvVipInfo.setText(getString(R.string.tv_vip_status5));
                        break;
                }
            } else {
                mTvVipInfo.setText(getText(R.string.tv_info_null));
            }
            if (!TextUtils.isEmpty(mVipCardInfo.getRegisterTime())) {
                mTvVipRegisterDate.setText(mVipCardInfo.getRegisterTime());
            } else {
                mTvVipRegisterDate.setText(getText(R.string.tv_info_null));
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VIP_PAY)) {
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCardInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mPayBean = msg.getData();
                    if (mPayBean != null) {
                        if (mPayBean.isNeedQuery()) {
                            checkOrderState();
                        } else {
                            Intent intent = new Intent(VipCardInfoActivity.this, VipPayResultActivity.class);
                            intent.putExtra(Constants.INTENT_VIP_PAY_RESULT, msg.getData());
                            startActivity(intent);
                        }
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.VIP_CHECK_ORDER_STATE_TAG)) {
            dismissLoading();
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    checkOrderState();
                    break;
                case Constants.CHECK_ORDER_STATE_FALSE:
                    checkOrderState();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCardInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(VipCardInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CHECK_ORDER_STATE_TRUE:
                    if (msg.getData() != null) {
                        PayBean.DataBean data = msg.getData();
                        if (data != null) {
                            String tradeState = data.getTradeState() + "";
                            //1:未支付，2：支付成功,3:已关闭，4：转入退款，8：已冲正，9：已撤销
                            if (!StringUtils.isEmptyOrNull(tradeState)) {
                                switch (tradeState) {
                                    case "2":
                                        Intent intent = new Intent(VipCardInfoActivity.this, VipPayResultActivity.class);
                                        intent.putExtra(Constants.INTENT_VIP_PAY_RESULT, data);
                                        startActivity(intent);
                                        VipCardInfoActivity.this.finish();
                                        dismissDialog();
                                        break;
                                    case "3"://已关闭
                                        dismissDialog();
                                        showCommonNoticeDialog(VipCardInfoActivity.this, "订单已关闭");
                                        break;
                                    case "4"://转入退款
                                        dismissDialog();
                                        showCommonNoticeDialog(VipCardInfoActivity.this, "转入退款");
                                        break;
                                    case "8"://已冲正
                                    case "9"://已撤销
                                        dismissDialog();
                                        showCommonNoticeDialog(VipCardInfoActivity.this, "订单已撤销");
                                        break;
                                    default:
                                        checkOrderState();
                                        break;
                                }
                            } else {
                                checkOrderState();
                            }
                        } else {
                            checkOrderState();
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.VIP_ORDER_REVERSE_TAG)) {
            dismissLoading();
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    showCheckDialog(getString(R.string.dialog_title_reverse_fail));
                    break;
                case Constants.ORDER_REVERSE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCardInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                String title = getString(R.string.dialog_title_reverse_fail);
                                if (!TextUtils.isEmpty(msg.getError().getMessage())) {
                                    title = msg.getError().getMessage();
                                }
                                showCheckDialog(title);
                            }
                        }
                    }
                    break;
                case Constants.ORDER_REVERSE_TRUE:
                    //冲正成功
                    showCommonNoticeDialog(VipCardInfoActivity.this, getString(R.string.order_cz_success));
                    break;
            }
        }
    }

    private void checkOrderState() {
        if (count < 9 && timeTotal <= 50) {
            if (count < 4) {
                if (count == 1) {
                    Timer timer = new Timer();
                    TimerTask timerTask = new MyTimerTask();
                    timer.schedule(timerTask, 1000, 1000);
                }
                isTagThreeOrFive = true;
                timeCount = 5;
                count++;
            } else if (count >= 4) {
                isTagThreeOrFive = false;
                timeCount = 7;
                count++;
                countFive++;
            }
            mNoticeDialog = new NoticeDialog(VipCardInfoActivity.this, dialogMessage,"",R.layout.notice_dialog_check_old);
            DialogHelper.resize(VipCardInfoActivity.this, mNoticeDialog);
            mNoticeDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            if (isCehckOrderStatus) {
                showReverse();
            } else {
                showCheckDialog(getString(R.string.dialog_title_vip_bill));
            }
        }
    }

    private void showCheckDialog(String title) {
        mDialogInfo = new CommonNoticeDialog(VipCardInfoActivity.this, title, getString(R.string.dialog_content_vip_bill), getString(R.string.btnOk));
        mDialogInfo.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent(VipCardInfoActivity.this, BillActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_RECHARGE_BILL);
                startActivity(intent);
            }
        });
        DialogHelper.resize(VipCardInfoActivity.this, mDialogInfo);
        mDialogInfo.show();
    }

    private void showReverse() {
        dismissLoading();
        mDialogReverse = new SelectDialog(VipCardInfoActivity.this, getString(R.string.tv_revers_prompt),  getString(R.string.revers_tx),getString(R.string.query_tx),R.layout.select_common_dialog);
        mDialogReverse.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                toCheckOrderState();
            }
        });
        mDialogReverse.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                if (!NetworkUtils.isNetworkAvailable(VipCardInfoActivity.this)) {
                    showCommonNoticeDialog(VipCardInfoActivity.this, getString(R.string.network_exception));
                } else {
                    showNewLoading(true, getString(R.string.public_order_reverse));
                    Map<String, Object> map = new HashMap<>();
                    map.put("outTradeNo", mPayBean.getOutTradeNo());
                    map.put("storeMerchantId", MyApplication.getDefaultStore());
                    map.put("deviceVersion", AppHelper.getVerCode(MyApplication.getContext()));
                    /*if (AppHelper.getAppType() == 1) {
                        map.put("deviceInfo", Constants.PAY_CLIENT_POS);
                    } else if (AppHelper.getAppType() == 2) {
                        map.put("deviceInfo", Constants.PAY_CLIENT_APP);
                    }*/
                    map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
                    ServerClient.newInstance(VipCardInfoActivity.this).orderReverse(VipCardInfoActivity.this, Constants.VIP_ORDER_REVERSE_TAG, map);
                }
            }
        });
        mDialogReverse.show();
    }

    private void toCheckOrderState() {
        if (!NetworkUtils.isNetworkAvailable(VipCardInfoActivity.this)) {
            showCommonNoticeDialog(VipCardInfoActivity.this, getString(R.string.network_exception));
            return;
        } else {
            loadDialog(VipCardInfoActivity.this, getString(R.string.public_order_state));
            Map<String, Object> map = new HashMap<>();
            if (mPayBean.getOutTradeNo() != null) {
                map.put("outTradeNo", mPayBean.getOutTradeNo());
                map.put("storeMerchantId", MyApplication.getDefaultStore());
                ServerClient.newInstance(VipCardInfoActivity.this).checkOrderState(VipCardInfoActivity.this, Constants.VIP_CHECK_ORDER_STATE_TAG, map);
            }
        }
    }

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            LogUtil.d("MyTimerTask计数：" + timeTotal);
            if (++timeTotal > 50) {
                LogUtil.d("MyTimerTask 结束了！！");
                System.gc();
                cancel();
            }
        }
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && mNoticeDialog != null) {
                if (isTagThreeOrFive) {
                    mNoticeDialog.setMessage(getString(R.string.dialog_order_stuts) + "，(" + timeCount
                            + getString(R.string.dialog_start) + count + getString(R.string.dialog_end));
                } else {
                    mNoticeDialog.setMessage(getString(R.string.dialog_order_stuts_five) + "，(" + timeCount
                            + getString(R.string.dialog_start) + countFive + getString(R.string.dialog_end));
                }
                timeCount--;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (mNoticeDialog != null && mNoticeDialog.isShowing()) {
                    mNoticeDialog.dismiss();
                }
                if (isCehckOrderStatus) {
                    toCheckOrderState();
                } else {
                    checkOrderState();
                }
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_vip_verification:
                MoneyPopupWindow moneyPopupWindow = new MoneyPopupWindow(new MoneyPopupWindow.OnSubmitListener() {
                    @Override
                    public void submit(double money) {
                        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                            showNewLoading(true, getString(R.string.public_loading));
                            Map<String, Object> map = new HashMap<>();
                            map.put("membersNo", mVipCardInfo.getMembersNo());
                            map.put("mobile", mVipCardInfo.getMobile());
                            map.put("money", BigDecimal.valueOf(money).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                            map.put("storeMerchantId", MyApplication.getDefaultStore());
                            map.put("userId", MyApplication.getUserId());
                            map.put("merchantId", MyApplication.getMechantId());
                            if (mTradeType == 0) {
                                ServerClient.newInstance(MyApplication.getContext()).vipVerification(MyApplication.getContext(), Constants.TAG_VIP_PAY, map);
                            } else if (mTradeType == 1) {
                                ServerClient.newInstance(MyApplication.getContext()).vipRecharge(MyApplication.getContext(), Constants.TAG_VIP_PAY, map);
                            }
                        } else {
                            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
                        }
                    }
                }, VipCardInfoActivity.this, mTradeType, mVipCardInfo.getMembersNo());
                moneyPopupWindow.showAtLocation(mBtnVerification, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                break;
            default:
                break;
        }
    }

    private void dismissDialog(){
        if (mNoticeDialog != null){
            mNoticeDialog.dismiss();
        }
        if (mDialogReverse != null){
            mDialogReverse.dismiss();
        }
        mHandler.removeCallbacks(myRunnable);
    }
}
