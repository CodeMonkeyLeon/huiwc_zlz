package com.hstypay.enterprise.activity.vipCard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.adapter.VipCardAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.VipCardListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VipCardSearchActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvNotice;
    private TextView mButtonTitle, mTvTitle,mTvNull;
    private RecyclerView mRecyclerView;
    private int mTradeType;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private EditTextDelete mEtInput;
    private List<VipCardListBean.DataEntity> mList;
    private VipCardAdapter mVipActiveAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_card_search);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButtonTitle = findViewById(R.id.button_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButtonTitle.setVisibility(View.INVISIBLE);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mIvNotice = (ImageView) findViewById(R.id.iv_notice_search);
        mTvTitle.setText(getString(R.string.title_vip_search));

        mEtInput = (EditTextDelete) findViewById(R.id.et_input);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);

    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButtonTitle.setOnClickListener(this);

        mEtInput.setOnEditClearListener(new EditTextDelete.OnEditClearListener() {
            @Override
            public void onEditClear() {
                mList.clear();
                mVipActiveAdapter.notifyDataSetChanged();
            }
        });
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        if (mEtInput.getText().toString().trim().length() == 6 || mEtInput.getText().toString().trim().length() == 11) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("storeMerchantId", MyApplication.getDefaultStore());
                            if (mEtInput.getText().toString().trim().length() == 6) {
                                map.put("cardTails", mEtInput.getText().toString().trim());
                            } else if (mEtInput.getText().toString().trim().length() == 11) {
                                map.put("mobile", mEtInput.getText().toString().trim());
                            }
                            mList.clear();
                            showNewLoading(true, getString(R.string.public_loading));
                            ServerClient.newInstance(MyApplication.getContext()).searchVipCard(MyApplication.getContext(), Constants.TAG_VIP_CARD_LIST, map);
                        } else {
                            MyToast.showToastShort(getString(R.string.hint_vip_code));
                        }
                    }else {
                        MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void initData() {
        /*mIvNotice.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);*/
        mTradeType = getIntent().getIntExtra(Constants.INTENT_VIP_TRADE_TYPE, 0);//0:核销 1:充值
        if (mTradeType == 1) {
            mIvNotice.setImageResource(R.mipmap.pic_notice_recharge);
        } else {
            mIvNotice.setImageResource(R.mipmap.pic_notice_verification);
        }

        mList = new ArrayList<>();
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mVipActiveAdapter = new VipCardAdapter(MyApplication.getContext(), mList);
        mRecyclerView.setAdapter(mVipActiveAdapter);

        mVipActiveAdapter.setOnItemClickListenser(new VipCardAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intentRecharge = new Intent(VipCardSearchActivity.this, VipCardInfoActivity.class);
                intentRecharge.putExtra(Constants.INTENT_VIP_TRADE_TYPE, mTradeType);
                intentRecharge.putExtra(Constants.INTENT_VIP_CARD_INFO, mList.get(position));
                startActivity(intentRecharge);
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VIP_CARD_LIST)) {
            VipCardListBean msg = (VipCardListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCardSearchActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mIvNotice.setVisibility(View.GONE);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mList.addAll(msg.getData());
                        mVipActiveAdapter.notifyDataSetChanged();
                    }else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_VIP_INFO) {
            //TODO 返回会员卡信息
        }
    }*/
}
