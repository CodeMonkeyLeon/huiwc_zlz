package com.hstypay.enterprise.activity.vipCard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.activity.bill.BillActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class VipCountCheckActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mButtonTitle, mTvTitle;
    private Button mBtnSubmit;
    private String mVipCountCode="";

    private boolean isTagThreeOrFive = true; //标示 默认是3次，false是5次7秒
    private long timeCount = 5;
    private int count = 0;
    private int countFive = 0;
    private String dialogMessage = null;
    private NoticeDialog mNoticeDialog;
    private int timeTotal;//轮询总时长，50s
    private Handler mHandler;
    private PayBean.DataBean mPayBean;
    private CommonNoticeDialog mDialogInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_count_check);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mButtonTitle = findViewById(R.id.button_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mTvTitle.setText(getString(R.string.title_vip_count_ensure));
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mButtonTitle.setVisibility(View.INVISIBLE);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButtonTitle.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
    }

    public void initData() {
        mHandler = new Handler();
        mVipCountCode = getIntent().getStringExtra(Constants.INTENT_VIP_COUNT_CODE);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VIP_COUNT_VERIFICATION)) {
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipCountCheckActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mPayBean = msg.getData();
                    if (msg.getData() != null) {
                        if (mPayBean.isNeedQuery()) {
                            checkOrderState();
                        } else {
                            Intent intent = new Intent(VipCountCheckActivity.this, VipPayResultActivity.class);
                            intent.putExtra(Constants.INTENT_VIP_PAY_RESULT, msg.getData());
                            startActivity(intent);
                        }
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private void checkOrderState() {
        if (count < 9 && timeTotal <= 50) {
            if (count < 4) {
                if (count == 1) {
                    Timer timer = new Timer();
                    TimerTask timerTask = new MyTimerTask();
                    timer.schedule(timerTask, 1000, 1000);
                }
                isTagThreeOrFive = true;
                timeCount = 5;
                count++;
            } else if (count >= 4) {
                isTagThreeOrFive = false;
                timeCount = 7;
                count++;
                countFive++;
            }
            mNoticeDialog = new NoticeDialog(VipCountCheckActivity.this, dialogMessage,"",R.layout.notice_dialog_check_old);
            DialogHelper.resize(VipCountCheckActivity.this, mNoticeDialog);
            mNoticeDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            showCheckDialog(getString(R.string.dialog_title_vip_bill));
        }
    }

    private void showCheckDialog(String title) {
        mDialogInfo = new CommonNoticeDialog(VipCountCheckActivity.this, title, getString(R.string.dialog_content_vip_bill), getString(R.string.btnOk));
        mDialogInfo.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                Intent intent = new Intent(VipCountCheckActivity.this, BillActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_RECHARGE_BILL);
                startActivity(intent);
            }
        });
        DialogHelper.resize(VipCountCheckActivity.this, mDialogInfo);
        mDialogInfo.show();
    }

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            LogUtil.d("MyTimerTask计数：" + timeTotal);
            if (++timeTotal > 50) {
                LogUtil.d("MyTimerTask 结束了！！");
                System.gc();
                cancel();
            }
        }
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && mNoticeDialog != null) {
                if (isTagThreeOrFive) {
                    mNoticeDialog.setMessage(getString(R.string.dialog_order_stuts) + "，(" + timeCount
                            + getString(R.string.dialog_start) + count + getString(R.string.dialog_end));
                } else {
                    mNoticeDialog.setMessage(getString(R.string.dialog_order_stuts_five) + "，(" + timeCount
                            + getString(R.string.dialog_start) + countFive + getString(R.string.dialog_end));
                }
                timeCount--;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (mNoticeDialog != null && mNoticeDialog.isShowing()) {
                    mNoticeDialog.dismiss();
                }
                checkOrderState();
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                submit();
                break;
            default:
                break;
        }
    }

    private void submit() {
        if (!NetworkUtils.isNetworkAvailable(VipCountCheckActivity.this)) {
            MyToast.showToastShort(ToastHelper.toStr(R.string.network_exception));
            return;
        } else {
            loadDialog(VipCountCheckActivity.this,getString(R.string.public_loading));
            Map<String, Object> map = new HashMap<>();
           /* map.put("membersNo", mVipCountCode);
            map.put("mobile", "18888888888");*/
            map.put("ottCode", mVipCountCode);
            map.put("storeMerchantId", MyApplication.getDefaultStore());
            map.put("userId", MyApplication.getUserId());
            map.put("merchantId", MyApplication.getMechantId());
            ServerClient.newInstance(VipCountCheckActivity.this).vipCountVerification(VipCountCheckActivity.this, Constants.TAG_VIP_COUNT_VERIFICATION, map);
        }
    }
}
