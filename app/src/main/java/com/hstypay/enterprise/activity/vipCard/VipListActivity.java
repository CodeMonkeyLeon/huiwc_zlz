package com.hstypay.enterprise.activity.vipCard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.adapter.VipCodeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StatusBarUtil;

public class VipListActivity extends BaseActivity implements View.OnClickListener, VipCodeAdapter.OnRecyclerViewItemClickListener {
    private ImageView mIvBack;
    private RecyclerView mRecyclerView;
    private StoreListBean mBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_list);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);

    }

    public void initData() {
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mBean = (StoreListBean) getIntent().getSerializableExtra(Constants.INTENT_VIP_STORE_LIST);
        if (mBean !=null && mBean.getData()!=null) {
            VipCodeAdapter adapter = new VipCodeAdapter(VipListActivity.this, mBean.getData());
            adapter.setOnItemClickListener(this);
            mRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

    /*@Override
    public boolean isNeedEventBus() {
        return true;
    }*/


    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(VipListActivity.this,ShareVipActivity.class);
        intent.putExtra(Constants.INTENT_VIP_STORE_ID,mBean.getData().get(position).getStoreId());
        startActivity(intent);
    }
}
