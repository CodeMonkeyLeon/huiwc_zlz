package com.hstypay.enterprise.activity.vipCard;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.ybx.PrintYbxService;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

import static com.hstypay.enterprise.R.id.iv_back;

/**
 * 支付成功
 * Created by admin on 2017/7/3.
 */

public class VipPayResultActivity extends BaseActivity implements View.OnClickListener {
    private Button mBlue_print;
    private ImageView mIvBack, mIvPayStatus;
    private Button mBtnComplete, mBtnPrint;
    private TextView mButton, mTvTitle, mTvTradeMoneyTitle, mTvPayStatus, mTvCouponMoney;
    private TextView mTvTradeMoney, mTvTradeTime, mTvTradeWay, mTvCashier, mTvTradeCode;
    private PayBean.DataBean mPayBean;
    private LinearLayout mTvCashierTitle, mLl_coupon, mLlVipMoney;
    private Printer printer;
    private PosPrintUtil mPosPrintUtil;
    private IWoyouService woyouService;
    private SafeDialog mLoadDialog;
    private PrintUtils mPrintUtils;

    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;

    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };
    private String mIntentName;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
            autoPrint();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(this, intent));
        boolean flag = false;
        flag = bindService(eintent, conn, Context.BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("print----onServiceDisconnected");
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogUtil.d("print----onServiceConnected");
            woyouService = IWoyouService.Stub.asInterface(service);
            autoPrint();
        }
    };

    /**
     * 绑定商米打印服务
     **/
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        startService(intent);
        bindService(intent, connService, Context.BIND_AUTO_CREATE);
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_pay_result);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();

        initListener();
        initData();
        initPosPrint();
        if (!MyApplication.getIsCasher()) {
            sendTodayDataBroadcast(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unregisterReceiver(mPrtReceiver);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.MIBAGPAY_LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
//            bindDeviceService();
            autoPrint();
        } else if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
            //autoPrint();
        }
    }

    protected void bindPayServer() {
        Intent intent = new Intent(PAY_SERVICE_ACTION);
        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        bindService(intent, payConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
                autoPrint();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil(this, mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            autoPrint();
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
            autoPrint();
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(this, PrintYbxService.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            registerReceiver(mPrtReceiver, filter);
            autoPrint();
        } else if ("a920".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        } else if ("hdy".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            autoPrint();
        }

    }

    private void autoPrint() {
        if (Constants.INTENT_NAME_CAPTURE_PAYRESULT.equals(mIntentName)) {
            if (mPayBean != null) {
                mPayBean.setPay(true);
                mPayBean.setTradeState(2);
                mPayBean.setCreateTime(mPayBean.getTradeTime());
                mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, getTradeDetail(mPayBean), true);
            }
            mIntentName = "";
        }
    }

    private void initListener() {
        mButton.setOnClickListener(this);
        mBlue_print.setOnClickListener(this);
        mBtnComplete.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvPayStatus = (ImageView) findViewById(R.id.logo_title);
        mTvPayStatus = (TextView) findViewById(R.id.tv_pay_status);
        mTvTradeMoneyTitle = (TextView) findViewById(R.id.tv_receive);//收款金额title
        mTvTradeMoney = (TextView) findViewById(R.id.tx_receivable);//收款金额
        mTvTradeTime = (TextView) findViewById(R.id.tv_order_time);//交易时间
        mTvTradeWay = (TextView) findViewById(R.id.tv_bank);//支付方式
        mTvTradeCode = (TextView) findViewById(R.id.tv_mch_transNo);
        mTvCashier = (TextView) findViewById(R.id.tv_cashier);//收银员
        mBtnComplete = (Button) findViewById(R.id.btn_success);
        mBtnPrint = (Button) findViewById(R.id.blue_print);
        mTvTitle.setText(R.string.title_pay_detail);
        mButton.setText(R.string.tv_refund);
        mBlue_print = (Button) findViewById(R.id.blue_print);
        mButton.setVisibility(View.INVISIBLE);
        mTvCashierTitle = (LinearLayout) findViewById(R.id.ll_cashier_title);

        mLl_coupon = (LinearLayout) findViewById(R.id.ll_coupon);//优惠金额
        mTvCouponMoney = (TextView) findViewById(R.id.tx_coupon);//优惠金额
        mLlVipMoney = (LinearLayout) findViewById(R.id.ll_vip_money);//金额

        mIvBack.setVisibility(View.INVISIBLE);
    }

    public void initData() {
        getPrintActive();
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mPayBean = (PayBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_VIP_PAY_RESULT);

        if (mPayBean != null) {
            //支付金额
            if (mPayBean.getApiProvider() == 7 || mPayBean.getApiProvider() == 8) {
                String money = mPayBean.getMoney() + "";
                if (!StringUtils.isEmptyOrNull(money)) {
                    mTvTradeMoney.setText(getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Long.parseLong(money) / 100d));
                }
                mLlVipMoney.setVisibility(View.VISIBLE);
            } else {
                mLlVipMoney.setVisibility(View.GONE);
                String residueNumber = mPayBean.getResidueNumber();
                mTvTradeMoneyTitle.setText("剩余次数");
                if (!StringUtils.isEmptyOrNull(residueNumber)) {
                    mTvTradeMoney.setText(mPayBean.getResidueNumber() + "次");
                } else {
                    mTvTradeMoney.setText("0次");
                }
            }

            //交易单号
            if (!StringUtils.isEmptyOrNull(mPayBean.getOrderNo())) {
                mTvTradeCode.setText(mPayBean.getOrderNo());
            }

            //支付时间
            if (!StringUtils.isEmptyOrNull(mPayBean.getTradeTime())) {
                mTvTradeTime.setText(mPayBean.getTradeTime());
            }


            if (MyApplication.getIsMerchant() || MyApplication.getIsAdmin() || MyApplication.getIsManager()) {
                mTvCashierTitle.setVisibility(View.GONE);
                mTvCashier.setVisibility(View.GONE);
            } else {
                //收银员
               /* if (!StringUtils.isEmptyOrNull(mPayBean.getOpUserRealName())) {
                    mTvCashier.setText(mPayBean.getOpUserRealName());
                } else {
                    mTvCashierTitle.setVisibility(View.GONE);
                }*/
            }
            int apiProvider = mPayBean.getApiProvider();
            switch (apiProvider) {
                case 1:
                    mTvTradeWay.setText("微信支付");
                    break;
                case 2:
                    mTvTradeWay.setText("支付宝支付");
                    break;
                case 3:
                    mTvTradeWay.setText("财付通支付");
                    break;
                case 4:
                    mTvTradeWay.setText("QQ钱包支付");
                    break;
                case 5:
                    mTvTradeWay.setText("银联支付");
                    break;
                case 6:
                    mTvTradeWay.setText("会员卡消费");
                    break;
                case 7:
                    mTvTradeWay.setText("人工充值");
                    break;
                case 8:
                    mTvTradeWay.setText("手动核销");
                    break;
                case 9:
                    mTvTradeWay.setText("次卡核销");
                    break;
                default:
                    mTvTradeWay.setText("其他");
                    break;
            }
        }
    }

    private void getPrintActive() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_PRINT_ACTIVE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_success:
                startActivity(new Intent(this, VipCardActivity.class));
                finish();
                break;
            //打印小票
            case R.id.blue_print:
                getPrint();
                break;
            default:
                break;
        }
    }

    public void getPrint() {
        /*if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.LIANDI)) {
            bindDeviceService();
        } else */
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        if (mPayBean != null) {
            mPayBean.setPay(true);
            mPayBean.setTradeState(2);
            mPayBean.setCreateTime(mPayBean.getTradeTime());
            mPosPrintUtil.print(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, getTradeDetail(mPayBean), false);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(this, VipCardActivity.class));
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderState(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PRINT_ACTIVE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        getLoginDialog(VipPayResultActivity.this, info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        showCommonNoticeDialog(VipPayResultActivity.this, info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, info.getData().isReceiptMerchantActivityEnabled());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, info.getData().getReceiptMerchantActivityUrl());
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, info.getData().getReceiptMerchantActivityTitle());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE, false);
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL, "");
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE, "");
                    }
                    break;
            }
        }
    }

    public static void startActivity(Context mContext, PayBean order) {
        Intent it = new Intent();
        it.setClass(mContext, VipPayResultActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("order", order);
        mContext.startActivity(it);
    }


    private TradeDetailBean getTradeDetail(PayBean.DataBean data) {
        TradeDetailBean tradeDetailBean = new TradeDetailBean();
        tradeDetailBean.setStoreMerchantIdCnt(data.getStoreMerchantIdCnt());
        tradeDetailBean.setOrderNo(data.getOrderNo());
        tradeDetailBean.setTransactionId(data.getTransactionId());
        tradeDetailBean.setTradeTime(data.getTradeFinishTime());
        tradeDetailBean.setTradeState(data.getTradeState());
        tradeDetailBean.setCashierName(data.getCashierName());
        tradeDetailBean.setOpUserRealName(data.getOpUserRealName());
        tradeDetailBean.setApiProvider(data.getApiProvider());
        tradeDetailBean.setApiCode(data.getApiCode());
        tradeDetailBean.setMoney(data.getMoney());
        tradeDetailBean.setRealMoney(data.getRealMoney());
        tradeDetailBean.setPayMoney(data.getPayMoney());
        tradeDetailBean.setCouponFee(data.getMchDiscountsMoney());
        tradeDetailBean.setCouponInfoList(data.getCouponInfoList());
        tradeDetailBean.setAttach(data.getAttach());
        tradeDetailBean.setOutTradeNo(data.getOutTradeNo());
        tradeDetailBean.setReqOrderNo(data.getReqOrderNo());
        tradeDetailBean.setOriReqOrderNo(data.getOriReqOrderNo());
        tradeDetailBean.setVoucherNo(data.getVoucherNo());
        tradeDetailBean.setCashPointName(data.getCashPointName());
        tradeDetailBean.setDeviceSn(data.getTermNo());

        tradeDetailBean.setOpenid(data.getOpenid());
        tradeDetailBean.setThirdMerchantId(data.getThirdMerchantId());
        tradeDetailBean.setThirdOrderNo(data.getThirdOrderNo());
        tradeDetailBean.setTradeCode(data.getTradeCode());
        return tradeDetailBean;
    }
}
