package com.hstypay.enterprise.activity.vipNew;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

public class ShareVipWechatActivity extends BaseActivity implements View.OnClickListener {
    private String mCodeUrl;
    private ImageView mIvBack, mIvShareCode;
    private TextView mButton, mTvMerchantName;
    private LinearLayout mLlShareWecaht, mLlShareFrients, mLlShareWeibo, mLlShareQQ, mLlShareQQSpace;
    private RelativeLayout mRlShareCode, mRlShareIcon;
    private SafeDialog dialog;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_QQ = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_SPACE = 103;
    private Bitmap mViewBitmap;
    private CommonNoticeDialog mDialogInfo;
    private SafeDialog mLoadDialog;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_wecaht_vip);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mIvShareCode = (ImageView) findViewById(R.id.iv_share_qrcode);
        mTvMerchantName = (TextView) findViewById(R.id.tv_code_merchant_name);
        mLlShareWecaht = (LinearLayout) findViewById(R.id.ll_share_wechat);
        mLlShareFrients = (LinearLayout) findViewById(R.id.ll_share_friends);
        mLlShareWeibo = (LinearLayout) findViewById(R.id.ll_share_weibo);
        mLlShareQQ = (LinearLayout) findViewById(R.id.ll_share_qq);
        mLlShareQQSpace = (LinearLayout) findViewById(R.id.ll_share_qq_space);
        mRlShareCode = (RelativeLayout) findViewById(R.id.rl_share_code);
        mRlShareIcon = (RelativeLayout) findViewById(R.id.rl_share_icon);
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);

        if (AppHelper.getAppType() == 1 || AppHelper.getApkType() != 0) {
            mRlShareIcon.setVisibility(View.INVISIBLE);
            mButton.setVisibility(View.INVISIBLE);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mLlShareWecaht.setOnClickListener(this);
        mLlShareFrients.setOnClickListener(this);
        mLlShareWeibo.setOnClickListener(this);
        mLlShareQQ.setOnClickListener(this);
        mLlShareQQSpace.setOnClickListener(this);
    }

    public void initData() {
        mTvMerchantName.setText(MyApplication.getMerchantName());
        mCodeUrl = getIntent().getStringExtra(Constants.INTENT_CODE_URL);
        if (!TextUtils.isEmpty(mCodeUrl)) {
            Picasso.get().load(mCodeUrl).error(R.mipmap.icon_general_noloading).into(mIvShareCode);
        } else {
            mRlShareIcon.setVisibility(View.INVISIBLE);
            mButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (!TextUtils.isEmpty(mCodeUrl)) {
            mViewBitmap = ScreenUtils.createViewBitmap(mRlShareCode);
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareVipWechatActivity.this, permissionArray);
                    if (results) {
                        ScreenUtils.saveImageToGallery(this, mViewBitmap);
                        getDialogSuccess(getString(R.string.dialog_copy_picture));
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                    }
                }
                break;
            case R.id.ll_share_wechat:
                MtaUtils.mtaId(ShareVipWechatActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    ShareImage(SHARE_MEDIA.WEIXIN, mViewBitmap, mViewBitmap);
                }
                break;
            case R.id.ll_share_friends:
                MtaUtils.mtaId(ShareVipWechatActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    ShareImage(SHARE_MEDIA.WEIXIN_CIRCLE, mViewBitmap, mViewBitmap);
                }
                break;
            case R.id.ll_share_weibo:
                break;
            case R.id.ll_share_qq:
                MtaUtils.mtaId(ShareVipWechatActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareVipWechatActivity.this, permissionArray);
                    if (results) {
                        ShareImage(SHARE_MEDIA.QQ, mViewBitmap, mViewBitmap);
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_QQ, permissionArray, getString(R.string.permission_content_share));
                    }
                }
                break;
            case R.id.ll_share_qq_space:
                MtaUtils.mtaId(ShareVipWechatActivity.this, "F002");
                if (TextUtils.isEmpty(mCodeUrl)) {
                    MyToast.showToastShort(getString(R.string.error_qrcode));
                } else {
                    boolean results = PermissionUtils.checkPermissionArray(ShareVipWechatActivity.this, permissionArray);
                    if (results) {
                        ShareImage(SHARE_MEDIA.QZONE, mViewBitmap, mViewBitmap);
                    } else {
                        showNotice(MY_PERMISSIONS_REQUEST_CALL_SPACE, permissionArray, getString(R.string.permission_content_share));
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ScreenUtils.saveImageToGallery(this, mViewBitmap);
                    getDialogSuccess(getString(R.string.dialog_copy_picture));
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_QQ:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ShareImage(SHARE_MEDIA.QQ, mViewBitmap, mViewBitmap);
                } else {
                    showDialog(getString(R.string.permission_set_content_share));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_SPACE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ShareImage(SHARE_MEDIA.QZONE, mViewBitmap, mViewBitmap);
                } else {
                    showDialog(getString(R.string.permission_set_content_share));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void ShareImage(SHARE_MEDIA platform, Bitmap image, Bitmap thumb) {
        UMImage pic = new UMImage(ShareVipWechatActivity.this, image);
        pic.setThumb(new UMImage(ShareVipWechatActivity.this, thumb));
        new ShareAction(ShareVipWechatActivity.this).withMedia(pic).setPlatform(platform).setCallback(shareListener).withText("识别二维码领取会员卡").share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }

    public void getDialogSuccess(String title) {
        if (mDialogInfo == null) {
            mDialogInfo = new CommonNoticeDialog(ShareVipWechatActivity.this, title, getString(R.string.dialog_notice_button));
            DialogHelper.resize(ShareVipWechatActivity.this, mDialogInfo);
        }
        mDialogInfo.show();
    }
}
