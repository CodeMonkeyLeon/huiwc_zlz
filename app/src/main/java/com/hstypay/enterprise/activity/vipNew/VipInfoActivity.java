package com.hstypay.enterprise.activity.vipNew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.HSVipInfoBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.NumberUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class VipInfoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack, mIvPhoto;
    private LinearLayout mLlContent;
    private TextView mTvRegisterDate, mTvVipName, mTvVipPhone, mTvVipLevel, mTvVipConsumeMoney, mTvVipAccountMoney, mTvVipScore, mTvVipUsedScore;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip_info);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);

        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvRegisterDate = findViewById(R.id.tv_register_date);
        mIvPhoto = findViewById(R.id.iv_photo);
        mTvVipName = findViewById(R.id.tv_vip_name);
        mTvVipPhone = findViewById(R.id.tv_vip_phone);
        mTvVipLevel = findViewById(R.id.tv_vip_level);
        mTvVipConsumeMoney = findViewById(R.id.tv_vip_consume_money);
        mTvVipAccountMoney = findViewById(R.id.tv_vip_account_money);
        mTvVipScore = findViewById(R.id.tv_vip_score);
        mTvVipUsedScore = findViewById(R.id.tv_vip_used_score);
        mLlContent = findViewById(R.id.ll_content);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
    }

    public void initData() {
        String cardId = getIntent().getStringExtra(Constants.INTENT_VIP_CARD_ID);
        if(TextUtils.isEmpty(cardId)){
            MyToast.showToastShort("会员号有误！");
            return;
        }
        if (!NetworkUtils.isNetworkAvailable(VipInfoActivity.this)) {
            showCommonNoticeDialog(VipInfoActivity.this, getString(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String,Object> map = new HashMap<>();
            map.put("cardId",cardId);
            ServerClient.newInstance(VipInfoActivity.this).queryCardDetail(VipInfoActivity.this, Constants.TAG_HS_VIP_INFO, map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_HS_VIP_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            HSVipInfoBean msg = (HSVipInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VipInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(VipInfoActivity.this, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    HSVipInfoBean.DataEntity data = msg.getData();
                    if (data != null) {
                        mLlContent.setVisibility(View.VISIBLE);
                        mTvRegisterDate.setText("注册时间：" + DateUtil.formartDate(data.getRegisterTime(),"yyyy-MM-dd HH:mm:ss","yyyy.MM.dd"));
                        Picasso.get().load(data.getHeadSculpture()).placeholder(R.mipmap.icon_my_business_general).error(R.mipmap.icon_my_business_general).into(mIvPhoto);
                        mTvVipName.setText(setViewString(data.getName()));
                        mTvVipPhone.setText(setViewString(data.getMobile()));
                        mTvVipLevel.setText(setViewString(data.getLevelName()));
                        mTvVipConsumeMoney.setText(DateUtil.formatMoneyUtil(data.getConsumeTotalAmt()));
                        mTvVipAccountMoney.setText(DateUtil.formatMoneyUtil(data.getBalance()));
                        mTvVipScore.setText(NumberUtil.formatNumber(data.getBonus()));
                        mTvVipUsedScore.setText(NumberUtil.formatNumber(data.getConsumeTotalBonus()));
                    } else {
                        mLlContent.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

    private String setViewString(String viewString){
        if (TextUtils.isEmpty(viewString))
            return "--";
        return viewString;
    }
}
