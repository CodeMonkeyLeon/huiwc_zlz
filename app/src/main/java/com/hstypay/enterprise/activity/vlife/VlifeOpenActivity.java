package com.hstypay.enterprise.activity.vlife;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.VlifeOpenBean;
import com.hstypay.enterprise.bean.VlifeOpenDetailBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;


public class VlifeOpenActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private Button mBtnSubmit;
    private TextView mTvTitle, mTvVlifeProtocol, mTvVlifeNotice;
    public static VlifeOpenActivity instance = null;
    private SafeDialog mLoadDialog;
    private VlifeOpenDetailBean.DataEntity mDetailBean;
    private boolean queryPayResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vlife_open);
        StatusBarUtil.setImmersiveStatusBar(this, true);
        MyApplication.getInstance().addActivity(this);
        instance = this;
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mTvVlifeNotice = findViewById(R.id.tv_vlife_notice);
        mTvVlifeProtocol = findViewById(R.id.tv_vlife_protocol);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mTvTitle.setText(R.string.title_open);

        setButtonEnable(mBtnSubmit, true);
    }

    public void initEvent() {
        mBtnSubmit.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mTvVlifeProtocol.setOnClickListener(this);
    }

    public void initData() {
        mDetailBean = (VlifeOpenDetailBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_DAOJIA_EDITION);
        mTvVlifeNotice.setText("“威生活到家”致力于线下泛零售商户转型线上服务\n" +
                "为消费者提供快捷的社区购物服务\n" +
                "实现线上+线下的综合经营模式");
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (queryPayResult){
            queryDaojiaEdition();
        }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                /*if (mDetailBean != null) {
                    showOpenDialog(mDetailBean);
                }*/
                getStoreList();
                break;
            case R.id.tv_vlife_protocol:
                Intent intent = new Intent(VlifeOpenActivity.this, RegisterActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT, "https://daojiamch.hstytest.com/scomch/protocol");
                startActivity(intent);
                break;
            default:
                break;
        }
    }

   /* private void showOpenDialog(final VlifeOpenDetailBean.DataEntity detailBean) {
        boolean showPay;
        showPay = (detailBean.getEdition() == 2);
        final SelectVlifeDialog openDialog = new SelectVlifeDialog(VlifeOpenActivity.this,
                getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(detailBean.getPrice())
                        + "/" + detailBean.getUnit(), showPay);
        openDialog.setOnClickOkListener(new SelectVlifeDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                if (detailBean.getEdition() == 2) {
                    toMiniProgram();
                } else {
                    getStoreList();
                }
            }
        });
        openDialog.show();
    }*/

    /*private void toMiniProgram() {
        if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
            String appId = "wx6e411e48e4dacb00"; // 填应用AppId
            IWXAPI api = WXAPIFactory.createWXAPI(MyApplication.getContext(), appId);
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = "gh_942f08615a59"; // 填小程序原始id
//        req.userName = "gh_34132a75721a"; // 填小程序原始id
            req.path = "pages/payment/daojia-pay?payMchId=" + mDetailBean.getMerchantId() +"&edition="+mDetailBean.getEdition()+"&appCode=daojia" ;//拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
            api.sendReq(req);
            queryPayResult = true;
        } else {
            MyToast.showToastShort("请先安装微信！");
        }
    }*/

    /**
     * 开通到家
     */
    private void openDaojia(String storeId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String,Object> map = new HashMap<>();
            map.put("storeId",storeId);
            ServerClient.newInstance(MyApplication.getContext()).openDaojia(MyApplication.getContext(), Constants.TAG_OPEN_DAOJIA, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 门店列表
     */
    private void getStoreList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", 15);
            map.put("currentPage", 1);
            map.put("merchantDataType", 1);
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_OPEN_DAOJIA_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 商户开通付费详情
     */
    /*private void queryDaojiaEdition() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).queryDaojiaEdition(MyApplication.getContext(), Constants.TAG_QUERY_DAOJIA_OPEN, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }*/

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_OPEN_DAOJIA_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VlifeOpenActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData().size() == 0) {
                            showNoStoreDialog();
                        } else if (msg.getData().size() == 1) {
                            openDaojia(msg.getData().get(0).getStoreId());
                        } else {
                            Intent intentShop = new Intent(this, ShopActivity.class);
                            intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_DAOJIA_STORE_LIST);
                            startActivityForResult(intentShop, Constants.REQUEST_DAOJIA_STORE_LIST);
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_OPEN_DAOJIA)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            VlifeOpenBean msg = (VlifeOpenBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VlifeOpenActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (!TextUtils.isEmpty(msg.getData())) {
                        finish();
                        String jumpUrl = msg.getData();
                        Intent intent = new Intent(this, RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, jumpUrl);
                        intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                        intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                        startActivity(intent);
                    }
                    break;
            }
        }

        /*if (event.getTag().equals(Constants.TAG_QUERY_DAOJIA_OPEN)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            VlifeOpenDetailBean msg = (VlifeOpenDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(VlifeOpenActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData().getEdition() == 2 && msg.getData().getOrderStatus() == 1) {
                            showPayResultDialog();
                        } else {
                            getStoreList();
                        }
                    } else {
                        MyToast.showToast(getString(R.string.data_error), Toast.LENGTH_SHORT);
                    }
                    break;
            }
        }*/
    }

    /*public void showPayResultDialog() {
        SelectDialog dialog = new SelectDialog(VlifeOpenActivity.this, "请确认支付状态", "取消","继续支付",R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                toMiniProgram();
                queryPayResult = true;
            }
        });
        DialogHelper.resize(VlifeOpenActivity.this, dialog);
        dialog.show();
    }*/

    private void showNoStoreDialog() {
        NoticeDialog noticeDialog = new NoticeDialog(VlifeOpenActivity.this
                , getString(R.string.tv_no_store), "", R.layout.notice_dialog_common);
        noticeDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_DAOJIA_STORE_LIST) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            if (shopBean != null) {
                openDaojia(shopBean.getStoreId());
            }
        }
    }
}

