package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class AccountItemAdapter extends RecyclerView.Adapter<AccountItemAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private OnRecyclerDeleteClickListener mOnItemDeleteListener;
    private Context mContext;
    private List<String> mAccounts;

    public AccountItemAdapter(Context context, List<String> accounts) {
        this.mContext = context;
        this.mAccounts = accounts;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnRecyclerDeleteClickListener {
        void onItemDelete(int position);
    }

    public void setOnItemDeleteClickListener(OnRecyclerDeleteClickListener listener) {
        this.mOnItemDeleteListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public AccountItemAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_account, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(AccountItemAdapter.HomeViewHolder holder, final int position) {
        if (mAccounts == null)
            return;
        holder.mTvAccountItem.setText(mAccounts.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
        holder.mIvDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemDeleteListener!=null) {
                    mOnItemDeleteListener.onItemDelete(position);
                }

            }
        });
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mAccounts != null)
            return mAccounts.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvAccountItem;
        private ImageView mIvDeleteItem;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvAccountItem = (TextView) itemView.findViewById(R.id.tv_account_item);
            mIvDeleteItem = (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }
}