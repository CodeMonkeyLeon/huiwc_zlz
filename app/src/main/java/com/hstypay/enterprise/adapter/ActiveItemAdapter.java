package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.VipActiveBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class ActiveItemAdapter extends RecyclerView.Adapter<ActiveItemAdapter.HomeViewHolder> {

    private Context mContext;
    private VipActiveBean mVipActiveBean;

    public ActiveItemAdapter(Context context, VipActiveBean vipActiveBean) {
        this.mContext = context;
        this.mVipActiveBean = vipActiveBean;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ActiveItemAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_active, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(ActiveItemAdapter.HomeViewHolder holder, int position) {
        List<VipActiveBean.ActiveItemBean> activeItem = mVipActiveBean.getActiveItem();
        if (mVipActiveBean.getActiveItem() == null)
            return;
        if (mVipActiveBean.getActiveType() == 2) {
            switch (mVipActiveBean.getActiveItemType()){
                case 1:
                    holder.mTvActiveItem.setText("充值 " +activeItem.get(position).getMoney()+" 元，送 "+activeItem.get(position).getSale()+" 元；");
                    break;
                case 2:
                    holder.mTvActiveItem.setText("充值 " +activeItem.get(position).getMoney()+" 元，减 "+activeItem.get(position).getSale()+" 元；");
                    break;
                case 3:
                    holder.mTvActiveItem.setText("充值满 " +activeItem.get(position).getMoney()+" 元， "+activeItem.get(position).getSale()+" 折；");
                    break;
            }
        }else if (mVipActiveBean.getActiveType() == 3) {
            switch (mVipActiveBean.getActiveItemType()){
                case 2:
                    holder.mTvActiveItem.setText("消费 " +activeItem.get(position).getMoney()+" 元，减 "+activeItem.get(position).getSale()+" 元；");
                    break;
                case 3:
                    holder.mTvActiveItem.setText("消费满 " +activeItem.get(position).getMoney()+" 元， "+activeItem.get(position).getSale()+" 折；");
                    break;
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mVipActiveBean.getActiveItem() != null)
            return mVipActiveBean.getActiveItem().size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvActiveItem;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvActiveItem = (TextView) itemView.findViewById(R.id.tv_active_item);
        }
    }
}