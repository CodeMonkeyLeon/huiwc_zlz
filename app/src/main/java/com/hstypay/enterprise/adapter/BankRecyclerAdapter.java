package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.BankListBean;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class BankRecyclerAdapter extends RecyclerView.Adapter<BankRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<BankListBean.DataEntity> mList;
    private long mBankId;
    private boolean isFirstEnter = true;
    private int layoutPosition;

    public BankRecyclerAdapter(Context context, List<BankListBean.DataEntity> list, long bankId) {
        this.mContext = context;
        this.mList = list;
        this.mBankId = bankId;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public BankRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_bank, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final BankRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mTvName.setText(mList.get(position).getBankName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                    layoutPosition = holder.getLayoutPosition();
                    isFirstEnter = false;
                    notifyDataSetChanged();
                }
            }
        });
        if (isFirstEnter) {
            if (mBankId!=-1L) {
                if (mBankId==mList.get(position).getBankId()) {
                    holder.mIvNameChoiced.setVisibility(View.VISIBLE);
                    holder.mTvName.setTextColor(UIUtils.getColor(R.color.theme_color));
                } else {
                    holder.mIvNameChoiced.setVisibility(View.INVISIBLE);
                    holder.mTvName.setTextColor(UIUtils.getColor(R.color.home_text));
                }
            }
        } else {
            if (position == layoutPosition) {
                holder.mIvNameChoiced.setVisibility(View.VISIBLE);
                holder.mTvName.setTextColor(UIUtils.getColor(R.color.theme_color));
            } else {
                holder.mIvNameChoiced.setVisibility(View.INVISIBLE);
                holder.mTvName.setTextColor(UIUtils.getColor(R.color.home_text));
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvName;
        private ImageView mIvNameChoiced;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvName = (TextView) itemView.findViewById(R.id.tv_name_choice);
            mIvNameChoiced = (ImageView) itemView.findViewById(R.id.iv_name_choice);
        }
    }

}