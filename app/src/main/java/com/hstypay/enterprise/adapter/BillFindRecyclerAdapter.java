package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.bill.BillFindActivity;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class BillFindRecyclerAdapter extends RecyclerView.Adapter<BillFindRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<BillsBean.DataEntity> mList;
    private String text = "";
    private String otherType= Constants.HPAY_RECEIVE_ORDER;//otherType: HPAY_REFUND_ORDER：退款、HPAY_RECEIVE_ORDER：收款

    public BillFindRecyclerAdapter(Context context, List<BillsBean.DataEntity> list) {
        this.mContext = context;
        this.mList = list;
        ((BillFindActivity) mContext).setOnItemSearchListener(new BillFindActivity.OnItemSearchListener() {
            @Override
            public void onItemSearch(String text) {
                BillFindRecyclerAdapter.this.text = text;
            }
        });
    }

    public void setOtherType(String otherType) {
        this.otherType = otherType;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public BillFindRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = View.inflate(parent.getContext(), R.layout.item_bill_find, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final BillFindRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
        BillsBean.DataEntity dataEntity = mList.get(position);
        setView(dataEntity, holder);
        if (dataEntity.getMchDiscountsMoney() > 0) {
            holder.mTvDiscountMoney.setVisibility(View.VISIBLE);
            holder.mTvDiscountMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getMoney() / 100d));
        }else {
            holder.mTvDiscountMoney.setVisibility(View.INVISIBLE);
        }
        if ( Constants.HPAY_REFUND_ORDER.equals(otherType)){
            //退款
            holder.mTvPayMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));

        }else {
            //收款
            holder.mTvPayMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getPayMoney() / 100d));
        }

        try{
            String orderNo;
            int index;
            if ( Constants.HPAY_REFUND_ORDER.equals(otherType)){
                //退款
                if (dataEntity.getRefundNo().length() > 7) {
                    orderNo = dataEntity.getRefundNo().substring(dataEntity.getRefundNo().length() - 7);
                    index = orderNo.lastIndexOf(text);
                } else {
                    orderNo = dataEntity.getRefundNo();
                    index = orderNo.lastIndexOf(text);
                }

            }else {
                //收款
                if (dataEntity.getOrderNo().length() > 7) {
                    orderNo = dataEntity.getOrderNo().substring(dataEntity.getOrderNo().length() - 7);
                    index = orderNo.lastIndexOf(text);
                } else {
                    orderNo = dataEntity.getOrderNo();
                    index = orderNo.lastIndexOf(text);
                }
            }
            if (index < 0) {
                holder.mTvBillNo.setText("(" + orderNo + ")");
            } else {
                SpannableString styledText = new SpannableString("(" + orderNo + ")");
                styledText.setSpan(new TextAppearanceSpan(mContext, R.style.styleNumber), index + 1, index + 1 + text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.mTvBillNo.setText(styledText, TextView.BufferType.SPANNABLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        if ( Constants.HPAY_REFUND_ORDER.equals(otherType)){
            //退款
            holder.mTvBillDate.setText(dataEntity.getRefundTime());
        }else {
            //收款
            holder.mTvBillDate.setText(dataEntity.getTradeTime());
        }

        if ( Constants.HPAY_REFUND_ORDER.equals(otherType)){
            //退款
            //退款状态
            switch (dataEntity.getRefundStatus()) {
                case 1://初始化
                case 6:
                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state1));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                case 2://退款成功
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state2));
                    break;
                case 3://退款失败
                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state3));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_notice_red));
                    break;
                case 4://未确定
                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state4));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                case 5://转入代发
                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state5));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
            }
            holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.home_text));
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(1.0f);//饱和度 0灰色 100过度彩色，50正常
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.mIvPayType.setColorFilter(filter);
        }else {
            //收款
            //交易状态
            if (dataEntity.getTradeStatus() == 1) {//未支付
                holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state1));
                holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(0);//饱和度 0灰色 100过度彩色，50正常
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                holder.mIvPayType.setColorFilter(filter);
            } else {
                switch (dataEntity.getTradeStatus()) {
                    case 2://支付成功
                        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state2));
                        break;
                    case 8://已冲正
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state8));
                        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                        break;
                    case 3://已关闭
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state3));
                        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                        break;
                    case 4://转入退款
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state4));
                        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
                        break;
                    case 9://已撤销
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state9));
                        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                        break;
                    default:
                        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                        break;
                }
                holder.mTvPayState.setText(OrderStringUtil.getTradeStateString(dataEntity.getTradeState()));
                holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.home_text));
                holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.home_text));
                holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.home_value_text));
                holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.home_text));
                ColorMatrix matrix = new ColorMatrix();
                matrix.setSaturation(1.0f);//饱和度 0灰色 100过度彩色，50正常
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                holder.mIvPayType.setColorFilter(filter);
            }
        }

        holder.mTvDiscountMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);  // 设置中划线并加清晰
    }

    public void setView(BillsBean.DataEntity dataEntity, BillFindRecyclerAdapter.HomeViewHolder holder) {
        switch (dataEntity.getApiProvider()) {
            case 1://微信
                if (dataEntity.getTradeType() == 1) {
                    holder.mTvPayType.setText("会员卡充值");
//                    holder.mTvPayType.setText("微信充值");
                } else {
                    holder.mTvPayType.setText("微信支付收款");
                }
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_wechat);
                break;
            case 2://支付宝
                holder.mTvPayType.setText("支付宝收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_alipay);
                break;
            case 4://qq钱包
                holder.mTvPayType.setText("QQ钱包收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_qq);
                break;
            case 5://银联
                holder.mTvPayType.setText("银联支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_unionpay);
                break;
            case 6://会员卡
                holder.mTvPayType.setText("会员卡消费");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_member);
                break;
            case 7://人工充值
                holder.mTvPayType.setText("人工充值");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_recharge);
                break;
            case 8://手动核销
                holder.mTvPayType.setText("会员卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_verification);
                break;
            case 9://次卡核销
                holder.mTvPayType.setText("次卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_count_verification);
                break;
            case 10://刷卡支付
                holder.mTvPayType.setText("刷卡支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_bcard);
                break;
            case 11://福卡支付
                holder.mTvPayType.setText("福卡支付");
                holder.mIvPayType.setImageResource(R.mipmap.icon_list_fuka);
                break;
            case 13://pos刷脸支付
                holder.mTvPayType.setText("pos刷脸支付");
                holder.mIvPayType.setImageResource(R.mipmap.icon_list_pos_face);
                break;
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE:
                holder.mTvPayType.setText("数字人民币收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                break;
            case 3://财付通
            case 99://未知
            default:
                holder.mTvPayType.setText("其他支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_receivables);
                break;

        }
        if (dataEntity.getTradeType() == 1)
            holder.mTvPayType.setText("会员卡充值");
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvPayType, mTvPayMoney, mTvDiscountMoney, mTvPayState, mTvBillDate, mTvBillNo;
        private ImageView mIvPayType;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvPayType = (TextView) itemView.findViewById(R.id.item_tv_pay_type);
            mTvDiscountMoney = (TextView) itemView.findViewById(R.id.item_tv_discount_money);
            mTvPayMoney = (TextView) itemView.findViewById(R.id.item_tv_pay_money);
            mTvBillDate = (TextView) itemView.findViewById(R.id.item_tv_bill_date);
            mTvBillNo = (TextView) itemView.findViewById(R.id.item_tv_bill_no);
            mTvPayState = (TextView) itemView.findViewById(R.id.item_tv_pay_state);
            mIvPayType = (ImageView) itemView.findViewById(R.id.item_iv_pay_type);
        }
    }

}