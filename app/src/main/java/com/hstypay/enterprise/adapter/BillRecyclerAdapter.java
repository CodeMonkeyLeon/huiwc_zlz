package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.text.ParseException;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class BillRecyclerAdapter extends RecyclerView.Adapter<BillRecyclerAdapter.HomeViewHolder> {

    private Context mContext;
    private List<BillsBean.DataEntity> mBillList;
    private String otherType= Constants.HPAY_RECEIVE_ORDER;//otherType: HPAY_REFUND_ORDER：退款、HPAY_RECEIVE_ORDER：收款

    public BillRecyclerAdapter(Context context, List<BillsBean.DataEntity> billList) {
        this.mContext = context;
        this.mBillList = billList;
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void setOtherType(String otherType) {
        this.otherType = otherType;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_bill, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(HomeViewHolder holder, final int position) {
        if (mBillList == null)
            return;
        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        }/*new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        }*/);
        if (position == mBillList.size()-1){
            holder.mViewLine.setVisibility(View.INVISIBLE);
        }else {
            holder.mViewLine.setVisibility(View.VISIBLE);
        }
        BillsBean.DataEntity dataEntity = mBillList.get(position);
        setView(dataEntity, holder);

        if ( Constants.HPAY_REFUND_ORDER.equals(otherType)){
            //筛选了退款
            if (dataEntity.getRefundNo() != null) {
                if (dataEntity.getRefundNo().length() >= 4) {
                    holder.mTvBillNo.setText(dataEntity.getRefundNo().substring(dataEntity.getRefundNo().length() - 4));
                } else {
                    holder.mTvBillNo.setText(dataEntity.getRefundNo());
                }
            }
        }else {
            //筛选了收款
            if (dataEntity.getOrderNo() != null) {
                if (dataEntity.getOrderNo().length() >= 4) {
                    holder.mTvBillNo.setText(dataEntity.getOrderNo().substring(dataEntity.getOrderNo().length() - 4));
                } else {
                    holder.mTvBillNo.setText(dataEntity.getOrderNo());
                }
            }
        }

        setRefundView(holder, dataEntity);
        if (dataEntity.getApiProvider() == 9) {
            holder.mTvPayMoney.setVisibility(View.INVISIBLE);
        } else {
            holder.mTvPayMoney.setVisibility(View.VISIBLE);
        }
        holder.mTvDiscountMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);  // 设置中划线并加清晰
    }

    private void setRefundView(HomeViewHolder holder, BillsBean.DataEntity dataEntity) {
        holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.home_text));
        holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.home_text));
        holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.home_value_text));
        holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.home_text));
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(.7f);//饱和度 0灰色 100过度彩色，50正常
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        holder.mIvPayType.setColorFilter(filter);
        holder.mTvPayMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));
        holder.mTvDiscountMoney.setVisibility(View.INVISIBLE);
        if (!TextUtils.isEmpty(dataEntity.getRefundTime())) {
            try {
                holder.mTvBillDate.setText(DateUtil.formartDateToHHMMSS(dataEntity.getRefundTime()) + "  订单后四位:");
            } catch (ParseException e) {
                holder.mTvBillDate.setText("  订单后四位:");
                e.printStackTrace();
            }
        }
        switch (dataEntity.getRefundStatus()) {
            case 1://初始化
            case 6:
                holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state1));
                holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                break;
            case 2://退款成功
                holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
                holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state2));
                break;
            case 3://退款失败
                holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state3));
                holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_notice_red));
                break;
            case 4://未确定
                holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state4));
                holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                break;
            case 5://转入代发
                holder.mTvPayState.setText(UIUtils.getString(R.string.tv_refund_state5));
                holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                break;
            default:
                setReceiveView(holder, dataEntity);
                break;
        }
    }

    private void setReceiveView(HomeViewHolder holder, BillsBean.DataEntity dataEntity) {
        if (!TextUtils.isEmpty(dataEntity.getTradeTime())) {
            try {
                holder.mTvBillDate.setText(DateUtil.formartDateToHHMMSS(dataEntity.getTradeTime()) + "  订单后四位:");
            } catch (ParseException e) {
                holder.mTvBillDate.setText("  订单后四位:");
                e.printStackTrace();
            }
        } else if (!TextUtils.isEmpty(dataEntity.getCreateTime())) {
            try {
                holder.mTvBillDate.setText(DateUtil.formartDateToHHMMSS(dataEntity.getCreateTime()) + "  订单后四位:");
            } catch (ParseException e) {
                holder.mTvBillDate.setText("  订单后四位:");
                e.printStackTrace();
            }
        } else {
            holder.mTvBillDate.setText("  订单后四位:");
        }
        if (dataEntity.getMchDiscountsMoney() > 0) {
            holder.mTvDiscountMoney.setVisibility(View.VISIBLE);
            holder.mTvDiscountMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getMoney() / 100d));
        }else {
            holder.mTvDiscountMoney.setVisibility(View.INVISIBLE);
        }
        holder.mTvPayMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getPayMoney() / 100d));
        if (dataEntity.getTradeStatus() == 1) {//未支付
            holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state1));
            holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            holder.mTvDiscountMoney.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);//饱和度 0灰色 100过度彩色，50正常
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.mIvPayType.setColorFilter(filter);
        } else {
            switch (dataEntity.getTradeStatus()) {
                case 2://支付成功
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state2));
                    break;
                case 8://已冲正
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state8));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                case 3://已关闭
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state3));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                case 4://转入退款
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state4));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
                    break;
                case 9://已撤销
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state9));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                default:
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
            }
            holder.mTvPayState.setText(OrderStringUtil.getTradeStateString(dataEntity.getTradeState()));
            holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.home_text));
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(.7f);//饱和度 0灰色 100过度彩色，50正常
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.mIvPayType.setColorFilter(filter);
        }
    }

    public void setView(BillsBean.DataEntity dataEntity, HomeViewHolder holder) {
        switch (dataEntity.getApiProvider()) {
            case Constants.ApiProviderCode.WX://微信
                if (dataEntity.getTradeType() == 1) {
                    holder.mTvPayType.setText("会员卡充值");
//                    holder.mTvPayType.setText("微信充值");
                } else {
                    holder.mTvPayType.setText("微信支付收款");
                }
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_wechat);
                break;
            case Constants.ApiProviderCode.ZFB://支付宝
                /*if (dataEntity.getTradeType()==1) {
                    holder.mTvPayType.setText("支付宝充值");
                }else {
                    holder.mTvPayType.setText("支付宝收款");
                }*/
                holder.mTvPayType.setText("支付宝收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_alipay);
                break;
            case Constants.ApiProviderCode.QQ://qq钱包
                /*if (dataEntity.getTradeType()==1) {
                    holder.mTvPayType.setText("QQ钱包充值");
                }else {
                    holder.mTvPayType.setText("QQ钱包收款");
                }*/
                holder.mTvPayType.setText("QQ钱包收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_qq);
                break;
            case Constants.ApiProviderCode.UNIONPAY://银联
                /*if (dataEntity.getTradeType()==1) {
                    holder.mTvPayType.setText("银联充值");
                }else {
                    holder.mTvPayType.setText("银联支付收款");
                }*/
                holder.mTvPayType.setText("银联支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_unionpay);
                break;
            case Constants.ApiProviderCode.MEMBER://会员卡
                holder.mTvPayType.setText("会员卡消费");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_member);
                break;
            case Constants.ApiProviderCode.VIP_RECHARGE://人工充值
                holder.mTvPayType.setText("人工充值");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_recharge);
                break;
            case Constants.ApiProviderCode.VIP_VERIFICATION://手动核销
                holder.mTvPayType.setText("会员卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_verification);
                break;
            case Constants.ApiProviderCode.VIP_COUNT_VERIFICATION://次卡核销
                holder.mTvPayType.setText("次卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_count_verification);
                break;
            case Constants.ApiProviderCode.CARD_PAY://刷卡支付
                holder.mTvPayType.setText("刷卡支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_bcard);
                break;
            case Constants.ApiProviderCode.FUKA://福卡支付
                holder.mTvPayType.setText("福卡支付");
                holder.mIvPayType.setImageResource(R.mipmap.icon_list_fuka);
                break;
            case Constants.ApiProviderCode.POS_UNION_FACE://银联POS支付(万科)
//                holder.mTvPayType.setText("pos刷脸支付");
                holder.mIvPayType.setImageResource(R.mipmap.icon_list_pos_union_face);
                if (dataEntity.getApiCode().equals(Constants.WECHAT_FACEPAY)){
                    holder.mTvPayType.setText("微信POS刷脸支付收款");
                }else {
                    holder.mTvPayType.setText("银联POS刷脸支付");
                }
                break;
            case Constants.ApiProviderCode.POS_WECHAT_FACE://微信银联POS支付(万科)
                holder.mIvPayType.setImageResource(R.mipmap.icon_list_pos_wechat_face);
                holder.mTvPayType.setText("微信POS刷脸支付收款");
                break;
            case Constants.ApiProviderCode.DIGITAL_RMB:
                holder.mTvPayType.setText("数字人民币收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                break;
            case Constants.ApiProviderCode.OTHER_3://财付通
            case Constants.ApiProviderCode.OTHER_9://未知
            default:
                holder.mTvPayType.setText("其他支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_receivables);
                break;
        }

    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mBillList != null)
            return mBillList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvPayType, mTvPayMoney, mTvPayState, mTvBillDate, mTvBillNo, mTvDiscountMoney;
        private ImageView mIvPayType;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvPayType = (TextView) itemView.findViewById(R.id.item_tv_pay_type);
            mTvPayMoney = (TextView) itemView.findViewById(R.id.item_tv_pay_money);
            mTvDiscountMoney = (TextView) itemView.findViewById(R.id.item_tv_discount_money);
            mTvBillDate = (TextView) itemView.findViewById(R.id.item_tv_bill_date);
            mTvBillNo = (TextView) itemView.findViewById(R.id.item_tv_bill_no);
            mTvPayState = (TextView) itemView.findViewById(R.id.item_tv_pay_state);
            mIvPayType = (ImageView) itemView.findViewById(R.id.item_iv_pay_type);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}