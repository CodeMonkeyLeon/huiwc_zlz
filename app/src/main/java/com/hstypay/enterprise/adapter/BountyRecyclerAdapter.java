package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.BountyDataBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.text.ParseException;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class BountyRecyclerAdapter extends RecyclerView.Adapter<BountyRecyclerAdapter.HomeViewHolder> {

    private Context mContext;
    private List<BountyDataBean.DataEntity> mBountyList;

    public BountyRecyclerAdapter(Context context, List<BountyDataBean.DataEntity> billList) {
        this.mContext = context;
        this.mBountyList = billList;
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_bounty_bill, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(HomeViewHolder holder, final int position) {
        if (mBountyList == null)
            return;
        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });*/
        BountyDataBean.DataEntity dataEntity = mBountyList.get(position);
        setView(holder, dataEntity);
        setReceiveView(holder, dataEntity);
        if (dataEntity.getOrderNo().length() >= 4) {
            holder.mTvBillNo.setText(dataEntity.getOrderNo().substring(dataEntity.getOrderNo().length() - 4));
        } else {
            holder.mTvBillNo.setText(dataEntity.getOrderNo());
        }
        if (dataEntity.getApiProvider()==9){
            holder.mTvPayMoney.setVisibility(View.INVISIBLE);
        }else {
            holder.mTvPayMoney.setVisibility(View.VISIBLE);
        }
        holder.mTvPayState.setText(UIUtils.getString(R.string.tv_bounty_list));
        holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_bounty_list));
    }

    private void setReceiveView(HomeViewHolder holder, BountyDataBean.DataEntity dataEntity) {
        if (!TextUtils.isEmpty(dataEntity.getTradeTime())) {
            try {
                holder.mTvBillDate.setText(DateUtil.formartmmssHHMMSS(dataEntity.getTradeTime()) + "  订单后四位：");
            } catch (ParseException e) {
                holder.mTvBillDate.setText("  订单后四位：");
                e.printStackTrace();
            }
        }else {
            holder.mTvBillDate.setText("  订单后四位：");
        }
        holder.mTvPayMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(dataEntity.getRewardMoney() / 100d));
    }

    public void setView(HomeViewHolder holder, BountyDataBean.DataEntity dataEntity) {
        switch (dataEntity.getApiProvider()) {
            case 1://微信
                holder.mTvPayType.setText("微信支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_wechat);
                break;
            case 2://支付宝
                holder.mTvPayType.setText("支付宝收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_alipay);
                break;
            case 4://qq钱包
                holder.mTvPayType.setText("QQ钱包收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_qq);
                break;
            case 5://银联
                holder.mTvPayType.setText("银联支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_unionpay);
                break;
            case 6://会员卡
                holder.mTvPayType.setText("会员卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_member);
                break;
            case 7://人工充值
                holder.mTvPayType.setText("人工充值");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_recharge);
                break;
            case 8://手动核销
                holder.mTvPayType.setText("会员卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_verification);
                break;
            case 9://次卡核销
                holder.mTvPayType.setText("次卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_count_verification);
                break;
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE:
                holder.mIvPayType.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                holder.mTvPayType.setText("数字人民币");
                break;
            case 3://财付通
            case 99://未知
            default:
                holder.mTvPayType.setText("其他支付收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_receivables);
                break;
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mBountyList != null)
            return mBountyList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvPayType, mTvPayMoney, mTvPayState, mTvBillDate, mTvBillNo;
        private ImageView mIvPayType;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvPayType = (TextView) itemView.findViewById(R.id.item_tv_pay_type);
            mTvPayMoney = (TextView) itemView.findViewById(R.id.item_tv_pay_money);
            mTvBillDate = (TextView) itemView.findViewById(R.id.item_tv_bill_date);
            mTvBillNo = (TextView) itemView.findViewById(R.id.item_tv_bill_no);
            mTvPayState = (TextView) itemView.findViewById(R.id.item_tv_pay_state);
            mIvPayType = (ImageView) itemView.findViewById(R.id.item_iv_pay_type);
        }
    }

}