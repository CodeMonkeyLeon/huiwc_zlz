package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class CashierRecyclerAdapter extends RecyclerView.Adapter<CashierRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<DataEntity> mCashierList;
    private String mCashierId = "";

    public CashierRecyclerAdapter(Context context, List<DataEntity> cashierList) {
        this.mContext = context;
        this.mCashierList = cashierList;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public CashierRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_cashier, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final CashierRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mCashierList == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                    notifyDataSetChanged();
                }
            }
        });
        holder.mViewLine.setVisibility(position == mCashierList.size()-1 ? View.INVISIBLE : View.VISIBLE);
        holder.mTvCashierName.setText(mCashierList.get(position).getRealName());
        if (mCashierList.get(position).getUserId().equals(mCashierId)) {
            holder.mIvCashierChoiced.setVisibility(View.VISIBLE);
            holder.mTvCashierName.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            holder.mIvCashierChoiced.setVisibility(View.GONE);
            holder.mTvCashierName.setTextColor(UIUtils.getColor(R.color.black));
        }
    }

    public void setSelected(String cashierId){
        this.mCashierId = cashierId;
        notifyDataSetChanged();
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mCashierList != null)
            return mCashierList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvCashierName;
        private ImageView mIvCashierChoiced;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvCashierName = (TextView) itemView.findViewById(R.id.tv_cashier_choice);
            mIvCashierChoiced = (ImageView) itemView.findViewById(R.id.iv_cashier_choice);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}