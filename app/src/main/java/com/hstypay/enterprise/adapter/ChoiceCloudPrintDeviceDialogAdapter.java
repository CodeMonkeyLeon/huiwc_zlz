package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.DeviceBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/9/22
 * @desc 选择蓝牙打印机和云打印机的Adapter
 */
public class ChoiceCloudPrintDeviceDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DeviceBean> mCloudPrintDevices = new ArrayList<>();
    private Context mContext;
    private static final  int VIEWTYPE_TITLE = 0;//标题类型
    private static final int VIEWTYPE_DEVICE = 1;//打印机名称

    public ChoiceCloudPrintDeviceDialogAdapter(Context context){
        mContext = context;
    }

    public void setCloudPrintDevices(List<DeviceBean> cloudPrintDevices){
        mCloudPrintDevices.clear();
        notifyDataSetChanged();
        if (cloudPrintDevices!=null&& cloudPrintDevices.size()>0){
            mCloudPrintDevices.addAll(cloudPrintDevices);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        if (viewType == VIEWTYPE_TITLE){
            viewHolder = new TitleViewHold(LayoutInflater.from(mContext).inflate(R.layout.item_choice_cloud_print_device_title,parent,false));
        }else /*if (viewType == VIEWTYPE_DEVICE)*/{
            viewHolder = new DeviceViewHold(LayoutInflater.from(mContext).inflate(R.layout.item_choice_cloud_print_device_content,parent,false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEWTYPE_TITLE){
            ((TitleViewHold)holder).setData(mCloudPrintDevices.get(holder.getPosition()));
        }else if (getItemViewType(position) == VIEWTYPE_DEVICE){
            ((DeviceViewHold)holder).setData(mCloudPrintDevices.get(holder.getPosition()));
            ((DeviceViewHold)holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeviceBean dataBean = mCloudPrintDevices.get(holder.getPosition());
                    boolean select = dataBean.isSelect();
                    dataBean.setSelect(!select);
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCloudPrintDevices.size();
    }

    @Override
    public int getItemViewType(int position) {
        DeviceBean dataBean = mCloudPrintDevices.get(position);
        boolean title = dataBean.isTitleView();
        if (title){
            //标题
            return VIEWTYPE_TITLE;
        }else {
            //内容
            return VIEWTYPE_DEVICE;
        }
    }


    class TitleViewHold extends RecyclerView.ViewHolder{

        private final TextView mTv_title_device_type;

        public TitleViewHold(@NonNull View itemView) {
            super(itemView);
            mTv_title_device_type = itemView.findViewById(R.id.tv_title_device_type);
        }

        public void setData(DeviceBean dataBean) {
            boolean bluthDevice = dataBean.isBluthDevice();
            if (!bluthDevice){
                //云打印设备
                mTv_title_device_type.setText("云打印机");
            }else{
                //蓝牙打印设备
                mTv_title_device_type.setText("蓝牙打印机");
            }
        }
    }

    class DeviceViewHold extends RecyclerView.ViewHolder{

        private final TextView mTv_device_name_choice;//FD-58D
        private final TextView mTv_device_sn_choice;//SN:87484748474
        private final ImageView mIv_choice_icon_device;//勾选

        public DeviceViewHold(@NonNull View itemView) {
            super(itemView);
            mTv_device_name_choice = itemView.findViewById(R.id.tv_device_name_choice);
            mTv_device_sn_choice = itemView.findViewById(R.id.tv_device_sn_choice);
            mIv_choice_icon_device = itemView.findViewById(R.id.iv_choice_icon_device);

        }

        public void setData(DeviceBean dataBean) {
            boolean bluthDevice = dataBean.isBluthDevice();
            if (!bluthDevice){
                //云打印设备
                StringBuilder builder = new StringBuilder();
                if (!TextUtils.isEmpty(dataBean.getFirmName())) {
                    builder.append(dataBean.getFirmName()).append(" ");
                }
                if (!TextUtils.isEmpty(dataBean.getDeviceModel())) {
                    builder.append(dataBean.getDeviceModel());
                }
                mTv_device_name_choice.setText(builder.toString());
                mTv_device_sn_choice.setText("SN："+dataBean.getSn());
                mTv_device_sn_choice.setVisibility(View.VISIBLE);
//                mIv_choice_icon_device.setVisibility(dataBean.isSelect()?View.VISIBLE:View.GONE);
                mIv_choice_icon_device.setImageResource(dataBean.isSelect()?R.mipmap.icon_choice_cloud_print_device:R.mipmap.icon_unchoice_cloud_print_device);
            }else {
                //蓝牙打印设备
                mTv_device_name_choice.setText(dataBean.getBluthPrintName());
                mTv_device_sn_choice.setVisibility(View.GONE);
//                mIv_choice_icon_device.setVisibility(dataBean.isSelect()?View.VISIBLE:View.GONE);
                mIv_choice_icon_device.setImageResource(dataBean.isSelect()?R.mipmap.icon_choice_cloud_print_device:R.mipmap.icon_unchoice_cloud_print_device);

            }
        }
    }

    public List<DeviceBean> getCloudPrintDevices() {
        return mCloudPrintDevices;
    }
}
