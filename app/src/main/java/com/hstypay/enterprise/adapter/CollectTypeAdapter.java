package com.hstypay.enterprise.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;

import java.util.List;

/**
 * Created by admin on 2017/7/8.
 */

public class CollectTypeAdapter extends RecyclerView.Adapter<CollectTypeAdapter.HomeViewHolder> {

    List<ReportBean.DataEntity.ListEntity> mDataBean;


    public CollectTypeAdapter(List<ReportBean.DataEntity.ListEntity> data) {
        this.mDataBean = data;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public CollectTypeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_collect_type, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final CollectTypeAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean == null)
            return;
        ReportBean.DataEntity.ListEntity dataBean = mDataBean.get(position);
        if (position == mDataBean.size() - 1) {
            holder.mView.setVisibility(View.INVISIBLE);
        } else {
            holder.mView.setVisibility(View.VISIBLE);
        }
        if (dataBean != null) {
            switch (dataBean.getApiProvider()) {
                case 1://1:微信;2:支付宝;3:财付通;4:qq钱包;5-银联;99:未知
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_wechat);
                    holder.mTvWachatType.setText("微信支付");
                    break;
                case 2://1:微信;2:支付宝;3:财付通;4:qq钱包;5-银联;99:未知
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_alipay);
                    holder.mTvWachatType.setText("支付宝");
                    break;
                case 4://1:微信;2:支付宝;3:财付通;4:qq钱包;5-银联;99:未知
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_qq);
                    holder.mTvWachatType.setText("QQ钱包");
                    break;
                case 5://1:微信;2:支付宝;3:财付通;4:qq钱包;5-银联;99:未知
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_unionpay);
                    holder.mTvWachatType.setText("银联支付");
                    break;
                case 6://会员卡充值
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_recharge);
                    holder.mTvWachatType.setText("会员卡充值");
                    break;
                case 7:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_vip_recharge);
                    holder.mTvWachatType.setText("人工充值");
                    break;
                case 8:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_vip_verification);
                    holder.mTvWachatType.setText("手动核销");
                    break;
                case 9:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_vip_count_verification);
                    holder.mTvWachatType.setText("次卡核销");
                    break;
                case 10:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_bcard);
                    holder.mTvWachatType.setText("刷卡支付");
                    break;
                case 11://福卡支付
                    holder.mIvWachat.setImageResource(R.mipmap.icon_list_fuka);
                    holder.mTvWachatType.setText("福卡支付");
                    break;
                case 13://银联POS支付(万科)
//                holder.mTvPayType.setText("pos刷脸支付");
                    holder.mIvWachat.setImageResource(R.mipmap.icon_list_pos_union_face);
                    holder.mTvWachatType.setText("银联POS刷脸支付");
                    break;
                case 15://微信银联POS支付(万科)
                    holder.mIvWachat.setImageResource(R.mipmap.icon_list_pos_wechat_face);
                    holder.mTvWachatType.setText("微信POS刷脸支付收款");
                    break;
                case 3://1:微信;2:支付宝;3:财付通;4:qq钱包;5-银联;99:未知
                case 99:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_receivables);
                    holder.mTvWachatType.setText("其他支付");
                    break;
                case Constants.DIGIT_RMB_PAY_TYPE_API_CODE:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                    holder.mTvWachatType.setText("数字人民币");
                    break;
                default:
                    holder.mIvWachat.setImageResource(R.mipmap.icon_general_receivables);
                    holder.mTvWachatType.setText("其他支付");
                    break;
            }
            holder.mTvWachatMoney.setText("金额：" + DateUtil.formatMoneyUtil(dataBean.getSuccessFee() / 100d) + "元");
            holder.mTvWachatNumber.setText("笔数：" + dataBean.getSuccessCount() + "笔");
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvWachatMoney, mTvWachatNumber, mTvWachatType;
        private ImageView mIvWachat;
        private View mView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvWachatMoney = (TextView) itemView.findViewById(R.id.tv_wachat_money);//微信支付总金额
            mTvWachatNumber = (TextView) itemView.findViewById(R.id.tv_wachat_number);//微信支付总笔数
            mIvWachat = (ImageView) itemView.findViewById(R.id.iv_wachat);
            mTvWachatType = (TextView) itemView.findViewById(R.id.tv_wachat_type);
            mView = itemView.findViewById(R.id.v_line);
        }
    }
}
