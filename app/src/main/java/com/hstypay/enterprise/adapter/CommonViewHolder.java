package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * FileName: CommonViewHolder
 * Founder: YuJiaJian
 * User: Administrator
 * Date: 2022年03月
 * Copyright (c) 2022 http://wxdgut.com
 * Email: hello_luyao@163.com
 * Profile:
 */
public class CommonViewHolder extends RecyclerView.ViewHolder {

    //子View的集合
    private SparseArray<View> mViews;
    private View mContentView;

    public CommonViewHolder(View itemView) {
        super(itemView);
        mViews = new SparseArray<>();
        mContentView = itemView;
    }

    /**
     * 获取CommonViewHolder实体
     *
     * @param parent
     * @param layoutId
     * @return
     */
    public static CommonViewHolder getViewHolder(ViewGroup parent, int layoutId) {
        return new CommonViewHolder((View.inflate(parent.getContext(), layoutId, null)));
    }

    /**
     * 提供给外部访问View的方法
     *
     * @param viewId
     * @param <T>
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mContentView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 设置文本
     *
     * @param viewId
     * @param text
     * @return
     */
    public CommonViewHolder setText(int viewId, String text) {
        TextView textView = getView(viewId);
        textView.setText(text);
        return this;
    }

    /**
     * 设置图片
     *
     * @param context
     * @param viewId
     * @param url
     * @return
     */
    public CommonViewHolder setImageUrl(Context context, int viewId, String url) {
        ImageView imageView = getView(viewId);
        Picasso.get().load(url).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(imageView);
        return this;
    }

    /**
     * 设置图片文件
     *
     * @param context
     * @param viewId
     * @param file
     * @return
     */
    public CommonViewHolder setImageFile(Context context, int viewId, File file) {
        ImageView imageView = getView(viewId);
        Picasso.get().load(file).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(imageView);
        return this;
    }

    /**
     * 设置图片
     *
     * @param viewId
     * @param resId
     * @return
     */
    public CommonViewHolder setImageResource(int viewId, int resId) {
        ImageView imageView = getView(viewId);
        imageView.setImageResource(resId);
        return this;
    }

    /**
     * 设置背景颜色
     *
     * @param viewId
     * @param color
     * @return
     */
    public CommonViewHolder setBackgroundColor(int viewId, int color) {
        View view = getView(viewId);
        view.setBackgroundColor(color);
        return this;
    }

    /**
     * 设置文本颜色
     *
     * @param viewId
     * @param color
     * @return
     */
    public CommonViewHolder setTextColor(int viewId, int color) {
        TextView textView = getView(viewId);
        textView.setTextColor(color);
        return this;
    }

    /**
     * 设置控件的显示隐藏
     *
     * @param viewId
     * @param status -1:GONE, 0:INVISIBLE, 1:VISIBLE
     * @return
     */
    public CommonViewHolder setViewVisibility(int viewId, int status) {
        View view = getView(viewId);
        view.setVisibility(status == -1 ? View.GONE : (status == 1 ? View.VISIBLE : View.INVISIBLE));
        return this;
    }
}
