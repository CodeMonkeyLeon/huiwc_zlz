package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.Coupon;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<Coupon> mList;

    public CouponAdapter(Context context, List<Coupon> couponList) {
        this.mContext = context;
        this.mList = couponList;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public CouponAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_coupon_detail, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(CouponAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
        Coupon coupon = mList.get(position);
        holder.mTvCouponName.setText(coupon.getCouponName());
        holder.mTvCouponMoney.setText(UIUtils.getString(R.string.tx_mark_coupon) + DateUtil.formatMoneyUtil(coupon.getParValue() / 100d));
        /*if (position == 0){
            holder.mViewLine.setVisibility(View.GONE);
        }else {
            holder.mViewLine.setVisibility(View.VISIBLE);
        }*/
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvCouponName,mTvCouponMoney;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvCouponName = (TextView) itemView.findViewById(R.id.tv_item_coupon_name);
            mTvCouponMoney = (TextView) itemView.findViewById(R.id.tv_item_coupon_money);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }
}