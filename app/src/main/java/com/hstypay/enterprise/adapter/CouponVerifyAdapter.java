package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoData;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/03/16 20:28
 * @描述: ${TODO}
 */
public class CouponVerifyAdapter extends RecyclerView.Adapter {

    private static final int NORMAL_ITEM = 0;
    private static final int GROUP_ITEM = 1;

    private Context mContext;
    private List<CouponHsInfoData> mDataList;
    private LayoutInflater mLayoutInflater;

    private OnRecyclerViewItemClickListener listener;

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener clickListener) {
        this.listener = clickListener;
    }


    public CouponVerifyAdapter(Context mContext, List<CouponHsInfoData> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == NORMAL_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_verify_view_item, parent, false);
            NormalItemHolder holder = new NormalItemHolder(mContext, itemView, listener);
            return holder;
        } else if (viewType == GROUP_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_verify_view_list, parent, false);
            GroupItemHolder holder = new GroupItemHolder(mContext, itemView, listener);
            return holder;
        }
        return null;//
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (mDataList == null)
            return;
        CouponHsInfoData entity = mDataList.get(position);

        if (null == entity)
            return;

        if (viewHolder instanceof GroupItemHolder) {
            bindGroupItem(entity, (GroupItemHolder) viewHolder);
        } else {
            NormalItemHolder holder = (NormalItemHolder) viewHolder;
            bindNormalItem(entity, holder.mItemTvVerifyTime, holder.mItemTvCouponTitle, holder.mItemTvVerifyStore, holder.mItemIvCouponPic);
        }
    }

    @Override
    public int getItemCount() {
        LogUtil.d("mDataList 的长度：", mDataList.size() + "");
//        if (mDataList.size() == 0)
//            linearLayout.setBackgroundResource(R.mipmap.null_record);
//        else
//            linearLayout.setBackgroundResource(0);
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        //第一个要显示时间
        if (position == 0)
            return GROUP_ITEM;

        String currentDate = DateUtil.formatYYMD(mDataList.get(position).getConsumeTime());
        int prevIndex = position - 1;
        boolean isDifferent = !DateUtil.formatYYMD(mDataList.get(prevIndex).getConsumeTime()).equals(currentDate);
        return isDifferent ? GROUP_ITEM : NORMAL_ITEM;
    }

    void bindNormalItem(CouponHsInfoData entity, TextView mItemTvVerifyTime, TextView mItemTvCouponTitle, TextView mItemTvVerifyStore, ImageView mItemIvCouponPic) {
        mItemTvVerifyTime.setText("核销时间："+DateUtil.formatMMDDTime(entity.getConsumeTime()));
        mItemTvCouponTitle.setText(entity.getActivityName());
        mItemTvVerifyStore.setText("核销门店："+entity.getShopName());
        Picasso.get()
                .load(entity.getActivityPicUrl())
                .placeholder(R.mipmap.icon_coupon_general)
                .error(R.mipmap.icon_coupon_general)
                .into(mItemIvCouponPic);
    }

    void bindGroupItem(CouponHsInfoData entity, GroupItemHolder holder) {
        bindNormalItem(entity, holder.mItemTvVerifyTime, holder.mItemTvCouponTitle, holder.mItemTvVerifyStore, holder.mItemIvCouponPic);
        holder.mItemTvDate.setText(DateUtil.formatMMDDNew(entity.getConsumeTime()));
    }

    public class NormalItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mItemTvVerifyTime, mItemTvCouponTitle, mItemTvVerifyStore;
        public ImageView mItemIvCouponPic;

        private OnRecyclerViewItemClickListener listener;
        private Context context;

        public NormalItemHolder(Context context, View itemView, OnRecyclerViewItemClickListener listener) {
            super(itemView);
            this.context = context;
            this.listener = listener;
            mItemTvVerifyTime = itemView.findViewById(R.id.item_tv_verify_time);
            mItemTvCouponTitle = itemView.findViewById(R.id.item_tv_coupon_title);
            mItemTvVerifyStore = itemView.findViewById(R.id.item_tv_verify_store);
            mItemIvCouponPic = itemView.findViewById(R.id.item_iv_coupon_pic);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
//                itemView.setBackgroundResource(R.drawable.recycler_bg);
                listener.onItemClick(getAdapterPosition());
            }
        }
    }

    public class GroupItemHolder extends NormalItemHolder {

        public TextView mItemTvDate;
        public TextView mItemTvVerifyTime, mItemTvCouponTitle, mItemTvVerifyStore;
        public ImageView mItemIvCouponPic;

        public GroupItemHolder(Context context, View itemView, OnRecyclerViewItemClickListener listener) {
            super(context, itemView, listener);
            mItemTvDate = (TextView) itemView.findViewById(R.id.item_tv_date);
            mItemTvVerifyTime = itemView.findViewById(R.id.item_tv_verify_time);
            mItemTvCouponTitle = itemView.findViewById(R.id.item_tv_coupon_title);
            mItemTvVerifyStore = itemView.findViewById(R.id.item_tv_verify_store);
            mItemIvCouponPic = itemView.findViewById(R.id.item_iv_coupon_pic);
        }
    }
}