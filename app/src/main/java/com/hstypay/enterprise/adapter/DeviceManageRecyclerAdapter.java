package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.DeviceManagerBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */
public class DeviceManageRecyclerAdapter extends RecyclerView.Adapter<DeviceManageRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<DeviceManagerBean> mDataBean;

    public DeviceManageRecyclerAdapter(Context context, List<DeviceManagerBean> data) {
        this.mContext = context;
        this.mDataBean = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public DeviceManageRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_device_manage, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final DeviceManageRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean == null){
            return;
        }
        DeviceManagerBean dataBean = mDataBean.get(position);
        holder.mIvDevice.setImageResource(dataBean.getImageRes());
        holder.mTvDevice.setText(dataBean.getFuncNameRes());
        holder.mTvDeviceInstruction.setText(dataBean.getFuncInstructionRes());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDevice,mTvDeviceInstruction;
        private ImageView mIvDevice;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mIvDevice = itemView.findViewById(R.id.iv_device);
            mTvDevice = itemView.findViewById(R.id.tv_device);
            mTvDeviceInstruction = itemView.findViewById(R.id.tv_device_instruction);
        }
    }

}