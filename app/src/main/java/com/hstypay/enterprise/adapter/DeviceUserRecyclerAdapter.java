package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.LinkEmployeeBean;

import java.util.List;
import java.util.TreeMap;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: 门店多选适配器
 */

public class DeviceUserRecyclerAdapter extends RecyclerView.Adapter<DeviceUserRecyclerAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<LinkEmployeeBean.DataEntity> mList;
    private TreeMap<String, String> mStoreMap;

    public DeviceUserRecyclerAdapter(Context context, List<LinkEmployeeBean.DataEntity> list) {
        this.mContext = context;
        this.mList = list;

    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(String userId);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public DeviceUserRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_cloud_user, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final DeviceUserRecyclerAdapter.HomeViewHolder holder, final int position) {
        final LinkEmployeeBean.DataEntity dataEntity = mList.get(position);

        if (dataEntity != null) {
            holder.mTvManagerMark.setVisibility(dataEntity.getRole() == 4 ? View.VISIBLE : View.GONE);
            holder.mTvUserName.setText(dataEntity.getName());
            holder.mTvUserPhone.setText(dataEntity.getPhone());
            if (dataEntity.getBind() == 1) {
                holder.mIvUserChoice.setVisibility(View.VISIBLE);
            } else {
                holder.mIvUserChoice.setVisibility(View.INVISIBLE);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(dataEntity.getUserId());
                }
            }
        });

        if (position == mList.size() - 1) {
            holder.mView.setVisibility(View.INVISIBLE);
        } else {
            holder.mView.setVisibility(View.VISIBLE);
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {

        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvUserName, mTvUserPhone, mTvManagerMark;
        private ImageView mIvUserChoice;
        private View mView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvManagerMark = (TextView) itemView.findViewById(R.id.tv_manager_mark);
            mTvUserName = (TextView) itemView.findViewById(R.id.tv_cloud_user_name);
            mTvUserPhone = (TextView) itemView.findViewById(R.id.tv_cloud_user_telephone);
            mIvUserChoice = (ImageView) itemView.findViewById(R.id.iv_shop_choice);
            mView = itemView.findViewById(R.id.view_line);
        }
    }

    public void setView(DeviceUserRecyclerAdapter.HomeViewHolder holder, int bind) {

        if (bind == 1) {
            holder.mIvUserChoice.setVisibility(View.VISIBLE);
        } else {
            holder.mIvUserChoice.setVisibility(View.INVISIBLE);
        }
    }

}