package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.CloudDevicesBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class DynamicCodeAdapter extends RecyclerView.Adapter<DynamicCodeAdapter.HomeViewHolder> {
    private Context mContext;
    private List<CloudDevicesBean.DataBeanX.DataBean> mList;
    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public DynamicCodeAdapter(Context context, List<CloudDevicesBean.DataBeanX.DataBean> list) {
        this.mContext = context;
        this.mList = list;
    }


    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public DynamicCodeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_dynamic_code, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final DynamicCodeAdapter.HomeViewHolder holder, final int position) {
        if (mList!=null && mList.size()>0){
            holder.mTvDeviceStore.setText(mList.get(position).getStoreMerchantIdCnt());
            holder.mTvDeviceNo.setText("设备SN码:"+mList.get(position).getSn());
            if (TextUtils.isEmpty(mList.get(position).getUserIdCnt())){
                holder.mTvDeviceType.setVisibility(View.GONE);
            }else {
                holder.mTvDeviceType.setVisibility(View.VISIBLE);
                holder.mTvDeviceType.setText("所属员工:" + mList.get(position).getUserIdCnt());
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            });
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDeviceStore,mTvDeviceType,mTvDeviceNo;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvDeviceStore = (TextView) itemView.findViewById(R.id.tv_device_store);
            mTvDeviceType = (TextView) itemView.findViewById(R.id.tv_device_type);
            mTvDeviceNo = (TextView) itemView.findViewById(R.id.tv_device_no);
        }
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}