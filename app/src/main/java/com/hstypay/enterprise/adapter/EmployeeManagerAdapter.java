package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.viewholder.EmployeeManagerViewHolder;
import com.hstypay.enterprise.bean.EmployeeManagerBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 员工管理页adapter
 */
public class EmployeeManagerAdapter extends RecyclerView.Adapter {
    private List<EmployeeManagerBean.EmployeeManagerData> mEmployeeManageItemBeanList = new ArrayList<>();
    private Context mContext;
    public EmployeeManagerAdapter(Context context){
        mContext = context;
    }
    public void setData(List<EmployeeManagerBean.EmployeeManagerData> employeeManageItemBeans){
        mEmployeeManageItemBeanList.clear();
        notifyDataSetChanged();
        if (employeeManageItemBeans!=null && employeeManageItemBeans.size()>0){
            mEmployeeManageItemBeanList.addAll(employeeManageItemBeans);
            notifyDataSetChanged();
        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new EmployeeManagerViewHolder( mContext,LayoutInflater.from(mContext).inflate(R.layout.item_employee_manage, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        EmployeeManagerViewHolder employeeManagerViewHolder = (EmployeeManagerViewHolder) viewHolder;
        employeeManagerViewHolder.setData(mEmployeeManageItemBeanList.get(position));
        employeeManagerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnRecycleItemClickListener!=null){
                    mOnRecycleItemClickListener.onItemClick(mEmployeeManageItemBeanList.get(employeeManagerViewHolder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEmployeeManageItemBeanList.size();
    }

    public interface OnRecycleItemClickListener{
        void onItemClick(EmployeeManagerBean.EmployeeManagerData employeeManageItemBean);
    }
    private OnRecycleItemClickListener mOnRecycleItemClickListener;

    public void setOnRecycleItemClickListener(OnRecycleItemClickListener onRecycleItemClickListener) {
        mOnRecycleItemClickListener = onRecycleItemClickListener;
    }
}
