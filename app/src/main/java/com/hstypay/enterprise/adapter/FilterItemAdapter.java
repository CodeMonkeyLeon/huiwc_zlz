package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.CheckBean;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class FilterItemAdapter extends RecyclerView.Adapter<FilterItemAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<CheckBean> mList;

    public FilterItemAdapter(Context context, List<CheckBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position,boolean isChecked);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public FilterItemAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_filter, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(FilterItemAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mItemCb.setText(mList.get(position).getName());
        holder.mItemCb.setChecked(mList.get(position).isChecked());
        if (mList.get(position).isCheckAble()) {
            holder.mItemCb.setTextColor(UIUtils.getCheckebleColor(R.drawable.checkbox_tv_color));
            holder.mItemCb.setEnabled(true);
        } else {
            holder.mItemCb.setTextColor(UIUtils.getColor(R.color.tv_no_pay));
            holder.mItemCb.setEnabled(false);
        }
        holder.mItemCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position,isChecked);
                }
            }
        });
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private CheckBox mItemCb;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mItemCb = itemView.findViewById(R.id.item_cb);
        }
    }
}