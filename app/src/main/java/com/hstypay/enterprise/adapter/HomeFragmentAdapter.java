package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentBaseViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentFunctionViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentHeaderViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentMerchantPagerViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentNoticeViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentReceiveListViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentReceiveTableViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentTradeViewHolder;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.bean.MchtalkBean;
import com.hstypay.enterprise.bean.MessageData;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.ReportTradeTrendBean;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.utils.DensityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc HomeFragment adapter
 */
public class HomeFragmentAdapter extends RecyclerView.Adapter<HomeFragmentBaseViewHolder> {
    public static final int VIEW_TYPE_HEADER = 1;//头部
    public static final int VIEW_TYPE_TRADE = 2;//交易概览
    public static final int VIEW_TYPE_NOTICE = 3;//通知
    public static final int VIEW_TYPE_FUNCTION = 4;//功能菜单
    public static final int VIEW_TYPE_RECEIVE_LIST = 5;//收款记录
    public static final int VIEW_TYPE_RECEIVE_TABLE = 6;//收款统计
    public static final int VIEW_TYPE_MERCHANT_PAGER = 7;//商家说
    private Context mContext;
    private MenuItem.OnHomeMenuItemClickListener mOnHomeMenuItemClickListener;//功能菜单点击事件
    private HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener mOnHomeReceiveItemClickListener;//收款记录条目点击事件
    private HomeFragmentHeaderViewHolder.OnHomeHeaderIconClickListener mOnHomeHeaderIconClickListener;//头部三个图标的点击事件
    private HomeFragmentNoticeViewHolder.OnNoticeClickListener mOnNoticeClickListener;//通知栏的点击事件
    private HomeFragmentReceiveTableViewHolder.OnReceiveStatictisMoreClickListener mOnReceiveStatictisMoreClickListener;//收款统计查看更多
    private HomeFragmentMerchantPagerViewHolder.OnHomeMchTalkItemClickListener mOnHomeMchTalkItemClickListener;//商家说条码点击事件
    private HomeFragmentTradeViewHolder.OnTradeItemClickListener mOnTradeItemClickListener;//交易详情点击事件
    private boolean showReceiveEmpty = false;//收款记录是否显示空视图里的文字，避免一开始就显示出一个空数据的UI

    public HomeFragmentAdapter(Context context) {
        mContext = context;
    }

    private List<Integer> mViewTypeList = new ArrayList<>();//多少种view

    public void setViewTypeList(List<Integer> viewTypeList) {
        mViewTypeList.clear();
        notifyDataSetChanged();
        if (viewTypeList != null && viewTypeList.size() > 0) {
            mViewTypeList.addAll(viewTypeList);
            notifyDataSetChanged();
        }
    }

    public void setOnHomeHeaderIconClickListener(HomeFragmentHeaderViewHolder.OnHomeHeaderIconClickListener onHomeHeaderIconClickListener) {
        mOnHomeHeaderIconClickListener = onHomeHeaderIconClickListener;
    }

    public void setOnHomeMenuItemClickListener(MenuItem.OnHomeMenuItemClickListener onHomeMenuItemClickListener) {
        mOnHomeMenuItemClickListener = onHomeMenuItemClickListener;
    }

    public void setOnHomeReceiveItemClickListener(HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener onHomeReceiveItemClickListener) {
        mOnHomeReceiveItemClickListener = onHomeReceiveItemClickListener;
    }

    public void setOnNoticeClickListener(HomeFragmentNoticeViewHolder.OnNoticeClickListener onNoticeClickListener) {
        mOnNoticeClickListener = onNoticeClickListener;
    }

    public void setOnReceiveStatictisMoreClickListener(HomeFragmentReceiveTableViewHolder.OnReceiveStatictisMoreClickListener onReceiveStatictisMoreClickListener) {
        mOnReceiveStatictisMoreClickListener = onReceiveStatictisMoreClickListener;
    }

    public void setOnHomeMchTalkItemClickListener(HomeFragmentMerchantPagerViewHolder.OnHomeMchTalkItemClickListener onHomeMchTalkItemClickListener) {
        mOnHomeMchTalkItemClickListener = onHomeMchTalkItemClickListener;
    }

    public void setOnTradeItemClickListener(HomeFragmentTradeViewHolder.OnTradeItemClickListener onTradeItemClickListener) {
        mOnTradeItemClickListener = onTradeItemClickListener;
    }

    private String headerName;//头部商户数据

    public void setHeaderData(String name) {
        headerName = name;
        notifyItemChanged(findItemPosition(VIEW_TYPE_HEADER));
    }

    private int mVisibility;//头部商户数据

    public void setVisible(int visibility) {
        mVisibility = visibility;
        notifyItemChanged(findItemPosition(VIEW_TYPE_HEADER));
    }

    private ReportBean mReportBean;//今日交易数据

    public void setTodayTradeData(ReportBean reportBean) {
        mReportBean = reportBean;
        notifyItemChanged(findItemPosition(VIEW_TYPE_TRADE));
    }

    List<MessageData.DataEntity.DataList> messageDatas;//消息通知数据

    public void setMessageData(List<MessageData.DataEntity.DataList> messageDatas) {
        this.messageDatas = messageDatas;
        notifyItemChanged(findItemPosition(VIEW_TYPE_NOTICE));
    }

    List<List<MenuItem>> mLlMenuBean = new ArrayList<>();//功能菜单数据

    public void setLlMenuBean(List<List<MenuItem>> llMenuBean) {
        mLlMenuBean.clear();
        notifyItemChanged(findItemPosition(VIEW_TYPE_FUNCTION));
        if (llMenuBean != null && llMenuBean.size() > 0) {
            mLlMenuBean.addAll(llMenuBean);
            notifyItemChanged(findItemPosition(VIEW_TYPE_FUNCTION));
        }
    }

    List<BillsBean.DataEntity> mBillList = new ArrayList<>();//收款记录数据

    public void setBillList(List<BillsBean.DataEntity> billList, boolean showReceiveEmpty) {
        this.showReceiveEmpty = showReceiveEmpty;
        mBillList.clear();
        notifyItemChanged(findItemPosition(VIEW_TYPE_RECEIVE_LIST));
        if (billList != null && billList.size() > 0) {
            mBillList.addAll(billList);
            notifyItemChanged(findItemPosition(VIEW_TYPE_RECEIVE_LIST));
        }
    }

    private ReportTradeTrendBean mReportTradeTrendBean;//收款统计数据

    public void setReportTradeTrendData(ReportTradeTrendBean reportTradeTrendBean) {
        mReportTradeTrendBean = reportTradeTrendBean;
        notifyItemChanged(findItemPosition(VIEW_TYPE_RECEIVE_TABLE));
    }

    List<MchtalkBean.MchTalkItemBean> mMerChantNewsItemBeans = new ArrayList<>();//商家说数据

    public void setMerChantNewsItemBeans(List<MchtalkBean.MchTalkItemBean> merChantNewsItemBeans) {
        mMerChantNewsItemBeans.clear();
        notifyDataSetChanged();
        if (merChantNewsItemBeans != null && merChantNewsItemBeans.size() > 0) {
            mMerChantNewsItemBeans.addAll(merChantNewsItemBeans);
            notifyItemChanged(findItemPosition(VIEW_TYPE_MERCHANT_PAGER));
        }
    }

    @NonNull
    @Override
    public HomeFragmentBaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        HomeFragmentBaseViewHolder viewHolder = null;
        if (viewType == VIEW_TYPE_HEADER) {
            viewHolder = new HomeFragmentHeaderViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_header, viewGroup, false));
        } else if (viewType == VIEW_TYPE_TRADE) {
            viewHolder = new HomeFragmentTradeViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_trade_overview, viewGroup, false));

        } else if (viewType == VIEW_TYPE_NOTICE) {
            viewHolder = new HomeFragmentNoticeViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_notice, viewGroup, false));

        } else if (viewType == VIEW_TYPE_FUNCTION) {
            viewHolder = new HomeFragmentFunctionViewHolder(mContext, LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_function, viewGroup, false));

        } else if (viewType == VIEW_TYPE_RECEIVE_LIST) {
            viewHolder = new HomeFragmentReceiveListViewHolder(mContext, LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_receive_recoders, viewGroup, false));

        } else if (viewType == VIEW_TYPE_RECEIVE_TABLE) {
            viewHolder = new HomeFragmentReceiveTableViewHolder(mContext, LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_receive_table, viewGroup, false));

        } else if (viewType == VIEW_TYPE_MERCHANT_PAGER) {
            viewHolder = new HomeFragmentMerchantPagerViewHolder(mContext, LayoutInflater.from(mContext).inflate(R.layout.item_home_fragment_merchant_news, viewGroup, false));
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeFragmentBaseViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == VIEW_TYPE_HEADER) {
            //头部
            viewHolder.setData(headerName);
            HomeFragmentHeaderViewHolder homeFragmentHeaderViewHolder = (HomeFragmentHeaderViewHolder) viewHolder;
            homeFragmentHeaderViewHolder.setOnHomeHeaderIconClickListener(mOnHomeHeaderIconClickListener);
            homeFragmentHeaderViewHolder.setVisible(mVisibility);
        } else if (viewType == VIEW_TYPE_TRADE) {
            //交易数据
            viewHolder.setData(mReportBean);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnTradeItemClickListener != null) {
                        mOnTradeItemClickListener.onTradeItemClick();
                    }
                }
            });

        } else if (viewType == VIEW_TYPE_NOTICE) {
            //通知
            viewHolder.setData(messageDatas);
            if (messageDatas != null && messageDatas.size() > 0) {
                HomeFragmentNoticeViewHolder homeFragmentNoticeViewHolder = (HomeFragmentNoticeViewHolder) viewHolder;
                View.OnClickListener l = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnNoticeClickListener != null) {
                            mOnNoticeClickListener.onNoticeClick();
                        }
                    }
                };
                homeFragmentNoticeViewHolder.mIvArrowNoticeHome.setOnClickListener(l);
                homeFragmentNoticeViewHolder.mLlHomeMsg.setOnClickListener(l);
            }
        } else if (viewType == VIEW_TYPE_FUNCTION) {
            //功能菜单
            viewHolder.setData(mLlMenuBean);
            HomeFragmentFunctionViewHolder homeFragmentFunctionViewHolder = (HomeFragmentFunctionViewHolder) viewHolder;
            homeFragmentFunctionViewHolder.setOnHomeMenuItemClickListener(mOnHomeMenuItemClickListener);
            int noticeItemPosition = findItemPosition(VIEW_TYPE_NOTICE);
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) homeFragmentFunctionViewHolder.itemView.getLayoutParams();
            if (noticeItemPosition == -1) {
                //通知栏不存在，调整菜单栏的位置
                layoutParams.topMargin = DensityUtils.dip2px(mContext, -10);
            } else {
                layoutParams.topMargin = DensityUtils.dip2px(mContext, 0);
            }
            homeFragmentFunctionViewHolder.itemView.setLayoutParams(layoutParams);

        } else if (viewType == VIEW_TYPE_RECEIVE_LIST) {
            //收款记录
            HomeFragmentReceiveListViewHolder homeFragmentReceiveListViewHolder = (HomeFragmentReceiveListViewHolder) viewHolder;
            homeFragmentReceiveListViewHolder.setShowEmpty(showReceiveEmpty);
            homeFragmentReceiveListViewHolder.setData(mBillList);
            homeFragmentReceiveListViewHolder.setOnHomeReceiveItemClickListener(mOnHomeReceiveItemClickListener);

        } else if (viewType == VIEW_TYPE_RECEIVE_TABLE) {
            //收款统计表
            viewHolder.setData(mReportTradeTrendBean);
            HomeFragmentReceiveTableViewHolder tableViewHolder = (HomeFragmentReceiveTableViewHolder) viewHolder;
            tableViewHolder.mRlMoreItemTableHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnReceiveStatictisMoreClickListener.onMoreReceiveClick();
                }
            });

        } else if (viewType == VIEW_TYPE_MERCHANT_PAGER) {
            //商家说
            viewHolder.setData(mMerChantNewsItemBeans);
            HomeFragmentMerchantPagerViewHolder merchantPagerViewHolder = (HomeFragmentMerchantPagerViewHolder) viewHolder;
            merchantPagerViewHolder.setOnHomeMchTalkItemClickListener(mOnHomeMchTalkItemClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return mViewTypeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mViewTypeList.get(position);
    }


    public int findItemPosition(int viewType) {
        int position = -1;
        for (int i = 0; i < mViewTypeList.size(); i++) {
            int type = mViewTypeList.get(i);
            if (type == viewType) {
                position = i;
            }
        }
        return position;//若为-1表示该viewType对应的item不存在
    }
}
