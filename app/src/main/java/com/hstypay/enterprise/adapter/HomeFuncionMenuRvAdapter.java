package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.utils.DensityUtils;
import com.hstypay.enterprise.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/11
 * @desc 首页功能菜单recycleview的adapter
 */
public class HomeFuncionMenuRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private MenuItem.OnHomeMenuItemClickListener mOnHomeMenuItemClickListener;

    private List<MenuItem> mHomeMenuBeans = new ArrayList<>();

    public HomeFuncionMenuRvAdapter(Context context) {
        mContext = context;
    }

    public void setOnHomeMenuItemClickListener(MenuItem.OnHomeMenuItemClickListener onHomeMenuItemClickListener) {
        mOnHomeMenuItemClickListener = onHomeMenuItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_home_menu, viewGroup, false);
        int screenWidth = ScreenUtils.getScreenWidth(mContext);
        int width = ( screenWidth - ( DensityUtils.dip2px(mContext , 12) * 2 ) ) / 5;
        ViewGroup.LayoutParams  lp = itemView.getLayoutParams();
        lp.width = width;
        itemView.setLayoutParams(lp);
        HomeMenuViewHolder homeMenuViewHolder = new HomeMenuViewHolder(mContext,itemView);
        return homeMenuViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        HomeMenuViewHolder homeMenuViewHolder = (HomeMenuViewHolder) viewHolder;
        homeMenuViewHolder.setData(mHomeMenuBeans.get(position));
        homeMenuViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeMenuItemClickListener!=null){
                    mOnHomeMenuItemClickListener.onMenuItemClick(mHomeMenuBeans.get(viewHolder.getAdapterPosition()));
                }
            }
        });
        if (position==0){
            viewHolder.itemView.setId(R.id.id_menu_function_first);
        }
    }

    @Override
    public int getItemCount() {
        return mHomeMenuBeans.size();
    }

    public void setData(List<MenuItem> homeMenuBeans) {
        mHomeMenuBeans.clear();
        notifyDataSetChanged();
        if (homeMenuBeans!=null && homeMenuBeans.size()>0){
            mHomeMenuBeans.addAll(homeMenuBeans);
            notifyDataSetChanged();
        }
    }


    static class HomeMenuViewHolder extends RecyclerView.ViewHolder{
        private Context mContext;
        private ImageView mIvItemMenuHome;
        private TextView mTvItemMenuHome;
        private LinearLayout itemView;
        private ImageView mIvItemFlagMenuHome;

        public HomeMenuViewHolder(Context context, @NonNull View itemView) {
            super(itemView);
            mContext = context;
            this.itemView = (LinearLayout)itemView;
            mIvItemMenuHome = itemView.findViewById(R.id.iv_item_menu_home);
            mTvItemMenuHome = itemView.findViewById(R.id.tv_item_menu_home);
            mIvItemFlagMenuHome = itemView.findViewById(R.id.iv_item_flag_menu_home);
        }


        public void setData(MenuItem menuItem) {
            if (menuItem==null){return;}
            mIvItemMenuHome.setImageResource(MenuHelper.getMenuImgResource(menuItem.getId()));
            mTvItemMenuHome.setText(menuItem.getName());
            MenuHelper.setMenuRightMarkIcon(menuItem,mIvItemFlagMenuHome);
        }
    }
}
