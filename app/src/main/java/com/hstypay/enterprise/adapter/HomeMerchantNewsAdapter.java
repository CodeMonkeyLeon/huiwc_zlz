package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.NiceImageView;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentMerchantPagerViewHolder;
import com.hstypay.enterprise.bean.MchtalkBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/12
 * @desc 商家说adapter
 */
public class HomeMerchantNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<MchtalkBean.MchTalkItemBean> mMerChantNewsItemBeans = new ArrayList<>();
    private HomeFragmentMerchantPagerViewHolder.OnHomeMchTalkItemClickListener mOnHomeMchTalkItemClickListener;
    private int mPageType;// 0是首页的商家说，1是商家说首页
    public void setOnHomeMchTalkItemClickListener(HomeFragmentMerchantPagerViewHolder.OnHomeMchTalkItemClickListener onHomeMchTalkItemClickListener) {
        mOnHomeMchTalkItemClickListener = onHomeMchTalkItemClickListener;
    }

    public HomeMerchantNewsAdapter(Context context) {
        mContext = context;
    }
    public void setData(List<MchtalkBean.MchTalkItemBean>  merChantNewsItemBeans,int pageType) {
        mPageType = pageType;
        mMerChantNewsItemBeans.clear();
        notifyDataSetChanged();
        if (merChantNewsItemBeans!=null&&merChantNewsItemBeans.size()>0){
            mMerChantNewsItemBeans.addAll(merChantNewsItemBeans);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_home_merchant_news, viewGroup, false);
        MerchantNewsViewHolder newsViewHolder = new MerchantNewsViewHolder(view);
        return newsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        MerchantNewsViewHolder newsViewHolder = (MerchantNewsViewHolder) viewHolder;
        newsViewHolder.setData(position,mMerChantNewsItemBeans.get(viewHolder.getAdapterPosition()),mPageType);
        newsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeMchTalkItemClickListener!=null){
                    mOnHomeMchTalkItemClickListener.onMchTalkItemClick(mMerChantNewsItemBeans.get(viewHolder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMerChantNewsItemBeans.size();
    }


    class MerchantNewsViewHolder extends RecyclerView.ViewHolder{

        private  TextView mTvTitleMerchantNewsHome;
        private  TextView mTvReadValue;
        private  TextView mTvDianZanValue;
        private  NiceImageView mNivMerchantNewsHome;
        private  View mViewLineMerchantNews;
        private  TextView mTvDateMchTalk;//日期

        public MerchantNewsViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvTitleMerchantNewsHome = itemView.findViewById(R.id.tv_title_merchant_news_home);
            mTvReadValue = itemView.findViewById(R.id.tv_read_value);
            mTvDianZanValue = itemView.findViewById(R.id.tv_dian_zan_value);
            mNivMerchantNewsHome = itemView.findViewById(R.id.niv_merchant_news_home);
            mTvDateMchTalk = itemView.findViewById(R.id.tv_date_mch_talk);
            mViewLineMerchantNews = itemView.findViewById(R.id.view_line_merchant_news);
        }

        public void setData(int position, MchtalkBean.MchTalkItemBean merChantNewsItemBean,int pageType) {
            if (merChantNewsItemBean==null)return;
            if (position==getItemCount()-1 && pageType!=1){
                mViewLineMerchantNews.setVisibility(View.GONE);
            }else {
                mViewLineMerchantNews.setVisibility(View.VISIBLE);
            }

            mTvTitleMerchantNewsHome.setText(merChantNewsItemBean.getTitle());
            mTvReadValue.setText(merChantNewsItemBean.getReadTotal());
            mTvDianZanValue.setText(merChantNewsItemBean.getLikeTotal());
            String imageUrl = Constants.H5_BASE_URL + merChantNewsItemBean.getCoverImg();
            Picasso.get().load(imageUrl).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mNivMerchantNewsHome);

            if (pageType==1){
                //商家说首页
                mTvDateMchTalk.setVisibility(View.VISIBLE);
                String publishTime = merChantNewsItemBean.getPublishTime();
                String timeDot = DateUtil.formartYMDToYYMDDot(publishTime);
                mTvDateMchTalk.setText(timeDot);
            }else {
                mTvDateMchTalk.setVisibility(View.GONE);
            }
        }
    }
}
