package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentReceiveListViewHolder;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * author: kuangzeyu2019
 * date: 2021/3/11
 * desc: 首页收款记录的adapter
 */
public class HomeReceiveRecordsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<BillsBean.DataEntity> mBillList = new ArrayList<>();
    private HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener mOnHomeReceiveItemClickListener;

    public void setOnHomeReceiveItemClickListener(HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener onHomeReceiveItemClickListener) {
        mOnHomeReceiveItemClickListener = onHomeReceiveItemClickListener;
    }

    public HomeReceiveRecordsAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_home_receive_record_home, viewGroup, false);
        HomeReceiveRecordViewHolder viewHolder = new HomeReceiveRecordViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        HomeReceiveRecordViewHolder recordViewHolder = (HomeReceiveRecordViewHolder) viewHolder;
        recordViewHolder.setData(mBillList.get(position),position);
        recordViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeReceiveItemClickListener!=null){
                    mOnHomeReceiveItemClickListener.onReceiveRecordItemClick(mBillList.get(viewHolder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBillList.size();
    }

    public void setData(List<BillsBean.DataEntity> billList) {
        mBillList.clear();
        notifyDataSetChanged();
        if (billList!=null && billList.size()>0){
            mBillList.addAll(billList);
            notifyDataSetChanged();
        }
    }


    public class HomeReceiveRecordViewHolder extends RecyclerView.ViewHolder{

        private ImageView mIv_icon_receive_type_home;//支付类型的图标
        private TextView mTv_pay_type_receive_record_home;//支付类型名称
        private TextView mTv_zjyb_receive_record_home;//最近一笔
        private TextView mTv_date_receive_record_home;//日期时间
        private TextView mTv_value_money_receive_record_home;//交易金额
        private TextView mTv_pay_status_receive_record_home;//支付状态

        public HomeReceiveRecordViewHolder(@NonNull View itemView) {
            super(itemView);
            mIv_icon_receive_type_home = itemView.findViewById(R.id.iv_icon_receive_type_home);
            mTv_pay_type_receive_record_home = itemView.findViewById(R.id.tv_pay_type_receive_record_home);
            mTv_zjyb_receive_record_home = itemView.findViewById(R.id.tv_zjyb_receive_record_home);
            mTv_date_receive_record_home = itemView.findViewById(R.id.tv_date_receive_record_home);
            mTv_value_money_receive_record_home = itemView.findViewById(R.id.tv_value_money_receive_record_home);
            mTv_pay_status_receive_record_home = itemView.findViewById(R.id.tv_pay_status_receive_record_home);
        }

        public void setData(BillsBean.DataEntity dataEntity, int position) {
            //最近一笔
            if (position==0){
                mTv_zjyb_receive_record_home.setVisibility(View.VISIBLE);
            }else {
                mTv_zjyb_receive_record_home.setVisibility(View.GONE);
            }

            //交易时间
            String tradeTime = dataEntity.getTradeTime();
            if (!TextUtils.isEmpty(tradeTime)){
                try {
                    String s = DateUtil.formartDateToYYMMDDHHMMSS(tradeTime);
                    mTv_date_receive_record_home.setText(s);
                } catch (ParseException e) {
                    e.printStackTrace();
                    mTv_date_receive_record_home.setText(" ");
                }
            }else {
                mTv_date_receive_record_home.setText(" ");
            }

            //交易金额和支付状态
            switch (dataEntity.getRefundStatus()) {
                case 1://退款中
                case 6:
                    mTv_pay_status_receive_record_home.setText(UIUtils.getString(R.string.tv_refund_state1));
                    mTv_value_money_receive_record_home.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));
                    break;
                case 2://退款成功
                    mTv_pay_status_receive_record_home.setTextColor(UIUtils.getColor(R.color.tv_success_color));
                    mTv_value_money_receive_record_home.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));
                    break;
                case 3://退款失败
                    mTv_pay_status_receive_record_home.setText(UIUtils.getString(R.string.tv_refund_state3));
                    mTv_value_money_receive_record_home.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));
                    break;
                case 4://未确定
                    mTv_pay_status_receive_record_home.setText(UIUtils.getString(R.string.tv_refund_state4));
                    mTv_value_money_receive_record_home.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));
                    break;
                case 5://转入代发
                    mTv_pay_status_receive_record_home.setText(UIUtils.getString(R.string.tv_refund_state5));
                    mTv_value_money_receive_record_home.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundMoney() / 100d));
                    break;
                default:
                    mTv_value_money_receive_record_home.setText( DateUtil.formatMoneyUtil(dataEntity.getPayMoney() / 100d));
                    if (dataEntity.getTradeStatus() == 1) {//未支付
                        mTv_pay_status_receive_record_home.setText(UIUtils.getString(R.string.tv_trade_state1));
                    }else {
                        mTv_pay_status_receive_record_home.setText(OrderStringUtil.getTradeStateString(dataEntity.getTradeState()));
                    }
                    break;
            }
            if (dataEntity.getApiProvider() == Constants.ApiProviderCode.VIP_COUNT_VERIFICATION) {
                mTv_value_money_receive_record_home.setVisibility(View.INVISIBLE);
            } else {
                mTv_value_money_receive_record_home.setVisibility(View.VISIBLE);
            }

            //支付类型
            switch (dataEntity.getApiProvider()) {
                case Constants.ApiProviderCode.WX://微信
                    if (dataEntity.getTradeType() == 1) {
                        mTv_pay_type_receive_record_home.setText("会员卡充值");
                    } else {
                        mTv_pay_type_receive_record_home.setText("微信支付收款");
                    }
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_wechat);
                    break;
                case Constants.ApiProviderCode.ZFB://支付宝
                    mTv_pay_type_receive_record_home.setText("支付宝收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_alipay);
                    break;
                case Constants.ApiProviderCode.QQ://qq钱包
                    mTv_pay_type_receive_record_home.setText("QQ钱包收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_qq);
                    break;
                case Constants.ApiProviderCode.UNIONPAY://银联
                    mTv_pay_type_receive_record_home.setText("银联支付收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_unionpay);
                    break;
                case Constants.ApiProviderCode.MEMBER://会员卡
                    mTv_pay_type_receive_record_home.setText("会员卡消费");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_member);
                    break;
                case Constants.ApiProviderCode.VIP_RECHARGE://人工充值
                    mTv_pay_type_receive_record_home.setText("人工充值");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_vip_recharge);
                    break;
                case Constants.ApiProviderCode.VIP_VERIFICATION://手动核销
                    mTv_pay_type_receive_record_home.setText("会员卡核销");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_vip_verification);
                    break;
                case Constants.ApiProviderCode.VIP_COUNT_VERIFICATION://次卡核销
                    mTv_pay_type_receive_record_home.setText("次卡核销");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_vip_count_verification);
                    break;
                case Constants.ApiProviderCode.CARD_PAY://刷卡支付
                    mTv_pay_type_receive_record_home.setText("刷卡支付收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_bcard);
                    break;
                case Constants.ApiProviderCode.FUKA://福卡支付
                    mTv_pay_type_receive_record_home.setText("福卡支付");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_list_fuka);
                    break;
                case Constants.ApiProviderCode.POS_UNION_FACE://银联POS支付(万科)
                    if (dataEntity.getApiCode().equals(Constants.WECHAT_FACEPAY)){
                        mTv_pay_type_receive_record_home.setText("微信POS刷脸支付收款");
                    }else {
                        mTv_pay_type_receive_record_home.setText("银联POS刷脸支付");
                    }
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_list_pos_union_face);
                    break;
                case Constants.ApiProviderCode.POS_WECHAT_FACE://微信银联POS支付(万科)
                    mTv_pay_type_receive_record_home.setText("微信POS刷脸支付收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_list_pos_wechat_face);
                    break;
                case Constants.ApiProviderCode.DIGITAL_RMB:
                    mTv_pay_type_receive_record_home.setText("数字人民币收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                    break;
                case Constants.ApiProviderCode.OTHER_3://财付通
                case Constants.ApiProviderCode.OTHER_9://未知
                default:
                    mTv_pay_type_receive_record_home.setText("其他支付收款");
                    mIv_icon_receive_type_home.setImageResource(R.mipmap.icon_general_receivables);
                    break;
            }
        }

    }
}
