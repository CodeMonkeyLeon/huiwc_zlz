package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.HbfqRateBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class HuabeiRateAdapter extends RecyclerView.Adapter<HuabeiRateAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<HbfqRateBean.DataBean> mDataBean;
    private int layoutPosition;
    private String mHbFqNum;

    public HuabeiRateAdapter(Context context, List<HbfqRateBean.DataBean> dataBean, String hbFqNum) {
        this.mContext = context;
        this.mDataBean = dataBean;
        this.mHbFqNum = hbFqNum;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public HuabeiRateAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_huabei_rate, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final HuabeiRateAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            }
        });
        holder.mTvMoney.setText("¥" + mDataBean.get(position).getPerAmount() + "X" + mDataBean.get(position).getHbfqNum() + "期");
        if (mDataBean.get(position).getUnionFeeFlag() && mDataBean.get(position).getUnionUserFee() > 0) {
            holder.mIvInterestFree.setVisibility(View.VISIBLE);
            SpannableStringBuilder spannableString = new SpannableStringBuilder();
            spannableString.append("含服务费¥" + DateUtil.formatMoneyByLong(mDataBean.get(position).getUnionUserFee()) + " (已减免¥" + DateUtil.formatMoneyByLong(mDataBean.get(position).getUnionMchFee()) + ")");
            if (mHbFqNum.equals(mDataBean.get(position).getHbfqNum())) {
                ForegroundColorSpan blueSpan = new ForegroundColorSpan(UIUtils.getColor(R.color.theme_color));
                spannableString.setSpan(blueSpan, 4, 5 + DateUtil.formatMoneyByLong(mDataBean.get(position).getUnionUserFee()).length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                holder.mTvRate.setText(spannableString);
            } else {
                ForegroundColorSpan redSpan = new ForegroundColorSpan(UIUtils.getColor(R.color.tv_notice_red));
                spannableString.setSpan(redSpan, 4, 5 + DateUtil.formatMoneyByLong(mDataBean.get(position).getUnionUserFee()).length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                holder.mTvRate.setText(spannableString);
            }
            holder.mIvInterestFree.setImageResource(R.mipmap.ic_interest_discount);
        } else {
            if (mDataBean.get(position).isFreeFee()) {
                holder.mIvInterestFree.setImageResource(R.mipmap.ic_interest_free);
                holder.mIvInterestFree.setVisibility(View.VISIBLE);
                if (mDataBean.get(position).getTotalFee() != null) {
                    holder.mTvRate.setText("含手续费¥" + DateUtil.formatMoneyByLong(mDataBean.get(position).getTotalFee().longValue()));
                }else {
                    holder.mTvRate.setText("含手续费");
                }
            } else {
                holder.mIvInterestFree.setVisibility(View.GONE);
                if (mDataBean.get(position).getTotalFee() != null){
                    holder.mTvRate.setText("含手续费¥" + DateUtil.formatMoneyByLong(mDataBean.get(position).getTotalFee().longValue()) + " (¥" + mDataBean.get(position).getPerFee() + "x" + mDataBean.get(position).getHbfqNum() + "期" + ")");
                } else {
                    holder.mTvRate.setText("含手续费" + " (¥" + mDataBean.get(position).getPerFee() + "x" + mDataBean.get(position).getHbfqNum() + "期" + ")");
                }
            }
        }

        if (mHbFqNum.equals(mDataBean.get(position).getHbfqNum())) {
            holder.mRlHbfqDetail.setBackgroundResource(R.drawable.shape_rectangle_blue_four);
            holder.mTvMoney.setTextColor(UIUtils.getColor(R.color.theme_color));
            holder.mTvRate.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            holder.mRlHbfqDetail.setBackgroundResource(R.drawable.shape_rectangle_white_strike_four);
            holder.mTvMoney.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvRate.setTextColor(UIUtils.getColor(R.color.home_btn_text));
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null)
            return mDataBean.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvMoney, mTvRate;
        private ImageView mIvInterestFree;
        private RelativeLayout mRlHbfqDetail;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvMoney = (TextView) itemView.findViewById(R.id.tv_money);
            mTvRate = (TextView) itemView.findViewById(R.id.tv_rate);
            mIvInterestFree = (ImageView) itemView.findViewById(R.id.iv_interest_free);
            mRlHbfqDetail = itemView.findViewById(R.id.rl_hbfq_detail);
        }
    }
}