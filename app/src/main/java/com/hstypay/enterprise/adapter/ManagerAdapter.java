package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.CashierManageBean;
import com.hstypay.enterprise.bean.ManagerListBean;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoresBean;
import com.hstypay.enterprise.utils.StringUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: 店长管理
 */
public class ManagerAdapter extends RecyclerView.Adapter<ManagerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<ManagerListBean.DataEntity.itemData> mDataBean;

    public ManagerAdapter(Context context, List<ManagerListBean.DataEntity.itemData> data) {
        this.mContext = context;
        this.mDataBean = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ManagerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_manager, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final ManagerAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean != null) {
            if (StringUtils.getStrLength(mDataBean.get(position).getEmpName()) > 26) {
                holder.mTvManagerName.setText(StringUtils.splitStr(mDataBean.get(position).getEmpName(), 24) + "...");
            } else {
                holder.mTvManagerName.setText(mDataBean.get(position).getEmpName());
            }
            String storeName = getStoreName(mDataBean.get(position).getStores());
            if (StringUtils.getStrLength(storeName) > 26){
                holder.mTvStoreName.setText(StringUtils.splitStr(storeName, 24)+"...");
            }else {
                holder.mTvStoreName.setText(storeName);
            }
            holder.mTvTelephone.setText(StringUtils.hideTelephone(mDataBean.get(position).getMobile()));
            if (position == mDataBean.size()-1){
                holder.mViewLine.setVisibility(View.INVISIBLE);
            }else{
                holder.mViewLine.setVisibility(View.VISIBLE);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            });
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvManagerName,mTvStoreName,mTvTelephone;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvTelephone = itemView.findViewById(R.id.tv_telephone);
            mTvManagerName = itemView.findViewById(R.id.tv_manager_name);
            mTvStoreName = itemView.findViewById(R.id.tv_store_name);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

    private String getStoreName(List<StoresBean> stores) {
        if (stores == null || stores.size() == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stores.size(); i++) {
            if (i == stores.size() - 1) {
                sb.append(stores.get(i).getMerchantName());
            } else {
                sb.append(stores.get(i).getMerchantName() + "/");
            }
        }
        return sb.toString();
    }
}