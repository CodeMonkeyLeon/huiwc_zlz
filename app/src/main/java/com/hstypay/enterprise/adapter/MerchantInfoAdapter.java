package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.MerchantIdBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.UIUtils;

import java.text.ParseException;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class MerchantInfoAdapter extends RecyclerView.Adapter<MerchantInfoAdapter.HomeViewHolder> {

    private Context mContext;
    private List<MerchantIdBean.DataEntity> mData;
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private int layoutPosition = -1;

    public MerchantInfoAdapter(Context context,List<MerchantIdBean.DataEntity> data) {
        this.mContext = context;
        this.mData = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public MerchantInfoAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_merchant_info, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final MerchantInfoAdapter.HomeViewHolder holder, final int position) {
        if (mData==null)
            return;
//        holder.mLlMerchantInfo.setBackground(stateListDrawable);
        holder.mTvMerchantDate.setText(UIUtils.getString(R.string.tv_merchant_item_date)+DateUtil.formartDateToYYMMDD(mData.get(position).getCreateTime()));
        holder.mTvMerchantName.setText(UIUtils.getString(R.string.tv_merchant_item_name)+mData.get(position).getOrgName());
        holder.mTvMerchantId.setText(UIUtils.getString(R.string.tv_merchant_item_id)+mData.get(position).getOrgId());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener!=null){
                    mOnItemClickListener.onItemClick(position);
                    layoutPosition = holder.getLayoutPosition();
                    notifyDataSetChanged();
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mData != null)
            return mData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvMerchantName,mTvMerchantId,mTvMerchantDate;
        private LinearLayout mLlMerchantInfo;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvMerchantName = (TextView) itemView.findViewById(R.id.tv_item_merchant_name);
            mTvMerchantId = (TextView) itemView.findViewById(R.id.tv_item_merchant_id);
            mTvMerchantDate = (TextView) itemView.findViewById(R.id.tv_item_merchant_date);
            mLlMerchantInfo = (LinearLayout) itemView.findViewById(R.id.ll_merchant_info);
        }
    }
}