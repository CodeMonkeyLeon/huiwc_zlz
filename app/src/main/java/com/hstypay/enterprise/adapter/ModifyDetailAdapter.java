package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ModifyRecordBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class ModifyDetailAdapter extends RecyclerView.Adapter<ModifyDetailAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<ModifyRecordBean.DataBean.RecordList> mList;

    public ModifyDetailAdapter(Context context, List<ModifyRecordBean.DataBean.RecordList> list) {
        this.mContext = context;
        this.mList = list;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ModifyDetailAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_modify_detail, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(ModifyDetailAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mTvModifyTitle.setText(mList.get(position).getUpdateTime() + " | " + mList.get(position).getUpdateUserName());
        String string1 = "将";
        String string2 = "由";
        String string3 = "改为";
        String proName;
        if (TextUtils.isEmpty(mList.get(position).getProNameCnt())){
            proName = "未知名称";
        } else {
            proName = mList.get(position).getProNameCnt();
        }
        if (!TextUtils.isEmpty(mList.get(position).getAfter())
                && (mList.get(position).getAfter().toLowerCase().contains(".jpg")
                || mList.get(position).getAfter().toLowerCase().contains(".png")
                || mList.get(position).getAfter().toLowerCase().contains(".bmp")
                || mList.get(position).getAfter().toLowerCase().contains(".gif"))) {
            //修改记录是图片
            SpannableStringBuilder spannableString = new SpannableStringBuilder();
            spannableString.append(string1).append(proName);
            spannableString.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.tv_merchant_item_data)), string1.length(), spannableString.toString().length() , Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            holder.mTvModifyText.setText(spannableString);
            holder.mLlModifyPhoto.setVisibility(View.VISIBLE);
            Picasso.get().load(Constants.H5_BASE_URL + mList.get(position).getBefore()).error(R.mipmap.icon_general_noloading).into(holder.mIvPhotoBefore);
            Picasso.get().load(Constants.H5_BASE_URL + mList.get(position).getAfter()).error(R.mipmap.icon_general_noloading).into(holder.mIvPhotoAfter);
        } else {
            SpannableStringBuilder spannableString = new SpannableStringBuilder();
            String before;
            if (TextUtils.isEmpty(mList.get(position).getBefore())){
                before = "无";
            } else {
                before = mList.get(position).getBefore();
            }
            String after;
            if (TextUtils.isEmpty(mList.get(position).getAfter())){
                after = "无";
            } else {
                after = mList.get(position).getAfter();
            }
            spannableString.append(string1).append(proName).append(string2).append(before).append(string3).append(after);
            int index1 = spannableString.toString().indexOf(string1);
            int index2 = spannableString.toString().indexOf(string2);
            int index3 = spannableString.toString().indexOf(string3);
            spannableString.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.tv_merchant_item_data)), index1+string1.length(), index2, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.tv_merchant_item_data)), index2+string2.length(), index3, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.tv_merchant_item_data)), index3+string3.length(), spannableString.toString().length() , Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            holder.mTvModifyText.setText(spannableString);
            holder.mLlModifyPhoto.setVisibility(View.GONE);
        }
        if (position == mList.size() - 1) {
            holder.mViewLine.setVisibility(View.GONE);
        } else {
            holder.mViewLine.setVisibility(View.VISIBLE);
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvModifyTitle, mTvModifyText;
        private LinearLayout mLlModifyPhoto;
        private ImageView mIvPhotoBefore, mIvPhotoAfter;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvModifyTitle = itemView.findViewById(R.id.tv_modify_title);
            mTvModifyText = itemView.findViewById(R.id.tv_modify_text);
            mLlModifyPhoto = itemView.findViewById(R.id.ll_modify_photo);
            mIvPhotoBefore = itemView.findViewById(R.id.iv_photo_before);
            mIvPhotoAfter = itemView.findViewById(R.id.iv_photo_after);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }
}