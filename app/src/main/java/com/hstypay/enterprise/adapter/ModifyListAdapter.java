package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.Coupon;
import com.hstypay.enterprise.bean.ModifyRecordBean;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class ModifyListAdapter extends RecyclerView.Adapter<ModifyListAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<ModifyRecordBean.DataBean.RecordList> mList;

    public ModifyListAdapter(Context context, List<ModifyRecordBean.DataBean.RecordList> couponList) {
        this.mContext = context;
        this.mList = couponList;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ModifyListAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_modify_list, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(ModifyListAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mTvModifyTime.setText(mList.get(position).getUpdateTime());
        switch (mList.get(position).getState()) {
            case 1:
                holder.mTvModifyStatus.setText("变更成功");
                holder.mTvModifyStatus.setTextColor(UIUtils.getColor(R.color.home_value_text));
                break;
            case 2:
                holder.mTvModifyStatus.setText("变更失败");
                holder.mTvModifyStatus.setTextColor(UIUtils.getColor(R.color.tv_notice_red));
                break;
            case 11:
                holder.mTvModifyStatus.setText("变更审核中");
                holder.mTvModifyStatus.setTextColor(UIUtils.getColor(R.color.home_value_text));
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null)
                    mOnItemClickListener.onItemClick(position);
            }
        });
        if (position == mList.size() - 1) {
            holder.mViewLine.setVisibility(View.GONE);
        } else {
            holder.mViewLine.setVisibility(View.VISIBLE);
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvModifyTime, mTvModifyStatus;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvModifyTime = itemView.findViewById(R.id.tv_modify_time);
            mTvModifyStatus = itemView.findViewById(R.id.tv_modify_status);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }
}