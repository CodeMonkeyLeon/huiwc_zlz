package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.MessageData;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 18:34
 * @描述: ${TODO}
 */

public class MsgRecyclerAdapter extends RecyclerView.Adapter<MsgRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private List<MessageData.DataEntity.DataList> mMsgList;
    private Context mContext;

    public MsgRecyclerAdapter(Context context,List<MessageData.DataEntity.DataList> msgList){
        this.mMsgList = msgList;
        this.mContext = context;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public MsgRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_message, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final MsgRecyclerAdapter.HomeViewHolder holder, final int position) {
        if(mMsgList==null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                    holder.mIvState.setVisibility(View.GONE);
                }
            }
        });
        if (position == mMsgList.size()-1){
            holder.mIvLine.setVisibility(View.INVISIBLE);
        }else{
            holder.mIvLine.setVisibility(View.VISIBLE);
        }
        holder.mTvMessageTitle.setText(mMsgList.get(position).getTitle());
        holder.mTvMessageDate.setText(mMsgList.get(position).getCreateTime());
        if(mMsgList.get(position).getIsRead()==0){
            holder.mIvState.setVisibility(View.VISIBLE);
        }else{
            //其它则已读
            holder.mIvState.setVisibility(View.GONE);
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mMsgList!=null){
            return mMsgList.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvMessageTitle, mTvMessageDate;
        private ImageView mIvMessageChoiced,mIvMessageIcon,mIvState;
        private View mIvLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvMessageTitle = (TextView) itemView.findViewById(R.id.tv_message_title);
            mTvMessageDate = (TextView) itemView.findViewById(R.id.tv_message_date);
            mIvMessageChoiced = (ImageView) itemView.findViewById(R.id.iv_message_choice);
            mIvMessageIcon = (ImageView) itemView.findViewById(R.id.iv_message_icon);
            mIvLine = (View) itemView.findViewById(R.id.view_line);
            mIvState = (ImageView) itemView.findViewById(R.id.iv_state);
        }
    }
}
