package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;
import java.util.TreeMap;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: 门店多选适配器
 */

public class MultiShopRecyclerAdapter extends RecyclerView.Adapter<MultiShopRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<StoreListBean.DataEntity> mList;
    private TreeMap<String,String> mStoreMap;

    public MultiShopRecyclerAdapter(Context context, List<StoreListBean.DataEntity> list, TreeMap<String,String> storeMap) {
        this.mContext=context;
        this.mList=list;
        this.mStoreMap = storeMap;

    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public MultiShopRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_shop, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final MultiShopRecyclerAdapter.HomeViewHolder holder, final int position) {
        if(mList == null)
            return;
        final StoreListBean.DataEntity dataEntity = mList.get(position);

        if(dataEntity!=null){
            holder.mTvShopName.setText(dataEntity.getStoreName());
            setView(holder,dataEntity.isSelcet());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("zhouwei--",position+"====");
                if (dataEntity.isSelcet()) {
                    dataEntity.setSelcet(false);
                }else{
                    dataEntity.setSelcet(true);
                }
                setView(holder,dataEntity.isSelcet());
                if(mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });

        if (mStoreMap != null && mStoreMap.size() > 0) {
            for (String storeId : mStoreMap.keySet()) {
                if (mList.get(position).getStoreId().equals(storeId)) {
                    holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
                    holder.mIvShopChoiced.setVisibility(View.VISIBLE);
                    mList.get(position).setSelcet(true);
                }
            }
        }
        if(position == mList.size()-1){
            holder.mView.setVisibility(View.INVISIBLE);
        }else{
            holder.mView.setVisibility(View.VISIBLE);
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {

        if (mList !=null && mList.size()>0) {
            return mList.size();
        }else{

        return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvShopName;
        private ImageView mIvShopChoiced;
        private View mView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvShopName = (TextView) itemView.findViewById(R.id.tv_shop_choice);
            mIvShopChoiced = (ImageView) itemView.findViewById(R.id.iv_shop_choice);
            mView = itemView.findViewById(R.id.view_line);
        }
    }

    public void setView(MultiShopRecyclerAdapter.HomeViewHolder holder,boolean isSelected){

        if(isSelected){
            holder.mIvShopChoiced.setVisibility(View.VISIBLE);
            holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
        }else{
            holder.mIvShopChoiced.setVisibility(View.GONE);
            holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.black));
        }
    }

    /*public void setToggle(MultiShopRecyclerAdapter.HomeViewHolder holder,boolean isSelected,StoreListBean.DataEntity dataEntity){
        dataEntity.setSelcet(!isSelected) ;
    }*/

}