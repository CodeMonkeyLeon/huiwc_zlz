package com.hstypay.enterprise.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.hstypay.enterprise.factory.FragmentFactory;
import com.hstypay.enterprise.fragment.HolderFragment;

import java.util.HashMap;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 11:14
 * @描述: ${TODO}
 */

public class NavAdapter extends FragmentStatePagerAdapter {

    private HashMap<Integer, HolderFragment> mFragments;

    public NavAdapter(FragmentManager fm,HashMap<Integer,HolderFragment> fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    @Override
    public Fragment getItem(final int position) {

        HolderFragment fragment = new HolderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        fragment.setArguments(bundle);
        mFragments.put(position, fragment);

        return fragment;
    }

    @Override
    public int getCount() {
        return FragmentFactory.getFragmentCount();
    }
}
