package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.VoiceBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class OrderShopAdapter extends RecyclerView.Adapter<OrderShopAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    Context mContext;
    List<VoiceBean.DataBean> mList;
    private String mPushClose;

    public OrderShopAdapter(Context context, List<VoiceBean.DataBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public OrderShopAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_order_shop, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final OrderShopAdapter.HomeViewHolder holder, final int position) {
        if (mList == null || mList.size() == 0){
            return;
        }
        final VoiceBean.DataBean dataBean = mList.get(position);
        if (dataBean != null) {
            holder.mTvVoiceShop.setText(dataBean.getMerchantName());
            mPushClose = dataBean.getPushClose() + "";
            if (mPushClose.equals("1")) {
                //开
                holder.mIvVoiceShop.setImageResource(R.mipmap.ic_switch_open);
            } else {
                holder.mIvVoiceShop.setImageResource(R.mipmap.ic_switch_close);

            }
        }
        holder.mViewLine.setVisibility(position == mList.size()-1 ? View.INVISIBLE : View.VISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataBean.getPushClose() == 1) {
                    dataBean.setPushClose(0);
                } else {
                    dataBean.setPushClose(1);
                }
                setView(holder, dataBean.getPushClose(), dataBean);

                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvVoiceShop;
        private ImageView mIvVoiceShop;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvVoiceShop = (TextView) itemView.findViewById(R.id.tv_voice_shop);
            mIvVoiceShop = (ImageView) itemView.findViewById(R.id.iv_voice_shop);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

    public void setView(OrderShopAdapter.HomeViewHolder holder, int close, VoiceBean.DataBean dataEntity) {
        if (dataEntity == null) {
            holder.mIvVoiceShop.setImageResource(R.mipmap.ic_switch_open);
        } else {
            if (close == 0) {
                holder.mIvVoiceShop.setImageResource(R.mipmap.ic_switch_close);
            } else {
                holder.mIvVoiceShop.setImageResource(R.mipmap.ic_switch_open);
            }

        }
    }
}