package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.FunctionBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */
public class PayAdapter extends RecyclerView.Adapter<PayAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<FunctionBean> mList;

    public PayAdapter(Context context, List<FunctionBean> data) {
        this.mContext = context;
        this.mList = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public PayAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_pay, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final PayAdapter.HomeViewHolder holder, final int position) {
        if (mList == null || mList.size() == 0){
            return;
        }
        holder.mIvPayType.setImageResource(mList.get(position).getImageRes());
        holder.mTvPayType.setText(mList.get(position).getFuncName());
        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(mList.get(position).getFunction());
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvPayType;
        private ImageView mIvPayType;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mIvPayType = itemView.findViewById(R.id.iv_pay_type);
            mTvPayType = itemView.findViewById(R.id.tv_pay_type);
        }
    }

}