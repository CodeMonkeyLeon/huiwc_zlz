package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * Created by admin on 2017/7/4.
 * girdview
 */

class PayTypeAdapter extends BaseAdapter{

    int[] images = new int[]{
            R.mipmap.icon_general_qrcode,
            R.mipmap.icon_general_cashier,
            R.mipmap.icon_general_expect
    };
    String[] titles = new String[] {"店铺二维码","收银员管理","敬请期待"};
    Context mContext;

    public PayTypeAdapter(Context context) {

        this.mContext=context;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        ViewPayTypeHolder payholder;
        if (convertView == null)
        {
            convertView = View.inflate(mContext, R.layout.activity_grid_pay_list_item, null);
            payholder = new ViewPayTypeHolder();
            payholder.iv_icon = (ImageView)convertView.findViewById(R.id.iv_icon);
            payholder.tv_item = (TextView)convertView.findViewById(R.id.tv_item);
            payholder.ly_grid_item = (LinearLayout)convertView.findViewById(R.id.ly_grid_item);
            convertView.setTag(payholder);
        }
        else
        {
            payholder = (ViewPayTypeHolder)convertView.getTag();
        }
        payholder.iv_icon.setImageResource(images[position]);
        payholder.tv_item.setText(titles[position]);

        return convertView;
    }


    class ViewPayTypeHolder {
        ImageView iv_icon;
        LinearLayout ly_grid_item;
        private TextView tv_item;
    }
}
