package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.text.ParseException;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class PledgeBillRecyclerAdapter extends RecyclerView.Adapter<PledgeBillRecyclerAdapter.HomeViewHolder> {

    private Context mContext;
    private List<PledgeBillsBean.DataBean> mBillList;

    public PledgeBillRecyclerAdapter(Context context, List<PledgeBillsBean.DataBean> billList) {
        this.mContext = context;
        this.mBillList = billList;
    }

    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public PledgeBillRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_bill, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(PledgeBillRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mBillList == null || mBillList.size()==0)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
        PledgeBillsBean.DataBean dataEntity = mBillList.get(position);
        setTradeTypeView(dataEntity, holder);
        setView(dataEntity, holder);
        if (dataEntity.getAuthNo() != null) {
            if (dataEntity.getAuthNo().length() >= 4) {
                holder.mTvBillNo.setText(dataEntity.getAuthNo().substring(dataEntity.getAuthNo().length() - 4));
            } else {
                holder.mTvBillNo.setText(dataEntity.getAuthNo());
            }
        }
        if (dataEntity.getApiProvider()==9) {
            holder.mTvPayMoney.setVisibility(View.INVISIBLE);
        } else {
            holder.mTvPayMoney.setVisibility(View.VISIBLE);
        }
        if (position == mBillList.size()-1){
            holder.mViewLine.setVisibility(View.INVISIBLE);
        }else {
            holder.mViewLine.setVisibility(View.VISIBLE);
        }
    }

    public void setTradeTypeView(PledgeBillsBean.DataBean dataEntity, PledgeBillRecyclerAdapter.HomeViewHolder holder) {
        switch (dataEntity.getApiProvider()) {
            case 1://微信
                holder.mTvPayType.setText("微信押金收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_wechat);
                break;
            case 2://支付宝
                holder.mTvPayType.setText("支付宝押金收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_alipay);
                break;
            case 4://qq钱包
                holder.mTvPayType.setText("QQ钱包押金收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_qq);
                break;
            case 5://银联
                holder.mTvPayType.setText("银联押金收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_unionpay);
                break;
            case 6://会员卡
                holder.mTvPayType.setText("会员卡消费");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_member);
                break;
            case 7://人工充值
                holder.mTvPayType.setText("人工充值");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_recharge);
                break;
            case 8://手动核销
                holder.mTvPayType.setText("会员卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_verification);
                break;
            case 9://次卡核销
                holder.mTvPayType.setText("次卡核销");
                holder.mIvPayType.setImageResource(R.mipmap.icon_vip_count_verification);
                break;
            case 10://刷卡支付
                holder.mTvPayType.setText("刷卡押金收款");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_bcard);
                break;
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE:
                holder.mIvPayType.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                holder.mTvPayType.setText("数字人民币");
                break;
            case 3://财付通
            case 99://未知
            default:
                holder.mTvPayType.setText("其他");
                holder.mIvPayType.setImageResource(R.mipmap.icon_general_receivables);
                break;
        }
    }

    public void setView(PledgeBillsBean.DataBean dataEntity, PledgeBillRecyclerAdapter.HomeViewHolder holder) {
        if (!TextUtils.isEmpty(dataEntity.getTradeTime())) {
            try {
                holder.mTvBillDate.setText(DateUtil.formartDateToHHMMSS(dataEntity.getTradeTime()) + "  订单后四位:");
            } catch (ParseException e) {
                holder.mTvBillDate.setText("  订单后四位:");
                e.printStackTrace();
            }
        }else if (!TextUtils.isEmpty(dataEntity.getCreateTime())) {
            try {
                holder.mTvBillDate.setText(DateUtil.formartDateToHHMMSS(dataEntity.getCreateTime()) + "  订单后四位:");
            } catch (ParseException e) {
                holder.mTvBillDate.setText("  订单后四位:");
                e.printStackTrace();
            }
        }else {
            holder.mTvBillDate.setText("  订单后四位:");
        }
        holder.mTvPayMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(Utils.Long.tryParse(dataEntity.getMoney(),-1) / 100d));
        if ("1".equals(dataEntity.getTradeState())) {//未支付
            holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state1));
            holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.home_value_text));
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);//饱和度 0灰色 100过度彩色，50正常
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.mIvPayType.setColorFilter(filter);
        } else {
            switch (dataEntity.getTradeState()) {
                case "2"://支付成功
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_success_color));
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_pledge_trade_state2));
                    break;
                case "8"://已冲正
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state8));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                case "3"://已关闭
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state3));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                case "4"://转入退款
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state4));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
                    break;
                case "45"://已解冻
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state4));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_text));
                    break;
                case "9"://已撤销
//                    holder.mTvPayState.setText(UIUtils.getString(R.string.tv_trade_state9));
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
                default:
                    holder.mTvPayState.setTextColor(UIUtils.getColor(R.color.home_value_text));
                    break;
            }
            holder.mTvPayState.setText(OrderStringUtil.getPledgeStateString(dataEntity.getTradeState()));
            holder.mTvPayMoney.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvBillNo.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvBillDate.setTextColor(UIUtils.getColor(R.color.home_value_text));
            holder.mTvPayType.setTextColor(UIUtils.getColor(R.color.home_text));
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(.7f);//饱和度 0灰色 100过度彩色，50正常
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            holder.mIvPayType.setColorFilter(filter);
        }
    }
    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mBillList != null)
            return mBillList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvPayType, mTvPayMoney, mTvPayState, mTvBillDate, mTvBillNo;
        private ImageView mIvPayType;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvPayType = (TextView) itemView.findViewById(R.id.item_tv_pay_type);
            mTvPayMoney = (TextView) itemView.findViewById(R.id.item_tv_pay_money);
            mTvBillDate = (TextView) itemView.findViewById(R.id.item_tv_bill_date);
            mTvBillNo = (TextView) itemView.findViewById(R.id.item_tv_bill_no);
            mTvPayState = (TextView) itemView.findViewById(R.id.item_tv_pay_state);
            mIvPayType = (ImageView) itemView.findViewById(R.id.item_iv_pay_type);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}