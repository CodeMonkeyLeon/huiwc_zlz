package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.RateBean;
import com.hstypay.enterprise.bean.UnionRateBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DensityUtils;
import com.hstypay.enterprise.utils.UIUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class RateRecyclerAdapter extends RecyclerView.Adapter<RateRecyclerAdapter.HomeViewHolder> {

    private Context mContext;
    private List<RateBean.DataEntity.RateListEntity> mList;
    private RateUnionAdapter mRateUnionAdapter;

    public RateRecyclerAdapter(Context context, List<RateBean.DataEntity.RateListEntity> list) {
        this.mContext = context;
        this.mList = list;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public RateRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_rate, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final RateRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        RateBean.DataEntity.RateListEntity dataEntity = mList.get(position);
        if (dataEntity.getApiProvider() == 5) {
            List<UnionRateBean> unionRates = new ArrayList<>();
            if (!TextUtils.isEmpty(dataEntity.getRate())) {
                UnionRateBean unionRateBean1 = new UnionRateBean();
                unionRateBean1.setUnionRate(dataEntity.getRate());
                unionRateBean1.setUnionRateString("借记卡单笔金额≤1000元");
                unionRates.add(unionRateBean1);
            }
            if (!TextUtils.isEmpty(dataEntity.getRate2())) {
                UnionRateBean unionRateBean2 = new UnionRateBean();
                unionRateBean2.setUnionRate(dataEntity.getRate2());
                unionRateBean2.setUnionRateString("借记卡单笔金额>1000元");
                unionRates.add(unionRateBean2);
            }
            if (!TextUtils.isEmpty(dataEntity.getRate3())) {
                UnionRateBean unionRateBean3 = new UnionRateBean();
                unionRateBean3.setUnionRate(dataEntity.getRate3());
                unionRateBean3.setUnionRateString("贷记卡单笔金额≤1000元");
                unionRates.add(unionRateBean3);
            }
            if (!TextUtils.isEmpty(dataEntity.getRate4())) {
                UnionRateBean unionRateBean4 = new UnionRateBean();
                unionRateBean4.setUnionRate(dataEntity.getRate4());
                unionRateBean4.setUnionRateString("贷记卡单笔金额>1000元");
                unionRates.add(unionRateBean4);
            }
            CustomLinearLayoutManager unionLayoutManager = new CustomLinearLayoutManager(mContext);
            holder.mRvUnionRate.setLayoutManager(unionLayoutManager);
            mRateUnionAdapter = new RateUnionAdapter(MyApplication.getContext(), unionRates);
            holder.mRvUnionRate.setAdapter(mRateUnionAdapter);
            holder.mTvSignRate.setPadding(0, 0, 0, 0);
            holder.mTvSignRate.setText(UIUtils.getString(R.string.tv_union_pay_rate));
            holder.mIvUnionForward.setVisibility(View.VISIBLE);
            holder.mIvUnionForward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mList == null || mList.size() == 0) {
                        setUnionRecyclerView(false, holder);
                    } else {
                        setUnionRecyclerView(true, holder);
                    }
                }
            });
        } else if (dataEntity.getApiProvider() == 10) {
            List<UnionRateBean> unionRates = new ArrayList<>();
            if (!TextUtils.isEmpty(dataEntity.getRate())) {
                UnionRateBean unionRateBean1 = new UnionRateBean();
                unionRateBean1.setUnionRate(DateUtil.formatMoneyUtil(new BigDecimal(dataEntity.getRate()).divide(BigDecimal.valueOf(100d)).doubleValue()));
                unionRateBean1.setUnionRateString("借记卡单笔封顶手续费（元）");
                unionRates.add(unionRateBean1);
            }
            if (!TextUtils.isEmpty(dataEntity.getRate2())) {
                UnionRateBean unionRateBean2 = new UnionRateBean();
                unionRateBean2.setUnionRate(dataEntity.getRate2());
                unionRateBean2.setUnionRateString("借记卡");
                unionRates.add(unionRateBean2);
            }
            if (!TextUtils.isEmpty(dataEntity.getRate3())) {
                UnionRateBean unionRateBean3 = new UnionRateBean();
                unionRateBean3.setUnionRate(dataEntity.getRate3());
                unionRateBean3.setUnionRateString("贷记卡");
                unionRates.add(unionRateBean3);
            }
            CustomLinearLayoutManager unionLayoutManager = new CustomLinearLayoutManager(mContext);
            holder.mRvUnionRate.setLayoutManager(unionLayoutManager);
            mRateUnionAdapter = new RateUnionAdapter(MyApplication.getContext(), unionRates);
            holder.mRvUnionRate.setAdapter(mRateUnionAdapter);
            holder.mTvSignRate.setPadding(0, 0, 0, 0);
            holder.mTvSignRate.setText(UIUtils.getString(R.string.tv_union_pay_rate));
            holder.mIvUnionForward.setVisibility(View.VISIBLE);
            holder.mIvUnionForward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mList == null || mList.size() == 0) {
                        setUnionRecyclerView(false, holder);
                    } else {
                        setUnionRecyclerView(true, holder);
                    }
                }
            });
        } else {
            holder.mTvSignRate.setPadding(0, 0, DensityUtils.dip2px(mContext, 18), 0);
            holder.mTvSignRate.setText(dataEntity.getRate());
            holder.mIvUnionForward.setVisibility(View.GONE);
        }
        setView(dataEntity.getApiProvider(), holder);
    }

    public void setView(int payType, RateRecyclerAdapter.HomeViewHolder holder) {
        switch (payType) {
            case 1://微信
                holder.mTvSignType.setText("微信支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_wechat);
                break;
            case 2://支付宝
                holder.mTvSignType.setText("支付宝");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_alipay);
                break;
            case 4://qq钱包
                holder.mTvSignType.setText("QQ钱包");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_qq);
                break;
            case 5://银联
                holder.mTvSignType.setText("银联支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_unionpay);
                break;
            case 6://会员卡充值
                holder.mTvSignType.setText("会员卡充值");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_member);
                break;
            case 10://刷卡支付
                holder.mTvSignType.setText("刷卡支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_bcard);
                break;
            case 11://福卡支付
                holder.mTvSignType.setText("福卡支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_list_fuka);
                break;
            case 13://银联POS支付(万科)
                holder.mTvSignType.setText("银联POS刷脸支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_list_pos_union_face);
                break;
            case 14://
                holder.mTvSignType.setText("猫酷支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_receivables);
                break;
            case 15://微信银联POS支付(万科)
                holder.mIvSignType.setImageResource(R.mipmap.icon_list_pos_wechat_face);
                holder.mTvSignType.setText("微信POS刷脸支付收款");
                break;
            case 16://美团点评支付(万科)
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_receivables);
                holder.mTvSignType.setText("美团点评支付");
                break;
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE:
                holder.mIvSignType.setImageResource(R.mipmap.icon_paytype_digital_rmb);
                holder.mTvSignType.setText("数字人民币");
                break;
            case 3://财付通
            case 99://未知
            default:
                holder.mTvSignType.setText("其他支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_receivables);
                break;

        }
    }

    private void setUnionRecyclerView(boolean hasRate, HomeViewHolder holder) {
        if (hasRate) {
            if (holder.mRvUnionRate.getVisibility() == View.VISIBLE) {
                holder.mRvUnionRate.setVisibility(View.GONE);
                holder.mIvUnionForward.setImageResource(R.mipmap.ic_arrow_down);
            } else if (holder.mRvUnionRate.getVisibility() == View.GONE) {
                holder.mRvUnionRate.setVisibility(View.VISIBLE);
                holder.mIvUnionForward.setImageResource(R.mipmap.ic_arrow_up);
            }
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvSignType, mTvSignRate;
        private ImageView mIvSignType, mIvUnionForward;
        private RecyclerView mRvUnionRate;
        private View mLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvSignType = (TextView) itemView.findViewById(R.id.item_tv_sign_type);
            mTvSignRate = (TextView) itemView.findViewById(R.id.item_tv_sign_rate);
            mIvSignType = (ImageView) itemView.findViewById(R.id.item_iv_sign_type);
            mIvUnionForward = (ImageView) itemView.findViewById(R.id.iv_union_pay_forward);
            mRvUnionRate = (RecyclerView) itemView.findViewById(R.id.rv_union_rate);
            mLine = itemView.findViewById(R.id.v_line);
        }
    }

}