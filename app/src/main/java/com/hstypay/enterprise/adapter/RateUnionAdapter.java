package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.UnionRateBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class RateUnionAdapter extends RecyclerView.Adapter<RateUnionAdapter.HomeViewHolder> {

    private Context mContext;
    private List<UnionRateBean> mList;

    public RateUnionAdapter(Context context, List<UnionRateBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public RateUnionAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_rate_union, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(RateUnionAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        UnionRateBean unionRateBean = mList.get(position);
        holder.mTvUnionRate.setText(unionRateBean.getUnionRate());
        holder.mTvUnionType.setText(unionRateBean.getUnionRateString());
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvUnionType, mTvUnionRate;
        private View mLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvUnionType = (TextView) itemView.findViewById(R.id.item_tv_union_type);
            mTvUnionRate = (TextView) itemView.findViewById(R.id.item_tv_union_rate);
            mLine = itemView.findViewById(R.id.v_line);
        }
    }

}