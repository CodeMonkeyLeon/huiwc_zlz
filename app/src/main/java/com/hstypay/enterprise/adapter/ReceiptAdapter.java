package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ReceiptDetailBean;
import com.hstypay.enterprise.bean.ReceiptListBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/03/16 20:28
 * @描述: ${TODO}
 */
public class ReceiptAdapter extends RecyclerView.Adapter {

    private static final int NORMAL_ITEM = 0;
    private static final int GROUP_ITEM = 1;

    private Context mContext;
    private List<ReceiptDetailBean> mDataList;
    private LayoutInflater mLayoutInflater;

    private OnRecyclerViewItemClickListener listener;

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener clickListener) {
        this.listener = clickListener;
    }


    public ReceiptAdapter(Context mContext, List<ReceiptDetailBean> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == NORMAL_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_receipt_view_item, parent, false);
            NormalItemHolder holder = new NormalItemHolder(mContext, itemView, listener);
            return holder;
        } else if (viewType == GROUP_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_receipt_view_list, parent, false);
            GroupItemHolder holder = new GroupItemHolder(mContext, itemView, listener);
            return holder;
        }
        return null;//
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (mDataList == null)
            return;
        ReceiptDetailBean entity = mDataList.get(position);

        if (null == entity)
            return;

        if (viewHolder instanceof GroupItemHolder) {
            bindGroupItem(entity, (GroupItemHolder) viewHolder);
        } else {
            NormalItemHolder holder = (NormalItemHolder) viewHolder;
            LogUtil.d("holder===" + holder);
            bindNormalItem(entity, holder.mItemViewDot, holder.mItemViewDivide, holder.mItemTvMoney, holder.mItemTvStatus
                    , holder.mItemTvTime, holder.mItemTvInstruction, holder.mItemIvLine);
        }
    }

    @Override
    public int getItemCount() {
        LogUtil.d("mDataList 的长度：", mDataList.size() + "");
//        if (mDataList.size() == 0)
//            linearLayout.setBackgroundResource(R.mipmap.null_record);
//        else
//            linearLayout.setBackgroundResource(0);
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        //第一个要显示时间
        if (position == 0)
            return GROUP_ITEM;

        String currentDate = DateUtil.formartDateToYYMMDD(mDataList.get(position).getCreateTime());
        int prevIndex = position - 1;
        boolean isDifferent = !DateUtil.formartDateToYYMMDD(mDataList.get(prevIndex).getCreateTime()).equals(currentDate);
        return isDifferent ? GROUP_ITEM : NORMAL_ITEM;
    }

    void bindNormalItem(ReceiptDetailBean entity, View mItemViewDot, View mItemViewDivide, TextView mItemTvMoney, TextView mItemTvStatus, TextView mItemTvTime, TextView mItemTvInstruction, ImageView mItemIvLine) {
        mItemTvMoney.setText(mContext.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(entity.getTradeMoney() / 100d));
        mItemTvTime.setText(DateUtil.formatDateToHHmmss(entity.getCreateTime()));
        if (!TextUtils.isEmpty(entity.getAttach())) {
            mItemViewDivide.setVisibility(View.VISIBLE);
            mItemTvInstruction.setVisibility(View.VISIBLE);
            mItemTvInstruction.setText(entity.getAttach());
        } else {
            mItemViewDivide.setVisibility(View.GONE);
            mItemTvInstruction.setVisibility(View.GONE);
        }

        switch (entity.getReceiptStatus()) {
            case 1:
                mItemTvStatus.setText(UIUtils.getString(R.string.tv_pay_wait_check));
                mItemViewDot.setBackgroundResource(R.drawable.shape_dot_pay_waiting);
                mItemTvMoney.setTextColor(UIUtils.getColor(R.color.home_text));
                mItemTvStatus.setTextColor(UIUtils.getColor(R.color.home_text));
                break;
            case 2:
                mItemTvStatus.setText(UIUtils.getString(R.string.tv_pay_checked));
                mItemViewDot.setBackgroundResource(R.drawable.shape_dot_pay_waiting);
                mItemTvMoney.setTextColor(UIUtils.getColor(R.color.home_text));
                mItemTvStatus.setTextColor(UIUtils.getColor(R.color.home_text));
                break;
            case 3:
                mItemTvStatus.setText(UIUtils.getString(R.string.tv_pay_closed));
                mItemViewDot.setBackgroundResource(R.drawable.shape_dot_pay_closed);
                mItemTvMoney.setTextColor(UIUtils.getColor(R.color.home_notice_text));
                mItemTvStatus.setTextColor(UIUtils.getColor(R.color.home_notice_text));
                break;
            case 4:
                mItemTvStatus.setText(UIUtils.getString(R.string.tv_pay_success));
                mItemViewDot.setBackgroundResource(R.drawable.shape_dot_pay_success);
                mItemTvMoney.setTextColor(UIUtils.getColor(R.color.home_text));
                mItemTvStatus.setTextColor(UIUtils.getColor(R.color.home_text));
                break;
            default:
                mItemTvStatus.setText(UIUtils.getString(R.string.tv_active_list_unknown));
                mItemViewDot.setBackgroundResource(R.drawable.shape_dot_pay_waiting);
                mItemTvMoney.setTextColor(UIUtils.getColor(R.color.home_text));
                mItemTvStatus.setTextColor(UIUtils.getColor(R.color.home_text));
                break;
        }
    }

    void bindGroupItem(ReceiptDetailBean entity, GroupItemHolder holder) {
        bindNormalItem(entity, holder.mItemViewDot, holder.mItemViewDivide, holder.mItemTvMoney, holder.mItemTvStatus
                , holder.mItemTvTime, holder.mItemTvInstruction, holder.mItemIvLine);
        if(DateUtil.getDate().equals(DateUtil.formartDateToYYMMDD(entity.getCreateTime()))){
            holder.mItemTvDate.setText("今日");
        }else {
            holder.mItemTvDate.setText(DateUtil.formartDateToYYMMDD(entity.getCreateTime()));
        }
    }

    public class NormalItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View mItemViewDot, mItemViewDivide;
        public TextView mItemTvMoney, mItemTvStatus, mItemTvTime, mItemTvInstruction;
        public ImageView mItemIvLine;


        private OnRecyclerViewItemClickListener listener;
        private Context context;

        public NormalItemHolder(Context context, View itemView, OnRecyclerViewItemClickListener listener) {
            super(itemView);
            this.context = context;
            this.listener = listener;
            mItemViewDot = itemView.findViewById(R.id.item_view_dot);
            mItemTvMoney = itemView.findViewById(R.id.item_tv_receipt_money);
            mItemTvStatus = itemView.findViewById(R.id.item_tv_receipt_status);
            mItemTvTime = itemView.findViewById(R.id.item_tv_receipt_time);
            mItemViewDivide = itemView.findViewById(R.id.item_view_divide);
            mItemTvInstruction = itemView.findViewById(R.id.item_tv_receipt_instruction);
            mItemIvLine = itemView.findViewById(R.id.item_iv_line);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
//                itemView.setBackgroundResource(R.drawable.recycler_bg);
                listener.onItemClick(getAdapterPosition());
            }
        }
    }

    public class GroupItemHolder extends NormalItemHolder {

        public TextView mItemTvDate;
        public View mItemViewDot, mItemViewDivide;
        public TextView mItemTvMoney, mItemTvStatus, mItemTvTime, mItemTvInstruction;
        public ImageView mItemIvLine;

        public GroupItemHolder(Context context, View itemView, OnRecyclerViewItemClickListener listener) {
            super(context, itemView, listener);
            mItemTvDate = (TextView) itemView.findViewById(R.id.item_tv_date);
            mItemViewDot = itemView.findViewById(R.id.item_view_dot);
            mItemTvMoney = itemView.findViewById(R.id.item_tv_receipt_money);
            mItemTvStatus = itemView.findViewById(R.id.item_tv_receipt_status);
            mItemTvTime = itemView.findViewById(R.id.item_tv_receipt_time);
            mItemViewDivide = itemView.findViewById(R.id.item_view_divide);
            mItemTvInstruction = itemView.findViewById(R.id.item_tv_receipt_instruction);
            mItemIvLine = itemView.findViewById(R.id.item_iv_line);
        }
    }
}