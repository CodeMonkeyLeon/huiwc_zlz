package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.StoreDevicesBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class ReceiveDevicesAdapter extends RecyclerView.Adapter<ReceiveDevicesAdapter.HomeViewHolder> {

    private Context mContext;
    private List<StoreDevicesBean.DataEntity.MerchantDeviceListEntity> mList;

    public ReceiveDevicesAdapter(Context context, List<StoreDevicesBean.DataEntity.MerchantDeviceListEntity> list) {
        this.mContext = context;
        this.mList = list;
    }


    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ReceiveDevicesAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_receive_devices, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final ReceiveDevicesAdapter.HomeViewHolder holder, final int position) {
        if (mList!=null && mList.size()>0){
            holder.mTvDeviceNo.setText(mList.get(position).getSn());
            if (position == mList.size()-1){
                holder.mViewLine.setVisibility(View.GONE);
            }else{
                holder.mViewLine.setVisibility(View.VISIBLE);
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDeviceNo;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvDeviceNo = (TextView) itemView.findViewById(R.id.tv_device_no);
            mViewLine = itemView.findViewById(R.id.vew_line);
        }
    }
}