package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.RefundBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class RefundRecordAdapter extends RecyclerView.Adapter<RefundRecordAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<RefundBean> mList;

    public RefundRecordAdapter(Context context, List<RefundBean> refundList) {
        this.mContext = context;
        this.mList = refundList;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public RefundRecordAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_refund_record, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(RefundRecordAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
        RefundBean refundBean = mList.get(position);
        holder.mTvRefundDate.setText(refundBean.getRefundTime());
        holder.mTvRefundMoney.setText(OrderStringUtil.getRefundStateString(refundBean.getRefundStatus())
                +"("+UIUtils.getString(R.string.tx_mark) + DateUtil.formatMoneyUtil(refundBean.getRefundMoney() / 100d)+")");
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvRefundDate,mTvRefundMoney;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvRefundDate = (TextView) itemView.findViewById(R.id.tv_item_refund_date);
            mTvRefundMoney = (TextView) itemView.findViewById(R.id.tv_item_refund_money);
        }
    }
}