package com.hstypay.enterprise.adapter.ReportData;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ReportSelectMonthBean;

import java.util.List;

/**
 * @author MiTa
 * @date 2017/11/20.
 */
public class ChoiceMonthAdapter extends RecyclerView.Adapter<ChoiceMonthAdapter.CalendarViewHolder> {

    private Context context;
    private List<ReportSelectMonthBean.DataBean> list;

    public ChoiceMonthAdapter(Context context, List<ReportSelectMonthBean.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public CalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.item_week_month, parent, false);
        return new CalendarViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(CalendarViewHolder holder, final int position) {
        holder.mTvTitle.setText(list.get(position).getYear());
        GridLayoutManager glm = new GridLayoutManager(context, 4) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        glm.setAutoMeasureEnabled(true);
        final ChoiceMonthChildAdapter adapter = new ChoiceMonthChildAdapter(context, list.get(position).getMonths());
        adapter.setChildClickListener(new ChoiceMonthChildAdapter.OnChildClickListener() {
            @Override
            public void onChildClick(int pos) {
                if (mChildClickListener != null) {
                    mChildClickListener.onMonthClick(position, pos);
                }
            }
        });
        holder.mRvCal.setAdapter(adapter);
        holder.mRvCal.setLayoutManager(glm);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    static class CalendarViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvTitle;
        private RecyclerView mRvCal;

        CalendarViewHolder(View itemView) {
            super(itemView);
            mRvCal = (RecyclerView) itemView.findViewById(R.id.recyclerView);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    private OnChildClickListener mChildClickListener;

    public interface OnChildClickListener {
        void onMonthClick(int parentPos, int pos);
    }

    public void setChildClickListener(OnChildClickListener childClickListener) {
        mChildClickListener = childClickListener;
    }
}
