package com.hstypay.enterprise.adapter.ReportData;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ReportSelectMonthBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @author MiTa
 * @date 2017/11/20.
 */
public class ChoiceMonthChildAdapter extends RecyclerView.Adapter<ChoiceMonthChildAdapter.CalendarViewHolder> {

    private Context context;
    private List<ReportSelectMonthBean.DataBean.MonthsBean> list;

    public ChoiceMonthChildAdapter(Context context, List<ReportSelectMonthBean.DataBean.MonthsBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public CalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.item_month_child, parent, false);
        return new CalendarViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final CalendarViewHolder holder, final int position) {
        holder.mTvDate.setText(DateUtil.formartDate(list.get(position).getPayTradeMonth(),"yyyy-MM","MM")+"月");
        holder.mTvMoney.setText(UIUtils.getString(R.string.tx_mark)+DateUtil.formatMoneyByLong(list.get(position).getSettlementFee()));
        holder.mTvDate.setTextColor(UIUtils.getColor(list.get(position).isSelected() ? R.color.theme_color : R.color.home_text));
        holder.mTvMoney.setTextColor(UIUtils.getColor(list.get(position).isSelected() ? R.color.theme_color : R.color.home_value_text));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mChildClickListener != null) {
                    mChildClickListener.onChildClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    static class CalendarViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDate,mTvMoney;

        CalendarViewHolder(View itemView) {
            super(itemView);
            mTvDate = itemView.findViewById(R.id.tv_date);
            mTvMoney = itemView.findViewById(R.id.tv_money);
        }
    }

    private OnChildClickListener mChildClickListener;

    public interface OnChildClickListener {
        void onChildClick(int pos);
    }

    public void setChildClickListener(OnChildClickListener childClickListener) {
        mChildClickListener = childClickListener;
    }
}
