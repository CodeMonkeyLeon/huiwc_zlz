package com.hstypay.enterprise.adapter.ReportData;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.MonthEntity;
import com.hstypay.enterprise.bean.ReportSelectWeekBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @author MiTa
 * @date 2017/11/20.
 */
public class ChoiceWeekChildAdapter extends RecyclerView.Adapter<ChoiceWeekChildAdapter.CalendarViewHolder> {

    private Context context;
    private List<ReportSelectWeekBean.DataBean.WeeksBean> list;

    public ChoiceWeekChildAdapter(Context context, List<ReportSelectWeekBean.DataBean.WeeksBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public CalendarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.item_week_child, parent, false);
        return new CalendarViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final CalendarViewHolder holder, final int position) {
        if (list == null || list.size()==0)
            return;
        holder.mTvDate.setText("第"+list.get(position).getPayTradeWeek()+"周");
        String[] dates = list.get(position).getPayTradeWeekScope().split(",");
        if (dates.length == 2){
            holder.mTvStartEnd.setText(DateUtil.formartDate(dates[0],"yyyy-MM-dd","MM,dd")
            +"~"+DateUtil.formartDate(dates[1],"yyyy-MM-dd","MM,dd"));
        }
        holder.mTvMoney.setText(UIUtils.getString(R.string.tx_mark)+DateUtil.formatMoneyByLong(list.get(position).getSettlementFee()));
        holder.mTvDate.setTextColor(UIUtils.getColor(list.get(position).isSelected() ? R.color.theme_color : R.color.home_text));
        holder.mTvMoney.setTextColor(UIUtils.getColor(list.get(position).isSelected() ? R.color.theme_color : R.color.home_value_text));
        holder.mTvStartEnd.setTextColor(UIUtils.getColor(list.get(position).isSelected() ? R.color.theme_color : R.color.home_value_text));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mChildClickListener != null){
                    /*holder.mTvDate.setTextColor(UIUtils.getColor(R.color.theme_color));
                    holder.mTvMoney.setTextColor(UIUtils.getColor(R.color.theme_color));
                    holder.mTvStartEnd.setTextColor(UIUtils.getColor(R.color.theme_color));*/
                    mChildClickListener.onChildClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    static class CalendarViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDate,mTvStartEnd,mTvMoney;

        CalendarViewHolder(View itemView) {
            super(itemView);
            mTvDate = itemView.findViewById(R.id.tv_date);
            mTvStartEnd = itemView.findViewById(R.id.tv_start_end);
            mTvMoney = itemView.findViewById(R.id.tv_money);
        }
    }

    private OnChildClickListener mChildClickListener;

    public interface OnChildClickListener {
        void onChildClick(int position);
    }

    public void setChildClickListener(OnChildClickListener childClickListener) {
        mChildClickListener = childClickListener;
    }
}
