package com.hstypay.enterprise.adapter.ReportData;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.MonthEntity;

import java.util.List;

/**
 * @author MiTa
 * @date 2017/11/20.
 */
public class MonthAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<MonthEntity> list;
    private boolean hideMoney;//是否隐藏金额
    //item类型
    public static final int ITEM_TYPE_HEADER = 0;
    public static final int ITEM_TYPE_CONTENT = 1;

    public MonthAdapter(Context context, List<MonthEntity> list) {
        this.context = context;
        this.list = list;

    }


    public MonthAdapter(Context context, List<MonthEntity> list, boolean hideMoney) {
        this.context = context;
        this.list = list;
        this.hideMoney = hideMoney;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {//头部
            return ITEM_TYPE_HEADER;
        } else {
            return ITEM_TYPE_CONTENT;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_HEADER) {
            View headerView = LayoutInflater.from(context).inflate(R.layout.hander_calendar, parent, false);
            return new HeaderViewHolder(headerView);
        } else {
            View convertView = LayoutInflater.from(context).inflate(R.layout.item_calendar, parent, false);
            return new CalendarViewHolder(convertView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderView, int position) {
        if (holderView instanceof CalendarViewHolder) {
            CalendarViewHolder holder = (CalendarViewHolder) holderView;
            MonthEntity entity=list.get(position-1);
            holder.mTvTitle.setText(entity.getTitle());
            GridLayoutManager glm = new GridLayoutManager(context, 7) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            glm.setAutoMeasureEnabled(true);
            final DateAdapter adapter = new DateAdapter(context, entity.getList(), hideMoney);
            adapter.setClickListener(new DateAdapter.OnDateClickListener() {
                @Override
                public void onDateClick(int parentPos, int pos) {
                    if (childClickListener != null) {
                        childClickListener.onMonthClick(parentPos, pos);
                    }
                }
            });
            holder.mRvCal.setAdapter(adapter);
            holder.mRvCal.setLayoutManager(glm);
        }

    }

    @Override
    public int getItemCount() {
        return list == null ? 1 : list.size() + 1;
    }

    public static class CalendarViewHolder extends RecyclerView.ViewHolder {

        CalendarViewHolder(View itemView) {
            super(itemView);
            mRvCal = (RecyclerView) itemView.findViewById(R.id.rv_cal);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_cal_title);
        }

        public TextView mTvTitle;
        public RecyclerView mRvCal;
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    private static OnMonthChildClickListener childClickListener;

    public interface OnMonthChildClickListener {
        void onMonthClick(int parentPos, int pos);
    }

    public void setChildClickListener(OnMonthChildClickListener childClickListener) {
        MonthAdapter.childClickListener = childClickListener;
    }
}
