package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.ReportData;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class ReportDataAdapter extends RecyclerView.Adapter<ReportDataAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private BaseActivity mActivity;
    private List<ReportData> mList;

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int index);
    }

    public void setOnItemClickListener(ReportDataAdapter.OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public ReportDataAdapter(BaseActivity activity, List<ReportData> list) {
        this.mActivity = activity;
        this.mList = list;
    }


    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ReportDataAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_report_data, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final ReportDataAdapter.HomeViewHolder holder, final int position) {
        if (mList != null && mList.size() > 0) {
            holder.mTvTitle.setText(mList.get(position).getTitle());
            holder.mTvValue.setText(mList.get(position).getValue());
            if (mList.get(position).getPercent() > 0) {
                holder.mTvPercent.setText(DateUtil.formatMoney(Math.abs(mList.get(position).getPercent()))+"%");
                holder.mTvPercent.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
                holder.mIvDataArrow.setImageResource(R.mipmap.icon_data_up);
                holder.mIvDataArrow.setVisibility(View.VISIBLE);
            } else if (mList.get(position).getPercent() < 0) {
                holder.mTvPercent.setText(DateUtil.formatMoney(Math.abs(mList.get(position).getPercent()))+"%");
                holder.mTvPercent.setTextColor(UIUtils.getColor(R.color.tv_data_down));
                holder.mIvDataArrow.setImageResource(R.mipmap.icon_data_down);
                holder.mIvDataArrow.setVisibility(View.VISIBLE);
            } else {
                holder.mTvPercent.setText("--");
                holder.mTvPercent.setTextColor(UIUtils.getColor(R.color.home_value_text));
                holder.mIvDataArrow.setVisibility(View.GONE);
            }

            if (mList.get(position).getIndex() == 4 || mList.get(position).getIndex() == 6 || mList.get(position).getIndex() == 7
                    || mList.get(position).getIndex() == 8 || mList.get(position).getIndex() == 9) {
                holder.mIvInstruction.setVisibility(View.VISIBLE);
            } else {
                holder.mIvInstruction.setVisibility(View.GONE);
            }
            holder.mIvInstruction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(mList.get(position).getIndex());
                }
            });
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIvDataArrow, mIvInstruction;
        private TextView mTvTitle, mTvValue, mTvPercent;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mTvValue = (TextView) itemView.findViewById(R.id.tv_value);
            mTvPercent = (TextView) itemView.findViewById(R.id.tv_percent);
            mIvDataArrow = (ImageView) itemView.findViewById(R.id.iv_data_arrow);
            mIvInstruction = (ImageView) itemView.findViewById(R.id.iv_instruction);
        }
    }
}