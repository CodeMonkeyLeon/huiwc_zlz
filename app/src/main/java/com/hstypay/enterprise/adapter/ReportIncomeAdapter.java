package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.bean.ReportDetailBean;
import com.hstypay.enterprise.utils.DateUtil;

import java.text.ParseException;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class ReportIncomeAdapter extends RecyclerView.Adapter<ReportIncomeAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<ReportDetailBean> mList;

    public ReportIncomeAdapter(Context context, List<ReportDetailBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ReportIncomeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_income_report, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(ReportIncomeAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
        holder.mIvIntroduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoticeDialog dialog = new NoticeDialog((MainActivity)mContext,"","",R.layout.notice_dialog_reports);
                dialog.show();
            }
        });
        holder.mTvActuaMoney.setText(DateUtil.formatMoneyByLong(mList.get(position).getPayNetFee()));
        holder.mTvReceiveMoney.setText(DateUtil.formatMoneyByLong(mList.get(position).getPayFee()));
        holder.mTvRefundMoney.setText(DateUtil.formatMoneyByLong(mList.get(position).getRefundFee()));
        if (TextUtils.isEmpty(mList.get(position).getStartTime())){
            if (!TextUtils.isEmpty(mList.get(position).getPayTradeTimeFmt())) {
                holder.mTvDate.setText(DateUtil.formartYMDToYYMM(mList.get(position).getPayTradeTimeFmt()));
            }
        }else {
            try {
                if (!TextUtils.isEmpty(mList.get(position).getStartTime()) && !TextUtils.isEmpty(mList.get(position).getEndTime()) ) {
                    if (DateUtil.formartDateToYYMMDD(mList.get(position).getStartTime()).equals(DateUtil.formartDateToYYMMDD(mList.get(position).getEndTime()))) {
                        holder.mTvDate.setText(DateUtil.formartDateToYYMMDD(mList.get(position).getStartTime()));
                    } else {
                        holder.mTvDate.setText(DateUtil.formartDateToYYMMDD(mList.get(position).getStartTime()) + "至" + DateUtil.formartDateToYYMMDD(mList.get(position).getEndTime()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvActuaMoney, mTvReceiveMoney,mTvRefundMoney,mTvDate;
        private ImageView mIvIntroduce;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mIvIntroduce = (ImageView) itemView.findViewById(R.id.iv_income_introduce);
            mTvDate = (TextView) itemView.findViewById(R.id.tv_income_date);
            mTvActuaMoney = (TextView) itemView.findViewById(R.id.tv_income_actual_money);
            mTvReceiveMoney = (TextView) itemView.findViewById(R.id.tv_income_receive_money);
            mTvRefundMoney = (TextView) itemView.findViewById(R.id.tv_income_refund_money);
        }
    }

}