package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.RoleSelectBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @描述: 角色选择
 */
public class RoleRecyclerAdapter extends RecyclerView.Adapter<RoleRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<RoleSelectBean> mDataBean;
    private String mRoleCode;
    private int layoutPosition;
    private boolean isFirstEnter = true;

    public RoleRecyclerAdapter(Context context, List<RoleSelectBean> data, String roleCode) {
        this.mContext = context;
        this.mDataBean = data;
        this.mRoleCode = roleCode;

        LogUtil.d("data====" + mRoleCode);
    }

    public void setStoreId(String roleCode) {
        this.mRoleCode = roleCode;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public RoleRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_shop, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final RoleRecyclerAdapter.HomeViewHolder holder,  int position) {
        if (mDataBean == null)
            return;
        RoleSelectBean dataBean = mDataBean.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.getAdapterPosition());
                    layoutPosition = holder.getLayoutPosition();
                    isFirstEnter = false;
                    notifyDataSetChanged();
                }
            }
        });
        holder.mViewLine.setVisibility(position == mDataBean.size() - 1 ? View.INVISIBLE : View.VISIBLE);
        if (dataBean != null) {
            holder.mTvShopName.setText(dataBean.getRoleName());
            if (dataBean.getRoleCode().equals(mRoleCode)) {
                holder.mIvShopChoiced.setVisibility(View.VISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
            } else {
                holder.mIvShopChoiced.setVisibility(View.INVISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.black));
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvShopName;
        private ImageView mIvShopChoiced;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvShopName = (TextView) itemView.findViewById(R.id.tv_shop_choice);
            mIvShopChoiced = (ImageView) itemView.findViewById(R.id.iv_shop_choice);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}