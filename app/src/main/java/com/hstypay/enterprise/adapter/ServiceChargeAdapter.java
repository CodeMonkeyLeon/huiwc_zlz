package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class ServiceChargeAdapter extends RecyclerView.Adapter<ServiceChargeAdapter.HomeViewHolder> {

    private Context mContext;
    private List<Object> mList;

    public ServiceChargeAdapter(Context context, List<Object> list) {
        this.mContext = context;
        this.mList = list;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ServiceChargeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_rate, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(ServiceChargeAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
    }

    public void setView(int payType, ServiceChargeAdapter.HomeViewHolder holder) {
        switch (payType) {
            case 1://微信
                holder.mTvSignType.setText("微信支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_wechat);
                break;
            case 2://支付宝
                holder.mTvSignType.setText("支付宝");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_alipay);
                break;
            case 4://qq钱包
                holder.mTvSignType.setText("QQ钱包");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_qq);
                break;
            case 3://财付通
            case 5://银联
            case 99://未知
            default:
                holder.mTvSignType.setText("其他支付");
                holder.mIvSignType.setImageResource(R.mipmap.icon_general_receivables);
                break;

        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvSignType, mTvSignRate;
        private ImageView mIvSignType;
        private View mLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvSignType = (TextView) itemView.findViewById(R.id.item_tv_sign_type);
            mTvSignRate = (TextView) itemView.findViewById(R.id.item_tv_sign_rate);
            mIvSignType = (ImageView) itemView.findViewById(R.id.item_iv_sign_type);
            mLine = itemView.findViewById(R.id.v_line);
        }
    }

}