package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.PushModeBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */
public class ShowCountAdapter extends RecyclerView.Adapter<ShowCountAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<PushModeBean.DataBean.PushFrequencysBean> mList;

    public ShowCountAdapter(Context context, List<PushModeBean.DataBean.PushFrequencysBean> data) {
        this.mContext = context;
        this.mList = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ShowCountAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_show_count, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final ShowCountAdapter.HomeViewHolder holder, final int position) {
        if (mList == null || mList.size() == 0){
            return;
        }
        holder.mTvShowCount.setText(mList.get(position).getTypeName());
        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvShowCount;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvShowCount = itemView.findViewById(R.id.tv_show_count);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}