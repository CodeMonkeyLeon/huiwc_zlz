package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.StoreCodeListBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class StoreCodeListAdapter extends RecyclerView.Adapter<StoreCodeListAdapter.HomeViewHolder> {

    private Context mContext;
    private List<StoreCodeListBean.DataBeanX.DataBean> mList;
    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public StoreCodeListAdapter(Context context, List<StoreCodeListBean.DataBeanX.DataBean> list) {
        this.mContext = context;
        this.mList = list;
    }


    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public StoreCodeListAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_store_code, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final StoreCodeListAdapter.HomeViewHolder holder, final int position) {
        if (mList != null && mList.size() > 0) {
//            holder.mTvDeviceStore.setText(mList.get(position).getStoreName());
            if (StringUtils.getStrLength(mList.get(position).getStoreName()) > 22){
                holder.mTvDeviceStore.setText(StringUtils.splitStr(mList.get(position).getStoreName(), 20)+"...");
            }else {
                holder.mTvDeviceStore.setText(mList.get(position).getStoreName());
            }
            Picasso.get().load(Constants.H5_BASE_URL + mList.get(position).getQrLogo())
                    .error(R.mipmap.img_payment_code_list)
                    .placeholder(R.mipmap.img_payment_code_list)
                    .into(holder.mIvDeviceLogo);
            if (mList.get(position).getQrType() == 2) {
                /*if (AppHelper.getApkType()==0){
                    holder.mIvDeviceLogo.setImageResource(R.mipmap.icon_item_store_code);
                }else {
                    holder.mIvDeviceLogo.setImageResource(R.mipmap.img_payment_code_list);
                }*/
                holder.mTvDeviceType.setText("编号：" + mList.get(position).getId());
                holder.mTvCodeMark.setText("银标码");
                holder.mTvCodeMark.setBackgroundResource(R.drawable.shape_green_solid_mark);
                holder.mTvCodeMark.setVisibility(View.VISIBLE);
                holder.mIvUpdateCode.setVisibility(View.GONE);
            } else if (mList.get(position).getQrType() == 4) {
//                holder.mIvDeviceLogo.setImageResource(R.mipmap.img_huabei_list);
                holder.mTvDeviceType.setText("编号：" + mList.get(position).getId());
                holder.mTvCodeMark.setText("花呗码");
                holder.mTvCodeMark.setBackgroundResource(R.drawable.shape_blue_solid_mark);
                holder.mTvCodeMark.setVisibility(View.VISIBLE);
                holder.mIvUpdateCode.setVisibility(View.GONE);
            } else {
                if (mList.get(position).getQrType() == 3/* && !TextUtils.isEmpty(mList.get(position).getWxappid())*/) {
                    /*if (AppHelper.getApkType()==0){
                        holder.mIvDeviceLogo.setImageResource(R.mipmap.icon_item_update_code);
                    }else {
                        holder.mIvDeviceLogo.setImageResource(R.mipmap.img_payment_code_list);
                    }*/
                    holder.mTvDeviceType.setText("编号：" + mList.get(position).getId());
                    holder.mTvCodeMark.setVisibility(View.GONE);
                    holder.mIvUpdateCode.setVisibility(View.VISIBLE);
                } else {
                    /*if (AppHelper.getApkType()==0){
                        holder.mIvDeviceLogo.setImageResource(R.mipmap.icon_item_store_code);
                    }else {
                        holder.mIvDeviceLogo.setImageResource(R.mipmap.img_payment_code_list);
                    }*/
                    holder.mTvDeviceType.setText("编号：" + mList.get(position).getId());
                    holder.mTvCodeMark.setText("聚合码");
                    holder.mTvCodeMark.setBackgroundResource(R.drawable.shape_red_solid_mark);
                    holder.mTvCodeMark.setVisibility(View.VISIBLE);
                    holder.mIvUpdateCode.setVisibility(View.GONE);
                }
            }
            if (TextUtils.isEmpty(mList.get(position).getEmpName())) {
                holder.mTvDeviceNo.setVisibility(View.INVISIBLE);
            } else {
                holder.mTvDeviceNo.setVisibility(View.VISIBLE);
                if (StringUtils.getStrLength("所属员工：" + mList.get(position).getEmpName()) > 30) {
                    holder.mTvDeviceNo.setText(StringUtils.splitStr("所属员工：" + mList.get(position).getEmpName(), 28) + "...");
                } else {
                    holder.mTvDeviceNo.setText("所属员工：" + mList.get(position).getEmpName());
                }
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            });
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIvDeviceLogo,mIvUpdateCode;
        private TextView mTvDeviceStore, mTvDeviceType, mTvDeviceNo,mTvCodeMark;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvDeviceStore = (TextView) itemView.findViewById(R.id.tv_device_store);
            mIvUpdateCode = (ImageView) itemView.findViewById(R.id.iv_update_code);
            mTvCodeMark = (TextView) itemView.findViewById(R.id.tv_code_mark);
            mTvDeviceType = (TextView) itemView.findViewById(R.id.tv_device_type);
            mTvDeviceNo = (TextView) itemView.findViewById(R.id.tv_device_no);
            mIvDeviceLogo = (ImageView) itemView.findViewById(R.id.iv_item_logo);
        }
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}