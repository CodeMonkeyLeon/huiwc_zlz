package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.bean.StoreDevicesBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class StoreDevicesAdapter extends RecyclerView.Adapter<StoreDevicesAdapter.HomeViewHolder> {
    private Context mContext;
    private List<StoreDevicesBean.DataEntity> mList;

    public StoreDevicesAdapter(Context context, List<StoreDevicesBean.DataEntity> list) {
        this.mContext = context;
        this.mList = list;
    }


    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public StoreDevicesAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_store_devices, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final StoreDevicesAdapter.HomeViewHolder holder, final int position) {
        if (mList!=null && mList.size()>0){
            StoreDevicesBean.DataEntity dataEntity = mList.get(position);
            if(dataEntity!=null) {
                holder.mTvStoreName.setText(dataEntity.getStoreName());
                if (position == mList.size() - 1) {
                    holder.mViewLine.setVisibility(View.GONE);
                } else {
                    holder.mViewLine.setVisibility(View.VISIBLE);
                }
                holder.mIvArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setArrow(mList,position);
                        notifyDataSetChanged();
                    }
                });
                if (dataEntity.isOpen()){
                    holder.mIvArrow.setImageResource(R.mipmap.icon_arrow_top);
                    holder.mLlDevices.setVisibility(View.VISIBLE);
                }else{
                    holder.mIvArrow.setImageResource(R.mipmap.icon_arrow_bottom);
                    holder.mLlDevices.setVisibility(View.GONE);
                }

                ReceiveDevicesAdapter adapter = new ReceiveDevicesAdapter(mContext,mList.get(position).getMerchantDeviceList());
                holder.mRvDevice.setAdapter(adapter);
            }
        }
    }

    private void setArrow(List<StoreDevicesBean.DataEntity> mList,int position){
        if (mList !=null && mList.size()>0){
            for (int i = 0; i < mList.size(); i++) {
                if (position == i){
                    if (mList.get(i).isOpen()) {
                        mList.get(i).setIsOpen(false);
                    }else {
                        mList.get(i).setIsOpen(true);
                    }
                }else {
                    mList.get(i).setIsOpen(false);
                }
            }
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLlDevices;
        private RecyclerView mRvDevice;
        private TextView mTvStoreName;
        private ImageView mIvArrow;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mLlDevices = (LinearLayout) itemView.findViewById(R.id.ll_devices);
            mRvDevice = (RecyclerView) itemView.findViewById(R.id.device_recycler);
            mTvStoreName = (TextView)itemView.findViewById(R.id.tv_store_name);
            mIvArrow = (ImageView)itemView.findViewById(R.id.iv_arrow);
            mViewLine = itemView.findViewById(R.id.vew_line);

            CustomLinearLayoutManager mLinearLayoutManager = new CustomLinearLayoutManager(mContext);
            mRvDevice.setLayoutManager(mLinearLayoutManager);
            mRvDevice.setItemAnimator(new DefaultItemAnimator());
        }
    }
}