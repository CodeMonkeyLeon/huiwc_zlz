package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.viewholder.StoreEmployeeTitleViewHolder;
import com.hstypay.enterprise.adapter.viewholder.StoreEmployeeViewHolder;
import com.hstypay.enterprise.bean.StoreEmployeeBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 门店员工
 */
public class StoreEmployeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StoreEmployeeBean.Employee> mEmployees = new ArrayList<>();//员工：包括店长和收银员，传数据进来时要把数据组织好
    private final int VIEW_TYPE_TITLE = 1;//标题view
    private final int VIEW_TYPE_EMPLOYEE = 2;//内容view
    private Context mContext;
    public StoreEmployeeAdapter(Context context){
        mContext = context;
    }
    public void setDatas(List<StoreEmployeeBean.Employee> employees){
        mEmployees.clear();
        notifyDataSetChanged();
        if (employees!=null && employees.size()>0){
            mEmployees.addAll(employees);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == VIEW_TYPE_TITLE){
            View titleView = LayoutInflater.from(mContext).inflate(R.layout.item_store_employee_title, viewGroup, false);
            viewHolder = new StoreEmployeeTitleViewHolder(titleView);
        }else {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.item_store_employee_content, viewGroup, false);
            viewHolder = new StoreEmployeeViewHolder(contentView);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_TITLE){
            StoreEmployeeTitleViewHolder titleViewHolder = (StoreEmployeeTitleViewHolder) viewHolder;
            titleViewHolder.setData(mEmployees.get(position).getTitleName());
        }else {
            StoreEmployeeViewHolder employeeViewHolder = (StoreEmployeeViewHolder) viewHolder;
            employeeViewHolder.setData(mEmployees.get(position));
            employeeViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnRecycleViewItemClickListener!=null){
                        mOnRecycleViewItemClickListener.onItemClick(mEmployees.get(viewHolder.getAdapterPosition()));
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return mEmployees.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (!TextUtils.isEmpty(mEmployees.get(position).getTitleName())){
            //标题
            return VIEW_TYPE_TITLE;
        }else {
            //内容
            return VIEW_TYPE_EMPLOYEE;
        }
    }

    public interface OnRecycleViewItemClickListener{
        void onItemClick(StoreEmployeeBean.Employee employee);
    }
    private OnRecycleViewItemClickListener mOnRecycleViewItemClickListener;

    public void setOnRecycleViewItemClickListener(OnRecycleViewItemClickListener onRecycleViewItemClickListener) {
        mOnRecycleViewItemClickListener = onRecycleViewItemClickListener;
    }
}
