package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @描述: 门店管理
 */
public class StoreManageAdapter extends RecyclerView.Adapter<StoreManageAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<StoreListBean.DataEntity> mDataBean;


    public StoreManageAdapter(Context context, List<StoreListBean.DataEntity> data) {
        this.mContext = context;
        this.mDataBean = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public StoreManageAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(mContext, R.layout.item_store_manage, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final StoreManageAdapter.HomeViewHolder holder,  int position) {
        if (mDataBean == null)
            return;
        holder.mTvStoreName.setText(mDataBean.get(position).getStoreName());
        holder.mTv_employee_num_store.setText("共"+mDataBean.get(position).getEmpTotal()+"名员工");
        if (mDataBean.get(position).getExamineStatus() == 0) {
            holder.mTvStoreExamineStatus.setText(UIUtils.getString(R.string.wait_check_tx));
            holder.mTvStoreExamineStatus.setTextColor(mContext.getResources().getColor(R.color.color_4272ee));
            holder.mTvStoreExamineStatus.setBackground(mContext.getResources().getDrawable(R.drawable.shape_store_manage_state0));
        } else if (mDataBean.get(position).getExamineStatus() == 1) {
            holder.mTvStoreExamineStatus.setText(UIUtils.getString(R.string.check_pass_tx));
            holder.mTvStoreExamineStatus.setTextColor(mContext.getResources().getColor(R.color.color_08cc9e));
            holder.mTvStoreExamineStatus.setBackground(mContext.getResources().getDrawable(R.drawable.shape_store_manage_state1));
        } else if (mDataBean.get(position).getExamineStatus() == 2) {
            holder.mTvStoreExamineStatus.setText(UIUtils.getString(R.string.check_unpass_tx));
            holder.mTvStoreExamineStatus.setTextColor(mContext.getResources().getColor(R.color.color_fc5a45));
            holder.mTvStoreExamineStatus.setBackground(mContext.getResources().getDrawable(R.drawable.shape_store_manage_state2));
        } else if (mDataBean.get(position).getExamineStatus() == 3) {
            holder.mTvStoreExamineStatus.setText(UIUtils.getString(R.string.checking_change_tx));
            holder.mTvStoreExamineStatus.setTextColor(mContext.getResources().getColor(R.color.color_4272ee));
            holder.mTvStoreExamineStatus.setBackground(mContext.getResources().getDrawable(R.drawable.shape_store_manage_state0));
        }
        if (position == mDataBean.size()-1) {
            holder.mViewLine.setVisibility(View.GONE);
        } else {
            holder.mViewLine.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.getAdapterPosition());
                }
            }
        });

    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null)
            return mDataBean.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvStoreName, mTvStoreExamineStatus;
        private View mViewLine;
        private TextView mTv_employee_num_store;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvStoreName = (TextView) itemView.findViewById(R.id.tv_store_name);
            mTvStoreExamineStatus = (TextView) itemView.findViewById(R.id.tv_store_examine_status);
            mViewLine = itemView.findViewById(R.id.view_line);
            mTv_employee_num_store = itemView.findViewById(R.id.tv_employee_num_store);
        }
    }

}