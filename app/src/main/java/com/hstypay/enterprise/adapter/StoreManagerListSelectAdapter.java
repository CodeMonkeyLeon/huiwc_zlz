package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ManagerListBean;
import com.hstypay.enterprise.bean.StoreGroupBean;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @描述: 店长列表选择adapter
 */
public class StoreManagerListSelectAdapter extends RecyclerView.Adapter<StoreManagerListSelectAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<ManagerListBean.DataEntity.itemData> mDataBean;

    public StoreManagerListSelectAdapter(Context context, List<ManagerListBean.DataEntity.itemData> data) {
        this.mContext = context;
        this.mDataBean = data;

    }


    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public StoreManagerListSelectAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_shop, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final StoreManagerListSelectAdapter.HomeViewHolder holder, int position) {
        if (mDataBean == null)
            return;
        ManagerListBean.DataEntity.itemData dataBean = mDataBean.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            }
        });
        holder.mViewLine.setVisibility(position == mDataBean.size() - 1 ? View.INVISIBLE : View.VISIBLE);
        if (dataBean != null) {
            //holder.mTvShopName.setText(dataBean.getGroupName());
            if (StringUtils.getStrLength(dataBean.getEmpName()) > 26) {
                holder.mTvShopName.setText(StringUtils.splitStr(dataBean.getEmpName(), 24) + "...");
            } else {
                holder.mTvShopName.setText(dataBean.getEmpName());
            }
            /*if (dataBean.getUserId().equals(mStoreId)) {
                holder.mIvShopChoiced.setVisibility(View.VISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
            } else {
                holder.mIvShopChoiced.setVisibility(View.INVISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.black));
            }*/
            if (dataBean.isSelected()){
                holder.mIvShopChoiced.setVisibility(View.VISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
            }else {
                holder.mIvShopChoiced.setVisibility(View.INVISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.black));
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvShopName;
        private ImageView mIvShopChoiced;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvShopName = (TextView) itemView.findViewById(R.id.tv_shop_choice);
            mIvShopChoiced = (ImageView) itemView.findViewById(R.id.iv_shop_choice);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}