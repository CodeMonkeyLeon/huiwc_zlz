package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintTicketTypeSetActivity;
import com.hstypay.enterprise.bean.QueryAvailabbleInfoBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/9/18
 * @desc 云打印机和云音箱关联设置的adapter
 */
public class TicketTypeSetRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private int mPageType;//1、收款码，2、收款设备、3、收银员
    private List<QueryAvailabbleInfoBean.BindDeviceBean> mData = new ArrayList<>();
    public TicketTypeSetRvAdapter(Context context,int pageType){
        mContext = context;
        mPageType = pageType;
    }

    public void setData(List<QueryAvailabbleInfoBean.BindDeviceBean> data){
        mData.clear();
        notifyDataSetChanged();
        if (data!=null){
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_ticket_type_set_adapter, parent, false);
        TicketTypeSetViewHold ticketTypeSetViewHold = new TicketTypeSetViewHold(itemView);
        return ticketTypeSetViewHold;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((TicketTypeSetViewHold)viewHolder).setData(mData.get(position));
        ((TicketTypeSetViewHold)viewHolder).mIv_switch_set_ticket_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnBindSwitchClickListener!=null){
                    mOnBindSwitchClickListener.onBind(mPageType,viewHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class TicketTypeSetViewHold extends RecyclerView.ViewHolder{

        private  TextView mTv_title_ticket_type_set;
        private  TextView mTv_id_phone_ticket_type_set;
        private  TextView mTv_address_ticket_type_set;
        private  ImageView mIv_switch_set_ticket_type;

        public TicketTypeSetViewHold(@NonNull View itemView) {
            super(itemView);
            mTv_title_ticket_type_set = itemView.findViewById(R.id.tv_title_ticket_type_set);
            mTv_id_phone_ticket_type_set = itemView.findViewById(R.id.tv_id_phone_ticket_type_set);
            mTv_address_ticket_type_set = itemView.findViewById(R.id.tv_address_ticket_type_set);
            mIv_switch_set_ticket_type = itemView.findViewById(R.id.iv_switch_set_ticket_type);
        }

        public void setData(QueryAvailabbleInfoBean.BindDeviceBean bindDeviceBean) {
            if (mPageType == CloudPrintTicketTypeSetActivity.BIND_TYPE_CODE){
                //收款码
                //名称
                mTv_title_ticket_type_set.setText(bindDeviceBean.getQrCodeTypeCnt());
                //编号
                String bindId = bindDeviceBean.getBindId();
                if (!TextUtils.isEmpty(bindId)){
                    mTv_id_phone_ticket_type_set.setVisibility(View.VISIBLE);
                    mTv_id_phone_ticket_type_set.setText("编号:"+bindId);
                }else {
                    mTv_id_phone_ticket_type_set.setVisibility(View.INVISIBLE);
                }
                //收银点
                String cashierName = bindDeviceBean.getCashPointName();
                if (!TextUtils.isEmpty(cashierName)){
                    mTv_address_ticket_type_set.setVisibility(View.VISIBLE);
                    mTv_address_ticket_type_set.setText("收银点:"+cashierName);
                }else {
                    mTv_address_ticket_type_set.setVisibility(View.GONE);
                }
                //绑定状态
                String physicFlag = bindDeviceBean.getPhysicFlag();
                if("1".equals(physicFlag)){
                    //已绑定
                    mIv_switch_set_ticket_type.setImageResource(R.mipmap.ic_switch_open);
                }else {
                    //未绑定
                    mIv_switch_set_ticket_type.setImageResource(R.mipmap.ic_switch_close);
                }
            }else if(mPageType == CloudPrintTicketTypeSetActivity.BIND_TYPE_DEVICE){
               //收款设备
                //名称
                mTv_title_ticket_type_set.setText(bindDeviceBean.getDeviceType());
                //编号
//                String bindId = bindDeviceBean.getBindId();
                String sn = bindDeviceBean.getSn();
                if (!TextUtils.isEmpty(sn)){
                    mTv_id_phone_ticket_type_set.setVisibility(View.VISIBLE);
                    mTv_id_phone_ticket_type_set.setText("设备编号:"+sn);
                }else {
                    mTv_id_phone_ticket_type_set.setVisibility(View.INVISIBLE);
                }
                //收银点
                String cashierName = bindDeviceBean.getCashPointName();
                if (!TextUtils.isEmpty(cashierName)){
                    mTv_address_ticket_type_set.setVisibility(View.VISIBLE);
                    mTv_address_ticket_type_set.setText("收银点:"+cashierName);
                }else {
                    mTv_address_ticket_type_set.setVisibility(View.GONE);
                }
                //绑定状态
                String physicFlag = bindDeviceBean.getPhysicFlag();
                if("1".equals(physicFlag)){
                    //已绑定
                    mIv_switch_set_ticket_type.setImageResource(R.mipmap.ic_switch_open);
                }else {
                    //未绑定
                    mIv_switch_set_ticket_type.setImageResource(R.mipmap.ic_switch_close);
                }
            }else if(mPageType == CloudPrintTicketTypeSetActivity.BIND_TYPE_CACHIER){
                //收银员
                //名称
                String cashierName = bindDeviceBean.getCashierName();
                /*if (cashierName!=null && cashierName.length()>1 && !cashierName.contains("*")){
                    mTv_title_ticket_type_set.setText(cashierName.charAt(0)+"**");
                }else {
                    mTv_title_ticket_type_set.setText(cashierName);
                }*/
                mTv_title_ticket_type_set.setText(cashierName);
                //电话号码
                String mobile = bindDeviceBean.getMobile();
                if (!TextUtils.isEmpty(mobile)){
                    mTv_id_phone_ticket_type_set.setVisibility(View.VISIBLE);
                    if (mobile.length()>7 && !mobile.contains("*")){
                        //隐藏4到7位
                        String startNum = mobile.substring(0, 3);
                        String endNum = mobile.substring(7);
                        mTv_id_phone_ticket_type_set.setText(startNum+"****"+endNum);
                    }else {
                        mTv_id_phone_ticket_type_set.setText(mobile);
                    }
                }else {
                    mTv_id_phone_ticket_type_set.setVisibility(View.INVISIBLE);
                }
                mTv_address_ticket_type_set.setVisibility(View.GONE);
                //绑定状态
                String physicFlag = bindDeviceBean.getPhysicFlag();
                if("1".equals(physicFlag)){
                    //已绑定
                    mIv_switch_set_ticket_type.setImageResource(R.mipmap.ic_switch_open);
                }else {
                    //未绑定
                    mIv_switch_set_ticket_type.setImageResource(R.mipmap.ic_switch_close);
                }
            }
        }
    }

    public interface OnBindSwitchClickListener{
        void onBind(int type,int position);
    }
    private OnBindSwitchClickListener mOnBindSwitchClickListener;

    public void setOnBindSwitchClickListener(OnBindSwitchClickListener onBindSwitchClickListener) {
        mOnBindSwitchClickListener = onBindSwitchClickListener;
    }
}
