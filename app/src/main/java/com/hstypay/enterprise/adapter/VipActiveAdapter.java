package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.VipActiveListBean;
import com.hstypay.enterprise.utils.DateUtil;

import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */
public class VipActiveAdapter extends RecyclerView.Adapter<VipActiveAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<VipActiveListBean.DataEntity.DataListEntity> mDataBean;
    private String mStoreId;

    public VipActiveAdapter(Context context, List<VipActiveListBean.DataEntity.DataListEntity> data, String storeId) {
        this.mContext = context;
        this.mDataBean = data;
        this.mStoreId = storeId;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemDownloadClick(int position);

        void onItemDetailClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public VipActiveAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_vip_active, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final VipActiveAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean == null)
            return;
        holder.mTvActiveDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemDownloadClick(position);
                }
            }
        });
        holder.mTvActiveDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemDetailClick(position);
                }
            }
        });
        holder.mTvActiveName.setText(mDataBean.get(position).getName());
        holder.mTvActiveCount.setText(mDataBean.get(position).getParticipation_user_num() + "");
        holder.mTvActiveMoney.setText(DateUtil.formatMoneyUtil(mDataBean.get(position).getPay_amount_total().divide(BigDecimal.valueOf(100d)).doubleValue()));
        if (MyApplication.getIsMerchant()) {
            holder.mTvActiveDate.setVisibility(View.GONE);
            holder.mLlActiveData.setVisibility(View.VISIBLE);
            holder.mViewLine.setVisibility(View.VISIBLE);
        } else {
            holder.mTvActiveDate.setVisibility(View.VISIBLE);
            holder.mLlActiveData.setVisibility(View.GONE);
            holder.mViewLine.setVisibility(View.GONE);
            holder.mTvActiveDate.setText(MyApplication.getContext().getString(R.string.tv_active_date)
                    + DateUtil.formatYYMD(mDataBean.get(position).getBegin_date(), "yyyy.MM.dd")
                    + "—" + DateUtil.formatYYMD(mDataBean.get(position).getEnd_date(), "yyyy.MM.dd"));
        }
        if (mDataBean.get(position).getScene_type() == 2) {
            holder.mTvActiveMoneyTitle.setText(MyApplication.getContext().getString(R.string.tx_vip_recharge_money));
        } else if (mDataBean.get(position).getScene_type() == 3) {
            holder.mTvActiveMoneyTitle.setText(MyApplication.getContext().getString(R.string.tx_vip_consume_money));
        }
        //1-初始 2-未开始 3-进行中 4-已完成 5-已暂停 6-已关闭
        switch (mDataBean.get(position).getState()) {
            case 2:
                holder.mTvActiveStatus.setText(mContext.getString(R.string.tv_active_list_not_start));
                holder.mTvActiveStatus.setBackgroundResource(R.drawable.shape_active_enable_bg);
                holder.mViewLineVertical.setVisibility(View.VISIBLE);
                holder.mTvActiveDownload.setVisibility(View.VISIBLE);
                break;
            case 3:
                holder.mTvActiveStatus.setText(mContext.getString(R.string.tv_active_list_process));
                holder.mTvActiveStatus.setBackgroundResource(R.drawable.shape_active_enable_bg);
                holder.mViewLineVertical.setVisibility(View.VISIBLE);
                holder.mTvActiveDownload.setVisibility(View.VISIBLE);
                break;
            case 4:
                holder.mTvActiveStatus.setText(mContext.getString(R.string.tv_active_list_over));
                holder.mTvActiveStatus.setBackgroundResource(R.drawable.shape_active_unable_bg);
                holder.mViewLineVertical.setVisibility(View.GONE);
                holder.mTvActiveDownload.setVisibility(View.GONE);
                break;
            case 5:
                holder.mTvActiveStatus.setText(mContext.getString(R.string.tv_active_list_pause));
                holder.mTvActiveStatus.setBackgroundResource(R.drawable.shape_active_enable_bg);
                holder.mViewLineVertical.setVisibility(View.VISIBLE);
                holder.mTvActiveDownload.setVisibility(View.VISIBLE);
                break;
            case 6:
                holder.mTvActiveStatus.setText(mContext.getString(R.string.tv_active_list_close));
                holder.mTvActiveStatus.setBackgroundResource(R.drawable.shape_active_unable_bg);
                holder.mViewLineVertical.setVisibility(View.GONE);
                holder.mTvActiveDownload.setVisibility(View.GONE);
                break;
            default:
                holder.mTvActiveStatus.setText(mContext.getString(R.string.tv_active_list_unknown));
                holder.mTvActiveStatus.setBackgroundResource(R.drawable.shape_active_unable_bg);
                holder.mViewLineVertical.setVisibility(View.GONE);
                holder.mTvActiveDownload.setVisibility(View.GONE);
                break;
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLlActiveData;
        private View mViewLine, mViewLineVertical;
        private TextView mTvActiveName, mTvActiveStatus, mTvActiveCount, mTvActiveMoney, mTvActiveDate, mTvActiveDownload, mTvActiveDetail, mTvActiveMoneyTitle;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvActiveName = (TextView) itemView.findViewById(R.id.tv_active_name);
            mTvActiveStatus = (TextView) itemView.findViewById(R.id.tv_active_status);
            mTvActiveCount = (TextView) itemView.findViewById(R.id.tv_active_count);
            mTvActiveMoney = (TextView) itemView.findViewById(R.id.tv_active_money);
            mTvActiveMoneyTitle = (TextView) itemView.findViewById(R.id.tv_active_money_title);
            mTvActiveDate = (TextView) itemView.findViewById(R.id.tv_active_date);

            mTvActiveDownload = (TextView) itemView.findViewById(R.id.tv_active_download);
            mTvActiveDetail = (TextView) itemView.findViewById(R.id.tv_active_detail);

            mViewLine = itemView.findViewById(R.id.view_line);
            mViewLineVertical = itemView.findViewById(R.id.view_line_vertical);
            mLlActiveData = (LinearLayout) itemView.findViewById(R.id.ll_active_data);
        }
    }

}