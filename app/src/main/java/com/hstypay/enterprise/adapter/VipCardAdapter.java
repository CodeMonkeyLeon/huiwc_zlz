package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.VipCardListBean;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class VipCardAdapter extends RecyclerView.Adapter<VipCardAdapter.HomeViewHolder> {

    private Context mContext;
    private List<VipCardListBean.DataEntity> mList;

    public VipCardAdapter(Context context, List<VipCardListBean.DataEntity> list) {
        this.mContext = context;
        this.mList = list;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public VipCardAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_vip_card, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(VipCardAdapter.HomeViewHolder holder, final int position) {
        if (mList != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            });
            if ("4".equals(mList.get(position).getCardType())) {
                holder.mTvCardType.setText(UIUtils.getString(R.string.tv_vip_card));
                holder.mIvVipCardType.setImageResource(R.mipmap.icon_vip_card);
            }else if ("1".equals(mList.get(position).getCardType())) {
                holder.mTvCardType.setText(UIUtils.getString(R.string.tv_value_card));
                holder.mIvVipCardType.setImageResource(R.mipmap.icon_value_card);
            }
            holder.mTvVipCardId.setText(mList.get(position).getMembersNo());
            holder.mTvVipPhone.setText(mList.get(position).getMobile());
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvVipCardId, mTvVipPhone,mTvCardType;
        private ImageView mIvVipCardType;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mIvVipCardType = (ImageView) itemView.findViewById(R.id.iv_vip_card_type);
            mTvCardType = (TextView) itemView.findViewById(R.id.tv_card_type);
            mTvVipCardId = (TextView) itemView.findViewById(R.id.tv_vip_card_id);
            mTvVipPhone = (TextView) itemView.findViewById(R.id.tv_vip_phone);
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListenser(OnItemClickListener onItemClickListenser) {
        this.mOnItemClickListener = onItemClickListenser;
    }
}