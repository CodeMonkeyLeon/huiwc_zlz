package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.StoreListBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */
public class VipCodeAdapter extends RecyclerView.Adapter<VipCodeAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<StoreListBean.DataEntity> mDataBean;

    public VipCodeAdapter(Context context, List<StoreListBean.DataEntity> data) {
        this.mContext = context;
        this.mDataBean = data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public VipCodeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_vip_code, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final VipCodeAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean!=null && mDataBean.size()>0) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            });
            holder.mTvStoreName.setText(MyApplication.getContext().getString(R.string.bind_code_content1)+mDataBean.get(position).getStoreName());
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvStoreName,mTvStoreManager;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvStoreName = (TextView) itemView.findViewById(R.id.tv_code_store_name);
            mTvStoreManager = (TextView) itemView.findViewById(R.id.tv_code_store_manager);
        }
    }

}