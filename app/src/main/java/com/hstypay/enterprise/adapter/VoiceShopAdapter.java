package com.hstypay.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.utils.Constants;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class VoiceShopAdapter extends RecyclerView.Adapter<VoiceShopAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    Context mContext;
    List<VoiceBean.DataBean> mList;
    private String mPushClose;
    private int mIntentType;//个推类型 1-收款语音推送；2-到家语音订单推送


    public VoiceShopAdapter(Context context, List<VoiceBean.DataBean> list,int intentType) {
        this.mContext = context;
        this.mList = list;
        mIntentType = intentType;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public VoiceShopAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_voice_shop, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final VoiceShopAdapter.HomeViewHolder holder,  int position) {
        if (mList == null)
            return;
        final VoiceBean.DataBean dataBean = mList.get(position);
        if (dataBean != null) {
            if (mIntentType == Constants.INTENT_VALUE_VOICE_COLLECTION ){
                holder.mTvVoiceShop.setText(dataBean.getMerchantName());
            }else if (mIntentType == Constants.INTENT_VALUE_VOICE_ORDER_DAOJIA ){
                holder.mTvVoiceShop.setText(dataBean.getStoreMerchantName());
            }
            mPushClose = dataBean.getPushClose() + "";
            if (mPushClose.equals("1")) {
                //开
                holder.mIvVoiceShop.setVisibility(View.VISIBLE);
            } else {//0关
                holder.mIvVoiceShop.setVisibility(View.INVISIBLE);
            }
        }
        holder.mViewLine.setVisibility(position == mList.size()-1 ? View.INVISIBLE : View.VISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.getAdapterPosition());
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvVoiceShop;
        private ImageView mIvVoiceShop;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvVoiceShop = (TextView) itemView.findViewById(R.id.tv_voice_shop);
            mIvVoiceShop = (ImageView) itemView.findViewById(R.id.iv_voice_shop);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}