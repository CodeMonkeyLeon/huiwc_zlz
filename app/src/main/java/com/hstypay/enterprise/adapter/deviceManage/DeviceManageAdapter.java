package com.hstypay.enterprise.adapter.deviceManage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class DeviceManageAdapter extends RecyclerView.Adapter<DeviceManageAdapter.HomeViewHolder> {

    private Context mContext;
    private List<DeviceBean> mList;
    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public DeviceManageAdapter(Context context, List<DeviceBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public DeviceManageAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_device, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final DeviceManageAdapter.HomeViewHolder holder, final int position) {
        if (mList != null && mList.size() > 0) {
            holder.mTvDeviceStore.setText(mList.get(position).getStoreMerchantIdCnt());
            holder.mTvDeviceType.setText("设备类型：" + mList.get(position).getCategoryName());
            holder.mTvDeviceNo.setText("设备SN码：" + mList.get(position).getSn());
            holder.itemView.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
            switch (mList.get(position).getCategoryCode()) {
                case "HAPY_POS"://扫码pos
                    Picasso.get().load(R.mipmap.icon_receive_device).into(holder.mImgIcon);
                    break;
                case "HPAY_POS"://智能pos
                    Picasso.get().load(R.mipmap.icon_item_pos).into(holder.mImgIcon);
                    break;
                case "HPAY_FACEPAY"://刷脸终端
                    Picasso.get().load(R.mipmap.icon_item_face_device).into(holder.mImgIcon);
                    break;
                case "CLOUD_BOX"://云音箱
                    Picasso.get().load(R.mipmap.icon_item_cloud).into(holder.mImgIcon);
                    break;
                case "CLOUD_PRINT"://云打印
                    Picasso.get().load(R.mipmap.icon_item_printer_device).into(holder.mImgIcon);
                    break;
                case "HPAY_RAM"://动态台卡
                    Picasso.get().load(R.mipmap.icon_item_dynamic).into(holder.mImgIcon);
                    break;
                default:
                    Picasso.get().load(Constants.H5_BASE_URL + mList.get(position).getImages()).error(R.mipmap.icon_item_cloud).placeholder(R.mipmap.icon_item_cloud).into(holder.mImgIcon);
                    break;
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDeviceStore, mTvDeviceType, mTvDeviceNo, mTvDeviceKey;
        private ImageView mImgIcon;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvDeviceStore = (TextView) itemView.findViewById(R.id.tv_device_store);
            mTvDeviceType = (TextView) itemView.findViewById(R.id.tv_device_type);
            mTvDeviceNo = (TextView) itemView.findViewById(R.id.tv_device_no);
            mTvDeviceKey = (TextView) itemView.findViewById(R.id.tv_device_key);
            mImgIcon = itemView.findViewById(R.id.iv_item_logo);
        }
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}