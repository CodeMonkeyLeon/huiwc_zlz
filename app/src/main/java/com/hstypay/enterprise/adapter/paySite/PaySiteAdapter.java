package com.hstypay.enterprise.adapter.paySite;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.paySite.PaySiteBean;
import com.hstypay.enterprise.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class PaySiteAdapter extends RecyclerView.Adapter<PaySiteAdapter.HomeViewHolder> {

    private Context mContext;
    private List<PaySiteBean> mList;

    public PaySiteAdapter(Context context, List<PaySiteBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public PaySiteAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_pay_site, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(PaySiteAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mTvSiteName.setText(mList.get(position).getCashPointName());
        if (TextUtils.isEmpty(mList.get(position).getStoreName())) {
            holder.mTvStoreName.setVisibility(View.GONE);
        } else {
            holder.mTvStoreName.setVisibility(View.VISIBLE);
        }
        holder.mTvStoreName.setText(mList.get(position).getStoreName());
        SpannableStringBuilder spannableString = new SpannableStringBuilder();
        spannableString.append("已关联" + mList.get(position).getRelateNum() + "设备");
        AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(UIUtils.dp2px(17));
        spannableString.setSpan(absoluteSizeSpan, 3, spannableString.length() - 2, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        holder.mTvLinkCount.setText(spannableString);
        if ("0".equals(mList.get(position).getRelateNum())) {
            holder.mTvLinkCount.setTextColor(UIUtils.getColor(R.color.home_value_text));
        } else {
            holder.mTvLinkCount.setTextColor(UIUtils.getColor(R.color.theme_color));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvSiteName, mTvStoreName, mTvLinkCount;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvSiteName = itemView.findViewById(R.id.tv_site_name);
            mTvStoreName = itemView.findViewById(R.id.tv_store_name);
            mTvLinkCount = itemView.findViewById(R.id.tv_link_count);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}