package com.hstypay.enterprise.adapter.paySite;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.paySite.SiteDeviceBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class SiteDetailAdapter extends RecyclerView.Adapter<SiteDetailAdapter.HomeViewHolder> {

    private Context mContext;
    private List<SiteDeviceBean> mList;

    public SiteDetailAdapter(Context context, List<SiteDeviceBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    private OnItemCancelClickListener mOnItemCancelClickListener;

    public interface OnItemCancelClickListener {
        void onItemCancelClick(int position);
    }

    public void setOnItemCancelClickListener(OnItemCancelClickListener listener) {
        this.mOnItemCancelClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public SiteDetailAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_site_detail, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(SiteDetailAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        if ("1".equals(mList.get(position).getDeviceClass())) {
            holder.mTvDeviceName.setText(mList.get(position).getUnitType() + " " + mList.get(position).getModel());
            holder.mTvDeviceNo.setText("SN：" + mList.get(position).getSn());
        } else if ("2".equals(mList.get(position).getDeviceClass())) {
            holder.mTvDeviceName.setText(mList.get(position).getModel());
            holder.mTvDeviceNo.setText(mList.get(position).getDeviceId());
        }
        switch (mList.get(position).getDeviceType()) {
            case "1":
            case "2":
                holder.mIvDevice.setImageResource(R.mipmap.img_store_code);
                break;
            case "3":
                holder.mIvDevice.setImageResource(R.mipmap.img_update_cade);
                break;
            case "4":
                holder.mIvDevice.setImageResource(R.mipmap.img_huabei);
                break;
            case "5":
            case "7":
                holder.mIvDevice.setImageResource(R.mipmap.img_receive_pos);
                break;
            case "6":
                holder.mIvDevice.setImageResource(R.mipmap.img_plug);
                break;
        }
        holder.mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemCancelClickListener != null) {
                    mOnItemCancelClickListener.onItemCancelClick(position);
                }
            }
        });
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDeviceName, mTvDeviceNo, mTvCancel;
        private ImageView mIvDevice;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvDeviceName = itemView.findViewById(R.id.tv_device_name);
            mTvDeviceNo = itemView.findViewById(R.id.tv_device_no);
            mIvDevice = itemView.findViewById(R.id.iv_device);
            mTvCancel = itemView.findViewById(R.id.tv_cancel_link);
        }
    }

}