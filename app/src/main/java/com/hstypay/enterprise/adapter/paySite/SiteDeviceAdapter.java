package com.hstypay.enterprise.adapter.paySite;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.paySite.SiteDeviceBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class SiteDeviceAdapter extends RecyclerView.Adapter<SiteDeviceAdapter.HomeViewHolder> {

    private Context mContext;
    private List<SiteDeviceBean> mList;
    private String mDeviceClass;

    public SiteDeviceAdapter(Context context, List<SiteDeviceBean> list, String deviceClass) {
        this.mContext = context;
        this.mList = list;
        this.mDeviceClass = deviceClass;
    }

    private OnItemCancelClickListener mOnItemCancelClickListener;

    public interface OnItemCancelClickListener {
        void onItemCancelClick(int position, boolean isCheck);
    }

    public void setOnItemCancelClickListener(OnItemCancelClickListener listener) {
        this.mOnItemCancelClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public SiteDeviceAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_device_link, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(SiteDeviceAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mIvChoice.setImageResource(mList.get(position).isChecked() ? R.mipmap.ic_radio_selected : R.mipmap.ic_radio_default);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemCancelClickListener != null) {
                    if (mList.get(position).isChecked()) {
                        mList.get(position).setChecked(false);
                    } else {
                        mList.get(position).setChecked(true);
                    }
                    notifyDataSetChanged();
                    mOnItemCancelClickListener.onItemCancelClick(position, mList.get(position).isChecked());
                }
            }
        });
        if ("1".equals(mDeviceClass)) {
            StringBuilder builder = new StringBuilder();
            if (!TextUtils.isEmpty(mList.get(position).getUnitType())) {
                builder.append(mList.get(position).getUnitType());
                builder.append(" ");
            }
            if (!TextUtils.isEmpty(mList.get(position).getModel())) {
                builder.append(mList.get(position).getModel());
            }
            holder.mTvDeviceName.setText(builder.toString());
            holder.mTvDeviceNo.setText("SN：" + mList.get(position).getSn());
        } else if ("2".equals(mDeviceClass)) {
            holder.mTvDeviceName.setText(mList.get(position).getModel());
            holder.mTvDeviceNo.setText(mList.get(position).getDeviceId());
        }
        switch (mList.get(position).getDeviceType()) {
            case "1":
                holder.mIvDevice.setImageResource(R.mipmap.img_store_code);
                break;
            case "2":
                holder.mIvDevice.setImageResource(R.mipmap.img_store_code);
                break;
            case "3":
                holder.mIvDevice.setImageResource(R.mipmap.img_update_cade);
                break;
            case "4":
                holder.mIvDevice.setImageResource(R.mipmap.img_huabei);
                break;
            case "5":
            case "7":
                holder.mIvDevice.setImageResource(R.mipmap.img_receive_pos);
                break;
            case "6":
                holder.mIvDevice.setImageResource(R.mipmap.img_plug);
                break;
        }
        if ("1".equals(mList.get(position).getBindFlag())) {
            holder.mTvLinkStatus.setBackgroundResource(R.drawable.shape_link_gray);
            holder.mTvLinkStatus.setText(mList.get(position).getCashPointName());
        } else {
            holder.mTvLinkStatus.setBackgroundResource(R.drawable.shape_link_green);
            holder.mTvLinkStatus.setText("未关联");
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvDeviceName, mTvDeviceNo, mTvLinkStatus;
        private ImageView mIvDevice, mIvChoice;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvDeviceName = itemView.findViewById(R.id.tv_device_name);
            mTvDeviceNo = itemView.findViewById(R.id.tv_device_no);
            mIvDevice = itemView.findViewById(R.id.iv_device);
            mIvChoice = itemView.findViewById(R.id.iv_choice);
            mTvLinkStatus = itemView.findViewById(R.id.tv_link_status);
        }
    }

}