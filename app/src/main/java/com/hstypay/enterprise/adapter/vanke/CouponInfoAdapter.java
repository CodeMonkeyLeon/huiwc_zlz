package com.hstypay.enterprise.adapter.vanke;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class CouponInfoAdapter extends RecyclerView.Adapter<CouponInfoAdapter.HomeViewHolder> {
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<CouponInfoData> mList;

    public CouponInfoAdapter(Context context, List<CouponInfoData> couponList) {
        this.mContext = context;
        this.mList = couponList;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);

        void onItemAddClick(int position);

        void onItemReduceClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public CouponInfoAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_coupon_info, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final CouponInfoAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        CouponInfoData couponInfoData = mList.get(position);
        String enableTime = DateUtil.formartDate(couponInfoData.getEnableTime(), "yyyy-MM-dd HH:mm:ss", "yyyy.MM.dd");
        String overdueTime = DateUtil.formartDate(couponInfoData.getOverdueTime(), "yyyy-MM-dd HH:mm:ss", "yyyy.MM.dd");
        holder.mTvCouponIndate.setText("有效期：" + enableTime + "至" + overdueTime);
        if (TextUtils.isEmpty(couponInfoData.getCouponDescribe())) {
            holder.mTvCouponLimit.setVisibility(View.GONE);
        } else {
            holder.mTvCouponLimit.setVisibility(View.VISIBLE);
            holder.mTvCouponLimit.setText(couponInfoData.getCouponDescribe());
        }
        holder.mTvCouponName.setText(StringUtils.ellipsisString(couponInfoData.getCouponName(), 22));
        if (couponInfoData.getPartTimeList() != null && couponInfoData.getPartTimeList().size() > 0) {
            holder.mTvCouponLimitTime.setVisibility(View.VISIBLE);
            if (couponInfoData.getPartTimeList().size() == 7) {
                holder.mTvCouponLimitTime.setText("周一至周日可用");
            } else {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < couponInfoData.getPartTimeList().size(); i++) {
                    switch (couponInfoData.getPartTimeList().get(i)) {
                        case 1:
                            builder.append("周一");
                            break;
                        case 2:
                            builder.append("周二");
                            break;
                        case 3:
                            builder.append("周三");
                            break;
                        case 4:
                            builder.append("周四");
                            break;
                        case 5:
                            builder.append("周五");
                            break;
                        case 6:
                            builder.append("周六");
                            break;
                        case 7:
                            builder.append("周日");
                            break;
                    }
                    if (i != couponInfoData.getPartTimeList().size() - 1)
                        builder.append("、");
                }
                builder.append("可用");
                holder.mTvCouponLimitTime.setText(builder.toString());
            }
        } else {
            holder.mTvCouponLimitTime.setVisibility(View.GONE);
        }
        //1-代金券;2-折扣券;3-兑换券;4-优惠券;5-停车券;6-团购券
        String couponType = "";
        switch (couponInfoData.getCouponType()) {
            case 1:
                couponType = "代金券";
                holder.mIvExchangeCoupon.setVisibility(View.GONE);
                holder.mLlLeft.setVisibility(View.VISIBLE);
                holder.mTvCouponMark.setVisibility(View.VISIBLE);
                holder.mTvCouponDiscount.setVisibility(View.GONE);
                holder.mTvCouponValue.setText(new BigDecimal(couponInfoData.getReduceMoney()).divide(new BigDecimal(100)).toString());
                break;
            case 2:
                couponType = "折扣券";
                holder.mIvExchangeCoupon.setVisibility(View.GONE);
                holder.mLlLeft.setVisibility(View.VISIBLE);
                holder.mTvCouponMark.setVisibility(View.GONE);
                holder.mTvCouponDiscount.setVisibility(View.VISIBLE);
                holder.mTvCouponValue.setText(new BigDecimal(couponInfoData.getDiscountAmount()).divide(new BigDecimal(100)).toString());
                break;
            case 3:
                couponType = "兑换券";
                holder.mIvExchangeCoupon.setVisibility(View.VISIBLE);
                holder.mLlLeft.setVisibility(View.GONE);
                holder.mTvCouponValue.setText(new BigDecimal(couponInfoData.getReduceMoney()).divide(new BigDecimal(100)).toString());
                break;
            case 4:
                couponType = "优惠券";
                holder.mIvExchangeCoupon.setVisibility(View.GONE);
                holder.mLlLeft.setVisibility(View.VISIBLE);
                holder.mTvCouponMark.setVisibility(View.VISIBLE);
                holder.mTvCouponDiscount.setVisibility(View.GONE);
                holder.mTvCouponValue.setText(new BigDecimal(couponInfoData.getReduceMoney()).divide(new BigDecimal(100)).toString());
                break;
            case 5:
                couponType = "停车券";
                holder.mTvCouponMark.setVisibility(View.VISIBLE);
                holder.mTvCouponDiscount.setVisibility(View.GONE);
                holder.mTvCouponValue.setText(new BigDecimal(couponInfoData.getReduceMoney()).divide(new BigDecimal(100)).toString());
                break;
            case 6:
                couponType = "团购券";
                holder.mIvExchangeCoupon.setVisibility(View.GONE);
                holder.mLlLeft.setVisibility(View.VISIBLE);
                holder.mTvCouponMark.setVisibility(View.VISIBLE);
                holder.mTvCouponDiscount.setVisibility(View.GONE);
                holder.mTvCouponValue.setText(new BigDecimal(couponInfoData.getReduceMoney()).divide(new BigDecimal(100)).toString());
                break;
        }
        holder.mTvCouponType.setText(couponType);
        if (mList.get(position).getInterProviderType() == 1) {
            holder.mIvCoupon.setVisibility(View.GONE);
            holder.mLlCouponCount.setVisibility(View.VISIBLE);
            if (mList.get(position).getNeedUseCount() == 0) {
                holder.mIvCouponReduce.setAlpha(0.4f);
                holder.mIvCouponReduce.setEnabled(false);
            } else {
                holder.mIvCouponReduce.setAlpha(1f);
                holder.mIvCouponReduce.setEnabled(true);
            }
            if (mList.get(position).getNeedUseCount() == mList.get(position).getCanUseCount()) {
                holder.mIvCouponAdd.setAlpha(0.4f);
                holder.mIvCouponAdd.setEnabled(false);
            } else {
                holder.mIvCouponAdd.setAlpha(1f);
                holder.mIvCouponAdd.setEnabled(true);
            }
            holder.mIvCouponAdd.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemAddClick(position);
                }
            });
            holder.mIvCouponReduce.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemReduceClick(position);
                }
            });
            holder.mTvCouponCount.setText(mList.get(position).getNeedUseCount() + "");
        } else {
            holder.mIvCoupon.setVisibility(View.VISIBLE);
            holder.mLlCouponCount.setVisibility(View.GONE);
            holder.mIvCoupon.setImageResource(mList.get(position).isChecked() ? R.mipmap.ic_radio_selected : R.mipmap.ic_radio_default);
            holder.mIvCoupon.setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvCouponMark, mTvCouponValue, mTvCouponDiscount, mTvCouponLimit, mTvCouponName, mTvCouponType, mTvCouponIndate, mTvCouponLimitTime, mTvCouponCount;
        private ImageView mIvCoupon, mIvCouponReduce, mIvCouponAdd, mIvExchangeCoupon;
        private LinearLayout mLlCouponCount, mLlLeft;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvCouponMark = itemView.findViewById(R.id.tv_coupon_mark);
            mTvCouponValue = itemView.findViewById(R.id.tv_coupon_value);
            mTvCouponDiscount = itemView.findViewById(R.id.tv_coupon_discount);
            mTvCouponLimit = itemView.findViewById(R.id.tv_coupon_limit);
            mTvCouponName = itemView.findViewById(R.id.tv_coupon_name);
            mTvCouponType = itemView.findViewById(R.id.tv_coupon_type);
            mTvCouponIndate = itemView.findViewById(R.id.tv_coupon_indate);
            mTvCouponLimitTime = itemView.findViewById(R.id.tv_coupon_limit_time);
            mIvCoupon = itemView.findViewById(R.id.iv_coupon);
            mIvCouponReduce = itemView.findViewById(R.id.iv_coupon_reduce);
            mIvCouponAdd = itemView.findViewById(R.id.iv_coupon_add);
            mTvCouponCount = itemView.findViewById(R.id.tv_coupon_count);
            mLlCouponCount = itemView.findViewById(R.id.ll_coupon_count);
            mLlLeft = itemView.findViewById(R.id.ll_left);
            mIvExchangeCoupon = itemView.findViewById(R.id.iv_exchange_coupon);
        }
    }
}