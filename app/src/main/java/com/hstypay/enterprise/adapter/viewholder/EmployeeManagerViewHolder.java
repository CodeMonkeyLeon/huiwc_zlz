package com.hstypay.enterprise.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.EmployeeManagerBean;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.StringUtils;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 员工管理ViewHolder
 */
public class EmployeeManagerViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;
    private TextView mTvNameEmployeeManage;//员工名称
    private TextView mTvRoleEmployeeManager;//员工角色
    private TextView mTvStoreNameEmployeeName;//员工所属店的名称
    private TextView mTvTelPhoneEmployeeManager;//电话号码
    private ImageView mIvActivate;

    public EmployeeManagerViewHolder(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;
        mTvNameEmployeeManage = itemView.findViewById(R.id.tv_name_employee_manage);
        mTvRoleEmployeeManager = itemView.findViewById(R.id.tv_role_employee_manager);
        mTvStoreNameEmployeeName = itemView.findViewById(R.id.tv_store_name_employee_name);
        mTvTelPhoneEmployeeManager = itemView.findViewById(R.id.tv_tel_phone_employee_manager);
        mIvActivate = itemView.findViewById(R.id.iv_activate);
    }

    public void setData(EmployeeManagerBean.EmployeeManagerData employeeManageItemBean) {
        if (employeeManageItemBean != null) {
            mTvNameEmployeeManage.setText(employeeManageItemBean.getUserIdCnt());
            int role = employeeManageItemBean.getRole();
            if (role == Constants.ROLE_STORE_MANAGER) {
                //店长
                mTvRoleEmployeeManager.setText(mContext.getResources().getString(R.string.string_role_store_manager));
                mTvRoleEmployeeManager.setTextColor(mContext.getResources().getColor(R.color.color_4272ee));
                mTvRoleEmployeeManager.setBackgroundResource(R.drawable.shape_store_manage_state0);
            } else if (role == Constants.ROLE_STORE_CASHIER) {
                //收银员
                mTvRoleEmployeeManager.setText(mContext.getResources().getString(R.string.string_role_store_cashier));
                mTvRoleEmployeeManager.setTextColor(mContext.getResources().getColor(R.color.color_08cc9e));
                mTvRoleEmployeeManager.setBackgroundResource(R.drawable.shape_store_manage_state1);
            } else {
                //其它
                mTvRoleEmployeeManager.setText(employeeManageItemBean.getEmpName());
                mTvRoleEmployeeManager.setTextColor(mContext.getResources().getColor(R.color.color_fc5a45));
                mTvRoleEmployeeManager.setBackgroundResource(R.drawable.shape_store_manage_state2);
            }
            mTvStoreNameEmployeeName.setText(employeeManageItemBean.getStoreName());
            mTvTelPhoneEmployeeManager.setText(StringUtils.hideMemberNum(employeeManageItemBean.getPhone()));
            if (employeeManageItemBean.getActivateStatus() == 1) {
                mIvActivate.setImageResource(R.mipmap.icon_activated);
            } else {
                mIvActivate.setImageResource(R.mipmap.icon_unactivate);
            }
        }
    }
}
