package com.hstypay.enterprise.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页BaseViewHolder
 */
public abstract class HomeFragmentBaseViewHolder<T> extends RecyclerView.ViewHolder {
    public HomeFragmentBaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void setData(T t);

}
