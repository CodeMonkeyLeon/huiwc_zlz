package com.hstypay.enterprise.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.HomeFuncionMenuRvAdapter;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.editmenu.entity.MenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页功能菜单ViewHolder
 */
public class HomeFragmentFunctionViewHolder extends HomeFragmentBaseViewHolder<List<List<MenuItem>>> {

    private ViewPager mVpItemHomeFunction;
    private HomeFunctionVpAdapter mVpAdapter;
    private Context mContext;
    private MenuItem.OnHomeMenuItemClickListener mOnHomeMenuItemClickListener;
    private final LinearLayout mLlPointsVp;
    private final View mViewPoint0;
    private final View mViewPoint1;
    private final RelativeLayout mRlContent;//内容区
    private final View mViewGapFunction;//空白占位区

    public HomeFragmentFunctionViewHolder(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;
        mRlContent = itemView.findViewById(R.id.rl_content_function);
        mViewGapFunction = itemView.findViewById(R.id.view_gap_function);
        mVpItemHomeFunction = itemView.findViewById(R.id.vp_item_home_function);
        mLlPointsVp = itemView.findViewById(R.id.ll_points_vp);
        mViewPoint0 = itemView.findViewById(R.id.view_point1);
        mViewPoint1 = itemView.findViewById(R.id.view_point2);
        mVpAdapter = new HomeFunctionVpAdapter();
        mVpItemHomeFunction.setAdapter(mVpAdapter);

    }

    public void setOnHomeMenuItemClickListener(MenuItem.OnHomeMenuItemClickListener onHomeMenuItemClickListener) {
        mOnHomeMenuItemClickListener = onHomeMenuItemClickListener;
    }

    @Override
    public void setData(List<List<MenuItem>> llMenuBean) {
        if (llMenuBean == null || llMenuBean.size() == 0 || llMenuBean.get(0) == null || llMenuBean.get(0).size() < 2) {//小于2是因为只有一个更多按钮的菜单
            mRlContent.setVisibility(View.GONE);//不显示菜单栏
            mViewGapFunction.setVisibility(View.VISIBLE);
        } else {
            mRlContent.setVisibility(View.VISIBLE);
            mViewGapFunction.setVisibility(View.GONE);
            mVpAdapter.setData(llMenuBean);
            if (llMenuBean == null || llMenuBean.size() < 2) {
                mLlPointsVp.setVisibility(View.GONE);//点点不显示
            } else {
                //最多10个菜单，2页。
                mLlPointsVp.setVisibility(View.VISIBLE);
                mVpItemHomeFunction.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {

                    }

                    @Override
                    public void onPageSelected(int i) {
                        if (i == 0) {
                            mViewPoint0.setBackground(mContext.getResources().getDrawable(R.drawable.shape_blue_select_point_vp));
                            mViewPoint1.setBackground(mContext.getResources().getDrawable(R.drawable.shape_blue_unselect_point_vp));
                        } else {
                            mViewPoint1.setBackground(mContext.getResources().getDrawable(R.drawable.shape_blue_select_point_vp));
                            mViewPoint0.setBackground(mContext.getResources().getDrawable(R.drawable.shape_blue_unselect_point_vp));
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });
            }
        }


    }

    //首页功能菜单viewpager控件的适配器
    public class HomeFunctionVpAdapter extends PagerAdapter {

        private List<List<MenuItem>> mLlMenuBean = new ArrayList<>();

        @Override
        public int getCount() {
            return mLlMenuBean.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_vp_home_function, null);
            RecyclerView recyclerview_vp_home = view.findViewById(R.id.recyclerview_vp_home);
            recyclerview_vp_home.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false) {
                @Override
                public boolean canScrollHorizontally() {
                    return false;
                }
            });
            if (recyclerview_vp_home.getAdapter() == null) {
                HomeFuncionMenuRvAdapter homeFuncionMenuRvAdapter = new HomeFuncionMenuRvAdapter(mContext);
                recyclerview_vp_home.setAdapter(homeFuncionMenuRvAdapter);
            }
            HomeFuncionMenuRvAdapter homeFuncionMenuRvAdapter = (HomeFuncionMenuRvAdapter) recyclerview_vp_home.getAdapter();
            homeFuncionMenuRvAdapter.setData(mLlMenuBean.get(position));
            homeFuncionMenuRvAdapter.setOnHomeMenuItemClickListener(mOnHomeMenuItemClickListener);

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getItemPosition(Object object) {
            // 简单解决 notifyDataSetChanged() 页面不刷新问题的方法
            return POSITION_NONE;
        }

        public void setData(List<List<MenuItem>> llMenuBean) {
            mLlMenuBean.clear();
            notifyDataSetChanged();
            if (llMenuBean != null && llMenuBean.size() > 0) {
                mLlMenuBean.addAll(llMenuBean);
                notifyDataSetChanged();
            }
        }

    }

}
