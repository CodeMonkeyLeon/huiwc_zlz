package com.hstypay.enterprise.adapter.viewholder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页头部功能
 */
public class HomeFragmentHeaderViewHolder extends HomeFragmentBaseViewHolder<String> {

    public   TextView mTvMerchantNameHome;
    public   ImageView mIvNoticeHeaderHome;//客服
    public   ImageView mIvMoneyHeaderHome;//收款
    public  ImageView mIvScanHeaderHome;//扫码
    public  ImageView mIvUpdateNotice;//平台升级提示

    public HomeFragmentHeaderViewHolder(@NonNull View itemView) {
        super(itemView);//item_home_fragment_header
        mTvMerchantNameHome = itemView.findViewById(R.id.tv_merchant_name_home);
        mIvNoticeHeaderHome = itemView.findViewById(R.id.iv_notice_header_home);
        mIvMoneyHeaderHome = itemView.findViewById(R.id.iv_money_header_home);
        mIvScanHeaderHome = itemView.findViewById(R.id.iv_scan_header_home);
        mIvUpdateNotice = itemView.findViewById(R.id.iv_update_notice);
    }

    public void setVisible (int visibility){
        mIvUpdateNotice.setVisibility(visibility);
    }

    @Override
    public void setData(String name) {
        mTvMerchantNameHome.setText(name);
        /*
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_COUNT)) {
            mIvMoneyHeaderHome.setVisibility(View.VISIBLE);
        } else {
            mIvMoneyHeaderHome.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_SCAN)) {
            mIvScanHeaderHome.setVisibility(View.VISIBLE);
        } else {
            mIvScanHeaderHome.setVisibility(View.GONE);
        }
        */

        mIvNoticeHeaderHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeHeaderIconClickListener!=null){
                    mOnHomeHeaderIconClickListener.onClickNotice();
                }
            }
        });
        mIvMoneyHeaderHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeHeaderIconClickListener!=null){
                    mOnHomeHeaderIconClickListener.onClickMoney();
                }
            }
        });
        mIvScanHeaderHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeHeaderIconClickListener!=null){
                    mOnHomeHeaderIconClickListener.onClickScan(mIvScanHeaderHome);
                }
            }
        });
        mIvUpdateNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeHeaderIconClickListener!=null){
                    mOnHomeHeaderIconClickListener.onClickUpdate();
                }
            }
        });
    }

    public interface OnHomeHeaderIconClickListener{
        void onClickNotice();
        void onClickUpdate();
        void onClickMoney();
        void onClickScan(View view);
    }
    public OnHomeHeaderIconClickListener mOnHomeHeaderIconClickListener;

    public void setOnHomeHeaderIconClickListener(OnHomeHeaderIconClickListener onHomeHeaderIconClickListener) {
        mOnHomeHeaderIconClickListener = onHomeHeaderIconClickListener;
    }
}
