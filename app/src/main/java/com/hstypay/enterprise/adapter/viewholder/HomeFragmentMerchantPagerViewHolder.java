package com.hstypay.enterprise.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.HomeMerchantNewsAdapter;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.bean.MchtalkBean;

import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页商家说
 */
public class HomeFragmentMerchantPagerViewHolder extends HomeFragmentBaseViewHolder<List<MchtalkBean.MchTalkItemBean>> {

    private Context mContext;
    private final RelativeLayout mRlMoreItemMerchantHome;
    private final RecyclerView mRvItemMerchantNews;
    private final HomeMerchantNewsAdapter mAdapter;
    private OnHomeMchTalkItemClickListener mOnHomeMchTalkItemClickListener;

    public void setOnHomeMchTalkItemClickListener(OnHomeMchTalkItemClickListener onHomeMchTalkItemClickListener) {
        mOnHomeMchTalkItemClickListener = onHomeMchTalkItemClickListener;
    }

    public HomeFragmentMerchantPagerViewHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;
        mRlMoreItemMerchantHome = itemView.findViewById(R.id.rl_more_item_merchant_home);
        mRvItemMerchantNews = itemView.findViewById(R.id.rv_item_merchant_news);
        LinearLayoutManager manager = new LinearLayoutManager(context){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        manager.setAutoMeasureEnabled(true);
        mRvItemMerchantNews.setLayoutManager(manager);
        mAdapter = new HomeMerchantNewsAdapter(context);
        mRvItemMerchantNews.setAdapter(mAdapter);

        mRlMoreItemMerchantHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeMchTalkItemClickListener!=null){
                    mOnHomeMchTalkItemClickListener.onMoreTalkClick();
                }
            }
        });

        mAdapter.setOnHomeMchTalkItemClickListener(new OnHomeMchTalkItemClickListener() {
            @Override
            public void onMchTalkItemClick(MchtalkBean.MchTalkItemBean mchTalkItemBean) {
                if (mOnHomeMchTalkItemClickListener!=null){
                    mOnHomeMchTalkItemClickListener.onMchTalkItemClick(mchTalkItemBean);
                }

            }

            @Override
            public void onMoreTalkClick() {

            }
        });
    }

    @Override
    public void setData( List<MchtalkBean.MchTalkItemBean> merChantNewsItemBeans) {
        mAdapter.setData(merChantNewsItemBeans,0);
    }

    //收款记录的条目点击事件
    public  interface OnHomeMchTalkItemClickListener{
        void onMchTalkItemClick(MchtalkBean.MchTalkItemBean mchTalkItemBean);
        void onMoreTalkClick();
    }
}
