package com.hstypay.enterprise.adapter.viewholder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.MessageData;

import java.util.List;

import static com.hstypay.enterprise.utils.DateUtil.homeMsgParseTime;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页的通知
 */
public class HomeFragmentNoticeViewHolder extends HomeFragmentBaseViewHolder<List<MessageData.DataEntity.DataList>> {

    private  LinearLayout mLlMsgTwo;
    private  TextView mTvMsgOneTitle;
    private  TextView mTvMsgOneTime;
    private  TextView mTvMsgTwoTitle;
    private  TextView mTvMsgTwoTime;
    public  LinearLayout mLlHomeMsg;
    public  ImageView mIvArrowNoticeHome;

    public HomeFragmentNoticeViewHolder(@NonNull View itemView) {
        super(itemView);//item_home_fragment_notice
        mLlHomeMsg = itemView.findViewById(R.id.ll_home_msg);
        mIvArrowNoticeHome = itemView.findViewById(R.id.iv_arrow_notice_home);
        mLlMsgTwo = itemView.findViewById(R.id.ll_msg_two);
        mTvMsgOneTitle = itemView.findViewById(R.id.tv_msg_one_title);
        mTvMsgOneTime = itemView.findViewById(R.id.tv_msg_one_time);
        mTvMsgTwoTitle = itemView.findViewById(R.id.tv_msg_two_title);
        mTvMsgTwoTime = itemView.findViewById(R.id.tv_msg_two_time);
    }

    @Override
    public void setData(List<MessageData.DataEntity.DataList> dataList) {
        if (dataList==null){
            return;
        }
        if (dataList.size() == 1) {
            mLlMsgTwo.setVisibility(View.GONE);
            if (dataList.get(0).getTitle() != null) {
                mTvMsgOneTitle.setText(dataList.get(0).getTitle());
            }
            if (dataList.get(0).getCreateTime() != null) {
                mTvMsgOneTime.setText(homeMsgParseTime(dataList.get(0).getCreateTime()));
            }
        } else {
            if (dataList.get(0).getTitle() != null) {
                mTvMsgOneTitle.setText(dataList.get(0).getTitle());
            }
            if (dataList.get(0).getCreateTime() != null) {
                mTvMsgOneTime.setText(homeMsgParseTime(dataList.get(0).getCreateTime()));
            }
            if (dataList.get(1).getTitle() != null) {
                mLlMsgTwo.setVisibility(View.VISIBLE);
                mTvMsgTwoTitle.setText(dataList.get(1).getTitle());
            }
            if (dataList.get(1).getCreateTime() != null) {
                mTvMsgTwoTime.setText(homeMsgParseTime(dataList.get(1).getCreateTime()));
            }
        }
    }

    public interface OnNoticeClickListener{
        void onNoticeClick();
    }
}
