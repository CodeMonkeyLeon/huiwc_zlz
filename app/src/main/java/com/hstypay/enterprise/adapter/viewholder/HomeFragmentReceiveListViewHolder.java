package com.hstypay.enterprise.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.adapter.HomeReceiveRecordsAdapter;
import com.hstypay.enterprise.bean.BillsBean;

import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页的收款记录ViewHolder
 */
public class HomeFragmentReceiveListViewHolder extends HomeFragmentBaseViewHolder<List<BillsBean.DataEntity>> {

    private  RecyclerView mRvItemReceiveRecords;
    private  HomeReceiveRecordsAdapter mAdapter;
    private  RelativeLayout mRlMoreItemReceiveHome;
    private HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener mOnHomeReceiveItemClickListener;
    private final TextView mTvNotData;//暂无数据
    private boolean showEmpty;//是否显示空视图里的文字，避免一开始就显示出一个空数据的UI，要等接口数据回来了再显示比较好
    private Context mContext;

    public void setShowEmpty(boolean showEmpty) {
        this.showEmpty = showEmpty;
    }

    public void setOnHomeReceiveItemClickListener(HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener onHomeReceiveItemClickListener) {
        mOnHomeReceiveItemClickListener = onHomeReceiveItemClickListener;
    }

    public HomeFragmentReceiveListViewHolder(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;
        mRlMoreItemReceiveHome = itemView.findViewById(R.id.rl_more_item_receive_home);
        mRvItemReceiveRecords = itemView.findViewById(R.id.rv_item_receive_records);
        mTvNotData = itemView.findViewById(R.id.tv_not_data_home);
        LinearLayoutManager manager = new LinearLayoutManager(context){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        manager.setAutoMeasureEnabled(true);
        mRvItemReceiveRecords.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new HomeReceiveRecordsAdapter(context);
        mRvItemReceiveRecords.setAdapter(mAdapter);

        mRlMoreItemReceiveHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnHomeReceiveItemClickListener!=null){
                    mOnHomeReceiveItemClickListener.onMoreRecordClick();
                }
            }
        });

        mAdapter.setOnHomeReceiveItemClickListener(new OnHomeReceiveItemClickListener() {
            @Override
            public void onReceiveRecordItemClick(BillsBean.DataEntity billData) {
                if (mOnHomeReceiveItemClickListener!=null){
                    mOnHomeReceiveItemClickListener.onReceiveRecordItemClick(billData);
                }
            }

            @Override
            public void onMoreRecordClick() {

            }
        });

    }

    @Override
    public void setData(List<BillsBean.DataEntity> billList) {
        if (billList==null || billList.size()==0){
            mRvItemReceiveRecords.setVisibility(View.GONE);
            mTvNotData.setVisibility(View.VISIBLE);
            if (showEmpty){
                mTvNotData.setText(mContext.getResources().getString(R.string.tv_moment_null));
            }else {
                mTvNotData.setText("");
            }
        }else {
            mTvNotData.setVisibility(View.GONE);
            mRvItemReceiveRecords.setVisibility(View.VISIBLE);
            mAdapter.setData(billList);
        }
    }

    //收款记录的条目点击事件
    public  interface OnHomeReceiveItemClickListener{
        void onReceiveRecordItemClick(BillsBean.DataEntity billData);
        void onMoreRecordClick();
    }
}
