package com.hstypay.enterprise.adapter.viewholder;

import android.content.Context;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hsty.charting.charts.BarChart;
import com.hsty.charting.components.Legend;
import com.hsty.charting.components.XAxis;
import com.hsty.charting.components.YAxis;
import com.hsty.charting.custom.DayAxisValueFormatter;
import com.hsty.charting.custom.MyAxisValueFormatter;
import com.hsty.charting.custom.MyValueFormatter;
import com.hsty.charting.custom.XYMarkerView;
import com.hsty.charting.data.BarData;
import com.hsty.charting.data.BarDataSet;
import com.hsty.charting.data.BarEntry;
import com.hsty.charting.data.Entry;
import com.hsty.charting.formatter.IAxisValueFormatter;
import com.hsty.charting.formatter.IValueFormatter;
import com.hsty.charting.highlight.Highlight;
import com.hsty.charting.interfaces.datasets.IBarDataSet;
import com.hsty.charting.listener.OnChartValueSelectedListener;
import com.hsty.charting.utils.MPPointF;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ReportTradeTrendBean;
import com.hstypay.enterprise.bean.TrendBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页收款统计图表
 */
public class HomeFragmentReceiveTableViewHolder extends HomeFragmentBaseViewHolder<ReportTradeTrendBean> implements OnChartValueSelectedListener {

    private Context mContext;
    public  RelativeLayout mRlMoreItemTableHome;
    private final TextView mTvDateTimeTableHome;
    private final TextView mTvTotalMoneyTableHome;
    private final BarChart chart;
    private Typeface mTfLight;
    private final TextView mTvMarkRmbTable;
    ArrayList<String> xx = new ArrayList<>();
    ArrayList<Float> yy = new ArrayList<>();

    public HomeFragmentReceiveTableViewHolder(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;
        mRlMoreItemTableHome = itemView.findViewById(R.id.rl_more_item_table_home);
        mTvDateTimeTableHome = itemView.findViewById(R.id.tv_date_time_table_home);
        mTvTotalMoneyTableHome = itemView.findViewById(R.id.tv_total_money_table_home);
        mTvMarkRmbTable = itemView.findViewById(R.id.tv_mark_rmb_table);
        chart = itemView.findViewById(R.id.chart_table_receive_home);

        initChart();

    }

    private void initChart() {
        chart.setOnChartValueSelectedListener(this);

        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);

        chart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawGridBackground(false);
//         chart.setDrawYLabels(false);

        chart.setScaleEnabled(false);

        mTfLight = Typeface.createFromAsset(mContext.getAssets(), "OpenSans-Light.ttf");

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart);//日期格式数字
        //x轴线
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
//        xAxis.setValueFormatter(xAxisFormatter);
        xAxis.setDrawAxisLine(false);

//kzy
        IAxisValueFormatter custom = new MyAxisValueFormatter();//金额格式数字
//        IAxisValueFormatter custom = new DefaultAxisValueFormatter(2);//金额格式数字
        //左轴线
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(6, true);//y轴强制6条线
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        //kzy
        leftAxis.setDrawAxisLine(false);

        //右轴线
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//kzy
        rightAxis.setEnabled(false);

        //底部额外标签
        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
//kzy
        l.setEnabled(false);

        //设置点击柱状图时的提示view
        XYMarkerView mv = new XYMarkerView(mContext, xAxisFormatter);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        //初始化显示
        // setting data
        xx.clear();
        for (int i=0;i<7;i++){
            xx.add("");
        }
        yy.clear();
        for (int i=0;i<xx.size();i++){//保证y的个数和x的个数一样
            yy.add(0f);
        }

        setChartData(xx,yy);

    }

    @Override
    public void setData(ReportTradeTrendBean reportTradeTrendBean) {
        if (reportTradeTrendBean!=null && reportTradeTrendBean.getData() != null && reportTradeTrendBean.getData().size() > 0) {
            List<ReportTradeTrendBean.DataBean> data = reportTradeTrendBean.getData();
            xx.clear();
            yy.clear();
            long successFee = 0l;
            for (int i=0;i<7;i++){
                try {
                    ReportTradeTrendBean.DataBean dataBean = data.get(i);//data的size不一定是7
                    if (dataBean!=null){
                        xx.add(DateUtil.formartDate(data.get(i).getPayTradeTime(), "yyyy-MM-dd", "MM.dd"));
                        yy.add(data.get(i).getSuccessFee()/ 100f);
                         successFee += data.get(i).getSuccessFee();
                    }else {
                        xx.add("");
                        yy.add(0f);
                    }
                }catch (Exception e){
                    xx.add("");
                    yy.add(0f);
                }

            }
            setChartData(xx,yy);
            /*String x0 = chart.getXAxis().getXx().get(0);
            if (!TextUtils.isEmpty(x0)){
                mTvDateTimeTableHome.setVisibility(View.VISIBLE);
                mTvMarkRmbTable.setVisibility(View.VISIBLE);
                mTvTotalMoneyTableHome.setVisibility(View.VISIBLE);
                mTvDateTimeTableHome.setText(chart.getXAxis().getXx().get(0));
                mTvTotalMoneyTableHome.setText(chart.getAxisLeft().getValueFormatter().getFormattedValue(data.get(0).getSuccessFee()/ 100f,chart.getAxisLeft()));
            }else {
                mTvTotalMoneyTableHome.setVisibility(View.INVISIBLE);
                mTvDateTimeTableHome.setVisibility(View.INVISIBLE);
                mTvMarkRmbTable.setVisibility(View.INVISIBLE);
            }*/

            ReportTradeTrendBean.DataBean dataBean0 = data.get(0);
            ReportTradeTrendBean.DataBean dataBean7 = data.get(data.size() - 1);
            String startTime = DateUtil.formartDate(dataBean0.getPayTradeTime(), "yyyy-MM-dd", "yyyy.MM.dd");
            String endTime = DateUtil.formartDate(dataBean7.getPayTradeTime(), "yyyy-MM-dd", "dd");
            mTvDateTimeTableHome.setVisibility(View.VISIBLE);
            mTvMarkRmbTable.setVisibility(View.VISIBLE);
            mTvTotalMoneyTableHome.setVisibility(View.VISIBLE);
            mTvDateTimeTableHome.setText(startTime+"-"+endTime);
            mTvTotalMoneyTableHome.setText((successFee/ 100f)+"");

        }else {
            mTvTotalMoneyTableHome.setVisibility(View.INVISIBLE);
            mTvDateTimeTableHome.setVisibility(View.INVISIBLE);
            mTvMarkRmbTable.setVisibility(View.INVISIBLE);
        }
    }

    private void setChartData(ArrayList<String> xx,ArrayList<Float> yy) {

        chart.getXAxis().setXX(xx);

        ArrayList<BarEntry> values = new ArrayList<>();

        for (int i=0;i<yy.size();i++){
            values.add(new BarEntry(i, yy.get(i)));
        }

        BarDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
            chart.postInvalidate();//kzy update markView
        } else {
            set1 = new BarDataSet(values);

            set1.setDrawIcons(false);

            set1.setColor(mContext.getResources().getColor(R.color.theme_color));

            set1.setHighLightColor(mContext.getResources().getColor(R.color.theme_color));
            set1.setHighLightAlpha(50);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            IValueFormatter custom = new MyValueFormatter();
            data.setValueFormatter(custom);
            data.setValueTextSize(10f);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.5f);

            chart.setData(data);
        }
    }


    private final RectF onValueSelectedRectF = new RectF();
    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;

        RectF bounds = onValueSelectedRectF;
        chart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = chart.getPosition(e, YAxis.AxisDependency.LEFT);
        MPPointF.recycleInstance(position);

        /*int index = (int) e.getX();
        String x0 = xx.get(index);
        Float y0 = yy.get(index);
        if (!TextUtils.isEmpty(x0)){
            mTvDateTimeTableHome.setVisibility(View.VISIBLE);
            mTvMarkRmbTable.setVisibility(View.VISIBLE);
            mTvTotalMoneyTableHome.setVisibility(View.VISIBLE);
            mTvDateTimeTableHome.setText(x0);
            mTvTotalMoneyTableHome.setText(chart.getAxisLeft().getValueFormatter().getFormattedValue(y0,chart.getAxisLeft()));
        }else {
            mTvTotalMoneyTableHome.setVisibility(View.INVISIBLE);
            mTvDateTimeTableHome.setVisibility(View.INVISIBLE);
            mTvMarkRmbTable.setVisibility(View.INVISIBLE);
        }*/

    }

    @Override
    public void onNothingSelected() {

    }

    public interface OnReceiveStatictisMoreClickListener{
        void onMoreReceiveClick();
    }
}
