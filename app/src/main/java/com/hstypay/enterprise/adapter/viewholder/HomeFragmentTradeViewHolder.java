package com.hstypay.enterprise.adapter.viewholder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.utils.DateUtil;

/**
 * @author kuangzeyu
 * @time 2021/3/10
 * @desc 首页交易概览
 */
public class HomeFragmentTradeViewHolder extends HomeFragmentBaseViewHolder<ReportBean> {

    private final TextView mTvValueTradeMoneyHome;//交易金额
    private final TextView mTvTradeCountHome;//交易笔数
    private final TextView mTvCustomerNumHome;//顾客人数

    public HomeFragmentTradeViewHolder(@NonNull View itemView) {
        super(itemView);//item_home_fragment_trade_overview
        mTvValueTradeMoneyHome = itemView.findViewById(R.id.tv_value_trade_money_home);
        mTvTradeCountHome = itemView.findViewById(R.id.tv_trade_count_home);
        mTvCustomerNumHome = itemView.findViewById(R.id.tv_customer_num_home);
    }

    @Override
    public void setData(ReportBean reportBean) {
        if (reportBean!=null){
            mTvValueTradeMoneyHome.setText(DateUtil.formatMoneyByLong(reportBean.getData().getSuccessFee()));
            mTvTradeCountHome.setText(""+reportBean.getData().getSuccessCount());
            mTvCustomerNumHome.setText(""+reportBean.getData().getCustomerCount());
        }
    }


    public interface OnTradeItemClickListener{
        void onTradeItemClick();
    }
}
