package com.hstypay.enterprise.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.hstypay.enterprise.R;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 门店员工标题栏
 */
public class StoreEmployeeTitleViewHolder extends RecyclerView.ViewHolder {

    private TextView mTvTitleNameEmployee;

    public StoreEmployeeTitleViewHolder(@NonNull View itemView) {
        super(itemView);
        mTvTitleNameEmployee = itemView.findViewById(R.id.tv_title_name_employee);
    }

    public void setData(String titleName) {
        if (!TextUtils.isEmpty(titleName)){
            mTvTitleNameEmployee.setText(titleName);
        }
    }
}
