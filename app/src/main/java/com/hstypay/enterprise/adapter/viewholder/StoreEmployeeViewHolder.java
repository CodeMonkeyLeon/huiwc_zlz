package com.hstypay.enterprise.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.StoreEmployeeBean;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 门店员工
 */
public class StoreEmployeeViewHolder extends RecyclerView.ViewHolder {

    private  TextView mTvNameEmployeeStore;
    private  TextView mTvTelPhoneEmployee;
    private  View mViewLineEmployee;

    public StoreEmployeeViewHolder(@NonNull View itemView) {
        super(itemView);
        mTvNameEmployeeStore = itemView.findViewById(R.id.tv_name_employee_store);
        mTvTelPhoneEmployee = itemView.findViewById(R.id.tv_tel_phone_employee);
        mViewLineEmployee = itemView.findViewById(R.id.view_line_employee);
    }

    public void setData(StoreEmployeeBean.Employee employee) {
        if (employee != null){
            mTvNameEmployeeStore.setText(employee.getEmployeeName());
            mTvTelPhoneEmployee.setText(employee.getTelPhone());
            mViewLineEmployee.setVisibility(employee.isShowLine()?View.VISIBLE:View.GONE);
        }
    }
}
