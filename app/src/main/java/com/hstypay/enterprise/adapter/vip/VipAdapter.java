package com.hstypay.enterprise.adapter.vip;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.bean.HSVipListBean;
import com.hstypay.enterprise.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 17:46
 * @描述: ${TODO}
 */

public class VipAdapter extends RecyclerView.Adapter<VipAdapter.HomeViewHolder> {
    private OnItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<HSVipListBean.DataEntity.DataListEntity> mList;

    public VipAdapter(Context context, List<HSVipListBean.DataEntity.DataListEntity> list) {
        this.mContext = context;
        this.mList = list;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public VipAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_vip, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final VipAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        HSVipListBean.DataEntity.DataListEntity dataEntity = mList.get(position);
        if (TextUtils.isEmpty(dataEntity.getName())) {
            holder.mTvVipName.setText("--");
        } else {
            holder.mTvVipName.setText(dataEntity.getName());
        }
        if (TextUtils.isEmpty(dataEntity.getLevelName())) {
            holder.mTvVipLevel.setText("--");
        } else {
            holder.mTvVipLevel.setText(dataEntity.getLevelName());
        }
        if (TextUtils.isEmpty(dataEntity.getMobile())) {
            holder.mTvVipPhone.setText("--");
        } else {
            holder.mTvVipPhone.setText(dataEntity.getMobile());
        }
        if(!TextUtils.isEmpty(dataEntity.getHeadSculpture())) {
            Picasso.get().load(dataEntity.getHeadSculpture()).placeholder(R.mipmap.pic_dog_dialog).error(R.mipmap.pic_dog_dialog).into(holder.mIvPhoto);
        } else {
            Picasso.get().load(R.mipmap.pic_dog_dialog).into(holder.mIvPhoto);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null)
                    mOnItemClickListener.onItemClick(position);
            }
        });
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvVipPhone, mTvVipLevel, mTvVipName;
        private ImageView mIvPhoto;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvVipPhone = itemView.findViewById(R.id.tv_vip_phone);
            mTvVipLevel = itemView.findViewById(R.id.tv_vip_level);
            mTvVipName = itemView.findViewById(R.id.tv_vip_name);
            mIvPhoto = itemView.findViewById(R.id.iv_photo);
        }
    }
}