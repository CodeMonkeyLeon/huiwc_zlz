package com.hstypay.enterprise.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.baidu.mobstat.StatService;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.BuildConfig;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.bean.BluetoothBondBean;
import com.hstypay.enterprise.network.MyTrustManager;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.PreferenceUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareConfig;
import com.v5kf.client.lib.Logger;
import com.v5kf.client.lib.V5ClientAgent;
import com.v5kf.client.lib.V5ClientConfig;
import com.v5kf.client.lib.callback.V5InitCallback;
import com.zhy.http.okhttp.OkHttpUtils;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import okhttp3.OkHttpClient;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.app
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 11:28
 * @描述: ${TODO}
 */
public class MyApplication extends Application {
    private int appCount = 0;
    private static Context mContext;
    private static MyApplication instance;
    private List<Activity> activities = new LinkedList<>();
    private static OkHttpClient okHttpClient;

    public static boolean isupdate = true;
    public static boolean iscache = false;
    public static boolean isRefresh = false;

    {
        PlatformConfig.setWeixin("wx6e411e48e4dacb00", "ee17bfb1d0e41d8a824b867e0c813f93");
        PlatformConfig.setQQZone("1106376240", "cOlj7Bw1m3vqyQS1");
        PlatformConfig.setSinaWeibo("", "", "");
    }

    // APP_ID 替换为你的应用从官方网站申请到的合法appID
    private static final String APP_ID = "wx88888888";

    // IWXAPI 是第三方app和微信通信的openApi接口
    private IWXAPI api;

    private void regToWx() {
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, APP_ID, true);

        // 将应用的appId注册到微信
        api.registerApp(APP_ID);

        //建议动态监听微信启动广播进行注册到微信
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // 将该app注册到微信
                api.registerApp(APP_ID);
            }
        }, new IntentFilter(ConstantsAPI.ACTION_REFRESH_WXAPP));

    }

    public MyApplication() {

    }

    public static MyApplication getInstance() {
        return instance;
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = getApplicationContext();
        try {
            if (iscache)
                return;
            AppHelper.getAppCacheDir();
            iscache = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        PreferenceUtil.init(mContext);
        SpUtil.init(mContext);
        SpStayUtil.init(mContext);

        //个推推送初始化
        PushManager.getInstance().initialize(this);

        initV5Client();
        OkHttpClient okHttpClient = initClient();
        OkHttpUtils.initClient(okHttpClient);
//        initStatCrash(mContext);

        initShare();
        registerActivityListener();
//        regToWx();
        StatService.setAuthorizedState(mContext, protocolShown());
        StatService.autoTrace(this, true, false);
    }

    /*@Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }*/

    /**
     * 初始化V5KF
     */
    private void initV5Client() {
        if (isMainProcess()) { // 判断为主进程，在主进程中初始化，多进程同时初始化可能导致不可预料的后果
            Logger.w("MyApplication", "onCreate isMainProcess V5ClientAgent.init");
            V5ClientConfig.FILE_PROVIDER = Constants.FILE_PROVIDER; // 设置fileprovider的authorities
            V5ClientAgent.init(this, "156230", "2624608012e41", new V5InitCallback() {

                @Override
                public void onSuccess(String response) {
                    // TODO Auto-generated method stub
                    Logger.i("MyApplication", "V5ClientAgent.init(): " + response);
                }

                @Override
                public void onFailure(String response) {
                    // TODO Auto-generated method stub
                    Logger.e("MyApplication", "V5ClientAgent.init(): " + response);
                }
            });
        }
    }

    public boolean isMainProcess() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = android.os.Process.myPid();
        for (RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

    private void initShare() {
        Config.DEBUG = true;
        UMShareConfig config = new UMShareConfig();
        config.isNeedAuthOnGetUserInfo(false);
        config.isOpenShareEditActivity(true);
        config.setSinaAuthType(UMShareConfig.AUTH_TYPE_SSO);
        config.setFacebookAuthType(UMShareConfig.AUTH_TYPE_SSO);
        config.setShareToLinkedInFriendScope(UMShareConfig.LINKED_IN_FRIEND_SCOPE_ANYONE);
        UMShareAPI.get(this);
    }

    /*public void initStatCrash(Context context) {
        StatCrashReporter crashReporter = StatCrashReporter.getStatCrashReporter(context);
        crashReporter.setEnableInstantReporting(true);
        crashReporter.setJavaCrashHandlerStatus(true);
        crashReporter.addCrashCallback(
                new StatCrashCallback() {
                    @Override
                    public void onJniNativeCrash(String nativeCrashStacks) { // native crash happened
                        // do something
                    }

                    @Override
                    public void onJavaCrash(Thread thread, Throwable ex) {// java crash happened
                        // do something
                        log("MTA StatCrashCallback onJavaCrash:\n" + ex);
                    }
                });
    }*/

    public OkHttpClient initClient() {
        if (okHttpClient == null) {
            MyTrustManager myTrustManager = new MyTrustManager();
            ClearableCookieJar cookieJar =
                    new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));
            okHttpClient = new OkHttpClient.Builder()
                    .cookieJar(cookieJar)
//                    .sslSocketFactory(getSSLSocketFactory(myTrustManager), myTrustManager)
                    //其他配置
                    .build();
        }
        return okHttpClient;
    }

    private SSLSocketFactory getSSLSocketFactory(MyTrustManager myTrustManager) {
        SSLContext context = null;
        try {
            context = SSLContext.getInstance("TLS");
            TrustManager[] trustManagers = {
                    myTrustManager
            };
            context.init(null, trustManagers, new SecureRandom());
            return context.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addActivity(Activity paramActivity) {
        this.activities.add(paramActivity);
    }

    public void finishAllActivity() {
        try {
            Iterator<Activity> localIterator = this.activities.iterator();
            while (localIterator.hasNext()) {
                localIterator.next().finish();
            }
            activities.clear();

        } catch (Exception localException) {
            localException.printStackTrace();
        }
        //android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void removeActivity(Activity paramActivity) {

        this.activities.remove(paramActivity);
    }

    private void registerActivityListener() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                                                   @Override
                                                   public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                                                       // TODO Auto-generated method stub
                                                   }

                                                   @Override
                                                   public void onActivityStarted(Activity activity) {
                                                       // TODO Auto-generated method stub
                                                       appCount++;
                                                   }

                                                   @Override
                                                   public void onActivityResumed(Activity activity) {
                                                       // TODO Auto-generated method stub
                                                   }

                                                   @Override
                                                   public void onActivityPaused(Activity activity) {
                                                       // TODO Auto-generated method stub
                                                   }

                                                   @Override
                                                   public void onActivityStopped(Activity activity) {
                                                       // TODO Auto-generated method stub
                                                       appCount--;
                                                       if (appCount == 0) {
                                                           MyToast.showToastShort(UIUtils.getString(R.string.toast_out_app));
                                                       }
                                                   }

                                                   @Override
                                                   public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                                                       // TODO Auto-generated method stub
                                                   }

                                                   @Override
                                                   public void onActivityDestroyed(Activity activity) {
                                                       // TODO Auto-generated method stub
                                                   }

                                               }

            );
        }
    }


    /**
     * 蓝牙打印设置状态
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setBluePrintSetting(Boolean sign) {
        PreferenceUtil.commitBoolean("BluePrintSetting", sign);
        //SpUtil.putBoolean("BluePrintSettings","BluePrintSetting",sign);
    }

    /**
     * 蓝牙打印设置状态
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */


    public static Boolean getBluePrintSetting() {
        return PreferenceUtil.getBoolean("BluePrintSetting", true);
    }

    /**
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setBlueDeviceName(String device) {
        PreferenceUtil.commitString("blueDevice", device);
    }

    /**
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getBlueDeviceAddress() {
        return PreferenceUtil.getString("blueDeviceAddress", "");
    }

    public static void setBlueDeviceNameAddress(String device) {
        PreferenceUtil.commitString("blueDeviceAddress", device);
    }


    /**
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getBlueDeviceName() {
        return PreferenceUtil.getString("blueDevice", "");
    }

    //不管是蓝牙连接方还是服务器方，得到socket对象后都传入
    public static BluetoothSocket bluetoothSocket = null;
    public static BluetoothSocket bluetoothSocket2 = null;
    public static BluetoothSocket bluetoothSocket3 = null;

    /**
     * 蓝牙状态
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setBlueState(Boolean sign) {
        PreferenceUtil.commitBoolean("blueState", sign);
    }

    /**
     * 蓝牙状态
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */

    // @SuppressWarnings("unchecked")
    public static Boolean getBlueState() {
        return PreferenceUtil.getBoolean("blueState", false);
    }

    public static String getBluetoothAddress(int type) {
        String printDeviceString = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_DEVICE);
        List<BluetoothBondBean> list;
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(printDeviceString)) {
            list = gson.fromJson(printDeviceString, new TypeToken<List<BluetoothBondBean>>() {
            }.getType());
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getType() == type) {
                        return list.get(i).getBluetoothAddress();
                    }
                }
            }
        }
        return null;
    }

    public static String getBluetoothName(int type) {
        String printDeviceString = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_DEVICE);
        List<BluetoothBondBean> list;
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(printDeviceString)) {
            list = gson.fromJson(printDeviceString, new TypeToken<List<BluetoothBondBean>>() {
            }.getType());
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getType() == type) {
                        return list.get(i).getBluetoothName();
                    }
                }
            }
        }
        return null;
    }

    public static void setDeviceEnable(int type, boolean enable) {

        switch (type) {
            case 1:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_DEVICE1, enable);
                break;
            case 2:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_DEVICE2, enable);
                break;
            case 3:
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_PRINT_DEVICE3, enable);
                break;
        }
    }

    public static boolean getDeviceEnable(int type) {
        switch (type) {
            case 1:
                return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_DEVICE1, true);
            case 2:
                return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_DEVICE2, true);
            case 3:
                return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_DEVICE3, true);
            default:
                return true;
        }
    }

    @SuppressLint("MissingPermission")
    public static void stayPrintDevice(int type, BluetoothDevice bluetoothDevice) {
        boolean hasType = false;
        BluetoothBondBean bluetoothBondBean = new BluetoothBondBean();
        bluetoothBondBean.setType(type);
//        bluetoothBondBean.setBluetoothDevice(bluetoothDevice);
        bluetoothBondBean.setBluetoothName(bluetoothDevice.getName());
        bluetoothBondBean.setBluetoothAddress(bluetoothDevice.getAddress());
        String printDeviceString = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_DEVICE);
        List<BluetoothBondBean> list;
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(printDeviceString)) {
            list = gson.fromJson(printDeviceString, new TypeToken<List<BluetoothBondBean>>() {
            }.getType());
            if (list != null) {
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getType() == type) {
                            hasType = true;
                            list.get(i).setType(type);
//                            list.get(i).setBluetoothDevice(bluetoothDevice);
                            list.get(i).setBluetoothName(bluetoothDevice.getName());
                            list.get(i).setBluetoothAddress(bluetoothDevice.getAddress());
                        }
                    }
                    if (!hasType) {
                        list.add(bluetoothBondBean);
                    }
                } else {
                    list.add(bluetoothBondBean);
                }
            } else {
                list = new ArrayList<>();
                list.add(bluetoothBondBean);
            }
        } else {
            list = new ArrayList<>();
            list.add(bluetoothBondBean);
        }
        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_PRINT_DEVICE, gson.toJson(list));
    }

    //是否自动打印
    public static Boolean getAutoPrinter() {
        return PreferenceUtil.getBoolean("autoprint", false);
    }

    public static void setAutoPrinter(Boolean sign) {
        PreferenceUtil.commitBoolean("autoprint", sign);
    }


    //用户ID
    public static String getUserId() {
        return SpUtil.getString(mContext, Constants.USER_ID, "");
    }

    //用户ID
    public static String getStayUserId() {
        return SpStayUtil.getString(mContext, Constants.SP_STAY_USER_ID, "");
    }

    //签名的key
    public static String getSignKey() {
        return SpUtil.getString(mContext, Constants.SIGN_KEY, "");
    }

    //cookie中的skey
    public static String getSkey() {

        return SpUtil.getString(MyApplication.getContext(), Constants.SKEY, "");
    }

    //cookie
    public static String getCookies() {
        return SpUtil.getString(MyApplication.getContext(), Constants.LOGIN_COOKIE, "");
    }

    //是否是超管（商户）
    public static Boolean getIsMerchant() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_MERCHANT_FLAG, false);
    }

    //是否是管理员
    public static Boolean getIsAdmin() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_ADMIN_FLAG, false);
    }

    //是否是收银员
    public static Boolean getIsCasher() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CASHER_FLAG, false);
    }

    //是否是店长
    public static Boolean getIsManager() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_MANAGER_FLAG, false);
    }

    //商户名称
    public static String getMerchantName() {
        return SpUtil.getString(MyApplication.getContext(), Constants.MERCHANT_NAME, "");
    }

    //商户简称
    public static String getMerchantShortName() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_SHORT_NAME, "");
    }

    //是否进件成功
    public static String getIsSuccessData() {
        return SpUtil.getString(MyApplication.getContext(), Constants.IS_SUCCESS_DATA, "");
    }

    //门店是否为空
    public static boolean isStoreNull() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
    }

    //审核状态
    public static int getExamineStatus() {
        return SpUtil.getInt(MyApplication.getContext(), Constants.SP_EXAMINE_STATUS, -1);
    }

    public static boolean examSuccess() {
        /*if (getExamineStatus()==1){
            return true;
        }*/
        return false;
    }

    //用户名
    public static String getUsername() {
        return SpUtil.getString(MyApplication.getContext(), Constants.APP_USERNAME, "");
    }

    //登录名
    public static String getLoginName() {
        return SpUtil.getString(MyApplication.getContext(), Constants.APP_LOGIN_NAME, "");
    }

    //手机号
    public static String getLoginPhone() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE, "");
    }

    //用户名
    public static String getRealName() {
        return SpUtil.getString(MyApplication.getContext(), Constants.REALNAME, "");
    }

    //permissions退款权限
    public static String getPermissionsRefund() {
        return SpUtil.getString(MyApplication.getContext(), Constants.PERMISSIONS_REFUND, "");
    }

    //funcCode退款权限
    public static String getFuncCodeRefund() {
        return SpUtil.getString(MyApplication.getContext(), Constants.FUNC_CODE_REFUND, "");
    }

    //收银员退款权限
    public static String getCashierRefund() {
        return SpUtil.getString(MyApplication.getContext(), Constants.CASHIER_REFUND_TAG, "");
    }

    /**
     * 收银员查看流水权限
     *
     * @return
     */
    public static String getOpCodeBill() {
        return SpUtil.getString(MyApplication.getContext(), Constants.CASHIER_BILL_TAG, "");
    }

    /**
     * 收银员查看统计权限
     *
     * @return
     */
    public static String getOpCodeReport() {
        return SpUtil.getString(MyApplication.getContext(), Constants.CASHIER_REPORT_TAG, "");
    }

    //商户号
    public static String getMechantId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.APP_MERCHANT_ID, "");
    }

    //个推clientID
    public static String getGetuiClienId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.GETUI_CLIENT_ID, "");
    }

    //密码登录过期
    public static String getFreeLogin() {
        return SpUtil.getString(MyApplication.getContext(), Constants.AUTO_PWD_FREE_LOGIN_ERROR, "common.not.login");
    }

    public static Boolean isWhite() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_WHITE, false);
    }


    /**
     * @return 是否开通会员
     */
    public static Boolean isOpenMember() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, false);
    }

    /**
     * 是否显示统计页面会员数据
     * 即开通了会员或饭卡消费
     *
     * @return
     */
    public static Boolean showCollectMemberData() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, false);
    }

    /**
     * 是否开通扫码点餐
     * @return
     */
    public static Boolean isOpenScanOrder() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_OPEN_SCAN_ORDER, false);
    }

    /**
     * 是否开通饭卡
     * @return
     */
    public static Boolean isOpenFuncard() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_ENABLE, false);
    }
    /**
     * 是否开通饭卡核销
     * @return
     */
    public static Boolean isOpenFuncardVerification() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, false);
    }

    /**
     * 门店是否开启语音播报
     * @return
     */
    public static Boolean isOpenVoice() {
        boolean isOpenVoice = isOpenAllVoice() && SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, true);
        return isOpenVoice;
    }

    public static Boolean isOpenSendHome() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, false);
    }

    /**
     * 查询是否开启到家订单推送的语音播报
     * 默认开启
     */
    public static boolean isOpenDaoJiaOrderVoice() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_DAOJIA_VOICE, true);
    }

    /**
     * 设置是否开启到家订单推送的语音播报
     * @param openDaoJiaOrderVoice
     */
    public static void setOpenDaoJiaOrderVoice(boolean openDaoJiaOrderVoice) {
        SpStayUtil.putBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_DAOJIA_VOICE, openDaoJiaOrderVoice);
    }

    /**
     * 是否开启收款的总语音播报
     */
    public static Boolean isOpenAllVoice() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_VOICE, true);
    }

    /**
     * 设置是否开启收款的总语音播报
     *
     * @param openAllVoice
     */
    public static void setOpenAllVoice(boolean openAllVoice) {
        SpStayUtil.putBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_VOICE, openAllVoice);
    }

    /**
     * 获取是否开启非API交易的语音播报
     */
    public static Boolean isOpenNotApiVoice() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_NOT_API_VOICE, true);
    }

    /**
     * 设置是否开启非API交易的语音播报
     *
     * @param openNotApiVoice
     */
    public static void setOpenNotApiVoice(boolean openNotApiVoice) {
        SpStayUtil.putBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_NOT_API_VOICE, openNotApiVoice);
    }

    /**
     * 获取是否开启API交易的语音播报
     */
    public static Boolean isOpenApiVoice() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_API_VOICE, false);
    }

    /**
     * 设置是否开启API交易的语音播报
     *
     * @param openApiVoice
     */
    public static void setOpenApiVoice(boolean openApiVoice) {
        SpStayUtil.putBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_API_VOICE, openApiVoice);
    }

    /**
     * 是否开启防逃单（取消支付）的语音播报
     */
    public static Boolean isOpenCancelPayWarnVoice() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_CANCEL_PAY_VOICE, true);
    }

    /**
     * 设置是否开启防逃单（取消支付）的语音播报
     *
     * @param openCancelPayWarnVoice
     */
    public static void setOpenCancelPayWarnVoice(boolean openCancelPayWarnVoice) {
        SpStayUtil.putBoolean(MyApplication.getContext(), getUserId() + Constants.SP_OPEN_CANCEL_PAY_VOICE, openCancelPayWarnVoice);
    }

    public static String getDefaultStore() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
    }

    public static String getDefaultStoreName() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME);
    }

    /**
     * 是否是营销商户
     *
     * @return
     */
    public static boolean getIsBounty() {
//        return true;
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_BOUNTY_MERCHANT);
    }

    /**
     * pos机登录状态
     *
     * @return
     */
    public static String getPosLoginStatus() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_POS_LOGIN_STATUS);
    }

    /**
     * pos返回的memberId
     *
     * @return
     */
    public static String getPosMemberId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_POS_MEMBER_ID);
    }

    /**
     * pos登录账号
     *
     * @return
     */
    public static String getPosEmpAccount() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_POS_EMP_ACCOUNT);
    }

    /**
     * 预授权开通状态
     *
     * @return
     */
    public static String getPledgeOpenStatus() {
//        return 2;
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_PLEDGE_OPEN_STATUS);
    }

    /**
     * 账号第一次登入
     *
     * @return
     */
    public static boolean isHidePledgeRefund() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_HIDE_PLEDGE_REFUND + MyApplication.getUserId());
    }

    /**
     * 花呗白名单
     *
     * @return
     */
    public static boolean isHuabeiWhite() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_HUABEI_WHITE);
    }


    /**
     * 是否打印结账单
     *
     * @return
     */
    public static boolean isPrintPayOrder() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_PAY_ORDER, true);
    }

    /**
     * 是否打印客户单
     *
     * @return
     */
    public static boolean isPrintClientOrder() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_CLIENT_ORDER, true);
    }

    /**
     * 是否打印厨房单
     *
     * @return
     */
    public static boolean isPrintKitchenOrder() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_KITCHEN_ORDER, true);
    }

    /**
     * 数量是否合并打印
     *
     * @return
     */
    public static boolean isPrintKitchenCount() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_KITCHEN_COUNT, true);
    }

    /**
     * 是否自动打印
     *
     * @return
     */
    public static boolean autoPrint() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_AUTO_PRINT, false);
    }

    /**
     * 风控白名单
     *
     * @return
     */
    public static int isRiskWhite() {
        return SpUtil.getInt(MyApplication.getContext(), Constants.SP_RISK_WHITE, 0);
    }

    /**
     * 收银点白名单
     *
     * @return
     */
    public static int isCashpointWhite() {
        return SpUtil.getInt(MyApplication.getContext(), Constants.SP_CASHPOINT_WHITE, 0);
    }

    /**
     * 弹出协议
     *
     * @return
     */
    public static boolean protocolShown() {
        return SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PROTOCOL_SHOWN + AppHelper.getVerCode(MyApplication.getContext()));
    }

    /**
     * 获取主题色
     *
     * @return
     */
    public static String themeColor() {
        String colorString = "#" + Integer.toHexString(ContextCompat.getColor(getContext(), R.color.theme_color));
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_THEME_COLOR, colorString);
    }

    /**
     * 获取配置信息
     *
     * @return
     */
    public static String getConfig() {
        return SpStayUtil.getString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getStayUserId(), Constants.CONFIG_ALL);
    }

    /**
     * 获取wizarpos签到时间
     *
     * @return
     */
    public static String getSignDate() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_SIGN_DATE);
    }

    /**
     * empId
     *
     * @return
     */
    public static String getEmpId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_EMP_ID);
    }

    /**
     * 终端号
     *
     * @return
     */
    public static String getDeviceNo() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_DEVICE_NO, "");
    }

    /**
     * 商户类型
     *
     * @return
     */
    public static String getMerchantType() {
        return SpUtil.getString(MyApplication.getContext(), Constants.APP_MERCHANT_TYPE, "");
//        return "12";
    }

    /**
     * 微信认证状态
     *
     * @return
     */
    public static int getWxrexStatus() {
        return SpUtil.getInt(MyApplication.getContext(), Constants.SP_WXREG_STATUS, 0);
    }
    /**
     * 支付宝认证状态
     *
     * @return
     */
    public static int getAlirexStatus() {
        return SpUtil.getInt(MyApplication.getContext(), Constants.SP_ALIREG_STATUS, 0);
    }

    /**
     * 银商开通
     *
     * @return
     */
    public static boolean isBcardOpen() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN);
    }

    /**
     * 收银点AppId
     *
     * @return
     */
    public static String getCashPointAppId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_CASH_POINT);
    }

    /**
     * 花呗分期免息AppId
     *
     * @return
     */
    public static String getHbAppId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_ALIPAY_ISTALLMENT);
    }

    /**
     * 收银点开通状态
     *
     * @return
     */
    public static String getCashPointOpenStatus() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_CASH_POINT_OPEN);
    }

    /**
     * 花呗分期免息开通状态
     *
     * @return
     */
    public static String getHbOpenStatus() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_ALIPAY_ISTALLMENT_OPEN);
    }

    /**
     * 是否需要修改密码
     *
     * @return
     */
    public static boolean isNeedUpdatePwd() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.NEED_CHANGE_PWD);
    }
}
