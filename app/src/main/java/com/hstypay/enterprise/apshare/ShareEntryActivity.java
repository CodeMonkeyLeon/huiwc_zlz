package com.hstypay.enterprise.apshare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alipay.share.sdk.openapi.APAPIFactory;
import com.alipay.share.sdk.openapi.BaseReq;
import com.alipay.share.sdk.openapi.BaseResp;
import com.alipay.share.sdk.openapi.IAPAPIEventHandler;
import com.alipay.share.sdk.openapi.IAPApi;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;

public class ShareEntryActivity extends BaseActivity implements IAPAPIEventHandler {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //创建工具对象实例，此处的APPID为上文提到的，申请应用生效后，在应用详情页中可以查到的支付宝应用唯一标识
        IAPApi api = APAPIFactory.createZFBApi(getApplicationContext(), Constants.ALIPAY_SHARE_ID, false);
        Intent intent = getIntent();
        //通过调用工具实例提供的handleIntent方法，绑定消息处理对象实例，
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {
        LogUtil.i("zhouwei","onResp=="+baseResp.errCode);
        //打印相应返回消息结果码
       /* if(baseResp.errCode==0){
            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_TRUE));
            //分享成功
        }else {
            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_RECEIPT_SHARE, Constants.ON_EVENT_FALSE));
            //分享失败
        }*/
        finish();
    }
}
