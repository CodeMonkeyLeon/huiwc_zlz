package com.hstypay.enterprise.base;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.dialog.UserInfoDialog;
import com.hstypay.enterprise.activity.BluetoothSetActivity;
import com.hstypay.enterprise.activity.LoginActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.ApiRes;
import com.hstypay.enterprise.commonlib.http.ApiManager;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.HandlerManager;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;
import com.hstypay.enterprise.utils.print.SdkTools;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.landicorp.android.eptapi.DeviceService;
import com.landicorp.android.eptapi.exception.ReloginException;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.exception.ServiceOccupiedException;
import com.landicorp.android.eptapi.exception.UnsupportMultiProcess;
import com.wizarpos.fucard.CloudPosPaymentClient.aidl.ICloudPay;

import org.greenrobot.eventbus.EventBus;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 11:40
 * @描述: activity基类
 */

public class BaseActivity extends FragmentActivity {
    private SelectDialog dialogInfo;
    private SafeDialog dialogNew = null;
    private CommonNoticeDialog mReoginDialog;
    private boolean isDeviceServiceLogined;
    public static final String PAY_SERVICE_ACTION = "huiyipos_usdk_pay_service";
    public static final String SERVICE_PACKAGE_NAME = "com.huiyi.nypos.pay";
    private int mPrintType;
    private static final int MY_PERMISSIONS_REQUEST_CALL_BLUETOOTH = 10010;
    private String[] permissionArray = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.d("Jeremy-onCreate--" + this.getLocalClassName());
        //MyApplication.getInstance().addActivity(this);
        if (isNeedEventBus()) {
            EventBus.getDefault().register(this);
        }

        initWindows();

        //监听蓝牙广播
        IntentFilter filterBlue = new IntentFilter();
        filterBlue.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filterBlue.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        try {
            registerReceiver(blueReceiver, filterBlue);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("zhouwei", "" + e);
        }
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            //初始化sdk，只需要在apk启动入口初始化一次，当应用完全退出是会自动调用sdk的onDestroy()
            SdkTools.initSdk(this);
        }
    }

    /**
     * 获取应用详情页面intent
     *
     * @return
     */
    public static Intent getAppDetailSettingIntent(Context context) {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            localIntent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        return localIntent;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissLoading();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissLoading();
        if (blueReceiver != null) {
            unregisterReceiver(blueReceiver);
        }
        if (isNeedEventBus()) {
            EventBus.getDefault().unregister(this);
        }
        MyApplication.getInstance().removeActivity(this);
    }

    public void showExitDialog(final Activity context) {
        if (isFinishing()) {
            return;
        }
        dialogInfo = new SelectDialog(context, getString(R.string.show_sign_out), getString(R.string.btnCancel), getString(R.string.btnOk), R.layout.select_common_dialog);
        dialogInfo.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                MyApplication.getInstance().addActivity(BaseActivity.this);
                if (LoginActivity.instance != null) {
                    LoginActivity.instance.finish();
                }
                //SpUtil.putBoolean(BaseActivity.this, Constants.UPDATE_VERSION_CANCEL, true);
                SpUtil.removeKey(Constants.SP_FUKA_SIGN_DATE);
                MyApplication.getInstance().finishAllActivity();
                //接触联迪A8 POS机绑定
                if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.LIANDI)) {
                    unbindDeviceService();
                }
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

    /**
     *
     */
    public void showPrintDialog(final int type) {
        if (isFinishing()) {
            return;
        }
        this.mPrintType = type;
        SelectDialog dialog = new SelectDialog(BaseActivity.this, getString(R.string.tx_blue_set), getString(R.string.btnCancel), getString(R.string.string_title_setting), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                boolean results = PermissionUtils.checkPermissionArray(BaseActivity.this, permissionArray);
                if (!results) {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_BLUETOOTH, permissionArray, getString(R.string.permission_content_bluetooth));
                } else {
                    startBluetoothSetActivity(type);
                }
            }
        });
        DialogHelper.resize(BaseActivity.this, dialog);
        dialog.show();
    }

    protected void showNotice(final int requestCode, final String[] permissionArray, String content) {
        if (isFinishing()) {
            return;
        }
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(BaseActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(BaseActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_BLUETOOTH:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    showDialog(getString(R.string.permission_set_content_bluetooth));
                } else {
                    startBluetoothSetActivity(mPrintType);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    protected void showDialog(String content) {
        if (isFinishing()) {
            return;
        }
        SelectDialog dialog = new SelectDialog(this, content
                , getString(R.string.btnCancel), getString(R.string.btn_set), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(BaseActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }

    protected void startBluetoothSetActivity(int printOrderType) {
        Intent intent = new Intent(this, BluetoothSetActivity.class);
        intent.putExtra(Constants.INTENT_PRINT_DEVICE_TYPE, printOrderType);
        startActivity(intent);
    }


    //销毁加载
    public void dismissLoading() {
        if (null != dialogNew) {
            dialogNew.dismiss();
            dialogNew = null;
        }
    }

    public void dissDialog(long delay) {
        if (null != dialogNew) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (null != dialogNew) {
                        dialogNew.dismiss();
                        dialogNew = null;
                    }
                }
            }, delay);
        }
    }

    //加载样式
    public void showNewLoading(boolean cancelble, String str) {
        if (!cancelble)
            return;
        loadDialog(BaseActivity.this, str);
    }

    //
    public void loadDialog(Activity context, String str) {
        if (isFinishing()) {
            return;
        }
        // 加载样式
        if (null == dialogNew) {
            dialogNew = new SafeDialog(context, R.style.my_dialog);
            dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogNew.setCancelable(false);
            dialogNew.setCanceledOnTouchOutside(false);
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
            TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
            tv.setText(str);
            // dialog透明
            WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
            lp.alpha = 0.9f;
            dialogNew.getWindow().setAttributes(lp);
            dialogNew.setContentView(v);

        }
        DialogHelper.resizeNew(context, dialogNew);

        dialogNew.show();
        //if (hasWindowFocus()) {
        //}
    }

    public SafeDialog getLoadDialog(Activity context, String str, boolean cancelable) {
        SafeDialog dialogNew = new SafeDialog(context, R.style.my_dialog);
        dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogNew.setCancelable(cancelable);
        dialogNew.setCanceledOnTouchOutside(cancelable);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
        TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
        tv.setText(str);
        // dialog透明
        WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
        lp.alpha = 0.9f;
        dialogNew.getWindow().setAttributes(lp);
        dialogNew.setContentView(v);
        DialogHelper.resizeNew(context, dialogNew);
        return dialogNew;
    }

    public SafeDialog getLoadDialog(Activity context, String str, boolean cancelable, float alpha) {
        SafeDialog dialogNew = new SafeDialog(context, R.style.my_dialog);
        dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogNew.setCancelable(cancelable);
        dialogNew.setCanceledOnTouchOutside(cancelable);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
        TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
        tv.setText(str);
        // dialog透明
        WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
        lp.alpha = alpha;
        dialogNew.getWindow().setAttributes(lp);
        dialogNew.setContentView(v);
        DialogHelper.resizeNew(context, dialogNew);
        return dialogNew;
    }

    private boolean isOpen = true, isClose = true, isOpening = true, isCloseing = true;

    /**
     * 连接默认上一次
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public void connentBlue(final int type) {
        /*final BluetoothDevice bluetoothDevice = MyApplication.getBluetoothDevice(type);
        if (bluetoothDevice == null){
            return;
        }*/
        if (MyApplication.getDeviceEnable(type) && !StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(type))) {
            //异步去连接蓝牙
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isConnect = BluePrintUtil.blueConnent(type, BaseActivity.this);
                    if (!isConnect) {
                        HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    }
                }
            }).start();
        }
    }

    /**
     * 实时监听蓝牙广播
     */
    private final BroadcastReceiver blueReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    MyApplication.setBlueState(false);
                    if (isClose) {
                        isClose = false;
                        MyToast.showToastShort(getString(R.string.tx_blue_closeing));
                    }
                    MyApplication.bluetoothSocket = null;
                    MyApplication.bluetoothSocket2 = null;
                    MyApplication.bluetoothSocket3 = null;
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS,
                            HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    MyApplication.setBlueState(false);
                    if (isCloseing) {
                        isCloseing = false;
                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS,
                            HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_ON:
                    MyApplication.setBlueState(true);
                    if (isOpen) {
                        isOpen = false;
                    }
                    connentBlue(1);
                    connentBlue(2);
                    connentBlue(3);
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    if (isOpening) {
                        isOpening = false;
                    }
                    break;
            }

        }

    };


    private void initWindows() {
        Window window = getWindow();
        //这里是自己定义状态栏的颜色
        int color = getResources().getColor(R.color.transparent);

        ViewGroup contentView = ((ViewGroup) findViewById(android.R.id.content));
        View childAt = contentView.getChildAt(0);
        if (childAt != null) {
            // 设置内容布局充满屏幕
            childAt.setFitsSystemWindows(true);
        }
        // 5.0及以上
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
            // 4.4到5.0
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 透明状态栏
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            View view = new View(this);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getStatusBarHeight(this)));
            view.setBackgroundColor(color);
            contentView.addView(view);
        }
    }

    /**
     * 获取状态栏高度
     *
     * @param context context
     * @return 状态栏高度
     */
    private static int getStatusBarHeight(Context context) {
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        return context.getResources().getDimensionPixelSize(resourceId);
    }


    //设置button按钮颜色
    public void setButtonBg(Button b, boolean enable, int res) {
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.selector_btn_register);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_unsure_bg);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
        }
    }


    public void getLoginDialog(Context context, String title) {
        if (mReoginDialog == null) {
            mReoginDialog = new CommonNoticeDialog(context, title, null);
            mReoginDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    MyApplication.iscache = false;
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    ApiManager.get().setMService(null);
                    startActivity(new Intent(MyApplication.getContext(), LoginActivity.class));
                    MyApplication.getInstance().finishAllActivity();
                }
            });
        }
        DialogHelper.resize(context, mReoginDialog);
        mReoginDialog.show();
    }


    public boolean isNeedEventBus() {
        return false;
    }

    public void unbindDeviceService() {
        DeviceService.logout();
        isDeviceServiceLogined = false;
    }

    public void bindDeviceService() {
        try {
            isDeviceServiceLogined = false;
            DeviceService.login(this);
            isDeviceServiceLogined = true;
        } catch (RequestException e) {
            e.printStackTrace();
        } catch (ServiceOccupiedException e) {
            e.printStackTrace();
        } catch (ReloginException e) {
            e.printStackTrace();
        } catch (UnsupportMultiProcess e) {
            e.printStackTrace();
        }
    }

    public static Boolean hideInputMethod(Context context, View v) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            return imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        return false;
    }

    public static boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            v.getLocationInWindow(leftTop);
            int left = leftTop[0], top = leftTop[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN &&
                ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) || "ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                if (hideInputMethod(this, v)) {
                    return true; //隐藏键盘时，其他控件不响应点击事件==》注释则不拦截点击事件
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    public void showCommonNoticeDialog(Activity activity, String title) {
        if (isFinishing()) {
            return;
        }
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, title, "");
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void showCommonNoticeDialog(Activity activity, String title, CommonNoticeDialog.OnClickOkListener onClickOkListener) {
        if (isFinishing()) {
            return;
        }
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, title, "");
        if (onClickOkListener != null) {
            dialog.setOnClickOkListener(onClickOkListener);
        }
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void showCommonNoticeDialog(Activity activity, String title, String content) {
        if (isFinishing()) {
            return;
        }
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, title, content, "");
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void showCommonNoticeDialog(Activity activity, String title, String content, CommonNoticeDialog.OnClickOkListener onClickOkListener) {
        if (isFinishing()) {
            return;
        }
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, title, content, "");
        if (onClickOkListener != null) {
            dialog.setOnClickOkListener(onClickOkListener);
        }
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void showCommonNoticeDialog(Activity activity, int res) {
        if (isFinishing()) {
            return;
        }
        NoticeDialog dialog = new NoticeDialog(activity, "", "", res);
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    // 初始化设备
    public void connectDevice(final N900Device n900Device) {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    n900Device.connectDevice();
                }
            }).start();
        } catch (Exception e) {
            MyToast.showToastShort("设备连接异常：" + e);
        }

    }

    public void setButtonEnable(Button button, boolean enable) {
        if (enable) {
            Drawable rectangleDefault = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), MyApplication.themeColor(), 5);
            String colorClickedString = "#" + Integer.toHexString(ContextCompat.getColor(MyApplication.getContext(), R.color.theme_color_clicked));
            Drawable rectanglePressed = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorClickedString, 5);
            rectanglePressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
            button.setBackground(stateListDrawable);
            button.setEnabled(true);
        } else {
            Drawable rectangleUnable = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), MyApplication.themeColor(), 5);
            rectangleUnable.setAlpha(102);
            button.setBackground(rectangleUnable);
            button.setEnabled(false);
        }
    }

    public void setButtonWithColor(Button button, int color) {
        String colorString = "#" + Integer.toHexString(ContextCompat.getColor(MyApplication.getContext(), color));
        Drawable rectangleDefault = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 5);
        Drawable rectanglePressed = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 5);
        rectanglePressed.setAlpha(204);
        StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
        button.setBackground(stateListDrawable);
        button.setEnabled(true);
    }

    public void setButtonWhite(Button button) {
        String colorString = "#" + Integer.toHexString(ContextCompat.getColor(MyApplication.getContext(), R.color.white));
        Drawable rectangleDefault = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 5);
        Drawable rectanglePressed = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 5);
        rectanglePressed.setAlpha(204);
        StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
        button.setBackground(stateListDrawable);
        button.setEnabled(true);
    }

    /**
     * 网络请求的错误回调
     */
    public void failHandler(NoticeEvent event) {
        if (event.getCls().equals(Constants.MSG_NET_ERROR)) {
            MyToast.showToastShort(getString(R.string.net_error));
        } else if (event.getCls().equals(Constants.ON_EVENT_FALSE)) {
            ApiRes res = (ApiRes) event.getMsg();
            if (res.getError() != null) {
                if (res.getError().getCode() != null) {
                    //重新登录
                    if (res.getError().getCode().equals(MyApplication.getFreeLogin())) {
                        if (res.getError().getMessage() != null) {
                            getLoginDialog(this, res.getError().getMessage());
                        }
                    } else {
                        if (res.getError().getMessage() != null) {
                            MyToast.showToast(res.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                }
            }
        }
    }

    /**
     * 慧银Q2福卡
     */
    public ICloudPay mCupService;

    private ServiceConnection mCupConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mCupService = ICloudPay.Stub.asInterface(service);

        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mCupService = null;
        }
    };

    public void bindCUPService() {
        try {
            //绑定支付服务
            Intent intent = new Intent();
            intent.setAction("com.wizarpos.fucard.CloudPosPaymentClient.aidl.ICloudPay");
            intent.setPackage("com.wizarpos.fucard");
            bindService(intent, mCupConnection,
                    Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unbindService() {
        /* 解除和支付服务的绑定 */
        if (mCupConnection != null)
            unbindService(mCupConnection);
    }

    //弹出dialog去完善资料
    public void openDialog() {
        UserInfoDialog dialog = new UserInfoDialog(this, new UserInfoDialog.ConfirmListener() {
            @Override
            public void ok() {
                Intent intent = new Intent(MyApplication.getContext(), RegisterActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT, Constants.H5_BASE_URL + ConfigUtil.getRegisterSecondUrl());
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }
}
