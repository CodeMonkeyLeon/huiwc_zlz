package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/22 11:24
 * @描述: ${TODO}
 */

public class ActiveBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"receiptMerchantActivityEnabled":true,"receiptMerchantActivityUrl":"https://mobilecodec.alipay.com/show.htm?code=cpx05103phsdtrpxvuaqc0f"}
     * logId : WZZSTMjw
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * receiptMerchantActivityEnabled : true
         * receiptMerchantActivityUrl : https://mobilecodec.alipay.com/show.htm?code=cpx05103phsdtrpxvuaqc0f
         */
        private boolean receiptMerchantActivityEnabled;//小票打印商家活动启用状态
        private String receiptMerchantActivityUrl;//小票打印商家活动链接
        private String receiptMerchantActivityTitle;//小票打印商家活动标题文案
        private boolean apiVoiceBroadcastButton;//API语言播报按钮显示
        public String getReceiptMerchantActivityTitle() {
            return receiptMerchantActivityTitle;
        }

        public void setReceiptMerchantActivityTitle(String receiptMerchantActivityTitle) {
            this.receiptMerchantActivityTitle = receiptMerchantActivityTitle;
        }

        public void setReceiptMerchantActivityEnabled(boolean receiptMerchantActivityEnabled) {
            this.receiptMerchantActivityEnabled = receiptMerchantActivityEnabled;
        }

        public void setReceiptMerchantActivityUrl(String receiptMerchantActivityUrl) {
            this.receiptMerchantActivityUrl = receiptMerchantActivityUrl;
        }

        public boolean isReceiptMerchantActivityEnabled() {
            return receiptMerchantActivityEnabled;
        }

        public String getReceiptMerchantActivityUrl() {
            return receiptMerchantActivityUrl;
        }

        public boolean isApiVoiceBroadcastButton() {
            return apiVoiceBroadcastButton;
        }

        public void setApiVoiceBroadcastButton(boolean apiVoiceBroadcastButton) {
            this.apiVoiceBroadcastButton = apiVoiceBroadcastButton;
        }
    }
}
