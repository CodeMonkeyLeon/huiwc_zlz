package com.hstypay.enterprise.bean;

import java.math.BigDecimal;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/7/3 11:34
 * @描述: ${TODO}
 */
public class ActiveRuleBean {
    private BigDecimal conditionAmount;
    private BigDecimal discount;

    public BigDecimal getConditionAmount() {
        return conditionAmount;
    }

    public void setConditionAmount(BigDecimal conditionAmount) {
        this.conditionAmount = conditionAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}
