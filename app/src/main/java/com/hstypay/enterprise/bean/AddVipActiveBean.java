package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/6/25 15:40
 * @描述: ${TODO}
 */
public class AddVipActiveBean {
    private String name;
    private String beginDate;
    private String endDate;
    private String instructions;
    private String ruleStr;
    private String sceneType;//活动场景 1-购卡 2-充值 3-消费
    private String activityType;//活动类型 1-满赠 2-满减 3-折扣
    private ActiveRules rule = new ActiveRules();

    public class ActiveRules{
        private long conditionAmount;
        private long discountVar;
        private long discountMaxAmount;

        public long getConditionAmount() {
            return conditionAmount;
        }

        public void setConditionAmount(long conditionAmount) {
            this.conditionAmount = conditionAmount;
        }

        public long getDiscountVar() {
            return discountVar;
        }

        public void setDiscountVar(long discountVar) {
            this.discountVar = discountVar;
        }

        public long getDiscountMaxAmount() {
            return discountMaxAmount;
        }

        public void setDiscountMaxAmount(long discountMaxAmount) {
            this.discountMaxAmount = discountMaxAmount;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getSceneType() {
        return sceneType;
    }

    public void setSceneType(String sceneType) {
        this.sceneType = sceneType;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public ActiveRules getActiveRules() {
        return rule;
    }

    public void setActiveRules(ActiveRules rule) {
        this.rule = rule;
    }

    public String getRuleStr() {
        return ruleStr;
    }

    public void setRuleStr(String ruleStr) {
        this.ruleStr = ruleStr;
    }
}
