package com.hstypay.enterprise.bean;


import java.util.List;

/**
 * @Author dean.zeng
 * @Description 人脸支付
 * @Date 2020-06-30 10:25
 **/
public class ApiResult<T> {

    public ApiResult() {
    }

    public ApiResult(ErrorBean error, String logId, boolean status, T data) {
        this.error = error;
        this.logId = logId;
        this.status = status;
        this.data = data;
    }

    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public ErrorBean(String code, String message, List<?> args) {
            this.code = code;
            this.message = message;
            this.args = args;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private T data;
    private String logId;
    private boolean status;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
