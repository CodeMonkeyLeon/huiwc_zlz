package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 17:38
 * @描述: ${TODO}
 */

public class BankListBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : [{"bankCode":"102100099996","bankLetterNo":"36","bankType":1,"bankName":"中国工商银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"ICBC","bankId":1,"createTime":"2017-09-08 18:03:12","bankTel":"95588","bankTypeCnt":"总行","bankNo":"36","createUser":0},{"bankCode":"104100000004","bankLetterNo":"33","bankType":1,"bankName":"中国银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"BOC","bankId":2,"createTime":"2017-09-08 18:03:12","bankTel":"95566","bankTypeCnt":"总行","bankNo":"33","createUser":0},{"bankCode":"105100000017","bankLetterNo":"37","bankType":1,"bankName":"中国建设银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"CCB","bankId":3,"createTime":"2017-09-08 18:03:12","bankTel":"95533","bankTypeCnt":"总行","bankNo":"37","createUser":0},{"bankCode":"103100000026","bankLetterNo":"38","bankType":1,"bankName":"中国农业银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"ABCHINA","bankId":4,"createTime":"2017-09-08 18:03:12","bankTel":"95599","bankTypeCnt":"总行","bankNo":"38","createUser":0},{"bankCode":"308584000013","bankLetterNo":"44","bankType":1,"bankName":"招商银行","createUserName":"system","updateTime":"2017-09-08 18:03:13","bankLetterCode":"CMBCHINA","bankId":5,"createTime":"2017-09-08 18:03:13","bankTel":"95555","bankTypeCnt":"总行","bankNo":"44","createUser":0},{"bankCode":"403100000004","bankLetterNo":"39","bankType":1,"bankName":"中国邮政储蓄银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"PSBC","bankId":6,"createTime":"2017-09-08 18:03:12","bankTel":"95580","bankTypeCnt":"总行","bankNo":"39","createUser":0},{"bankCode":"303100000006","bankLetterNo":"34","bankType":1,"bankName":"中国光大银行","createUserName":"system","updateTime":"2017-09-08 18:03:09","bankLetterCode":"CEBBANK","bankId":7,"createTime":"2017-09-08 18:03:09","bankTel":"95595","bankTypeCnt":"总行","bankNo":"34","createUser":0},{"bankCode":"310290000013","bankLetterNo":"40","bankType":1,"bankName":"上海浦东发展银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"SPDB","bankId":8,"createTime":"2017-09-08 18:03:12","bankTel":"95528","bankTypeCnt":"总行","bankNo":"40","createUser":0},{"bankCode":"307584007998","bankLetterNo":"41","bankType":1,"bankName":"深圳发展银行","createUserName":"system","updateTime":"2017-09-08 18:03:18","bankLetterCode":"SDB","bankId":9,"createTime":"2017-09-08 18:03:18","bankTel":"95511","bankTypeCnt":"总行","bankNo":"41","createUser":0},{"bankCode":"309391000011","bankLetterNo":"45","bankType":1,"bankName":"兴业银行","createUserName":"system","updateTime":"2017-09-08 18:03:13","bankLetterCode":"CIB","bankId":10,"createTime":"2017-09-08 18:03:13","bankTel":"95561","bankTypeCnt":"总行","bankNo":"45","createUser":0},{"bankCode":"307584007998","bankLetterNo":"46","bankType":1,"bankName":"平安银行","createUserName":"system","updateTime":"2017-09-08 18:03:13","bankLetterCode":"PINGAN","bankId":11,"createTime":"2017-09-08 18:03:13","bankTel":"95511-3","bankTypeCnt":"总行","bankNo":"46","createUser":0},{"bankCode":"306581000003","bankLetterNo":"42","bankType":1,"bankName":"广东发展银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"CGB","bankId":12,"createTime":"2017-09-08 18:03:12","bankTel":"95508","bankTypeCnt":"总行","bankNo":"42","createUser":0},{"bankCode":"302100011000","bankLetterNo":"35","bankType":1,"bankName":"中信银行","createUserName":"system","updateTime":"2017-09-08 18:03:12","bankLetterCode":"ECITCI","bankId":13,"createTime":"2017-09-08 18:03:12","bankTel":"95558","bankTypeCnt":"总行","bankNo":"35","createUser":0}]
     * logId : IQfV1lTz
     * status : true
     */
    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{
        /**
         * bankCode : 102100099996
         * bankLetterNo : 36
         * bankType : 1
         * bankName : 中国工商银行
         * createUserName : system
         * updateTime : 2017-09-08 18:03:12
         * bankLetterCode : ICBC
         * bankId : 1
         * createTime : 2017-09-08 18:03:12
         * bankTel : 95588
         * bankTypeCnt : 总行
         * bankNo : 36
         * createUser : 0
         */
        private String bankCode;
        private String bankLetterNo;
        private int bankType;
        private String bankName;
        private String createUserName;
        private String updateTime;
        private String bankLetterCode;
        private long bankId;
        private String createTime;
        private String bankTel;
        private String bankTypeCnt;
        private String bankNo;
        private int createUser;

        public void setBankCode(String bankCode) {
            this.bankCode = bankCode;
        }

        public void setBankLetterNo(String bankLetterNo) {
            this.bankLetterNo = bankLetterNo;
        }

        public void setBankType(int bankType) {
            this.bankType = bankType;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public void setBankLetterCode(String bankLetterCode) {
            this.bankLetterCode = bankLetterCode;
        }

        public void setBankId(long bankId) {
            this.bankId = bankId;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setBankTel(String bankTel) {
            this.bankTel = bankTel;
        }

        public void setBankTypeCnt(String bankTypeCnt) {
            this.bankTypeCnt = bankTypeCnt;
        }

        public void setBankNo(String bankNo) {
            this.bankNo = bankNo;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public String getBankCode() {
            return bankCode;
        }

        public String getBankLetterNo() {
            return bankLetterNo;
        }

        public int getBankType() {
            return bankType;
        }

        public String getBankName() {
            return bankName;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public String getBankLetterCode() {
            return bankLetterCode;
        }

        public long getBankId() {
            return bankId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getBankTel() {
            return bankTel;
        }

        public String getBankTypeCnt() {
            return bankTypeCnt;
        }

        public String getBankNo() {
            return bankNo;
        }

        public int getCreateUser() {
            return createUser;
        }
    }
}
