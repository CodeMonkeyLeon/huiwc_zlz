package com.hstypay.enterprise.bean;

public class BasicBean {

    private String merchantShortName;
    private int industryId;
    private String email;
    private String servicePhone;
    private String province;
    private String city;
    private String county;
    private String address;
    private String licenseCode;
    private boolean licenseExpiredFlag;
    private String licenseExpiredBegin;
    /** 主营业务*/
    private String businScope;
    /** 注册资本*/
    private String capital;

    public String getBusinScope() {
        return businScope;
    }

    public void setBusinScope(String businScope) {
        this.businScope = businScope;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
    public String getMerchantShortName() {
        return merchantShortName;
    }

    public void setMerchantShortName(String merchantShortName) {
        this.merchantShortName = merchantShortName;
    }

    public int getIndustryId() {
        return industryId;
    }

    public void setIndustryId(int industryId) {
        this.industryId = industryId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLicenseCode() {
        return licenseCode;
    }

    public void setLicenseCode(String licenseCode) {
        this.licenseCode = licenseCode;
    }

    public boolean isLicenseExpiredFlag() {
        return licenseExpiredFlag;
    }

    public void setLicenseExpiredFlag(boolean licenseExpiredFlag) {
        this.licenseExpiredFlag = licenseExpiredFlag;
    }

    public String getLicenseExpiredBegin() {
        return licenseExpiredBegin;
    }

    public void setLicenseExpiredBegin(String licenseExpiredBegin) {
        this.licenseExpiredBegin = licenseExpiredBegin;
    }

    public String getLicenseExpiredDate() {
        return licenseExpiredDate;
    }

    public void setLicenseExpiredDate(String licenseExpiredDate) {
        this.licenseExpiredDate = licenseExpiredDate;
    }

    private String licenseExpiredDate;
}
