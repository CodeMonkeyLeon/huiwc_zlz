package com.hstypay.enterprise.bean;

import android.bluetooth.BluetoothDevice;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/7/25 19:48
 * @描述: ${TODO}
 */
public class BluetoothDeviceBean {
    private BluetoothDevice bluetoothDevice;
    private boolean connected;

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
