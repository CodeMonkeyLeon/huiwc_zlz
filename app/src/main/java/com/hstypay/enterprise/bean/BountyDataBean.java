package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/12 19:16
 * @描述: ${TODO}
 */

public class BountyDataBean {
    /**
     * logId : WMvlCSJ5
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"realName":"一路发加","stores":[{"storeId":"101300000001","storeName":"支付接口联调测试数据_门店"}],"opCode":"APPLY-REFUND","userId":121}
     */

    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * tradeTime : 2018-12-13 11:59:36
         * apiProvider : 1
         * orderNo : 20181213873520000000111000006408
         * rewardMoney : 200
         */
        private String tradeTime;
        private int apiProvider;
        private String orderNo;
        private int rewardMoney;

        public void setTradeTime(String tradeTime) {
            this.tradeTime = tradeTime;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public void setRewardMoney(int rewardMoney) {
            this.rewardMoney = rewardMoney;
        }

        public String getTradeTime() {
            return tradeTime;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public int getRewardMoney() {
            return rewardMoney;
        }
    }
}
