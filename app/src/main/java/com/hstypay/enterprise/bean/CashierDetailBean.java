package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/8/3.
 */

public class CashierDetailBean implements Serializable {
    /**
     * logId : WMvlCSJ5
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"realName":"一路发加","stores":[{"storeId":"101300000001","storeName":"支付接口联调测试数据_门店"}],"opCode":"APPLY-REFUND","userId":121}
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * realName : 一路发加
         * stores : [{"storeId":"101300000001","storeName":"支付接口联调测试数据_门店"}]
         * opCode : APPLY-REFUND
         * userId : 121
         */
        private String realName;
        private String opCode;
        private String userName;
        private String loginPhone;
        private boolean applyRefund;
        private boolean applyBill;
        private boolean applyWater;
        private boolean enabled;
        private int editEnable;//是否可编辑 1:是 0:否
        private String userId;
        private List<StoresBean> stores;
        /**
         * 角色退款权限，true 打开 false 关闭
         */
        private boolean roleApplyRefund;
        /**
         * 是否需要重发邀请短信
         */
        private boolean needResendSmsFlag;
        /**
         * 激活状态,1:已激活 2:未激活
         */
        private int activateStatus;

        public int getActivateStatus() {
            return activateStatus;
        }

        public void setActivateStatus(int activateStatus) {
            this.activateStatus = activateStatus;
        }

        public boolean isRoleApplyRefund() {
            return roleApplyRefund;
        }

        public void setRoleApplyRefund(boolean roleApplyRefund) {
            this.roleApplyRefund = roleApplyRefund;
        }

        public boolean isNeedResendSmsFlag() {
            return needResendSmsFlag;
        }

        public void setNeedResendSmsFlag(boolean needResendSmsFlag) {
            this.needResendSmsFlag = needResendSmsFlag;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getLoginPhone() {
            return loginPhone;
        }

        public void setLoginPhone(String loginPhone) {
            this.loginPhone = loginPhone;
        }

        public boolean isApplyBill() {
            return applyBill;
        }

        public void setApplyBill(boolean applyBill) {
            this.applyBill = applyBill;
        }

        public boolean isApplyWater() {
            return applyWater;
        }

        public void setApplyWater(boolean applyWater) {
            this.applyWater = applyWater;
        }

        public boolean isApplyRefund() {
            return applyRefund;
        }

        public void setApplyRefund(boolean applyRefund) {
            this.applyRefund = applyRefund;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public String getOpCode() {
            return opCode;
        }

        public void setOpCode(String opCode) {
            this.opCode = opCode;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public int getEditEnable() {
            return editEnable;
        }

        public void setEditEnable(int editEnable) {
            this.editEnable = editEnable;
        }

        public List<StoresBean> getStores() {
            return stores;
        }

        public void setStores(List<StoresBean> stores) {
            this.stores = stores;
        }
    }
}
