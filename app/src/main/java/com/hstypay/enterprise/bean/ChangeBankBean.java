package com.hstypay.enterprise.bean;


import java.util.List;

public class ChangeBankBean {
    private String logId;
    private boolean status;
    private Info.ErrorBean error;
    private DataEntity data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Info.ErrorBean getError() {
        return error;
    }

    public void setError(Info.ErrorBean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public class DataEntity {
        private String tel;
        private int verifyCode;// 1：需要验证

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public int getVerifyCode() {
            return verifyCode;
        }

        public void setVerifyCode(int verifyCode) {
            this.verifyCode = verifyCode;
        }
    }
}
