package com.hstypay.enterprise.bean;

public class CheckBean {

    private String name;
    private int id;//后端约定的业务标识id
    private boolean isChecked;
    private boolean isCheckAble = true;

    public boolean isCheckAble() {
        return isCheckAble;
    }

    public void setCheckAble(boolean checkAble) {
        isCheckAble = checkAble;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }
}
