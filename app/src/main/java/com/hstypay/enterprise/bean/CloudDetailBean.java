package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/2/2 10:51
 * @描述: ${TODO}
 */

public class CloudDetailBean {
    private ErrorBean error;
    /**
     * logId : dc3e81644fd64e8a91df301f736249a6
     * status : true
     * data : {"createUser":1,"createUserName":"超级管理员","createTime":"2019-05-20 16:51:56","updateTime":"2019-05-22 16:23:52","pageSize":0,"currentPage":0,"id":161,"merchantId":"735200000001","merchantIdCnt":"彭国卿测试商户(威富通支付专用)","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","sn":"10002066","status":1,"statusCnt":"已激活","signKey":"PhRpCUWvSdDIF1jaV8sBHR8UwTonnTU7","model":"汇播报V1.0","lableServiceChannelId":"0","setUp":"{\"PAY_TYPE_BROAD_CAST\":1}","userList":[{"pageSize":0,"currentPage":0,"userId":0,"role":0,"name":"彭国卿测试商户","bind":1},{"pageSize":0,"currentPage":0,"userId":782,"role":2,"phone":"18665877763","name":"123456","bind":2},{"pageSize":0,"currentPage":0,"userId":783,"role":4,"phone":"15474253652","name":"不讲理","bind":2},{"pageSize":0,"currentPage":0,"userId":784,"role":2,"phone":"15325248745","name":"2222222","bind":2},{"pageSize":0,"currentPage":0,"userId":785,"role":4,"phone":"15800000009","name":"店长2","bind":2},{"pageSize":0,"currentPage":0,"userId":786,"role":4,"phone":"13417510012","name":"店长1","bind":2},{"pageSize":0,"currentPage":0,"userId":787,"role":2,"phone":"15800000004","name":"363633","bind":2},{"pageSize":0,"currentPage":0,"userId":788,"role":2,"phone":"13417510014","name":"风扇","bind":2},{"pageSize":0,"currentPage":0,"userId":793,"role":4,"phone":"17722404212","name":"will","bind":2},{"pageSize":0,"currentPage":0,"userId":857,"role":4,"phone":"13250758566","name":"彭国卿测试店长","bind":2},{"pageSize":0,"currentPage":0,"userId":868,"role":2,"phone":"18718571111","name":"尤永圣","bind":2},{"pageSize":0,"currentPage":0,"userId":895,"role":2,"phone":"15100000000","name":"测试收银员","bind":2},{"pageSize":0,"currentPage":0,"userId":901,"role":2,"phone":"18588243549","name":"测试验证","bind":2},{"pageSize":0,"currentPage":0,"userId":902,"role":4,"phone":"18588243547","name":"测试店长","bind":2},{"pageSize":0,"currentPage":0,"userId":933,"role":2,"phone":"13417510016","name":"嗯嗯","bind":2},{"pageSize":0,"currentPage":0,"userId":951,"role":2,"phone":"13729087765","name":"嗯嗯","bind":2},{"pageSize":0,"currentPage":0,"userId":987,"role":2,"phone":"18600000001","name":"张丹411","bind":2},{"pageSize":0,"currentPage":0,"userId":996,"role":4,"phone":"13184274627","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":997,"role":4,"phone":"13184274628","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":998,"role":4,"phone":"15899868195","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":999,"role":2,"phone":"13184274630","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":1127,"role":2,"phone":"18955466666","name":"彭国卿收银员","bind":2}]}
     */

    private String logId;
    private boolean status;
    private DataBean data;

    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * createUser : 1
         * createUserName : 超级管理员
         * createTime : 2019-05-20 16:51:56
         * updateTime : 2019-05-22 16:23:52
         * pageSize : 0
         * currentPage : 0
         * id : 161
         * merchantId : 735200000001
         * merchantIdCnt : 彭国卿测试商户(威富通支付专用)
         * storeMerchantId : 462300000001
         * storeMerchantIdCnt : 彭国卿测试商户
         * sn : 10002066
         * status : 1
         * statusCnt : 已激活
         * signKey : PhRpCUWvSdDIF1jaV8sBHR8UwTonnTU7
         * model : 汇播报V1.0
         * lableServiceChannelId : 0
         * setUp : {"PAY_TYPE_BROAD_CAST":1}
         * userList : [{"pageSize":0,"currentPage":0,"userId":0,"role":0,"name":"彭国卿测试商户","bind":1},{"pageSize":0,"currentPage":0,"userId":782,"role":2,"phone":"18665877763","name":"123456","bind":2},{"pageSize":0,"currentPage":0,"userId":783,"role":4,"phone":"15474253652","name":"不讲理","bind":2},{"pageSize":0,"currentPage":0,"userId":784,"role":2,"phone":"15325248745","name":"2222222","bind":2},{"pageSize":0,"currentPage":0,"userId":785,"role":4,"phone":"15800000009","name":"店长2","bind":2},{"pageSize":0,"currentPage":0,"userId":786,"role":4,"phone":"13417510012","name":"店长1","bind":2},{"pageSize":0,"currentPage":0,"userId":787,"role":2,"phone":"15800000004","name":"363633","bind":2},{"pageSize":0,"currentPage":0,"userId":788,"role":2,"phone":"13417510014","name":"风扇","bind":2},{"pageSize":0,"currentPage":0,"userId":793,"role":4,"phone":"17722404212","name":"will","bind":2},{"pageSize":0,"currentPage":0,"userId":857,"role":4,"phone":"13250758566","name":"彭国卿测试店长","bind":2},{"pageSize":0,"currentPage":0,"userId":868,"role":2,"phone":"18718571111","name":"尤永圣","bind":2},{"pageSize":0,"currentPage":0,"userId":895,"role":2,"phone":"15100000000","name":"测试收银员","bind":2},{"pageSize":0,"currentPage":0,"userId":901,"role":2,"phone":"18588243549","name":"测试验证","bind":2},{"pageSize":0,"currentPage":0,"userId":902,"role":4,"phone":"18588243547","name":"测试店长","bind":2},{"pageSize":0,"currentPage":0,"userId":933,"role":2,"phone":"13417510016","name":"嗯嗯","bind":2},{"pageSize":0,"currentPage":0,"userId":951,"role":2,"phone":"13729087765","name":"嗯嗯","bind":2},{"pageSize":0,"currentPage":0,"userId":987,"role":2,"phone":"18600000001","name":"张丹411","bind":2},{"pageSize":0,"currentPage":0,"userId":996,"role":4,"phone":"13184274627","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":997,"role":4,"phone":"13184274628","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":998,"role":4,"phone":"15899868195","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":999,"role":2,"phone":"13184274630","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":1127,"role":2,"phone":"18955466666","name":"彭国卿收银员","bind":2}]
         */

        private int createUser;
        private String createUserName;
        private String createTime;
        private String updateTime;
        private int pageSize;
        private int currentPage;
        private String id;//关联收款码、收款设备、收银人员的ID
        private String merchantId;
        private String merchantIdCnt;
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String sn;
        private int status;
        private String statusCnt;
        private String signKey;
        private String model;
        private String lableServiceChannelId;
        private String setUp;
        private String payTypeBroadcast;//播报收款类型 0 关闭，1开启
        private String categoryCode;
        private String categoryName;
        private String deviceModel;
        private List<UserListBean> userList;
        private String unitType;//设备型号
        private String deviceId;//设备ID
        private String firmName;//厂商名字
        private String enabled;
        private String configType;//防逃单语音提醒 0 关闭，1开启
        private String iotShowFlag;//是否显示设备状态 0-不显示，1-显示
        private String iotDeviceStatusDesc;//设备状态描述
        private String apiTradeBroadcast;//API交易云播报  1开启，2关闭

        public String getCategoryCode() {
            return categoryCode;
        }

        public void setCategoryCode(String categoryCode) {
            this.categoryCode = categoryCode;
        }

        public String getApiTradeBroadcast() {

            return apiTradeBroadcast == null ? "" : apiTradeBroadcast;

        }

        public void setApiTradeBroadcast(String apiTradeBroadcast) {
            this.apiTradeBroadcast = apiTradeBroadcast;
        }

        public String getIotShowFlag() {
            return iotShowFlag;
        }

        public void setIotShowFlag(String iotShowFlag) {
            this.iotShowFlag = iotShowFlag;
        }

        public String getIotDeviceStatusDesc() {
            return iotDeviceStatusDesc;
        }

        public void setIotDeviceStatusDesc(String iotDeviceStatusDesc) {
            this.iotDeviceStatusDesc = iotDeviceStatusDesc;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getFirmName() {
            return firmName;
        }

        public void setFirmName(String firmName) {
            this.firmName = firmName;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getConfigType() {
            return configType;
        }

        public void setConfigType(String configType) {
            this.configType = configType;
        }

        public String getUnitType() {
            return unitType;
        }

        public void setUnitType(String unitType) {
            this.unitType = unitType;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getPayTypeBroadcast() {
            return payTypeBroadcast;
        }

        public void setPayTypeBroadcast(String payTypeBroadcast) {
            this.payTypeBroadcast = payTypeBroadcast;
        }

        public int getCreateUser() {
            return createUser;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantIdCnt() {
            return merchantIdCnt;
        }

        public void setMerchantIdCnt(String merchantIdCnt) {
            this.merchantIdCnt = merchantIdCnt;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt;
        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public String getSn() {
            return sn;
        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getStatusCnt() {
            return statusCnt;
        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public String getSignKey() {
            return signKey;
        }

        public void setSignKey(String signKey) {
            this.signKey = signKey;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getLableServiceChannelId() {
            return lableServiceChannelId;
        }

        public void setLableServiceChannelId(String lableServiceChannelId) {
            this.lableServiceChannelId = lableServiceChannelId;
        }

        public String getSetUp() {
            return setUp;
        }

        public void setSetUp(String setUp) {
            this.setUp = setUp;
        }

        public List<UserListBean> getUserList() {
            return userList;
        }

        public void setUserList(List<UserListBean> userList) {
            this.userList = userList;
        }

        public static class UserListBean implements Serializable{
            /**
             * pageSize : 0
             * currentPage : 0
             * userId : 0
             * role : 0
             * name : 彭国卿测试商户
             * bind : 1
             * phone : 18665877763
             */

            private int pageSize;
            private int currentPage;
            private String userId;
            private int role;
            private String name;
            private int bind;
            private String phone;

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public int getRole() {
                return role;
            }

            public void setRole(int role) {
                this.role = role;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getBind() {
                return bind;
            }

            public void setBind(int bind) {
                this.bind = bind;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }
        }
    }
}
