package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/3/13 16:36
 * @描述: ${TODO}
 */
public class CloudDevicesBean {
    private ErrorBean error;
    /**
     * logId : e522af5c0d8d4fc58fae5139a6079114
     * status : true
     * data : {"pageSize":15,"currentPage":1,"totalPages":0,"totalRows":0,"data":[{"createUser":1,"createUserName":"超级管理员","createTime":"2019-05-20 16:51:56","updateTime":"2019-05-21 17:41:03","pageSize":0,"currentPage":0,"id":161,"merchantId":"735200000001","merchantIdCnt":"彭国卿测试商户(威富通支付专用)","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","sn":"10002066","status":1,"statusCnt":"已激活","model":"汇播报V1.0","lableServiceChannelId":"0","merchantName":"彭国卿测试商户","salesmanId":253,"serviceProviderId":"319100000001","serviceChannelId":"720100000001","setUp":"{\"payTypeBroadcast\":1}","firmName":"智网","purposeName":"语音播报","deviceId":4}]}
     */
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private String logId;
    private boolean status;
    private DataBeanX data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * pageSize : 15
         * currentPage : 1
         * totalPages : 0
         * totalRows : 0
         * data : [{"createUser":1,"createUserName":"超级管理员","createTime":"2019-05-20 16:51:56","updateTime":"2019-05-21 17:41:03","pageSize":0,"currentPage":0,"id":161,"merchantId":"735200000001","merchantIdCnt":"彭国卿测试商户(威富通支付专用)","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","sn":"10002066","status":1,"statusCnt":"已激活","model":"汇播报V1.0","lableServiceChannelId":"0","merchantName":"彭国卿测试商户","salesmanId":253,"serviceProviderId":"319100000001","serviceChannelId":"720100000001","setUp":"{\"payTypeBroadcast\":1}","firmName":"智网","purposeName":"语音播报","deviceId":4}]
         */

        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<DataBean> data;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean implements Serializable {
            /**
             * createUser : 1
             * createUserName : 超级管理员
             * createTime : 2019-05-20 16:51:56
             * updateTime : 2019-05-21 17:41:03
             * pageSize : 0
             * currentPage : 0
             * id : 161
             * merchantId : 735200000001
             * merchantIdCnt : 彭国卿测试商户(威富通支付专用)
             * storeMerchantId : 462300000001
             * storeMerchantIdCnt : 彭国卿测试商户
             * sn : 10002066
             * status : 1
             * statusCnt : 已激活
             * model : 汇播报V1.0
             * lableServiceChannelId : 0
             * merchantName : 彭国卿测试商户
             * salesmanId : 253
             * serviceProviderId : 319100000001
             * serviceChannelId : 720100000001
             * setUp : {"payTypeBroadcast":1}
             * firmName : 智网
             * purposeName : 语音播报
             * deviceId : 4
             */

            private int createUser;
            private String createUserName;
            private String createTime;
            private String updateTime;
            private int pageSize;
            private int currentPage;
            private int id;
            private String merchantId;
            private String merchantIdCnt;
            private String storeMerchantId;
            private String storeMerchantIdCnt;
            private String sn;
            private String key;
            private int status;
            private String statusCnt;
            private String model;
            private String lableServiceChannelId;
            private String merchantName;
            private int salesmanId;
            private String serviceProviderId;
            private String serviceChannelId;
            private String setUp;
            private String firmName;
            private String purposeName;
            private int deviceId;
            private String userId;
            private String userIdCnt;
            private String firm;

            //界面需要的辅助字段
            private boolean isTitleView;//是否是标题
            private boolean isBluthDevice;//是否是蓝牙打印机
            private String bluthPrintName;//蓝牙打印机名称
            private boolean isSelect;//是否被选中

            public boolean isSelect() {
                return isSelect;
            }

            public void setSelect(boolean select) {
                isSelect = select;
            }

            public String getBluthPrintName() {
                return bluthPrintName;
            }

            public void setBluthPrintName(String bluthPrintName) {
                this.bluthPrintName = bluthPrintName;
            }

            public boolean isBluthDevice() {
                return isBluthDevice;
            }

            public void setBluthDevice(boolean bluthDevice) {
                isBluthDevice = bluthDevice;
            }

            public boolean isTitleView() {
                return isTitleView;
            }

            public void setTitleView(boolean titleView) {
                isTitleView = titleView;
            }

            public String getFirm() {
                return firm;
            }

            public void setFirm(String firm) {
                this.firm = firm;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getUserIdCnt() {
                return userIdCnt;
            }

            public void setUserIdCnt(String userIdCnt) {
                this.userIdCnt = userIdCnt;
            }

            public int getCreateUser() {
                return createUser;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }

            public String getMerchantIdCnt() {
                return merchantIdCnt;
            }

            public void setMerchantIdCnt(String merchantIdCnt) {
                this.merchantIdCnt = merchantIdCnt;
            }

            public String getStoreMerchantId() {
                return storeMerchantId;
            }

            public void setStoreMerchantId(String storeMerchantId) {
                this.storeMerchantId = storeMerchantId;
            }

            public String getStoreMerchantIdCnt() {
                return storeMerchantIdCnt;
            }

            public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
                this.storeMerchantIdCnt = storeMerchantIdCnt;
            }

            public String getSn() {
                return sn;
            }

            public void setSn(String sn) {
                this.sn = sn;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getStatusCnt() {
                return statusCnt;
            }

            public void setStatusCnt(String statusCnt) {
                this.statusCnt = statusCnt;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public String getLableServiceChannelId() {
                return lableServiceChannelId;
            }

            public void setLableServiceChannelId(String lableServiceChannelId) {
                this.lableServiceChannelId = lableServiceChannelId;
            }

            public String getMerchantName() {
                return merchantName;
            }

            public void setMerchantName(String merchantName) {
                this.merchantName = merchantName;
            }

            public int getSalesmanId() {
                return salesmanId;
            }

            public void setSalesmanId(int salesmanId) {
                this.salesmanId = salesmanId;
            }

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public String getServiceChannelId() {
                return serviceChannelId;
            }

            public void setServiceChannelId(String serviceChannelId) {
                this.serviceChannelId = serviceChannelId;
            }

            public String getSetUp() {
                return setUp;
            }

            public void setSetUp(String setUp) {
                this.setUp = setUp;
            }

            public String getFirmName() {
                return firmName;
            }

            public void setFirmName(String firmName) {
                this.firmName = firmName;
            }

            public String getPurposeName() {
                return purposeName;
            }

            public void setPurposeName(String purposeName) {
                this.purposeName = purposeName;
            }

            public int getDeviceId() {
                return deviceId;
            }

            public void setDeviceId(int deviceId) {
                this.deviceId = deviceId;
            }
        }
    }
}
