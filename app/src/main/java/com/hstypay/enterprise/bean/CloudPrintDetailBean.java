package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/2/2 10:51
 * @描述: ${TODO}
 */

public class CloudPrintDetailBean {
    private ErrorBean error;
    /**
     * logId : dc3e81644fd64e8a91df301f736249a6
     * status : true
     * data : {"createUser":1,"createUserName":"超级管理员","createTime":"2019-05-20 16:51:56","updateTime":"2019-05-22 16:23:52","pageSize":0,"currentPage":0,"id":161,"merchantId":"735200000001","merchantIdCnt":"彭国卿测试商户(威富通支付专用)","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","sn":"10002066","status":1,"statusCnt":"已激活","signKey":"PhRpCUWvSdDIF1jaV8sBHR8UwTonnTU7","model":"汇播报V1.0","lableServiceChannelId":"0","setUp":"{\"PAY_TYPE_BROAD_CAST\":1}","userList":[{"pageSize":0,"currentPage":0,"userId":0,"role":0,"name":"彭国卿测试商户","bind":1},{"pageSize":0,"currentPage":0,"userId":782,"role":2,"phone":"18665877763","name":"123456","bind":2},{"pageSize":0,"currentPage":0,"userId":783,"role":4,"phone":"15474253652","name":"不讲理","bind":2},{"pageSize":0,"currentPage":0,"userId":784,"role":2,"phone":"15325248745","name":"2222222","bind":2},{"pageSize":0,"currentPage":0,"userId":785,"role":4,"phone":"15800000009","name":"店长2","bind":2},{"pageSize":0,"currentPage":0,"userId":786,"role":4,"phone":"13417510012","name":"店长1","bind":2},{"pageSize":0,"currentPage":0,"userId":787,"role":2,"phone":"15800000004","name":"363633","bind":2},{"pageSize":0,"currentPage":0,"userId":788,"role":2,"phone":"13417510014","name":"风扇","bind":2},{"pageSize":0,"currentPage":0,"userId":793,"role":4,"phone":"17722404212","name":"will","bind":2},{"pageSize":0,"currentPage":0,"userId":857,"role":4,"phone":"13250758566","name":"彭国卿测试店长","bind":2},{"pageSize":0,"currentPage":0,"userId":868,"role":2,"phone":"18718571111","name":"尤永圣","bind":2},{"pageSize":0,"currentPage":0,"userId":895,"role":2,"phone":"15100000000","name":"测试收银员","bind":2},{"pageSize":0,"currentPage":0,"userId":901,"role":2,"phone":"18588243549","name":"测试验证","bind":2},{"pageSize":0,"currentPage":0,"userId":902,"role":4,"phone":"18588243547","name":"测试店长","bind":2},{"pageSize":0,"currentPage":0,"userId":933,"role":2,"phone":"13417510016","name":"嗯嗯","bind":2},{"pageSize":0,"currentPage":0,"userId":951,"role":2,"phone":"13729087765","name":"嗯嗯","bind":2},{"pageSize":0,"currentPage":0,"userId":987,"role":2,"phone":"18600000001","name":"张丹411","bind":2},{"pageSize":0,"currentPage":0,"userId":996,"role":4,"phone":"13184274627","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":997,"role":4,"phone":"13184274628","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":998,"role":4,"phone":"15899868195","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":999,"role":2,"phone":"13184274630","name":"w123456","bind":2},{"pageSize":0,"currentPage":0,"userId":1127,"role":2,"phone":"18955466666","name":"彭国卿收银员","bind":2}]}
     */

    private String logId;
    private boolean status;
    private DataBean data;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable, Cloneable {
/*
{
    "logId": "893bc70d000941b88eee431c57509b73",
    "status": true,
    "data": {
        "createUser": 1,
        "createUserName": "超级管理员",
        "createTime": "2020-09-22 16:34:31",
        "updateTime": "2020-09-23 22:03:28",
        "pageSize": 0,
        "currentPage": 0,
        "id": 2094,
        "merchantId": "1020001616",
        "merchantIdCnt": "hydream商户专用",
        "storeMerchantId": "1030001511",
        "storeMerchantIdCnt": "hydream商户专用",
        "sn": "KD900000363",
        "status": 1,
        "statusCnt": "已绑定",
        "signKey": "XbNaxNQZitQqfKXWBjogOxCKDapFImdz",
        "model": "云打印",
        "lableServiceChannelId": "0",
        "setUp": "{\"count_merge_print\":\"1\",\"customer_print\":\"1\",\"kitchen_print\":\"1\",\"settlement_print\":\"1\"}",
        "purposeCode": 13,
        "unitType": "KD90",
        "firmName": "新国都",
        "deviceId": 23,
        "configType": 1,
        "enabled": 1,
        "state": "离线",
        "waitingPrintCount": "0"
    }
}
* */

        private SetUp setUpBean;

        private String setUp;

        private String sn;
        private String model;//设备类型

        private String state;//1、离线。 2、在线，工作状态正常。 3、在线，工作状态不正常

        private String waitingPrintCount;//待打印数量

        private String enabled;//自动打印小票标志 0 关闭，1开启

        private String configType;//票据类型 1 商户存根，2用户+商户存根

        private String unitType;//设备型号

        private String firmName;//厂商名字
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String deviceId;//设备ID
        private String id;//关联收款码、收款设备、收银人员的ID

        private String categoryName;//设备类型名称
        private String deviceModel;//设备型号

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt;
        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getDeviceModel() {
            return deviceModel;
        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public SetUp getSetUpBean() {
            return setUpBean;
        }

        public void setSetUpBean(SetUp setUpBean) {
            this.setUpBean = setUpBean;
        }

        public String getSetUp() {
            return setUp;
        }

        public void setSetUp(String setUp) {
            this.setUp = setUp;
        }

        public String getSn() {
            return sn;
        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getWaitingPrintCount() {
            return waitingPrintCount;
        }

        public void setWaitingPrintCount(String waitingPrintCount) {
            this.waitingPrintCount = waitingPrintCount;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getConfigType() {
            return configType;
        }

        public void setConfigType(String configType) {
            this.configType = configType;
        }

        public String getUnitType() {
            return unitType;
        }

        public void setUnitType(String unitType) {
            this.unitType = unitType;
        }

        public String getFirmName() {
            return firmName;
        }

        public void setFirmName(String firmName) {
            this.firmName = firmName;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        @Override
        public Object clone() {
            DataBean ben = null;
            try {
                ben = (DataBean) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return ben;
        }
    }


}
