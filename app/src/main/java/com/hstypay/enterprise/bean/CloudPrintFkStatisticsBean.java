package com.hstypay.enterprise.bean;

/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc 云打印饭卡统计信息
 */
public class CloudPrintFkStatisticsBean {
    /**
     * 商户名
     */
    private String merchantName;

    /**
     * 标题
     */
    private String title;

    /**
     * 商户编号
     */
    private String merchantId;

    /**
     * 交易门店
     */
    private String storeName;

    /**
     * 收银人员
     */
    private String opUserName;

    /**
     * 开始时间
     */
    private String startTimeStr;

    /**
     * 结束时间
     */
    private String endTimeStr;

    /**
     * 交易金额 单位元，精确到分，100.00
     */
    private String tradeAmount;

    /**
     * 优惠金额 单位元，精确到分，100.00
     */
    private String discountAmount;

    /**
     * 核销金额 单位元，精确到分，100.00
     */
    private String consumerAmount;

    /**
     * 核销净额 单位元，精确到分，100.00
     */
    private String income;

    /**
     * 退款金额 单位元，精确到分，100.00
     */
    private String refundAmount;

    /**
     * 交易笔数
     */
    private String tradeNum;

    /**
     * 退款笔数
     */
    private String refundNum;

    /**
     * 打印时间 yyyy-MM-dd HH:mm:ss
     */
    private String printTimeStr;

    private int printMode;//1：自动、2：手动

    /**
     * 结算率 %
     * */
    private String settleRate;
    /**
     *结算金额 单位元，精确到分，100.00
     * */
    private String settleAmount;

    private String outRequestNo;

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public String getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(String tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getConsumerAmount() {
        return consumerAmount;
    }

    public void setConsumerAmount(String consumerAmount) {
        this.consumerAmount = consumerAmount;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getTradeNum() {
        return tradeNum;
    }

    public void setTradeNum(String tradeNum) {
        this.tradeNum = tradeNum;
    }

    public String getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(String refundNum) {
        this.refundNum = refundNum;
    }

    public String getPrintTimeStr() {
        return printTimeStr;
    }

    public void setPrintTimeStr(String printTimeStr) {
        this.printTimeStr = printTimeStr;
    }

    public int getPrintMode() {
        return printMode;
    }

    public void setPrintMode(int printMode) {
        this.printMode = printMode;
    }

    public String getSettleRate() {
        return settleRate;
    }

    public void setSettleRate(String settleRate) {
        this.settleRate = settleRate;
    }

    public String getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(String settleAmount) {
        this.settleAmount = settleAmount;
    }

    public String getOutRequestNo() {
        return outRequestNo;
    }

    public void setOutRequestNo(String outRequestNo) {
        this.outRequestNo = outRequestNo;
    }
}
