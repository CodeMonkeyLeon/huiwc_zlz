package com.hstypay.enterprise.bean;

/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc 云打印收款统计信息
 */
public class CloudPrintIncomeStatisticsBean {
    private String merchantName;//商户名
    private String title;//标题
    private String merchantId;//商户编号
    private String storeName;//交易门店
    private String opUserName;//收银人员
    private String startTimeStr;//开始时间
    private String endTimeStr;//结束时间
    private String tradeAmount;//交易金额 单位元，精确到分，100.00
    private String discountAmount;//优惠金额 单位元，精确到分，100.00
    private String receiptAmount;//实收金额 单位元，精确到分，100.00
    private String refundAmount;//退款金额 单位元，精确到分，100.00
    private String tradeNetAmount;//交易净额 单位元，精确到分，100.00
    private String tradeNum;//交易笔数
    private String refundNum;//退款笔数
    private String wxPayAmount;//微信支付金额
    private String wxPayNum;//微信支付笔数
    private String aliPayAmount;//支付宝支付金额
    private String aliPayNum;//支付宝支付笔数
    private String unionPayAmount;//银联支付金额
    private String unionPayNum;//银联支付笔数
    private String payCardAmount;//刷卡
    private String payCardNum;//刷卡
    private String printTimeStr;//打印时间
    private int printMode;//1：自动、2：手动

    public int getPrintMode() {
        return printMode;
    }

    public void setPrintMode(int printMode) {
        this.printMode = printMode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public String getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(String tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(String receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getTradeNetAmount() {
        return tradeNetAmount;
    }

    public void setTradeNetAmount(String tradeNetAmount) {
        this.tradeNetAmount = tradeNetAmount;
    }

    public String getTradeNum() {
        return tradeNum;
    }

    public void setTradeNum(String tradeNum) {
        this.tradeNum = tradeNum;
    }

    public String getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(String refundNum) {
        this.refundNum = refundNum;
    }

    public String getWxPayAmount() {
        return wxPayAmount;
    }

    public void setWxPayAmount(String wxPayAmount) {
        this.wxPayAmount = wxPayAmount;
    }

    public String getWxPayNum() {
        return wxPayNum;
    }

    public void setWxPayNum(String wxPayNum) {
        this.wxPayNum = wxPayNum;
    }

    public String getAliPayAmount() {
        return aliPayAmount;
    }

    public void setAliPayAmount(String aliPayAmount) {
        this.aliPayAmount = aliPayAmount;
    }

    public String getAliPayNum() {
        return aliPayNum;
    }

    public void setAliPayNum(String aliPayNum) {
        this.aliPayNum = aliPayNum;
    }

    public String getUnionPayAmount() {
        return unionPayAmount;
    }

    public void setUnionPayAmount(String unionPayAmount) {
        this.unionPayAmount = unionPayAmount;
    }

    public String getUnionPayNum() {
        return unionPayNum;
    }

    public void setUnionPayNum(String unionPayNum) {
        this.unionPayNum = unionPayNum;
    }

    public String getPayCardAmount() {
        return payCardAmount;
    }

    public void setPayCardAmount(String payCardAmount) {
        this.payCardAmount = payCardAmount;
    }

    public String getPayCardNum() {
        return payCardNum;
    }

    public void setPayCardNum(String payCardNum) {
        this.payCardNum = payCardNum;
    }

    public String getPrintTimeStr() {
        return printTimeStr;
    }

    public void setPrintTimeStr(String printTimeStr) {
        this.printTimeStr = printTimeStr;
    }
}
