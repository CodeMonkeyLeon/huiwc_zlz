package com.hstypay.enterprise.bean;

/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc 押金解冻云打印信息
 */
public class CloudPrintPledgeConsumeBean {
    /**
     * 商户名
     */
    private String merchantName;

    /**
     * 订单号 解冻交易汇商平台订单号
     */
    private String orderNo;

    /**
     * 存根名：商户存根、用户存根
     */
    private String stubName;

    /**
     * 商户id
     */
    private String merchantId;

    /**
     * 交易门店
     */
    private String storeName;

    /**
     * 交易类型
     */
    private String tradeType;

    /**
     * 退款人
     */
    private String opUserName;

    /**
     * 退款申请时间 yyyy-MM-dd HH:mm:ss
     */
    private String refundApplyTimeStr;

    /**
     * 交易状态
     */
    private String tradeStatus;

    /**
     * 消费金额  单位元 保留2位小数 10.00
     */
    private String consumeAmount;

    /**
     * 退款金额 单位元  保留2位小数 10.00
     */
    private String refundAmount;

    /**
     * 打印时间 yyyy-MM-dd HH:mm:ss
     */
    private String printTimeStr;

    private int printMode;//1：自动、2：手动

    public int getPrintMode() {
        return printMode;
    }

    public void setPrintMode(int printMode) {
        this.printMode = printMode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getStubName() {
        return stubName;
    }

    public void setStubName(String stubName) {
        this.stubName = stubName;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getRefundApplyTimeStr() {
        return refundApplyTimeStr;
    }

    public void setRefundApplyTimeStr(String refundApplyTimeStr) {
        this.refundApplyTimeStr = refundApplyTimeStr;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getConsumeAmount() {
        return consumeAmount;
    }

    public void setConsumeAmount(String consumeAmount) {
        this.consumeAmount = consumeAmount;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getPrintTimeStr() {
        return printTimeStr;
    }

    public void setPrintTimeStr(String printTimeStr) {
        this.printTimeStr = printTimeStr;
    }
}
