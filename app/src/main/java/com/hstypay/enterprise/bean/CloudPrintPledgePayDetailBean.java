package com.hstypay.enterprise.bean;

/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc 押金收款云打印信息
 */
public class CloudPrintPledgePayDetailBean {

    /**
     * 商户名
     */
    private String merchantName;

    /**
     * 订单号,商户平台预授权订单号
     */
    private String orderNo;

    /**
     * 存根名
     */
    private String stubName;

    /**
     * 商户编号
     */
    private String merchantId;

    /**
     * 交易门店
     */
    private String storeName;

    /**
     * 交易类型
     */
    private String tradeType;

    /**
     * 收银人员
     */
    private String opUserName;

    /**
     * 交易订单号，微信预授权订单号
     */
    private String tradeOrderNo;

    /**
     * 交易状态
     */
    private String tradeStatus;

    /**
     * 支付方式
     */
    private String payType;

    /**
     * 交易时间 yyyy-MM-dd HH:mm:ss
     */
    private String payTimeStr;

    /**
     * 预授权金额 单位元，精确到分，100.00
     */
    private String sumPreMoney;

    /**
     * 备注
     */
    private String remark;

    /**
     * 打印时间 yyyy-MM-dd HH:mm:ss
     */
    private String printTimeStr;

    private int printMode;//1：自动、2：手动

    public int getPrintMode() {
        return printMode;
    }

    public void setPrintMode(int printMode) {
        this.printMode = printMode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getStubName() {
        return stubName;
    }

    public void setStubName(String stubName) {
        this.stubName = stubName;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getTradeOrderNo() {
        return tradeOrderNo;
    }

    public void setTradeOrderNo(String tradeOrderNo) {
        this.tradeOrderNo = tradeOrderNo;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayTimeStr() {
        return payTimeStr;
    }

    public void setPayTimeStr(String payTimeStr) {
        this.payTimeStr = payTimeStr;
    }

    public String getSumPreMoney() {
        return sumPreMoney;
    }

    public void setSumPreMoney(String sumPreMoney) {
        this.sumPreMoney = sumPreMoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPrintTimeStr() {
        return printTimeStr;
    }

    public void setPrintTimeStr(String printTimeStr) {
        this.printTimeStr = printTimeStr;
    }
}
