package com.hstypay.enterprise.bean;

/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc 押金统计云打印信息
 */
public class CloudPrintPledgeReportBean {
    /**
     * 商户名
     */
    private String merchantName;

    /**
     * 标题
     */
    private String title;

    /**
     * 商户编号
     */
    private String merchantId;

    /**
     * 交易门店
     */
    private String storeName;

    /**
     * 收银人员
     */
    private String opUserName;

    /**
     * 开始时间
     */
    private String startTimeStr;

    /**
     * 结束时间
     */
    private String endTimeStr;

    /**
     * 押金金额 单位元，精确到分，100.00
     */
    private String depositAmount;

    /**
     * 押金笔数
     */
    private String depositNum;

    /**
     * 消费金额 单位元，精确到分，100.00
     */
    private String consumeAmount;

    /**
     * 消费笔数
     */
    private String consumeNum;

    /**
     * 退款金额 单位元，精确到分，100.00
     */
    private String refundAmount;


    /**
     * 退款笔数
     */
    private String refundNum;

    /**
     * 打印时间 yyyy-MM-dd HH:mm:ss
     */
    private String printTimeStr;

    private int printMode;//1：自动、2：手动

    public int getPrintMode() {
        return printMode;
    }

    public void setPrintMode(int printMode) {
        this.printMode = printMode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public String getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getDepositNum() {
        return depositNum;
    }

    public void setDepositNum(String depositNum) {
        this.depositNum = depositNum;
    }

    public String getConsumeAmount() {
        return consumeAmount;
    }

    public void setConsumeAmount(String consumeAmount) {
        this.consumeAmount = consumeAmount;
    }

    public String getConsumeNum() {
        return consumeNum;
    }

    public void setConsumeNum(String consumeNum) {
        this.consumeNum = consumeNum;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(String refundNum) {
        this.refundNum = refundNum;
    }

    public String getPrintTimeStr() {
        return printTimeStr;
    }

    public void setPrintTimeStr(String printTimeStr) {
        this.printTimeStr = printTimeStr;
    }
}
