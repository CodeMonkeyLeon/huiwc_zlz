package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc
 */
public class CloudPrintResultBean extends BaseBean<List<CloudPrintResultBean.Data>> {

    public static class Data{
        private String printStatus;
        private String sn;
    }
}
