package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hsty.wankepay.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 10:51
 * @描述: ${TODO}
 */

public class CodeListBean {
    /**
     * data : [{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"}]
     * logId : 9NKy0gJG
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{
        /**
         * qrcode : [{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}]
         * storeName : 支付接口联调测试数据_门店
         * storeId : 101300000001
         */
        private List<QrcodeEntity> qrcode;
        private String storeName;
        private String storeId;

        public void setQrcode(List<QrcodeEntity> qrcode) {
            this.qrcode = qrcode;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public List<QrcodeEntity> getQrcode() {
            return qrcode;
        }

        public String getStoreName() {
            return storeName;
        }

        public String getStoreId() {
            return storeId;
        }

        public class QrcodeEntity implements Serializable {
            /**
             * qrcodeId : 133fd91839c54f1f9c538727b304e085
             */
            private String qrcode;
            private String qrUrl;
            private String qrType;
            private long id;
            private boolean isClicked;
            private int logoType;
            private String qrLogo;
            private List<String> apiProvider;

            public int getLogoType() {
                return logoType;
            }

            public void setLogoType(int logoType) {
                this.logoType = logoType;
            }

            public String getQrLogo() {
                return qrLogo;
            }

            public void setQrLogo(String qrLogo) {
                this.qrLogo = qrLogo;
            }

            public List<String> getApiProvider() {
                return apiProvider;
            }

            public void setApiProvider(List<String> apiProvider) {
                this.apiProvider = apiProvider;
            }

            public String getQrUrl() {
                return qrUrl;
            }

            public void setQrUrl(String qrUrl) {
                this.qrUrl = qrUrl;
            }

            public String getQrType() {
                return qrType;
            }

            public void setQrType(String qrType) {
                this.qrType = qrType;
            }

            public boolean isClicked() {
                return isClicked;
            }

            public void setClicked(boolean clicked) {
                isClicked = clicked;
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public void setQrcodeId(String qrcode) {
                this.qrcode = qrcode;
            }

            public String getQrcodeId() {
                return qrcode;
            }
        }
    }
}
