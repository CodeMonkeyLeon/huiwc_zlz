package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/11 19:26
 * @描述: ${TODO}
 */

public class ConfigBean {
    /**
     * data : {"realName":"yaokun","userName":"liuyeyu123","userId":39}
     * logId : 1jPRuVRa
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * realName : [
         *   "PAY",
         *   "BILL",
         *   "SCAN",
         *   "COUNT",
         *   "DECCA",
         *   "SOUND",
         *   "DEVICE",
         *   "FORGET",
         *   "NOTICE",
         *   "REPORT",
         *   "DEPOSIT",
         *   "SETTING",
         *   "REGISTER",
         *   "BANK-CARD",
         *   "SIGN-INFO",
         *   "HELP-CENTER",
         *   "SHOP-MANAGE",
         *   "STORE-MANAGE",
         *   "STORE-QRCODE",
         *   "CASHER-MANAGE",
         *   "MERCHANT-INFO"
         *   "SCAN-ORDER-FOOD",
         *   "HBFQ"
         * ]
         *
         * userName : [
         *   "收款",
         *   "账单",
         *   "扫一扫",
         *   "统计",
         *   "动态台卡",
         *   "收款音箱",
         *   "收款设备",
         *   "忘记密码",
         *   "通知",
         *   "报表",
         *   "押金收款",
         *   "设置",
         *   "新用户注册",
         *   "我的银行卡",
         *   "签约信息",
         *   "帮助中心",
         *   "店长管理",
         *   "门店管理",
         *   "门店收款码",
         *   "收银员管理",
         *   "商户信息"
         *   "扫码点餐",
         *   "花呗分期"
         * ]
         */
        private String permissionCode;
        private String permissionName;

        public String getPermissionCode() {
            return permissionCode;
        }

        public void setPermissionCode(String permissionCode) {
            this.permissionCode = permissionCode;
        }

        public String getPermissionName() {
            return permissionName;
        }

        public void setPermissionName(String permissionName) {
            this.permissionName = permissionName;
        }
    }

}
