package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/8/21 17:57
 * @描述: ${TODO}
 */
public class Coupon implements Serializable {
    /**
     * 优惠方式
     */
    private String couponName;
    /**
     * 优惠金额
     */
    private long parValue;

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public long getParValue() {
        return parValue;
    }

    public void setParValue(long parValue) {
        this.parValue = parValue;
    }
}
