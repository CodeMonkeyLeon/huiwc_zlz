package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/22 11:24
 * @描述: ${TODO}
 */

public class DeviceManagerBean {
    private int imageRes;
    private int function;
    private int funcNameRes;
    private int funcInstructionRes;

    public DeviceManagerBean(){}

    public DeviceManagerBean(int imageRes, int function, int funcNameRes,int funcInstructionRes) {
        this.imageRes = imageRes;
        this.function = function;
        this.funcNameRes = funcNameRes;
        this.funcInstructionRes = funcInstructionRes;
    }

    public int getFunction() {
        return function;
    }

    public void setFunction(int function) {
        this.function = function;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public int getFuncNameRes() {
        return funcNameRes;
    }

    public void setFuncNameRes(int funcNameRes) {
        this.funcNameRes = funcNameRes;
    }

    public int getFuncInstructionRes() {
        return funcInstructionRes;
    }

    public void setFuncInstructionRes(int funcInstructionRes) {
        this.funcInstructionRes = funcInstructionRes;
    }
}
