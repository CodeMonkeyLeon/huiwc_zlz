package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/2/2 10:51
 * @描述: ${TODO}
 */

public class DevicesBean {
    /**
     * data : [{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"},{"qrcode":[{"qrcodeId":"133fd91839c54f1f9c538727b304e085"},{"qrcodeId":"20788c7008974dd8acf3842bbefffebd"},{"qrcodeId":"5bd53802111c426fa522c6666d94fdbd"},{"qrcodeId":"5e56b35acd4f4faf83b14d0158e43f5d"},{"qrcodeId":"65cfdc2bd35d46a3bd210043ab872432"}],"storeName":"支付接口联调测试数据_门店","storeId":"101300000001"}]
     * logId : 9NKy0gJG
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"createTime":"2018-02-02 10:35:48","merchantId":"735200000001","createUser":5,"createUserName":"彭国卿","updateTime":"2018-02-02 10:35:48","signKey":"sh8jRiNjqLm2pHtb5nRgIoj5zXrlco1h","id":4,"sn":"1803AQ500128","statusCnt":"已激活","status":1},{"createTime":"2018-01-24 15:43:26","merchantId":"735200000001","createUser":5,"createUserName":"彭国卿","updateTime":"2018-02-02 10:34:43","signKey":"BEe2LEZ6k4PaQhkLouhNT2dXbBVn2GFI","id":1,"sn":"1803AQ500142","statusCnt":"已激活","status":1}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : qHnbSS9T
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * data : [{"createTime":"2018-02-02 10:35:48","merchantId":"735200000001","createUser":5,"createUserName":"彭国卿","updateTime":"2018-02-02 10:35:48","signKey":"sh8jRiNjqLm2pHtb5nRgIoj5zXrlco1h","id":4,"sn":"1803AQ500128","statusCnt":"已激活","status":1},{"createTime":"2018-01-24 15:43:26","merchantId":"735200000001","createUser":5,"createUserName":"彭国卿","updateTime":"2018-02-02 10:34:43","signKey":"BEe2LEZ6k4PaQhkLouhNT2dXbBVn2GFI","id":1,"sn":"1803AQ500142","statusCnt":"已激活","status":1}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<DataItem> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<DataItem> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<DataItem> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public class DataItem {
            /**
             * createTime : 2018-02-02 10:35:48
             * merchantId : 735200000001
             * createUser : 5
             * createUserName : 彭国卿
             * updateTime : 2018-02-02 10:35:48
             * signKey : sh8jRiNjqLm2pHtb5nRgIoj5zXrlco1h
             * id : 4
             * sn : 1803AQ500128
             * statusCnt : 已激活
             * status : 1
             */
            private String createTime;
            private String merchantId;
            private int createUser;
            private String createUserName;
            private String updateTime;
            private String signKey;
            private int id;
            private String sn;
            private String statusCnt;
            private int status;

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setSignKey(String signKey) {
                this.signKey = signKey;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setSn(String sn) {
                this.sn = sn;
            }

            public void setStatusCnt(String statusCnt) {
                this.statusCnt = statusCnt;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getCreateTime() {
                return createTime;
            }

            public String getMerchantId() {
                return merchantId;
            }

            public int getCreateUser() {
                return createUser;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public String getSignKey() {
                return signKey;
            }

            public int getId() {
                return id;
            }

            public String getSn() {
                return sn;
            }

            public String getStatusCnt() {
                return statusCnt;
            }

            public int getStatus() {
                return status;
            }
        }
    }
}
