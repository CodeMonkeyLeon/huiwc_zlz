package com.hstypay.enterprise.bean;

public class EasypayInfo {

    /**
     * acquirer : 48312900
     * amount : 0.01
     * batch_no : 000007
     * card_no : 5187100017892755
     * date_time : 20200210 194106
     * exp_date : 2103
     * issuer : 03080010
     * merchant_code : 831290075120021
     * merchant_name : 华势预授权测试
     * operator : 01
     * refer_no : 947149194106
     * term_code : 11002127
     * trace : 000215
     * trans_type : card
     */

    private String acquirer;
    private String amount;
    private String batch_no;
    private String card_no;
    private String date_time;
    private String exp_date;
    private String issuer;
    private String merchant_code;
    private String merchant_name;
    private String operator;
    private String refer_no;
    private String term_code;
    private String trace;
    private String trans_type;

    public String getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(String acquirer) {
        this.acquirer = acquirer;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getMerchant_code() {
        return merchant_code;
    }

    public void setMerchant_code(String merchant_code) {
        this.merchant_code = merchant_code;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getRefer_no() {
        return refer_no;
    }

    public void setRefer_no(String refer_no) {
        this.refer_no = refer_no;
    }

    public String getTerm_code() {
        return term_code;
    }

    public void setTerm_code(String term_code) {
        this.term_code = term_code;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }
}
