package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 员工管理页bean
 */
public class EmployeeManageBean extends BaseBean<List<EmployeeManageBean.EmployeeManageItemBean>>{
    public static  class EmployeeManageItemBean{
        private String employeeName;//员工名称
        private int role;//员工角色 1是店长，2是收银员
        private String storeName;//所属店名称
        private String telPhone;//手机号码

        public String getEmployeeName() {

            return employeeName == null ? "" : employeeName;

        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public int getRole() {

            return role;

        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getStoreName() {

            return storeName == null ? "" : storeName;

        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getTelPhone() {

            return telPhone == null ? "" : telPhone;

        }

        public void setTelPhone(String telPhone) {
            this.telPhone = telPhone;
        }
    }
}
