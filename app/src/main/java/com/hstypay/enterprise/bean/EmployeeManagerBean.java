package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/4
 * @desc 员工管理
 */
public class EmployeeManagerBean extends BaseBean<List<EmployeeManagerBean.EmployeeManagerData>>{

    public static class EmployeeManagerData{
        /*
            "storeName": "廖测试3",
            "userId": 6467,
            "userIdCnt": "测试1106",
            "role": 2,
            "name": "测试1106",
            "phone": "11133003167",
            "empName": "收银员"
        * */
        private String storeName;
        private String userId;
        private String userIdCnt;
        private int role;
        private String name;
        private String phone;
        private String empName;
        /** 激活状态, 0 激活失败 1 激活成功 2 未激活 */
        private int activateStatus;

        public int getActivateStatus() {
            return activateStatus;
        }

        public void setActivateStatus(int activateStatus) {
            this.activateStatus = activateStatus;
        }

        public String getStoreName() {

            return storeName == null ? "" : storeName;

        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getUserId() {

            return userId == null ? "" : userId;

        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserIdCnt() {

            return userIdCnt == null ? "" : userIdCnt;

        }

        public void setUserIdCnt(String userIdCnt) {
            this.userIdCnt = userIdCnt;
        }

        public int getRole() {

            return role;

        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getName() {

            return name == null ? "" : name;

        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {

            return phone == null ? "" : phone;

        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmpName() {

            return empName == null ? "" : empName;

        }

        public void setEmpName(String empName) {
            this.empName = empName;
        }
    }
}
