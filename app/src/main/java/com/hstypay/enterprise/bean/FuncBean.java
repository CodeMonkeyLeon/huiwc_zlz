package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 17:38
 * @描述: ${TODO}
 */

public class FuncBean {
    private DevicesBean.ErrorBean error;
    public DevicesBean.ErrorBean getError() {
        return error;
    }
    public void setError(DevicesBean.ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * logId : b3ed0bbd1c6447eb8af9442b57db7b7a
     * status : true
     * data : [{"pageSize":0,"currentPage":0,"permissionCode":"PAY","permissionName":"收款"},{"pageSize":0,"currentPage":0,"permissionCode":"BILL","permissionName":"账单"},{"pageSize":0,"currentPage":0,"permissionCode":"HBFQ","permissionName":"花呗分期"},{"pageSize":0,"currentPage":0,"permissionCode":"SCAN","permissionName":"扫一秒"},{"pageSize":0,"currentPage":0,"permissionCode":"COUNT","permissionName":"统计"},{"pageSize":0,"currentPage":0,"permissionCode":"DECCA","permissionName":"动态台卡"},{"pageSize":0,"currentPage":0,"permissionCode":"SOUND","permissionName":"收款音响"},{"pageSize":0,"currentPage":0,"permissionCode":"DEVICE","permissionName":"收款设备"},{"pageSize":0,"currentPage":0,"permissionCode":"FORGET","permissionName":"忘记密码"},{"pageSize":0,"currentPage":0,"permissionCode":"NOTICE","permissionName":"通知"},{"pageSize":0,"currentPage":0,"permissionCode":"REPORT","permissionName":"报表"},{"pageSize":0,"currentPage":0,"permissionCode":"DEPOSIT","permissionName":"押金收款"},{"pageSize":0,"currentPage":0,"permissionCode":"SETTING","permissionName":"设置"},{"pageSize":0,"currentPage":0,"permissionCode":"REGISTER","permissionName":"注册"},{"pageSize":0,"currentPage":0,"permissionCode":"BANK-CARD","permissionName":"我的银行卡"},{"pageSize":0,"currentPage":0,"permissionCode":"SIGN-INFO","permissionName":"签约信息"},{"pageSize":0,"currentPage":0,"permissionCode":"HELP-CENTER","permissionName":"帮助中心"},{"pageSize":0,"currentPage":0,"permissionCode":"SHOP-MANAGE","permissionName":"店长管理"},{"pageSize":0,"currentPage":0,"permissionCode":"STORE-MANAGE","permissionName":"门店管理"},{"pageSize":0,"currentPage":0,"permissionCode":"STORE-QRCODE","permissionName":"门店收款码"},{"pageSize":0,"currentPage":0,"permissionCode":"CASHER-MANAGE","permissionName":"收银员管理"},{"pageSize":0,"currentPage":0,"permissionCode":"MERCHANT-INFO","permissionName":"商户信息"},{"pageSize":0,"currentPage":0,"permissionCode":"","permissionName":""}]
     */

    private String logId;
    private boolean status;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pageSize : 0
         * currentPage : 0
         * permissionCode : PAY
         * permissionName : 收款
         */

        private int pageSize;
        private int currentPage;
        private String permissionCode;
        private String permissionName;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public String getPermissionCode() {
            return permissionCode;
        }

        public void setPermissionCode(String permissionCode) {
            this.permissionCode = permissionCode;
        }

        public String getPermissionName() {
            return permissionName;
        }

        public void setPermissionName(String permissionName) {
            this.permissionName = permissionName;
        }
    }
}
