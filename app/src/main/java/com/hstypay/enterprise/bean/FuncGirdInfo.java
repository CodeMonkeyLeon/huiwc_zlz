package com.hstypay.enterprise.bean;

/**
 * Created by admin on 2017/7/5.
 */

public class FuncGirdInfo {


    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getFuncSmallIconUrl() {
        return funcSmallIconUrl;
    }

    public void setFuncSmallIconUrl(String funcSmallIconUrl) {
        this.funcSmallIconUrl = funcSmallIconUrl;
    }

    private String funcName;//功能名称

    private String funcSmallIconUrl;//小图片
}
