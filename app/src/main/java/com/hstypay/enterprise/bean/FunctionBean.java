package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/21 17:37
 * @描述: ${TODO}
 */

public class FunctionBean {
    private int imageRes;
    private int function;
    private String funcName;

    public FunctionBean(){}

    public FunctionBean(int imageRes, int function, String funcName) {
        this.imageRes = imageRes;
        this.function = function;
        this.funcName = funcName;
    }

    public int getFunction() {
        return function;
    }

    public void setFunction(int function) {
        this.function = function;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }
}
