package com.hstypay.enterprise.bean;


public class FuxunBean {
    private String RespCode;//返回码
    private String RespDesc;//返回结果描述
    private String OptCode;//操作员号
    private String BatchNum;//批次号

    /**
     * 支付
     */
    private String trxId;//第三方交易流水
    private String TransType;//交易类型
    private String TransAmount;//交易金额
    private String TransDate;//交易日期
    private String TransTime;//交易时间
    private String CardNum;//卡号
    private String CertNum;//凭证号
    private String ReferCode;//参考号
    private String Reference;//备注
    private String FeeAmount;//小费金额
    private String TransIndexCode;//交易索引号
    private String AuthNo;//授权码
    private String BalanceAmount;//余额
    private String CustomerOID;//交易单号
    private int EnterMode;//支付方式：挥卡，插卡，扫码=3


    public int getEnterMode() {
        return EnterMode;
    }

    public void setEnterMode(int enterMode) {
        EnterMode = enterMode;
    }

    public String getCustomerOID() {
        return CustomerOID;
    }

    public void setCustomerOID(String customerOID) {
        CustomerOID = customerOID;
    }

    public String getRespCode() {
        return RespCode;
    }

    public void setRespCode(String respCode) {
        RespCode = respCode;
    }

    public String getRespDesc() {
        return RespDesc;
    }

    public void setRespDesc(String respDesc) {
        RespDesc = respDesc;
    }

    public String getOptCode() {
        return OptCode;
    }

    public void setOptCode(String optCode) {
        OptCode = optCode;
    }

    public String getBatchNum() {
        return BatchNum;
    }

    public void setBatchNum(String batchNum) {
        BatchNum = batchNum;
    }

    public String getTrxID() {
        return trxId;
    }

    public void setTrxID(String trxID) {
        trxId = trxID;
    }

    public String getTransType() {
        return TransType;
    }

    public void setTransType(String transType) {
        TransType = transType;
    }

    public String getTransAmount() {
        return TransAmount;
    }

    public void setTransAmount(String transAmount) {
        TransAmount = transAmount;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getCardNum() {
        return CardNum;
    }

    public void setCardNum(String cardNum) {
        CardNum = cardNum;
    }

    public String getCertNum() {
        return CertNum;
    }

    public void setCertNum(String certNum) {
        CertNum = certNum;
    }

    public String getReferCode() {
        return ReferCode;
    }

    public void setReferCode(String referCode) {
        ReferCode = referCode;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getFeeAmount() {
        return FeeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        FeeAmount = feeAmount;
    }

    public String getTransIndexCode() {
        return TransIndexCode;
    }

    public void setTransIndexCode(String transIndexCode) {
        TransIndexCode = transIndexCode;
    }

    public String getAuthNo() {
        return AuthNo;
    }

    public void setAuthNo(String authNo) {
        AuthNo = authNo;
    }

    public String getBalanceAmount() {
        return BalanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        BalanceAmount = balanceAmount;
    }
}
