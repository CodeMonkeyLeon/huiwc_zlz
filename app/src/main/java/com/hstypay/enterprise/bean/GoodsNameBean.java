package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/8 11:21
 * @描述: ${TODO}
 */

public class GoodsNameBean {
    /**
     * data : {"payGoodsName":"武汉热干面"}
     * logId : n5HL5mGk
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * payGoodsName : 武汉热干面
         */
        private String payGoodsName;
        private String merchantShortName;

        public String getMerchantShortName() {
            return merchantShortName;
        }

        public void setMerchantShortName(String merchantShortName) {
            this.merchantShortName = merchantShortName;
        }

        public void setPayGoodsName(String payGoodsName) {
            this.payGoodsName = payGoodsName;
        }

        public String getPayGoodsName() {
            return payGoodsName;
        }
    }
}
