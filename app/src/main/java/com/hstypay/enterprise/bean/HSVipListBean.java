package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/24 18:24
 * @描述: ${TODO}
 */

public class HSVipListBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : {"whiteListEnabled":false}
     * logId : TTtaDUrQ
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        private List<DataListEntity> dataList;

        public List<DataListEntity> getDataList() {
            return dataList;
        }

        public void setDataList(List<DataListEntity> dataList) {
            this.dataList = dataList;
        }

        public class DataListEntity {
            private String cardId;//会员id 会员卡号
            private String levelName;//会员等级名称
            private String mobile;//会员手机号
            private String headSculpture;//会员头像
            private String name;//会员名称

            public String getCardId() {
                return cardId;
            }

            public void setCardId(String cardId) {
                this.cardId = cardId;
            }

            public String getLevelName() {
                return levelName;
            }

            public void setLevelName(String levelName) {
                this.levelName = levelName;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getHeadSculpture() {
                return headSculpture;
            }

            public void setHeadSculpture(String headSculpture) {
                this.headSculpture = headSculpture;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
