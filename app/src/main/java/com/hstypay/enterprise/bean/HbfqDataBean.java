package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/27.
 */

public class HbfqDataBean implements Serializable {


    /**
     * logId : 4y4nd6g0
     * status : true
     * data : {"id":11,"title":"商户信息测试111","content":"<p>商户信息测试111<\/p>","readCount":0,"userId":1}
     */
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private String logId;
    private boolean status;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private long tradeMoney;
        private String tradeCount;

        public long getTradeMoney() {
            return tradeMoney;
        }

        public void setTradeMoney(long tradeMoney) {
            this.tradeMoney = tradeMoney;
        }

        public String getTradeCount() {
            return tradeCount;
        }

        public void setTradeCount(String tradeCount) {
            this.tradeCount = tradeCount;
        }
    }
}
