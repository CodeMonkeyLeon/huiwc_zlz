package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/8/4.
 */

public class HbfqRateBean implements Serializable {

    /**
     * logId : 8Z3qxGMI
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : [{"userId":3,"merchantId":"683200000001","storeMerchantId":"101300000001","merchantName":"支付接口联调测试数据_门店","pushClose":1},{"userId":3,"merchantId":"683200000001","storeMerchantId":"501300000001","merchantName":"火影忍者之雏田vs小樱","pushClose":1}]
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public class DataBean implements Serializable {
        private String hbfqNum;
        private String rate;
        private String hbfqName;
        private String perAmount;
        private String perFee;
        private boolean freeFee;//是否免息
        private boolean unionFeeFlag;//是否支持联合付息模式
        private long unionMchFee;//联合付息商户费率金额，单位(分)
        private long unionUserFee;//联合付息用户费率金额，单位(分)
        private Number totalFee;//总手续费

        public String getHbfqNum() {
            return hbfqNum;
        }

        public void setHbfqNum(String hbfqNum) {
            this.hbfqNum = hbfqNum;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getHbfqName() {
            return hbfqName;
        }

        public void setHbfqName(String hbfqName) {
            this.hbfqName = hbfqName;
        }

        public String getPerAmount() {
            return perAmount;
        }

        public void setPerAmount(String perAmount) {
            this.perAmount = perAmount;
        }

        public String getPerFee() {
            return perFee;
        }

        public void setPerFee(String perFee) {
            this.perFee = perFee;
        }

        public boolean isFreeFee() {
            return freeFee;
        }

        public void setFreeFee(boolean freeFee) {
            this.freeFee = freeFee;
        }

        public boolean getUnionFeeFlag() {
            return unionFeeFlag;
        }

        public void setUnionFeeFlag(boolean unionFeeFlag) {
            this.unionFeeFlag = unionFeeFlag;
        }

        public long getUnionMchFee() {
            return unionMchFee;
        }

        public void setUnionMchFee(long unionMchFee) {
            this.unionMchFee = unionMchFee;
        }

        public long getUnionUserFee() {
            return unionUserFee;
        }

        public void setUnionUserFee(long unionUserFee) {
            this.unionUserFee = unionUserFee;
        }

        public Number getTotalFee() {
            return totalFee;
        }

        public void setTotalFee(Number totalFee) {
            this.totalFee = totalFee;
        }
    }
}
