package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/10/17 16:13
 * @描述: ${TODO}
 */

public class HtmlMTABean {
    /**
     * id : 1234
     * props : [{"value":"on","key":"switch"},{"value":"￥100.00","key":"money"}]
     */
    private String id;
    private List<PropsEntity> props;

    public void setId(String id) {
        this.id = id;
    }

    public void setProps(List<PropsEntity> props) {
        this.props = props;
    }

    public String getId() {
        return id;
    }

    public List<PropsEntity> getProps() {
        return props;
    }

    public class PropsEntity {
        /**
         * value : on
         * key : switch
         */
        private String value;
        private String key;

        public void setValue(String value) {
            this.value = value;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public String getKey() {
            return key;
        }
    }
}
