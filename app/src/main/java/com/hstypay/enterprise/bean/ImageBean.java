package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/12 19:16
 * @描述: ${TODO}
 */

public class ImageBean {
    /**
     * logId : WMvlCSJ5
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"realName":"一路发加","stores":[{"storeId":"101300000001","storeName":"支付接口联调测试数据_门店"}],"opCode":"APPLY-REFUND","userId":121}
     */

    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"businessLicense":{"enddate":"长期","regaddress":"东莞市莞城金牛路八达花园B2栋311","scope":"批发、零售：机电产品、自动化设备、五金交电、建筑材料、电子产品、阀门、仪器。","name":"东莞市海久机电有限公司","regdate":"","legalperson":"李久睛","creditno":"91441900094615080Q","regorgan":"广东省东莞市工商行政管理局"},"licenseImg":"/pic/mch/2017/12/12/f1460127-29f4-4254-b719-5815b94c8b1e.jpg"}
     * logId : zQawBtnm
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * businessLicense : {"enddate":"长期","regaddress":"东莞市莞城金牛路八达花园B2栋311","scope":"批发、零售：机电产品、自动化设备、五金交电、建筑材料、电子产品、阀门、仪器。","name":"东莞市海久机电有限公司","regdate":"","legalperson":"李久睛","creditno":"91441900094615080Q","regorgan":"广东省东莞市工商行政管理局"}
         * licenseImg : /pic/mch/2017/12/12/f1460127-29f4-4254-b719-5815b94c8b1e.jpg
         */
        private BusinessLicenseEntity businessLicense;
        private IdCardEntity idCard;
        private BankCardEntity bankCard;
        private String licenseImg;//营业执照
        private String identityPerImg;//身份证正面
        private String identityDevImg;//身份证反面
        private String storeImg;//门头照
        private String handIcardImg;//手持身份证照
        private String bankCardImg;//银行卡正面照
        private String permissionImg;//开户许可证

        public BankCardEntity getBankCard() {
            return bankCard;
        }

        public void setBankCard(BankCardEntity bankCard) {
            this.bankCard = bankCard;
        }

        public String getPermissionImg() {
            return permissionImg;
        }

        public void setPermissionImg(String permissionImg) {
            this.permissionImg = permissionImg;
        }

        public IdCardEntity getIdCard() {
            return idCard;
        }

        public void setIdCard(IdCardEntity idCard) {
            this.idCard = idCard;
        }

        public String getIdentityPerImg() {
            return identityPerImg;
        }

        public void setIdentityPerImg(String identityPerImg) {
            this.identityPerImg = identityPerImg;
        }

        public String getIdentityDevImg() {
            return identityDevImg;
        }

        public void setIdentityDevImg(String identityDevImg) {
            this.identityDevImg = identityDevImg;
        }

        public String getStoreImg() {
            return storeImg;
        }

        public void setStoreImg(String storeImg) {
            this.storeImg = storeImg;
        }

        public String getHandIcardImg() {
            return handIcardImg;
        }

        public void setHandIcardImg(String handIcardImg) {
            this.handIcardImg = handIcardImg;
        }

        public String getBankCardImg() {
            return bankCardImg;
        }

        public void setBankCardImg(String bankCardImg) {
            this.bankCardImg = bankCardImg;
        }

        public void setBusinessLicense(BusinessLicenseEntity businessLicense) {
            this.businessLicense = businessLicense;
        }

        public void setLicenseImg(String licenseImg) {
            this.licenseImg = licenseImg;
        }

        public BusinessLicenseEntity getBusinessLicense() {
            return businessLicense;
        }

        public String getLicenseImg() {
            return licenseImg;
        }

        public class IdCardEntity {
            private String address;
            private String birth;
            private String name;
            private String nation;
            private String number;
            private String sex;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getBirth() {
                return birth;
            }

            public void setBirth(String birth) {
                this.birth = birth;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getNation() {
                return nation;
            }

            public void setNation(String nation) {
                this.nation = nation;
            }

            public String getNumber() {
                return number;
            }

            public void setNumber(String number) {
                this.number = number;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }
        }

        public class BankCardEntity {
            private String bankname;
            private String bankno;
            private String number;
            private String type;
            private String bankCardImg;

            public String getBankname() {
                return bankname;
            }

            public void setBankname(String bankname) {
                this.bankname = bankname;
            }

            public String getBankno() {
                return bankno;
            }

            public void setBankno(String bankno) {
                this.bankno = bankno;
            }

            public String getNumber() {
                return number;
            }

            public void setNumber(String number) {
                this.number = number;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getBankCardImg() {
                return bankCardImg;
            }

            public void setBankCardImg(String bankCardImg) {
                this.bankCardImg = bankCardImg;
            }
        }

        public class BusinessLicenseEntity {
            /**
             * enddate : 长期
             * regaddress : 东莞市莞城金牛路八达花园B2栋311
             * scope : 批发、零售：机电产品、自动化设备、五金交电、建筑材料、电子产品、阀门、仪器。
             * name : 东莞市海久机电有限公司
             * regdate :
             * legalperson : 李久睛
             * creditno : 91441900094615080Q
             * regorgan : 广东省东莞市工商行政管理局
             */
            private String enddate;
            private String regaddress;
            private String scope;
            private String name;
            private String regdate;
            private String legalperson;
            private String creditno;
            private String regorgan;
            /**
             * 注册资本
             */
            private String regcapital;

            public String getRegcapital() {
                return regcapital;
            }

            public void setRegcapital(String regcapital) {
                this.regcapital = regcapital;
            }
            public void setEnddate(String enddate) {
                this.enddate = enddate;
            }

            public void setRegaddress(String regaddress) {
                this.regaddress = regaddress;
            }

            public void setScope(String scope) {
                this.scope = scope;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setRegdate(String regdate) {
                this.regdate = regdate;
            }

            public void setLegalperson(String legalperson) {
                this.legalperson = legalperson;
            }

            public void setCreditno(String creditno) {
                this.creditno = creditno;
            }

            public void setRegorgan(String regorgan) {
                this.regorgan = regorgan;
            }

            public String getEnddate() {
                return enddate;
            }

            public String getRegaddress() {
                return regaddress;
            }

            public String getScope() {
                return scope;
            }

            public String getName() {
                return name;
            }

            public String getRegdate() {
                return regdate;
            }

            public String getLegalperson() {
                return legalperson;
            }

            public String getCreditno() {
                return creditno;
            }

            public String getRegorgan() {
                return regorgan;
            }
        }
    }
}
