package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * Created by admin on 2017/8/3.
 */

public class Infos implements Serializable{

    private String realName;
    private int userId;

    public boolean isSelcet() {
        return isSelcet;
    }

    public void setSelcet(boolean selcet) {
        isSelcet = selcet;
    }

    private boolean isSelcet;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
