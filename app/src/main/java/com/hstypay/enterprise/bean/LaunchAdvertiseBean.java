package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/10/13 11:54
 * @描述: ${TODO}
 */

public class LaunchAdvertiseBean {
    /**
     * data : [{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024045","strTradeStatus":"未支付","createTime":"2017-07-26 20:51:57","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024044","strTradeStatus":"未支付","createTime":"2017-07-26 20:47:11","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024043","strTradeStatus":"转入退款","createTime":"2017-07-26 20:13:42","tradeStatus":4,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024042","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:52","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":50,"apiProvider":3,"orderNo":"20170726868320000000100000024041","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:39","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024040","strTradeStatus":"未支付","createTime":"2017-07-26 19:40:47","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024039","strTradeStatus":"未支付","createTime":"2017-07-26 19:17:09","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024038","strTradeStatus":"未支付","createTime":"2017-07-26 19:08:14","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024037","strTradeStatus":"支付成功","createTime":"2017-07-26 18:21:37","tradeStatus":2,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024036","strTradeStatus":"未支付","createTime":"2017-07-26 18:19:00","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024035","strTradeStatus":"未支付","createTime":"2017-07-26 18:17:36","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024034","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:44","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024033","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:06","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024032","strTradeStatus":"未支付","createTime":"2017-07-26 18:15:37","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024031","strTradeStatus":"未支付","createTime":"2017-07-26 18:14:46","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"}]
     * logId : mxfMM2GB
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : {"advCode":"63640492","advUrl":"http://www.baidu.com","createUserName":"超级管理员","updateTime":"2017-10-11 10:39:23","remark":"","title":"APP启动页10","statusCnt":"投放中","createTime":"2017-09-30 09:35:58","createUser":1,"location":"3","id":3,"advImage":"d3e07e3e-3c19-454d-ad7a-75f875fe2404.jpg","locationCnt":"app启动页","status":1}
     * logId : f90RoKtI
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * advCode : 63640492
         * advUrl : http://www.baidu.com
         * createUserName : 超级管理员
         * updateTime : 2017-10-11 10:39:23
         * remark :
         * title : APP启动页10
         * statusCnt : 投放中
         * createTime : 2017-09-30 09:35:58
         * createUser : 1
         * location : 3
         * id : 3
         * advImage : d3e07e3e-3c19-454d-ad7a-75f875fe2404.jpg
         * locationCnt : app启动页
         * status : 1
         */
        private String advCode;
        private String advUrl;
        private String createUserName;
        private String updateTime;
        private String remark;
        private String title;
        private String statusCnt;
        private String createTime;
        private int createUser;
        private String location;
        private int id;
        private String advImage;
        private String locationCnt;
        private int status;

        public void setAdvCode(String advCode) {
            this.advCode = advCode;
        }

        public void setAdvUrl(String advUrl) {
            this.advUrl = advUrl;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setAdvImage(String advImage) {
            this.advImage = advImage;
        }

        public void setLocationCnt(String locationCnt) {
            this.locationCnt = locationCnt;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getAdvCode() {
            return advCode;
        }

        public String getAdvUrl() {
            return advUrl;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public String getRemark() {
            return remark;
        }

        public String getTitle() {
            return title;
        }

        public String getStatusCnt() {
            return statusCnt;
        }

        public String getCreateTime() {
            return createTime;
        }

        public int getCreateUser() {
            return createUser;
        }

        public String getLocation() {
            return location;
        }

        public int getId() {
            return id;
        }

        public String getAdvImage() {
            return advImage;
        }

        public String getLocationCnt() {
            return locationCnt;
        }

        public int getStatus() {
            return status;
        }
    }
}
