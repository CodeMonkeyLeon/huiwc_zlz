package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/12 20:17
 * @描述: ${TODO}
 */

public class LicenseDetailBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"merchantShortName":"活力","licensePhoto":"/pic/mch/2017/12/12/53a52698-528a-4214-8c5d-e162930badbc.jpg","examineStatusCnt":"待审核","licenseCode":"914403007488775152","examineStatus":0,"merchantName":"深圳市中南活力实业股份有限公司"}
     * logId : VeVBFblo
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity  implements Serializable {
        /**
         * merchantShortName : 活力
         * licensePhoto : /pic/mch/2017/12/12/53a52698-528a-4214-8c5d-e162930badbc.jpg
         * examineStatusCnt : 待审核
         * licenseCode : 914403007488775152
         * examineStatus : 0
         * merchantName : 深圳市中南活力实业股份有限公司
         */
        private String merchantShortName;
        private String licensePhoto;
        private String examineStatusCnt;
        private String licenseCode;
        private int examineStatus;
        private String merchantName;
        private String examineRemark;

        public String getExamineRemark() {
            return examineRemark;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setMerchantShortName(String merchantShortName) {
            this.merchantShortName = merchantShortName;
        }

        public void setLicensePhoto(String licensePhoto) {
            this.licensePhoto = licensePhoto;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public void setLicenseCode(String licenseCode) {
            this.licenseCode = licenseCode;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getMerchantShortName() {
            return merchantShortName;
        }

        public String getLicensePhoto() {
            return licensePhoto;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public String getLicenseCode() {
            return licenseCode;
        }

        public int getExamineStatus() {
            return examineStatus;
        }

        public String getMerchantName() {
            return merchantName;
        }
    }
}
