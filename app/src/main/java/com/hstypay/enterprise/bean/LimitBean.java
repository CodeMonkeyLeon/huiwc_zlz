package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/12 21:07
 * @描述: ${TODO}
 */

public class LimitBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"singleMaxQuota":150000,"dayMaxSum":60,"monthMaxQuota":20000000,"dayMaxQuota":1200000}
     * logId : iedxKvIr
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * singleMaxQuota : 150000
         * dayMaxSum : 60
         * monthMaxQuota : 20000000
         * dayMaxQuota : 1200000
         */
        private long singleMaxQuota;
        private long dayMaxSum;
        private long monthMaxQuota;
        private long dayMaxQuota;

        public void setSingleMaxQuota(long singleMaxQuota) {
            this.singleMaxQuota = singleMaxQuota;
        }

        public void setDayMaxSum(long dayMaxSum) {
            this.dayMaxSum = dayMaxSum;
        }

        public void setMonthMaxQuota(long monthMaxQuota) {
            this.monthMaxQuota = monthMaxQuota;
        }

        public void setDayMaxQuota(long dayMaxQuota) {
            this.dayMaxQuota = dayMaxQuota;
        }

        public long getSingleMaxQuota() {
            return singleMaxQuota;
        }

        public long getDayMaxSum() {
            return dayMaxSum;
        }

        public long getMonthMaxQuota() {
            return monthMaxQuota;
        }

        public long getDayMaxQuota() {
            return dayMaxQuota;
        }
    }
}
