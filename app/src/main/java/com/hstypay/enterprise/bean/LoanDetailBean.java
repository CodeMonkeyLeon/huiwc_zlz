package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/24 17:02
 * @描述: ${TODO}
 */

public class LoanDetailBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"loanNo":"20171224162526748200000003","signTime":"2017-12-24 16:25:26","createTime":"2017-12-24 16:25:26","merchantId":"748200000003","loanBorrowStatusCnt":"申请中","loanBorrowStatus":1,"loanStatus":"100","updateTime":"2017-12-24 16:28:00","id":30,"monthComplexRate":"0.0","loanPeriod":"3","creditBalance":2000000}
     * logId : xBUO1dMB
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * loanNo : 20171224162526748200000003
         * signTime : 2017-12-24 16:25:26
         * createTime : 2017-12-24 16:25:26
         * merchantId : 748200000003
         * loanBorrowStatusCnt : 申请中
         * loanBorrowStatus : 1
         * loanStatus : 100
         * updateTime : 2017-12-24 16:28:00
         * id : 30
         * monthComplexRate : 0.0
         * loanPeriod : 3
         * creditBalance : 2000000
         */
        private String loanNo;
        private String signTime;
        private String createTime;
        private String merchantId;
        private String loanBorrowStatusCnt;
        private int loanBorrowStatus;
        private String loanStatus;
        private String updateTime;
        private int id;
        private String monthComplexRate;
        private String loanPeriod;
        private int creditBalance;

        public void setLoanNo(String loanNo) {
            this.loanNo = loanNo;
        }

        public void setSignTime(String signTime) {
            this.signTime = signTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public void setLoanBorrowStatusCnt(String loanBorrowStatusCnt) {
            this.loanBorrowStatusCnt = loanBorrowStatusCnt;
        }

        public void setLoanBorrowStatus(int loanBorrowStatus) {
            this.loanBorrowStatus = loanBorrowStatus;
        }

        public void setLoanStatus(String loanStatus) {
            this.loanStatus = loanStatus;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setMonthComplexRate(String monthComplexRate) {
            this.monthComplexRate = monthComplexRate;
        }

        public void setLoanPeriod(String loanPeriod) {
            this.loanPeriod = loanPeriod;
        }

        public void setCreditBalance(int creditBalance) {
            this.creditBalance = creditBalance;
        }

        public String getLoanNo() {
            return loanNo;
        }

        public String getSignTime() {
            return signTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public String getLoanBorrowStatusCnt() {
            return loanBorrowStatusCnt;
        }

        public int getLoanBorrowStatus() {
            return loanBorrowStatus;
        }

        public String getLoanStatus() {
            return loanStatus;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public int getId() {
            return id;
        }

        public String getMonthComplexRate() {
            return monthComplexRate;
        }

        public String getLoanPeriod() {
            return loanPeriod;
        }

        public int getCreditBalance() {
            return creditBalance;
        }
    }
}
