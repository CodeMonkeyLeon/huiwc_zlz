package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/25.
 */

public class LoginBean implements Serializable {


    /**
     * logId : CkmS6g4Z
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"user":{"userId":3,"userName":"18666076073","userType":3,"orgId":"683200000001","merchant":{"merchantId":"683200000001","merchantName":"支付接口联调测试数据_商户","examineStatus":1,"examineStatusCnt":"审核通过"},"merchantFlag":true,"adminFlag":false,"casherFlag":false,"roles":[{"createUser":1,"createUserName":"superadmin","createTime":"2017-07-22 15:26:58","updateTime":"2017-07-22 15:26:58","roleId":15,"appId":3,"serviceProvider":"","roleName":"超级管理员","roleCode":"SUPERADMIN","enable":true,"deleteEnable":false,"remark":"商户平台超级管理员"}]},"ops":{"userOps":[{"createTime":"2017-07-29 15:27:53","id":1,"userId":3,"merchantId":"683200000001","opCode":"APPLY-REFUND"}],"merchantOps":[]},"funcs":[{"funcId":90,"appId":3,"funcCode":"APP-OPERATION","funcName":"终端相关","parentFunc":0,"ordered":0,"menuFlag":false,"remark":"终端登录","permissions":[{"funcId":90,"permissionCode":"APPLOGIN","permissionName":"登录"},{"funcId":90,"permissionCode":"APPLY-REFUND","permissionName":"终端退款权限"}]}],"signKey":"fd9bb2c05a833c6b6465c3db8913b755"}
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean {
        /**
         * user : {"userId":3,"userName":"18666076073","userType":3,"orgId":"683200000001","merchant":{"merchantId":"683200000001","merchantName":"支付接口联调测试数据_商户","examineStatus":1,"examineStatusCnt":"审核通过"},"merchantFlag":true,"adminFlag":false,"casherFlag":false,"roles":[{"createUser":1,"createUserName":"superadmin","createTime":"2017-07-22 15:26:58","updateTime":"2017-07-22 15:26:58","roleId":15,"appId":3,"serviceProvider":"","roleName":"超级管理员","roleCode":"SUPERADMIN","enable":true,"deleteEnable":false,"remark":"商户平台超级管理员"}]}
         * ops : {"userOps":[{"createTime":"2017-07-29 15:27:53","id":1,"userId":3,"merchantId":"683200000001","opCode":"APPLY-REFUND"}],"merchantOps":[]}
         * funcs : [{"funcId":90,"appId":3,"funcCode":"APP-OPERATION","funcName":"终端相关","parentFunc":0,"ordered":0,"menuFlag":false,"remark":"终端登录","permissions":[{"funcId":90,"permissionCode":"APPLOGIN","permissionName":"登录"},{"funcId":90,"permissionCode":"APPLY-REFUND","permissionName":"终端退款权限"}]}]
         * signKey : fd9bb2c05a833c6b6465c3db8913b755
         */

        private UserBean user;
        private OpsBean ops;
        private String signKey;
        private List<FuncsBean> funcs;

        private boolean showWSY;
        private boolean mchRegisterFlag;//新老商户注册标识 true:2.0商户
        private boolean standardIndexFlag;//登录所在平台标识 true:2.0
        private boolean standardFlag;//商户白名单
        private boolean belonedHSHFFlag;//商户主体是否属于汇商合肥

        public boolean isBelonedHSHFFlag() {
            return belonedHSHFFlag;
        }

        public void setBelonedHSHFFlag(boolean belonedHSHFFlag) {
            this.belonedHSHFFlag = belonedHSHFFlag;
        }

        public boolean isStandardFlag() {
            return standardFlag;
        }

        public void setStandardFlag(boolean standardFlag) {
            this.standardFlag = standardFlag;
        }

        public boolean isMchRegisterFlag() {
            return mchRegisterFlag;
        }

        public void setMchRegisterFlag(boolean mchRegisterFlag) {
            this.mchRegisterFlag = mchRegisterFlag;
        }

        public boolean isStandardIndexFlag() {
            return standardIndexFlag;
        }

        public void setStandardIndexFlag(boolean standardIndexFlag) {
            this.standardIndexFlag = standardIndexFlag;
        }

        public boolean isShowWSY() {
            return showWSY;
        }

        public void setShowWSY(boolean showWSY) {
            this.showWSY = showWSY;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public OpsBean getOps() {
            return ops;
        }

        public void setOps(OpsBean ops) {
            this.ops = ops;
        }

        public String getSignKey() {
            return signKey;
        }

        public void setSignKey(String signKey) {
            this.signKey = signKey;
        }

        public List<FuncsBean> getFuncs() {
            return funcs;
        }

        public void setFuncs(List<FuncsBean> funcs) {
            this.funcs = funcs;
        }

        public static class UserBean {
            /**
             * userId : 3
             * userName : 18666076073
             * userType : 3
             * orgId : 683200000001
             * merchant : {"merchantId":"683200000001","merchantName":"支付接口联调测试数据_商户","examineStatus":1,"examineStatusCnt":"审核通过"}
             * merchantFlag : true
             * adminFlag : false
             * casherFlag : false
             * roles : [{"createUser":1,"createUserName":"superadmin","createTime":"2017-07-22 15:26:58","updateTime":"2017-07-22 15:26:58","roleId":15,"appId":3,"serviceProvider":"","roleName":"超级管理员","roleCode":"SUPERADMIN","enable":true,"deleteEnable":false,"remark":"商户平台超级管理员"}]
             */
            /**
             * attestationStatus
             * 0、未认证 资料未补件
             * 1、已认证 资料已补件，审核通过，修改待审核，修改待审核不通过，可交易或部分可交易
             * 2、认证中 资料已补件，待审核，审核通过，不可交易
             * 3、未认证 资料已补件，审核不通过，不可交易（有失败原因）
             */

            private int userId;
            private String userName;
            private String loginName;
            private int userType;
            private String orgId;
            private MerchantBean merchant;
            private EmployeeBean emp;
            private boolean merchantFlag;
            private boolean adminFlag;
            private boolean casherFlag;
            private boolean storeManagerFlag;
            private List<RolesBean> roles;
            private String realName;
            private String loginPhone;
            private boolean updatePwdFlag;
            private boolean showWSY;
            private int wxRegStatus;
            private int aliRegStatus;
            private int attestationStatus;

            public int getAliRegStatus() {
                return aliRegStatus;
            }

            public void setAliRegStatus(int aliRegStatus) {
                this.aliRegStatus = aliRegStatus;
            }

            public int getAttestationStatus() {
                return attestationStatus;
            }

            public void setAttestationStatus(int attestationStatus) {
                this.attestationStatus = attestationStatus;
            }

            public boolean isShowWSY() {
                return showWSY;
            }

            public void setShowWSY(boolean showWSY) {
                this.showWSY = showWSY;
            }

            public int getWxRegStatus() {
                return wxRegStatus;
            }

            public void setWxRegStatus(int wxRegStatus) {
                this.wxRegStatus = wxRegStatus;
            }

            public EmployeeBean getEmp() {
                return emp;
            }

            public void setEmp(EmployeeBean emp) {
                this.emp = emp;
            }

            public String getLoginName() {
                return loginName;
            }

            public void setLoginName(String loginName) {
                this.loginName = loginName;
            }

            public boolean isStoreManagerFlag() {
                return storeManagerFlag;
            }

            public void setStoreManagerFlag(boolean storeManagerFlag) {
                this.storeManagerFlag = storeManagerFlag;
            }

            public String getLoginPhone() {
                return loginPhone;
            }

            public void setLoginPhone(String loginPhone) {
                this.loginPhone = loginPhone;
            }

            public boolean isUpdatePwdFlag() {
                return updatePwdFlag;
            }

            public void setUpdatePwdFlag(boolean updatePwdFlag) {
                this.updatePwdFlag = updatePwdFlag;
            }

            public String getRealName() {
                return realName;
            }

            public void setRealName(String realName) {
                this.realName = realName;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public int getUserType() {
                return userType;
            }

            public void setUserType(int userType) {
                this.userType = userType;
            }

            public String getOrgId() {
                return orgId;
            }

            public void setOrgId(String orgId) {
                this.orgId = orgId;
            }

            public MerchantBean getMerchant() {
                return merchant;
            }

            public void setMerchant(MerchantBean merchant) {
                this.merchant = merchant;
            }

            public boolean isMerchantFlag() {
                return merchantFlag;
            }

            public void setMerchantFlag(boolean merchantFlag) {
                this.merchantFlag = merchantFlag;
            }

            public boolean isAdminFlag() {
                return adminFlag;
            }

            public void setAdminFlag(boolean adminFlag) {
                this.adminFlag = adminFlag;
            }

            public boolean isCasherFlag() {
                return casherFlag;
            }

            public void setCasherFlag(boolean casherFlag) {
                this.casherFlag = casherFlag;
            }

            public List<RolesBean> getRoles() {
                return roles;
            }

            public void setRoles(List<RolesBean> roles) {
                this.roles = roles;
            }

            public static class EmployeeBean {
                private String empId;

                public String getEmpId() {
                    return empId;
                }

                public void setEmpId(String empId) {
                    this.empId = empId;
                }
            }

            public static class MerchantBean {
                /**
                 * merchantId : 683200000001
                 * merchantName : 支付接口联调测试数据_商户
                 * examineStatus : 1
                 * examineStatusCnt : 审核通过
                 */

                private String merchantId;
                private String merchantName;
                private int examineStatus;
                private String examineStatusCnt;
                private String payGoodsName;
                private String rewardFlage;
                private String merchantType;
                private String merchantTypeCnt;
                private int preauthStatus;

                public int getPreauthStatus() {
                    return preauthStatus;
                }

                public void setPreauthStatus(int preauthStatus) {
                    this.preauthStatus = preauthStatus;
                }

                public String getMerchantType() {
                    return merchantType;
                }

                public void setMerchantType(String merchantType) {
                    this.merchantType = merchantType;
                }

                public String getMerchantTypeCnt() {
                    return merchantTypeCnt;
                }

                public void setMerchantTypeCnt(String merchantTypeCnt) {
                    this.merchantTypeCnt = merchantTypeCnt;
                }

                public String getRewardFlage() {
                    return rewardFlage;
                }

                public void setRewardFlage(String rewardFlage) {
                    this.rewardFlage = rewardFlage;
                }

                public String getPayGoodsName() {
                    return payGoodsName;
                }

                public void setPayGoodsName(String payGoodsName) {
                    this.payGoodsName = payGoodsName;
                }

                public String getMerchantId() {
                    return merchantId;
                }

                public void setMerchantId(String merchantId) {
                    this.merchantId = merchantId;
                }

                public String getMerchantName() {
                    return merchantName;
                }

                public void setMerchantName(String merchantName) {
                    this.merchantName = merchantName;
                }

                public int getExamineStatus() {
                    return examineStatus;
                }

                public void setExamineStatus(int examineStatus) {
                    this.examineStatus = examineStatus;
                }

                public String getExamineStatusCnt() {
                    return examineStatusCnt;
                }

                public void setExamineStatusCnt(String examineStatusCnt) {
                    this.examineStatusCnt = examineStatusCnt;
                }
            }

            public static class RolesBean {
                /**
                 * createUser : 1
                 * createUserName : superadmin
                 * createTime : 2017-07-22 15:26:58
                 * updateTime : 2017-07-22 15:26:58
                 * roleId : 15
                 * appId : 3
                 * serviceProvider :
                 * roleName : 超级管理员
                 * roleCode : SUPERADMIN
                 * enable : true
                 * deleteEnable : false
                 * remark : 商户平台超级管理员
                 */

                private int createUser;
                private String createUserName;
                private String createTime;
                private String updateTime;
                private int roleId;
                private int appId;
                private String serviceProvider;
                private String roleName;
                private String roleCode;
                private boolean enable;
                private boolean deleteEnable;
                private String remark;

                public int getCreateUser() {
                    return createUser;
                }

                public void setCreateUser(int createUser) {
                    this.createUser = createUser;
                }

                public String getCreateUserName() {
                    return createUserName;
                }

                public void setCreateUserName(String createUserName) {
                    this.createUserName = createUserName;
                }

                public String getCreateTime() {
                    return createTime;
                }

                public void setCreateTime(String createTime) {
                    this.createTime = createTime;
                }

                public String getUpdateTime() {
                    return updateTime;
                }

                public void setUpdateTime(String updateTime) {
                    this.updateTime = updateTime;
                }

                public int getRoleId() {
                    return roleId;
                }

                public void setRoleId(int roleId) {
                    this.roleId = roleId;
                }

                public int getAppId() {
                    return appId;
                }

                public void setAppId(int appId) {
                    this.appId = appId;
                }

                public String getServiceProvider() {
                    return serviceProvider;
                }

                public void setServiceProvider(String serviceProvider) {
                    this.serviceProvider = serviceProvider;
                }

                public String getRoleName() {
                    return roleName;
                }

                public void setRoleName(String roleName) {
                    this.roleName = roleName;
                }

                public String getRoleCode() {
                    return roleCode;
                }

                public void setRoleCode(String roleCode) {
                    this.roleCode = roleCode;
                }

                public boolean isEnable() {
                    return enable;
                }

                public void setEnable(boolean enable) {
                    this.enable = enable;
                }

                public boolean isDeleteEnable() {
                    return deleteEnable;
                }

                public void setDeleteEnable(boolean deleteEnable) {
                    this.deleteEnable = deleteEnable;
                }

                public String getRemark() {
                    return remark;
                }

                public void setRemark(String remark) {
                    this.remark = remark;
                }
            }
        }

        public static class OpsBean {
            private List<UserOpsBean> userOps;
            private List<?> merchantOps;

            public List<UserOpsBean> getUserOps() {
                return userOps;
            }

            public void setUserOps(List<UserOpsBean> userOps) {
                this.userOps = userOps;
            }

            public List<?> getMerchantOps() {
                return merchantOps;
            }

            public void setMerchantOps(List<?> merchantOps) {
                this.merchantOps = merchantOps;
            }

            public static class UserOpsBean {
                /**
                 * createTime : 2017-07-29 15:27:53
                 * id : 1
                 * userId : 3
                 * merchantId : 683200000001
                 * opCode : APPLY-REFUND
                 */

                private String createTime;
                private int id;
                private int userId;
                private String merchantId;
                private String opCode;

                public String getCreateTime() {
                    return createTime;
                }

                public void setCreateTime(String createTime) {
                    this.createTime = createTime;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getMerchantId() {
                    return merchantId;
                }

                public void setMerchantId(String merchantId) {
                    this.merchantId = merchantId;
                }

                public String getOpCode() {
                    return opCode;
                }

                public void setOpCode(String opCode) {
                    this.opCode = opCode;
                }
            }
        }

        public static class FuncsBean {
            /**
             * funcId : 90
             * appId : 3
             * funcCode : APP-OPERATION
             * funcName : 终端相关
             * parentFunc : 0
             * ordered : 0
             * menuFlag : false
             * remark : 终端登录
             * permissions : [{"funcId":90,"permissionCode":"APPLOGIN","permissionName":"登录"},{"funcId":90,"permissionCode":"APPLY-REFUND","permissionName":"终端退款权限"}]
             */

            private int funcId;
            private int appId;
            private String funcCode;
            private String funcName;
            private int parentFunc;
            private int ordered;
            private boolean menuFlag;
            private String remark;
            private List<PermissionsBean> permissions;

            public int getFuncId() {
                return funcId;
            }

            public void setFuncId(int funcId) {
                this.funcId = funcId;
            }

            public int getAppId() {
                return appId;
            }

            public void setAppId(int appId) {
                this.appId = appId;
            }

            public String getFuncCode() {
                return funcCode;
            }

            public void setFuncCode(String funcCode) {
                this.funcCode = funcCode;
            }

            public String getFuncName() {
                return funcName;
            }

            public void setFuncName(String funcName) {
                this.funcName = funcName;
            }

            public int getParentFunc() {
                return parentFunc;
            }

            public void setParentFunc(int parentFunc) {
                this.parentFunc = parentFunc;
            }

            public int getOrdered() {
                return ordered;
            }

            public void setOrdered(int ordered) {
                this.ordered = ordered;
            }

            public boolean isMenuFlag() {
                return menuFlag;
            }

            public void setMenuFlag(boolean menuFlag) {
                this.menuFlag = menuFlag;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public List<PermissionsBean> getPermissions() {
                return permissions;
            }

            public void setPermissions(List<PermissionsBean> permissions) {
                this.permissions = permissions;
            }

            public static class PermissionsBean {
                /**
                 * funcId : 90
                 * permissionCode : APPLOGIN
                 * permissionName : 登录
                 */

                private int funcId;
                private String permissionCode;
                private String permissionName;

                public int getFuncId() {
                    return funcId;
                }

                public void setFuncId(int funcId) {
                    this.funcId = funcId;
                }

                public String getPermissionCode() {
                    return permissionCode;
                }

                public void setPermissionCode(String permissionCode) {
                    this.permissionCode = permissionCode;
                }

                public String getPermissionName() {
                    return permissionName;
                }

                public void setPermissionName(String permissionName) {
                    this.permissionName = permissionName;
                }
            }
        }
    }
}
