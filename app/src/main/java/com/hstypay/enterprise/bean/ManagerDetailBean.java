package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/14 20:08
 * @描述: ${TODO}
 */

public class ManagerDetailBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"idCardFrontPhoto":"/pic/mch/2017/12/13/4dc74fbd-c98f-4b01-b583-25d141a79834.jpg","realName":"方大呆2号","idCardBackPhoto":"/pic/mch/2017/12/13/a2997d28-e773-4337-8e42-07c627f7fa78.jpg","idCode":"371002200610020000","stores":[{"storeName":"钱多多","storeId":"694300000001"}],"telphone":"18718578888","userId":162,"enabled":true}
     * logId : xVFe63nc
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable {
        /**
         * idCardFrontPhoto : /pic/mch/2017/12/13/4dc74fbd-c98f-4b01-b583-25d141a79834.jpg
         * realName : 方大呆2号
         * idCardBackPhoto : /pic/mch/2017/12/13/a2997d28-e773-4337-8e42-07c627f7fa78.jpg
         * idCode : 371002200610020000
         * stores : [{"storeName":"钱多多","storeId":"694300000001"}]
         * telphone : 18718578888
         * userId : 162
         * enabled : true
         */
        private String idCardFrontPhoto;
        private String realName;
        private String idCardBackPhoto;
        private String idCode;
        private List<StoresBean> stores;
        private String telphone;
        private String userId;
        private boolean enabled;
        private int editEnable;
        private boolean applyRefund;
        /**
         * 角色退款权限，true 打开 false 关闭
         */
        private boolean roleApplyRefund;
        /**
         * 是否需要重发邀请短信
         */
        private boolean needResendSmsFlag;
        /**
         * 激活状态,1:已激活 2:未激活
         */
        private int activateStatus;

        public int getActivateStatus() {
            return activateStatus;
        }

        public void setActivateStatus(int activateStatus) {
            this.activateStatus = activateStatus;
        }

        public boolean isRoleApplyRefund() {
            return roleApplyRefund;
        }

        public void setRoleApplyRefund(boolean roleApplyRefund) {
            this.roleApplyRefund = roleApplyRefund;
        }

        public boolean isNeedResendSmsFlag() {
            return needResendSmsFlag;
        }

        public void setNeedResendSmsFlag(boolean needResendSmsFlag) {
            this.needResendSmsFlag = needResendSmsFlag;
        }

        public boolean isApplyRefund() {
            return applyRefund;
        }

        public void setApplyRefund(boolean applyRefund) {
            this.applyRefund = applyRefund;
        }

        public int getEditEnable() {
            return editEnable;
        }

        public void setEditEnable(int editEnable) {
            this.editEnable = editEnable;
        }

        public void setIdCardFrontPhoto(String idCardFrontPhoto) {
            this.idCardFrontPhoto = idCardFrontPhoto;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public void setIdCardBackPhoto(String idCardBackPhoto) {
            this.idCardBackPhoto = idCardBackPhoto;
        }

        public void setIdCode(String idCode) {
            this.idCode = idCode;
        }

        public void setStores(List<StoresBean> stores) {
            this.stores = stores;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getIdCardFrontPhoto() {
            return idCardFrontPhoto;
        }

        public String getRealName() {
            return realName;
        }

        public String getIdCardBackPhoto() {
            return idCardBackPhoto;
        }

        public String getIdCode() {
            return idCode;
        }

        public List<StoresBean> getStores() {
            return stores;
        }

        public String getTelphone() {
            return telphone;
        }

        public String getUserId() {
            return userId;
        }

        public boolean isEnabled() {
            return enabled;
        }
    }
}
