package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/14 17:22
 * @描述: ${TODO}
 */

public class ManagerListBean implements Serializable{
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"idCardFrontPhoto":"/pic/mch/2017/12/13/4dc74fbd-c98f-4b01-b583-25d141a79834.jpg","empId":68,"serviceProviderId":"889100000001","empType":4,"idCode":"371002200610020000","updateUserName":"超级管理员","mobile":"18718578888","updateUser":1,"createUserName":"韦小宝","updateTime":"2017-12-14 16:08:18","userId":162,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/a2997d28-e773-4337-8e42-07c627f7fa78.jpg","empSex":0,"createTime":"2017-12-13 20:17:19","merchantId":"748200000003","empName":"方大呆2号","enabledCnt":"启用","createUser":36},{"idCardFrontPhoto":"/pic/mch/2017/12/13/2d122863-cb46-43fb-b693-ee24347864d0.jpg","empId":67,"serviceProviderId":"889100000001","empType":4,"idCode":"371002200610020000","updateUserName":"超级管理员","mobile":"18718571234","updateUser":1,"createUserName":"韦小宝","updateTime":"2017-12-13 20:11:36","userId":161,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/56f60880-1f3b-4e50-9352-66627e332891.jpg","empSex":0,"createTime":"2017-12-13 17:22:21","merchantId":"748200000003","empName":"方大呆","enabledCnt":"启用","createUser":36},{"idCardFrontPhoto":"/pic/mch/2017/12/13/6c0e2bbf-0ca0-4ada-a2c7-8cb63f7f2967.jpg","empId":66,"serviceProviderId":"476100000001","empType":4,"idCode":"4321432143214321","updateUserName":"超级管理员","mobile":"15895845737","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 16:40:13","userId":160,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/bad6e69e-4934-42de-9a54-cf6eae98d9c1.jpg","empSex":0,"createTime":"2017-12-13 15:55:35","merchantId":"661200000010","empName":"mmfdsam","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/4129c201-b499-47a7-b9d2-43547d0a63d4.jpg","empId":65,"serviceProviderId":"476100000001","empType":4,"idCode":"431024199602052117","updateUserName":"超级管理员","mobile":"18820607928","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 16:40:25","userId":159,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/43a804bb-0077-4c5d-a830-dd89f16d15ac.jpg","empSex":0,"createTime":"2017-12-13 15:45:35","merchantId":"661200000010","empName":"店长","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/f7cab49c-7cb2-4b62-bad2-d2ed56d69dba.jpg","empId":64,"serviceProviderId":"476100000001","empType":4,"idCode":"222222222222222","updateUserName":"超级管理员","mobile":"13222222222","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 19:40:56","userId":157,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/f1317ced-2c21-4b82-ba99-d09ec732c7ff.jpg","empSex":0,"createTime":"2017-12-13 15:32:23","merchantId":"834200000012","empName":"1213测试店长","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/1a784bcc-c3aa-4942-aa45-37c7535144de.jpg","empId":63,"serviceProviderId":"889100000001","empType":4,"idCode":"222222222222222222","updateUserName":"超级管理员","mobile":"13811111111","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 15:20:35","userId":155,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/039af84c-a690-4efe-9ad2-3f4b7e9751ef.jpg","empSex":0,"createTime":"2017-12-13 15:20:35","merchantId":"123200000015","empName":"测试","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/98e17d93-8408-414c-a7fb-5ec0c6026263.jpg","empId":62,"serviceProviderId":"476100000001","empType":4,"idCode":"360730199112283332","updateUserName":"测试商户6","mobile":"13421111111","updateUser":152,"createUserName":"测试商户6","updateTime":"2017-12-13 15:20:16","userId":154,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/60ed4333-cf7c-4cb6-adbb-4f44ede290e8.jpg","empSex":0,"createTime":"2017-12-13 15:20:16","merchantId":"338200000016","empName":"店长大人","enabledCnt":"启用","createUser":152}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : 2Zyrk8bx
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static  class DataEntity implements Serializable{
        /**
         * data : [{"idCardFrontPhoto":"/pic/mch/2017/12/13/4dc74fbd-c98f-4b01-b583-25d141a79834.jpg","empId":68,"serviceProviderId":"889100000001","empType":4,"idCode":"371002200610020000","updateUserName":"超级管理员","mobile":"18718578888","updateUser":1,"createUserName":"韦小宝","updateTime":"2017-12-14 16:08:18","userId":162,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/a2997d28-e773-4337-8e42-07c627f7fa78.jpg","empSex":0,"createTime":"2017-12-13 20:17:19","merchantId":"748200000003","empName":"方大呆2号","enabledCnt":"启用","createUser":36},{"idCardFrontPhoto":"/pic/mch/2017/12/13/2d122863-cb46-43fb-b693-ee24347864d0.jpg","empId":67,"serviceProviderId":"889100000001","empType":4,"idCode":"371002200610020000","updateUserName":"超级管理员","mobile":"18718571234","updateUser":1,"createUserName":"韦小宝","updateTime":"2017-12-13 20:11:36","userId":161,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/56f60880-1f3b-4e50-9352-66627e332891.jpg","empSex":0,"createTime":"2017-12-13 17:22:21","merchantId":"748200000003","empName":"方大呆","enabledCnt":"启用","createUser":36},{"idCardFrontPhoto":"/pic/mch/2017/12/13/6c0e2bbf-0ca0-4ada-a2c7-8cb63f7f2967.jpg","empId":66,"serviceProviderId":"476100000001","empType":4,"idCode":"4321432143214321","updateUserName":"超级管理员","mobile":"15895845737","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 16:40:13","userId":160,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/bad6e69e-4934-42de-9a54-cf6eae98d9c1.jpg","empSex":0,"createTime":"2017-12-13 15:55:35","merchantId":"661200000010","empName":"mmfdsam","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/4129c201-b499-47a7-b9d2-43547d0a63d4.jpg","empId":65,"serviceProviderId":"476100000001","empType":4,"idCode":"431024199602052117","updateUserName":"超级管理员","mobile":"18820607928","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 16:40:25","userId":159,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/43a804bb-0077-4c5d-a830-dd89f16d15ac.jpg","empSex":0,"createTime":"2017-12-13 15:45:35","merchantId":"661200000010","empName":"店长","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/f7cab49c-7cb2-4b62-bad2-d2ed56d69dba.jpg","empId":64,"serviceProviderId":"476100000001","empType":4,"idCode":"222222222222222","updateUserName":"超级管理员","mobile":"13222222222","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 19:40:56","userId":157,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/f1317ced-2c21-4b82-ba99-d09ec732c7ff.jpg","empSex":0,"createTime":"2017-12-13 15:32:23","merchantId":"834200000012","empName":"1213测试店长","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/1a784bcc-c3aa-4942-aa45-37c7535144de.jpg","empId":63,"serviceProviderId":"889100000001","empType":4,"idCode":"222222222222222222","updateUserName":"超级管理员","mobile":"13811111111","updateUser":1,"createUserName":"超级管理员","updateTime":"2017-12-13 15:20:35","userId":155,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/039af84c-a690-4efe-9ad2-3f4b7e9751ef.jpg","empSex":0,"createTime":"2017-12-13 15:20:35","merchantId":"123200000015","empName":"测试","enabledCnt":"启用","createUser":1},{"idCardFrontPhoto":"/pic/mch/2017/12/13/98e17d93-8408-414c-a7fb-5ec0c6026263.jpg","empId":62,"serviceProviderId":"476100000001","empType":4,"idCode":"360730199112283332","updateUserName":"测试商户6","mobile":"13421111111","updateUser":152,"createUserName":"测试商户6","updateTime":"2017-12-13 15:20:16","userId":154,"enabled":true,"idCardBackPhoto":"/pic/mch/2017/12/13/60ed4333-cf7c-4cb6-adbb-4f44ede290e8.jpg","empSex":0,"createTime":"2017-12-13 15:20:16","merchantId":"338200000016","empName":"店长大人","enabledCnt":"启用","createUser":152}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<itemData> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<itemData> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<itemData> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public static  class itemData implements Serializable {
            /**
             * idCardFrontPhoto : /pic/mch/2017/12/13/4dc74fbd-c98f-4b01-b583-25d141a79834.jpg
             * empId : 68
             * serviceProviderId : 889100000001
             * empType : 4
             * idCode : 371002200610020000
             * updateUserName : 超级管理员
             * mobile : 18718578888
             * updateUser : 1
             * createUserName : 韦小宝
             * updateTime : 2017-12-14 16:08:18
             * userId : 162
             * enabled : true
             * idCardBackPhoto : /pic/mch/2017/12/13/a2997d28-e773-4337-8e42-07c627f7fa78.jpg
             * empSex : 0
             * createTime : 2017-12-13 20:17:19
             * merchantId : 748200000003
             * empName : 方大呆2号
             * enabledCnt : 启用
             * createUser : 36
             */
            private String idCardFrontPhoto;
            private int empId;
            private String serviceProviderId;
            private int empType;
            private String idCode;
            private String updateUserName;
            private String mobile;
            private int updateUser;
            private String createUserName;
            private String updateTime;
            private String userId;
            private boolean enabled;
            private String idCardBackPhoto;
            private int empSex;
            private String createTime;
            private String merchantId;
            private String empName;
            private String enabledCnt;
            private int createUser;
            private List<StoresBean> stores;
            private boolean isSelected;

            public boolean isSelected() {

                return isSelected;

            }

            public void setSelected(boolean selected) {
                isSelected = selected;
            }

            public List<StoresBean> getStores() {
                return stores;
            }

            public void setStores(List<StoresBean> stores) {
                this.stores = stores;
            }

            public void setIdCardFrontPhoto(String idCardFrontPhoto) {
                this.idCardFrontPhoto = idCardFrontPhoto;
            }

            public void setEmpId(int empId) {
                this.empId = empId;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public void setEmpType(int empType) {
                this.empType = empType;
            }

            public void setIdCode(String idCode) {
                this.idCode = idCode;
            }

            public void setUpdateUserName(String updateUserName) {
                this.updateUserName = updateUserName;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public void setUpdateUser(int updateUser) {
                this.updateUser = updateUser;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public void setIdCardBackPhoto(String idCardBackPhoto) {
                this.idCardBackPhoto = idCardBackPhoto;
            }

            public void setEmpSex(int empSex) {
                this.empSex = empSex;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }

            public void setEmpName(String empName) {
                this.empName = empName;
            }

            public void setEnabledCnt(String enabledCnt) {
                this.enabledCnt = enabledCnt;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public String getIdCardFrontPhoto() {
                return idCardFrontPhoto;
            }

            public int getEmpId() {
                return empId;
            }

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public int getEmpType() {
                return empType;
            }

            public String getIdCode() {
                return idCode;
            }

            public String getUpdateUserName() {
                return updateUserName;
            }

            public String getMobile() {
                return mobile;
            }

            public int getUpdateUser() {
                return updateUser;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public String getUserId() {
                return userId;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public String getIdCardBackPhoto() {
                return idCardBackPhoto;
            }

            public int getEmpSex() {
                return empSex;
            }

            public String getCreateTime() {
                return createTime;
            }

            public String getMerchantId() {
                return merchantId;
            }

            public String getEmpName() {
                return empName;
            }

            public String getEnabledCnt() {
                return enabledCnt;
            }

            public int getCreateUser() {
                return createUser;
            }
        }
    }
}
