package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/7/4 11:42
 * @描述: ${TODO}
 */
public class MaterialBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"posterStyle1Pic":"/pic/mem_activity/20180703/1950a220-bbcd-4a6e-a4f7-99a805715a7f.jpg","posterStyle2Pic":"/pic/mem_activity/20180703/17d39396-e446-42ca-add4-5dda0282f0ac.jpg","tableStickerStyle1Pic":"/pic/mem_activity/20180703/37a400f9-1149-4bba-a584-2e7b33d227cb.jpg","tableStickerStyle2Pic":"/pic/mem_activity/20180703/714e8a41-2a91-4ce3-bdec-1aed0cf4fad4.jpg"}
     * logId : NvcbZnyg
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * posterStyle1Pic : /pic/mem_activity/20180703/1950a220-bbcd-4a6e-a4f7-99a805715a7f.jpg
         * posterStyle2Pic : /pic/mem_activity/20180703/17d39396-e446-42ca-add4-5dda0282f0ac.jpg
         * tableStickerStyle1Pic : /pic/mem_activity/20180703/37a400f9-1149-4bba-a584-2e7b33d227cb.jpg
         * tableStickerStyle2Pic : /pic/mem_activity/20180703/714e8a41-2a91-4ce3-bdec-1aed0cf4fad4.jpg
         */
        private String posterStyle1Pic;
        private String posterStyle2Pic;
        private String tableStickerStyle1Pic;
        private String tableStickerStyle2Pic;

        public void setPosterStyle1Pic(String posterStyle1Pic) {
            this.posterStyle1Pic = posterStyle1Pic;
        }

        public void setPosterStyle2Pic(String posterStyle2Pic) {
            this.posterStyle2Pic = posterStyle2Pic;
        }

        public void setTableStickerStyle1Pic(String tableStickerStyle1Pic) {
            this.tableStickerStyle1Pic = tableStickerStyle1Pic;
        }

        public void setTableStickerStyle2Pic(String tableStickerStyle2Pic) {
            this.tableStickerStyle2Pic = tableStickerStyle2Pic;
        }

        public String getPosterStyle1Pic() {
            return posterStyle1Pic;
        }

        public String getPosterStyle2Pic() {
            return posterStyle2Pic;
        }

        public String getTableStickerStyle1Pic() {
            return tableStickerStyle1Pic;
        }

        public String getTableStickerStyle2Pic() {
            return tableStickerStyle2Pic;
        }
    }
}
