package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/20 15:47
 * @描述: ${TODO}
 */

public class MaterialStatusBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"examineStatusCnt":"待审核","examineRemark":"商户业务模式不清晰;商户经营范围不符合法律法规规定;营业执照不清晰;","examineStatus":0}
     * logId : DJPQWONo
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * examineStatusCnt : 待审核
         * examineRemark : 商户业务模式不清晰;商户经营范围不符合法律法规规定;营业执照不清晰;
         * examineStatus : 0
         */
        private String standby7;
        private String standby8;
        private String standby9;
        private String examineStatusCnt;
        private String examineRemark;
        private int examineStatus;

        public String getStandby7() {
            return standby7;
        }

        public void setStandby7(String standby7) {
            this.standby7 = standby7;
        }

        public String getStandby8() {
            return standby8;
        }

        public void setStandby8(String standby8) {
            this.standby8 = standby8;
        }

        public String getStandby9() {
            return standby9;
        }

        public void setStandby9(String standby9) {
            this.standby9 = standby9;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public int getExamineStatus() {
            return examineStatus;
        }
    }
}
