package com.hstypay.enterprise.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/22
 * @desc 商家说
 */
public class MchtalkBean extends BaseBean<MchtalkBean.MchtalkData>{
    public static class MchtalkData{
        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;

        private List<MchTalkItemBean> data;

        public int getPageSize() {

            return pageSize;

        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public List<MchTalkItemBean> getData() {

            if (data == null) {
                return new ArrayList<>();
            }
            return data;

        }

        public void setData(List<MchTalkItemBean> data) {
            this.data = data;
        }
    }

    public static class MchTalkItemBean{
        private String noticeCode;//文章编码
        private String title;//标题
        private String coverImg;//封面图片路径 /pic/mch/2019/08/29/df6c93a3-cf37-47d1-a987-36262b45d338.jpg
        private String staticPageAccessPath;//静态页面访问路径
        private String likeTotal;//点赞数
        private String readTotal;//阅读数
        private String publishUserName;//发布人
        private String publishTime;//发布时间 "2005-08-28 16:21:54"

        public String getNoticeCode() {

            return noticeCode == null ? "" : noticeCode;

        }

        public void setNoticeCode(String noticeCode) {
            this.noticeCode = noticeCode;
        }

        public String getTitle() {

            return title == null ? "" : title;

        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCoverImg() {

            return coverImg == null ? "" : coverImg;

        }

        public void setCoverImg(String coverImg) {
            this.coverImg = coverImg;
        }

        public String getStaticPageAccessPath() {

            return staticPageAccessPath == null ? "" : staticPageAccessPath;

        }

        public void setStaticPageAccessPath(String staticPageAccessPath) {
            this.staticPageAccessPath = staticPageAccessPath;
        }

        public String getLikeTotal() {

            return likeTotal == null ? "" : likeTotal;

        }

        public void setLikeTotal(String likeTotal) {
            this.likeTotal = likeTotal;
        }

        public String getReadTotal() {

            return readTotal == null ? "" : readTotal;

        }

        public void setReadTotal(String readTotal) {
            this.readTotal = readTotal;
        }

        public String getPublishUserName() {

            return publishUserName == null ? "" : publishUserName;

        }

        public void setPublishUserName(String publishUserName) {
            this.publishUserName = publishUserName;
        }

        public String getPublishTime() {

            return publishTime == null ? "" : publishTime;

        }

        public void setPublishTime(String publishTime) {
            this.publishTime = publishTime;
        }


    }

}
