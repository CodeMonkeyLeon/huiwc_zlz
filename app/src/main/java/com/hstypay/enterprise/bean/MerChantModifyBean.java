package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/14 20:44
 * @描述: ${TODO}
 */

public class MerChantModifyBean {
    /**
     * data : {"examineStatusCnt":"待审核","examineRemark":"就是审核不通过","examineStatus":0}
     * logId : JNG8PnCt
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * examineStatusCnt : 待审核
         * examineRemark : 就是审核不通过
         * examineStatus : 0 待审核 ,1 通过 ,2 成功
         */
        /**
         * attestationStatus
         * 0、未认证 资料未补件
         * 1、已认证 资料已补件，审核通过，修改待审核，修改待审核不通过，可交易或部分可交易
         * 2、认证中 资料已补件，待审核，审核通过，不可交易
         * 3、未认证 资料已补件，审核不通过，不可交易（有失败原因）
         */
        private String examineStatusCnt;
        private String examineRemark;
        private int examineStatus;
        private int accountAttribute; //1是对私，2对公
        private int accountType; //1 企业 ； 2 个人
        private int attestationStatus;

        public int getAttestationStatus() {
            return attestationStatus;
        }

        public void setAttestationStatus(int attestationStatus) {
            this.attestationStatus = attestationStatus;
        }
        public int getAccountAttribute() {
            return accountAttribute;
        }

        public void setAccountAttribute(int accountAttribute) {
            this.accountAttribute = accountAttribute;
        }

        public int getAccountType() {
            return accountType;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public int getExamineStatus() {
            return examineStatus;
        }
    }
}
