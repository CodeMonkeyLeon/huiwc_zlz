package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/2/18 17:46
 * @描述: ${TODO}
 */
public class MerchantIdBean {

    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : [{"orgName":"少雄测试商户编号A","createTime":"2019-02-18 16:15:56","orgId":"10200000951"},{"orgName":"少雄测试商户编号B","createTime":"2019-02-18 16:19:44","orgId":"10200000952"},{"orgName":"少雄测试商户编号C","createTime":"2019-02-18 16:20:15","orgId":"10200000953"},{"orgName":"少雄测试商户编号D","createTime":"2019-02-18 16:20:30","orgId":"10200000954"},{"orgName":"少雄测试商户编号E","createTime":"2019-02-18 16:43:02","orgId":"10200000955"},{"orgName":"少雄测试商户编号E","createTime":"2019-02-18 16:43:47","orgId":"10200000956"}]
     * logId : a54a37923e0a45669bafc5c9330faae4
     * status : true
     */
    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{
        /**
         * orgName : 少雄测试商户编号A
         * createTime : 2019-02-18 16:15:56
         * orgId : 10200000951
         */
        private String orgName;
        private String createTime;
        private String orgId;

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setOrgId(String orgId) {
            this.orgId = orgId;
        }

        public String getOrgName() {
            return orgName;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getOrgId() {
            return orgId;
        }
    }
}
