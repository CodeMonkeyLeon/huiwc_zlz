package com.hstypay.enterprise.bean;

import java.util.List;

public class ModifyRecordBean {

    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public class DataBean {
        private String merchantId;
        private String reason;//原因
        private List<RecordList> recordList;
        private int state;//1 审核通过 2 审核不通过 11 变更审核中

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public List<RecordList> getRecordList() {
            return recordList;
        }

        public void setRecordList(List<RecordList> recordList) {
            this.recordList = recordList;
        }

        public class RecordList {
            private String proName;//变更记录id
            private String proNameCnt;//变更记录id
            private String before;//变更前
            private String after;//变更后
            private String updateTime;//变更时间
            private String updateUserId;//变更人USER ID
            private String updateUserName;//变更人
            private String recordId;
            private int state;

            public String getProNameCnt() {
                return proNameCnt;
            }

            public void setProNameCnt(String proNameCnt) {
                this.proNameCnt = proNameCnt;
            }

            public String getRecordId() {
                return recordId;
            }

            public void setRecordId(String recordId) {
                this.recordId = recordId;
            }

            public int getState() {
                return state;
            }

            public void setState(int state) {
                this.state = state;
            }

            public String getProName() {
                return proName;
            }

            public void setProName(String proName) {
                this.proName = proName;
            }

            public String getBefore() {
                return before;
            }

            public void setBefore(String before) {
                this.before = before;
            }

            public String getAfter() {
                return after;
            }

            public void setAfter(String after) {
                this.after = after;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public String getUpdateUserId() {
                return updateUserId;
            }

            public void setUpdateUserId(String updateUserId) {
                this.updateUserId = updateUserId;
            }

            public String getUpdateUserName() {
                return updateUserName;
            }

            public void setUpdateUserName(String updateUserName) {
                this.updateUserName = updateUserName;
            }
        }
    }
}
