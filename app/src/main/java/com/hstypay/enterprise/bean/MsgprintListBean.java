package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * @author zeyu.kuang
 * @time 2020/9/22
 * @desc
 */
public class MsgprintListBean implements Serializable {
    private String sn;
    private String deviceId;
    private String firm;
    private String storeMerchantId;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getStoreMerchantId() {
        return storeMerchantId;
    }

    public void setStoreMerchantId(String storeMerchantId) {
        this.storeMerchantId = storeMerchantId;
    }
}
