package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/15 15:53
 * @描述: ${TODO}
 */

public class MyCardBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : [{"accountCode":"62224242424242424242","bankId":1,"examineStatusCnt":"审核通过","bankIdCnt":"中国工商银行","examineRemark":"商户业务模式不清晰;商户经营范围不符合法律法规规定;营业执照不清晰;","examineStatus":1}]
     * logId : 6kxUCUXw
     * status : true
     */
    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * accountCode : 62224242424242424242
         * bankId : 1
         * examineStatusCnt : 审核通过
         * bankIdCnt : 中国工商银行
         * examineRemark : 商户业务模式不清晰;商户经营范围不符合法律法规规定;营业执照不清晰;
         * examineStatus : 1
         */
        private String accountCode;
        private long bankId;
        private String examineStatusCnt;
        private String bankIdCnt;
        private String examineRemark;
        private int examineStatus;

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public void setBankId(long bankId) {
            this.bankId = bankId;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public void setBankIdCnt(String bankIdCnt) {
            this.bankIdCnt = bankIdCnt;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public long getBankId() {
            return bankId;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public String getBankIdCnt() {
            return bankIdCnt;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public int getExamineStatus() {
            return examineStatus;
        }
    }
}
