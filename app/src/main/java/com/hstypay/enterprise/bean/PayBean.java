package com.hstypay.enterprise.bean;

import com.hstypay.enterprise.bean.vanke.CouponInfoData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/30.
 */

public class PayBean implements Serializable {
    /**
     * logId : dyev07UL
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"outTradeNo":"68320000000176699083625814","orderNo":"20170731868320000000100000032033","outTransactionId":"4008472001201707313604513827","money":"1","feeType":"CNY","needQuery":true,"tradeFinishTime":"2017-07-31 10:15:00","apiCode":"pay.wechat.micropay","isSubscribe":"1","apiName":"微信","apiProvider":1,"apiProviderName":"微信"}
     */
    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable {

        /** 商户名称**/
        private String mchName;
        /*** 商户退款单号**/
        private String outRefundNo;

        /*** 退款用户**/
        private String refundUserId;

        /*** 退款用户实名**/
        private String refundUserRealName;
        public String partner;

        //订单详情
        private String operNo;//退款操作员编号s
        private String strTradeStatus;
        private String createTime;
        private String tradeTime;

        private long orderType;//1:退款 2：收款
        private long refundMoney;//退款金额
        private String refundTime;//退款时间
        private String transactionId;//第三方单号

        private String refundUser;//退款人
        private String refundUpdateTime;//退款完成时间
        private String refundNo;//退款单号
        private int refundStatus;//退款状态
        private String strRefundStatus;
        private String storeMerchantIdCnt;
        private int mchAuditStatus;
        private String opUserId;

        //================
        private String mchId;
        private String storeMerchantId;
        private int tradeState;
        private String tradeStateName;
        private String deviceInfo;
        private String termNo;
        private int editEnable;//是否可编辑/退款 1:是 0:否
        private String opUserRealName;

        private String outTradeNo;
        private String orderNo;
        private String outTransactionId;
        private long money;
        private String feeType;
        private boolean needQuery;
        private String tradeFinishTime;
        private String apiCode;
        private String isSubscribe;
        private String apiName;
        private int apiProvider;
        private String apiProviderName;
        private long couponFee;//优惠券
        private int tradeType;
        private String residueNumber;
        private long rewardMoney;
        private String reqOrderNo;
        private String reqRefundNo;
        private String oriReqOrderNo;
        private String voucherNo;
        private String retNotifyUrl;
        private String ext1;
        private int billTodayFlag;//是否今日订单 0-非当日订单 1-当日订单
        private int reverseFlag;// 0-退款 1-冲正
        private long unionPayMoney;// 分期加价金额
        private String openid;
        private String thirdMerchantId;//收单机构商户编号
        private String tradeCode;//终端编号
        private String thirdOrderNo;//收单机构商户订单号
        private String oriRefNo;//原交易参考号
        private String refNo;//原交易参考号
        private String oriBatchId;//原交易批次号
        private String oriVoucherNo;//原交易凭证号
        private String cashPointName;//收银点
        /** 支付状态码 SUCCESS—支付成功,REFUND—转入退款,NOTPAY—未支付,REVOKED/REVERSE—已撤销,USERPAYING—用户支付中,PAYERROR—支付失败(其他原因，如银行返回失败) */
        private String stateCode;
        /**
         * 支付状态描述
         */
        private String stateDesc;

        private boolean isPay = false;

        /**
         * 实付金额
         */
        private long payMoney;
        /**
         * 实收金额
         */
        private long realMoney;
        /**
         * 商家优惠金额
         */
        private long mchDiscountsMoney;
        /**
         * 免充值金额/
         * 微信代金券
         */
        private long mdiscount;
        /**
         * 退款记录
         */
        private List<RefundBean> refundDTOList;
        /**
         * 优惠明细
         */
        private List<Coupon> couponInfoList;

        /**
         * 使用优惠券使用列表
         */
        private List<CouponInfoData> couponInfoData;


        /**
         * 使用团购券核销明细列表
         */
        private List<CouponInfoData> couponGroupInfoData;

        /**
         * 使用优惠券类型列表
         * couponProviderTypeList属性（非必填），interProviderType如果有1，表示包含美团优惠券
         */
        private List<Integer> couponProviderTypeList;

        public List<CouponInfoData> getCouponGroupInfoData() {
            return couponGroupInfoData;
        }

        public void setCouponGroupInfoData(List<CouponInfoData> couponGroupInfoData) {
            this.couponGroupInfoData = couponGroupInfoData;
        }

        public List<Integer> getCouponProviderTypeList() {
            return couponProviderTypeList;
        }

        public void setCouponProviderTypeList(List<Integer> couponProviderTypeList) {
            this.couponProviderTypeList = couponProviderTypeList;
        }

        private String merchantName;


        private String payCardId;//刷卡卡号
        private String payCardType;//刷卡类型
        private String payCardBankName;//发卡行
        private String batchID;//批次号

        public long getOrderType() {
            return orderType;
        }

        public void setOrderType(long orderType) {
            this.orderType = orderType;
        }

        public String getTermNo() {
            return termNo;
        }

        public void setTermNo(String termNo) {
            this.termNo = termNo;
        }

        public String getOriBatchId() {
            return oriBatchId;
        }

        public void setOriBatchId(String oriBatchId) {
            this.oriBatchId = oriBatchId;
        }

        public String getOriVoucherNo() {
            return oriVoucherNo;
        }

        public void setOriVoucherNo(String oriVoucherNo) {
            this.oriVoucherNo = oriVoucherNo;
        }

        public String getBatchID() {
            return batchID;
        }

        public void setBatchID(String batchID) {
            this.batchID = batchID;
        }

        public String getPayCardId() {
            return payCardId;
        }

        public void setPayCardId(String payCardId) {
            this.payCardId = payCardId;
        }

        public String getPayCardType() {
            return payCardType;
        }

        public void setPayCardType(String payCardType) {
            this.payCardType = payCardType;
        }

        public String getPayCardBankName() {
            return payCardBankName;
        }

        public void setPayCardBankName(String payCardBankName) {
            this.payCardBankName = payCardBankName;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public List<CouponInfoData> getCouponInfoData() {
            return couponInfoData;
        }

        public void setCouponInfoData(List<CouponInfoData> couponInfoData) {
            this.couponInfoData = couponInfoData;
        }

        public List<Coupon> getCouponInfoList() {
            return couponInfoList;
        }


        public String getOpUserId() {
            return opUserId;
        }

        public void setOpUserId(String opUserId) {
            this.opUserId = opUserId;
        }

        public void setCouponInfoList(List<Coupon> couponInfoList) {
            this.couponInfoList = couponInfoList;
        }

        public long getRealMoney() {
            return realMoney;
        }

        public void setRealMoney(long realMoney) {
            this.realMoney = realMoney;
        }

        public long getPayMoney() {
            return payMoney;
        }

        public void setPayMoney(long payMoney) {
            this.payMoney = payMoney;
        }

        public long getMchDiscountsMoney() {
            return mchDiscountsMoney;
        }

        public void setMchDiscountsMoney(long mchDiscountsMoney) {
            this.mchDiscountsMoney = mchDiscountsMoney;
        }

        public long getMdiscount() {
            return mdiscount;
        }

        public void setMdiscount(long mdiscount) {
            this.mdiscount = mdiscount;
        }

        public List<RefundBean> getRefundDTOList() {
            return refundDTOList;
        }

        public void setRefundDTOList(List<RefundBean> refundDTOList) {
            this.refundDTOList = refundDTOList;
        }

        public String getReqRefundNo() {
            return reqRefundNo;
        }

        public void setReqRefundNo(String reqRefundNo) {
            this.reqRefundNo = reqRefundNo;
        }

        public String getOriReqOrderNo() {
            return oriReqOrderNo;
        }

        public void setOriReqOrderNo(String oriReqOrderNo) {
            this.oriReqOrderNo = oriReqOrderNo;
        }

        public int getReverseFlag() {
            return reverseFlag;
        }

        public void setReverseFlag(int reverseFlag) {
            this.reverseFlag = reverseFlag;
        }

        public int getBillTodayFlag() {
            return billTodayFlag;
        }

        public void setBillTodayFlag(int billTodayFlag) {
            this.billTodayFlag = billTodayFlag;
        }

        public String getReqOrderNo() {
            return reqOrderNo;
        }

        public void setReqOrderNo(String reqOrderNo) {
            this.reqOrderNo = reqOrderNo;
        }

        public String getVoucherNo() {
            return voucherNo;
        }

        public void setVoucherNo(String voucherNo) {
            this.voucherNo = voucherNo;
        }

        public String getRetNotifyUrl() {
            return retNotifyUrl;
        }

        public void setRetNotifyUrl(String retNotifyUrl) {
            this.retNotifyUrl = retNotifyUrl;
        }

        public String getExt1() {
            return ext1;
        }

        public void setExt1(String ext1) {
            this.ext1 = ext1;
        }

        public long getRewardMoney() {
            return rewardMoney;
        }

        public void setRewardMoney(long rewardMoney) {
            this.rewardMoney = rewardMoney;
        }

        public String getResidueNumber() {
            return residueNumber;
        }

        public void setResidueNumber(String residueNumber) {
            this.residueNumber = residueNumber;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        /**
         * @return 返回 isPay
         */
        public boolean isPay() {
            return isPay;
        }

        /**
         * param 对isPay进行赋值
         */
        public void setPay(boolean isPay) {
            this.isPay = isPay;
        }

        public long getRefundMoney() {
            return refundMoney;
        }

        public void setRefundMoney(long refundMoney) {
            this.refundMoney = refundMoney;
        }

        public String getAttach() {
            return attach;
        }

        public void setAttach(String attach) {
            this.attach = attach;
        }

        private String attach;//付款备注

        public String getOperNo() {
            return operNo;
        }

        public void setOperNo(String operNo) {
            this.operNo = operNo;
        }

        public String getStrTradeStatus() {
            return strTradeStatus;
        }

        public void setStrTradeStatus(String strTradeStatus) {
            this.strTradeStatus = strTradeStatus;
        }

        public String getTradeTime() {
            return tradeTime;
        }

        public void setTradeTime(String tradeTime) {
            this.tradeTime = tradeTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }


        public String getRefundTime() {
            return refundTime;
        }

        public void setRefundTime(String refundTime) {
            this.refundTime = refundTime;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getRefundUser() {
            return refundUser;
        }

        public void setRefundUser(String refundUser) {
            this.refundUser = refundUser;
        }

        public String getRefundUpdateTime() {
            return refundUpdateTime;
        }

        public void setRefundUpdateTime(String refundUpdateTime) {
            this.refundUpdateTime = refundUpdateTime;
        }

        public String getRefundNo() {
            return refundNo;
        }

        public void setRefundNo(String refundNo) {
            this.refundNo = refundNo;
        }

        public int getRefundStatus() {
            return refundStatus;
        }

        public void setRefundStatus(int refundStatus) {
            this.refundStatus = refundStatus;
        }

        public String getStrRefundStatus() {
            return strRefundStatus;
        }

        public void setStrRefundStatus(String strRefundStatus) {
            this.strRefundStatus = strRefundStatus;
        }

        public int getEditEnable(){
            return editEnable;
        }

        public void setEditEnable(int editEnable){
            this.editEnable = editEnable;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt;
        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public int getMchAuditStatus() {
            return mchAuditStatus;
        }

        public void setMchAuditStatus(int mchAuditStatus) {
            this.mchAuditStatus = mchAuditStatus;
        }

        public String getOpUserRealName() {
            return opUserRealName;
        }

        public void setOpUserRealName(String opUserRealName) {
            this.opUserRealName = opUserRealName;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public int getTradeState() {
            return tradeState;
        }

        public void setTradeState(int tradeState) {
            this.tradeState = tradeState;
        }

        public String getTradeStateName() {
            return tradeStateName;
        }

        public void setTradeStateName(String tradeStateName) {
            this.tradeStateName = tradeStateName;
        }

        public String getDeviceInfo() {
            return deviceInfo;
        }

        public void setDeviceInfo(String deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public String getMchId() {
            return mchId;
        }

        public void setMchId(String mchId) {
            this.mchId = mchId;
        }

        public String getCashierName() {
            return cashierName;
        }

        public void setCashierName(String cashierName) {
            this.cashierName = cashierName;
        }

        private String cashierName;//收银员

        public long getMoney() {
            return money;
        }

        public void setMoney(long money) {
            this.money = money;
        }

        public int getTradeType(){
            return tradeType;
        }

        public void setTradeType(int tradeType){
            this.tradeType = tradeType;
        }


        public long getCouponFee(){
            return couponFee;
        }

        public void setCouponFee(long couponFee){
            this.couponFee = couponFee;
        }

        public String getOutTradeNo() {
            return outTradeNo;
        }

        public void setOutTradeNo(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getOutTransactionId() {
            return outTransactionId;
        }

        public void setOutTransactionId(String outTransactionId) {
            this.outTransactionId = outTransactionId;
        }


        public String getFeeType() {
            return feeType;
        }

        public void setFeeType(String feeType) {
            this.feeType = feeType;
        }

        public boolean isNeedQuery() {
            return needQuery;
        }

        public void setNeedQuery(boolean needQuery) {
            this.needQuery = needQuery;
        }

        public String getTradeFinishTime() {
            return tradeFinishTime;
        }

        public void setTradeFinishTime(String tradeFinishTime) {
            this.tradeFinishTime = tradeFinishTime;
        }

        public String getApiCode() {
            return apiCode;
        }

        public void setApiCode(String apiCode) {
            this.apiCode = apiCode;
        }

        public String getIsSubscribe() {
            return isSubscribe;
        }

        public void setIsSubscribe(String isSubscribe) {
            this.isSubscribe = isSubscribe;
        }

        public String getApiName() {
            return apiName;
        }

        public void setApiName(String apiName) {
            this.apiName = apiName;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getApiProviderName() {
            return apiProviderName;
        }

        public void setApiProviderName(String apiProviderName) {
            this.apiProviderName = apiProviderName;
        }

        /**
         * outTradeNo : 68320000000176699083625814
         * orderNo : 20170731868320000000100000032033
         * outTransactionId : 4008472001201707313604513827
         * money : 1
         * feeType : CNY
         * needQuery : true
         * tradeFinishTime : 2017-07-31 10:15:00
         * apiCode : pay.wechat.micropay
         * isSubscribe : 1
         * apiName : 微信
         * apiProvider : 1
         * apiProviderName : 微信
         */

        //退款
        public String getMchName() {
            return mchName;
        }

        public void setMchName(String mchName) {
            this.mchName = mchName;
        }

        public String getOutRefundNo() {
            return outRefundNo;
        }

        public void setOutRefundNo(String outRefundNo) {
            this.outRefundNo = outRefundNo;
        }

        public String getRefundUserId() {
            return refundUserId;
        }

        public void setRefundUserId(String refundUserId) {
            this.refundUserId = refundUserId;
        }

        public String getRefundUserRealName() {
            return refundUserRealName;
        }

        public void setRefundUserRealName(String refundUserRealName) {
            this.refundUserRealName = refundUserRealName;
        }

        public long getUnionPayMoney() {
            return unionPayMoney;
        }

        public void setUnionPayMoney(long unionPayMoney) {
            this.unionPayMoney = unionPayMoney;
        }

        public String getOpenid() {
            return openid;
        }

        public void setOpenid(String openid) {
            this.openid = openid;
        }

        public String getThirdMerchantId() {
            return thirdMerchantId;
        }

        public void setThirdMerchantId(String thirdMerchantId) {
            this.thirdMerchantId = thirdMerchantId;
        }

        public String getTradeCode() {
            return tradeCode;
        }

        public void setTradeCode(String tradeCode) {
            this.tradeCode = tradeCode;
        }

        public String getThirdOrderNo() {
            return thirdOrderNo;
        }

        public void setThirdOrderNo(String thirdOrderNo) {
            this.thirdOrderNo = thirdOrderNo;
        }

        public String getOriRefNo() {
            return oriRefNo;
        }

        public void setOriRefNo(String oriRefNo) {
            this.oriRefNo = oriRefNo;
        }

        public String getRefNo() {
            return refNo;
        }

        public void setRefNo(String refNo) {
            this.refNo = refNo;
        }

        public String getCashPointName() {
            return cashPointName;
        }

        public void setCashPointName(String cashPointName) {
            this.cashPointName = cashPointName;
        }

        public String getStateCode() {
            return stateCode;
        }

        public void setStateCode(String stateCode) {
            this.stateCode = stateCode;
        }

        public String getStateDesc() {
            return stateDesc;
        }

        public void setStateDesc(String stateDesc) {
            this.stateDesc = stateDesc;
        }
    }
}
