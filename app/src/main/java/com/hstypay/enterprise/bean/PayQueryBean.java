package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/8/1.
 */

public class PayQueryBean implements Serializable {

    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * logId : S3h26TOb
     * status : true
     * data : {"operNo":"12","orderNo":"20170726868320000000100000024043","deviceInfo":"QR_CODE","storeMerchantId":"101300000001","apiProvider":3,"strTradeStatus":"转入退款","tradeStatus":4,"tradeMoney":2,"createTime":"2017-07-26 20:13:42","refundMoney":2,"refundTime":"2017-07-27 14:07:52","transactionId":"2017072621001004860283021801","refundUser":"超级管理员","refundUpdateTime":"2017-07-27 11:47:32","refundNo":"20170727868320000000100000000001","refundStatus":2,"strRefundStatus":"退款成功"}
     */

    private String logId;
    private boolean status;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * operNo : 12
         * orderNo : 20170726868320000000100000024043
         * deviceInfo : QR_CODE
         * storeMerchantId : 101300000001
         * apiProvider : 3
         * strTradeStatus : 转入退款
         * tradeStatus : 4
         * tradeMoney : 2
         * createTime : 2017-07-26 20:13:42
         * refundMoney : 2
         * refundTime : 2017-07-27 14:07:52
         * transactionId : 2017072621001004860283021801
         * refundUser : 超级管理员
         * refundUpdateTime : 2017-07-27 11:47:32
         * refundNo : 20170727868320000000100000000001
         * refundStatus : 2
         * strRefundStatus : 退款成功
         */

        private String operNo;
        private String orderNo;
        private String deviceInfo;
        private String storeMerchantId;
        private int apiProvider;
        private String strTradeStatus;
        private int tradeStatus;
        private int tradeMoney;
        private String createTime;
        private int refundMoney;
        private String refundTime;
        private String transactionId;
        private String refundUser;
        private String refundUpdateTime;
        private String refundNo;
        private int refundStatus;
        private String strRefundStatus;

        public String getOperNo() {
            return operNo;
        }

        public void setOperNo(String operNo) {
            this.operNo = operNo;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getDeviceInfo() {
            return deviceInfo;
        }

        public void setDeviceInfo(String deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getStrTradeStatus() {
            return strTradeStatus;
        }

        public void setStrTradeStatus(String strTradeStatus) {
            this.strTradeStatus = strTradeStatus;
        }

        public int getTradeStatus() {
            return tradeStatus;
        }

        public void setTradeStatus(int tradeStatus) {
            this.tradeStatus = tradeStatus;
        }

        public int getTradeMoney() {
            return tradeMoney;
        }

        public void setTradeMoney(int tradeMoney) {
            this.tradeMoney = tradeMoney;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getRefundMoney() {
            return refundMoney;
        }

        public void setRefundMoney(int refundMoney) {
            this.refundMoney = refundMoney;
        }

        public String getRefundTime() {
            return refundTime;
        }

        public void setRefundTime(String refundTime) {
            this.refundTime = refundTime;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getRefundUser() {
            return refundUser;
        }

        public void setRefundUser(String refundUser) {
            this.refundUser = refundUser;
        }

        public String getRefundUpdateTime() {
            return refundUpdateTime;
        }

        public void setRefundUpdateTime(String refundUpdateTime) {
            this.refundUpdateTime = refundUpdateTime;
        }

        public String getRefundNo() {
            return refundNo;
        }

        public void setRefundNo(String refundNo) {
            this.refundNo = refundNo;
        }

        public int getRefundStatus() {
            return refundStatus;
        }

        public void setRefundStatus(int refundStatus) {
            this.refundStatus = refundStatus;
        }

        public String getStrRefundStatus() {
            return strRefundStatus;
        }

        public void setStrRefundStatus(String strRefundStatus) {
            this.strRefundStatus = strRefundStatus;
        }
    }
}
