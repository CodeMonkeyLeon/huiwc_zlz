package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 18:27
 * @描述: ${TODO}
 */
public class PledgeBillsBean implements Serializable{
    private ErrorBean error;
    /**
     * logId : 8b4b19f6163c4307804cbc1aa7b92e1d
     * status : true
     * data : [{"operNo":"462300000001","authNo":"2019042901000058002","deviceInfo":"HPAY_ANDROID_100001","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","outAuthNo":"1556519958345","apiProvider":1,"tradeState":1,"merchantId":"735200000001","merchantName":"彭国卿测试商户(威富通支付专用)","money":1,"createTime":"2019-04-29 14:41:11","tradeTime":"2019-04-29 14:41:11","tab":1},{"operNo":"462300000001","authNo":"2019042901000058001","deviceInfo":"HPAY_ANDROID_100001","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","outAuthNo":"1556516031009","apiProvider":1,"tradeState":1,"merchantId":"735200000001","merchantName":"彭国卿测试商户(威富通支付专用)","money":1,"createTime":"2019-04-29 14:20:32","tradeTime":"2019-04-29 14:20:32","tab":1},{"operNo":"462300000001","authNo":"2019042901000058000","deviceInfo":"HPAY_ANDROID_100001","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","outAuthNo":"1556516031008","apiProvider":1,"tradeState":1,"merchantId":"735200000001","merchantName":"彭国卿测试商户(威富通支付专用)","money":1,"createTime":"2019-04-29 14:17:11","tradeTime":"2019-04-29 14:17:11","tab":1},{"operNo":"462300000001","authNo":"2019042901000056000","deviceInfo":"HPAY_ANDROID_100001","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","outAuthNo":"1556516031007","apiProvider":1,"tradeState":1,"merchantId":"735200000001","merchantName":"彭国卿测试商户(威富通支付专用)","money":1,"createTime":"2019-04-29 14:16:52","tradeTime":"2019-04-29 14:16:52","tab":1}]
     */

    private String logId;
    private boolean status;
    private List<DataBean> data;

    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    public static class DataBean implements Serializable {
        /**
         * operNo : 462300000001
         * authNo : 2019042901000058002
         * deviceInfo : HPAY_ANDROID_100001
         * storeMerchantId : 462300000001
         * storeMerchantIdCnt : 彭国卿测试商户
         * outAuthNo : 1556519958345
         * apiProvider : 1
         * tradeState : 1
         * merchantId : 735200000001
         * merchantName : 彭国卿测试商户(威富通支付专用)
         * money : 1
         * createTime : 2019-04-29 14:41:11
         * tradeTime : 2019-04-29 14:41:11
         * tab : 1
         */

        private String orderNo;
        private String operNo;
        private String authNo;
        private String deviceInfo;
        private String transactionId;
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String outAuthNo;
        private int apiProvider;
        private String tradeState;
        private String merchantId;
        private String merchantName;
        private String money;
        private String createTime;
        private String tradeTime;
        private String sumPayMoney;
        private String sumFreeMoney;
        private String remainMoney;
        private String optStatus;
        private String cashierName;
        private String termNo;
        private int tab;

        public String getTermNo() {
            return termNo;
        }

        public void setTermNo(String termNo) {
            this.termNo = termNo;
        }

        public String getCashierName() {
            return cashierName;
        }

        public void setCashierName(String cashierName) {
            this.cashierName = cashierName;
        }

        public String getRemainMoney() {
            return remainMoney;
        }

        public void setRemainMoney(String remainMoney) {
            this.remainMoney = remainMoney;
        }

        public String getOptStatus() {
            return optStatus;
        }

        public void setOptStatus(String optStatus) {
            this.optStatus = optStatus;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getSumPayMoney() {
            return sumPayMoney;
        }

        public void setSumPayMoney(String sumPayMoney) {
            this.sumPayMoney = sumPayMoney;
        }

        public String getSumFreeMoney() {
            return sumFreeMoney;
        }

        public void setSumFreeMoney(String sumFreeMoney) {
            this.sumFreeMoney = sumFreeMoney;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getOperNo() {
            return operNo;
        }

        public void setOperNo(String operNo) {
            this.operNo = operNo;
        }

        public String getAuthNo() {
            return authNo;
        }

        public void setAuthNo(String authNo) {
            this.authNo = authNo;
        }

        public String getDeviceInfo() {
            return deviceInfo;
        }

        public void setDeviceInfo(String deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt;
        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public String getOutAuthNo() {
            return outAuthNo;
        }

        public void setOutAuthNo(String outAuthNo) {
            this.outAuthNo = outAuthNo;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getTradeState() {
            return tradeState;
        }

        public void setTradeState(String tradeState) {
            this.tradeState = tradeState;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getTradeTime() {
            return tradeTime;
        }

        public void setTradeTime(String tradeTime) {
            this.tradeTime = tradeTime;
        }

        public int getTab() {
            return tab;
        }

        public void setTab(int tab) {
            this.tab = tab;
        }
    }
}
