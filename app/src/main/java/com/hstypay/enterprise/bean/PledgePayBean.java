package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/30.
 */

public class PledgePayBean {

    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * logId : c6b037a910bf46c2b424e28d374c0fb0
     * status : true
     * data : {"needQuery":false,"mchId":"735200000001","storeMerchantId":"863300000001","storeMerchantIdCnt":"第一家门店","outAuthNo":"73520000000124905353852663264","authNo":"2019050811000007216","outTransactionId":"4200000332201905083773598618","thirdAuthNo":"2019050811000007216","originTradeNo":"2019050811000007216","couponFee":"0","tradeFinishTime":"2019-05-08 15:59:51","apiCode":"pay.wechat.micropay.preauth","opUserId":"5","deviceInfo":"HPAY_ANDROID","feeType":"CNY"}
     */
    public static class DataBean implements Serializable {
        /**
         * needQuery : false
         * mchId : 735200000001
         * storeMerchantId : 863300000001
         * storeMerchantIdCnt : 第一家门店
         * outAuthNo : 73520000000124905353852663264
         * authNo : 2019050811000007216
         * outTransactionId : 4200000332201905083773598618
         * thirdAuthNo : 2019050811000007216
         * originTradeNo : 2019050811000007216
         * couponFee : 0
         * tradeFinishTime : 2019-05-08 15:59:51
         * apiCode : pay.wechat.micropay.preauth
         * opUserId : 5
         * deviceInfo : HPAY_ANDROID
         * feeType : CNY
         */

        private boolean needQuery;
        private String mchId;
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String outAuthNo;
        private String authNo;
        private String outTransactionId;
        private String thirdAuthNo;
        private String originTradeNo;
        private String couponFee;
        private String tradeFinishTime;
        private String apiCode;
        private String apiName;
        private String opUserId;
        private String opUserRealName;
        private String deviceInfo;
        private String feeType;
        private String money;
        private String bankType;
        private String attach;
        private int apiProvider;
        private String apiProviderName;
        private String tradeStatus;
        private String requestNo;
        private String sumPayMoney;
        private String sumFreeMoney;
        private String remainMoney;
        private String optStatus;
        private String partner;
        private String cashierName;
        private boolean isToPay;//true 押金解冻（消费） ，false 押金收款
        private String termNo;//设备SN
        /**
         * 是否自动打印
         */
        private boolean isAutoPrint;

        public boolean isAutoPrint() {
            return isAutoPrint;
        }

        public void setAutoPrint(boolean autoPrint) {
            isAutoPrint = autoPrint;
        }

        public String getTermNo() {
            return termNo;
        }

        public void setTermNo(String termNo) {
            this.termNo = termNo;
        }

        public boolean isToPay() {
            return isToPay;
        }

        public void setToPay(boolean toPay) {
            isToPay = toPay;
        }

        public String getCashierName() {
            return cashierName;
        }

        public void setCashierName(String cashierName) {
            this.cashierName = cashierName;
        }

        public String getRemainMoney() {
            return remainMoney;
        }

        public void setRemainMoney(String remainMoney) {
            this.remainMoney = remainMoney;
        }

        public String getOptStatus() {
            return optStatus;
        }

        public void setOptStatus(String optStatus) {
            this.optStatus = optStatus;
        }

        public String getPartner() {
            return partner;
        }

        public void setPartner(String partner) {
            this.partner = partner;
        }

        public String getRequestNo() {
            return requestNo;
        }

        public void setRequestNo(String requestNo) {
            this.requestNo = requestNo;
        }

        public String getSumPayMoney() {
            return sumPayMoney;
        }

        public void setSumPayMoney(String sumPayMoney) {
            this.sumPayMoney = sumPayMoney;
        }

        public String getSumFreeMoney() {
            return sumFreeMoney;
        }

        public void setSumFreeMoney(String sumFreeMoney) {
            this.sumFreeMoney = sumFreeMoney;
        }

        public String getOpUserRealName() {
            return opUserRealName;
        }

        public void setOpUserRealName(String opUserRealName) {
            this.opUserRealName = opUserRealName;
        }

        public String getApiName() {
            return apiName;
        }

        public void setApiName(String apiName) {
            this.apiName = apiName;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getBankType() {
            return bankType;
        }

        public void setBankType(String bankType) {
            this.bankType = bankType;
        }

        public String getAttach() {
            return attach;
        }

        public void setAttach(String attach) {
            this.attach = attach;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getApiProviderName() {
            return apiProviderName;
        }

        public void setApiProviderName(String apiProviderName) {
            this.apiProviderName = apiProviderName;
        }

        public String getTradeStatus() {
            return tradeStatus;
        }

        public void setTradeStatus(String tradeStatus) {
            this.tradeStatus = tradeStatus;
        }

        public boolean isNeedQuery() {
            return needQuery;
        }

        public void setNeedQuery(boolean needQuery) {
            this.needQuery = needQuery;
        }

        public String getMchId() {
            return mchId;
        }

        public void setMchId(String mchId) {
            this.mchId = mchId;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt;
        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public String getOutAuthNo() {
            return outAuthNo;
        }

        public void setOutAuthNo(String outAuthNo) {
            this.outAuthNo = outAuthNo;
        }

        public String getAuthNo() {
            return authNo;
        }

        public void setAuthNo(String authNo) {
            this.authNo = authNo;
        }

        public String getOutTransactionId() {
            return outTransactionId;
        }

        public void setOutTransactionId(String outTransactionId) {
            this.outTransactionId = outTransactionId;
        }

        public String getThirdAuthNo() {
            return thirdAuthNo;
        }

        public void setThirdAuthNo(String thirdAuthNo) {
            this.thirdAuthNo = thirdAuthNo;
        }

        public String getOriginTradeNo() {
            return originTradeNo;
        }

        public void setOriginTradeNo(String originTradeNo) {
            this.originTradeNo = originTradeNo;
        }

        public String getCouponFee() {
            return couponFee;
        }

        public void setCouponFee(String couponFee) {
            this.couponFee = couponFee;
        }

        public String getTradeFinishTime() {
            return tradeFinishTime;
        }

        public void setTradeFinishTime(String tradeFinishTime) {
            this.tradeFinishTime = tradeFinishTime;
        }

        public String getApiCode() {
            return apiCode;
        }

        public void setApiCode(String apiCode) {
            this.apiCode = apiCode;
        }

        public String getOpUserId() {
            return opUserId;
        }

        public void setOpUserId(String opUserId) {
            this.opUserId = opUserId;
        }

        public String getDeviceInfo() {
            return deviceInfo;
        }

        public void setDeviceInfo(String deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public String getFeeType() {
            return feeType;
        }

        public void setFeeType(String feeType) {
            this.feeType = feeType;
        }
    }
}
