package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 18:27
 * @描述: ${TODO}
 */
public class PledgeReportBean implements Serializable{
    private ErrorBean error;
    /**
     * logId : 6557595fb3a1415f8b2d0cb6c69f32ad
     * status : true
     * data : {"sumPreMoney":4,"cntPreMoney":4,"sumPayMoney":1,"cntPayMoney":1,"sumFreeMoney":1,"cntFreeMoney":1}
     */

    private String logId;
    private boolean status;
    private DataBean data;

    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    public static class DataBean implements Serializable{
        /**
         * sumPreMoney : 4
         * cntPreMoney : 4
         * sumPayMoney : 1
         * cntPayMoney : 1
         * sumFreeMoney : 1
         * cntFreeMoney : 1
         */

        private long sumPreMoney;
        private int cntPreMoney;
        private long sumPayMoney;
        private int cntPayMoney;
        private long sumFreeMoney;
        private int cntFreeMoney;
        private long sumNotFreeMoney;
        
        private String cashierName;
        private String storeName;
        private String printTime;
        private String startTime;
        private String endTime;
        private String storeMerchantId;

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public long getSumNotFreeMoney() {
            return sumNotFreeMoney;
        }

        public void setSumNotFreeMoney(long sumNotFreeMoney) {
            this.sumNotFreeMoney = sumNotFreeMoney;
        }

        public String getCashierName() {
            return cashierName;
        }

        public void setCashierName(String cashierName) {
            this.cashierName = cashierName;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getPrintTime() {
            return printTime;
        }

        public void setPrintTime(String printTime) {
            this.printTime = printTime;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public long getSumPreMoney() {
            return sumPreMoney;
        }

        public void setSumPreMoney(long sumPreMoney) {
            this.sumPreMoney = sumPreMoney;
        }

        public int getCntPreMoney() {
            return cntPreMoney;
        }

        public void setCntPreMoney(int cntPreMoney) {
            this.cntPreMoney = cntPreMoney;
        }

        public long getSumPayMoney() {
            return sumPayMoney;
        }

        public void setSumPayMoney(long sumPayMoney) {
            this.sumPayMoney = sumPayMoney;
        }

        public int getCntPayMoney() {
            return cntPayMoney;
        }

        public void setCntPayMoney(int cntPayMoney) {
            this.cntPayMoney = cntPayMoney;
        }

        public long getSumFreeMoney() {
            return sumFreeMoney;
        }

        public void setSumFreeMoney(long sumFreeMoney) {
            this.sumFreeMoney = sumFreeMoney;
        }

        public int getCntFreeMoney() {
            return cntFreeMoney;
        }

        public void setCntFreeMoney(int cntFreeMoney) {
            this.cntFreeMoney = cntFreeMoney;
        }
    }
}
