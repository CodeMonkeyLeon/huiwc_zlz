package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/3/12 17:06
 * @描述: ${TODO}
 */
public class PosAccountBean {
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        private String status;//0:已开通刷卡，需要保存设备记录，1:已开通刷卡，无需保存设备记录，2：未开通刷卡
        private String pay_card_account;
        private String pay_card_password;
        private String thirdMerchantId;

        public String getThirdMerchantId() {
            return thirdMerchantId;
        }

        public void setThirdMerchantId(String thirdMerchantId) {
            this.thirdMerchantId = thirdMerchantId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPay_card_account() {
            return pay_card_account;
        }

        public void setPay_card_account(String pay_card_account) {
            this.pay_card_account = pay_card_account;
        }

        public String getPay_card_password() {
            return pay_card_password;
        }

        public void setPay_card_password(String pay_card_password) {
            this.pay_card_password = pay_card_password;
        }
    }
}
