package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/3/13 16:36
 * @描述: ${TODO}
 */
public class PosOrderBean {
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"storeMerchantIdCnt":"彭国卿测试商户","thirdApiCode":"pay.huifu.bcard","mchId":"735200000001","orderNo":"20190313873520000000111000006901","outTradeNo":"73520000000120069010947657115","storeMerchantId":"462300000001","retNotifyUrl":"https://notify.hstydev.com/notify/huifu/gateway","opUserId":"5","body":"Test2","deviceInfo":"HPAY_HUIFU_POS"}
     * logId : 9c7a85ae1eee4cda81f94fd46a7ddac2
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable {
        /**
         * storeMerchantIdCnt : 彭国卿测试商户
         * thirdApiCode : pay.huifu.bcard
         * mchId : 735200000001
         * orderNo : 20190313873520000000111000006901
         * outTradeNo : 73520000000120069010947657115
         * storeMerchantId : 462300000001
         * retNotifyUrl : https://notify.hstydev.com/notify/huifu/gateway
         * opUserId : 5
         * body : Test2
         * deviceInfo : HPAY_HUIFU_POS
         */
        private String thirdApiCode;
        private String mchId;
        private String orderNo;
        private String outTradeNo;
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String retNotifyUrl;
        private String opUserId;
        private String body;
        private String deviceInfo;
        private String reqOrderNo;//对应merOrdId
        private String ext1;//对应accSplitBunch
        private String ext2;//对应merPiv
        private String money;//金额
        private String createTime;//订单创建时间

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getReqOrderNo() {
            return reqOrderNo;
        }

        public void setReqOrderNo(String reqOrderNo) {
            this.reqOrderNo = reqOrderNo;
        }

        public String getExt1() {
            return ext1;
        }

        public void setExt1(String ext1) {
            this.ext1 = ext1;
        }

        public String getExt2() {
            return ext2;
        }

        public void setExt2(String ext2) {
            this.ext2 = ext2;
        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public void setThirdApiCode(String thirdApiCode) {
            this.thirdApiCode = thirdApiCode;
        }

        public void setMchId(String mchId) {
            this.mchId = mchId;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public void setOutTradeNo(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public void setRetNotifyUrl(String retNotifyUrl) {
            this.retNotifyUrl = retNotifyUrl;
        }

        public void setOpUserId(String opUserId) {
            this.opUserId = opUserId;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public void setDeviceInfo(String deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt;
        }

        public String getThirdApiCode() {
            return thirdApiCode;
        }

        public String getMchId() {
            return mchId;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public String getOutTradeNo() {
            return outTradeNo;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public String getRetNotifyUrl() {
            return retNotifyUrl;
        }

        public String getOpUserId() {
            return opUserId;
        }

        public String getBody() {
            return body;
        }

        public String getDeviceInfo() {
            return deviceInfo;
        }
    }
}
