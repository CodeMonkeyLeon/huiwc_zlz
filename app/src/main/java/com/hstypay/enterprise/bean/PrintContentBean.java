package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/22 11:24
 * @描述: ${TODO}
 */

public class PrintContentBean {

    /**
     * charset : UTF-8
     * customerId : c001
     * discountAmt : 0
     * eatSite : 1
     * nonceStr : 1565080780228
     * orderAmt : 1
     * orderId : 20170817873520000000100000042005
     * orderTime : 20190806150000
     * order_goods_list : [{"discountPrice":1,"goodsAmt":1,"goodsCode":"g01","goodsCount":1,"goodsName":"三文鱼","goodsPrice":1,"remarks":"菜品备注"}]
     * payTime : 20190806160000
     * payType : 微信支付
     * realAmt : 1
     * remark : 订单备注
     * shopCode : 462300000001
     * signAgentno : 10100000179
     * signType : MD5
     * storeName : 彭国卿测试商户
     * tableCode : tb001
     * version : 2.0
     */

    private String charset;//默认UTF-8
    private String customerId;
    private int eatSite;//1-堂吃  2-打包
    private String nonceStr;//随机数
    private long orderAmt;//订单金额
    private long realAmt;//订单支付金额
    private long discountAmt;//订单优惠金额
    private String orderId;//扫码点单平台订单号
    private String orderTime;//下单时间
    private String payTime;//支付时间
    private String payType;//支付类型
    private String remark;//订单备注
    private String shopCode;//门店编号
    private String signAgentno;//汇商提供，跟交易的授权渠道号保持一致
    private String storeName;//门店名称
    private String tableCode;//桌台编号
    private String version;//默认2.0
    private String userPhone;//手机号
    private List<OrderGoodsListBean> order_goods_list;
    private int[] scoPrintTypeList;

    public int[] getScoPrintTypeList() {
        return scoPrintTypeList;
    }

    public void setScoPrintTypeList(int[] scoPrintTypeList) {
        this.scoPrintTypeList = scoPrintTypeList;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public long getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(long discountAmt) {
        this.discountAmt = discountAmt;
    }

    public int getEatSite() {
        return eatSite;
    }

    public void setEatSite(int eatSite) {
        this.eatSite = eatSite;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public long getOrderAmt() {
        return orderAmt;
    }

    public void setOrderAmt(long orderAmt) {
        this.orderAmt = orderAmt;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public long getRealAmt() {
        return realAmt;
    }

    public void setRealAmt(long realAmt) {
        this.realAmt = realAmt;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getSignAgentno() {
        return signAgentno;
    }

    public void setSignAgentno(String signAgentno) {
        this.signAgentno = signAgentno;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<OrderGoodsListBean> getOrderGoods() {
        return order_goods_list;
    }

    public void setOrderGoods(List<OrderGoodsListBean> order_goods_list) {
        this.order_goods_list = order_goods_list;
    }

    public static class OrderGoodsListBean {
        /**
         * discountPrice : 1
         * goodsAmt : 1
         * goodsCode : g01
         * goodsCount : 1
         * goodsName : 三文鱼
         * goodsPrice : 1
         * remarks : 菜品备注
         */

        private long discountPrice;//商品优惠价格
        private long goodsAmt;//商品金额
        private String goodsCode;//商品编码
        private int goodsCount;//商品数量
        private String goodsName;//商品名称
        private long goodsPrice;//商品价格
        private String remarks;//备注

        public long getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(long discountPrice) {
            this.discountPrice = discountPrice;
        }

        public long getGoodsAmt() {
            return goodsAmt;
        }

        public void setGoodsAmt(long goodsAmt) {
            this.goodsAmt = goodsAmt;
        }

        public String getGoodsCode() {
            return goodsCode;
        }

        public void setGoodsCode(String goodsCode) {
            this.goodsCode = goodsCode;
        }

        public int getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(int goodsCount) {
            this.goodsCount = goodsCount;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public long getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(long goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }
    }
}
