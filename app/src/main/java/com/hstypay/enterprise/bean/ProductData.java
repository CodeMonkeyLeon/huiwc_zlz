package com.hstypay.enterprise.bean;

public class ProductData {
	private String p_name;
	private String p_price;
	private String p_count;
	private String p_amount;
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public String getP_price() {
		return p_price;
	}
	public void setP_price(String p_price) {
		this.p_price = p_price;
	}
	public String getP_count() {
		return p_count;
	}
	public void setP_count(String p_count) {
		this.p_count = p_count;
	}
	public String getP_amount() {
		return p_amount;
	}
	public void setP_amount(String p_amount) {
		this.p_amount = p_amount;
	}
	
}
