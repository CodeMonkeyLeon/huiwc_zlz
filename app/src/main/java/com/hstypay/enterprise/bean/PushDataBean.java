package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/20 14:50
 * @描述: ${TODO}
 */
public class PushDataBean {


    /**
     * content : 您有一条新的到家消息！
     * contentFormat : 2
     * createDate : 1589956082267
     * data : {"url":"http://www.baidu.com"}
     * messageKind : 60002
     * orderNo : 2020501912318546133
     * title : 新的消息
     * voiceContent : daojia_notice
     */

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
