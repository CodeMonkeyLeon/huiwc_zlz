package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/25.
 */

public class PushModeBean {
    /**
     * data : [{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024045","strTradeStatus":"未支付","createTime":"2017-07-26 20:51:57","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024044","strTradeStatus":"未支付","createTime":"2017-07-26 20:47:11","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024043","strTradeStatus":"转入退款","createTime":"2017-07-26 20:13:42","tradeStatus":4,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024042","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:52","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":50,"apiProvider":3,"orderNo":"20170726868320000000100000024041","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:39","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024040","strTradeStatus":"未支付","createTime":"2017-07-26 19:40:47","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024039","strTradeStatus":"未支付","createTime":"2017-07-26 19:17:09","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024038","strTradeStatus":"未支付","createTime":"2017-07-26 19:08:14","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024037","strTradeStatus":"支付成功","createTime":"2017-07-26 18:21:37","tradeStatus":2,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024036","strTradeStatus":"未支付","createTime":"2017-07-26 18:19:00","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024035","strTradeStatus":"未支付","createTime":"2017-07-26 18:17:36","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024034","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:44","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024033","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:06","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024032","strTradeStatus":"未支付","createTime":"2017-07-26 18:15:37","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024031","strTradeStatus":"未支付","createTime":"2017-07-26 18:14:46","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"}]
     * logId : mxfMM2GB
     * status : true
     */
    private ErrorBean error;
    /**
     * data : {"pushFrequencys":[{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"NOT_PUSH","typeId":0,"typeName":"不再推送","updateTime":"2021-06-17 17:24:35"},{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"EVERY_DAY","typeId":1,"typeName":"每天首次登录","updateTime":"2021-06-17 12:29:41"},{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"EVERY_WEEK","typeId":7,"typeName":"每周首次登录","updateTime":"2021-06-17 12:29:41"},{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"EVERY_LOGIN","typeId":99,"typeName":"每次登录","updateTime":"2021-06-17 12:29:41"}],"pushMode":1,"pushUpgradeFrequency":99}
     * logId : 5ab047187d574488b924bec0bee98b37
     * status : true
     */

    private DataBean data;
    private String logId;
    private boolean status;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class DataBean implements Serializable {
        /**
         * pushFrequencys : [{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"NOT_PUSH","typeId":0,"typeName":"不再推送","updateTime":"2021-06-17 17:24:35"},{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"EVERY_DAY","typeId":1,"typeName":"每天首次登录","updateTime":"2021-06-17 12:29:41"},{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"EVERY_WEEK","typeId":7,"typeName":"每周首次登录","updateTime":"2021-06-17 12:29:41"},{"createTime":"2021-06-17 12:29:41","createUser":1,"createUserName":"superadmin","currentPage":0,"pageSize":0,"typeClass":"PUSH_NEW_PLATFORM_FREQUENCY","typeCode":"EVERY_LOGIN","typeId":99,"typeName":"每次登录","updateTime":"2021-06-17 12:29:41"}]
         * pushMode : 1
         * pushUpgradeFrequency : 99
         */

        private int pushMode;
        private int pushUpgradeFrequency;
        private List<PushFrequencysBean> pushFrequencys;

        public int getPushMode() {
            return pushMode;
        }

        public void setPushMode(int pushMode) {
            this.pushMode = pushMode;
        }

        public int getPushUpgradeFrequency() {
            return pushUpgradeFrequency;
        }

        public void setPushUpgradeFrequency(int pushUpgradeFrequency) {
            this.pushUpgradeFrequency = pushUpgradeFrequency;
        }

        public List<PushFrequencysBean> getPushFrequencys() {
            return pushFrequencys;
        }

        public void setPushFrequencys(List<PushFrequencysBean> pushFrequencys) {
            this.pushFrequencys = pushFrequencys;
        }

        public static class PushFrequencysBean implements Serializable {
            /**
             * createTime : 2021-06-17 12:29:41
             * createUser : 1
             * createUserName : superadmin
             * currentPage : 0
             * pageSize : 0
             * typeClass : PUSH_NEW_PLATFORM_FREQUENCY
             * typeCode : NOT_PUSH
             * typeId : 0
             * typeName : 不再推送
             * updateTime : 2021-06-17 17:24:35
             */

            private String createTime;
            private int createUser;
            private String createUserName;
            private int currentPage;
            private int pageSize;
            private String typeClass;
            private String typeCode;
            private int typeId;
            private String typeName;
            private String updateTime;

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public int getCreateUser() {
                return createUser;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public String getTypeClass() {
                return typeClass;
            }

            public void setTypeClass(String typeClass) {
                this.typeClass = typeClass;
            }

            public String getTypeCode() {
                return typeCode;
            }

            public void setTypeCode(String typeCode) {
                this.typeCode = typeCode;
            }

            public int getTypeId() {
                return typeId;
            }

            public void setTypeId(int typeId) {
                this.typeId = typeId;
            }

            public String getTypeName() {
                return typeName;
            }

            public void setTypeName(String typeName) {
                this.typeName = typeName;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }
        }
    }
}
