package com.hstypay.enterprise.bean;

/**
 * Created by admin on 2017/7/17.
 * 语音播报
 */

public class PushModel {

    //{"content":"收到一笔订单，20元","contentFormat":2,"createDate":1500270146961,"messageKind":90002,"title":"订单：20170715115509001，支付成功","voiceContent":"收款成功，20元"


    /**
     * 收银员颁奖
     */
    public static final int MESSAGE_KIND_CASHIER_WIN = 90000;

    /**
     * 收银员发奖
     */
    public static final int MESSAGE_KIND_CASHIER_CASH_OUT = 90001;

    /**
     * 固定二维码推送
     */
    public static final int MESSAGE_KIND_FIXCODE_PAY_SUCCESS = 90002;

    /**
     * 文本格式
     */
    public static final int CONTENT_FORMAT_TEXT = 1;

    /**
     * JSON格式
     */
    public static final int CONTENT_FORMAT_JSON = 2;

    /**
     * 消息类型
     */
    private Integer messageKind;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 播报内容
     */
    private String voiceContent;

    private String data;

    public String getTradeMoney() {
        return tradeMoney;
    }

    public void setTradeMoney(String tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    private String tradeMoney;

    /**
     * 标题
     */
    private String title;

    private int examineStatus;//0 待审核 ,1 通过 ,2 成功

    public int getExamineStatus() {
        return examineStatus;
    }

    public void setExamineStatus(int examineStatus) {
        this.examineStatus = examineStatus;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    private String orderNo;

    /**
     * 内容格式
     */
    private Integer contentFormat = CONTENT_FORMAT_TEXT;

    /**
     * 生成时间
     */
    private Long createDate = System.currentTimeMillis();

    public Integer getMessageKind()
    {
        return messageKind;
    }

    public void setMessageKind(Integer messageKind)
    {
        this.messageKind = messageKind;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Integer getContentFormat()
    {
        return contentFormat;
    }

    public void setContentFormat(Integer contentFormat)
    {
        this.contentFormat = contentFormat;
    }

    public Long getCreateDate()
    {
        return createDate;
    }

    public void setCreateDate(Long createDate)
    {
        this.createDate = createDate;
    }

    public String getVoiceContent()
    {
        return voiceContent;
    }

    public void setVoiceContent(String voiceContent)
    {
        this.voiceContent = voiceContent;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public class DataEntity{
        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
