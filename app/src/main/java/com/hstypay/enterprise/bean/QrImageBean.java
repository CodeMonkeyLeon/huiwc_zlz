package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/8/4.
 */

public class QrImageBean implements Serializable {


    /**
     * logId : 8Z3qxGMI
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : [{"userId":3,"merchantId":"683200000001","storeMerchantId":"101300000001","merchantName":"支付接口联调测试数据_门店","pushClose":1},{"userId":3,"merchantId":"683200000001","storeMerchantId":"501300000001","merchantName":"火影忍者之雏田vs小樱","pushClose":1}]
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private String data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
}
