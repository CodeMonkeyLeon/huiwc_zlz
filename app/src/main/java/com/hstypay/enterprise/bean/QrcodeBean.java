package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/17 16:54
 * @描述: ${TODO}
 */

public class QrcodeBean {
    /**
     * data : {"qrcode":"d66507f32e4e4273b8066a2b2a83ffc2","id":1685,"useStatus":0}
     * logId : 1l9X6iJY
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * qrcode : d66507f32e4e4273b8066a2b2a83ffc2
         * id : 1685
         * useStatus : 0    使用状态, 0-未绑定  1-已绑定   2-已失效 3-禁用
         */
        private String qrcode;
        private String id;
        private int useStatus;

        public void setQrcode(String qrcode) {
            this.qrcode = qrcode;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setUseStatus(int useStatus) {
            this.useStatus = useStatus;
        }

        public String getQrcode() {
            return qrcode;
        }

        public String getId() {
            return id;
        }

        public int getUseStatus() {
            return useStatus;
        }
    }
}
