package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/17 16:54
 * @描述: ${TODO}
 */

public class QrcodeSetDetailBean  {
    /**
     * data : {"qrcode":"d66507f32e4e4273b8066a2b2a83ffc2","id":1685,"useStatus":0}
     * logId : 1l9X6iJY
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity implements Serializable {
        /**
         * prompt : 提示词
         * fixedQrCodeSwitchStatus : 是否必填 0:关闭 1:开启
         */
        private String prompt;
        private int fixedQrCodeSwitchStatus;

        public String getPrompt() {
            return prompt;
        }

        public void setPrompt(String prompt) {
            this.prompt = prompt;
        }

        public int getFixedQrCodeSwitchStatus() {
            return fixedQrCodeSwitchStatus;
        }

        public void setFixedQrCodeSwitchStatus(int fixedQrCodeSwitchStatus) {
            this.fixedQrCodeSwitchStatus = fixedQrCodeSwitchStatus;
        }
    }
}
