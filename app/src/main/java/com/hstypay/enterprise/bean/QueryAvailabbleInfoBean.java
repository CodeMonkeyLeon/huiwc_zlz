package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/9/21
 * @desc
 */
public class QueryAvailabbleInfoBean extends BaseBean<List<QueryAvailabbleInfoBean.BindDeviceBean>>{

    public static class BindDeviceBean{
        private String cashierName;//收银员名称/
        private String mobile;//电话号码
        private String physicFlag;//0 没有绑定，1绑定
        private String classId;//设备号, 设备ID（打印机ID）
        private String bindId;//设备编号/二维码编号
        private String qrCodeType;//二维码类型
        private String qrCodeTypeCnt;//二维码类型名称
        private String deviceType;//设备类别 简易POS,PC收银插件、刷脸设备
        private String cashPointName;//收银点名称
        private String sn;

        public String getSn() {
            return sn;
        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public String getCashPointName() {
            return cashPointName;
        }

        public void setCashPointName(String cashPointName) {
            this.cashPointName = cashPointName;
        }

        public String getCashierName() {
            return cashierName;
        }

        public void setCashierName(String cashierName) {
            this.cashierName = cashierName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPhysicFlag() {
            return physicFlag;
        }

        public void setPhysicFlag(String physicFlag) {
            this.physicFlag = physicFlag;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getBindId() {
            return bindId;
        }

        public void setBindId(String bindId) {
            this.bindId = bindId;
        }

        public String getQrCodeType() {
            return qrCodeType;
        }

        public void setQrCodeType(String qrCodeType) {
            this.qrCodeType = qrCodeType;
        }

        public String getQrCodeTypeCnt() {
            return qrCodeTypeCnt;
        }

        public void setQrCodeTypeCnt(String qrCodeTypeCnt) {
            this.qrCodeTypeCnt = qrCodeTypeCnt;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }
    }
}
