package com.hstypay.enterprise.bean;

/**
 * @author zeyu.kuang
 * @time 2020/12/28
 * @desc 电子发票-获取状态
 */
public class QueryMchStatusEleTicketBean extends BaseBean<QueryMchStatusEleTicketBean.MchStatusEleTicket>{

    public class MchStatusEleTicket{
        private String deviceBindStatus;//0未绑定 1绑定
        private String qrcodeBindStatus;//0未绑定 1绑定

        public String getDeviceBindStatus() {

            return deviceBindStatus == null ? "" : deviceBindStatus;

        }

        public void setDeviceBindStatus(String deviceBindStatus) {
            this.deviceBindStatus = deviceBindStatus;
        }

        public String getQrcodeBindStatus() {

            return qrcodeBindStatus == null ? "" : qrcodeBindStatus;

        }

        public void setQrcodeBindStatus(String qrcodeBindStatus) {
            this.qrcodeBindStatus = qrcodeBindStatus;
        }
    }
}
