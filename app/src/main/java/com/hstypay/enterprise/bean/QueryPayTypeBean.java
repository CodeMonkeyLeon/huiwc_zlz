package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/11/23
 * @desc 查询开通的支付方式
 */
public class QueryPayTypeBean extends BaseBean<List<QueryPayTypeBean.Entity>>{
    public class Entity{
        private String id;
        private String merchantId;
        private int apiProvider=-1;//支付方式id
        private String apiProviderCnt;//支付方式cnt

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getApiProviderCnt() {
            return apiProviderCnt;
        }

        public void setApiProviderCnt(String apiProviderCnt) {
            this.apiProviderCnt = apiProviderCnt;
        }
    }
}
