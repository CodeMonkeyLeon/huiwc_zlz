package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/8 11:21
 * @描述: ${TODO}
 */

public class RateBean {
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"date":"2017-10-09 16:06:15","rateList":[{"apiProvider":1,"rate":"0.38%"},{"apiProvider":2,"rate":"0.38%"},{"apiProvider":3,"rate":"0.38%"}]}
     * logId : CW8lRKpS
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * date : 2017-10-09 16:06:15
         * rateList : [{"apiProvider":1,"rate":"0.38%"},{"apiProvider":2,"rate":"0.38%"},{"apiProvider":3,"rate":"0.38%"}]
         */
        private String date;
        private List<RateListEntity> rateList;

        public void setDate(String date) {
            this.date = date;
        }

        public void setRateList(List<RateListEntity> rateList) {
            this.rateList = rateList;
        }

        public String getDate() {
            return date;
        }

        public List<RateListEntity> getRateList() {
            return rateList;
        }

        public class RateListEntity {
            /**
             * apiProvider : 1
             * rate : 0.38%
             */
            private int apiProvider;
            private String rate;
            private String rate2;
            private String rate3;
            private String rate4;

            public void setApiProvider(int apiProvider) {
                this.apiProvider = apiProvider;
            }

            public void setRate(String rate) {
                this.rate = rate;
            }

            public int getApiProvider() {
                return apiProvider;
            }

            public String getRate() {
                return rate;
            }

            public String getRate2() {
                return rate2;
            }

            public void setRate2(String rate2) {
                this.rate2 = rate2;
            }

            public String getRate3() {
                return rate3;
            }

            public void setRate3(String rate3) {
                this.rate3 = rate3;
            }

            public String getRate4() {
                return rate4;
            }

            public void setRate4(String rate4) {
                this.rate4 = rate4;
            }
        }
    }
}
