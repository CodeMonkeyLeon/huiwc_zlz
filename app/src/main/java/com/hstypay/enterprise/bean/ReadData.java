package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/7/27 17:46
 * @描述: ${TODO}
 */

public class ReadData {

    /**
     * data : {"platformCnt":"商户平台","createUserName":"超级管理员","updateTime":"2017-07-27 10:57:20","title":"123123123","readCount":0,"statusCnt":"已发布","userId":0,"content":"<p>3123123<br/><\/p>","platform":2,"createTime":"2017-07-26 17:27:41","createUser":1,"id":21,"status":1}
     * logId : UYcbyWp9
     * status : true
     */

    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * platformCnt : 商户平台
         * createUserName : 超级管理员
         * updateTime : 2017-07-27 10:57:20
         * title : 123123123
         * readCount : 0
         * statusCnt : 已发布
         * userId : 0
         * content : <p>3123123<br/></p>
         * platform : 2
         * createTime : 2017-07-26 17:27:41
         * createUser : 1
         * id : 21
         * status : 1
         */
        private String platformCnt;
        private String createUserName;
        private String updateTime;
        private String title;
        private int readCount;
        private String statusCnt;
        private int userId;
        private String content;
        private int platform;
        private String createTime;
        private int createUser;
        private int id;
        private int status;

        public void setPlatformCnt(String platformCnt) {
            this.platformCnt = platformCnt;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setReadCount(int readCount) {
            this.readCount = readCount;
        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setPlatform(int platform) {
            this.platform = platform;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getPlatformCnt() {
            return platformCnt;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public String getTitle() {
            return title;
        }

        public int getReadCount() {
            return readCount;
        }

        public String getStatusCnt() {
            return statusCnt;
        }

        public int getUserId() {
            return userId;
        }

        public String getContent() {
            return content;
        }

        public int getPlatform() {
            return platform;
        }

        public String getCreateTime() {
            return createTime;
        }

        public int getCreateUser() {
            return createUser;
        }

        public int getId() {
            return id;
        }

        public int getStatus() {
            return status;
        }
    }
}
