package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 18:27
 * @描述: ${TODO}
 */
public class RealNameAuthBean implements Serializable{
   private String payTypeName;
   private String authStatusStr;
   private int authStatus;
   private int apiProvider;
   private int imageRes;

   public RealNameAuthBean(String payTypeName,String authStatusStr,int authStatus,int apiProvider,int imageRes) {
       this.payTypeName = payTypeName;
       this.authStatusStr = authStatusStr;
       this.authStatus = authStatus;
       this.apiProvider = apiProvider;
       this.imageRes = imageRes;
   }

    public String getPayTypeName() {
        return payTypeName;
    }

    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }

    public String getAuthStatusStr() {
        return authStatusStr;
    }

    public void setAuthStatusStr(String authStatusStr) {
        this.authStatusStr = authStatusStr;
    }

    public int getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(int authStatus) {
        this.authStatus = authStatus;
    }

    public int getApiProvider() {
        return apiProvider;
    }

    public void setApiProvider(int apiProvider) {
        this.apiProvider = apiProvider;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }
}
