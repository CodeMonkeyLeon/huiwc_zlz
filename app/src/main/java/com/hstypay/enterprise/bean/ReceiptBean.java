package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 18:27
 * @描述: ${TODO}
 */
public class ReceiptBean implements Serializable {
    /**
     * data : [{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024045","strTradeStatus":"未支付","createTime":"2017-07-26 20:51:57","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024044","strTradeStatus":"未支付","createTime":"2017-07-26 20:47:11","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024043","strTradeStatus":"转入退款","createTime":"2017-07-26 20:13:42","tradeStatus":4,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024042","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:52","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":50,"apiProvider":3,"orderNo":"20170726868320000000100000024041","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:39","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024040","strTradeStatus":"未支付","createTime":"2017-07-26 19:40:47","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024039","strTradeStatus":"未支付","createTime":"2017-07-26 19:17:09","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024038","strTradeStatus":"未支付","createTime":"2017-07-26 19:08:14","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024037","strTradeStatus":"支付成功","createTime":"2017-07-26 18:21:37","tradeStatus":2,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024036","strTradeStatus":"未支付","createTime":"2017-07-26 18:19:00","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024035","strTradeStatus":"未支付","createTime":"2017-07-26 18:17:36","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024034","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:44","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024033","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:06","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024032","strTradeStatus":"未支付","createTime":"2017-07-26 18:15:37","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024031","strTradeStatus":"未支付","createTime":"2017-07-26 18:14:46","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"}]
     * logId : mxfMM2GB
     * status : true
     */
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private ReceiptDetailBean data;
    private String logId;
    private boolean status;

    public void setData(ReceiptDetailBean data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ReceiptDetailBean getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

   /* public class DataEntity implements Serializable {
        private String tokenId;//收款单tokenId
        private String storeMerchantId;//收款门店id
        private String outTradeNo;//商户订单号
        private String storeMerchantName;//收款门店名称
        private String attach;//收款备注
        private String createTime;//创建时间
        private long tradeMoney;//收款金额
        private String opUserId;//收银员id
        private String opUserName;//收银员名称
        private String expireTime;//过期时间
        private int receiptStatus;//收款单状态，1：待对方查看，2：已查看，待支付，3：已关闭，4：已支付
        private String receiptStatusCnt;//收款单状态名称
        private int apiProvider;//支付方式
        private String apiProviderCnt;//支付方式名称
        private String orderNo;//平台订单号
        private String transactionId;//交易订单号
        private String tradeTime;//支付时间
        private String body;//商品描述
        private String appid;
        private String openid;
        private String payUrl;//付款地址

        public String getTokenId() {
            return tokenId;
        }

        public void setTokenId(String tokenId) {
            this.tokenId = tokenId;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getOutTradeNo() {
            return outTradeNo;
        }

        public void setOutTradeNo(String outTradeNo) {
            this.outTradeNo = outTradeNo;
        }

        public String getStoreMerchantName() {
            return storeMerchantName;
        }

        public void setStoreMerchantName(String storeMerchantName) {
            this.storeMerchantName = storeMerchantName;
        }

        public String getAttach() {
            return attach;
        }

        public void setAttach(String attach) {
            this.attach = attach;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public long getTradeMoney() {
            return tradeMoney;
        }

        public void setTradeMoney(long tradeMoney) {
            this.tradeMoney = tradeMoney;
        }

        public String getOpUserId() {
            return opUserId;
        }

        public void setOpUserId(String opUserId) {
            this.opUserId = opUserId;
        }

        public String getOpUserName() {
            return opUserName;
        }

        public void setOpUserName(String opUserName) {
            this.opUserName = opUserName;
        }

        public String getExpireTime() {
            return expireTime;
        }

        public void setExpireTime(String expireTime) {
            this.expireTime = expireTime;
        }

        public int getReceiptStatus() {
            return receiptStatus;
        }

        public void setReceiptStatus(int receiptStatus) {
            this.receiptStatus = receiptStatus;
        }

        public String getReceiptStatusCnt() {
            return receiptStatusCnt;
        }

        public void setReceiptStatusCnt(String receiptStatusCnt) {
            this.receiptStatusCnt = receiptStatusCnt;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getOpenid() {
            return openid;
        }

        public void setOpenid(String openid) {
            this.openid = openid;
        }

        public String getPayUrl() {
            return payUrl;
        }

        public void setPayUrl(String payUrl) {
            this.payUrl = payUrl;
        }

        public String getApiProviderCnt() {
            return apiProviderCnt;
        }

        public void setApiProviderCnt(String apiProviderCnt) {
            this.apiProviderCnt = apiProviderCnt;
        }

        public String getTradeTime() {
            return tradeTime;
        }

        public void setTradeTime(String tradeTime) {
            this.tradeTime = tradeTime;
        }
    }*/
}
