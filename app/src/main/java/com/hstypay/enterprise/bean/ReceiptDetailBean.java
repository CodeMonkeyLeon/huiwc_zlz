package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 18:27
 * @描述: ${TODO}
 */
public class ReceiptDetailBean implements Serializable {
    private String tokenId;//收款单tokenId
    private String storeMerchantId;//收款门店id
    private String outTradeNo;//商户订单号
    private String storeMerchantName;//收款门店名称
    private String attach;//收款备注
    private String createTime;//创建时间
    private long tradeMoney;//收款金额
    private String opUserId;//收银员id
    private String opUserName;//收银员名称
    private String expireTime;//过期时间
    private int receiptStatus;//收款单状态，1：待对方查看，2：已查看，待支付，3：已关闭，4：已支付
    private String receiptStatusCnt;//收款单状态名称
    private int apiProvider;//支付方式
    private String apiProviderCnt;//支付方式名称
    private String orderNo;//平台订单号
    private String transactionId;//交易订单号
    private String tradeTime;//支付时间
    private String body;//商品描述
    private String appid;
    private String openid;
    private String payUrl;//付款地址
    private String updateTime;

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getStoreMerchantId() {
        return storeMerchantId;
    }

    public void setStoreMerchantId(String storeMerchantId) {
        this.storeMerchantId = storeMerchantId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getStoreMerchantName() {
        return storeMerchantName;
    }

    public void setStoreMerchantName(String storeMerchantName) {
        this.storeMerchantName = storeMerchantName;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public long getTradeMoney() {
        return tradeMoney;
    }

    public void setTradeMoney(long tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    public String getOpUserId() {
        return opUserId;
    }

    public void setOpUserId(String opUserId) {
        this.opUserId = opUserId;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public int getReceiptStatus() {
        return receiptStatus;
    }

    public void setReceiptStatus(int receiptStatus) {
        this.receiptStatus = receiptStatus;
    }

    public String getReceiptStatusCnt() {
        return receiptStatusCnt;
    }

    public void setReceiptStatusCnt(String receiptStatusCnt) {
        this.receiptStatusCnt = receiptStatusCnt;
    }

    public int getApiProvider() {
        return apiProvider;
    }

    public void setApiProvider(int apiProvider) {
        this.apiProvider = apiProvider;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPayUrl() {
        return payUrl;
    }

    public void setPayUrl(String payUrl) {
        this.payUrl = payUrl;
    }

    public String getApiProviderCnt() {
        return apiProviderCnt;
    }

    public void setApiProviderCnt(String apiProviderCnt) {
        this.apiProviderCnt = apiProviderCnt;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }
}
