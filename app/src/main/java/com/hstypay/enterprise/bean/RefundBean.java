package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * Created by admin on 2017/7/30.
 */

public class RefundBean implements Serializable {
    /**
     * 退款金额
     */
    private long refundMoney;

    /**
     * 退款状态，1:初始化;2:退款成功;3:退款失败;4:未确定;5:转入代发;6:退款处理中
     */
    private int refundStatus;

    /**
     * 退款原因描述
     */
    private String ptRefundReason;

    /**
     * 平台审核状态 1：待审核，2：审核通过，3：退回
     * 余额不足时为待审核状态
     * */
    private int ptAuditStatus;

    /**
     * 退款完成时间
     */
    private String refundTime;

    /**
     * 退款发起人
     */
    private String refundUser;

    /**
     * 退款发起人
     */
    private String refundUserCnt;
    /**
     * 退款单号
     */
    private String refundNo;
    /**
     * 退款发起时间
     */
    private String createTime;
    /**
     * 门店名称
     */
    private String storeMerchantIdCnt;
    /**
     * 提供给上游渠道的退款单号
     */
    private String reqRefundNo;
    /**
     * 退款备注
     */
    private String standby4;
    /**
     * 设备号
     */
    private String deviceInfo;
    /**
     * 设备sn
     */
    private String termNo;

    public String getTermNo() {
        return termNo;
    }

    public void setTermNo(String termNo) {
        this.termNo = termNo;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public int getPtAuditStatus() {
        return ptAuditStatus;
    }

    public void setPtAuditStatus(int ptAuditStatus) {
        this.ptAuditStatus = ptAuditStatus;
    }

    public String getPtRefundReason() {
        return ptRefundReason;
    }

    public void setPtRefundReason(String ptRefundReason) {
        this.ptRefundReason = ptRefundReason;
    }

    public String getStandby4() {
        return standby4;
    }

    public void setStandby4(String standby4) {
        this.standby4 = standby4;
    }

    public String getReqRefundNo() {
        return reqRefundNo;
    }

    public void setReqRefundNo(String reqRefundNo) {
        this.reqRefundNo = reqRefundNo;
    }

    public long getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(long refundMoney) {
        this.refundMoney = refundMoney;
    }

    public int getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(int refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getRefundUser() {
        return refundUser;
    }

    public void setRefundUser(String refundUser) {
        this.refundUser = refundUser;
    }

    public String getRefundUserCnt() {
        return refundUserCnt;
    }

    public void setRefundUserCnt(String refundUserCnt) {
        this.refundUserCnt = refundUserCnt;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public String getStoreMerchantIdCnt() {
        return storeMerchantIdCnt;
    }

    public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
        this.storeMerchantIdCnt = storeMerchantIdCnt;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String refundUpdateTime) {
        this.createTime = refundUpdateTime;
    }
}
