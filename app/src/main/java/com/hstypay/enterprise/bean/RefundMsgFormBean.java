package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * @author zeyu.kuang
 * @time 2020/9/22
 * @desc 退款打印信息
 */
public class RefundMsgFormBean implements Serializable {
    private String merchantName;
    private String orderNo;
    private String stubName;
    private String merchantId;
    private String storeName;
    private String tradeType;
    private String opUserName;
    private String refundApplyTimeStr;
    private String refundStatus;
    private String refundAmount;
    private String printTimeStr;
    private int printMode;//1：自动、2：手动

    public int getPrintMode() {
        return printMode;
    }

    public void setPrintMode(int printMode) {
        this.printMode = printMode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getStubName() {
        return stubName;
    }

    public void setStubName(String stubName) {
        this.stubName = stubName;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public String getRefundApplyTimeStr() {
        return refundApplyTimeStr;
    }

    public void setRefundApplyTimeStr(String refundApplyTimeStr) {
        this.refundApplyTimeStr = refundApplyTimeStr;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getPrintTimeStr() {
        return printTimeStr;
    }

    public void setPrintTimeStr(String printTimeStr) {
        this.printTimeStr = printTimeStr;
    }
}
