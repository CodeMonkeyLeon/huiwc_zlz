package com.hstypay.enterprise.bean;

import android.support.annotation.NonNull;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.UIUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/2 21:26
 * @描述: ${TODO}
 */

public class ReportBean implements Serializable{
    /**
     * data : {"income":9,"refundFee":30,"refundCount":10,"successFee":39,"successCount":13,"list":[{"apiProvider":1,"successFee":10,"successCount":3},{"successFee":2,"successCount":1},{"successFee":2,"successCount":1},{"successFee":2,"successCount":1},{"successFee":5,"successCount":2},{"successFee":27,"successCount":9}]}
     * logId : Cn15nv8s
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity implements Serializable{
        /**
         * income : 9
         * refundFee : 30
         * refundCount : 10
         * successFee : 39
         * successCount : 13
         * list : [{"apiProvider":1,"successFee":10,"successCount":3},{"successFee":2,"successCount":1},{"successFee":2,"successCount":1},{"successFee":2,"successCount":1},{"successFee":5,"successCount":2},{"successFee":27,"successCount":9}]
         */
        private int type;//1收款 2会员消费 3饭卡消费
        private long income;//交易净额
        private long refundFee;//退款金额
        private long refundCount;//退款笔数
        private long successFee;//交易金额
        private long successCount;//交易笔数
        private long customerCount;//顾客人数
        private long numberPaymentCount;//次卡核销次数
        private long rechargeAmount;//充值金额
        private long rechargeNumber;//充值笔数
        private List<ListEntity> list;
        private String cashierName = UIUtils.getString(R.string.tv_all_user);
        private String storeName = "全部门店";
        private String cashierPointName = "全部收银点";
        private String startTime;
        private String endTime;
        private String printTime;
        private String storeMerchantId;
        private long mchDiscountsFee;//商家优惠金额
        private long mchDiscountsCount;//商家优惠笔数
        private long payFee;//实收金额、核销金额
        private long commissionFee;//手续费
        private long settlementFee;//结算金额
        private long fzSuccessFee;//服务费
        private String settleRate;//结算率,该门店对应的饭卡结算率。若该商户有多门店，且每个门店结算率不一样，则汇总数据处的结算率显示为“--”
        private double test;

        public long getCustomerCount() {

            return customerCount;

        }

        public void setCustomerCount(long customerCount) {
            this.customerCount = customerCount;
        }

        public double getTest() {
            return test;
        }

        public void setTest(double test) {
            this.test = test;
        }

        public String getSettleRate() {
            return settleRate;
        }

        public void setSettleRate(String settleRate) {
            this.settleRate = settleRate;
        }

        public long getFzSuccessFee() {
            return fzSuccessFee;
        }

        public void setFzSuccessFee(long fzSuccessFee) {
            this.fzSuccessFee = fzSuccessFee;
        }

        public long getCommissionFee() {
            return commissionFee;
        }

        public void setCommissionFee(long commissionFee) {
            this.commissionFee = commissionFee;
        }

        public long getSettlementFee() {
            return settlementFee;
        }

        public void setSettlementFee(long settlementFee) {
            this.settlementFee = settlementFee;
        }

        public String getCashierPointName() {
            return cashierPointName;
        }

        public void setCashierPointName(String cashierPointName) {
            this.cashierPointName = cashierPointName;
        }

        public long getMchDiscountsFee() {
            return mchDiscountsFee;
        }

        public void setMchDiscountsFee(long mchDiscountsFee) {
            this.mchDiscountsFee = mchDiscountsFee;
        }

        public long getMchDiscountsCount() {
            return mchDiscountsCount;
        }

        public void setMchDiscountsCount(long mchDiscountsCount) {
            this.mchDiscountsCount = mchDiscountsCount;
        }

        public long getPayFee() {
            return payFee;
        }

        public void setPayFee(long payFee) {
            this.payFee = payFee;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public long getNumberPaymentCount() {
            return numberPaymentCount;
        }

        public void setNumberPaymentCount(long numberPaymentCount) {
            this.numberPaymentCount = numberPaymentCount;
        }

        public long getRechargeAmount() {
            return rechargeAmount;
        }

        public void setRechargeAmount(long rechargeAmount) {
            this.rechargeAmount = rechargeAmount;
        }

        public long getRechargeNumber() {
            return rechargeNumber;
        }

        public void setRechargeNumber(long rechargeNumber) {
            this.rechargeNumber = rechargeNumber;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getCashierName() {
            return cashierName;
        }

        public void setCashierName(String cashierName) {
            this.cashierName = cashierName;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getPrintTime() {
            return printTime;
        }

        public void setPrintTime(String printTime) {
            this.printTime = printTime;
        }

        public void setIncome(long income) {
            this.income = income;
        }

        public void setRefundFee(long refundFee) {
            this.refundFee = refundFee;
        }

        public void setRefundCount(long refundCount) {
            this.refundCount = refundCount;
        }

        public void setSuccessFee(long successFee) {
            this.successFee = successFee;
        }

        public void setSuccessCount(long successCount) {
            this.successCount = successCount;
        }

        public void setList(List<ListEntity> list) {
            this.list = list;
        }

        public long getIncome() {
            return income;
        }

        public long getRefundFee() {
            return refundFee;
        }

        public long getRefundCount() {
            return refundCount;
        }

        public long getSuccessFee() {
            return successFee;
        }

        public long getSuccessCount() {
            return successCount;
        }

        public List<ListEntity> getList() {
            return list;
        }

        public class ListEntity implements Serializable,Comparable<ListEntity>{
            /**
             * apiProvider : 1
             * successFee : 10
             * successCount : 3
             */
            private int apiProvider;
            private long successFee;
            private int successCount;

            public void setApiProvider(int apiProvider) {
                this.apiProvider = apiProvider;
            }

            public void setSuccessFee(long successFee) {
                this.successFee = successFee;
            }

            public void setSuccessCount(int successCount) {
                this.successCount = successCount;
            }

            public int getApiProvider() {
                return apiProvider;
            }

            public long getSuccessFee() {
                return successFee;
            }

            public int getSuccessCount() {
                return successCount;
            }

            @Override
            public int compareTo(@NonNull ListEntity o) {
                if(this.successFee >= o.getSuccessFee()){
                    return -1;
                }
                if(this.successFee == o.getSuccessFee()){
                    return 0;
                }
                return 1;
            }
        }
    }
}
