package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/23 17:43
 * @描述: ${报表数据概览}
 */
public class ReportDataView {
    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }
        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * logId : 79943d6ca16940c690eaba8e6413a593
     * status : true
     * data : {"merchantId":"735200000001","startTime":"2020-05-20 00:00:00","endTime":"2020-05-20 00:00:00","netFee":0,"successFee":0,"successCount":0,"refundFee":0,"refundCount":0,"eachSuccessFee":0,"mchDiscountsFee":0,"actualFee":0,"settlementFee":0,"netFeeLinkRelative":0,"actualFeeLinkRelative":0,"settlementFeeLinkRelative":0,"successFeeLinkRelative":0,"successCountLinkRelative":0,"refundFeeLinkRelative":0,"refundCountLinkRelative":0,"eachSuccessFeeLinkRelative":0,"commissionFeeLinkRelative":0,"\u201cmchDiscountsFeeLinkRelative\u201d":0,"payTradeTime":"2020-05-20","fqCommissionFee":0,"commissionFee":0,"fzFee":0}
     */
    public static class DataBean {
        /**
         * merchantId : 735200000001
         * startTime : 2020-05-20 00:00:00
         * endTime : 2020-05-20 00:00:00
         * netFee : 0
         * successFee : 0
         * successCount : 0
         * refundFee : 0
         * refundCount : 0
         * eachSuccessFee : 0
         * mchDiscountsFee : 0
         * actualFee : 0
         * settlementFee : 0
         * netFeeLinkRelative : 0
         * actualFeeLinkRelative : 0
         * settlementFeeLinkRelative : 0
         * successFeeLinkRelative : 0
         * successCountLinkRelative : 0
         * refundFeeLinkRelative : 0
         * refundCountLinkRelative : 0
         * eachSuccessFeeLinkRelative : 0
         * commissionFeeLinkRelative : 0
         * “mchDiscountsFeeLinkRelative” : 0
         * payTradeTime : 2020-05-20
         * fqCommissionFee : 0
         * commissionFee : 0
         * fzFee : 0
         */

        private String merchantId;
        private String startTime;
        private String endTime;
        private long netFee;
        private long successFee;
        private long successCount;
        private long refundFee;
        private long refundCount;
        private long eachSuccessFee;
        private long mchDiscountsFee;
        private long actualFee;
        private long settlementFee;
        private double netFeeLinkRelative;
        private double actualFeeLinkRelative;
        private double settlementFeeLinkRelative;
        private double successFeeLinkRelative;
        private double successCountLinkRelative;
        private double refundFeeLinkRelative;
        private double refundCountLinkRelative;
        private double eachSuccessFeeLinkRelative;
        private double commissionFeeLinkRelative;
        private double mchDiscountsFeeLinkRelative;
        private String payTradeTime;
        private long fqCommissionFee;
        private long commissionFee;//手续费
        private long fzFee;//服务费

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public long getNetFee() {
            return netFee;
        }

        public void setNetFee(long netFee) {
            this.netFee = netFee;
        }

        public long getSuccessFee() {
            return successFee;
        }

        public void setSuccessFee(long successFee) {
            this.successFee = successFee;
        }

        public long getSuccessCount() {
            return successCount;
        }

        public void setSuccessCount(long successCount) {
            this.successCount = successCount;
        }

        public long getRefundFee() {
            return refundFee;
        }

        public void setRefundFee(long refundFee) {
            this.refundFee = refundFee;
        }

        public long getRefundCount() {
            return refundCount;
        }

        public void setRefundCount(long refundCount) {
            this.refundCount = refundCount;
        }

        public long getEachSuccessFee() {
            return eachSuccessFee;
        }

        public void setEachSuccessFee(long eachSuccessFee) {
            this.eachSuccessFee = eachSuccessFee;
        }

        public long getMchDiscountsFee() {
            return mchDiscountsFee;
        }

        public void setMchDiscountsFee(long mchDiscountsFee) {
            this.mchDiscountsFee = mchDiscountsFee;
        }

        public long getActualFee() {
            return actualFee;
        }

        public void setActualFee(long actualFee) {
            this.actualFee = actualFee;
        }

        public long getSettlementFee() {
            return settlementFee;
        }

        public void setSettlementFee(long settlementFee) {
            this.settlementFee = settlementFee;
        }

        public double getNetFeeLinkRelative() {
            return netFeeLinkRelative;
        }

        public void setNetFeeLinkRelative(double netFeeLinkRelative) {
            this.netFeeLinkRelative = netFeeLinkRelative;
        }

        public double getActualFeeLinkRelative() {
            return actualFeeLinkRelative;
        }

        public void setActualFeeLinkRelative(double actualFeeLinkRelative) {
            this.actualFeeLinkRelative = actualFeeLinkRelative;
        }

        public double getSettlementFeeLinkRelative() {
            return settlementFeeLinkRelative;
        }

        public void setSettlementFeeLinkRelative(double settlementFeeLinkRelative) {
            this.settlementFeeLinkRelative = settlementFeeLinkRelative;
        }

        public double getSuccessFeeLinkRelative() {
            return successFeeLinkRelative;
        }

        public void setSuccessFeeLinkRelative(double successFeeLinkRelative) {
            this.successFeeLinkRelative = successFeeLinkRelative;
        }

        public double getSuccessCountLinkRelative() {
            return successCountLinkRelative;
        }

        public void setSuccessCountLinkRelative(double successCountLinkRelative) {
            this.successCountLinkRelative = successCountLinkRelative;
        }

        public double getRefundFeeLinkRelative() {
            return refundFeeLinkRelative;
        }

        public void setRefundFeeLinkRelative(double refundFeeLinkRelative) {
            this.refundFeeLinkRelative = refundFeeLinkRelative;
        }

        public double getRefundCountLinkRelative() {
            return refundCountLinkRelative;
        }

        public void setRefundCountLinkRelative(double refundCountLinkRelative) {
            this.refundCountLinkRelative = refundCountLinkRelative;
        }

        public double getEachSuccessFeeLinkRelative() {
            return eachSuccessFeeLinkRelative;
        }

        public void setEachSuccessFeeLinkRelative(double eachSuccessFeeLinkRelative) {
            this.eachSuccessFeeLinkRelative = eachSuccessFeeLinkRelative;
        }

        public double getCommissionFeeLinkRelative() {
            return commissionFeeLinkRelative;
        }

        public void setCommissionFeeLinkRelative(double commissionFeeLinkRelative) {
            this.commissionFeeLinkRelative = commissionFeeLinkRelative;
        }

        public double getMchDiscountsFeeLinkRelative() {
            return mchDiscountsFeeLinkRelative;
        }

        public void setMchDiscountsFeeLinkRelative(long mchDiscountsFeeLinkRelative) {
            this.mchDiscountsFeeLinkRelative = mchDiscountsFeeLinkRelative;
        }

        public String getPayTradeTime() {
            return payTradeTime;
        }

        public void setPayTradeTime(String payTradeTime) {
            this.payTradeTime = payTradeTime;
        }

        public long getFqCommissionFee() {
            return fqCommissionFee;
        }

        public void setFqCommissionFee(long fqCommissionFee) {
            this.fqCommissionFee = fqCommissionFee;
        }

        public long getCommissionFee() {
            return commissionFee;
        }

        public void setCommissionFee(long commissionFee) {
            this.commissionFee = commissionFee;
        }

        public long getFzFee() {
            return fzFee;
        }

        public void setFzFee(long fzFee) {
            this.fzFee = fzFee;
        }
    }
}
