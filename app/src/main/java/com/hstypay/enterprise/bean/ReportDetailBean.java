package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/5/12 15:30
 * @描述: ${TODO}
 */

public class ReportDetailBean {
    /** 总手续费*/
    private long commissionFee;
    /** 交易金额 */
    private long successFee;
    /** 退款金额 */
    private long refundFee;
    /** 退款笔数 */
    private long refundCount;
    /** 净金额 */
    private long payNetFee;
    /** 实收金额 */
    private long payFee;
    /** 优惠金额 */
    private long thirdCouponSuccessFee;
    /** 成功笔数 */
    private long successCount;
    /** 商家优惠金额 */
    private long mchDiscountsFee;
    /** 商家优惠笔数 */
    private long mchDiscountsCount;

    private String startTime;
    private String endTime;
    private String payTradeTime;
    private String payTradeTimeFmt;
    private String costRateStr;
    private String benefitRateStr;
    private String billRateStr;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public long getPayFee() {
        return payFee;
    }

    public void setPayFee(long payFee) {
        this.payFee = payFee;
    }

    public long getMchDiscountsFee() {
        return mchDiscountsFee;
    }

    public void setMchDiscountsFee(long mchDiscountsFee) {
        this.mchDiscountsFee = mchDiscountsFee;
    }

    public long getMchDiscountsCount() {
        return mchDiscountsCount;
    }

    public void setMchDiscountsCount(long mchDiscountsCount) {
        this.mchDiscountsCount = mchDiscountsCount;
    }

    public long getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(long commissionFee) {
        this.commissionFee = commissionFee;
    }

    public long getSuccessFee() {
        return successFee;
    }

    public void setSuccessFee(long successFee) {
        this.successFee = successFee;
    }

    public long getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(long refundFee) {
        this.refundFee = refundFee;
    }

    public long getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(long refundCount) {
        this.refundCount = refundCount;
    }

    public long getPayNetFee() {
        return payNetFee;
    }

    public void setPayNetFee(long payNetFee) {
        this.payNetFee = payNetFee;
    }

    public long getThirdCouponSuccessFee() {
        return thirdCouponSuccessFee;
    }

    public void setThirdCouponSuccessFee(long thirdCouponSuccessFee) {
        this.thirdCouponSuccessFee = thirdCouponSuccessFee;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public String getPayTradeTime() {
        return payTradeTime;
    }

    public void setPayTradeTime(String payTradeTime) {
        this.payTradeTime = payTradeTime;
    }

    public String getPayTradeTimeFmt() {
        return payTradeTimeFmt;
    }

    public void setPayTradeTimeFmt(String payTradeTimeFmt) {
        this.payTradeTimeFmt = payTradeTimeFmt;
    }

    public String getCostRateStr() {
        return costRateStr;
    }

    public void setCostRateStr(String costRateStr) {
        this.costRateStr = costRateStr;
    }

    public String getBenefitRateStr() {
        return benefitRateStr;
    }

    public void setBenefitRateStr(String benefitRateStr) {
        this.benefitRateStr = benefitRateStr;
    }

    public String getBillRateStr() {
        return billRateStr;
    }

    public void setBillRateStr(String billRateStr) {
        this.billRateStr = billRateStr;
    }
}
