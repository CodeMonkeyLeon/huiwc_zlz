package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/5/12 13:52
 * @描述: ${TODO}
 */

public class ReportIncomeBean {
    /**
     * data : {"income":9,"refundFee":30,"refundCount":10,"successFee":39,"successCount":13,"list":[{"apiProvider":1,"successFee":10,"successCount":3},{"successFee":2,"successCount":1},{"successFee":2,"successCount":1},{"successFee":2,"successCount":1},{"successFee":5,"successCount":2},{"successFee":27,"successCount":9}]}
     * logId : Cn15nv8s
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"refundFee":0,"payTradeTimeFmt":"2018-05-12","successFeeYuan":315264.43,"benefitTotalFeeYuan":0,"payNetFee":31526443,"commissionFeeYuan":0,"costRateStr":"","successFee":31526443,"benefitRateStr":"0.0\u2030","totalFeeYuan":0,"commissionFeeSix":0,"billRateStr":"0.0\u2030","payRemitFeeYuan":0,"payNetFeeYuan":3.1526443E7,"salesmanTotalFeeYuan":0,"refundFeeYuan":0,"rankNum":0,"salesmanTotalFee":0}
     * logId : vXNRS4ZV
     * status : true
     */
    private ReportDetailBean data;
    private String logId;
    private boolean status;

    public void setData(ReportDetailBean data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ReportDetailBean getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * refundFee : 0
         * payTradeTimeFmt : 2018-05-12
         * successFeeYuan : 315264.43
         * benefitTotalFeeYuan : 0.0
         * payNetFee : 31526443
         * commissionFeeYuan : 0
         * costRateStr :
         * successFee : 31526443
         * benefitRateStr : 0.0‰
         * totalFeeYuan : 0.0
         * commissionFeeSix : 0
         * billRateStr : 0.0‰
         * payRemitFeeYuan : 0
         * payNetFeeYuan : 3.1526443E7
         * salesmanTotalFeeYuan : 0
         * refundFeeYuan : 0.0
         * rankNum : 0
         * salesmanTotalFee : 0
         */

        /** 总手续费*/
        //private double commissionFee;
        /** 交易金额 */
        //private long successFee;
        /** 退款金额 */
        //private long refundFee;
        /** 退款笔数 */
        //private long refundCount;
        /** 净金额 */
        //private long payNetFee;
        /** 优惠金额 */
        //private long thirdCouponSuccessFee;
        /** 成功笔数 */
        //private long successCount;

        /*private String payTradeTimeFmt;
        private String costRateStr;
        private String benefitRateStr;
        private String billRateStr;*/

        /*private double successFeeYuan;
        private double benefitTotalFeeYuan;
        private double commissionFeeYuan;
        private double totalFeeYuan;
        private int commissionFeeSix;
        private double payRemitFeeYuan;
        private double payNetFeeYuan;
        private double salesmanTotalFeeYuan;
        private double refundFeeYuan;
        private int rankNum;
        private long salesmanTotalFee;*/

        /*public double getCommissionFee() {
            return commissionFee;
        }

        public void setCommissionFee(double commissionFee) {
            this.commissionFee = commissionFee;
        }

        public long getRefundCount() {
            return refundCount;
        }

        public void setRefundCount(long refundCount) {
            this.refundCount = refundCount;
        }

        public long getThirdCouponSuccessFee() {
            return thirdCouponSuccessFee;
        }

        public void setThirdCouponSuccessFee(long thirdCouponSuccessFee) {
            this.thirdCouponSuccessFee = thirdCouponSuccessFee;
        }

        public long getSuccessCount() {
            return successCount;
        }


        public long getRefundFee() {
            return refundFee;
        }

        public String getPayTradeTimeFmt() {
            return payTradeTimeFmt;
        }

        public void setSuccessCount(long successCount) {
            this.successCount = successCount;
        }

        public void setRefundFee(long refundFee) {
            this.refundFee = refundFee;
        }

        public void setPayTradeTimeFmt(String payTradeTimeFmt) {
            this.payTradeTimeFmt = payTradeTimeFmt;
        }

        public void setPayNetFee(long payNetFee) {
            this.payNetFee = payNetFee;
        }

        public void setCostRateStr(String costRateStr) {
            this.costRateStr = costRateStr;
        }

        public void setSuccessFee(long successFee) {
            this.successFee = successFee;
        }

        public void setBenefitRateStr(String benefitRateStr) {
            this.benefitRateStr = benefitRateStr;
        }

        public void setBillRateStr(String billRateStr) {
            this.billRateStr = billRateStr;
        }


        public long getPayNetFee() {
            return payNetFee;
        }

        public String getCostRateStr() {
            return costRateStr;
        }

        public long getSuccessFee() {
            return successFee;
        }

        public String getBenefitRateStr() {
            return benefitRateStr;
        }

        public String getBillRateStr() {
            return billRateStr;
        }*/

        /*public void setTotalFeeYuan(double totalFeeYuan) {
            this.totalFeeYuan = totalFeeYuan;
        }

        public void setCommissionFeeSix(int commissionFeeSix) {
            this.commissionFeeSix = commissionFeeSix;
        }

        public void setPayRemitFeeYuan(double payRemitFeeYuan) {
            this.payRemitFeeYuan = payRemitFeeYuan;
        }

        public void setPayNetFeeYuan(double payNetFeeYuan) {
            this.payNetFeeYuan = payNetFeeYuan;
        }

        public void setSalesmanTotalFeeYuan(double salesmanTotalFeeYuan) {
            this.salesmanTotalFeeYuan = salesmanTotalFeeYuan;
        }

        public void setRefundFeeYuan(double refundFeeYuan) {
            this.refundFeeYuan = refundFeeYuan;
        }

        public void setRankNum(int rankNum) {
            this.rankNum = rankNum;
        }

        public void setSalesmanTotalFee(long salesmanTotalFee) {
            this.salesmanTotalFee = salesmanTotalFee;
        }

        public void setSuccessFeeYuan(double successFeeYuan) {
            this.successFeeYuan = successFeeYuan;
        }

        public void setBenefitTotalFeeYuan(double benefitTotalFeeYuan) {
            this.benefitTotalFeeYuan = benefitTotalFeeYuan;
        }

        public void setCommissionFeeYuan(double commissionFeeYuan) {
            this.commissionFeeYuan = commissionFeeYuan;
        }

        public double getSuccessFeeYuan() {
            return successFeeYuan;
        }

        public double getBenefitTotalFeeYuan() {
            return benefitTotalFeeYuan;
        }

        public double getCommissionFeeYuan() {
            return commissionFeeYuan;
        }

        public double getTotalFeeYuan() {
            return totalFeeYuan;
        }

        public int getCommissionFeeSix() {
            return commissionFeeSix;
        }

        public double getPayRemitFeeYuan() {
            return payRemitFeeYuan;
        }

        public double getPayNetFeeYuan() {
            return payNetFeeYuan;
        }

        public double getSalesmanTotalFeeYuan() {
            return salesmanTotalFeeYuan;
        }

        public double getRefundFeeYuan() {
            return refundFeeYuan;
        }

        public int getRankNum() {
            return rankNum;
        }

        public long getSalesmanTotalFee() {
            return salesmanTotalFee;
        }*/
    }
}
