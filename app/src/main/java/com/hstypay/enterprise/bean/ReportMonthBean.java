package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/5/12 14:01
 * @描述: ${TODO}
 */

public class ReportMonthBean {
    /**
     * data : [{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024045","strTradeStatus":"未支付","createTime":"2017-07-26 20:51:57","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024044","strTradeStatus":"未支付","createTime":"2017-07-26 20:47:11","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024043","strTradeStatus":"转入退款","createTime":"2017-07-26 20:13:42","tradeStatus":4,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024042","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:52","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":50,"apiProvider":3,"orderNo":"20170726868320000000100000024041","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:39","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024040","strTradeStatus":"未支付","createTime":"2017-07-26 19:40:47","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024039","strTradeStatus":"未支付","createTime":"2017-07-26 19:17:09","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024038","strTradeStatus":"未支付","createTime":"2017-07-26 19:08:14","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024037","strTradeStatus":"支付成功","createTime":"2017-07-26 18:21:37","tradeStatus":2,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024036","strTradeStatus":"未支付","createTime":"2017-07-26 18:19:00","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024035","strTradeStatus":"未支付","createTime":"2017-07-26 18:17:36","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024034","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:44","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024033","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:06","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024032","strTradeStatus":"未支付","createTime":"2017-07-26 18:15:37","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024031","strTradeStatus":"未支付","createTime":"2017-07-26 18:14:46","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"}]
     * logId : mxfMM2GB
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : [{"payTradeTime":"2018-04-01 00:00:00","refundFee":0,"payTradeTimeFmt":"2018-04-01","successFeeYuan":315264.43,"payNetFee":31526443,"commissionFeeYuan":0,"successFee":31526443,"totalFeeYuan":0,"commissionFeeSix":0,"payRemitFeeYuan":0,"payNetFeeYuan":3.1526443E7,"totalIncome":0,"refundFeeYuan":0}]
     * logId : Y0ReoNtW
     * status : true
     */
    private List<ReportDetailBean> data;
    private String logId;
    private boolean status;

    public void setData(List<ReportDetailBean> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<ReportDetailBean> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    //public class DataEntity {
        /**
         * payTradeTime : 2018-04-01 00:00:00
         * refundFee : 0
         * payTradeTimeFmt : 2018-04-01
         * successFeeYuan : 315264.43
         * payNetFee : 31526443
         * commissionFeeYuan : 0
         * successFee : 31526443
         * totalFeeYuan : 0.0
         * commissionFeeSix : 0
         * payRemitFeeYuan : 0
         * payNetFeeYuan : 3.1526443E7
         * totalIncome : 0
         * refundFeeYuan : 0.0
         */

        /** 交易金额 */
        //private long successFee;
        /** 退款金额 */
        //private long refundFee;
        /** 净金额 */
        //private long payNetFee;

        //private String payTradeTime;
        //private String payTradeTimeFmt;

        /*private double successFeeYuan;
        private int commissionFeeYuan;
        private double totalFeeYuan;
        private int commissionFeeSix;
        private int payRemitFeeYuan;
        private double payNetFeeYuan;
        private int totalIncome;
        private double refundFeeYuan;*/

        /*public void setPayTradeTime(String payTradeTime) {
            this.payTradeTime = payTradeTime;
        }

        public void setRefundFee(long refundFee) {
            this.refundFee = refundFee;
        }

        public void setPayTradeTimeFmt(String payTradeTimeFmt) {
            this.payTradeTimeFmt = payTradeTimeFmt;
        }

        public void setPayNetFee(long payNetFee) {
            this.payNetFee = payNetFee;
        }

        public void setSuccessFee(long successFee) {
            this.successFee = successFee;
        }

        public String getPayTradeTime() {
            return payTradeTime;
        }

        public long getRefundFee() {
            return refundFee;
        }

        public String getPayTradeTimeFmt() {
            return payTradeTimeFmt;
        }


        public long getPayNetFee() {
            return payNetFee;
        }

        public long getSuccessFee() {
            return successFee;
        }*/

        /*public void setSuccessFeeYuan(double successFeeYuan) {
            this.successFeeYuan = successFeeYuan;
        }

        public void setCommissionFeeYuan(int commissionFeeYuan) {
            this.commissionFeeYuan = commissionFeeYuan;
        }

        public void setTotalFeeYuan(double totalFeeYuan) {
            this.totalFeeYuan = totalFeeYuan;
        }

        public void setCommissionFeeSix(int commissionFeeSix) {
            this.commissionFeeSix = commissionFeeSix;
        }

        public void setPayRemitFeeYuan(int payRemitFeeYuan) {
            this.payRemitFeeYuan = payRemitFeeYuan;
        }

        public void setPayNetFeeYuan(double payNetFeeYuan) {
            this.payNetFeeYuan = payNetFeeYuan;
        }

        public void setTotalIncome(int totalIncome) {
            this.totalIncome = totalIncome;
        }

        public void setRefundFeeYuan(double refundFeeYuan) {
            this.refundFeeYuan = refundFeeYuan;
        }

        public double getSuccessFeeYuan() {
            return successFeeYuan;
        }

        public int getCommissionFeeYuan() {
            return commissionFeeYuan;
        }


        public double getTotalFeeYuan() {
            return totalFeeYuan;
        }

        public int getCommissionFeeSix() {
            return commissionFeeSix;
        }

        public int getPayRemitFeeYuan() {
            return payRemitFeeYuan;
        }

        public double getPayNetFeeYuan() {
            return payNetFeeYuan;
        }

        public int getTotalIncome() {
            return totalIncome;
        }

        public double getRefundFeeYuan() {
            return refundFeeYuan;
        }*/
    //}
}
