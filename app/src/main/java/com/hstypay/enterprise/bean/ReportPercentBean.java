package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/23 16:35
 * @描述: ${报表百分比}
 */
public class ReportPercentBean {
    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }
        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    public static class DataBean {
        /**
         * merchantId : 735200000001
         * startTime : 2020-04-01 00:00:00
         * endTime : 2020-04-30 00:00:00
         * successFee : 3507000
         * successCount : 1012
         * apiProvider : 1
         * apiProviderCnt : 微信
         * amountRelativePercentage : 94.8
         * countRelativePercentage : 93.01
         */

        private String merchantId;
        private String startTime;
        private String endTime;
        private int successFee;
        private int successCount;
        private int apiProvider;
        private String apiProviderCnt;
        private double amountRelativePercentage;
        private double countRelativePercentage;
        private String termType;
        private String termTypeCnt;

        public String getTermType() {
            return termType;
        }

        public void setTermType(String termType) {
            this.termType = termType;
        }

        public String getTermTypeCnt() {
            return termTypeCnt;
        }

        public void setTermTypeCnt(String termTypeCnt) {
            this.termTypeCnt = termTypeCnt;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public int getSuccessFee() {
            return successFee;
        }

        public void setSuccessFee(int successFee) {
            this.successFee = successFee;
        }

        public int getSuccessCount() {
            return successCount;
        }

        public void setSuccessCount(int successCount) {
            this.successCount = successCount;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public String getApiProviderCnt() {
            return apiProviderCnt;
        }

        public void setApiProviderCnt(String apiProviderCnt) {
            this.apiProviderCnt = apiProviderCnt;
        }

        public double getAmountRelativePercentage() {
            return amountRelativePercentage;
        }

        public void setAmountRelativePercentage(double amountRelativePercentage) {
            this.amountRelativePercentage = amountRelativePercentage;
        }

        public double getCountRelativePercentage() {
            return countRelativePercentage;
        }

        public void setCountRelativePercentage(double countRelativePercentage) {
            this.countRelativePercentage = countRelativePercentage;
        }
    }
}
