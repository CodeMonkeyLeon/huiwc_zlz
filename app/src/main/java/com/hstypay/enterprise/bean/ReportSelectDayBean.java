package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/23 16:35
 * @描述: ${报表选择日期(天)}
 */
public class ReportSelectDayBean implements Serializable {
    private String logId;
    private boolean status;
    private ErrorBean error;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }
        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    /**
     * logId : bb7a279545154112bd6b18d3f20510ad
     * status : true
     * data : [{"year":"2020","month":"2020-05","days":[{"netFee":0,"payTradeTime":"2020-05-11","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":1},{"netFee":0,"payTradeTime":"2020-05-12","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":2},{"netFee":0,"payTradeTime":"2020-05-13","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":3},{"netFee":0,"payTradeTime":"2020-05-14","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":4},{"netFee":0,"payTradeTime":"2020-05-15","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":5},{"netFee":0,"payTradeTime":"2020-05-16","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":6},{"netFee":0,"payTradeTime":"2020-05-17","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":7}]}]
     */
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * year : 2020
         * month : 2020-05
         * days : [{"netFee":0,"payTradeTime":"2020-05-11","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":1},{"netFee":0,"payTradeTime":"2020-05-12","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":2},{"netFee":0,"payTradeTime":"2020-05-13","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":3},{"netFee":0,"payTradeTime":"2020-05-14","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":4},{"netFee":0,"payTradeTime":"2020-05-15","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":5},{"netFee":0,"payTradeTime":"2020-05-16","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":6},{"netFee":0,"payTradeTime":"2020-05-17","payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":7}]
         */

        private String year;
        private String month;
        private List<DaysBean> days;

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public List<DaysBean> getDays() {
            return days;
        }

        public void setDays(List<DaysBean> days) {
            this.days = days;
        }

        public static class DaysBean implements Serializable {
            /**
             * netFee : 0
             * payTradeTime : 2020-05-11
             * payTradeYear : 2020
             * payTradeMonth : 2020-05
             * payTradeWeek : 1
             */

            private int netFee;//交易净额(分)
            private String payTradeTime;
            private String payTradeYear;
            private String payTradeMonth;
            private int payTradeWeek;
            private int settlementFee;//结算金额（分）

            public int getSettlementFee() {
                return settlementFee;
            }

            public void setSettlementFee(int settlementFee) {
                this.settlementFee = settlementFee;
            }

            public int getNetFee() {
                return netFee;
            }

            public void setNetFee(int netFee) {
                this.netFee = netFee;
            }

            public String getPayTradeTime() {
                return payTradeTime;
            }

            public void setPayTradeTime(String payTradeTime) {
                this.payTradeTime = payTradeTime;
            }

            public String getPayTradeYear() {
                return payTradeYear;
            }

            public void setPayTradeYear(String payTradeYear) {
                this.payTradeYear = payTradeYear;
            }

            public String getPayTradeMonth() {
                return payTradeMonth;
            }

            public void setPayTradeMonth(String payTradeMonth) {
                this.payTradeMonth = payTradeMonth;
            }

            public int getPayTradeWeek() {
                return payTradeWeek;
            }

            public void setPayTradeWeek(int payTradeWeek) {
                this.payTradeWeek = payTradeWeek;
            }
        }
    }
}
