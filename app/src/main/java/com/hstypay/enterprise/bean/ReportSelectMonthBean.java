package com.hstypay.enterprise.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/23 16:35
 * @描述: ${报表选择日期(月)}
 */
public class ReportSelectMonthBean implements Serializable {
    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {

        private String code;
        private String message;
        private List<?> args;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }
        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * year : 2020
         * months : [{"netFee":3699200,"payTradeYear":"2020","payTradeMonth":"2020-04"}]
         */

        private String year;
        private List<MonthsBean> months;

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public List<MonthsBean> getMonths() {
            return months;
        }

        public void setMonths(List<MonthsBean> months) {
            this.months = months;
        }

        public static class MonthsBean implements Serializable {
            /**
             * netFee : 3699200
             * payTradeYear : 2020
             * payTradeMonth : 2020-04
             */

            private boolean selected;
            private int netFee;//交易净额(分)
            private String payTradeYear;
            private String payTradeMonth;
            private int settlementFee;//结算金额（分）

            public int getSettlementFee() {
                return settlementFee;
            }

            public void setSettlementFee(int settlementFee) {
                this.settlementFee = settlementFee;
            }

            public boolean isSelected() {
                return selected;
            }

            public void setSelected(boolean selected) {
                this.selected = selected;
            }

            public int getNetFee() {
                return netFee;
            }

            public void setNetFee(int netFee) {
                this.netFee = netFee;
            }

            public String getPayTradeYear() {
                return payTradeYear;
            }

            public void setPayTradeYear(String payTradeYear) {
                this.payTradeYear = payTradeYear;
            }

            public String getPayTradeMonth() {
                return payTradeMonth;
            }

            public void setPayTradeMonth(String payTradeMonth) {
                this.payTradeMonth = payTradeMonth;
            }
        }
    }
}
