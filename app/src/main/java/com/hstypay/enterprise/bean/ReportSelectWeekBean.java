package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/23 16:35
 * @描述: ${报表选择日期(周)}
 */
public class ReportSelectWeekBean implements Serializable {
    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }
        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public class DataBean implements Serializable{
        /**
         * year : 2020
         * month : 2020-05
         * weeks : [{"netFee":0,"payTradeYear":"2020","payTradeMonth":"2020-05","payTradeWeek":2,"payTradeWeekScope":"2020-05-11,2020-05-17"}]
         */

        private String year;
        private String month;
        private List<WeeksBean> weeks;

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public List<WeeksBean> getWeeks() {
            return weeks;
        }

        public void setWeeks(List<WeeksBean> weeks) {
            this.weeks = weeks;
        }

        public class WeeksBean implements Serializable{
            /**
             * netFee : 0
             * payTradeYear : 2020
             * payTradeMonth : 2020-05
             * payTradeWeek : 2
             * payTradeWeekScope : 2020-05-11,2020-05-17
             */
            private boolean selected;
            private int netFee;
            private String payTradeYear;
            private String payTradeMonth;
            private int payTradeWeek;
            private String payTradeWeekScope;
            private int settlementFee;

            public int getSettlementFee() {
                return settlementFee;
            }

            public void setSettlementFee(int settlementFee) {
                this.settlementFee = settlementFee;
            }

            public boolean isSelected() {
                return selected;
            }

            public void setSelected(boolean selected) {
                this.selected = selected;
            }

            public int getNetFee() {
                return netFee;
            }

            public void setNetFee(int netFee) {
                this.netFee = netFee;
            }

            public String getPayTradeYear() {
                return payTradeYear;
            }

            public void setPayTradeYear(String payTradeYear) {
                this.payTradeYear = payTradeYear;
            }

            public String getPayTradeMonth() {
                return payTradeMonth;
            }

            public void setPayTradeMonth(String payTradeMonth) {
                this.payTradeMonth = payTradeMonth;
            }

            public int getPayTradeWeek() {
                return payTradeWeek;
            }

            public void setPayTradeWeek(int payTradeWeek) {
                this.payTradeWeek = payTradeWeek;
            }

            public String getPayTradeWeekScope() {
                return payTradeWeekScope;
            }

            public void setPayTradeWeekScope(String payTradeWeekScope) {
                this.payTradeWeekScope = payTradeWeekScope;
            }
        }
    }
}
