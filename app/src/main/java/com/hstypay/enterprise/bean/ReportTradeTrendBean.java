package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/23 17:10
 * @描述: ${交易趋势}
 */
public class ReportTradeTrendBean {
    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public List<?> getArgs() {
            return args;
        }
        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean {
        /**
         * merchantId : 1020003609
         * startTime : 2020-04-01 00:00:00
         * endTime : 2020-04-30 00:00:00
         * successFee : 0
         * successCount : 0
         * refundFee : 0
         * refundCount : 0
         * payTradeTime : 2020-04-01
         */

        private String merchantId;
        private String startTime;
        private String endTime;
        private long successFee;
        private int successCount;
        private long refundFee;
        private int refundCount;
        private String payTradeTime;
        private String tradeHour;

        public String getTradeHour() {
            return tradeHour;
        }

        public void setTradeHour(String tradeHour) {
            this.tradeHour = tradeHour;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public long getSuccessFee() {
            return successFee;
        }

        public void setSuccessFee(long successFee) {
            this.successFee = successFee;
        }

        public int getSuccessCount() {
            return successCount;
        }

        public void setSuccessCount(int successCount) {
            this.successCount = successCount;
        }

        public long getRefundFee() {
            return refundFee;
        }

        public void setRefundFee(long refundFee) {
            this.refundFee = refundFee;
        }

        public int getRefundCount() {
            return refundCount;
        }

        public void setRefundCount(int refundCount) {
            this.refundCount = refundCount;
        }

        public String getPayTradeTime() {
            return payTradeTime;
        }

        public void setPayTradeTime(String payTradeTime) {
            this.payTradeTime = payTradeTime;
        }
    }
}
