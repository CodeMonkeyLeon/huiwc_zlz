package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * @author kuangzeyu
 * @time 2021/3/5
 * @desc 角色选择bean
 */
public class RoleSelectBean implements Serializable {
    private String roleName;
    private String roleCode;//角色CODE（CASHIER（收银员），STORE_MANAGER（店长）,FINANCE（财务）ADMIN(管理员)）
    private String roleId;//4店长  2收银员

    public String getRoleId() {

        return roleId == null ? "" : roleId;

    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {

        return roleName == null ? "" : roleName;

    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {

        return roleCode == null ? "" : roleCode;

    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
