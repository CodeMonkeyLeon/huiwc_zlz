package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/22 11:24
 * @描述: ${TODO}
 */

public class SaveImageBean {
    /**
     * file : ["http://aiatest.ipd.swiftpass.cn/vmsfile/file/download?16662b28664045beaeabfccdb827fc83.png"]
     * callback : saveImageComplete
     */

    private String callback;
    private List<String> file;

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public List<String> getFile() {
        return file;
    }

    public void setFile(List<String> file) {
        this.file = file;
    }

    /*private String callback;
    private List<String> file;

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public List<String> getFile() {
        return file;
    }

    public void setFile(List<String> file) {
        this.file = file;
    }*/
}
