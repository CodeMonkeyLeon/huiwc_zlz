package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 11:05
 * @描述: ${TODO}
 */

public class ScanLoginBean implements Serializable {
    private ErrorBean error;
    /**
     * logId : cc22dc7d600647ca80f38e4fbceecd41
     * status : true
     * data : {"qrcodeId":"c320ca1ed6e144bb88a64ffd90e6cd57","initTime":"2019-07-02 17:35:45","intervalTime":10,"invalidTime":"2019-07-02 17:38:45","status":1,"validTime":180,"channel":0}
     */

    private String logId;
    private boolean status;
    private DataBean data;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }


    public static class DataBean implements Serializable{
        /**
         * qrcodeId : c320ca1ed6e144bb88a64ffd90e6cd57
         * initTime : 2019-07-02 17:35:45
         * intervalTime : 10
         * invalidTime : 2019-07-02 17:38:45
         * status : 1
         * validTime : 180
         * channel : 0
         */

        private String qrcodeId;
        private int status;//0(待使用) 1(已被扫描) 2(已失效) 3(被取消登陆) 4(已登录) 5（二维码不存在)

        public String getQrcodeId() {
            return qrcodeId;
        }

        public void setQrcodeId(String qrcodeId) {
            this.qrcodeId = qrcodeId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

    }
}
