package com.hstypay.enterprise.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * 云打印
 */
public class SetUp implements Serializable, Cloneable{

    @SerializedName("settlement_print")
    private String settlementPrint;
    @SerializedName("customer_print")
    private String customerPrint;
    @SerializedName("kitchen_print")
    private String kitchenPrint;
    @SerializedName("count_merge_print")
    private String countMergePrint;
    @SerializedName("pay_print")
    private String payPrint;

    public String getSettlementPrint() {
        if (settlementPrint == null) {
            return "2";
        } else {
            return settlementPrint;
        }
    }

    public void setSettlementPrint(String settlementPrint) {
        this.settlementPrint = settlementPrint;
    }

    public String getCustomerPrint() {
        if (customerPrint==null){
            return "2";
        }
        return customerPrint;
    }

    public void setCustomerPrint(String customerPrint) {
        this.customerPrint = customerPrint;
    }

    public String getKitchenPrint() {
        if (kitchenPrint==null){
            return "2";
        }
        return kitchenPrint;
    }

    public void setKitchenPrint(String kitchenPrint) {
        this.kitchenPrint = kitchenPrint;
    }

    public String getCountMergePrint() {
        if (countMergePrint==null){
            return "2";
        }
        return countMergePrint;
    }

    public void setCountMergePrint(String countMergePrint) {
        this.countMergePrint = countMergePrint;
    }

    public String getPayPrint() {
        if (payPrint==null){
            return "2";
        }
        return payPrint;
    }

    public void setPayPrint(String payPrint) {
        this.payPrint = payPrint;
    }

    @Override
    public Object clone() {
        SetUp ben = null;
        try {
            ben = (SetUp) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return ben;
    }
}
