package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/10/12 17:35
 * @描述: ${TODO}
 */

public class SignStatus {
    /**
     * data : [{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024045","strTradeStatus":"未支付","createTime":"2017-07-26 20:51:57","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":1,"apiProvider":1,"orderNo":"20170726868320000000100000024044","strTradeStatus":"未支付","createTime":"2017-07-26 20:47:11","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"HPAY_ANDROID_100001"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024043","strTradeStatus":"转入退款","createTime":"2017-07-26 20:13:42","tradeStatus":4,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024042","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:52","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":50,"apiProvider":3,"orderNo":"20170726868320000000100000024041","strTradeStatus":"未支付","createTime":"2017-07-26 19:56:39","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024040","strTradeStatus":"未支付","createTime":"2017-07-26 19:40:47","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024039","strTradeStatus":"未支付","createTime":"2017-07-26 19:17:09","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024038","strTradeStatus":"未支付","createTime":"2017-07-26 19:08:14","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024037","strTradeStatus":"支付成功","createTime":"2017-07-26 18:21:37","tradeStatus":2,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024036","strTradeStatus":"未支付","createTime":"2017-07-26 18:19:00","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024035","strTradeStatus":"未支付","createTime":"2017-07-26 18:17:36","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024034","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:44","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024033","strTradeStatus":"未支付","createTime":"2017-07-26 18:16:06","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024032","strTradeStatus":"未支付","createTime":"2017-07-26 18:15:37","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"},{"tradeMoney":2,"apiProvider":3,"orderNo":"20170726868320000000100000024031","strTradeStatus":"未支付","createTime":"2017-07-26 18:14:46","tradeStatus":1,"storeMerchantId":"101300000001","deviceInfo":"QR_CODE"}]
     * logId : mxfMM2GB
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : {"contractStatus":0}
     * logId : g2mQ9YQQ
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * contractStatus : 0
         */
        private int contractStatus;
        private int memberContractStatus;

        public void setContractStatus(int contractStatus) {
            this.contractStatus = contractStatus;
        }

        public int getContractStatus() {
            return contractStatus;
        }

        public void setMemberContractStatus(int memberContractStatus) {
            this.memberContractStatus = memberContractStatus;
        }

        public int getMemberContractStatus() {
            return memberContractStatus;
        }
    }
}
