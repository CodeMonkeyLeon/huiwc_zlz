package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/15 15:53
 * @描述: ${TODO}
 */

public class StoreCodeListBean {
    private ErrorBean error;
    /**
     * logId : e3d7385f32b24ae2acc88b45a19b5b04
     * status : true
     * data : {"pageSize":15,"currentPage":1,"totalPages":0,"totalRows":0,"data":[{"createUser":1,"createUserName":"system","createTime":"2018-11-22 17:41:11","updateTime":"2018-11-22 17:41:11","pageSize":0,"currentPage":0,"id":30176,"qrcode":"f20349d7cd314a429dc75020a29df640","serviceProviderId":"319100000001","serviceChannelId":"720100000001","useStatus":1,"useStatusCnt":"已绑定","batchId":0,"qrType":2,"qrTypeCnt":"银标码","qrUrl":"https://qr.95516.com/03095810/0fb649f2fbf453ef444425816bae48e8f9fd7adc97?attach=op_user_id%3D5%26store_merchant_id%3D10300000703","initTime":"2018-11-22 17:41:11","remark":"门店进件自动生成二维码","merchantName":"第三家门店","adminPlatform":false},{"createUser":1,"createUserName":"system","createTime":"2018-11-22 17:41:09","updateTime":"2018-11-22 17:41:09","pageSize":0,"currentPage":0,"id":30175,"qrcode":"9dc52c8cee7348dfa1dcc13303278f9e","serviceProviderId":"319100000001","serviceChannelId":"720100000001","useStatus":1,"useStatusCnt":"已绑定","batchId":0,"qrType":1,"qrTypeCnt":"聚合码","qrUrl":"https://hpay.hstydev.com/app/qrcodePay?qrcode=9dc52c8cee7348dfa1dcc13303278f9e","initTime":"2018-11-22 17:41:09","remark":"门店进件自动生成二维码","merchantName":"第三家门店","adminPlatform":false}]}
     */

    private String logId;
    private boolean status;
    private DataBeanX data;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBeanX {
        /**
         * pageSize : 15
         * currentPage : 1
         * totalPages : 0
         * totalRows : 0
         * data : [{"createUser":1,"createUserName":"system","createTime":"2018-11-22 17:41:11","updateTime":"2018-11-22 17:41:11","pageSize":0,"currentPage":0,"id":30176,"qrcode":"f20349d7cd314a429dc75020a29df640","serviceProviderId":"319100000001","serviceChannelId":"720100000001","useStatus":1,"useStatusCnt":"已绑定","batchId":0,"qrType":2,"qrTypeCnt":"银标码","qrUrl":"https://qr.95516.com/03095810/0fb649f2fbf453ef444425816bae48e8f9fd7adc97?attach=op_user_id%3D5%26store_merchant_id%3D10300000703","initTime":"2018-11-22 17:41:11","remark":"门店进件自动生成二维码","merchantName":"第三家门店","adminPlatform":false},{"createUser":1,"createUserName":"system","createTime":"2018-11-22 17:41:09","updateTime":"2018-11-22 17:41:09","pageSize":0,"currentPage":0,"id":30175,"qrcode":"9dc52c8cee7348dfa1dcc13303278f9e","serviceProviderId":"319100000001","serviceChannelId":"720100000001","useStatus":1,"useStatusCnt":"已绑定","batchId":0,"qrType":1,"qrTypeCnt":"聚合码","qrUrl":"https://hpay.hstydev.com/app/qrcodePay?qrcode=9dc52c8cee7348dfa1dcc13303278f9e","initTime":"2018-11-22 17:41:09","remark":"门店进件自动生成二维码","merchantName":"第三家门店","adminPlatform":false}]
         */

        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<DataBean> data;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean implements Serializable {
            /**
             * createUser : 1
             * createUserName : system
             * createTime : 2018-11-22 17:41:11
             * updateTime : 2018-11-22 17:41:11
             * pageSize : 0
             * currentPage : 0
             * id : 30176
             * qrcode : f20349d7cd314a429dc75020a29df640
             * serviceProviderId : 319100000001
             * serviceChannelId : 720100000001
             * useStatus : 1
             * useStatusCnt : 已绑定
             * batchId : 0
             * qrType : 2
             * qrTypeCnt : 银标码
             * qrUrl : https://qr.95516.com/03095810/0fb649f2fbf453ef444425816bae48e8f9fd7adc97?attach=op_user_id%3D5%26store_merchant_id%3D10300000703
             * initTime : 2018-11-22 17:41:11
             * remark : 门店进件自动生成二维码
             * merchantName : 第三家门店
             * adminPlatform : false
             */

            private int createUser;
            private String createUserName;
            private String createTime;
            private String updateTime;
            private int pageSize;
            private int currentPage;
            private int id;
            private String qrcode;
            private String serviceProviderId;
            private String serviceChannelId;
            private int useStatus;
            private String useStatusCnt;
            private int batchId;
            private int qrType;
            private String qrTypeCnt;
            private String qrUrl;
            private String initTime;
            private String remark;
            private String storeId;
            private String storeName;
            private String empName;
            private String userId;
            private boolean adminPlatform;

            private String qrLogo;
            private int logoType;
            private List<String> apiProvider;
            private String wxappid;
            private String isAppletsPay;

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getWxappid() {
                return wxappid;
            }

            public void setWxappid(String wxappid) {
                this.wxappid = wxappid;
            }

            public String getIsAppletsPay() {
                return isAppletsPay;
            }

            public void setIsAppletsPay(String isAppletsPay) {
                this.isAppletsPay = isAppletsPay;
            }

            public String getQrLogo() {
                return qrLogo;
            }

            public void setQrLogo(String qrLogo) {
                this.qrLogo = qrLogo;
            }

            public int getLogoType() {
                return logoType;
            }

            public void setLogoType(int logoType) {
                this.logoType = logoType;
            }

            public List<String> getApiProvider() {
                return apiProvider;
            }

            public void setApiProvider(List<String> apiProvider) {
                this.apiProvider = apiProvider;
            }

            public String getStoreId() {
                return storeId;
            }

            public void setStoreId(String storeId) {
                this.storeId = storeId;
            }

            public String getEmpName() {
                return empName;
            }

            public void setEmpName(String empName) {
                this.empName = empName;
            }

            public int getCreateUser() {
                return createUser;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getQrcode() {
                return qrcode;
            }

            public void setQrcode(String qrcode) {
                this.qrcode = qrcode;
            }

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public String getServiceChannelId() {
                return serviceChannelId;
            }

            public void setServiceChannelId(String serviceChannelId) {
                this.serviceChannelId = serviceChannelId;
            }

            public int getUseStatus() {
                return useStatus;
            }

            public void setUseStatus(int useStatus) {
                this.useStatus = useStatus;
            }

            public String getUseStatusCnt() {
                return useStatusCnt;
            }

            public void setUseStatusCnt(String useStatusCnt) {
                this.useStatusCnt = useStatusCnt;
            }

            public int getBatchId() {
                return batchId;
            }

            public void setBatchId(int batchId) {
                this.batchId = batchId;
            }

            public int getQrType() {
                return qrType;
            }

            public void setQrType(int qrType) {
                this.qrType = qrType;
            }

            public String getQrTypeCnt() {
                return qrTypeCnt;
            }

            public void setQrTypeCnt(String qrTypeCnt) {
                this.qrTypeCnt = qrTypeCnt;
            }

            public String getQrUrl() {
                return qrUrl;
            }

            public void setQrUrl(String qrUrl) {
                this.qrUrl = qrUrl;
            }

            public String getInitTime() {
                return initTime;
            }

            public void setInitTime(String initTime) {
                this.initTime = initTime;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getStoreName() {
                return storeName;
            }

            public void setStoreName(String merchantName) {
                this.storeName = merchantName;
            }

            public boolean isAdminPlatform() {
                return adminPlatform;
            }

            public void setAdminPlatform(boolean adminPlatform) {
                this.adminPlatform = adminPlatform;
            }
        }
    }
}
