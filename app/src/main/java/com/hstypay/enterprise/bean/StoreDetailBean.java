package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/29.
 */

public class StoreDetailBean {

    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : {"companyPhoto":"/pic/mch/2017/08/11/95dad48c-c756-444f-a569-e6779c9796df.jpg","address":"高新南一道9号中科大厦25楼","province":"190000","city":"190300","examineStatusCnt":"审核通过","county":"190303","storeName":"彭国卿测试商户","storeId":"462300000001","activateStatus":1,"examineStatus":1}
     * logId : X7o7BWyH
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable {
        /**
         * companyPhoto : /pic/mch/2017/08/11/95dad48c-c756-444f-a569-e6779c9796df.jpg
         * address : 高新南一道9号中科大厦25楼
         * province : 190000
         * city : 190300
         * examineStatusCnt : 审核通过
         * county : 190303
         * storeName : 彭国卿测试商户
         * storeId : 462300000001
         * activateStatus : 1
         * examineStatus : 1
         */
        private String companyPhoto;
        private String address;
        private String province;
        private String provinceCnt;
        private String city;
        private String cityCnt;
        private String examineStatusCnt;
        private String county;
        private String countyCnt;
        private String storeName;
        private String storeId;
        private String userId;//店长id
        private String userIdCnt;//店长名
        private int activateStatus;
        private int examineStatus;
        private String examineRemark;
        private int storeType;//直营店21，加盟店22
        private int editEnable;//是否可编辑 1:是 0:否
        private String storeGroup;//所属分组的id
        private String groupName;//所属分组的名称
        private String empTotal;//员工数量

        public String getEmpTotal() {

            return empTotal == null ? "0" : empTotal;

        }

        public void setEmpTotal(String empTotal) {
            this.empTotal = empTotal;
        }

        public String getStoreGroup() {

            return storeGroup == null ? "" : storeGroup;

        }

        public void setStoreGroup(String storeGroup) {
            this.storeGroup = storeGroup;
        }

        public String getGroupName() {

            return groupName == null ? "" : groupName;

        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getEditEnable() {
            return editEnable;
        }

        public void setEditEnable(int editEnable) {
            this.editEnable = editEnable;
        }

        public int getStoreType() {
            return storeType;
        }

        public void setStoreType(int storeType) {
            this.storeType = storeType;
        }

        public String getProvinceCnt() {
            return provinceCnt;
        }

        public void setProvinceCnt(String provinceCnt) {
            this.provinceCnt = provinceCnt;
        }

        public String getCityCnt() {
            return cityCnt;
        }

        public void setCityCnt(String cityCnt) {
            this.cityCnt = cityCnt;
        }

        public String getCountyCnt() {
            return countyCnt;
        }

        public void setCountyCnt(String countyCnt) {
            this.countyCnt = countyCnt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserIdCnt() {
            return userIdCnt;
        }

        public void setUserIdCnt(String userIdCnt) {
            this.userIdCnt = userIdCnt;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setCompanyPhoto(String companyPhoto) {
            this.companyPhoto = companyPhoto;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public void setCounty(String county) {
            this.county = county;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public void setActivateStatus(int activateStatus) {
            this.activateStatus = activateStatus;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public String getCompanyPhoto() {
            return companyPhoto;
        }

        public String getAddress() {
            return address;
        }

        public String getProvince() {
            return province;
        }

        public String getCity() {
            return city;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public String getCounty() {
            return county;
        }

        public String getStoreName() {
            return storeName;
        }

        public String getStoreId() {
            return storeId;
        }

        public int getActivateStatus() {
            return activateStatus;
        }

        public int getExamineStatus() {
            return examineStatus;
        }
    }
}
