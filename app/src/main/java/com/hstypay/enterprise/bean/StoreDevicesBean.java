package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/3/6 14:26
 * @描述: ${TODO}
 */

public class StoreDevicesBean {
    /**
     * data : [{"storeName":"测试店","storeId":"661200010002"},{"storeName":"测试店","storeId":"661200010302"},{"storeName":"测试店","storeId":"661209510302"},{"storeName":"测试","storeId":"661300000001"},{"storeName":"测试店","storeId":"661300010002"},{"storeName":"门店2","storeId":"535300000001"},{"storeName":"店2","storeId":"535300010002"},{"storeName":"店2","storeId":"535302010002"},{"storeName":"店2","storeId":"535302310002"},{"storeName":"店2","storeId":"5353083510002"},{"storeName":"门店1","storeId":"371300000002"},{"storeName":"店1","storeId":"371300001003"},{"storeName":"店1","storeId":"371302001003"},{"storeName":"店1","storeId":"371302031003"},{"storeName":"店1","storeId":"371302037003"}]
     * logId : ML7qKgVI
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : [{"merchantDeviceList":[{"id":1253,"sn":"ce0000000000002"},{"id":1252,"sn":"ce0000000000001"}],"storeName":"测试2","storeId":"683300000002"},{"merchantDeviceList":[{"id":1254,"sn":"ce0000000000003"}],"storeName":"烦烦烦方法","storeId":"460300000001"}]
     * logId : xtLpkKwd
     * status : true
     */
    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable {
        /**
         * merchantDeviceList : [{"id":1253,"sn":"ce0000000000002"},{"id":1252,"sn":"ce0000000000001"}]
         * storeName : 测试2
         * storeId : 683300000002
         */
        private List<MerchantDeviceListEntity> merchantDeviceList;
        private boolean isOpen;
        private String storeName;
        private String storeId;
        private String deviceId;
        private String model;
        private String sn;
        private String userId;
        private String userIdCnt;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserIdCnt() {
            return userIdCnt;
        }

        public void setUserIdCnt(String userIdCnt) {
            this.userIdCnt = userIdCnt;
        }

        public void setOpen(boolean open) {
            isOpen = open;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getSn() {
            return sn;
        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public void setMerchantDeviceList(List<MerchantDeviceListEntity> merchantDeviceList) {
            this.merchantDeviceList = merchantDeviceList;
        }

        public void setIsOpen(boolean isOpen) {
            this.isOpen = isOpen;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public List<MerchantDeviceListEntity> getMerchantDeviceList() {
            return merchantDeviceList;
        }

        public boolean isOpen() {
            return isOpen;
        }

        public String getStoreName() {
            return storeName;
        }

        public String getStoreId() {
            return storeId;
        }

        public class MerchantDeviceListEntity {
            /**
             * id : 1253
             * sn : ce0000000000002
             */
            private int id;
            private String sn;

            public void setId(int id) {
                this.id = id;
            }

            public void setSn(String sn) {
                this.sn = sn;
            }

            public int getId() {
                return id;
            }

            public String getSn() {
                return sn;
            }
        }
    }
}
