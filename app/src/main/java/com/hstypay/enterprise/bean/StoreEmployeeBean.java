package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/1
 * @desc 门店员工
 */
public class StoreEmployeeBean extends BaseBean<List<StoreEmployeeBean.Employee>>{
    public static class Employee implements Serializable {
        private String userId;
        private String employeeName;
        private String telPhone;
        private int role;//角色  4是店长 2是收银员

        private String titleName;//标题栏名称，为空时不是标题
        private boolean showLine = true;//是否有下划线

        public String getUserId() {

            return userId == null ? "" : userId;

        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public int getRole() {

            return role;

        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getEmployeeName() {

            return employeeName == null ? "" : employeeName;

        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getTelPhone() {

            return telPhone == null ? "" : telPhone;

        }

        public void setTelPhone(String telPhone) {
            this.telPhone = telPhone;
        }

        public String getTitleName() {

            return titleName == null ? "" : titleName;

        }

        public void setTitleName(String titleName) {
            this.titleName = titleName;
        }

        public boolean isShowLine() {

            return showLine;

        }

        public void setShowLine(boolean showLine) {
            this.showLine = showLine;
        }
    }
}
