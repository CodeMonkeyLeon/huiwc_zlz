package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/3
 * @desc 所属分组
 */
public class StoreGroupBean extends BaseBean<StoreGroupBean.StoreGroupData> implements Serializable{
    public static class StoreGroupData implements Serializable{
        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<StoreGroupItemData> data;

        public int getPageSize() {

            return pageSize;

        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {

            return currentPage;

        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {

            return totalPages;

        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {

            return totalRows;

        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<StoreGroupItemData> getData() {

            if (data == null) {
                return new ArrayList<>();
            }
            return data;

        }

        public void setData(List<StoreGroupItemData> data) {
            this.data = data;
        }
    }

    public static class StoreGroupItemData implements Serializable {
        private String createUser;
        private String createUserName;
        private String createTime;
        private String updateTime;
        private String physicsFlag;
        private String pageSize;
        private String currentPage;
        private String groupId;
        private String groupName;
        private String merchantId;
        private String merchantIdCnt;
        private String serviceProviderId;
        private String serviceChannelId;
        private String storeCount;
        private String editEnable;
        private boolean isSelected;

        public boolean isSelected() {

            return isSelected;

        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getCreateUser() {

            return createUser == null ? "" : createUser;

        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getCreateUserName() {

            return createUserName == null ? "" : createUserName;

        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public String getCreateTime() {

            return createTime == null ? "" : createTime;

        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {

            return updateTime == null ? "" : updateTime;

        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getPhysicsFlag() {

            return physicsFlag == null ? "" : physicsFlag;

        }

        public void setPhysicsFlag(String physicsFlag) {
            this.physicsFlag = physicsFlag;
        }

        public String getPageSize() {

            return pageSize == null ? "" : pageSize;

        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public String getCurrentPage() {

            return currentPage == null ? "" : currentPage;

        }

        public void setCurrentPage(String currentPage) {
            this.currentPage = currentPage;
        }

        public String getGroupId() {

            return groupId == null ? "" : groupId;

        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {

            return groupName == null ? "" : groupName;

        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getMerchantId() {

            return merchantId == null ? "" : merchantId;

        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantIdCnt() {

            return merchantIdCnt == null ? "" : merchantIdCnt;

        }

        public void setMerchantIdCnt(String merchantIdCnt) {
            this.merchantIdCnt = merchantIdCnt;
        }

        public String getServiceProviderId() {

            return serviceProviderId == null ? "" : serviceProviderId;

        }

        public void setServiceProviderId(String serviceProviderId) {
            this.serviceProviderId = serviceProviderId;
        }

        public String getServiceChannelId() {

            return serviceChannelId == null ? "" : serviceChannelId;

        }

        public void setServiceChannelId(String serviceChannelId) {
            this.serviceChannelId = serviceChannelId;
        }

        public String getStoreCount() {

            return storeCount == null ? "" : storeCount;

        }

        public void setStoreCount(String storeCount) {
            this.storeCount = storeCount;
        }

        public String getEditEnable() {

            return editEnable == null ? "" : editEnable;

        }

        public void setEditEnable(String editEnable) {
            this.editEnable = editEnable;
        }
    }
}
