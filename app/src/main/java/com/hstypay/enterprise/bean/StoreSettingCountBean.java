package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/20
 * @desc http://doc.hstydev.com/doc/2YdjUhu0zU
 */
public class StoreSettingCountBean {
    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean {
        private String voice;//语音播报数量
        private String voiceDaoJia;//到家语音订单数量

        public String getVoice() {

            return voice == null ? "" : voice;

        }

        public void setVoice(String voice) {
            this.voice = voice;
        }

        public String getVoiceDaoJia() {

            return voiceDaoJia == null ? "" : voiceDaoJia;

        }

        public void setVoiceDaoJia(String voiceDaoJia) {
            this.voiceDaoJia = voiceDaoJia;
        }
    }

    public String getLogId() {

        return logId == null ? "" : logId;

    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {

        return status;

    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {

        return error;

    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {

        return data;

    }

    public void setData(DataBean data) {
        this.data = data;
    }
}
