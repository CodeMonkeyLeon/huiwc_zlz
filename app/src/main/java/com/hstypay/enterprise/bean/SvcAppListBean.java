package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * Created by admin on 2017/7/25.
 */

public class SvcAppListBean {
    /**
     * logId : CkmS6g4Z
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"user":{"userId":3,"userName":"18666076073","userType":3,"orgId":"683200000001","merchant":{"merchantId":"683200000001","merchantName":"支付接口联调测试数据_商户","examineStatus":1,"examineStatusCnt":"审核通过"},"merchantFlag":true,"adminFlag":false,"casherFlag":false,"roles":[{"createUser":1,"createUserName":"superadmin","createTime":"2017-07-22 15:26:58","updateTime":"2017-07-22 15:26:58","roleId":15,"appId":3,"serviceProvider":"","roleName":"超级管理员","roleCode":"SUPERADMIN","enable":true,"deleteEnable":false,"remark":"商户平台超级管理员"}]},"ops":{"userOps":[{"createTime":"2017-07-29 15:27:53","id":1,"userId":3,"merchantId":"683200000001","opCode":"APPLY-REFUND"}],"merchantOps":[]},"funcs":[{"funcId":90,"appId":3,"funcCode":"APP-OPERATION","funcName":"终端相关","parentFunc":0,"ordered":0,"menuFlag":false,"remark":"终端登录","permissions":[{"funcId":90,"permissionCode":"APPLOGIN","permissionName":"登录"},{"funcId":90,"permissionCode":"APPLY-REFUND","permissionName":"终端退款权限"}]}],"signKey":"fd9bb2c05a833c6b6465c3db8913b755"}
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean {
        private String id;
        private String appName;
        private String appCode;
        private String appType;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppCode() {
            return appCode;
        }

        public void setAppCode(String appCode) {
            this.appCode = appCode;
        }

        public String getAppType() {
            return appType;
        }

        public void setAppType(String appType) {
            this.appType = appType;
        }
    }
}
