package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/7 17:46
 * @描述: ${TODO}
 */

public class ThirdOrderBean {
    /**
     * data : {"apiProvider":1,"orderNo":"20170807868320000000100000038007","money":1,"createTime":"2017-08-07 16:53:10","merchantId":"683200000001","storeMerchantId":"101300000001","operNo":"3","deviceInfo":"HAPY_IOS","tradeState":2,"transactionId":"4004832001201708074988833025"}
     * logId : apQCS1Jj
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * apiProvider : 1
         * orderNo : 20170807868320000000100000038007
         * money : 1
         * createTime : 2017-08-07 16:53:10
         * merchantId : 683200000001
         * storeMerchantId : 101300000001
         * operNo : 3
         * deviceInfo : HAPY_IOS
         * tradeState : 2
         * transactionId : 4004832001201708074988833025
         */
        private int apiProvider;
        private String orderNo;
        private int money;
        private String createTime;
        private String merchantId;
        private String storeMerchantId;
        private String operNo;
        private String deviceInfo;
        private int tradeState;
        private String transactionId;
        private int editEnable;//是否可编辑/退款 1:是 0:否

        public int getEditEnable(){
            return editEnable;
        }

        public void setEditEnable(int editEnable){
            this.editEnable = editEnable;
        }

        public void setApiProvider(int apiProvider) {
            this.apiProvider = apiProvider;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public void setMoney(int money) {
            this.money = money;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public void setOperNo(String operNo) {
            this.operNo = operNo;
        }

        public void setDeviceInfo(String deviceInfo) {
            this.deviceInfo = deviceInfo;
        }

        public void setTradeState(int tradeState) {
            this.tradeState = tradeState;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public int getApiProvider() {
            return apiProvider;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public int getMoney() {
            return money;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public String getOperNo() {
            return operNo;
        }

        public String getDeviceInfo() {
            return deviceInfo;
        }

        public int getTradeState() {
            return tradeState;
        }

        public String getTransactionId() {
            return transactionId;
        }
    }
}
