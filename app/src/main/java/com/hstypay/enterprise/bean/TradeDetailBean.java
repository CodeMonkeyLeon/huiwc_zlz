package com.hstypay.enterprise.bean;

import com.hstypay.enterprise.bean.vanke.CouponInfoData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/30.
 */

public class TradeDetailBean implements Serializable {
    /**
     * 免充值金额/
     * 微信代金券
     */
    private long mdiscount;
    /**
     * 交易类型
     */
    private int tradeType;
    /**
     * 是否是交易订单
     * false 退款
     * true 收款
     */
    private boolean isPay = false;
    /**
     * 商户存根/客户存根
     */
    public String partner;
    /**
     * 门店名称
     */
    private String storeMerchantIdCnt;
    /**
     * 平台订单号
     */
    private String orderNo;
    /**
     * 第三方单号
     */
    private String transactionId;
    /**
     * 交易时间
     */
    private String tradeTime;
    /**
     * 交易状态
     */
    private int tradeState;
    /**
     * 收银员
     */
    private String cashierName;
    /**
     * 收银点
     */
    private String cashPointName;
    /**
     * 操作员
     */
    private String opUserRealName;
    /**
     * 交易类型
     */
    private int apiProvider;
    /**
     * 交易金额
     */
    private long money;
    /**
     * 实付金额
     */
    private long payMoney;
    /**
     * 实收金额
     */
    private long realMoney;
    /**
     * 优惠金额
     */
    private long couponFee;

    private List<Coupon> couponInfoList;

    /**
     * 付款备注
     */
    private String attach;
    /**
     * 商户订单号
     */
    private String outTradeNo;


    /**
     * 退款单号
     */
    private String refundNo;
    /**
     * 退款申请时间
     */
    private String refundTime;
    /**
     * 退款办理人员
     */
    private String refundUser;
    /**
     * 退款用户实名
     */
    private String refundUserRealName;
    /**
     * 退款状态
     * 1:初始化;2:退款成功;3:退款失败;4:未确定;5:转入代发;6:退款处理中
     */
    private int refundStatus;
    /**
     * 退款原因描述
     */
    private String ptRefundReason;

    /**
     * 平台审核状态 1：待审核，2：审核通过，3：退回
     * 余额不足时为待审核状态
     * */
    private int ptAuditStatus;

    /**
     * 退款金额
     */
    private long refundMoney;
    /**
     * 退款完成时间
     */
    private String refundUpdateTime;
    /**
     * 提供给上游渠道的订单号
     */
    private String reqOrderNo;
    /**
     * 提供给上游渠道的退款订单号
     */
    private String reqRefundNo;
    /**
     * 提供给上游渠道的通知地址
     */
    private String retNotifyUrl;
    /**
     * 原交易请求订单号
     */
    private String oriReqOrderNo;
    /**
     * 交易凭证号
     */
    private String voucherNo;
    /**
     * 退款备注
     */
    private String standby4;
    /**
     * 使用优惠券使用列表
     */
    private List<CouponInfoData> couponInfoData;

    /**
     * 使用团购券核销明细列表
     */
    private List<CouponInfoData> couponGroupInfoData;

    /**
     * 区分刷脸类型
     */
    private String apiCode;

    /**
     * 是否自动打印
     */
    private boolean isAutoPrint;

    public boolean isAutoPrint() {
        return isAutoPrint;
    }

    public void setAutoPrint(boolean autoPrint) {
        isAutoPrint = autoPrint;
    }

    public int getPtAuditStatus() {
        return ptAuditStatus;
    }

    public void setPtAuditStatus(int ptAuditStatus) {
        this.ptAuditStatus = ptAuditStatus;
    }

    public String getPtRefundReason() {
        return ptRefundReason;
    }

    public void setPtRefundReason(String ptRefundReason) {
        this.ptRefundReason = ptRefundReason;
    }

    public List<CouponInfoData> getCouponGroupInfoData() {
        return couponGroupInfoData;
    }

    public void setCouponGroupInfoData(List<CouponInfoData> couponGroupInfoData) {
        this.couponGroupInfoData = couponGroupInfoData;
    }

    public String getApiCode() {
        return apiCode;
    }

    public void setApiCode(String apiCode) {
        this.apiCode = apiCode;
    }

    public List<CouponInfoData> getCouponInfoData() {
        return couponInfoData;
    }

    public void setCouponInfoData(List<CouponInfoData> couponInfoData) {
        this.couponInfoData = couponInfoData;
    }

    public String getStandby4() {
        return standby4;
    }

    public void setStandby4(String standby4) {
        this.standby4 = standby4;
    }


    private String openid;
    private String thirdMerchantId;//收单机构商户编号
    private String tradeCode;//终端编号
    private String thirdOrderNo;//收单机构商户订单号
    private String payCardId;//刷卡卡号
    private String payCardType;//刷卡类型
    private String payCardBankName;//发卡行
    private String batchID;//批次号

    private String merchantName;
    private String deviceSn;//设备SN

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }

    public String getPayCardId() {
        return payCardId;
    }

    public void setPayCardId(String payCardId) {
        this.payCardId = payCardId;
    }

    public String getPayCardType() {
        return payCardType;
    }

    public void setPayCardType(String payCardType) {
        this.payCardType = payCardType;
    }

    public String getPayCardBankName() {
        return payCardBankName;
    }

    public void setPayCardBankName(String payCardBankName) {
        this.payCardBankName = payCardBankName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public List<Coupon> getCouponInfoList() {
        return couponInfoList;
    }

    public void setCouponInfoList(List<Coupon> couponInfoList) {
        this.couponInfoList = couponInfoList;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getThirdMerchantId() {
        return thirdMerchantId;
    }

    public void setThirdMerchantId(String thirdMerchantId) {
        this.thirdMerchantId = thirdMerchantId;
    }

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getThirdOrderNo() {
        return thirdOrderNo;
    }

    public void setThirdOrderNo(String thirdOrderNo) {
        this.thirdOrderNo = thirdOrderNo;
    }

    public int getTradeType() {
        return tradeType;
    }

    public void setTradeType(int tradeType) {
        this.tradeType = tradeType;
    }

    public boolean isPay() {
        return isPay;
    }

    public void setPay(boolean pay) {
        isPay = pay;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getStoreMerchantIdCnt() {
        return storeMerchantIdCnt;
    }

    public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
        this.storeMerchantIdCnt = storeMerchantIdCnt;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public int getTradeState() {
        return tradeState;
    }

    public void setTradeState(int tradeState) {
        this.tradeState = tradeState;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public String getOpUserRealName() {
        return opUserRealName;
    }

    public void setOpUserRealName(String opUserRealName) {
        this.opUserRealName = opUserRealName;
    }

    public int getApiProvider() {
        return apiProvider;
    }

    public void setApiProvider(int apiProvider) {
        this.apiProvider = apiProvider;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public long getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(long payMoney) {
        this.payMoney = payMoney;
    }

    public long getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(long realMoney) {
        this.realMoney = realMoney;
    }

    public long getCouponFee() {
        return couponFee;
    }

    public void setCouponFee(long couponFee) {
        this.couponFee = couponFee;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getReqOrderNo() {
        return reqOrderNo;
    }

    public void setReqOrderNo(String reqOrderNo) {
        this.reqOrderNo = reqOrderNo;
    }

    public String getReqRefundNo() {
        return reqRefundNo;
    }

    public void setReqRefundNo(String reqRefundNo) {
        this.reqRefundNo = reqRefundNo;
    }

    public String getRetNotifyUrl() {
        return retNotifyUrl;
    }

    public void setRetNotifyUrl(String retNotifyUrl) {
        this.retNotifyUrl = retNotifyUrl;
    }

    public String getOriReqOrderNo() {
        return oriReqOrderNo;
    }

    public void setOriReqOrderNo(String oriReqOrderNo) {
        this.oriReqOrderNo = oriReqOrderNo;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getRefundUser() {
        return refundUser;
    }

    public void setRefundUser(String refundUser) {
        this.refundUser = refundUser;
    }

    public String getRefundUserRealName() {
        return refundUserRealName;
    }

    public void setRefundUserRealName(String refundUserRealName) {
        this.refundUserRealName = refundUserRealName;
    }

    public int getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(int refundStatus) {
        this.refundStatus = refundStatus;
    }

    public long getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(long refundMoney) {
        this.refundMoney = refundMoney;
    }

    public String getRefundUpdateTime() {
        return refundUpdateTime;
    }

    public void setRefundUpdateTime(String refundUpdateTime) {
        this.refundUpdateTime = refundUpdateTime;
    }

    public String getCashPointName() {
        return cashPointName;
    }

    public void setCashPointName(String cashPointName) {
        this.cashPointName = cashPointName;
    }

    public long getMdiscount() {
        return mdiscount;
    }

    public void setMdiscount(long mdiscount) {
        this.mdiscount = mdiscount;
    }
}
