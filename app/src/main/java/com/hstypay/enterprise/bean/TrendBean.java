package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/25 00:40
 * @描述: ${TODO}
 */
public class TrendBean {
    private long successFee;
    private long successCount;
    private String xMsg;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getSuccessFee() {
        return successFee;
    }

    public void setSuccessFee(long successFee) {
        this.successFee = successFee;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public String getxMsg() {
        return xMsg;
    }

    public void setxMsg(String xMsg) {
        this.xMsg = xMsg;
    }
}
