package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/1/4 17:13
 * @描述: ${TODO}
 */
public class UnionRateBean {
    private String unionRate;
    private String unionRateString;

    public String getUnionRate() {
        return unionRate;
    }

    public void setUnionRate(String unionRate) {
        this.unionRate = unionRate;
    }

    public String getUnionRateString() {
        return unionRateString;
    }

    public void setUnionRateString(String unionRateString) {
        this.unionRateString = unionRateString;
    }
}
