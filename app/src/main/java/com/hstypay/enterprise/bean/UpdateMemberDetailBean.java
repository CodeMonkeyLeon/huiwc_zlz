package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/2/2 10:51
 * @描述: ${TODO}
 */

public class UpdateMemberDetailBean {
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        private int upgradeStatus;
        private String upgradeUser;
        private String upgradeUserName;
        private String upgradeTime;
        private String upgradeRemark;

        public String getUpgradeRemark() {
            return upgradeRemark;
        }

        public void setUpgradeRemark(String upgradeRemark) {
            this.upgradeRemark = upgradeRemark;
        }

        public int getUpgradeStatus() {
            return upgradeStatus;
        }

        public void setUpgradeStatus(int upgradeStatus) {
            this.upgradeStatus = upgradeStatus;
        }

        public String getUpgradeUser() {
            return upgradeUser;
        }

        public void setUpgradeUser(String upgradeUser) {
            this.upgradeUser = upgradeUser;
        }

        public String getUpgradeUserName() {
            return upgradeUserName;
        }

        public void setUpgradeUserName(String upgradeUserName) {
            this.upgradeUserName = upgradeUserName;
        }

        public String getUpgradeTime() {
            return upgradeTime;
        }

        public void setUpgradeTime(String upgradeTime) {
            this.upgradeTime = upgradeTime;
        }
    }
}
