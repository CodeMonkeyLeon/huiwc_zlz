package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/4 16:39
 * @描述: ${TODO}
 */

public class UploadImageBean {
    /**
     * data : {"picfilefm":"/pic/2017/08/04/af81e9fe-284f-4412-a9ff-383cfc7dd66c.jpg","license":"/pic/2017/08/04/6b6f2292-2bae-4502-a53f-7cfdd97adf93.jpg","picfilezm":"/pic/2017/08/04/ddee6216-b515-4d37-9069-642b44b9ca3b.jpg","store":"/pic/2017/08/04/0ae995e5-d0e4-41cc-a6b7-f8e080d3e0b5.jpg"}
     * logId : ti41OKRM
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{
        /**
         * picfilefm : /pic/2017/08/04/af81e9fe-284f-4412-a9ff-383cfc7dd66c.jpg
         * license : /pic/2017/08/04/6b6f2292-2bae-4502-a53f-7cfdd97adf93.jpg
         * picfilezm : /pic/2017/08/04/ddee6216-b515-4d37-9069-642b44b9ca3b.jpg
         * store : /pic/2017/08/04/0ae995e5-d0e4-41cc-a6b7-f8e080d3e0b5.jpg
         */
        private String picfilefm;
        private String license;
        private String picfilezm;
        private String store;

        private String bkLicensePhoto;//开户许可证
        private String bkCardPhoto;//银行卡正面照
        private String handIdcardPhoto;//手持身份证照

        public String getBkLicensePhoto() {
            return bkLicensePhoto;
        }

        public void setBkLicensePhoto(String bkLicensePhoto) {
            this.bkLicensePhoto = bkLicensePhoto;
        }

        public String getBkCardPhoto() {
            return bkCardPhoto;
        }

        public void setBkCardPhoto(String bkCardPhoto) {
            this.bkCardPhoto = bkCardPhoto;
        }

        public String getHandIdcardPhoto() {
            return handIdcardPhoto;
        }

        public void setHandIdcardPhoto(String handIdcardPhoto) {
            this.handIdcardPhoto = handIdcardPhoto;
        }

        public void setPicfilefm(String picfilefm) {
            this.picfilefm = picfilefm;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public void setPicfilezm(String picfilezm) {
            this.picfilezm = picfilezm;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getPicfilefm() {
            return picfilefm;
        }

        public String getLicense() {
            return license;
        }

        public String getPicfilezm() {
            return picfilezm;
        }

        public String getStore() {
            return store;
        }
    }
}
