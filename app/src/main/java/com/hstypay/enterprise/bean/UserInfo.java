package com.hstypay.enterprise.bean;

import java.io.Serializable;

/**
 * Created by admin on 2017/7/4.
 */

public class UserInfo implements Serializable {

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    private String tel;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private  String message;
}
