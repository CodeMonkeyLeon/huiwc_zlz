package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/5/30 16:44
 * @描述: ${TODO}
 */

public class VipActiveBean implements Serializable {
    private Number activityId;
    private int activeType;
    private int activeItemType;
    private String activeName;
    private long startTime;
    private long endTime;
    private String activeRemark;
    private String ruleStr;

    public Number getActivityId() {
        return activityId;
    }

    public void setActivityId(Number activityId) {
        this.activityId = activityId;
    }

    private List<ActiveItemBean> activeItem;

    public int getActiveType() {
        return activeType;
    }

    public void setActiveType(int activeType) {
        this.activeType = activeType;
    }

    public int getActiveItemType() {
        return activeItemType;
    }

    public void setActiveItemType(int activeItemType) {
        this.activeItemType = activeItemType;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getActiveRemark() {
        return activeRemark;
    }

    public void setActiveRemark(String activeRemark) {
        this.activeRemark = activeRemark;
    }

    public List<ActiveItemBean> getActiveItem() {
        return activeItem;
    }

    public void setActiveItem(List<ActiveItemBean> activeItem) {
        this.activeItem = activeItem;
    }

    public String getRuleStr() {
        return ruleStr;
    }

    public void setRuleStr(String ruleStr) {
        this.ruleStr = ruleStr;
    }

    public class ActiveItemBean implements Serializable{
        private String money;
        private String sale;

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getSale() {
            return sale;
        }

        public void setSale(String sale) {
            this.sale = sale;
        }
    }
}
