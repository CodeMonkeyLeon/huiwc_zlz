package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/6/26 15:11
 * @描述: ${TODO}
 */
public class VipActiveInfoBean {
    private BankListBean.ErrorBean error;

    public BankListBean.ErrorBean getError() {
        return error;
    }

    public void setError(BankListBean.ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"instructions":"说明","userDayParticipationNum":0,"userParticipationTotalNum":0,"endDate":1537692576000,"onlyNewUser":0,"cardType":"MEMBER_CARD","activityName":"test001","participationUserNum":0,"activityId":2152,"beginDate":1532335776000,"participationNum":0,"participationTotalNum":100000000,"sceneType":3,"createTime":1529747101000,"sharingMcard":2,"state":6,"ruleStr":"2","activityType":2,"applyStoreType":2}
     * logId : NNdI0dVe
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{
        /**
         * instructions : 说明
         * userDayParticipationNum : 0
         * userParticipationTotalNum : 0
         * endDate : 1537692576000
         * onlyNewUser : 0
         * cardType : MEMBER_CARD
         * activityName : test001
         * participationUserNum : 0
         * activityId : 2152
         * beginDate : 1532335776000
         * participationNum : 0
         * participationTotalNum : 100000000
         * sceneType : 3
         * createTime : 1529747101000
         * sharingMcard : 2
         * state : 6
         * ruleStr : 2
         * activityType : 2
         * applyStoreType : 2
         */
        private String instructions;
        private int userDayParticipationNum;
        private int userParticipationTotalNum;
        private long endDate;
        private int onlyNewUser;
        private String cardType;
        private String activityName;
        private int participationUserNum;
        private Number activityId;
        private long beginDate;
        private int participationNum;
        private int participationTotalNum;
        private BigDecimal payAmountTotal;//实际支付总金额
        private int sceneType;
        private long createTime;
        private int sharingMcard;
        private int state;
        private String ruleStr;
        private String rule;
        private int activityType;
        private int applyStoreType;

        public BigDecimal getPayAmountTotal() {
            return payAmountTotal;
        }

        public void setPayAmountTotal(BigDecimal payAmountTotal) {
            this.payAmountTotal = payAmountTotal;
        }

        public void setInstructions(String instructions) {
            this.instructions = instructions;
        }

        public void setUserDayParticipationNum(int userDayParticipationNum) {
            this.userDayParticipationNum = userDayParticipationNum;
        }

        public void setUserParticipationTotalNum(int userParticipationTotalNum) {
            this.userParticipationTotalNum = userParticipationTotalNum;
        }

        public void setEndDate(long endDate) {
            this.endDate = endDate;
        }

        public void setOnlyNewUser(int onlyNewUser) {
            this.onlyNewUser = onlyNewUser;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public void setActivityName(String activityName) {
            this.activityName = activityName;
        }

        public void setParticipationUserNum(int participationUserNum) {
            this.participationUserNum = participationUserNum;
        }

        public void setActivityId(Number activityId) {
            this.activityId = activityId;
        }

        public void setBeginDate(long beginDate) {
            this.beginDate = beginDate;
        }

        public void setParticipationNum(int participationNum) {
            this.participationNum = participationNum;
        }

        public void setParticipationTotalNum(int participationTotalNum) {
            this.participationTotalNum = participationTotalNum;
        }

        public void setSceneType(int sceneType) {
            this.sceneType = sceneType;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public void setSharingMcard(int sharingMcard) {
            this.sharingMcard = sharingMcard;
        }

        public void setState(int state) {
            this.state = state;
        }

        public void setRuleStr(String ruleStr) {
            this.ruleStr = ruleStr;
        }

        public void setActivityType(int activityType) {
            this.activityType = activityType;
        }

        public void setApplyStoreType(int applyStoreType) {
            this.applyStoreType = applyStoreType;
        }

        public String getInstructions() {
            return instructions;
        }

        public int getUserDayParticipationNum() {
            return userDayParticipationNum;
        }

        public int getUserParticipationTotalNum() {
            return userParticipationTotalNum;
        }

        public long getEndDate() {
            return endDate;
        }

        public int getOnlyNewUser() {
            return onlyNewUser;
        }

        public String getCardType() {
            return cardType;
        }

        public String getActivityName() {
            return activityName;
        }

        public int getParticipationUserNum() {
            return participationUserNum;
        }

        public Number getActivityId() {
            return activityId;
        }

        public long getBeginDate() {
            return beginDate;
        }

        public int getParticipationNum() {
            return participationNum;
        }

        public int getParticipationTotalNum() {
            return participationTotalNum;
        }

        public int getSceneType() {
            return sceneType;
        }

        public long getCreateTime() {
            return createTime;
        }

        public int getSharingMcard() {
            return sharingMcard;
        }

        public int getState() {
            return state;
        }

        public String getRuleStr() {
            return ruleStr;
        }

        public int getActivityType() {
            return activityType;
        }

        public int getApplyStoreType() {
            return applyStoreType;
        }

        public String getRule() {
            return rule;
        }

        public void setRule(String rule) {
            this.rule = rule;
        }
    }
}
