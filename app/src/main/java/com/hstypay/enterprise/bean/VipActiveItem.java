package com.hstypay.enterprise.bean;

import java.math.BigDecimal;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/6/25 18:57
 * @描述: ${TODO}
 */
public class VipActiveItem {
    private BigDecimal conditionAmount;//条件金额
    private BigDecimal discountVar;//优惠面值
    private BigDecimal discountMaxAmount;//最大优惠金额
    private BigDecimal discount;//折扣
    private BigDecimal maxDiscount;//最大折扣金额

    public BigDecimal getConditionAmount() {
        return conditionAmount;
    }

    public void setConditionAmount(BigDecimal conditionAmount) {
        this.conditionAmount = conditionAmount;
    }

    public BigDecimal getDiscountVar() {
        return discountVar;
    }

    public void setDiscountVar(BigDecimal discountVar) {
        this.discountVar = discountVar;
    }

    public BigDecimal getDiscountMaxAmount() {
        return discountMaxAmount;
    }

    public void setDiscountMaxAmount(BigDecimal discountMaxAmount) {
        this.discountMaxAmount = discountMaxAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(BigDecimal maxDiscount) {
        this.maxDiscount = maxDiscount;
    }
}
