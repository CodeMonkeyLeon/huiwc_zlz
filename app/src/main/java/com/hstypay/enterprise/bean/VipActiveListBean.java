package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2018/6/26 14:36
 * @描述: ${TODO}
 */
public class VipActiveListBean {

    private BankListBean.ErrorBean error;

    public BankListBean.ErrorBean getError() {
        return error;
    }

    public void setError(BankListBean.ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"total":6,"offset":1,"dataList":[{"end_date":1537692576000,"create_time":1529747101000,"activity_type":2,"begin_date":1532335776000,"activity_id":2152,"name":"test001","rule_str":"2","state":6,"card_type":"MEMBER_CARD","scene_type":3},{"end_date":1559923199000,"create_time":1528955651000,"activity_type":2,"begin_date":1528905600000,"activity_id":2082,"name":"1","rule_str":"充值满100.00元，减100.00元","state":5,"card_type":"MEMBER_CARD","scene_type":2},{"end_date":1528127999000,"create_time":1528121624000,"activity_type":3,"begin_date":1528041600000,"activity_id":1979,"name":"T2","rule_str":"充值满1.00元，5折，最高折扣10.00元","state":4,"card_type":"MEMBER_CARD","scene_type":2},{"end_date":1526486399000,"create_time":1526450887000,"activity_type":2,"begin_date":1526400000000,"activity_id":1757,"name":"发的","rule_str":"充值满1.00元，减1.00元","state":6,"card_type":"MEMBER_CARD","scene_type":2},{"end_date":1523203199000,"create_time":1523174795000,"activity_type":1,"begin_date":1523116800000,"activity_id":1106,"name":"是打发动感光波","rule_str":"充值满1.00元，赠送1.00元","state":6,"card_type":"MEMBER_CARD","scene_type":2}],"limit":15}
     * logId : q5zgV4v9
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity {
        /**
         * total : 6
         * offset : 1
         * dataList : [{"end_date":1537692576000,"create_time":1529747101000,"activity_type":2,"begin_date":1532335776000,"activity_id":2152,"name":"test001","rule_str":"2","state":6,"card_type":"MEMBER_CARD","scene_type":3},{"end_date":1559923199000,"create_time":1528955651000,"activity_type":2,"begin_date":1528905600000,"activity_id":2082,"name":"1","rule_str":"充值满100.00元，减100.00元","state":5,"card_type":"MEMBER_CARD","scene_type":2},{"end_date":1528127999000,"create_time":1528121624000,"activity_type":3,"begin_date":1528041600000,"activity_id":1979,"name":"T2","rule_str":"充值满1.00元，5折，最高折扣10.00元","state":4,"card_type":"MEMBER_CARD","scene_type":2},{"end_date":1526486399000,"create_time":1526450887000,"activity_type":2,"begin_date":1526400000000,"activity_id":1757,"name":"发的","rule_str":"充值满1.00元，减1.00元","state":6,"card_type":"MEMBER_CARD","scene_type":2},{"end_date":1523203199000,"create_time":1523174795000,"activity_type":1,"begin_date":1523116800000,"activity_id":1106,"name":"是打发动感光波","rule_str":"充值满1.00元，赠送1.00元","state":6,"card_type":"MEMBER_CARD","scene_type":2}]
         * limit : 15
         */
        private int total;
        private int offset;
        private List<DataListEntity> dataList;
        private int limit;

        public void setTotal(int total) {
            this.total = total;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public void setDataList(List<DataListEntity> dataList) {
            this.dataList = dataList;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getTotal() {
            return total;
        }

        public int getOffset() {
            return offset;
        }

        public List<DataListEntity> getDataList() {
            return dataList;
        }

        public int getLimit() {
            return limit;
        }

        public static class DataListEntity implements Serializable {
            /**
             * end_date : 1537692576000
             * create_time : 1529747101000
             * activity_type : 2
             * begin_date : 1532335776000
             * activity_id : 2152
             * name : test001
             * rule_str : 2
             * state : 6
             * card_type : MEMBER_CARD
             * scene_type : 3
             */
            private long end_date;
            private long create_time;
            private int activity_type;
            private long begin_date;
            private BigDecimal activity_id;
            private String name;
            private String rule_str;
            private int state;
            private String card_type;
            private int scene_type;
            private long participation_user_num;//参与人数
            private long participation_num;//参与次数
            private BigDecimal pay_amount_total;//实际支付总金额
            private String rule;

            public BigDecimal getPay_amount_total() {
                return pay_amount_total;
            }

            public void setPay_amount_total(BigDecimal pay_amount_total) {
                this.pay_amount_total = pay_amount_total;
            }

            public void setEnd_date(long end_date) {
                this.end_date = end_date;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }

            public void setActivity_type(int activity_type) {
                this.activity_type = activity_type;
            }

            public void setBegin_date(long begin_date) {
                this.begin_date = begin_date;
            }

            public void setActivity_id(BigDecimal activity_id) {
                this.activity_id = activity_id;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setRule_str(String rule_str) {
                this.rule_str = rule_str;
            }

            public void setState(int state) {
                this.state = state;
            }

            public void setCard_type(String card_type) {
                this.card_type = card_type;
            }

            public void setScene_type(int scene_type) {
                this.scene_type = scene_type;
            }

            public long getEnd_date() {
                return end_date;
            }

            public long getCreate_time() {
                return create_time;
            }

            public int getActivity_type() {
                return activity_type;
            }

            public long getBegin_date() {
                return begin_date;
            }

            public BigDecimal getActivity_id() {
                return activity_id;
            }

            public String getName() {
                return name;
            }

            public String getRule_str() {
                return rule_str;
            }

            public int getState() {
                return state;
            }

            public String getCard_type() {
                return card_type;
            }

            public int getScene_type() {
                return scene_type;
            }

            public long getParticipation_user_num() {
                return participation_user_num;
            }

            public void setParticipation_user_num(long participation_user_num) {
                this.participation_user_num = participation_user_num;
            }

            public long getParticipation_num() {
                return participation_num;
            }

            public void setParticipation_num(long participation_num) {
                this.participation_num = participation_num;
            }

            public String getRule() {
                return rule;
            }

            public void setRule(String rule) {
                this.rule = rule;
            }
        }
    }
}
