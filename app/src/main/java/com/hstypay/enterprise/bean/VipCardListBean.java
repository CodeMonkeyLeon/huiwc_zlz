package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/1 18:27
 * @描述: ${TODO}
 */
public class VipCardListBean implements Serializable{

    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity implements Serializable{
        private String cardType;//卡类型：1--储蓄卡(普通卡) 4--会员卡
        private String membersNo;//会员卡号
        private String mobile;//手机号号
        private String name;//姓名
        private String birthday;//生日
        private String membersGrade;//会员等级(1~10)
        private String accountBalance;//账户余额
        private String countConsume;//累计消费
        private String integralBalance;//积分余额
        private String state;//状态 1-初始 2-正常 3-冻结 4-卡失效 5-赠送中
        private String activatedState;//完善激活信息
        private String registerTime;//注册日期

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getMembersNo() {
            return membersNo;
        }

        public void setMembersNo(String membersNo) {
            this.membersNo = membersNo;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getMembersGrade() {
            return membersGrade;
        }

        public void setMembersGrade(String membersGrade) {
            this.membersGrade = membersGrade;
        }

        public String getAccountBalance() {
            return accountBalance;
        }

        public void setAccountBalance(String accountBalance) {
            this.accountBalance = accountBalance;
        }

        public String getCountConsume() {
            return countConsume;
        }

        public void setCountConsume(String countConsume) {
            this.countConsume = countConsume;
        }

        public String getIntegralBalance() {
            return integralBalance;
        }

        public void setIntegralBalance(String integralBalance) {
            this.integralBalance = integralBalance;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getActivatedState() {
            return activatedState;
        }

        public void setActivatedState(String activatedState) {
            this.activatedState = activatedState;
        }

        public String getRegisterTime() {
            return registerTime;
        }

        public void setRegisterTime(String registerTime) {
            this.registerTime = registerTime;
        }
    }
}
