package com.hstypay.enterprise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/8/4.
 */

public class VoiceBean implements Serializable {


    /**
     * logId : 8Z3qxGMI
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : [{"userId":3,"merchantId":"683200000001","storeMerchantId":"101300000001","merchantName":"支付接口联调测试数据_门店","pushClose":1},{"userId":3,"merchantId":"683200000001","storeMerchantId":"501300000001","merchantName":"火影忍者之雏田vs小樱","pushClose":1}]
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * userId : 3
         * merchantId : 683200000001
         * storeMerchantId : 101300000001
         * merchantName : 支付接口联调测试数据_门店
         * pushClose : 1
         */

        private int userId;
        private String merchantId;
        private String storeMerchantId;
        private String merchantName;
        private int pushClose;
        // app/pushUser/storeSettingList新增字段
        private String storeMerchantName;//威到家门店名称
        private String id;
        public boolean isSelcet() {
            return isSelcet;
        }

        public void setSelcet(boolean selcet) {
            isSelcet = selcet;
        }

        private boolean isSelcet;

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getStoreMerchantId() {
            return storeMerchantId;
        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public int getPushClose() {
            return pushClose;
        }

        public void setPushClose(int pushClose) {
            this.pushClose = pushClose;
        }

        public String getStoreMerchantName() {
            return storeMerchantName;
        }

        public void setStoreMerchantName(String storeMerchantName) {
            this.storeMerchantName = storeMerchantName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
