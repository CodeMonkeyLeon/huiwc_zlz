package com.hstypay.enterprise.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/24 18:24
 * @描述: ${TODO}
 */

public class WhiteListBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"whiteListEnabled":false}
     * logId : TTtaDUrQ
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * whiteListEnabled : false
         */
        private boolean isWhiteList;
        private boolean whiteListEnabled;
        private boolean retcode;
        private boolean statisticsCode;//是否显示统计页面会员数据
        private String mcard_receive_url;
        private String message;
        private boolean enable;//花呗可用
        private int isRiskWhite;//风控白名单 0 不是 1 是
        private String qrcodeUrl;
        private int isCashPointWhite;//收银点白名单 0 不是 1 是
        private String finishConfig;//是否配置完成（）1完成
        private String serviceStatus;//是否已开通对应应用 0-未开通，1-已开通
        private String examineRemark;//审核备注字段
        private boolean applyRefund;//退款权限，true 打开 false 关闭

        public int getIsRiskWhite() {
            return isRiskWhite;
        }

        public void setIsRiskWhite(int isRiskWhite) {
            this.isRiskWhite = isRiskWhite;
        }

        public boolean isApplyRefund() {
            return applyRefund;
        }

        public void setApplyRefund(boolean applyRefund) {
            this.applyRefund = applyRefund;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public String getServiceStatus() {
            return serviceStatus;
        }

        public void setServiceStatus(String serviceStatus) {
            this.serviceStatus = serviceStatus;
        }

        public String getFinishConfig() {
            return finishConfig;
        }

        public void setFinishConfig(String finishConfig) {
            this.finishConfig = finishConfig;
        }

        public boolean isStatisticsCode() {
            return statisticsCode;
        }

        public void setStatisticsCode(boolean statisticsCode) {
            this.statisticsCode = statisticsCode;
        }

        public String getQrcodeUrl() {
            return qrcodeUrl;
        }

        public void setQrcodeUrl(String qrcodeUrl) {
            this.qrcodeUrl = qrcodeUrl;
        }

        public int getIsCashPointWhite() {
            return isCashPointWhite;
        }

        public void setIsCashPointWhite(int isCashPointWhite) {
            this.isCashPointWhite = isCashPointWhite;
        }

        public int isRiskWhite() {
            return isRiskWhite;
        }

        public void setRiskWhite(int riskWhite) {
            isRiskWhite = riskWhite;
        }

        public boolean isRetcode() {
            return retcode;
        }

        public void setRetcode(boolean retcode) {
            this.retcode = retcode;
        }

        public String getMcard_receive_url() {
            return mcard_receive_url;
        }

        public void setMcard_receive_url(String mcard_receive_url) {
            this.mcard_receive_url = mcard_receive_url;
        }

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public boolean isWhiteList() {
            return isWhiteList;
        }

        public void setWhiteList(boolean whiteList) {
            isWhiteList = whiteList;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setMemerUrl(String mcard_receive_url) {
            this.mcard_receive_url = mcard_receive_url;
        }

        public String getMemerUrl() {
            return mcard_receive_url;
        }

        public void setRetCode(boolean retcode) {
            this.retcode = retcode;
        }

        public boolean getRetCode() {
            return retcode;
        }

        public void setWhiteListEnabled(boolean whiteListEnabled) {
            this.whiteListEnabled = whiteListEnabled;
        }

        public boolean isWhiteListEnabled() {
            return whiteListEnabled;
        }
    }
}
