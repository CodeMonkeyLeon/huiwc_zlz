package com.hstypay.enterprise.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/06/29 18:29
 * @描述: ${TODO}
 */
public class YinshangPosBean {

    /**
     * appName : 公共资源
     * transId : 签到
     * resultCode : 0
     * resultMsg : 接口调用成功
     * transData : {"resCode":"00","resDesc":"签到成功","uniCode":"89844036513660866080005","merchantNo":"898440365136608","terminalNo":"66080005"}
     */

    private String appName;
    private String transId;
    private String resultCode;
    private String resultMsg;
    private TransDataBean transData;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public TransDataBean getTransData() {
        return transData;
    }

    public void setTransData(TransDataBean transData) {
        this.transData = transData;
    }

    public static class TransDataBean {
        /**
         * resCode : 00
         * resDesc : 签到成功
         * uniCode : 89844036513660866080005
         * merchantNo : 898440365136608
         * terminalNo : 66080005
         */

        private String resCode;
        private String resDesc;
        private String uniCode;
        private String merchantNo;
        private String terminalNo;
        private String orderStatus;
        private String extOrderNo;
        private String refNo;
        private String amt;
        private String cardType;
        private String date;
        private String time;
        private String cardNo;
        private String batchNo;//批次号
        private String traceNo;//凭证号
        private String APPLAB;

        public String getAPPLAB() {
            return APPLAB;
        }

        public void setAPPLAB(String APPLAB) {
            this.APPLAB = APPLAB;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public String getBatchNo() {
            return batchNo;
        }

        public void setBatchNo(String batchNo) {
            this.batchNo = batchNo;
        }

        public String getTraceNo() {
            return traceNo;
        }

        public void setTraceNo(String traceNo) {
            this.traceNo = traceNo;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getAmt() {
            return amt;
        }

        public void setAmt(String amt) {
            this.amt = amt;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getExtOrderNo() {
            return extOrderNo;
        }

        public void setExtOrderNo(String extOrderNo) {
            this.extOrderNo = extOrderNo;
        }

        public String getRefNo() {
            return refNo;
        }

        public void setRefNo(String refNo) {
            this.refNo = refNo;
        }

        public String getResCode() {
            return resCode;
        }

        public void setResCode(String resCode) {
            this.resCode = resCode;
        }

        public String getResDesc() {
            return resDesc;
        }

        public void setResDesc(String resDesc) {
            this.resDesc = resDesc;
        }

        public String getUniCode() {
            return uniCode;
        }

        public void setUniCode(String uniCode) {
            this.uniCode = uniCode;
        }

        public String getMerchantNo() {
            return merchantNo;
        }

        public void setMerchantNo(String merchantNo) {
            this.merchantNo = merchantNo;
        }

        public String getTerminalNo() {
            return terminalNo;
        }

        public void setTerminalNo(String terminalNo) {
            this.terminalNo = terminalNo;
        }
    }
}
