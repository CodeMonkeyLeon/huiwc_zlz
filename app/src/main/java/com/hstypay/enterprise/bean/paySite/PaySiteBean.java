package com.hstypay.enterprise.bean.paySite;

import java.io.Serializable;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean.paySite
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/07/20 15:32
 * @描述: ${TODO}
 */
public class PaySiteBean implements Serializable {
    private String id;//收银点编号
    private String cashPointName;//收银点名称
    private String relateNum;//关联数
    private String storeName;//店铺名称
    private String storeMerchantId;//店铺id
    private String ifRelate;//是否关联（1：已关联；0：关联）

    public String getStoreMerchantId() {
        return storeMerchantId;
    }

    public void setStoreMerchantId(String storeMerchantId) {
        this.storeMerchantId = storeMerchantId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCashPointName() {
        return cashPointName;
    }

    public void setCashPointName(String cashPointName) {
        this.cashPointName = cashPointName;
    }

    public String getRelateNum() {
        return relateNum;
    }

    public void setRelateNum(String relateNum) {
        this.relateNum = relateNum;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getIfRelate() {
        return ifRelate;
    }

    public void setIfRelate(String ifRelate) {
        this.ifRelate = ifRelate;
    }
}
