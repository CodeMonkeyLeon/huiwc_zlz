package com.hstypay.enterprise.bean.paySite;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean.paySite
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/07/20 15:32
 * @描述: ${TODO}
 */
public class SiteDeviceBean {
    private String deviceType;//设备类型
    private String model;//设备型号
    private String sn;//sn码
    private String bindFlag;//是否关联
    private String id;
    private String deviceId;//设备ID
    private boolean isChecked;
    private String deviceClass;
    private String unitType;
    private String cashPointName;

    public String getBindFlag() {
        return bindFlag;
    }

    public void setBindFlag(String bindFlag) {
        this.bindFlag = bindFlag;
    }

    public String getCashPointName() {
        return cashPointName;
    }

    public void setCashPointName(String cashPointName) {
        this.cashPointName = cashPointName;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceClass(String deviceClass) {
        this.deviceClass = deviceClass;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
