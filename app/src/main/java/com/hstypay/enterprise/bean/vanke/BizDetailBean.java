package com.hstypay.enterprise.bean.vanke;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean.vanke
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/07/13 14:39
 * @描述: ${TODO}
 */
public class BizDetailBean {
    private int is_biz;
    private int biz_type;
    private long biz_fee;
    private String card_no;
    private String uid;
    private String mobile;
    private String inter_provider_type;

    public String getInterProviderType() {
        return inter_provider_type;
    }

    public void setInterProviderType(String interProviderType) {
        this.inter_provider_type = interProviderType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getIs_biz() {
        return is_biz;
    }

    public void setIs_biz(int is_biz) {
        this.is_biz = is_biz;
    }

    public int getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(int biz_type) {
        this.biz_type = biz_type;
    }

    public long getBiz_fee() {
        return biz_fee;
    }

    public void setBiz_fee(long biz_fee) {
        this.biz_fee = biz_fee;
    }
}
