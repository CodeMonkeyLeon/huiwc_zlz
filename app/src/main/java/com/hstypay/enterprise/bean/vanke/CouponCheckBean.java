package com.hstypay.enterprise.bean.vanke;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean.vanke
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/07/09 15:49
 * @描述: ${TODO}
 */
public class CouponCheckBean {

    /**
     * logId : 3821271fe3474a9daaddf0faffdf92e2
     * status : false
     * error : {"code":"10000","message":"订单金额必须大于等于券优惠金额和立减金额和会员权益金额","args":null}
     * data : null
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private CouponCheckData data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public CouponCheckData getData() {
        return data;
    }

    public void setData(CouponCheckData data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : 10000
         * message : 订单金额必须大于等于券优惠金额和立减金额和会员权益金额
         * args : null
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
}
