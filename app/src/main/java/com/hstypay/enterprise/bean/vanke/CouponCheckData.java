package com.hstypay.enterprise.bean.vanke;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2020/07/06 16:27
 * @描述: ${TODO}
 */

public class CouponCheckData {
    private long couponMoney;//优惠金额(分)
    private long reduceMoney;//立减金额(分)
    private long discountMoney;//会员权益金额(分)
    private long paymentMoney;//应付金额(分)

    private List<CouponInfoData> filterCouponInfoList;

    public List<CouponInfoData> getFilterCouponInfoList() {
        return filterCouponInfoList;
    }

    public void setFilterCouponInfoList(List<CouponInfoData> filterCouponInfoList) {
        this.filterCouponInfoList = filterCouponInfoList;
    }

    public long getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(long couponMoney) {
        this.couponMoney = couponMoney;
    }

    public long getReduceMoney() {
        return reduceMoney;
    }

    public void setReduceMoney(long reduceMoney) {
        this.reduceMoney = reduceMoney;
    }

    public long getDiscountMoney() {
        return discountMoney;
    }

    public void setDiscountMoney(long discountMoney) {
        this.discountMoney = discountMoney;
    }

    public long getPaymentMoney() {
        return paymentMoney;
    }

    public void setPaymentMoney(long paymentMoney) {
        this.paymentMoney = paymentMoney;
    }
}
