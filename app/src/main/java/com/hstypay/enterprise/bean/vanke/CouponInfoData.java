package com.hstypay.enterprise.bean.vanke;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2020/07/06 16:27
 * @描述: ${TODO}
 */

public class CouponInfoData implements Serializable  {
    private String couponID;//券ID
    private String couponName;//券标题(名称)
    private String couponRuleNo;//券模板编号
    private String couponSubtitle;//券副标题
    private int couponType;//卡券类型 1-代金券;2-折扣券;3-兑换券;4-优惠券;5-停车券;6-团购券
    private String couponCode;//券码
    private List<String> couponCodeList;//团购核销券码
    private String couponDesc;//券简介
    private String enableTime;//启用时间(yyyy-MM-dd HH:mm:ss)
    private String overdueTime;//过期时间(yyyy-MM-dd HH:mm:ss)
    private String couponDescribe;//券描述
    private List<Integer> partTimeList;//每周可用时段集合(存储：1 [代表周一]，2 [代表周二]...)
    private List<DayUseful> dPTList;//每日可用时段
    private long singleCosts;//单张成本(分)
    private long subsidy;//单张补贴(分)
    private long reduceMoney;//减免金额(分)【代金券专用】
    private long couponMoney;
    private long deductible;//抵扣条件(消费满多少元可用。如不写则默认：消费满任意金额可用)[元]【代金券专用】
    private int useState;//使用状态 1：已使用 2：未使用
    private String useTime;//使用时间【已使用的券有值】
    private long discountAmount;//折扣额度【折扣券专用】（1 ~ 9.9） [1代表一折]
    private String insteadMoney;//抵消时常(分钟)【停车券专用】
    private String insteadTime;//抵消时常(分钟)【停车券专用】
    private String exchangeInfo;//兑换说明【兑换券专用】
    private String promotionInfo;//优惠说明【优惠券专用】
    private int interProviderType;//新增参数，券提供方。美团返回1
    private int canUseCount;//新增参数，美团会返回，核销时券信息原样上送即可
    private int needUseCount;//使用数量
    private boolean isChecked;//是否选中

    private int  checkFlag;//选中状态1已经选中 2最新选中

    public long getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(long couponMoney) {
        this.couponMoney = couponMoney;
    }

    public List<String> getCouponCodeList() {
        return couponCodeList;
    }

    public void setCouponCodeList(List<String> couponCodeList) {
        this.couponCodeList = couponCodeList;
    }

    public int getNeedUseCount() {
        return needUseCount;
    }

    public void setNeedUseCount(int useCount) {
        this.needUseCount = useCount;
    }

    public int getInterProviderType() {
        return interProviderType;
    }

    public void setInterProviderType(int interProviderType) {
        this.interProviderType = interProviderType;
    }

    public int getCanUseCount() {
        return canUseCount;
    }

    public void setCanUseCount(int canUseCount) {
        this.canUseCount = canUseCount;
    }

    public int getCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(int checkFlag) {
        this.checkFlag = checkFlag;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getCouponDescribe() {
        return couponDescribe;
    }

    public void setCouponDescribe(String couponDescribe) {
        this.couponDescribe = couponDescribe;
    }

    public String getCouponID() {
        return couponID;
    }

    public void setCouponID(String couponID) {
        this.couponID = couponID;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponRuleNo() {
        return couponRuleNo;
    }

    public void setCouponRuleNo(String couponRuleNo) {
        this.couponRuleNo = couponRuleNo;
    }

    public String getCouponSubtitle() {
        return couponSubtitle;
    }

    public void setCouponSubtitle(String couponSubtitle) {
        this.couponSubtitle = couponSubtitle;
    }

    public int getCouponType() {
        return couponType;
    }

    public void setCouponType(int couponType) {
        this.couponType = couponType;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    public String getEnableTime() {
        return enableTime;
    }

    public void setEnableTime(String enableTime) {
        this.enableTime = enableTime;
    }

    public String getOverdueTime() {
        return overdueTime;
    }

    public void setOverdueTime(String overdueTime) {
        this.overdueTime = overdueTime;
    }

    public List<Integer> getPartTimeList() {
        return partTimeList;
    }

    public void setPartTimeList(List<Integer> partTimeList) {
        this.partTimeList = partTimeList;
    }

    public List<DayUseful> getdPTList() {
        return dPTList;
    }

    public void setdPTList(List<DayUseful> dPTList) {
        this.dPTList = dPTList;
    }

    public long getSingleCosts() {
        return singleCosts;
    }

    public void setSingleCosts(long singleCosts) {
        this.singleCosts = singleCosts;
    }

    public long getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(long subsidy) {
        this.subsidy = subsidy;
    }

    public long getReduceMoney() {
        return reduceMoney;
    }

    public void setReduceMoney(long reduceMoney) {
        this.reduceMoney = reduceMoney;
    }

    public long getDeductible() {
        return deductible;
    }

    public void setDeductible(long deductible) {
        this.deductible = deductible;
    }

    public int getUseState() {
        return useState;
    }

    public void setUseState(int useState) {
        this.useState = useState;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public long getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(long discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getInsteadMoney() {
        return insteadMoney;
    }

    public void setInsteadMoney(String insteadMoney) {
        this.insteadMoney = insteadMoney;
    }

    public String getInsteadTime() {
        return insteadTime;
    }

    public void setInsteadTime(String insteadTime) {
        this.insteadTime = insteadTime;
    }

    public String getExchangeInfo() {
        return exchangeInfo;
    }

    public void setExchangeInfo(String exchangeInfo) {
        this.exchangeInfo = exchangeInfo;
    }

    public String getPromotionInfo() {
        return promotionInfo;
    }

    public void setPromotionInfo(String promotionInfo) {
        this.promotionInfo = promotionInfo;
    }

    private static class DayUseful {
       private Number week;//星期【1-7】(存储：1 [代表周一]，2 [代表周二]...)
       private String timeBegin;//适用时段[开始](yyyy-MM-dd HH:mm:ss)
       private String timeEnd;//适用时段[结束](yyyy-MM-dd HH:mm:ss)

       public Number getWeek() {
           return week;
       }

       public void setWeek(Number week) {
           this.week = week;
       }

       public String getTimeBegin() {
           return timeBegin;
       }

       public void setTimeBegin(String timeBegin) {
           this.timeBegin = timeBegin;
       }

       public String getTimeEnd() {
           return timeEnd;
       }

       public void setTimeEnd(String timeEnd) {
           this.timeEnd = timeEnd;
       }
   }
}
