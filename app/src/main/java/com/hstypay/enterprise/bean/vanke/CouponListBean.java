package com.hstypay.enterprise.bean.vanke;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/17 21:12
 * @描述: ${TODO}
 */

public class CouponListBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"url":"http://hpay.dev.hstypay.com/app/registe/qrcode/11cc8a48ac6b405f0ee6bbf69a0f0409990f44b7f3fd911f0d3a62bf6848eeb50561f09989be900ef27a2c4d54040b47","timeout":1800}
     * logId : qLx8qQ4Q
     * status : true
     */
    private List<CouponInfoData> data;
    private String logId;
    private boolean status;

    public void setData(List<CouponInfoData> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<CouponInfoData> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }


}
