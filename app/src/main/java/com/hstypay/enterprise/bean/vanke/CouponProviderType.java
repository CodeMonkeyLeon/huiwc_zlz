package com.hstypay.enterprise.bean.vanke;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2020/07/06 16:27
 * @描述: ${TODO}
 */

public class CouponProviderType implements Serializable  {
    private int interProviderType;//优惠券支付方式提供方类型 0：猫酷；1：美团

    public int getInterProviderType() {
        return interProviderType;
    }

    public void setInterProviderType(int interProviderType) {
        this.interProviderType = interProviderType;
    }
}
