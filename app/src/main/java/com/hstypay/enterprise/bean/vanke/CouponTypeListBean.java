package com.hstypay.enterprise.bean.vanke;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/17 21:12
 * @描述: ${TODO}
 */

public class CouponTypeListBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<CouponType> data;
    private String logId;
    private boolean status;

    public void setData(List<CouponType> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<CouponType> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class CouponType{
        private String apiCode;
        private String interProviderType;
        private String interProviderName;

        public String getInterProviderType() {
            return interProviderType;
        }

        public void setInterProviderType(String interProviderType) {
            this.interProviderType = interProviderType;
        }

        public String getInterProviderName() {
            return interProviderName;
        }

        public void setInterProviderName(String interProviderName) {
            this.interProviderName = interProviderName;
        }

        public String getApiCode() {
            return apiCode;
        }

        public void setApiCode(String apiCode) {
            this.apiCode = apiCode;
        }
    }
}
