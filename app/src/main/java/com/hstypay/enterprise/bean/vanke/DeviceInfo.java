package com.hstypay.enterprise.bean.vanke;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean.vanke
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/07/29 16:16
 * @描述: ${TODO}
 */
public class DeviceInfo {
    private String Vendor;//设备厂商
    private String SN;//硬件序列号号
    private String TermType;//设备机型
    private String SoftwareVer;//智能桌面版本

    public String getVendor() {
        return Vendor;
    }

    public void setVendor(String vendor) {
        Vendor = vendor;
    }

    public String getSN() {
        return SN;
    }

    public void setSN(String SN) {
        this.SN = SN;
    }

    public String getTermType() {
        return TermType;
    }

    public void setTermType(String termType) {
        TermType = termType;
    }

    public String getSoftwareVer() {
        return SoftwareVer;
    }

    public void setSoftwareVer(String softwareVer) {
        SoftwareVer = softwareVer;
    }
}
