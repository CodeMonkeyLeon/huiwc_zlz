package com.hstypay.enterprise.bean.vanke;

import java.io.Serializable;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2020/07/06 16:27
 * @描述: ${TODO}
 */

public class VipInfoData implements Serializable {
    private String bizUid;//用户唯一标识
    private String nickName;//用户昵称
    private String avatar;//头像URL
    private String mobile;//手机号码
    private String wXOpenID;//微信OpenID
    private String mallCardNo;//猫酷会员卡号
    private String mallCardName;//猫酷会员卡名称
    private String score;//积分
    private String mallCardTypeID;//猫酷会员卡类型ID
    private String userName;//姓名
    private String gender;//性别（1: 男2:女0: 未知）
    private String age;//年龄
    private String birthday;//生日
    private String userCreateTime;//用户注册时间
    private String mallCardApplyTime;//会员开卡时
    private String thirdPartyCardID;//第三方会员ID
    private String thirdPartyCardNo;//第三方会员卡号
    private long cardDiscount;//权益折扣（0.1-0.99） [0.1代表一折]
    private int mallCardLevel;//会员等级
    private String interProviderType;//优惠券支付方式提供方类型

    public String getInterProviderType() {
        return interProviderType;
    }

    public void setInterProviderType(String interProviderType) {
        this.interProviderType = interProviderType;
    }

    public int getMallCardLevel() {
        return mallCardLevel;
    }

    public void setMallCardLevel(int mallCardLevel) {
        this.mallCardLevel = mallCardLevel;
    }

    public long getCardDiscount() {
        return cardDiscount;
    }

    public void setCardDiscount(long cardDiscount) {
        this.cardDiscount = cardDiscount;
    }

    public String getBizUid() {
        return bizUid;
    }

    public void setBizUid(String bizUid) {
        this.bizUid = bizUid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getwXOpenID() {
        return wXOpenID;
    }

    public void setwXOpenID(String wXOpenID) {
        this.wXOpenID = wXOpenID;
    }

    public String getMallCardNo() {
        return mallCardNo;
    }

    public void setMallCardNo(String mallCardNo) {
        this.mallCardNo = mallCardNo;
    }

    public String getMallCardName() {
        return mallCardName;
    }

    public void setMallCardName(String mallCardName) {
        this.mallCardName = mallCardName;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getMallCardTypeID() {
        return mallCardTypeID;
    }

    public void setMallCardTypeID(String mallCardTypeID) {
        this.mallCardTypeID = mallCardTypeID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getUserCreateTime() {
        return userCreateTime;
    }

    public void setUserCreateTime(String userCreateTime) {
        this.userCreateTime = userCreateTime;
    }

    public String getMallCardApplyTime() {
        return mallCardApplyTime;
    }

    public void setMallCardApplyTime(String mallCardApplyTime) {
        this.mallCardApplyTime = mallCardApplyTime;
    }

    public String getThirdPartyCardID() {
        return thirdPartyCardID;
    }

    public void setThirdPartyCardID(String thirdPartyCardID) {
        this.thirdPartyCardID = thirdPartyCardID;
    }

    public String getThirdPartyCardNo() {
        return thirdPartyCardNo;
    }

    public void setThirdPartyCardNo(String thirdPartyCardNo) {
        this.thirdPartyCardNo = thirdPartyCardNo;
    }
}
