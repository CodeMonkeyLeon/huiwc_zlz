package com.hstypay.enterprise.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hstypay.enterprise.activity.merchantInfo.MerchantInfoActivity;

/**
 * Created by admin on 2017/8/21.
 */

public class MerchantStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        Intent intentNew = new Intent();
        intentNew.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentNew.setClass(context, MerchantInfoActivity.class);
        context.startActivity(intentNew);
    }

}
