package com.hstypay.enterprise.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.LoginActivity;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.pledge.PledgeDetailActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.network.OkHttpUtil;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.SignUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import static com.hstypay.enterprise.utils.UIUtils.getString;

/**
 * Created by admin on 2017/8/21.
 */

public class NotificationClickReceiver extends BroadcastReceiver {

    private Gson mGson;
    private CommonNoticeDialog mDialogInfos;
    private Context mContext;
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String message = (String) msg.obj;
            MyToast.showToastShort(message);
        }
    };
    @Override
    public void onReceive(final Context context, Intent intent) {

        this.mContext = context;
        String order = intent.getStringExtra(Constants.INTENT_KEY_ORDERNO);
        int messageKind = intent.getIntExtra(Constants.INTENT_KEY_MESSAGEKIND, 0);
        mGson = new Gson();
        //时间戳
        long times = System.currentTimeMillis();
        String time = times + "";

        if (messageKind==90002 || messageKind==90003){
            //普通的正扫反扫收款成功
            getPushOrderDetailData(order,time);
        }else if (messageKind == 90005){
            //押金收款成功
            getPushPledgeDetailData(order,time);
        }

    }

    /**
     * 押金收款详情
     * */
    private void getPushPledgeDetailData(String order, String time) {
        Map<String, Object> map = new HashMap<>();
        if (!StringUtils.isEmptyOrNull(order)) {
            map.put("authNo", order);
        }
        String data = SignUtil.getInstance().createAppSign(map, time);
        String url = Constants.BASE_URL + "orderPreauth/pay/list";
        OkHttpUtil.postData(mContext, url, Constants.TAG_PUSH_PLEDGE_BILL, data, time, new OkHttpUtil.OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                PledgeBillsBean info = HstyCallBack(response, PledgeBillsBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        //getLoginDialog(mContext, info.getError().getMessage());
                                        MyToast.showToastLong(info.getError().getMessage());
                                        Message message = mHandler.obtainMessage();
                                        message.obj = info.getError().getMessage();
                                        message.sendToTarget();
                                        SpUtil.removeKey(Constants.SKEY);
                                        SpUtil.removeAll();
                                        MyApplication.getInstance().finishAllActivity();
                                        Intent intent = new Intent(MyApplication.getContext(), LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        mContext.startActivity(intent);
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        //MyToast.showToastShort(info.getError().getMessage());
                                        Message message = mHandler.obtainMessage();
                                        message.obj = info.getError().getMessage();
                                        message.sendToTarget();
                                    }
                                }
                            }
                        }
                    } else {
                        if (info.getData() != null && info.getData().size() > 0) {
                            Intent intent = new Intent(mContext, PledgeDetailActivity.class);
                            intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, info.getData().get(0));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }
                    }
                }
            }
        });
    }

    /**
     * 普通收款详情
     * */
    private void getPushOrderDetailData(String order, String time) {
        Map<String, Object> map = new HashMap<>();
        if (!StringUtils.isEmptyOrNull(order)) {
            map.put("orderNo", order);
        }
        String data = SignUtil.getInstance().createAppSign(map, time);
        String url = Constants.BASE_URL + "order/detail";
        OkHttpUtil.postData(mContext, url, Constants.PUSH_VOICE_DETAIL_TAG, data, time, new OkHttpUtil.OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {

                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        //getLoginDialog(mContext, info.getError().getMessage());
                                        Message message = mHandler.obtainMessage();
                                        message.obj = info.getError().getMessage();
                                        message.sendToTarget();
                                        SpUtil.removeKey(Constants.SKEY);
                                        SpUtil.removeAll();
                                        MyApplication.getInstance().finishAllActivity();
                                        Intent intent = new Intent(MyApplication.getContext(), LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        mContext.startActivity(intent);
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        Message message = mHandler.obtainMessage();
                                        message.obj = info.getError().getMessage();
                                        message.sendToTarget();
                                    }
                                }
                            }
                        }
                    } else {
                        PayBean.DataBean data1 = info.getData();
                        if (data1 != null) {
                            //成功
                            Intent intent = new Intent();
                            intent.putExtra(Constants.INTENT_BILL_DATA, data1);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setClass(mContext, PayDetailActivity.class);
                            mContext.startActivity(intent);
                        }
                    }
                }
            }
        });
    }

    public <T> T HstyCallBack(String arg0, Class clazz) {
        return (T) mGson.fromJson(arg0, clazz);
    }

    /*public void getLoginDialog(Context context, String title) {
        if (mDialogInfos == null) {
            mDialogInfos = new CommonNoticeDialog(context, title, getString(R.string.dialog_notice_button));
            mDialogInfos.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    MyApplication.getInstance().finishAllActivity();
                    mContext.startActivity(new Intent(MyApplication.getContext(), LoginActivity.class));
                }
            });
        }
        DialogHelper.resize(context, mDialogInfos);
        mDialogInfos.show();
    }*/
}
