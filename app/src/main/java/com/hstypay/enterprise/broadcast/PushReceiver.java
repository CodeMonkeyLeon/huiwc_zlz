package com.hstypay.enterprise.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.ToastDialog;
import com.hstypay.enterprise.utils.Constants;

/**
 * Created by admin on 2017/8/21.
 */

public class PushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        String title = intent.getStringExtra("title");
        String content = intent.getStringExtra("content");
        final String url = intent.getStringExtra("url");
        final String activityName = intent.getStringExtra("activityName");
        ToastDialog dialog = new ToastDialog(context, content, title);
        dialog.setOnClickToastListener(new ToastDialog.OnClickToastListener() {
            @Override
            public void clickToast() {
                Intent intent = new Intent(activityName);
                intent.putExtra(Constants.REGISTER_INTENT, url);
                context.startActivity(intent);
            }
        });
        dialog.showUpdateSuccessDialog(R.layout.view_push_toast);
    }

}
