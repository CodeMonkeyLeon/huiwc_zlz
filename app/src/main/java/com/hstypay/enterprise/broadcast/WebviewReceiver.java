package com.hstypay.enterprise.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.utils.Constants;

/**
 * Created by admin on 2017/8/21.
 */

public class WebviewReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        String url = intent.getStringExtra("url");
        Intent intentNew = new Intent();
        intentNew.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentNew.setClass(context, RegisterActivity.class);
        intentNew.putExtra(Constants.REGISTER_INTENT, url);
        context.startActivity(intentNew);
    }

}
