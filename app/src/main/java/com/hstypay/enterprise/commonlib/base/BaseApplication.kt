package com.hstypay.enterprise.commonlib.base

import android.app.Application

/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2020-04-14 13:47
 **/
open class BaseApplication : Application() {


    companion object {
        lateinit var mInstance: BaseApplication
    }


    override fun onCreate() {
        super.onCreate()
        mInstance=this

    }
}