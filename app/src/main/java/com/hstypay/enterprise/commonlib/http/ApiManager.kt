package com.hstypay.enterprise.commonlib.http

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.hstypay.enterprise.app.MyApplication
import com.hstypay.enterprise.commonlib.http.gson.CustomConverterFactory
import com.hstypay.enterprise.commonlib.http.gson.ListDefaultAdapter
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * @Author dean.zeng
 * @Description ApiManager
 * @Date 2019/9/29 16:06
 **/
class ApiManager private constructor() {

    var mService: AppService? = null

    companion object {
        private var instance: ApiManager? = null
            get() {
                if (field == null) {
                    field = ApiManager()
                }
                return field
            }

        @JvmStatic
        fun get(): ApiManager {
            return instance!!
        }
    }


    /**
     * 封装配置API
     */
    fun getService(): AppService {
        if (mService == null) {
            val retrofit = Retrofit.Builder()
                    .client(getClient(OkHttpClient.Builder()))
                    .baseUrl(AppService.HOST)
                    .addConverterFactory(GsonConverterFactory.create(buildGson()))
//                .addConverterFactory(CustomConverterFactory.create(buildGson()))

                    .build()
            mService = retrofit.create(AppService::class.java)
        }
        return mService!!
    }


    private fun getClient(builder: OkHttpClient.Builder): OkHttpClient {
        builder.hostnameVerifier { _, _ -> true }
        //设置超时
        val time = 10_000L
        val cookieJar: ClearableCookieJar = PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(MyApplication.getContext()))
        builder.apply {
            connectTimeout(time, TimeUnit.MILLISECONDS)
            writeTimeout(time, TimeUnit.MILLISECONDS)
            readTimeout(time, TimeUnit.MILLISECONDS)
            addInterceptor(FormBodyToJsonInterceptor())
            cookieJar(cookieJar)
            //错误重连
            retryOnConnectionFailure(true)
        }

        return builder.build()
    }

    /**
     * @return buildGson
     */
    private fun buildGson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(List::class.java, ListDefaultAdapter())
                .create()
    }


}