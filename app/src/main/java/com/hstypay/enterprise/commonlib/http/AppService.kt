package com.hstypay.enterprise.commonlib.http


import com.hstypay.enterprise.bean.*
import com.hstypay.enterprise.utils.Constants
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2020-04-14 14:28
 **/
interface AppService {
    companion object {
        val HOST = Constants.BASE_URL
    }

    /**
     * 全部门店
     */
    @FormUrlEncoded
    @POST("store/findStores")
    suspend fun findStores(@Field("pageSize") pageSize: String, @Field("currentPage") currentPage: String, @Field("merchantDataType") merchantDataType: String): ApiRes<List<StoreList>>

    /**
     *  微收银激活码管理
     *  @param pageSize 当前页
     *  @param currentPage 分页大小
     *  @param activationStatus 激活状态：-1-全部；0-未激活；1-已激活
     *  @param storeMerchantId 门店ID，空代表全部
     */
    @FormUrlEncoded
    @POST("activationcode/list")
    suspend fun activationCodeList(@Field("pageSize") pageSize: String, @Field("currentPage") currentPage: String,
                                   @Field("activationStatus") activationStatus: String,
                                   @Field("storeMerchantId") storeMerchantId: String): ApiRes<ListData<ActivationCode>>

    /**
     *  微收银激活码详情
     *  @param id 当前页
     */
    @FormUrlEncoded
    @POST("activationcode/detail")
    suspend fun getActivationCodeDetails(@Field("id") id: Long):ApiRes<ActivationCode>


    /**
     *  微收银激活码修改
     *  @param id
     */
    @FormUrlEncoded
    @POST("activationcode/update")
    suspend fun activationCodeUpdate(@Field("id") id: Long,@Field("storeMerchantShortName") storeMerchantShortName: String):ApiRes<Any>


    /**
     *  微收银激活码修改
     *  @param
     */
    @FormUrlEncoded
    @POST("activationcode/add")
    suspend fun addActivationCode(@Field("storeMerchantId") storeMerchantId: String,@Field("storeMerchantShortName") storeMerchantShortName: String):ApiRes<Any>



    /**
     *  获取收银员列表
     *  @param
     */
    @FormUrlEncoded
    @POST("store/find/user/list")
    suspend fun getCashierList(@Field("storeMerchantId") storeMerchantId: String):ApiRes<List<CashierItem>>
}