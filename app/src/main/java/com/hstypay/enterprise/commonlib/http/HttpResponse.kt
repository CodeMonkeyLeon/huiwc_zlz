package com.hstypay.enterprise.commonlib.http

import android.support.annotation.StringDef


/**
 * @Author dean.zeng
 * @Description 状态类
 * @Date 2020-04-14 18:11
 **/
class HttpResponse(
    /**
     * 是否弹出错误
     */
    val isShowError: Boolean = true,

    /**
     * 是否显示Loading
     */
    val isLoading: Boolean = true
) {
    companion object {
        const val SHOW_LOADING = "show_loading"
        const val SHOW_ERROR = "show_error"
        const val RE_LOGIN = "re_login"
        const val HIDE_LOADING = "hide_loading"

        inline fun build(block: Builder.() -> Unit) = Builder().apply(block).build()
    }


    //状态控制
    @State
    var state = SHOW_LOADING

    var msg = ""


    fun setState1(@State state1: String) {
        this.state = state1
    }


    class Builder {
        /**
         * 是否弹出错误
         */
        var isShowError: Boolean = true

        /**
         * 是否显示Loading
         */
        var isLoading: Boolean = true

        fun build() = HttpResponse(isShowError, isLoading)
    }

    @StringDef(
            SHOW_LOADING,
            HIDE_LOADING,
            SHOW_ERROR
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class State
}

