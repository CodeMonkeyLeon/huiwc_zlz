package com.hstypay.enterprise.commonlib.http.gson

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit

class CustomConverterFactory private constructor(private val gson: Gson) : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type?,
        annotations: Array<Annotation>?,
        retrofit: Retrofit?
    ): Converter<ResponseBody, *> {
        val adapter = gson.getAdapter(TypeToken.get(type!!))
        // attention here!
        return CustomResponseConverter(gson, adapter)
    }

    companion object {

        fun create(): CustomConverterFactory {
            return create(Gson())
        }

        // Guarding public API nullability.
        fun create(gson: Gson?): CustomConverterFactory {
            if (gson == null) throw NullPointerException("gson == null")
            return CustomConverterFactory(gson)
        }
    }


}
