package com.hstypay.enterprise.commonlib.http.gson

import com.google.gson.*
import com.hstypay.enterprise.utils.Lists
import java.lang.Exception
import java.lang.reflect.Type
import java.util.*


/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2020-07-22 14:59
 **/
class ListDefaultAdapter : JsonDeserializer<List<*>> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): List<*> {
        return if (json.isJsonArray) {
            val newGson = Gson()
            newGson.fromJson(json, typeOfT)
        } else {
            Collections.EMPTY_LIST
        }
    }

}