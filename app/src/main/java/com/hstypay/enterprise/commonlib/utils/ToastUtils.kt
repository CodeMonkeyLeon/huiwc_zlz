package com.hstypay.enterprise.commonlib.utils

import android.widget.Toast
import com.hstypay.enterprise.commonlib.base.BaseApplication

/**
 * @Author dean.zeng
 * @Description 吐司相关工具类
 * @Date 2019/9/29 17:24
 **/
object ToastUtils {

    private var mToast: Toast? = null

    @JvmStatic
    fun showShortToast(c: CharSequence) {
        if (mToast == null) {
            mToast = Toast.makeText(BaseApplication.mInstance, c, Toast.LENGTH_SHORT)
//            mToast!!.setGravity(Gravity.CENT, 0, 0)
        } else {
            mToast!!.setText(c)
        }
        mToast!!.show()
    }

    fun showLongToast(c: CharSequence) {
        if (mToast == null) {
            mToast = Toast.makeText(BaseApplication.mInstance, c, Toast.LENGTH_LONG)
//            mToast!!.setGravity(Gravity.CENTER, 0, 0)
        } else {
            mToast!!.setText(c)
        }
        mToast!!.show()
    }

}