package com.hstypay.enterprise.commonlib.widget

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import com.hstypay.enterprise.R
import com.hstypay.enterprise.utils.DisplayUtil
import com.hstypay.enterprise.utils.UIUtils
import kotlinx.android.synthetic.main.load_progress_info.view.*

/**
 * @Author dean.zeng
 * @Description 加载
 * @Date 2019/9/29 17:09
 **/
class LoadingDialog(private val mContext: Context, val text: CharSequence = UIUtils.getString(R.string.public_loading)) : Dialog(mContext, R.style.my_dialog) {


    fun showLoading(): LoadingDialog {
        val loadingDialog = LoadingDialog(mContext)
        loadingDialog.show()
        loadingDialog.setCanceledOnTouchOutside(false)
        loadingDialog.setCanceledOnTouchOutside(false)
        loadingDialog.setCancelable(false)
        return loadingDialog
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        window.setBackgroundDrawableResource(android.R.color.transparent)
        val inflater = LayoutInflater.from(context)
        val v = inflater.inflate(R.layout.layout_loding_dialog, null) // 得到加载view dialog_common
        v.tv_dialog.text = text
        val lp = window.attributes
        lp.alpha = 0.9f
        window.attributes = lp
        setContentView(v)
        lp.width = DisplayUtil.dip2Px(mContext, 110f)
        window.attributes = lp
    }

    override fun dismiss() {
        if (mContext is Activity && !mContext.isFinishing) {
            super.dismiss() //调用超类对应方法
        }
    }
}

































