package com.hstypay.enterprise.editmenu;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.umeng.socialize.utils.ContextUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 描述:菜单数据控制助手，模拟本地数据库
 */
public class MenuHelper {

    private static int mMenuVersion = 4;//菜单版本号，每次修改、删除菜单要更改下版本号。20210610-v3

    /*分组的标签*/
    public static final String GROUP_HOME = "group_home";//首页展示GROUP
    public static final String GROUP_TOOL = "group_tool";//常用工具，收款工具GROUP
    public static final String GROUP_BUSINESS = "group_business";//智慧经营，店铺经营GROUP

    public static final class MenuItemId {
        public static final int ACTIVE_ID = 2;//会员折扣
        public static final int STORECODE_ID = 3;//收款码
        public static final int CASHIER_ID = 4;//员工管理
        public static final int BOUNTY_ID = 5;//奖励金
        public static final int HB_ID = 7;//花呗分期
        public static final int PLEDGE_ID = 8;//押金收款
        public static final int SCANORDER_ID = 9;//扫码点单
        public static final int DEVICE_ID = 10;//设备管理
        public static final int RECEIPT_ID = 12;//收款单
        public static final int SENDHOME_ID = 13;//威点单
        public static final int PAYSITE_ID = 14;//收银点
        public static final int PAY_ID = 15;//扫码收款
        public static final int VERIFY_ID = 17;//核销明细
        public static final int TICKETSERVICE_ID = 18;//去开票
        public static final int VIP_SERVICE_ID = 19;//会员服务
        public static final int STORE_MANAGE_ID = 20;//门店管理
        public static final int PC_CJ_ID = 21;//pc收银插件
        public static final int DEVICE_MALL_ID = 22;//设备商城
        public static final int SMART_MANAGEMENT = 23;//智慧经营

    }


    /**
     * 根据菜单ID获取菜单图片资源id，由于菜单id等信息会持久化到文件，而图片资源id会随着项目资源的改变而变化,
     * 所以不能将图片资源id也持久化到文件，就需要显示时去获取图片资源id
     */
    public static int getMenuImgResource(int menuId) {
        int res = R.drawable.shape_bg_item_menu_default;
        switch (menuId) {
            case MenuHelper.MenuItemId.ACTIVE_ID:
                res = R.mipmap.ic_menu_memberdiscount;
                break;
            case MenuHelper.MenuItemId.CASHIER_ID:
                res = R.mipmap.ic_menu_worker;
                break;
            case MenuHelper.MenuItemId.STORECODE_ID:
                res = R.mipmap.ic_menu_moneyscan;
                break;
            case MenuHelper.MenuItemId.BOUNTY_ID:
                res = R.mipmap.pic_home_bounty;//图片是旧版的，大小不适
                break;
            case MenuHelper.MenuItemId.HB_ID:
                res = R.mipmap.ic_menu_flowerstaging;
                break;
            case MenuHelper.MenuItemId.PLEDGE_ID:
                res = R.mipmap.ic_menu_depositcollection;
                break;
            case MenuHelper.MenuItemId.SCANORDER_ID:
                res = R.mipmap.ic_menu_scanfood;
                break;
            case MenuHelper.MenuItemId.DEVICE_ID:
                res = R.mipmap.ic_menu_devie;
                break;
            case MenuHelper.MenuItemId.RECEIPT_ID:
                res = R.mipmap.ic_menu_moneylist;
                break;
            case MenuHelper.MenuItemId.SENDHOME_ID:
                res = R.mipmap.ic_menu_homeservice;
                break;
            case MenuHelper.MenuItemId.PAYSITE_ID:
                res = R.mipmap.ic_menu_cashierpoint;
                break;
            case MenuHelper.MenuItemId.PAY_ID:
                res = R.mipmap.ic_menu_scan_receive;
                break;
            case MenuHelper.MenuItemId.VERIFY_ID:
                res = R.mipmap.ic_menu_exchange;
                break;
            case MenuHelper.MenuItemId.TICKETSERVICE_ID:
                res = R.mipmap.ic_menu_invoiceservice;
                break;
            case MenuHelper.MenuItemId.VIP_SERVICE_ID:
                res = R.mipmap.ic_menu_memberservice;
                break;
            case MenuItem.ITEM_ID_EDIT:
                res = R.mipmap.ic_edit_grey;
                break;
            case MenuItem.ITEM_ID_MORE:
                res = R.mipmap.ic_menu_more;
                break;
            case MenuHelper.MenuItemId.STORE_MANAGE_ID:
                res = R.mipmap.ic_menu_storemanagement;
                break;
            case MenuHelper.MenuItemId.PC_CJ_ID:
                res = R.mipmap.ic_menu_cashierplug;
                break;
            case MenuHelper.MenuItemId.DEVICE_MALL_ID:
                res = R.mipmap.ic_menu_deviceshop;
                break;
            case MenuHelper.MenuItemId.SMART_MANAGEMENT:
                res = R.mipmap.ic_smart_management;
                break;
        }

        return res;
    }


    /**
     * 给菜单menuItem设置角标iv
     *
     * @param menuItem
     * @param iv
     */
    public static void setMenuRightMarkIcon(MenuItem menuItem, ImageView iv) {
        int id = menuItem.getId();
        if (id == MenuHelper.MenuItemId.HB_ID) {
            //花呗分期
            iv.setVisibility(View.VISIBLE);
            iv.setImageResource(R.mipmap.icon_recommendation);
        } else if (/*id == MenuHelper.MenuItemId.PAYSITE_ID || */id == MenuHelper.MenuItemId.SENDHOME_ID) {
            //收银点、威点单
            iv.setVisibility(View.VISIBLE);
            iv.setImageResource(R.mipmap.img_new_flag);
        } else {
            iv.setVisibility(View.GONE);
        }
    }


    /**
     * 更新保存的菜单数据
     * 注意：每次修改、删除菜单需要更改下版本号 mMenuVersion
     */
    public static void setMenuData() {
        Map<Integer, MenuItem> menuItemSrcs = new HashMap<Integer, MenuItem>();
        MenuItem active = new MenuItem(MenuHelper.GROUP_BUSINESS, "会员折扣", MenuHelper.MenuItemId.ACTIVE_ID);
        menuItemSrcs.put(active.getId(), active);
        MenuItem storeCode = new MenuItem(MenuHelper.GROUP_TOOL, "收款码", MenuHelper.MenuItemId.STORECODE_ID);
        menuItemSrcs.put(storeCode.getId(), storeCode);
        MenuItem cashier = new MenuItem(MenuHelper.GROUP_TOOL, "员工管理", MenuHelper.MenuItemId.CASHIER_ID);
        menuItemSrcs.put(cashier.getId(), cashier);
        MenuItem bounty = new MenuItem(MenuHelper.GROUP_TOOL, "奖励金", MenuHelper.MenuItemId.BOUNTY_ID);
        menuItemSrcs.put(bounty.getId(), bounty);
        MenuItem hb = new MenuItem(MenuHelper.GROUP_BUSINESS, "花呗分期", MenuHelper.MenuItemId.HB_ID);
        menuItemSrcs.put(hb.getId(), hb);
        MenuItem pledge = new MenuItem(MenuHelper.GROUP_TOOL, "押金收款", MenuHelper.MenuItemId.PLEDGE_ID);
        menuItemSrcs.put(pledge.getId(), pledge);
        MenuItem scanOrder = new MenuItem(MenuHelper.GROUP_BUSINESS, "扫码点单", MenuHelper.MenuItemId.SCANORDER_ID);
        menuItemSrcs.put(scanOrder.getId(), scanOrder);
        MenuItem device = new MenuItem(MenuHelper.GROUP_TOOL, "设备管理", MenuHelper.MenuItemId.DEVICE_ID);
        menuItemSrcs.put(device.getId(), device);
        MenuItem receipt = new MenuItem(MenuHelper.GROUP_TOOL, "收款单", MenuHelper.MenuItemId.RECEIPT_ID);
        menuItemSrcs.put(receipt.getId(), receipt);
        MenuItem sendHome = new MenuItem(MenuHelper.GROUP_BUSINESS, "威点单", MenuHelper.MenuItemId.SENDHOME_ID);
        menuItemSrcs.put(sendHome.getId(), sendHome);
        MenuItem paySite = new MenuItem(MenuHelper.GROUP_TOOL, "收银点", MenuHelper.MenuItemId.PAYSITE_ID);
        menuItemSrcs.put(paySite.getId(), paySite);
        MenuItem pay = new MenuItem(MenuHelper.GROUP_TOOL, "扫码收款", MenuHelper.MenuItemId.PAY_ID);
        menuItemSrcs.put(pay.getId(), pay);
        MenuItem verify = new MenuItem(MenuHelper.GROUP_TOOL, "兑换记录", MenuHelper.MenuItemId.VERIFY_ID);
        menuItemSrcs.put(verify.getId(), verify);
        MenuItem ticketService = new MenuItem(MenuHelper.GROUP_BUSINESS, "去开票", MenuHelper.MenuItemId.TICKETSERVICE_ID);
        menuItemSrcs.put(ticketService.getId(), ticketService);
        MenuItem vipservice = new MenuItem(MenuHelper.GROUP_BUSINESS, "会员服务", MenuHelper.MenuItemId.VIP_SERVICE_ID);
        menuItemSrcs.put(vipservice.getId(), vipservice);
        MenuItem storemanage = new MenuItem(MenuHelper.GROUP_TOOL, "门店管理", MenuHelper.MenuItemId.STORE_MANAGE_ID);//门店管理
        menuItemSrcs.put(storemanage.getId(), storemanage);
        MenuItem pcPlug = new MenuItem(MenuHelper.GROUP_TOOL, "收银插件", MenuHelper.MenuItemId.PC_CJ_ID);//收银插件
        menuItemSrcs.put(pcPlug.getId(), pcPlug);
        MenuItem deviceMall = new MenuItem(MenuHelper.GROUP_TOOL, "设备商城", MenuHelper.MenuItemId.DEVICE_MALL_ID);//设备商城
        menuItemSrcs.put(deviceMall.getId(), deviceMall);
        MenuItem smartManagement = new MenuItem(MenuHelper.GROUP_BUSINESS, "智慧经营", MenuItemId.SMART_MANAGEMENT);//智慧经营
        menuItemSrcs.put(smartManagement.getId(), smartManagement);

        int sp_menu_version = SpStayUtil.getInt(MyApplication.getContext(), Constants.SP_NAME_MENU_VERSION, 0);
        if (mMenuVersion > sp_menu_version) {
            //清除已保存的分组数据，后面再重新添加
            MenuHelper.deletePreferGroop(MenuHelper.GROUP_HOME);
            MenuHelper.deletePreferGroop(MenuHelper.GROUP_TOOL);
            MenuHelper.deletePreferGroop(MenuHelper.GROUP_BUSINESS);
        }

        //根据配置接口更新分类数据，包含首页展示的数据
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_PAY)) {
//            payList.add(pay);
            if (!MenuHelper.containsWithHome(pay.getGroup(), pay.getId())) {//之前有该菜单，后来又没了(即deleteItem)，现在给添加进去
                MenuHelper.addItem(MenuHelper.GROUP_HOME, pay);//扫码收款要放到首页展示栏，但它本质上属于常用工具
            } else {
                //do noting  菜单存在首页或者分类页就不要去改变它所处的分组和位置
            }
        } else {
            MenuHelper.deleteItem(pay.getGroup(), pay.getId());
            MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pay.getId());
        }


        if (AppHelper.getAppType() == 2 && isShowVip()) {
            if (!MenuHelper.containsWithHome(vipservice.getGroup(), vipservice.getId())) {
                MenuHelper.addItem(vipservice.getGroup(), vipservice);//会员服务
            }
        } else {
            MenuHelper.deleteItem(vipservice.getGroup(), vipservice.getId());
            MenuHelper.deleteItem(MenuHelper.GROUP_HOME, vipservice.getId());
        }


        //根据角色和配置接口更新分类数据，包含首页展示的数据
        if (MyApplication.getIsCasher()) {
            //收银员：
            //收款工具
            if ("2".equals(MyApplication.getPledgeOpenStatus()) && MyApplication.getConfig().contains(Constants.CONFIG_DEPOSIT)) {
//                payList.add(pledge);
                if (!MenuHelper.containsWithHome(pledge.getGroup(), pledge.getId())) {
                    MenuHelper.addItem(pledge.getGroup(), pledge);
                }
            } else {
                MenuHelper.deleteItem(pledge.getGroup(), pledge.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pledge.getId());
            }


            if (MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
//                payList.add(storeCode);
                if (!MenuHelper.containsWithHome(storeCode.getGroup(), storeCode.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,storeCode);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, storeCode);
                }
            } else {
                MenuHelper.deleteItem(storeCode.getGroup(), storeCode.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, storeCode.getId());
            }
            if (AppHelper.getApkType() == 0) {
//                payList.add(receipt);
                if (!MenuHelper.containsWithHome(receipt.getGroup(), receipt.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,receipt);
//                    MenuHelper.addItem(MenuHelper.GROUP_HOME,receipt);
                    MenuHelper.addItem(receipt.getGroup(), receipt);

                }
            } else {
                MenuHelper.deleteItem(receipt.getGroup(), receipt.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, receipt.getId());
            }

            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_SHOW_WSY, false) && MyApplication.getConfig().contains(Constants.CONFIG_PC_CODE_MANAGER)) {

                if (!MenuHelper.containsWithHome(pcPlug.getGroup(), pcPlug.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,storeCode);
                    MenuHelper.addItem(pcPlug.getGroup(), pcPlug);
                }
            } else {
                MenuHelper.deleteItem(pcPlug.getGroup(), pcPlug.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pcPlug.getId());
            }


            if (MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ)) {
//                storeList.add(hb);
                if (!MenuHelper.containsWithHome(hb.getGroup(), hb.getId())) {
                    MenuHelper.addItem(hb.getGroup(), hb);
                }
            } else {
                MenuHelper.deleteItem(hb.getGroup(), hb.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, hb.getId());
            }

            //智慧经营
            if (MyApplication.getConfig().contains(Constants.CONFIG_DEVICE_MANAGE)) {
//                storeList.add(device);
                if (!MenuHelper.containsWithHome(device.getGroup(), device.getId())) {
//                    MenuHelper.addItem(device.getGroup(),device);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, device);
                }
            } else {
                MenuHelper.deleteItem(device.getGroup(), device.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, device.getId());
            }

//            if (MyApplication.isOpenFuncard()) {
//                storeList.add(verify);
            if (!MenuHelper.containsWithHome(verify.getGroup(), verify.getId())) {
                MenuHelper.addItem(verify.getGroup(), verify);
            }

            /*}else {
                MenuHelper.deleteItem(verify.getGroup(),verify.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME,verify.getId());
            }*/

            if (MyApplication.isOpenMember() && !(SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM) || SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST))) {
//                riseList.add(active);
                if (!MenuHelper.containsWithHome(active.getGroup(), active.getId())) {
                    MenuHelper.addItem(active.getGroup(), active);
                }
            } else {
                MenuHelper.deleteItem(active.getGroup(), active.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, active.getId());
            }

            if (MyApplication.isOpenScanOrder() && MyApplication.getConfig().contains(Constants.CONFIG_SCAN_ORDER_FOOD)) {
//                riseList.add(scanOrder);
                if (!MenuHelper.containsWithHome(scanOrder.getGroup(), scanOrder.getId())) {
                    MenuHelper.addItem(scanOrder.getGroup(), scanOrder);
                }
            } else {
                MenuHelper.deleteItem(scanOrder.getGroup(), scanOrder.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, scanOrder.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_GET_HOME) && MyApplication.isOpenSendHome()) {
//                riseList.add(sendHome);
                if (!MenuHelper.containsWithHome(sendHome.getGroup(), sendHome.getId())) {
                    MenuHelper.addItem(sendHome.getGroup(), sendHome);
                }
            } else {
                MenuHelper.deleteItem(sendHome.getGroup(), sendHome.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, sendHome.getId());
            }

            if (MyApplication.getIsBounty()) {
//                riseList.add(bounty);
                if (!MenuHelper.containsWithHome(bounty.getGroup(), bounty.getId())) {
                    MenuHelper.addItem(bounty.getGroup(), bounty);
                }
            } else {
                MenuHelper.deleteItem(bounty.getGroup(), bounty.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, bounty.getId());
            }

        } else if (MyApplication.getIsManager()) {
            //店长:
            //收款工具
            if ("2".equals(MyApplication.getPledgeOpenStatus()) && MyApplication.getConfig().contains(Constants.CONFIG_DEPOSIT)) {
//                payList.add(pledge);
                if (!MenuHelper.containsWithHome(pledge.getGroup(), pledge.getId())) {
                    MenuHelper.addItem(pledge.getGroup(), pledge);
                }
            } else {
                MenuHelper.deleteItem(pledge.getGroup(), pledge.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pledge.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
//                payList.add(storeCode);
                if (!MenuHelper.containsWithHome(storeCode.getGroup(), storeCode.getId())) {
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, storeCode);
//                    MenuHelper.addItem(storeCode.getGroup(),storeCode);
                }
            } else {
                MenuHelper.deleteItem(storeCode.getGroup(), storeCode.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, storeCode.getId());
            }

            if (AppHelper.getApkType() == 0) {
//                payList.add(receipt);
                if (!MenuHelper.containsWithHome(receipt.getGroup(), receipt.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,receipt);
                    MenuHelper.addItem(receipt.getGroup(), receipt);
                }
            } else {
                MenuHelper.deleteItem(receipt.getGroup(), receipt.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, receipt.getId());
            }
            if (MyApplication.getConfig().contains(Constants.CONFIG_CASH_POINT)) {
//                storeList.add(paySite);
                if (!MenuHelper.containsWithHome(paySite.getGroup(), paySite.getId())) {
                    MenuHelper.addItem(paySite.getGroup(), paySite);
                }
            } else {
                MenuHelper.deleteItem(paySite.getGroup(), paySite.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, paySite.getId());

            }

            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_SHOW_WSY, false) && MyApplication.getConfig().contains(Constants.CONFIG_PC_CODE_MANAGER)) {
                if (!MenuHelper.containsWithHome(pcPlug.getGroup(), pcPlug.getId())) {
                    MenuHelper.addItem(pcPlug.getGroup(), pcPlug);//收银插件
                }
            } else {
                MenuHelper.deleteItem(pcPlug.getGroup(), pcPlug.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pcPlug.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_INVOICE_SERVICE)) {
//                riseList.add(ticketService);
                if (!MenuHelper.containsWithHome(ticketService.getGroup(), ticketService.getId())) {
                    MenuHelper.addItem(ticketService.getGroup(), ticketService);
                }
            } else {
                MenuHelper.deleteItem(ticketService.getGroup(), ticketService.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, ticketService.getId());
            }

            if (MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ)) {
//                storeList.add(hb);
                if (!MenuHelper.containsWithHome(hb.getGroup(), hb.getId())) {
                    MenuHelper.addItem(hb.getGroup(), hb);
                }
            } else {
                MenuHelper.deleteItem(hb.getGroup(), hb.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, hb.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_DEVICE_MANAGE)) {
//                storeList.add(device);
                if (!MenuHelper.containsWithHome(device.getGroup(), device.getId())) {
//                    MenuHelper.addItem(device.getGroup(),device);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, device);
                }
            } else {
                MenuHelper.deleteItem(device.getGroup(), device.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, device.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_STAFF_MANAGE)) {
//                storeList.add(cashier);
                if (!MenuHelper.containsWithHome(cashier.getGroup(), cashier.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_BUSINESS,cashier);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, cashier);
                }
            } else {
                MenuHelper.deleteItem(cashier.getGroup(), cashier.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, cashier.getId());
            }


//            if (MyApplication.isOpenFuncard()) {
//                storeList.add(verify);
            if (!MenuHelper.containsWithHome(verify.getGroup(), verify.getId())) {
                MenuHelper.addItem(verify.getGroup(), verify);
            }

           /* }else {
                MenuHelper.deleteItem(verify.getGroup(),verify.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME,verify.getId());
            }*/

            if (MyApplication.isOpenMember() && !(SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM) || SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST))) {
//                riseList.add(active);
                if (!MenuHelper.containsWithHome(active.getGroup(), active.getId())) {
                    MenuHelper.addItem(active.getGroup(), active);
                }
            } else {
                MenuHelper.deleteItem(active.getGroup(), active.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, active.getId());
            }


            if (MyApplication.isOpenScanOrder() && MyApplication.getConfig().contains(Constants.CONFIG_SCAN_ORDER_FOOD)) {
//                riseList.add(scanOrder);
                if (!MenuHelper.containsWithHome(scanOrder.getGroup(), scanOrder.getId())) {
                    MenuHelper.addItem(scanOrder.getGroup(), scanOrder);
                }
            } else {
                MenuHelper.deleteItem(scanOrder.getGroup(), scanOrder.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, scanOrder.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_GET_HOME) && MyApplication.isOpenSendHome()) {
//                riseList.add(sendHome);
                if (!MenuHelper.containsWithHome(sendHome.getGroup(), sendHome.getId())) {
                    MenuHelper.addItem(sendHome.getGroup(), sendHome);
                }
            } else {
                MenuHelper.deleteItem(sendHome.getGroup(), sendHome.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, sendHome.getId());
            }

            if (MyApplication.getIsBounty()) {
//                riseList.add(bounty);
                if (!MenuHelper.containsWithHome(bounty.getGroup(), bounty.getId())) {
                    MenuHelper.addItem(bounty.getGroup(), bounty);
                }
            } else {
                MenuHelper.deleteItem(bounty.getGroup(), bounty.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, bounty.getId());
            }


        } else if (MyApplication.getIsMerchant()) {
            //商户：
            //收款工具
            if ("2".equals(MyApplication.getPledgeOpenStatus()) && MyApplication.getConfig().contains(Constants.CONFIG_DEPOSIT)) {
//                payList.add(pledge);
                if (!MenuHelper.containsWithHome(pledge.getGroup(), pledge.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,new MenuItem(MenuHelper.GROUP_TOOL,R.mipmap.pic_home_pledge, MenuHelper.MenuItemStr.PLEDGE_Str, MenuHelper.MenuItemId.PLEDGE_ID));
                    MenuHelper.addItem(pledge.getGroup(), pledge);
                }
            } else {
                MenuHelper.deleteItem(pledge.getGroup(), pledge.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pledge.getId());
            }

            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_BELONG_HSHF)
                    && (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST)
                    || SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM))
                    && !MenuHelper.containsWithHome(smartManagement.getGroup(), smartManagement.getId())) {
                MenuHelper.addItem(smartManagement.getGroup(), smartManagement);
            }

            if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_MANAGE)) {
                if (!MenuHelper.containsWithHome(storemanage.getGroup(), storemanage.getId())) {
                    MenuHelper.addItem(storemanage.getGroup(), storemanage);//门店管理
                }
            } else {
                MenuHelper.deleteItem(storemanage.getGroup(), storemanage.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, storemanage.getId());
            }


            if (MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
//                payList.add(storeCode);
                if (!MenuHelper.containsWithHome(storeCode.getGroup(), storeCode.getId())) {
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, storeCode);
                }
            } else {
                MenuHelper.deleteItem(storeCode.getGroup(), storeCode.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, storeCode.getId());
            }
            if (AppHelper.getApkType() == 0) {
//                payList.add(receipt);
                if (!MenuHelper.containsWithHome(receipt.getGroup(), receipt.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_HOME,receipt);
                    MenuHelper.addItem(receipt.getGroup(), receipt);

                }
            } else {
                MenuHelper.deleteItem(receipt.getGroup(), receipt.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, receipt.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_CASH_POINT)) {
//                storeList.add(paySite);
                if (!MenuHelper.containsWithHome(paySite.getGroup(), paySite.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_BUSINESS,new MenuItem(MenuHelper.GROUP_BUSINESS,R.mipmap.pic_home_site, MenuHelper.MenuItemStr.PAYSITE_Str, MenuHelper.MenuItemId.PAYSITE_ID));
                    MenuHelper.addItem(paySite.getGroup(), paySite);
                }
            } else {
                MenuHelper.deleteItem(paySite.getGroup(), paySite.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, paySite.getId());

            }

            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_SHOW_WSY, false) && MyApplication.getConfig().contains(Constants.CONFIG_PC_CODE_MANAGER)) {
                if (!MenuHelper.containsWithHome(pcPlug.getGroup(), pcPlug.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,storeCode);
                    MenuHelper.addItem(pcPlug.getGroup(), pcPlug);//收银插件
                }
            } else {
                MenuHelper.deleteItem(pcPlug.getGroup(), pcPlug.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, pcPlug.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_INVOICE_SERVICE)) {
//                riseList.add(ticketService);
                if (!MenuHelper.containsWithHome(ticketService.getGroup(), ticketService.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.icon_function_ticket, MenuHelper.MenuItemStr.TICKETSERVICE_Str, MenuHelper.MenuItemId.TICKETSERVICE_ID));
                    MenuHelper.addItem(ticketService.getGroup(), ticketService);
                }
            } else {
                MenuHelper.deleteItem(ticketService.getGroup(), ticketService.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, ticketService.getId());
            }

            if (MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ)) {
//                storeList.add(hb);
                if (!MenuHelper.containsWithHome(hb.getGroup(), hb.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_BUSINESS,new MenuItem(MenuHelper.GROUP_BUSINESS,R.mipmap.pic_home_huabei, MenuHelper.MenuItemStr.HB_Str, MenuHelper.MenuItemId.HB_ID));
                    MenuHelper.addItem(hb.getGroup(), hb);

                }
            } else {
                MenuHelper.deleteItem(hb.getGroup(), hb.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, hb.getId());
            }

            //智慧经营
            if (MyApplication.getConfig().contains(Constants.CONFIG_DEVICE_MANAGE)) {
//                storeList.add(device);
                if (!MenuHelper.containsWithHome(device.getGroup(), device.getId())) {
//                    MenuHelper.addItem(device.getGroup(),device);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, device);
                }
            } else {
                MenuHelper.deleteItem(device.getGroup(), device.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, device.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_STAFF_MANAGE)) {
//                storeList.add(cashier);
                if (!MenuHelper.containsWithHome(cashier.getGroup(), cashier.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_BUSINESS,new MenuItem(MenuHelper.GROUP_BUSINESS,R.mipmap.pic_home_cashier, MenuHelper.MenuItemStr.CASHIER_Str, MenuHelper.MenuItemId.CASHIER_ID));
//                    MenuHelper.addItem(MenuHelper.GROUP_BUSINESS,cashier);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, cashier);

                }
            } else {
                MenuHelper.deleteItem(cashier.getGroup(), cashier.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, cashier.getId());
            }


//            if (MyApplication.isOpenFuncard()) {
//                storeList.add(verify);
            if (!MenuHelper.containsWithHome(verify.getGroup(), verify.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_BUSINESS, new MenuItem(MenuHelper.GROUP_BUSINESS,R.mipmap.icon_function_verify, MenuHelper.MenuItemStr.VERIFY_Str, MenuHelper.MenuItemId.VERIFY_ID));
                MenuHelper.addItem(verify.getGroup(), verify);
            }

            /*}else {
                MenuHelper.deleteItem(verify.getGroup(),verify.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME,verify.getId());
            }*/

            if (MyApplication.isOpenMember() && !(SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM) || SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST))) {
//                riseList.add(active);
                if (!MenuHelper.containsWithHome(active.getGroup(), active.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.pic_home_vip_active, MenuHelper.MenuItemStr.ACTIVE_Str, MenuHelper.MenuItemId.ACTIVE_ID));
                    MenuHelper.addItem(active.getGroup(), active);

                }
            } else {
                MenuHelper.deleteItem(active.getGroup(), active.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, active.getId());
            }

            if (MyApplication.isOpenScanOrder() && MyApplication.getConfig().contains(Constants.CONFIG_SCAN_ORDER_FOOD)) {
//                riseList.add(scanOrder);
                if (!MenuHelper.containsWithHome(scanOrder.getGroup(), scanOrder.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.pic_home_scan_order, MenuHelper.MenuItemStr.SCANORDER_Str, MenuHelper.MenuItemId.SCANORDER_ID));
                    MenuHelper.addItem(scanOrder.getGroup(), scanOrder);
                }
            } else {
                MenuHelper.deleteItem(scanOrder.getGroup(), scanOrder.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, scanOrder.getId());
            }

            if (MyApplication.getConfig().contains(Constants.CONFIG_GET_HOME)/* && MyApplication.isOpenSendHome()*/) {
//                riseList.add(sendHome);
                if (!MenuHelper.containsWithHome(sendHome.getGroup(), sendHome.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.pic_home_vlife, MenuHelper.MenuItemStr.SENDHOME_Str, MenuHelper.MenuItemId.SENDHOME_ID));
                    MenuHelper.addItem(sendHome.getGroup(), sendHome);
                }
            } else {
                MenuHelper.deleteItem(sendHome.getGroup(), sendHome.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, sendHome.getId());
            }

            if (MyApplication.getIsBounty()) {
//                riseList.add(bounty);
                if (!MenuHelper.containsWithHome(bounty.getGroup(), bounty.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.pic_home_bounty, MenuHelper.MenuItemStr.BOUNTY_Str, MenuHelper.MenuItemId.BOUNTY_ID));
                    MenuHelper.addItem(bounty.getGroup(), bounty);
                }
            } else {
                MenuHelper.deleteItem(bounty.getGroup(), bounty.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, bounty.getId());
            }


        } else if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            //其它角色
            //收款工具

//            payList.add(storeCode);
            if (!MenuHelper.containsWithHome(storeCode.getGroup(), storeCode.getId())) {
//                MenuHelper.addItem(MenuHelper.GROUP_TOOL,new MenuItem(MenuHelper.GROUP_TOOL,R.mipmap.pic_home_qrcode, MenuHelper.MenuItemStr.STORECODE_Str, MenuHelper.MenuItemId.STORECODE_ID));
//                MenuHelper.addItem(MenuHelper.GROUP_TOOL,storeCode);
                MenuHelper.addItem(MenuHelper.GROUP_HOME, storeCode);
            }

            if (AppHelper.getApkType() == 0) {
//                payList.add(receipt);
                if (!MenuHelper.containsWithHome(receipt.getGroup(), receipt.getId())) {
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,new MenuItem(MenuHelper.GROUP_TOOL,R.mipmap.pic_home_receipt, MenuHelper.MenuItemStr.RECEIPT_Str, MenuHelper.MenuItemId.RECEIPT_ID));
//                    MenuHelper.addItem(MenuHelper.GROUP_TOOL,receipt);
                    MenuHelper.addItem(MenuHelper.GROUP_HOME, receipt);
                }
            } else {
                MenuHelper.deleteItem(receipt.getGroup(), receipt.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, receipt.getId());
            }

            if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_MANAGE)) {
                if (!MenuHelper.containsWithHome(storemanage.getGroup(), storemanage.getId())) {
                    MenuHelper.addItem(storemanage.getGroup(), storemanage);//门店管理
                }
            } else {
                MenuHelper.deleteItem(storemanage.getGroup(), storemanage.getId());
                MenuHelper.deleteItem(MenuHelper.GROUP_HOME, storemanage.getId());
            }

//            storeList.add(device);
            if (!MenuHelper.containsWithHome(device.getGroup(), device.getId())) {
                MenuHelper.addItem(device.getGroup(), device);
            }

//            storeList.add(cashier);
            if (!MenuHelper.containsWithHome(cashier.getGroup(), cashier.getId())) {
                MenuHelper.addItem(MenuHelper.GROUP_HOME, cashier);

            }

//            riseList.add(sendHome);
            if (!MenuHelper.containsWithHome(sendHome.getGroup(), sendHome.getId())) {
//                MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.pic_home_vlife, MenuHelper.MenuItemStr.SENDHOME_Str, MenuHelper.MenuItemId.SENDHOME_ID));
                MenuHelper.addItem(sendHome.getGroup(), sendHome);
            }
//            riseList.add(ticketService);
            if (!MenuHelper.containsWithHome(ticketService.getGroup(), ticketService.getId())) {
//                MenuHelper.addItem(MenuHelper.GROUP_RISE,new MenuItem(MenuHelper.GROUP_RISE,R.mipmap.icon_function_ticket, MenuHelper.MenuItemStr.TICKETSERVICE_Str, MenuHelper.MenuItemId.TICKETSERVICE_ID));
                MenuHelper.addItem(ticketService.getGroup(), ticketService);
            }
        }

        if (ConfigUtil.getMiniEnable()) {
            if (!MenuHelper.containsWithHome(deviceMall.getGroup(), deviceMall.getId())) {
                MenuHelper.addItem(deviceMall.getGroup(), deviceMall);
            } else {
                //do noting
            }
        } else {
            MenuHelper.deleteItem(deviceMall.getGroup(), deviceMall.getId());
            MenuHelper.deleteItem(MenuHelper.GROUP_HOME, deviceMall.getId());
        }

        SpStayUtil.putInt(MyApplication.getContext(), Constants.SP_NAME_MENU_VERSION, mMenuVersion);
    }

    private static boolean isShowVip() {
        return !TextUtils.isEmpty(MyApplication.getIsSuccessData()) && (!SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM)
                && MyApplication.isOpenMember());
    }

    /**
     * 初始化菜单数据
     */
    public static void init() {
        MenuHelper helper = new MenuHelper();
//        helper.parseJSONData();
        setInit(true);
    }

    /**
     * 用于保存本地数据的文件名字
     */
    private static final String PREFERENCE_MENU_DATA_NAME = "menu_data";
    /**
     * 是否已经进行过初始化的字段名
     */
    private static final String PREFERENCE_HAS_EVER_INIT = "has_ever_init";

    /**
     * 获取本地数据的文件
     *
     * @return
     */
    public static SharedPreferences getMenuDataConfig() {
        return ContextUtil.getContext().getSharedPreferences(PREFERENCE_MENU_DATA_NAME + "_" + MyApplication.getUserId(), Context.MODE_PRIVATE);
    }

    /**
     * 清空本地数据文件里面的内容
     */
    public static void clearMenuDataConfig() {
        getMenuDataConfig().edit().clear().commit();
    }

    public static boolean hasEverInit() {
        return getMenuDataConfig().getBoolean(PREFERENCE_HAS_EVER_INIT, false);
    }

    public static void setInit(boolean isInit) {
        getMenuDataConfig().edit().putBoolean(PREFERENCE_HAS_EVER_INIT, isInit).commit();
    }

    /*----------------------------原始方法start-----------------------------------*/

    /**
     * 将List转换为JsonString保存进SharedPreference
     *
     * @param group
     * @param list
     */
    private static void savePreferMenuListData(String group, List<MenuItem> list) {
        SharedPreferences.Editor editor = getMenuDataConfig().edit();
        editor.putString(group, JSON.toJSONString(list));
        editor.commit();
    }

    /**
     * 从SharedPreference里面取出JsonString,再转换为List
     *
     * @param group
     * @return
     */
    private static List<MenuItem> getPreferMenuListData(String group) {
        try {
            String jsonStr = getMenuDataConfig().getString(group, "");
            JSONArray array = JSONArray.parseArray(jsonStr);
            return array.toJavaList(MenuItem.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        return null;
        return new ArrayList<MenuItem>();
    }

    /**
     * 从本地数据缓存列表里面删除一个item
     *
     * @param group
     * @param item
     */
    public static void deleteItem(String group, MenuItem item) {
        List<MenuItem> list = getPreferMenuListData(group);
        for (MenuItem i : list) {
            if (i.getId() == item.getId()) {
                list.remove(i);
                break;
            }
        }
        savePreferMenuListData(group, list);
    }

    public static void deleteItem(String group, long id) {
        List<MenuItem> list = getPreferMenuListData(group);
        for (MenuItem i : list) {
            if (i.getId() == id) {
                list.remove(i);
                break;
            }
        }
        savePreferMenuListData(group, list);
    }

    /**
     * 从本地数据元素里面添加一个item
     *
     * @param group
     * @param item
     */
    public static void addItem(String group, MenuItem item) {
        List<MenuItem> list = getPreferMenuListData(group);
        if (!contains(list, item)) {
            list.add(item);
            if (!group.equals(GROUP_HOME)) {
                Collections.sort(list, new Comparator<MenuItem>() {
                    @Override
                    public int compare(MenuItem o1, MenuItem o2) {
                        return (o1.getId() - o2.getId());
                    }
                });
            }
            savePreferMenuListData(group, list);
        }
    }

    public static boolean contains(List<MenuItem> list, MenuItem item) {
        if (list != null && list.size() > 0) {
            for (MenuItem i : list) {
                if (i.getId() == item.getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    //分组group菜单是否包括了对应id号的菜单
    public static boolean contains(String group, long id) {
        List<MenuItem> list = getPreferMenuListData(group);
        if (list != null && list.size() > 0) {
            for (MenuItem i : list) {
                if (i.getId() == id) {
                    return true;
                }
            }
        }
        return false;
    }

    //分组group菜单或首页菜单是否包括了对应id号的菜单
    public static boolean containsWithHome(String group, long id) {
        List<MenuItem> list = getPreferMenuListData(group);
        if (list != null && list.size() > 0) {
            for (MenuItem i : list) {
                if (i.getId() == id) {
                    return true;
                }
            }
        }
        return contains(GROUP_HOME, id);
    }


    /*----------------------------原始方法end-----------------------------------*/

    /*----------------------------衍生方法start-----------------------------------*/
    public static void savePreferHomeList(List<MenuItem> list) {
        savePreferMenuListData(GROUP_HOME, list);
    }

    public static void savePreferToolList(List<MenuItem> list) {
        savePreferMenuListData(GROUP_TOOL, list);
    }

    public static void savePreferBusinessList(List<MenuItem> list) {
        savePreferMenuListData(GROUP_BUSINESS, list);
    }


    public static List<MenuItem> getPreferHomeList() {
        return getPreferMenuListData(GROUP_HOME);
    }

    public static List<MenuItem> getPreferToolList() {
        return getPreferMenuListData(GROUP_TOOL);
    }

    public static List<MenuItem> getPreferBusinessList() {
        return getPreferMenuListData(GROUP_BUSINESS);
    }


    public static void addPreferHomeItem(MenuItem item) {
        addItem(GROUP_HOME, item);
    }

    public static void addPreferToolItem(MenuItem item) {
        addItem(GROUP_TOOL, item);
    }

    public static void addPreferBusinessItem(MenuItem item) {
        addItem(GROUP_BUSINESS, item);
    }


    //清除某组数据
    public static void deletePreferGroop(String group) {
        savePreferMenuListData(group, new ArrayList<>());
    }


    public static void deletePreferHomeItem(MenuItem item) {
        deleteItem(GROUP_HOME, item);
    }

    public static void deletePreferToolItem(MenuItem item) {
        deleteItem(GROUP_TOOL, item);
    }

    public static void deletePreferBusinessItem(MenuItem item) {
        deleteItem(GROUP_BUSINESS, item);
    }

    /*----------------------------衍生方法end-----------------------------------*/
}
