package com.hstypay.enterprise.editmenu;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hstypay.enterprise.utils.Constants;

/**
 * @author kuangzeyu
 * @time 2021/3/18
 * @desc recyclerview 的item间隔
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        /*outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildPosition(view) == 0){
            outRect.top = space;
        }*/
        if (parent.getChildAdapterPosition(view) >= Constants.MENUNUM){//每行5个
            outRect.top = space;
        }
    }
}
