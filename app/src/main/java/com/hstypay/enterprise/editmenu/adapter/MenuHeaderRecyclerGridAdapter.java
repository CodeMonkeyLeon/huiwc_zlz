package com.hstypay.enterprise.editmenu.adapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.adapter.holder.MenuHeaderRecyclerGridHolder;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.editmenu.recyclerview.BaseDraggableRecyclerAdapter;

import java.util.List;


/**
 * 描述:编辑页面的头部的子元素适配器
 * 可以拖拽的RecyclerView适配器
 */
public class MenuHeaderRecyclerGridAdapter extends BaseDraggableRecyclerAdapter<MenuHeaderRecyclerGridHolder, MenuItem> {
    private int maxSlots;
    private OnDeleteClickListener mOnDeleteClickListener;
    private boolean isEditState = false;//是否为编辑状态

    public void setOnDeleteClickListener(OnDeleteClickListener onDeleteClickListener) {
        mOnDeleteClickListener = onDeleteClickListener;
    }

    /**
     * 可以拖拽的RecyclerView适配器
     *
     * @param recyclerItems 数据列表
     * @param recyclerView  与改适配器匹配的RecyclerView
     */
    public MenuHeaderRecyclerGridAdapter(List<MenuItem> recyclerItems, RecyclerView recyclerView) {
        super(recyclerItems, recyclerView);
    }

    @Override
    public MenuHeaderRecyclerGridHolder createRecyclerViewHolder(ViewGroup parent, int viewType) {
        return new MenuHeaderRecyclerGridHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_grid_header, parent, false));
    }

    public int getMaxSlots() {
        return maxSlots;
    }

    public void setMaxSlots(int maxSlots) {
        this.maxSlots = maxSlots;
    }

    public void setEditState(boolean editState) {
        isEditState = editState;
    }

    @Override
    public void bindViewHolder(final MenuHeaderRecyclerGridHolder holder, MenuItem item) {
        final MenuItem menuItem = item;
//        holder.mIv_delete_edit.setVisibility(showEditIcon ? View.VISIBLE : View.GONE);
        if (!isEditState){
            holder.mLl_menu_grid.setBackground(new ColorDrawable(Color.TRANSPARENT));
            holder.mIv_delete_edit.setVisibility(View.GONE);
            //设置角标
            MenuHelper.setMenuRightMarkIcon(item,holder.mIvItemFlagMenuHome);
        }else {
            holder.mLl_menu_grid.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.shape_bg_item_menu_grid));
            holder.mIv_delete_edit.setVisibility(View.VISIBLE);
            holder.mIv_delete_edit.setImageResource(R.mipmap.ic_minus_circle_fill);
            //隐藏角标
            holder.mIvItemFlagMenuHome.setVisibility(View.GONE);
        }

        holder.mIv_delete_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDeleteClickListener != null) {
                    mOnDeleteClickListener.onDeleteClick(v, menuItem, holder.getAdapterPosition());
                }
            }
        });
        holder.mTv_item_name_menu_home.setText(item.getName());
        holder.mIv_item_icon_menu_home.setImageResource(MenuHelper.getMenuImgResource(item.getId()));
    }

    @Override
    public int getItemCount() {
        return mRecyclerItems == null ? 0 : mRecyclerItems.size();
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(MenuHeaderRecyclerGridHolder holder, int position) {
        //设置可拖动的范围
        /*MenuItem lastItem = mRecyclerItems.get(getItemCount()-1);
        if(lastItem.getViewType() == MenuItem.VIEW_TYPE_DASH){
        //除最后一个item范围的位置不可拖动，其余的范围都可以
            return new ItemDraggableRange(0, getItemCount() - 2);
        }else{
        //所有item范围都可以拖动，默认的
            return new ItemDraggableRange(0, getItemCount() - 1);
        }*/
        if (isEditState){
            return new ItemDraggableRange(0, getItemCount() - 1);
        }else {
            return new ItemDraggableRange(-1, -1);
        }

    }

    /**
     * 描述:删除脚标单击监听器
     */
    public interface OnDeleteClickListener {
        void onDeleteClick(View v, MenuItem item, int position);
    }
}
