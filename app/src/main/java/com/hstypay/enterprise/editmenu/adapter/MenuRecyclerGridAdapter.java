package com.hstypay.enterprise.editmenu.adapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.adapter.holder.MenuRecyclerGridHolder;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.editmenu.recyclerview.BaseSimpleRecyclerAdapter;

import java.util.List;


/**
 * 描述:编辑页面主体元素的子元素适配器
 */
public class MenuRecyclerGridAdapter extends BaseSimpleRecyclerAdapter<MenuRecyclerGridHolder, MenuItem> {

    private boolean isEditState = false;//是否为编辑状态

    public MenuRecyclerGridAdapter(List<MenuItem> recyclerItems) {
        super(recyclerItems);
    }

    public void setEditState(boolean editState) {
        isEditState = editState;
    }

    @Override
    public MenuRecyclerGridHolder createRecyclerViewHolder(ViewGroup parent, int viewType) {
        return new MenuRecyclerGridHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_grid_header, parent, false));
    }

    @Override
    public void bindViewHolder(MenuRecyclerGridHolder holder, MenuItem item) {

        if (!isEditState){
            holder.mLl_menu_grid.setBackground(new ColorDrawable(Color.TRANSPARENT));
            holder.mIv_add_edit.setVisibility(View.GONE);
            //设置角标
            MenuHelper.setMenuRightMarkIcon(item,holder.mIvItemFlagMenuHome);
        }else {
            holder.mLl_menu_grid.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.shape_bg_item_menu_grid));
            holder.mIv_add_edit.setVisibility(View.VISIBLE);
            //隐藏角标
            holder.mIvItemFlagMenuHome.setVisibility(View.GONE);
        }

        holder.mTv_item_menu_home.setText(item.getName());
        holder.mIv_item_menu_home.setImageResource(MenuHelper.getMenuImgResource(item.getId()));
    }

    @Override
    public int getItemCount() {
        return mRecyclerItems == null ? 0 : mRecyclerItems.size();
    }
}
