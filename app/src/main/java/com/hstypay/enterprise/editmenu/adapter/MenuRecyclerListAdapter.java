package com.hstypay.enterprise.editmenu.adapter;

import android.os.SystemClock;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.SpacesItemDecoration;
import com.hstypay.enterprise.editmenu.adapter.holder.MenuRecyclerListHolder;
import com.hstypay.enterprise.editmenu.entity.EditItem;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.editmenu.recyclerview.BaseSimpleRecyclerAdapter;
import com.hstypay.enterprise.editmenu.recyclerview.OnRecyclerItemClickListener;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DensityUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 描述:编辑页面的列表适配器
 */
public class MenuRecyclerListAdapter extends BaseSimpleRecyclerAdapter<MenuRecyclerListHolder, EditItem> {
    private Map<String,MenuRecyclerGridAdapter> mAdapterMap=new HashMap<>();//用于存放子列表的adapter的map
    private Map<String,MenuRecyclerListHolder> mHolderMap=new HashMap<>();//用于存放子列表的adapter的map

    private OnRecyclerItemClickListener<MenuItem> mChildItemClickListener;
    private boolean isEditState = false;//是否为编辑状态

    public void setChildItemClickListener(OnRecyclerItemClickListener<MenuItem> childItemClickListener) {
        mChildItemClickListener = childItemClickListener;
    }
    public void setEditState(boolean editState) {
        isEditState = editState;
    }
    public MenuRecyclerListAdapter(List<EditItem> recyclerItems) {
        super(recyclerItems);
        for (int i=recyclerItems.size()-1;i>=0;i--){
            EditItem editItem = recyclerItems.get(i);
            List<MenuItem> menuItemList = editItem.getMenuItemList();
            if (menuItemList==null || menuItemList.size()==0){
                recyclerItems.remove(i);
            }
        }
    }

    @Override
    public MenuRecyclerListHolder createRecyclerViewHolder(ViewGroup parent, int viewType) {
        return new MenuRecyclerListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_edit,parent,false));
    }

    @Override
    public void bindViewHolder(MenuRecyclerListHolder holder, EditItem item) {
        holder.tv_group_name.setText(item.getGroupTitle());
        if (holder.getAdapterPosition()==getItemCount()){
            holder.mViewBottomGap.setVisibility(View.VISIBLE);
        }else {
            holder.mViewBottomGap.setVisibility(View.GONE);
        }
        MenuRecyclerGridAdapter adapter = new MenuRecyclerGridAdapter(item.getMenuItemList());
//        if (holder.recyclerView.getAdapter()==null){
//        }
        adapter.setEditState(isEditState);
        adapter.setOnRecyclerItemClickListener(mChildItemClickListener);
        holder.recyclerView.setLayoutManager(new GridLayoutManager(holder.recyclerView.getContext(), Constants.MENUNUM));
        holder.recyclerView.setAdapter(adapter);
        mAdapterMap.put(item.getGroup(),adapter);
        mHolderMap.put(item.getGroup(),holder);
        isNotifying = false;
    }

    @Override
    public int getItemCount() {
        return mRecyclerItems==null?0:mRecyclerItems.size();
    }

    public void notifyChildDataChanged(String group, MenuItem item){
        MenuRecyclerGridAdapter adapter=mAdapterMap.get(group);
        if(adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }

    boolean isNotifying = false;
    //editItem 是item所属的组
    public void notifyChildDataAdded(String group, MenuItem item,EditItem editItem){
        if (isNotifying){
            return;
        }
        //UI上先添加分组
        if (editItem!=null && mRecyclerItems!=null){
            if (!mRecyclerItems.contains(editItem)){
                if (mRecyclerItems.size()==0){
                    mRecyclerItems.add(editItem);
                }else {
                    mRecyclerItems.add(editItem.getPosition(),editItem);
                }
                isNotifying = true;
                notifyDataSetChanged();
            }else {
                isNotifying = false;
            }
        }else {
            isNotifying = false;
        }

        //再添加组里的菜单
        MenuRecyclerGridAdapter adapter1=mAdapterMap.get(group);
        if (isNotifying || adapter1==null){
            //recycleview正在刷新中,需要等待mAdapterMap赋值完成
            new Thread(){
                @Override
                public void run() {
                    super.run();
                    MenuRecyclerGridAdapter adapter2=mAdapterMap.get(group);
                    while (isNotifying || adapter2==null){
                        //等待刷新完成，mAdapterMap赋值完成
                        SystemClock.sleep(100);
                        adapter2=mAdapterMap.get(group);
                    }
                    MenuRecyclerListHolder holder = mHolderMap.get(group);
                    holder.recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            MenuRecyclerGridAdapter adapter=mAdapterMap.get(group);
                            if(adapter!=null){
                                if(!adapter.getRecyclerItems().contains(item)){
                                    adapter.getRecyclerItems().add(item);
                                    adapter.notifyDataSetChanged();

                                }
                            }
                        }
                    });

                }
            }.start();
        }else {
            MenuRecyclerGridAdapter adapter=mAdapterMap.get(group);
            if(adapter!=null){
                if(!adapter.getRecyclerItems().contains(item)){
                    adapter.getRecyclerItems().add(item);

                    List<MenuItem> list = adapter.getRecyclerItems();
                    Collections.sort(list, new Comparator<MenuItem>() {
                        @Override
                        public int compare(MenuItem o1, MenuItem o2) {
                            return (o1.getId()-o2.getId());
                        }
                    });

                    adapter.notifyDataSetChanged();

                }
            }
        }
    }

    //editItem 是item所属的组
    public void notifyChildDataRemoved(String group, MenuItem item,EditItem editItem){
        if (isNotifying){
            return;
        }
        MenuRecyclerGridAdapter adapter=mAdapterMap.get(group);
        if(adapter!=null){
            adapter.getRecyclerItems().remove(item);
            adapter.notifyDataSetChanged();

            //UI上删除组
            if (editItem!=null&& mRecyclerItems!=null){
                int itemCount = adapter.getItemCount();
                if (itemCount==0){//没有内容item了才要移除分组
                    if (mRecyclerItems.contains(editItem)){
                        mRecyclerItems.remove(editItem);
                        notifyDataSetChanged();
                    }
                }
            }
        }
    }
}
