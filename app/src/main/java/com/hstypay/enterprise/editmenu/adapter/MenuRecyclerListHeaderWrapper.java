package com.hstypay.enterprise.editmenu.adapter;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.SpacesItemDecoration;
import com.hstypay.enterprise.editmenu.adapter.holder.MenuHeaderRecyclerListHolder;
import com.hstypay.enterprise.editmenu.entity.EditItem;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.editmenu.recyclerview.BaseHeaderFooterRecyclerAdapterWrapper;
import com.hstypay.enterprise.editmenu.recyclerview.OnRecyclerItemClickListener;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DensityUtils;


/**
 * 描述:编辑页的头部包裹适配器，因为只有头部，所以尾部的代码不用写
 */
public class MenuRecyclerListHeaderWrapper extends BaseHeaderFooterRecyclerAdapterWrapper<EditItem, EditItem, MenuHeaderRecyclerListHolder, MenuHeaderRecyclerListHolder> implements RecyclerViewDragDropManager.OnItemDragEventListener {
    private OnRecyclerItemClickListener<MenuItem> mOnChildItemClickListener;
    private MenuHeaderRecyclerGridAdapter.OnDeleteClickListener mOnDeleteClickListener;
    private RecyclerViewDragDropManager mDragDropManager;
    private MenuHeaderRecyclerGridAdapter adapter;//原装适配器
    private RecyclerView.Adapter dragAdapter;//对原装适配器封装包裹后的实现了拖拽功能的适配器
    private int itemMoveMode = RecyclerViewDragDropManager.ITEM_MOVE_MODE_DEFAULT;
    private boolean hasDragChanged;//优化刷新数据的参数，是否发生过拖拽
    private int maxSlots;
    private boolean isEditState = false;//是否为编辑状态
    private Vibrator mVibrator;

    public void setOnChildItemClickListener(OnRecyclerItemClickListener<MenuItem> onChildItemClickListener) {
        mOnChildItemClickListener = onChildItemClickListener;
    }

    public void setOnDeleteClickListener(MenuHeaderRecyclerGridAdapter.OnDeleteClickListener onDeleteClickListener) {
        mOnDeleteClickListener = onDeleteClickListener;
    }

    public int getMaxSlots() {
        return maxSlots;
    }

    public void setMaxSlots(int maxSlots) {
        this.maxSlots = maxSlots;
    }

    public void setEditState(boolean editState) {
        isEditState = editState;
    }

    /**
     * 具有设置Header和Footer功能的RecyclerView适配器
     *
     * @param adapter 原始的RecyclerView的适配器, 在原始adapter上添加Header和Footer
     */
    public MenuRecyclerListHeaderWrapper(Context context,RecyclerView.Adapter adapter) {
        super(adapter);
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public MenuHeaderRecyclerListHolder createHeaderViewHolder(ViewGroup parent, int viewType) {
        return new MenuHeaderRecyclerListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_menu_edit_recycler, parent, false));
    }

    @Override
    public MenuHeaderRecyclerListHolder createFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void bindHeaderViewHolder(MenuHeaderRecyclerListHolder headerViewHolder, EditItem headerItem) {
        headerViewHolder.tv_group_name.setText(headerItem.getGroupTitle());
        headerViewHolder.mTvTipDrag.setVisibility(isEditState? View.VISIBLE:View.GONE);
        adapter = new MenuHeaderRecyclerGridAdapter(headerItem.getMenuItemList(), headerViewHolder.recyclerView);
        adapter.setOnRecyclerItemClickListener(mOnChildItemClickListener);
        adapter.setOnDeleteClickListener(mOnDeleteClickListener);
        adapter.setMaxSlots(maxSlots);
        adapter.setEditState(isEditState);
        mDragDropManager = new RecyclerViewDragDropManager();
        mDragDropManager.setItemMoveMode(itemMoveMode);
        // Start dragging after long press
        mDragDropManager.setInitiateOnLongPress(true);
        mDragDropManager.setOnItemDragEventListener(null);
        mDragDropManager.setInitiateOnMove(false);
        mDragDropManager.setLongPressTimeout(750);
        mDragDropManager.setOnItemDragEventListener(this);
        // setup dragging item effects (NOTE: DraggableItemAnimator is required)
//        dragDropManager.setDragStartItemAnimationDuration(250);
//        dragDropManager.setDraggingItemAlpha(0.8f);
        mDragDropManager.setDraggingItemScale(1.1f);
//        dragDropManager.setDraggingItemRotation(15.0f);
        dragAdapter = mDragDropManager.createWrappedAdapter(adapter);
//        headerViewHolder.recyclerView.addItemDecoration(new SpacesItemDecoration((int) headerViewHolder.itemView.getContext().getResources().getDimension(R.dimen.menu_icon_top_gap)));
        headerViewHolder.recyclerView.setAdapter(dragAdapter);
        headerViewHolder.recyclerView.setLayoutManager(new GridLayoutManager(headerViewHolder.recyclerView.getContext(), Constants.MENUNUM));
        GeneralItemAnimator itemAnimator = new DraggableItemAnimator();
        headerViewHolder.recyclerView.setItemAnimator(itemAnimator);

        mDragDropManager.attachRecyclerView(headerViewHolder.recyclerView);//关键步骤，设置好DragDropManager和RecyclerView后将二者绑定实现拖拽功能

    }

    @Override
    public void bindFooterViewHolder(MenuHeaderRecyclerListHolder footerViewHolder, EditItem footerItem) {

    }

    @Override
    public void onItemDragStarted(int position) {
        mVibrator.vibrate(25);
    }

    @Override
    public void onItemDragPositionChanged(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDragFinished(int fromPosition, int toPosition, boolean result) {
        if (fromPosition != toPosition && result) {
            if (adapter != null) {
                MenuHelper.savePreferHomeList(adapter.getRecyclerItems());
                hasDragChanged = true;
            }
            /*int size = adapter.getRecyclerItems().size();
            for (int i = 0; i < size; i++) {
                if (adapter.getRecyclerItems().get(i).getViewType() == MenuItem.VIEW_TYPE_DASH) {
                    adapter.getRecyclerItems().remove(i);
                    break;
                }
            }
            if(adapter.getRecyclerItems().size()<maxSlots){
                MenuItem dashItem=new MenuItem();
                dashItem.setItemId(MenuItem.ITEM_ID_DASH);
                dashItem.setViewType(MenuItem.VIEW_TYPE_DASH);
                adapter.getRecyclerItems().add(dashItem);
                adapter.notifyDataSetChanged();
            }*/
        }
    }

    @Override
    public void onItemDragMoveDistanceUpdated(int offsetX, int offsetY) {

    }

    /**
     * 外部页面销毁之前调用
     */
    public void releaseDragManager() {
        if (mDragDropManager != null) {
            mDragDropManager.release();
            mDragDropManager = null;
        }
        if (dragAdapter != null) {
            WrapperAdapterUtils.releaseAll(dragAdapter);
            dragAdapter = null;
        }
    }

    /**
     * 是否发生过拖拽变化
     *
     * @return
     */
    public boolean isHasDragChanged() {
        return hasDragChanged;
    }

    public void setHasDragChanged(boolean hasDragChanged) {
        this.hasDragChanged = hasDragChanged;
    }

    /**
     * 通知刷新子列表数据
     */
    public void notifyChildDataChanged() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void notifyChildDataAdded(MenuItem item) {
        if (adapter != null) {
            if (!adapter.getRecyclerItems().contains(item)) {
                /*int size = adapter.getRecyclerItems().size();
                for (int i = 0; i < size; i++) {
                    MenuItem ii = adapter.getRecyclerItems().get(i);
                    if (ii.getId() == MenuItem.ITEM_ID_EDIT) {
                        adapter.getRecyclerItems().remove(i);
                        break;
                    }
                }*/
                if (item != null) {
                    adapter.getRecyclerItems().add(item);
                }
                /*if (adapter.getRecyclerItems().size() < maxSlots) {
                    MenuItem editItem = new MenuItem();
                    editItem.setId(MenuItem.ITEM_ID_EDIT);
                    editItem.setName(MenuItem.ITEM_NAME_EDIT);
                    editItem.setResId(R.mipmap.ic_edit_grey);
                    editItem.setGroup(MenuHelper.GROUP_HOME);
                    adapter.getRecyclerItems().add(editItem);
                }*/
                adapter.notifyDataSetChanged();
            }
        }
    }

    public void notifyChildDataRemoved(MenuItem item) {
        if (adapter != null) {
            adapter.getRecyclerItems().remove(item);
            /*int size = adapter.getRecyclerItems().size();
            boolean containsEdit = false;
            for (int i = 0; i < size; i++) {
                if (adapter.getRecyclerItems().get(i).getId() == MenuItem.ITEM_ID_EDIT) {
                    containsEdit = true;
                    break;
                }
            }
            if (!containsEdit) {
                MenuItem editItem = new MenuItem();
                editItem.setId(MenuItem.ITEM_ID_EDIT);
                editItem.setName(MenuItem.ITEM_NAME_EDIT);
                editItem.setResId(R.mipmap.ic_edit_grey);
                editItem.setGroup(MenuHelper.GROUP_HOME);
                adapter.getRecyclerItems().add(editItem);
            }*/
            adapter.notifyDataSetChanged();
        }
    }
}
