package com.hstypay.enterprise.editmenu.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.draggable.annotation.DraggableItemStateFlags;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.hstypay.enterprise.R;


/**
 * 描述:菜单编辑列表适配器的头部的ViewHolder<br>
 *     注意，一定要继承{@link AbstractDraggableItemViewHolder}或者照着这个类实现{@link DraggableItemViewHolder}
 *     才能有拖拽效果
 */
public class MenuHeaderRecyclerGridHolder extends RecyclerView.ViewHolder implements DraggableItemViewHolder {
    public ImageView iv_delete;

    @DraggableItemStateFlags
    private int mDragStateFlags;
    public final ImageView mIv_delete_edit;
    public final LinearLayout mLl_menu_grid;
    public final ImageView mIv_item_icon_menu_home;
    public final TextView mTv_item_name_menu_home;
    public final ImageView mIvItemFlagMenuHome;

    public MenuHeaderRecyclerGridHolder(View itemView) {
        super(itemView);//item_menu_grid_header
        mLl_menu_grid = itemView.findViewById(R.id.ll_menu_grid);
        mIv_item_icon_menu_home = itemView.findViewById(R.id.iv_item_icon_menu_home);
        mTv_item_name_menu_home = itemView.findViewById(R.id.tv_item_name_menu_home);
        mIv_delete_edit = itemView.findViewById(R.id.iv_delete_edit);
        mIvItemFlagMenuHome = itemView.findViewById(R.id.iv_item_flag_menu_home);
    }

    @Override
    public void setDragStateFlags(@DraggableItemStateFlags int flags) {
        mDragStateFlags=flags;
    }

    @Override
    @DraggableItemStateFlags
    public int getDragStateFlags() {
        return mDragStateFlags;
    }
}
