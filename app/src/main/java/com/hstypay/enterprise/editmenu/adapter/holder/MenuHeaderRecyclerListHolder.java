package com.hstypay.enterprise.editmenu.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.SpacesItemDecoration;


/**
 * 描述:编辑页面的列表Holder，和编辑页的头部列表holder
 */
public class MenuHeaderRecyclerListHolder extends RecyclerView.ViewHolder {
    public TextView tv_group_name;
    public RecyclerView recyclerView;
    public final TextView mTvTipDrag;

    public MenuHeaderRecyclerListHolder(View itemView) {
        super(itemView);//header_menu_edit_recycler
        tv_group_name= (TextView) itemView.findViewById(R.id.title);
        recyclerView= (RecyclerView) itemView.findViewById(R.id.recycler);
        mTvTipDrag = itemView.findViewById(R.id.tv_tip_drag);

        recyclerView.addItemDecoration(new SpacesItemDecoration((int) itemView.getContext().getResources().getDimension(R.dimen.menu_icon_top_gap)));
    }
}
