package com.hstypay.enterprise.editmenu.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;


/**
 * 描述:编辑页面的主体item的holder
 * <p>
 * 作者:陈俊森
 * @version 1.0
 */
public class MenuRecyclerGridHolder extends RecyclerView.ViewHolder {

    public final LinearLayout mLl_menu_grid;
    public final ImageView mIv_item_menu_home;
    public final TextView mTv_item_menu_home;
    public final ImageView mIv_add_edit;
    public final ImageView mIvItemFlagMenuHome;

    public MenuRecyclerGridHolder(View itemView) {
        super(itemView);//item_menu_grid_header(item_menu_grid)
        mLl_menu_grid = itemView.findViewById(R.id.ll_menu_grid);
        mIv_item_menu_home = itemView.findViewById(R.id.iv_item_icon_menu_home);
        mTv_item_menu_home = itemView.findViewById(R.id.tv_item_name_menu_home);
        mIv_add_edit = itemView.findViewById(R.id.iv_delete_edit);
        mIv_add_edit.setImageResource(R.mipmap.ic_plus_circle_fill);
        mIvItemFlagMenuHome = itemView.findViewById(R.id.iv_item_flag_menu_home);
    }
}
