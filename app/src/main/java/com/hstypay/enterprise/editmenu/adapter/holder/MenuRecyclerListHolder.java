package com.hstypay.enterprise.editmenu.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.editmenu.SpacesItemDecoration;


/**
 * 描述:编辑页面的列表Holder，和编辑页的头部列表holder
 */
public class MenuRecyclerListHolder extends RecyclerView.ViewHolder {
    public TextView tv_group_name;
    public RecyclerView recyclerView;
    public  View mViewBottomGap;

    public MenuRecyclerListHolder(View itemView) {
        super(itemView);//item_menu_edit
        tv_group_name= (TextView) itemView.findViewById(R.id.title);
        recyclerView= (RecyclerView) itemView.findViewById(R.id.recycler);
        mViewBottomGap = itemView.findViewById(R.id.view_bottom_gap);

        recyclerView.addItemDecoration(new SpacesItemDecoration((int) itemView.getContext().getResources().getDimension(R.dimen.menu_icon_top_gap)));
    }
}
