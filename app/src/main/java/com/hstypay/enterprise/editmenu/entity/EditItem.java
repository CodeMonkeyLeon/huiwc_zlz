package com.hstypay.enterprise.editmenu.entity;


import android.annotation.SuppressLint;

import com.hstypay.enterprise.editmenu.recyclerview.BaseRecyclerItem;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


/**
 * 描述:列表条目的实体类
 */
public class EditItem implements BaseRecyclerItem, Serializable {
    private String mGroup;
    private String mGroupTitle;
    private List<MenuItem> mMenuItemList;
    private int position;//分组的位置，如常用工具是0， 智慧经营是1
    public EditItem() {
    }

    public EditItem(String group, String groupTitle, List<MenuItem> menuItemList, int itemId,int position) {
        mGroup = group;
        mGroupTitle = groupTitle;
        mMenuItemList = menuItemList;
        this.itemId = itemId;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }


    public String getGroup() {
        return mGroup;
    }

    public void setGroup(String group) {
        mGroup = group;
    }

    public String getGroupTitle() {
        return mGroupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        mGroupTitle = groupTitle;
    }

    public List<MenuItem> getMenuItemList() {
        return mMenuItemList;
    }

    public void setMenuItemList(List<MenuItem> menuItemList) {
        mMenuItemList = menuItemList;
    }

    private int viewType;
    private int itemId;

    @Override
    public int getViewType() {
        return viewType;
    }

    @Override
    public int getItemId() {
        return itemId;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

}
