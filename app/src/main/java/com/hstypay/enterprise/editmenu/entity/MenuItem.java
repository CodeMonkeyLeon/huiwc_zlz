package com.hstypay.enterprise.editmenu.entity;

import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;

import com.hstypay.enterprise.editmenu.recyclerview.BaseRecyclerItem;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author kuangzeyu
 * @time 2021/3/11
 * @desc 菜单bean
 */
public class MenuItem implements BaseRecyclerItem, Serializable {

    public static final int ITEM_ID_EDIT=-999;
    public static final String ITEM_NAME_EDIT = "编辑";

    public static final int ITEM_ID_MORE = -1000;
    public static final String ITEM_NAME_MORE = "更多";


//    private int resId;//图标名称，不能是图片资源的id，因为当下次新增图片资源或减少图片资源时，以前图片的id会变化

    private String name;//名称

    private int id;//编号，唯一, 也是排序的依据

    private String group;//item所属组


    public MenuItem() {
    }

    public MenuItem(String group, String name, int id) {
        this.group = group;
        this.name = name;
        this.id = id;
    }

    public MenuItem(  String name, int id) {
        this.name = name;
        this.id = id;
    }


    public String getGroup() {

        return group == null ? "" : group;

    }

    public void setGroup(String group) {
        this.group = group;
    }


    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", group='" + group + '\'' +
                '}';
    }

    @Override
    public int getViewType() {
        return 0;
    }//只有一种viewtype

    @Override
    public int getItemId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return id == menuItem.id;
    }


    @SuppressLint("NewApi")
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static interface OnHomeMenuItemClickListener{
        void onMenuItemClick(MenuItem menuItem);
    }
}
