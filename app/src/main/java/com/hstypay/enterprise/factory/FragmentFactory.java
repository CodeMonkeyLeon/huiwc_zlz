package com.hstypay.enterprise.factory;

import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.fragment.BUserFragment;
import com.hstypay.enterprise.fragment.BillFragment;
import com.hstypay.enterprise.fragment.HomeFragment;
import com.hstypay.enterprise.fragment.MessageFragment;
import com.hstypay.enterprise.fragment.ReceiveFragment;
import com.hstypay.enterprise.fragment.ReportFragment;
import com.hstypay.enterprise.fragment.UserFragment;
import com.hstypay.enterprise.utils.AppHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:07
 * @描述: ${TODO}
 */

public class FragmentFactory {

    private static Map<Integer, BaseFragment> sFragmentMap = new HashMap<>();

    /*public static final int FRAGMENT_HOME   = 0; //主页
    public static final int FRAGMENT_MESSAGE = 1; //消息
    public static final int FRAGMENT_MINE   = 2; //我的*/

    /**
     * Fragment数量
     */
    //public static final int MAX_FRAGMENT_COUNT = FRAGMENT_MINE + 1;

    /**
     * 根据传入的Fragment类型获取相对应的Fragment
     *
     * @param fragmentType 碎片类型
     * @return 返回获取到的碎片实例
     */
    public static BaseFragment getFragment(int fragmentType) {

        BaseFragment baseFragment = sFragmentMap.get(fragmentType);
        if (baseFragment != null) {
            return baseFragment;
        }

        if (AppHelper.getAppType() == 2) {
            switch (fragmentType) {
                case 0:
                    baseFragment = new HomeFragment();
                    break;
                case 1:
                    baseFragment = new MessageFragment();
                    break;
                case 2:
                    if (AppHelper.getApkType()==1) {
                        baseFragment = new BUserFragment();
                    }else {
                        baseFragment = new UserFragment();
                    }
                    break;
            }
        }else if(AppHelper.getAppType() == 1){
            switch (fragmentType) {
                case 0:
                    baseFragment = new ReceiveFragment();
                    break;
                case 1:
                    baseFragment = new BillFragment();
                    break;
                case 2:
                    baseFragment = new ReportFragment();
                    break;
                case 3:
                    if (AppHelper.getApkType()==1) {
                        baseFragment = new BUserFragment();
                    }else {
                        baseFragment = new UserFragment();
                    }
                    break;
            }
        }

        sFragmentMap.put(fragmentType, baseFragment);
        return baseFragment;
    }

    public static int getFragmentCount(){
        int count = 0;
        if (AppHelper.getAppType() == 1){
            count = 4;
        }else if (AppHelper.getAppType() == 2){
            count = 3;
        }
        return count;
    }
}

