package com.hstypay.enterprise.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.dialog.CustomViewFullScreenDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.SettingActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.adapter.MenuHeaderRecyclerGridAdapter;
import com.hstypay.enterprise.editmenu.adapter.MenuRecyclerListAdapter;
import com.hstypay.enterprise.editmenu.adapter.MenuRecyclerListHeaderWrapper;
import com.hstypay.enterprise.editmenu.entity.EditItem;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.editmenu.recyclerview.OnRecyclerItemClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MenuClickUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author kuangzeyu
 * @time 2021/3/16
 * @desc APP应用
 */
public class ApplicationFragment  extends BaseFragment implements MenuHeaderRecyclerGridAdapter.OnDeleteClickListener {

    private View mRootView;
    private Context mContext;
    private TextView mTvCancelEditApp;
    private TextView mTvTitleEditApp;
    private TextView mTvFinishEditApp;
    private RecyclerView mRecyclerView;

    private List<MenuItem> mHomeList;//首页展示的数据，不属于分类原数据
    private List<MenuItem> mToolList ;//收款工具的数据
    private List<MenuItem> mBusinessList;//店铺经营的数据

    private List<MenuItem> mHomeListBake = new ArrayList<>();//备份数据，用于取消编辑，还原数据
    private List<MenuItem> mToolListBake = new ArrayList<>();//备份，用于取消编辑，还原数据
    private List<MenuItem> mBusinessListBake = new ArrayList<>();//备份，用于取消编辑，还原数据

    private List<EditItem> mEditList;//分类的数据
    private List<EditItem> mEditListBake = new ArrayList<>();//分类的数据的备份

    private MenuRecyclerListAdapter mListAdapter;//可编辑菜单列表，源数据菜单列表的外层适配器
    private MenuRecyclerListHeaderWrapper mListHeaderWrapper;//首页展示菜单列表的适配器

    static final int MAX_SLOTS = 10;//首页最多展示个数

    private boolean hasChangedListData;

    private boolean isEditState = false;//是否为编辑状态

    public boolean isEditState() {
        return isEditState;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        StatusBarUtil.setTranslucentStatusFontIconDark(getActivity(), mRootView);
        initData();
        EventBus.getDefault().register(this);
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.fragment_application, container, false);
        StatusBarUtil.setTranslucentStatusFontIconDark(getActivity(),  mRootView);
        initView();
        MenuHelper.setMenuData();
        //initData();
        initEvent();
        return mRootView;
    }

    private void initEvent() {
        mTvCancelEditApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消编辑
                cancelEdit();
            }
        });

        mTvFinishEditApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //完成编辑
                finishEdit();
            }
        });
    }

    //完成编辑
    private void finishEdit() {
        isEditState = false;
        initData(); //更新UI，备份数据并再次初始化
    }

    //取消编辑
    private void cancelEdit() {
        isEditState = false;
        //还原数据
        MenuHelper.savePreferHomeList(mHomeListBake);
        MenuHelper.savePreferToolList(mToolListBake);
        MenuHelper.savePreferBusinessList(mBusinessListBake);
        //更新UI，备份数据并再次初始化
        initData();
    }

    private void initData() {
        mHomeList = MenuHelper.getPreferHomeList();
        mToolList = MenuHelper.getPreferToolList();
        mBusinessList = MenuHelper.getPreferBusinessList();
        bakeData();
        if (!isEditState){
            MenuItem editItem = new MenuItem();
            editItem.setId(MenuItem.ITEM_ID_EDIT);
            editItem.setName(MenuItem.ITEM_NAME_EDIT);
            editItem.setGroup(MenuHelper.GROUP_HOME);
            mHomeList.remove(editItem);
            mHomeList.add(editItem);//添加编辑按钮
            mTvCancelEditApp.setVisibility(View.GONE);
            mTvFinishEditApp.setVisibility(View.GONE);
            mTvTitleEditApp.setText("应用");
        }else {
            MenuItem editItem = new MenuItem();
            editItem.setId(MenuItem.ITEM_ID_EDIT);
            mHomeList.remove(editItem);//移除编辑按钮
            mTvCancelEditApp.setVisibility(View.VISIBLE);
            mTvFinishEditApp.setVisibility(View.VISIBLE);
            mTvTitleEditApp.setText("编辑首页应用展示");
        }
        if (mListHeaderWrapper!=null){
            mListHeaderWrapper.releaseDragManager();
        }

        mEditList = new ArrayList<>();
        mEditList.add(new EditItem(MenuHelper.GROUP_TOOL, getResources().getString(R.string.app_group_tool), mToolList, 1,0));
        mEditList.add(new EditItem(MenuHelper.GROUP_BUSINESS, getResources().getString(R.string.app_group_business), mBusinessList, 2,1));
        mEditListBake.clear();
        mEditListBake.addAll(mEditList);

        mListAdapter = new MenuRecyclerListAdapter(mEditList);//可编辑菜单列表适配器
        mListAdapter.setEditState(isEditState);
        mListAdapter.setChildItemClickListener(new ListChildItemClickListener());

        mListHeaderWrapper = new MenuRecyclerListHeaderWrapper(getContext(),mListAdapter);//头部菜单列表适配器,它会加到MenuRecyclerListAdapter里去作为头部item
        mListHeaderWrapper.setMaxSlots(MAX_SLOTS);
        mListHeaderWrapper.setEditState(isEditState);
        mListHeaderWrapper.setOnChildItemClickListener(new HeaderChildItemClickListener());
        mListHeaderWrapper.setOnDeleteClickListener(this);
        mListHeaderWrapper.addHeader(new EditItem(MenuHelper.GROUP_HOME, getResources().getString(R.string.app_group_home), mHomeList,3,-1));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mListHeaderWrapper);

    }

    private void initView() {
        mTvCancelEditApp = mRootView.findViewById(R.id.tv_cancel_edit_app);
        mTvTitleEditApp = mRootView.findViewById(R.id.tv_title_edit_app);
        mTvFinishEditApp = mRootView.findViewById(R.id.tv_finish_edit_app);
        mRecyclerView = mRootView.findViewById(R.id.recyclerView);

        mRecyclerView.getItemAnimator().setAddDuration(0);
        mRecyclerView.getItemAnimator().setChangeDuration(0);
        mRecyclerView.getItemAnimator().setMoveDuration(0);
        mRecyclerView.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator)mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    //备份菜单数据
    private void bakeData() {
        mHomeListBake.clear();
        mHomeListBake.addAll(mHomeList);
        mToolListBake.clear();
        mToolListBake.addAll(mToolList);
        mBusinessListBake.clear();
        mBusinessListBake.addAll(mBusinessList);
    }

    /**
     * 切换页面时调用，提示保存编辑内容
     * @param toCheckedRadioBtnId 下一步要切换的页面的id
     * @return 是否需要拦截   菜单被编辑了需要拦截，否则不需要
     */
    public boolean setEditSaveTip(int toCheckedRadioBtnId) {
        if (isEditMenuChanged()){
            //菜单被编辑了
            hasChangedListData = false;
            mListHeaderWrapper.setHasDragChanged(false);
            showWarnSaveEditDialog(toCheckedRadioBtnId);
            return true;
        }else {
            cancelEdit();
            return false;
        }
    }

    //提示保存编辑内容
    private void showWarnSaveEditDialog(int checkedRadioBtnId) {
        SelectDialog dialogInfo = new SelectDialog(getActivity(), getString(R.string.app_editing_tip), getString(R.string.btnCancel), getString(R.string.tx_btn_save), R.layout.select_common_dialog);
        dialogInfo.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                finishEdit();
                MainActivity activity = (MainActivity) getActivity();
                activity.setRadioBtnChecked(checkedRadioBtnId);
            }
        });
        dialogInfo.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
            @Override
            public void clickCancel() {
                cancelEdit();
                MainActivity activity = (MainActivity) getActivity();
                activity.setRadioBtnChecked(checkedRadioBtnId);
            }
        });
        DialogHelper.resize(mContext, dialogInfo);
        dialogInfo.show();
    }


    //头部菜单列表点击事件
    private class HeaderChildItemClickListener implements OnRecyclerItemClickListener<MenuItem> {
        @Override
        public void onItemClick(View v, MenuItem item, int position, int segment) {
            if (item.getId() == MenuItem.ITEM_ID_EDIT){
                //点了编辑菜单
                isEditState = true;
                initData();//备份数据并再次初始化
            }else {
                //点击头部其它菜单
                if (isEditState){
                    onDeleteClick(v,item,position);
                }else {
                    onMenuItemClick(item);
                }

            }
        }
    }

    //可编辑的菜单列表条目点击事件
    private class ListChildItemClickListener implements OnRecyclerItemClickListener<MenuItem> {
        @Override
        public void onItemClick(View v, MenuItem item, int position, int segment) {
            if (isEditState){
                int max = MAX_SLOTS;
                if (mHomeList.size() >= max) {
                    Toast.makeText(getActivity(), "最多只可添加" + MAX_SLOTS + "个菜单", Toast.LENGTH_SHORT).show();
                } else {
                    MenuHelper.addPreferHomeItem(item);
                    MenuHelper.deleteItem(item.getGroup(), item);
                    notifyHomeDataAdded(item);
                }
            }else {
                onMenuItemClick(item);
            }
        }
    }

    //菜单点击
    public void onMenuItemClick(MenuItem item){
        MenuClickUtil.clickMenu((MainActivity) getActivity(),item);
    }

    @Override
    public void onDeleteClick(View v, MenuItem item, int position) {
        MenuHelper.deletePreferHomeItem(item);
        MenuHelper.addItem(item.getGroup(), item);
        notifyHomeDataRemoved(item);
    }

    private void notifyHomeDataAdded(MenuItem item) {
        mListHeaderWrapper.notifyChildDataAdded(item);
        mListAdapter.notifyChildDataRemoved(item.getGroup(), item,findEditItem(item));
        hasChangedListData = true;
    }

    private void notifyHomeDataRemoved(MenuItem item) {
        mListHeaderWrapper.notifyChildDataRemoved(item);
        mListAdapter.notifyChildDataAdded(item.getGroup(), item,findEditItem(item));
        hasChangedListData = true;
    }

    private boolean isEditMenuChanged(){
        if (mListHeaderWrapper==null) return false;
        return  (mListHeaderWrapper.isHasDragChanged() || hasChangedListData);
    }

    //找item所属的组EditItem
    private EditItem findEditItem(MenuItem item){
        EditItem editItem = null;
        String group = item.getGroup();
        for (EditItem etm :mEditListBake){
            String group1 = etm.getGroup();
            if (group.equals(group1)){
                editItem = etm;
                break;
            }
        }
        return editItem;
    }

    @Override
    protected void onFragmentInVisible() {
        super.onFragmentInVisible();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateMenu(NoticeEvent noticeEvent){
        if (noticeEvent.getTag().equals(Constants.TAG_UPDATE_MENU)){
            LogUtil.d("tagtag","ApplicationFragment onUpdateMenu");
            if (mListHeaderWrapper==null||mListAdapter==null){return;}
            List<MenuItem> preferHomeList = MenuHelper.getPreferHomeList();
            if (preferHomeList.size()>mHomeListBake.size()){
                //home有菜单新增了
                LogUtil.d("tagtag","home有菜单新增了");
                LogUtil.d("tagtag","preferHomeList:"+preferHomeList.toString()+",mHomeListBake:"+mHomeListBake.toString());
                List<MenuItem> updateMenuItems = getUpdateMenuItems(preferHomeList, mHomeListBake);
                for (MenuItem menuItem:updateMenuItems){
                    mListHeaderWrapper.notifyChildDataAdded(menuItem);
                }
            }
            if (preferHomeList.size()<mHomeListBake.size()){
                //home有菜单减少
                LogUtil.d("tagtag","home有菜单减少");
                List<MenuItem> updateMenuItems = getUpdateMenuItems(mHomeListBake, preferHomeList);
                for (MenuItem menuItem:updateMenuItems){
                    mListHeaderWrapper.notifyChildDataRemoved(menuItem);
                }
            }

            List<MenuItem> preferToolList = MenuHelper.getPreferToolList();
            if (preferToolList.size()>mToolListBake.size()){
                //tool有菜单新增
                LogUtil.d("tagtag","tool有菜单新增");
                List<MenuItem> updateMenuItems = getUpdateMenuItems(preferToolList, mToolListBake);
                for (MenuItem menuItem:updateMenuItems){
                    mListAdapter.notifyChildDataAdded(menuItem.getGroup(), menuItem,findEditItem(menuItem));
                }
            }
            if (preferToolList.size()<mToolListBake.size()){
                //tool有菜单减少
                LogUtil.d("tagtag","tool有菜单减少");
                List<MenuItem> updateMenuItems = getUpdateMenuItems(mToolListBake, preferToolList);
                for (MenuItem menuItem:updateMenuItems){
                    mListAdapter.notifyChildDataRemoved(menuItem.getGroup(), menuItem,findEditItem(menuItem));
                }
            }
            List<MenuItem> preferBusinessList = MenuHelper.getPreferBusinessList();
            if (preferBusinessList.size()>mBusinessListBake.size()){
                //business有菜单新增
                LogUtil.d("tagtag","business有菜单新增");
                LogUtil.d("tagtag","preferBusinessList:"+preferBusinessList.toString()+",mBusinessListBake:"+mBusinessListBake.toString());
                List<MenuItem> updateMenuItems = getUpdateMenuItems(preferBusinessList, mBusinessListBake);
                for (MenuItem menuItem:updateMenuItems){
                    mListAdapter.notifyChildDataAdded(menuItem.getGroup(), menuItem,findEditItem(menuItem));
                }
            }
            if (preferBusinessList.size()<mBusinessListBake.size()){
                //business有菜单减少
                LogUtil.d("tagtag","business有菜单减少");
                LogUtil.d("tagtag","preferBusinessList:"+preferBusinessList.toString()+",mBusinessListBake:"+mBusinessListBake.toString());
                List<MenuItem> updateMenuItems = getUpdateMenuItems(mBusinessListBake, preferBusinessList);
                for (MenuItem menuItem:updateMenuItems){
                    mListAdapter.notifyChildDataRemoved(menuItem.getGroup(), menuItem,findEditItem(menuItem));
                }
            }
        }
    }

    //bList大集合，slist小集合
    private List<MenuItem> getUpdateMenuItems(List<MenuItem> bList,List<MenuItem> sList){
        List<MenuItem> updateList = new ArrayList<>();
        for (MenuItem menuItem:bList){
            if (!sList.contains(menuItem) && !updateList.contains(menuItem)){
                updateList.add(menuItem);
            }
        }
        return updateList;
    }

    @Override
    public void onDestroy() {
        if (mListHeaderWrapper!=null){
            mListHeaderWrapper.releaseDragManager();
        }
        super.onDestroy();
    }
}
