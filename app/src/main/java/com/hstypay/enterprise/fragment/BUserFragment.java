package com.hstypay.enterprise.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.activity.ClipImageActivity;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.MessageActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.activity.SettingActivity;
import com.hstypay.enterprise.activity.ShareActivity;
import com.hstypay.enterprise.activity.SignInfoActivity;
import com.hstypay.enterprise.activity.UpLimitActivity;
import com.hstypay.enterprise.activity.bankcard.BankCardActivity;
import com.hstypay.enterprise.activity.employee.EmployeeManagerActivity;
import com.hstypay.enterprise.activity.merchantInfo.MerchantInfoActivity;
import com.hstypay.enterprise.activity.store.StoreManageActivity;
import com.hstypay.enterprise.activity.storeCode.StoreCodeListActivity;
import com.hstypay.enterprise.activity.vipCard.ShareVipActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.InvitateBean;
import com.hstypay.enterprise.bean.LicenseDetailBean;
import com.hstypay.enterprise.bean.MaterialStatusBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:16
 * @描述: ${TODO}
 */

public class BUserFragment extends BaseFragment implements View.OnClickListener {
    private View mView;
    private LinearLayout mLlSetting;
    //private LinearLayout mLlCommonQuestion;
    private LinearLayout mLlReceiveDevices, mLlBankCard, mLlMyUpLimit, mLlInviteFriendsOut, mLlMyUpLimitOut;
    private LinearLayout mLlShopinfo, mLlSign, mLlService, mLlStoreCode, mLlMessage, mLlCashierCode;
    private LinearLayout  mLlShopManage, mLlInviteFriends, mLlStoreAndManager, mLlVipCardOut, mLlVipCard, mLlManageFunction;
    private Activity mMainActivity;
    private ImageView mIvForward;

    private static final int IDENTITY_MERCHANT = 0;//商户
    private static final int IDENTITY_ADMIN = 1;//管理员
    private static final int IDENTITY_CASHIER = 2;//收银员
    private static final int IDENTITY_MANAGER = 3;//管理员
    private static final int IDENTITY_VISITOR = 4;//未进件

    private int examineStatus = -1;//审核状态
    private TextView mTvMchName, mTvMchCheckStatus;
    private ImageView mIvPhoto;
    private MyBroadCast mMyBroadCast;
    private ImageView mIvMessageLine, mIvCashierCode;
    private SafeDialog mLoadDialog;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    public static final int REQUEST_CROP_PHOTO = 0x1003;
    private File tempFile;
    private Uri originalUri;
    private int type = 1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMainActivity = getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        ((MainActivity) getActivity()).dismissLoading();
        mView = inflater.inflate(R.layout.fragment_buser, container, false);
        StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        initView(mView);
        initEvent();
        initData();
        registerBoradcastReceiver();
        return mView;
    }

    public void initView(View view) {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mLlSetting = (LinearLayout) view.findViewById(R.id.setting);
        //mLlCommonQuestion = (LinearLayout) view.findViewById(R.id.common_question);
        mLlReceiveDevices = (LinearLayout) view.findViewById(R.id.device_manage);
        mLlShopinfo = (LinearLayout) view.findViewById(R.id.ll_merchant_info);
        mLlSign = (LinearLayout) view.findViewById(R.id.ll_sign_contact);
        mLlService = (LinearLayout) view.findViewById(R.id.service);
        mLlManageFunction = (LinearLayout) view.findViewById(R.id.ll_manage_function);
        mLlStoreAndManager = (LinearLayout) view.findViewById(R.id.ll_store_and_manager);
        mLlCashierCode = (LinearLayout) view.findViewById(R.id.ll_cashier_code);
        mIvCashierCode = (ImageView) view.findViewById(R.id.iv_cashier_code);
        mLlStoreCode = (LinearLayout) view.findViewById(R.id.store_code);
        mLlMessage = (LinearLayout) view.findViewById(R.id.message);

        mLlVipCardOut = (LinearLayout) view.findViewById(R.id.ll_vip_card);
        mLlVipCard = (LinearLayout) view.findViewById(R.id.my_vip_card);
        mLlBankCard = (LinearLayout) view.findViewById(R.id.ll_bank_card);
        mLlMyUpLimitOut = (LinearLayout) view.findViewById(R.id.ll_up_limit);
        mLlMyUpLimit = (LinearLayout) view.findViewById(R.id.my_up_limit);
        mLlInviteFriendsOut = (LinearLayout) view.findViewById(R.id.ll_invitation_friends);
        mLlInviteFriends = (LinearLayout) view.findViewById(R.id.my_invite_friends);
        mLlShopManage = (LinearLayout) view.findViewById(R.id.store_manage);
        //mLlEmployeeManager = view.findViewById(R.id.ll_employee_manager);//员工管理：店长+收银员

        mIvForward = (ImageView) view.findViewById(R.id.iv_title);
        mIvPhoto = (ImageView) view.findViewById(R.id.iv_photo);
        mTvMchName = (TextView) view.findViewById(R.id.tv_mch_name);
        mTvMchCheckStatus = (TextView) view.findViewById(R.id.tv_mch_check_status);
        mIvMessageLine = (ImageView) view.findViewById(R.id.message_line);

        mTvMchName.setText(MyApplication.getUsername());
    }

    public void initEvent() {
        mLlSetting.setOnClickListener(this);
        //mLlCommonQuestion.setOnClickListener(this);
        mLlReceiveDevices.setOnClickListener(this);
        mLlShopinfo.setOnClickListener(this);
        mLlSign.setOnClickListener(this);
        mLlService.setOnClickListener(this);
        mLlStoreCode.setOnClickListener(this);
        mLlMessage.setOnClickListener(this);
        mLlBankCard.setOnClickListener(this);
        mLlVipCard.setOnClickListener(this);
        mLlMyUpLimit.setOnClickListener(this);
        mLlInviteFriends.setOnClickListener(this);
        mLlShopManage.setOnClickListener(this);
    }

    private void initData() {
        mLlVipCardOut.setVisibility(MyApplication.isOpenMember() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        examineStatus = MyApplication.getExamineStatus();
        setView(getUserFlag());
    }

    @Override
    public void onPause() {
        super.onPause();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        destoryBoradcastReceiver();
    }

    private void invitationCode() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).invitationCode(MyApplication.getContext(), Constants.TAG_INVITATION_CODE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_sign_contact://签约信息
                MtaUtils.mtaId(getActivity(), "L002");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, SignInfoActivity.class));
                }
                break;
            case R.id.setting://设置
                mMainActivity.startActivity(new Intent(mMainActivity, SettingActivity.class));
                break;
            case R.id.service://联系客服
                MtaUtils.mtaId(getActivity(), "Q001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    Intent intent = new Intent(mMainActivity, RegisterActivity.class);
                    intent.putExtra(Constants.REGISTER_INTENT, ConfigUtil.getContactUsUrl());
                    mMainActivity.startActivity(intent);
                }
                break;
            case R.id.device_manage://设备管理
                MtaUtils.mtaId(getActivity(),"R001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, DeviceListActivity.class));
                }
                break;
            case R.id.ll_merchant_info://商户信息
                MtaUtils.mtaId(getActivity(), "L001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, MerchantInfoActivity.class));
                }
                break;
            case R.id.store_code://门店二维码
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, StoreCodeListActivity.class));
                }
                break;
            case R.id.ll_employee_manager://员工管理
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, EmployeeManagerActivity.class));
                }
                break;
            case R.id.store_manage://门店管理
                MtaUtils.mtaId(getActivity(), "M001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, StoreManageActivity.class));
                }
                break;
            case R.id.message://消息
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, MessageActivity.class));
                }
                break;
            case R.id.my_invite_friends://邀请好友
                MtaUtils.mtaId(getActivity(), "P001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    invitationCode();
                }
                break;
            case R.id.my_vip_card://会员卡
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    if (MyApplication.getIsMerchant() || MyApplication.getIsCasher()) {
                        getActivity().startActivity(new Intent(getActivity(), ShareVipActivity.class));
                    } else {
                        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("pageSize", 1000);
                            map.put("currentPage", 1);
                            map.put("merchantDataType", 1);
                            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_VIP_STORE_LIST, map);
                        } else {
                            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                        }
                    }
                }
                break;
            case R.id.ll_bank_card://我的银行卡
                MtaUtils.mtaId(getActivity(), "O001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    mMainActivity.startActivity(new Intent(mMainActivity, BankCardActivity.class));
                }
                break;
            case R.id.my_up_limit://商户提额
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    materialStatus();
                }
                break;
            default:
                break;
        }
    }

    public void setView(int identity) {
        switch (identity) {
            case IDENTITY_MERCHANT:
                if (examineStatus == 0) {
                    mTvMchCheckStatus.setText("审核中");
                    mTvMchCheckStatus.setTextColor(UIUtils.getColor(R.color.white_half));
                    mTvMchCheckStatus.setBackgroundResource(R.drawable.shape_checking);
                } else if (examineStatus == 1) {
                    mTvMchCheckStatus.setText("审核通过");
                    mLlReceiveDevices.setVisibility(View.VISIBLE);
                    mLlStoreAndManager.setVisibility(View.VISIBLE);
                    if (AppHelper.getAppType() == 2) {
                        mLlBankCard.setVisibility(View.VISIBLE);
                        int merchantType = SpUtil.getInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE);
                        if (merchantType == 1) {//企业
                            mLlMyUpLimitOut.setVisibility(View.GONE);
                        } else if (merchantType == 2) {//个人
                            mLlMyUpLimitOut.setVisibility(View.VISIBLE);
                        }
                    } else if (AppHelper.getAppType() == 1) {
                        mLlBankCard.setVisibility(View.GONE);
                        mLlMyUpLimitOut.setVisibility(View.GONE);
                    }
                    mTvMchCheckStatus.setTextColor(UIUtils.getColor(R.color.white));
                    mTvMchCheckStatus.setBackgroundResource(R.drawable.shape_check_passed);
                } else if (examineStatus == 2) {
                    mTvMchCheckStatus.setText("审核未通过");
                    mTvMchCheckStatus.setTextColor(UIUtils.getColor(R.color.white));
                    mTvMchCheckStatus.setBackgroundResource(R.drawable.shape_check_failed);
                }
                if (SpStayUtil.getBoolean(MyApplication.getContext(), MyApplication.getUserId())) {
                    mTvMchCheckStatus.setVisibility(View.GONE);
                } else {
                    mTvMchCheckStatus.setVisibility(View.VISIBLE);
                }
                mIvForward.setVisibility(View.VISIBLE);
                if (AppHelper.getAppType() == 2) {
                    mLlSign.setVisibility(View.VISIBLE);
                    mLlCashierCode.setVisibility(View.GONE);
                } else if (AppHelper.getAppType() == 1) {
                    mLlCashierCode.setVisibility(View.VISIBLE);
                    mLlSign.setVisibility(View.GONE);
                    mLlMessage.setVisibility(View.VISIBLE);
                    mIvMessageLine.setVisibility(View.VISIBLE);
                }
                break;
            case IDENTITY_ADMIN:
                mTvMchCheckStatus.setVisibility(View.GONE);
                mIvForward.setVisibility(View.GONE);
                mLlShopinfo.setEnabled(false);
                if (AppHelper.getAppType() == 1) {
                    mLlMessage.setVisibility(View.VISIBLE);
                    mIvMessageLine.setVisibility(View.VISIBLE);
                    mLlCashierCode.setVisibility(View.VISIBLE);
                }
                break;
            case IDENTITY_MANAGER:
                mTvMchCheckStatus.setVisibility(View.GONE);
                mIvForward.setVisibility(View.GONE);
                mLlShopinfo.setEnabled(false);
                if (AppHelper.getAppType() == 1) {
                    mLlMessage.setVisibility(View.VISIBLE);
                    mIvMessageLine.setVisibility(View.VISIBLE);
                    mLlCashierCode.setVisibility(View.VISIBLE);
                }
                break;
            case IDENTITY_CASHIER:
                mTvMchCheckStatus.setVisibility(View.GONE);
                mIvForward.setVisibility(View.GONE);
                mLlShopinfo.setEnabled(false);
                if (AppHelper.getAppType() == 1) {
                    mLlMessage.setVisibility(View.VISIBLE);
                    mIvMessageLine.setVisibility(View.VISIBLE);
                    mLlCashierCode.setVisibility(View.VISIBLE);
                    mIvCashierCode.setVisibility(View.GONE);
                } else if (AppHelper.getAppType() == 2) {
                    mLlCashierCode.setVisibility(View.GONE);
                }
                break;
            case IDENTITY_VISITOR:
                if (AppHelper.getAppType() == 1) {
                    mLlMessage.setVisibility(View.VISIBLE);
                    mIvMessageLine.setVisibility(View.VISIBLE);
                    mLlCashierCode.setVisibility(View.VISIBLE);
                    mLlStoreAndManager.setVisibility(View.VISIBLE);
                    mLlReceiveDevices.setVisibility(View.VISIBLE);
                    mTvMchCheckStatus.setVisibility(View.GONE);
                } else if (AppHelper.getAppType() == 2) {
                    mLlBankCard.setVisibility(View.VISIBLE);
                    mLlMyUpLimitOut.setVisibility(View.VISIBLE);
                    mTvMchCheckStatus.setVisibility(View.GONE);
                }
                break;
            default:
                break;
        }
        if (mLlCashierCode.getVisibility() == View.GONE && mLlStoreAndManager.getVisibility() == View.GONE) {
            mLlManageFunction.setVisibility(View.GONE);
        } else {
            mLlManageFunction.setVisibility(View.VISIBLE);
        }
    }

    public int getUserFlag() {
        int identity = -1;
        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            identity = 4;
        } else {
            if (MyApplication.getIsMerchant()) {
                identity = 0;
            } else if (MyApplication.getIsAdmin()) {
                identity = 1;
            } else if (MyApplication.getIsCasher()) {
                identity = 2;
            } else if (MyApplication.getIsManager()) {
                identity = 3;
            }
        }
        return identity;
    }

    public void registerBoradcastReceiver() {
        IntentFilter myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(Constants.ACTION_MERCHANT_STATUS);
        //注册广播
        mMyBroadCast = new MyBroadCast();
        getActivity().registerReceiver(mMyBroadCast, myIntentFilter);
    }

    public void destoryBoradcastReceiver() {
        getActivity().unregisterReceiver(mMyBroadCast);
    }

    public class MyBroadCast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.ACTION_MERCHANT_STATUS)) {
                examineStatus = MyApplication.getExamineStatus();
                setView(getUserFlag());
            }
        }
    }

    public void materialStatus() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).materialStatus(MyApplication.getContext(), Constants.TAG_STATUS_UP_LIMIT, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getUpLimitInfo() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).licenseDetail(MyApplication.getContext(), Constants.TAG_LICENSE_DETAIL, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LICENSE_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LicenseDetailBean msg = (LicenseDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    Intent intent = new Intent(mMainActivity, UpLimitActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.INTENT_LICENSE_DETAL, msg.getData());
                    intent.putExtras(bundle);
                    mMainActivity.startActivity(intent);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_INVITATION_CODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InvitateBean msg = (InvitateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData() != null) {
                            Intent intent = new Intent(mMainActivity, ShareActivity.class);
                            intent.putExtra(Constants.INTENT_CODE_URL, msg.getData().getUrl());
                            intent.putExtra(Constants.INTENT_CODE_TIMEOUT, msg.getData().getTimeout());
                            mMainActivity.startActivity(intent);
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_STATUS_UP_LIMIT)) {
            MaterialStatusBean info = (MaterialStatusBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        ((MainActivity) getActivity()).getLoginDialog(getActivity(), info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        MaterialStatusBean.DataEntity data = info.getData();
                        if (data.getExamineStatus() == 1) {
                            getUpLimitInfo();
                        } else {
                            if ("1".equals(data.getStandby8())) {
                                getUpLimitInfo();
                            } else {
                                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), UIUtils.getString(R.string.toast_not_material_change));
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(getActivity(), new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        MyToast.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }
        });

        picPopupWindow.showAtLocation(mIvPhoto, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    String imageUrl;

    private void startCamrae() throws Exception {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";

        tempFile = new File(imageUrl);
        if (!tempFile.getParentFile().exists()){
            tempFile.getParentFile().mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            //如果是则使用FileProvider
            originalUri = FileProvider.getUriForFile(getActivity(),
                    Constants.FILE_PROVIDER, tempFile);
        } else {
            originalUri = Uri.fromFile(tempFile);
        }
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    Uri uri = data.getData();
                    gotoClipActivity(uri);
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    gotoClipActivity(Uri.fromFile(tempFile));
                    break;
                case REQUEST_CROP_PHOTO:  //剪切图片返回
                    if (resultCode == Activity.RESULT_OK) {
                        final Uri clipUri = data.getData();
                        if (clipUri == null) {
                            return;
                        }
                        String cropImagePath = AppHelper.getPicPath(clipUri);
                        Bitmap bitMap = BitmapFactory.decodeFile(cropImagePath);
                        /*if (type == 1) {
                            mIvPhoto.setImageBitmap(bitMap);
                        } else {*/
                            mIvPhoto.setImageBitmap(bitMap);
//                        }
                        //此处后面可以将bitMap转为二进制上传后台网络
                        //......

                    }
                    break;
            }
        }
    }

    /**
     * 打开截图界面
     */
    public void gotoClipActivity(Uri uri) {
        if (uri == null) {
            return;
        }
        Intent intent = new Intent();
        intent.setClass(getActivity(), ClipImageActivity.class);
        intent.putExtra(Constants.INTENT_CLIP_TYPE, type);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_CROP_PHOTO);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        }
    }
}
