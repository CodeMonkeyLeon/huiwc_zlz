package com.hstypay.enterprise.fragment;

import static android.app.Activity.RESULT_OK;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.FilterPopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDatePopupWindow;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.CashierActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.bill.BillFindActivity;
import com.hstypay.enterprise.activity.reportDate.BillDatePickActivity;
import com.hstypay.enterprise.activity.reportDate.CalendarSinglePickActivity;
import com.hstypay.enterprise.adapter.BillRecyclerAdapter;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.paySite.PaySiteChoiceAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.SelectDateBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.paySite.PaySiteBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class BillFragment extends BaseFragment implements View.OnClickListener {
    private ImageView mIvBack;
    private RecyclerView mRvBill, mStoreRecyclerView, mCashierRecyclerView, mSiteRecyclerView;
    private ImageView mIvSearch, mIvFilter, mIvScan, mIvCashierArrow, mIvShopArrow, mIvSiteArrow;
    private LinearLayout mLlDate, mLlFilterTop, mLlShop, mLlCashier, mLlSite, mShopPopLayout, mCashierPopLayout, mSitePopLayout;
    private TextView mTvDate, mTvShop, mTvCashier, mTvSite, mTvNull, mTvShopNull;
    private SelectDatePopupWindow mDatePopupWindow;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;

    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private BillRecyclerAdapter mBillRecyclerAdapter;
    private List<BillsBean.DataEntity> billList;

    private String startTime;
    private String endTime;
    private String userId = "";
    private String cashierName;
    private String mSiteId = "";
    private String storeMerchantId;
    private int[] apiProvider;
    private int[] tradeStatus;//收款时的交易状态
    private int[] refundStatus;//退款时的交易状态
    private List<Integer> tradeTypeList;
    private List<String> sceneList;
    private String otherType;
    private int mParentPos = -1;
    private int mChildPos = -1;
    private View mView;
    private OrderBroadcastReceiver mOrderBroadcastReceiver;
    private SafeDialog mLoadDialog;
    private FilterPopupWindow popupWindow;
    private String mShopType;
    private RadioGroup mRgType;
    private RadioButton mRbAllStore, mRbDirectStore, mRbJoinStore;
    private EditTextDelete mEtStoreInput, mEtCashierInput, mEtSiteInput;
    private ShopRecyclerAdapter mStoreAdapter;
    private CashierRecyclerAdapter mCashierAdapter;
    private boolean mShopSwitchArrow, mCashierSwitchArrow, mSiteSwitchArrow;
    private Animation rotate;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DataEntity> mCashierList;
    private List<DataEntity> mOriginCashierList;
    private List<PaySiteBean> mSiteList;
    private List<PaySiteBean> mOriginSiteList;
    private PaySiteChoiceAdapter mSiteAdapter;
    private TimePickerView pvTime;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        registerBroadcast();
        mView = inflater.inflate(R.layout.activity_bill, container, false);
        StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
        initView(mView);
        initEvent();
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            mTvNull.setVisibility(View.VISIBLE);
        } else {
            initData();
        }
        return mView;
    }

    public void initView(View view) {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) view.findViewById(R.id.iv_back);
        mIvSearch = (ImageView) view.findViewById(R.id.iv_search);
        mIvScan = (ImageView) view.findViewById(R.id.iv_scan);
        mLlDate = (LinearLayout) view.findViewById(R.id.ll_date);
        mTvDate = (TextView) view.findViewById(R.id.tv_date);
        mLlShop = (LinearLayout) view.findViewById(R.id.ll_shop);
        mTvShop = (TextView) view.findViewById(R.id.tv_shop);
        mIvShopArrow = (ImageView) view.findViewById(R.id.iv_shop_arrow);
        mLlCashier = (LinearLayout) view.findViewById(R.id.ll_cashier);
        mTvCashier = (TextView) view.findViewById(R.id.tv_cashier);
        mIvCashierArrow = (ImageView) view.findViewById(R.id.iv_cashier_arrow);
        mLlSite = (LinearLayout) view.findViewById(R.id.ll_pay_site);
        mTvSite = (TextView) view.findViewById(R.id.tv_pay_site);
        mIvSiteArrow = (ImageView) view.findViewById(R.id.iv_pay_site_arrow);
        mIvFilter = (ImageView) view.findViewById(R.id.iv_filter);
        mLlFilterTop = (LinearLayout) view.findViewById(R.id.bill_filter_top);
        mTvNull = (TextView) view.findViewById(R.id.tv_not_data);
        mTvShopNull = (TextView) view.findViewById(R.id.tv_shop_null);
        mIvBack.setVisibility(View.GONE);

        mRgType = view.findViewById(R.id.rg_type);
        mRbAllStore = view.findViewById(R.id.rb_all_store);
        mRbDirectStore = view.findViewById(R.id.rb_direct_store);
        mRbJoinStore = view.findViewById(R.id.rb_join_store);
        mShopPopLayout = view.findViewById(R.id.shop_pop_layout);
        mEtStoreInput = view.findViewById(R.id.et_store_input);
        mStoreRecyclerView = view.findViewById(R.id.recyclerView_store);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mStoreRecyclerView.setLayoutManager(storeLinearLayoutManager);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);

        mCashierPopLayout = view.findViewById(R.id.cashier_pop_layout);
        mEtCashierInput = view.findViewById(R.id.et_cashier_input);
        CustomLinearLayoutManager cashierLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mCashierRecyclerView = view.findViewById(R.id.recyclerView_cashier);
        mCashierRecyclerView.setLayoutManager(cashierLinearLayoutManager);
        mEtCashierInput.setClearImage(R.mipmap.ic_search_clear);

        mSitePopLayout = view.findViewById(R.id.site_pop_layout);
        mEtSiteInput = view.findViewById(R.id.et_site_input);
        CustomLinearLayoutManager siteLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mSiteRecyclerView = view.findViewById(R.id.recyclerView_site);
        mSiteRecyclerView.setLayoutManager(siteLinearLayoutManager);
        mEtSiteInput.setClearImage(R.mipmap.ic_search_clear);
        mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);

        initRecyclerView(view);
        initSwipeRefreshLayout(view);
//        initTimePicker();
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -3);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        //时间选择器
        pvTime = new TimePickerView.Builder(getActivity(), new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date);
                mTvDate.setText(time);
                startTime = time + " 00:00:00";
                endTime = time + " 23:59:59";
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    billList.clear();
                    Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus,refundStatus, otherType, tradeTypeList, sceneList);
                    currentPage = 2;
                    getGetBills(map, true);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年", "月", "日", null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    private void initRecyclerView(View view) {
        mRvBill = (RecyclerView) view.findViewById(R.id.bill_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRvBill.setLayoutManager(mLinearLayoutManager);
        mRvBill.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
            mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, refundStatus,otherType, tradeTypeList,sceneList);
                    getGetBills(map, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    Map<String, Object> map = getRequestMap(pageSize + "" , currentPage + "" , startTime, endTime, userId,
                            storeMerchantId, apiProvider, tradeStatus,refundStatus, otherType, tradeTypeList, sceneList);
                    getGetBills(map, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        /*mLinearLayoutManager.setScrollEnabled(false);
                        mRvBill.setLayoutManager(mLinearLayoutManager);*/
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        /*mLinearLayoutManager.setScrollEnabled(false);
                        mRvBill.setLayoutManager(mLinearLayoutManager);*/
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mLlDate.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlCashier.setOnClickListener(this);
        mLlSite.setOnClickListener(this);
        mIvSearch.setOnClickListener(this);
        mIvScan.setOnClickListener(this);
        mIvFilter.setOnClickListener(this);

        mRbAllStore.setOnClickListener(this);
        mRbDirectStore.setOnClickListener(this);
        mRbJoinStore.setOnClickListener(this);
    }

    public void initData() {
        mTvDate.setText(DateUtil.formatYYMD(System.currentTimeMillis()));
        initTimePicker();
        mShopType = "";
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mCashierList = new ArrayList<>();
        mOriginCashierList = new ArrayList<>();
        mSiteList = new ArrayList<>();
        mOriginSiteList = new ArrayList<>();
        billList = new ArrayList<>();
        mBillRecyclerAdapter = new BillRecyclerAdapter(MyApplication.getContext(), billList);
        mBillRecyclerAdapter.setOnItemClickListener(new BillRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                MtaUtils.mtaId(getActivity(), "C002");
                BillsBean.DataEntity dataEntity = billList.get(position);
                if (dataEntity != null) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        Map<String, Object> map = new HashMap<>();
                        if (dataEntity.getApiProvider() == 7 || dataEntity.getApiProvider() == 9) {
                            if (!StringUtils.isEmptyOrNull(dataEntity.getOrderNo())) {
                                map.put("orderNo", dataEntity.getOrderNo());
                            }
                            map.put("storeMerchantId", storeMerchantId);
                            map.put("type", dataEntity.getApiProvider());
                            ServerClient.newInstance(MyApplication.getContext()).vipBillDetail(MyApplication.getContext(), Constants.TAG_GET_ORDER_DETAIL, map);
                        } else {
                            if (!StringUtils.isEmptyOrNull(dataEntity.getOrderNo())) {
                                map.put("orderNo", dataEntity.getOrderNo());
                            }
                            if (!StringUtils.isEmptyOrNull(dataEntity.getRefundNo())) {
                                map.put("refundNo", dataEntity.getRefundNo());
                            }
                            ServerClient.newInstance(MyApplication.getContext()).getOrderDetail(MyApplication.getContext(), Constants.TAG_GET_ORDER_DETAIL, map);
                        }
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mRvBill.setAdapter(mBillRecyclerAdapter);
        initStore();
        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtCashierInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData(mEtCashierInput.getText().toString().trim());
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtCashierInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData("");
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtSiteInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setSiteData(mEtSiteInput.getText().toString().trim());
                        if (mSiteAdapter != null)
                            mSiteAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtSiteInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setSiteData("");
                        if (mSiteAdapter != null)
                            mSiteAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
        mParentPos = -1;
        mChildPos = -1;
        tradeStatus = new int[]{};
        refundStatus = new int[]{};
        apiProvider = null;
        tradeTypeList = null;
        sceneList = null;
        otherType = null;
        getBill();
    }

    private void getBill() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            billList.clear();
            Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                    apiProvider, tradeStatus, refundStatus,otherType, tradeTypeList, sceneList);
            currentPage = 2;
            getGetBills(map, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void initStore() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() /*|| MyApplication.getIsManager()*/) {
            storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
            if (TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""));
            }
            userId = "";
            cashierName = getString(R.string.tv_all_user);
        } else if (MyApplication.getIsCasher()) {
            storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            mLlShop.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            if (TextUtils.isEmpty(MyApplication.getOpCodeBill())) {
                mLlCashier.setEnabled(false);
                mIvCashierArrow.setVisibility(View.GONE);
            }
            mTvCashier.setText(MyApplication.getRealName());
            userId = MyApplication.getUserId();
            cashierName = MyApplication.getRealName();
        }
    }

    private void setSiteData(String search) {
        mSiteList.clear();
        if (TextUtils.isEmpty(search)) {
            mSiteList.addAll(mOriginSiteList);
        } else {
            for (int i = 0; i < mOriginSiteList.size(); i++) {
                if (mOriginSiteList.get(i).getCashPointName().contains(search)) {
                    mSiteList.add(mOriginSiteList.get(i));
                }
            }
        }
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                mRgType.setVisibility(View.VISIBLE);
            }
            mShopList.addAll(mOriginShopList);
        } else {
            mRgType.setVisibility(View.GONE);
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    private void setCashierData(String search) {
        mCashierList.clear();
        if (TextUtils.isEmpty(search)) {
            mCashierList.addAll(mOriginCashierList);
        } else {
            for (int i = 0; i < mOriginCashierList.size(); i++) {
                if (mOriginCashierList.get(i).getRealName().contains(search)) {
                    mCashierList.add(mOriginCashierList.get(i));
                }
            }
        }
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mShopSwitchArrow = false;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mEtCashierInput.setText("");
                mCashierSwitchArrow = false;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.GONE);
                break;
            case 3:
                mEtSiteInput.setText("");
                mSiteSwitchArrow = false;
                mIvSiteArrow.startAnimation(rotate);
                mSitePopLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                    mRgType.setVisibility(View.VISIBLE);
                } else {
                    mRgType.setVisibility(View.GONE);
                }
                if (mRgType.getVisibility() == View.VISIBLE) {
                    switch (mShopType) {
                        case "":
                            mRgType.clearCheck();
                            mRbAllStore.setChecked(true);
                            break;
                        case "21":
                            mRgType.clearCheck();
                            mRbDirectStore.setChecked(true);
                            break;
                        case "22":
                            mRgType.clearCheck();
                            mRbJoinStore.setChecked(true);
                            break;
                    }
                }
                mShopSwitchArrow = true;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.VISIBLE);
                loadShopData(mShopType);
                break;
            case 2:
                mCashierSwitchArrow = true;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.VISIBLE);
                break;
            case 3:
                mSiteSwitchArrow = true;
                mIvSiteArrow.startAnimation(rotate);
                mSitePopLayout.setVisibility(View.VISIBLE);
                paySiteList();
                break;
        }
    }

    private void paySiteList() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginSiteList.clear();
            mSiteList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_BILL_SITE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadShopData(String shopType) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
//            if (mRbAllStore.isChecked())
//                mOriginShopList.clear();
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(shopType))
                map.put("merchantType", shopType);
            ServerClient.newInstance(MyApplication.getContext()).findStore(MyApplication.getContext(), Constants.TAG_BILL_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadCashierData() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginCashierList.clear();
            mCashierList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(mShopType))
                map.put("merchantType", mShopType);
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), Constants.TAG_BILL_CASHIER, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private Map<String, Object> getRequestMap(String pageSize, String currentPage, String startTime, String endTime,
                                              String userId, String storeMerchantId, int[] apiProvider, int[] tradeStatus, int[] refundStatus, String otherType, List<Integer> tradeTypeList, List<String> sceneList) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (TextUtils.isEmpty(mSiteId)) {
            map.put("cashPointType", 1);
        } else if ("tv_other_site".equals(mSiteId)) {
            map.put("cashPointType", 2);
        } else {
            map.put("cashPointType", 3);
            map.put("cashPointId", mSiteId);
        }
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(mShopType))
            map.put("merchantType", mShopType);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        if (apiProvider != null && apiProvider.length > 0)
            map.put("apiProviderList", apiProvider);
        if (tradeStatus != null && tradeStatus.length > 0)
            map.put("tradeStatusList" , tradeStatus);
        if (refundStatus != null && refundStatus.length > 0)
            map.put("refundStatusList" , refundStatus);
        if (tradeTypeList != null && tradeTypeList.size() > 0)
            map.put("tradeTypeList", tradeTypeList);
        if (sceneList != null && sceneList.size() > 0)
            map.put("termTypeList", sceneList);
        if (!TextUtils.isEmpty(otherType)) {
            map.put("otherType", otherType);
        }
        return map;
    }

    public void getGetBills(Map<String, Object> map, boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            ServerClient.newInstance(MyApplication.getContext()).getBills(MyApplication.getContext(), Constants.TAG_GET_BILLS, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_BILLS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            BillsBean msg = (BillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_BILLS_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_BILLS_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        billList.addAll(msg.getData());
                        mBillRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        mBillRecyclerAdapter.notifyDataSetChanged();
                        if (isLoadmore) {
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_BILL_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        if (mRbAllStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_shop));
                            dataEntity.setStoreId("");
                            mOriginShopList.clear();
                            mOriginShopList.add(dataEntity);
                            mOriginShopList.addAll(msg.getData());
                            mShopList.addAll(mOriginShopList);
                        } else if (mRbDirectStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_direct_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        } else if (mRbJoinStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_join_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        }
                        mStoreAdapter = new ShopRecyclerAdapter(getActivity(), mShopList, storeMerchantId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                if (mRgType.getVisibility() == View.VISIBLE) {
                                    switch (mRgType.getCheckedRadioButtonId()) {
                                        case R.id.rb_all_store:
                                            mShopType = "";
                                            break;
                                        case R.id.rb_direct_store:
                                            mShopType = "21";
                                            break;
                                        case R.id.rb_join_store:
                                            mShopType = "22";
                                            break;
                                    }
                                } else {
                                    mShopType = "";
                                }
                                storeMerchantId = mShopList.get(position).getStoreId();
                                mTvShop.setText(mShopList.get(position).getStoreName());
                                if (!MyApplication.getIsCasher()) {
                                    mTvCashier.setText(getString(R.string.tv_all_user));
                                    cashierName = getString(R.string.tv_all_user);
                                    userId = "";
                                } else {
                                    mTvCashier.setText(MyApplication.getRealName());
                                    cashierName = MyApplication.getRealName();
                                    userId = MyApplication.getUserId();
                                }
                                mTvSite.setText(getString(R.string.tv_all_site));
                                mSiteId = "";
                                closeArrow(1);
                                getBill();
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        mTvShopNull.setVisibility(View.GONE);
                        mStoreRecyclerView.setVisibility(View.VISIBLE);
                    } else {
                        mTvShopNull.setVisibility(View.VISIBLE);
                        mStoreRecyclerView.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_BILL_CASHIER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.ERROR_CODE_NO_RIGH)) {
                                DataEntity dataEntity = new DataEntity();
                                dataEntity.setRealName(getString(R.string.tv_all_user));
                                dataEntity.setUserId("");
                                mOriginCashierList.add(dataEntity);
                                mCashierList.addAll(mOriginCashierList);
                                mCashierAdapter = new CashierRecyclerAdapter(getActivity(), mCashierList);
                                mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        userId = mCashierList.get(position).getUserId();
                                        mTvCashier.setText(mCashierList.get(position).getRealName());
                                        closeArrow(2);
                                        getBill();
                                    }
                                });
                                mCashierRecyclerView.setAdapter(mCashierAdapter);
                                mCashierAdapter.setSelected(userId);
                                openArrow(2);
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        DataEntity dataEntity = new DataEntity();
                        dataEntity.setRealName(getString(R.string.tv_all_user));
                        dataEntity.setUserId("");
                        mOriginCashierList.add(dataEntity);
                        mOriginCashierList.addAll(msg.getData());
                        mCashierList.addAll(mOriginCashierList);
                        mCashierAdapter = new CashierRecyclerAdapter(getActivity(), mCashierList);
                        mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                userId = mCashierList.get(position).getUserId();
                                mTvCashier.setText(mCashierList.get(position).getRealName());
                                closeArrow(2);
                                getBill();
                            }
                        });
                        mCashierRecyclerView.setAdapter(mCashierAdapter);
                        mCashierAdapter.setSelected(userId);
                        openArrow(2);
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_BILL_SITE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.ERROR_CODE_NO_RIGH)) {
                                PaySiteBean dataEntity = new PaySiteBean();
                                dataEntity.setCashPointName(getString(R.string.tv_all_site));
                                dataEntity.setId("");
                                PaySiteBean otherSite = new PaySiteBean();
                                otherSite.setCashPointName(getString(R.string.tv_other_site));
                                otherSite.setId("tv_other_site");
                                mOriginSiteList.add(dataEntity);
                                mOriginSiteList.add(otherSite);
                                mSiteList.addAll(mOriginSiteList);
                                mSiteAdapter = new PaySiteChoiceAdapter(getActivity(), mSiteList, mSiteId);
                                mSiteAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        mSiteId = mSiteList.get(position).getId();
                                        mTvSite.setText(mSiteList.get(position).getCashPointName());
                                        closeArrow(3);
                                        getBill();
                                    }
                                });
                                mSiteRecyclerView.setAdapter(mSiteAdapter);
//                                openArrow(3);
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getCashPointList() != null) {
                        PaySiteBean dataEntity = new PaySiteBean();
                        dataEntity.setCashPointName(getString(R.string.tv_all_site));
                        dataEntity.setId("");
                        PaySiteBean otherSite = new PaySiteBean();
                        otherSite.setCashPointName(getString(R.string.tv_other_site));
                        otherSite.setId("tv_other_site");
                        mOriginSiteList.add(dataEntity);
                        mOriginSiteList.add(otherSite);
                        mOriginSiteList.addAll(msg.getData().getCashPointList());
                        mSiteList.addAll(mOriginSiteList);
                        mSiteAdapter = new PaySiteChoiceAdapter(getActivity(), mSiteList, mSiteId);
                        mSiteAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                mSiteId = mSiteList.get(position).getId();
                                mTvSite.setText(mSiteList.get(position).getCashPointName());
                                closeArrow(3);
                                getBill();
                            }
                        });
                        mSiteRecyclerView.setAdapter(mSiteAdapter);
//                        openArrow(3);
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            billList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_date://选择日期
//                getSelectDayList();
//                showDatePop();
                Intent dateIntent = new Intent(getContext(), BillDatePickActivity.class);
                //intent.putExtra(Constants.INTENT_REPORT_DAY_DATA, msg);
                dateIntent.putExtra(Constants.INTENT_PARENT_POSITION, mParentPos);
                dateIntent.putExtra(Constants.INTENT_CHILD_POSITION, mChildPos);
                dateIntent.putExtra(CalendarSinglePickActivity.KEY_HIDE_MONEY, true);
                startActivityForResult(dateIntent, Constants.REQUEST_SELECT_DAY);
                break;
            /*case R.id.ll_shop://选择店铺
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    Intent intentShop = new Intent(getActivity(), ShopActivity.class);
                    if (TextUtils.isEmpty(storeMerchantId)) {
                        intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                    } else {
                        intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                    }
                    intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BILL_SHOP);
                    intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                }
                break;
            case R.id.ll_cashier://选择收银员
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog();
                } else {
                    Intent intentCashier = new Intent(getActivity(), CashierActivity.class);
                    intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                    if (TextUtils.isEmpty(userId)) {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                    } else {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                    }
                    intentCashier.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                }
                break;*/
            case R.id.ll_shop:
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mShopSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_cashier:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                } else {
                    loadCashierData();
                }
                break;
            case R.id.ll_pay_site:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                } else {
//                    paySiteList();
                    openArrow(3);
                }
                break;
            case R.id.iv_filter://选择筛选条件
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    showFilterPop();
                }
                break;
            case R.id.iv_search://搜索
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    startActivity(new Intent(getActivity(), BillFindActivity.class));
                }
                break;
            case R.id.iv_scan://扫一扫
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    Intent intent = new Intent(getActivity(), CaptureActivity.class);
                    intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_SCAN_BILL);
                    startActivity(intent);
                }
                break;
            case R.id.rb_all_store:
                loadShopData("");
                break;
            case R.id.rb_direct_store:
                loadShopData("21");
                break;
            case R.id.rb_join_store:
                loadShopData("22");
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeMerchantId = shopBean.getStoreId();
            SpStayUtil.putString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, shopBean.getStoreId());
            SpStayUtil.putString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, shopBean.getStoreName());
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText(getString(R.string.tv_all_user));
                cashierName = getString(R.string.tv_all_user);
                userId = "";
            } else {
                mTvCashier.setText(MyApplication.getRealName());
                cashierName = MyApplication.getRealName();
                userId = MyApplication.getUserId();
            }
            mTvSite.setText(getString(R.string.tv_all_site));
            mSiteId = "";
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                billList.clear();
                Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                        apiProvider, tradeStatus,refundStatus, otherType, tradeTypeList, sceneList);
                currentPage = 2;
                getGetBills(map, true);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }

        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            popupWindow.setCashier(cashierBean.getUserId(), cashierBean.getRealName());
            /*Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            empId = cashierBean.getEmpId();
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                billList.clear();
                Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                        apiProvider, tradeStatus, otherType, tradeTypeList, sceneList);
                currentPage = 2;
                getGetBills(map, true);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }*/
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_BILL_DETAIL) {
            if (data != null) {
                int orderStatus = data.getIntExtra(Constants.RESULT_CASHIER_INTENT, -1);
                String orderNo = data.getStringExtra(Constants.RESULT_ORDER_NO);
                if (orderStatus == -1 || orderNo == null)
                    return;
                for (int i = 0; i < billList.size(); i++) {
                    if (orderNo.equals(billList.get(i).getOrderNo())) {
                        billList.get(i).setTradeState(orderStatus);
                        mBillRecyclerAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SELECT_DAY) {
            Bundle extras = data.getExtras();
            SelectDateBean selectDateBean = (SelectDateBean) extras.getSerializable(Constants.RESULT_SELECT_DATE);
            if (selectDateBean != null) {
                mTvDate.setText(selectDateBean.getStartTime());
                startTime = selectDateBean.getStartTime() + " 00:00:00";
                endTime = selectDateBean.getEndTime() + " 23:59:59";
                mParentPos = selectDateBean.getParentPos();
                mChildPos = selectDateBean.getChildPos();
            } else {
                mTvDate.setText(DateUtil.getBeforeDate());
                startTime = DateUtil.getBeforeDate() + " 00:00:00";
                endTime = DateUtil.getBeforeDate() + " 23:59:59";
                mParentPos = -1;
                mChildPos = -1;
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                getBill();
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    public void showFilterPop() {
        if (popupWindow == null) {
            popupWindow = new FilterPopupWindow(getActivity(), apiProvider, tradeStatus, refundStatus,otherType, tradeTypeList, sceneList, new FilterPopupWindow.HandleBtn() {
                @Override
                public void handleOkBtn(String cashierName, String userId, int[] apiProvider, int[] tradeStatus, int[] refundStatus, String otherType, List<Integer> tradeTypeList, List<String> sceneList) {
                    Properties prop = new Properties();
                    prop.setProperty("apiProviderList", apiToString(apiProvider));
                    prop.setProperty("tradeStatusList", statusToString(tradeStatus));
                    if (otherType == null) {
                        prop.setProperty("otherType", "");
                    } else {
                        prop.setProperty("otherType", otherType);
                    }
                    BillFragment.this.apiProvider = apiProvider;
                    BillFragment.this.tradeStatus = tradeStatus;
                    BillFragment.this.refundStatus = refundStatus;
                    BillFragment.this.otherType = otherType;
                    BillFragment.this.tradeTypeList = tradeTypeList;
                    BillFragment.this.sceneList = sceneList;
                    BillFragment.this.userId = userId;
                    BillFragment.this.cashierName = cashierName;
                    mBillRecyclerAdapter.setOtherType(otherType);
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        billList.clear();
                        Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                                apiProvider, tradeStatus,refundStatus, otherType, tradeTypeList, sceneList);
                        currentPage = 2;
                        getGetBills(map, true);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            });
            popupWindow.setOnClickCashier(new FilterPopupWindow.OnClickCashier() {
                @Override
                public void onClickCashier() {
                    Intent intentCashier = new Intent(getActivity(), CashierActivity.class);
                    intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                    if (TextUtils.isEmpty(userId)) {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                    } else {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                    }
                    startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                }
            });
        }
        popupWindow.setCashier(userId, cashierName);
        popupWindow.showAsDropDown(mLlDate);
    }

    //订单详情返回数据
    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_ORDER_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.INTENT_OTHER_TYPE, otherType);
                        intent.putExtra(Constants.INTENT_BILL_DATA, data);
                        intent.setClass(getActivity(), PayDetailActivity.class);
                        startActivityForResult(intent, Constants.REQUEST_BILL_DETAIL);
                    }
                    break;
            }
        }
        /*if (event.getTag().equals(Constants.TAG_POS_BCARD_REFUND_CANCELED)) {
            if (!TextUtils.isEmpty(event.getCls())) {
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), event.getCls());
            }
        }*/
    }


    private String apiToString(int[] arr) {
        if (arr == null || arr.length == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                builder.append("微信支付 ");
            } else if (arr[i] == 2) {
                builder.append("支付宝 ");
            } else if (arr[i] == 4) {
                builder.append("QQ钱包 ");
            } else if (arr[i] == 5) {
                builder.append("银联支付 ");
            } else if (arr[i] == 10) {
                builder.append("刷卡支付 ");
            }
        }
        return builder.toString();
    }

    private String statusToString(int[] arr) {
        if (arr == null || arr.length == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                builder.append("未支付 ");
            } else if (arr[i] == 2) {
                builder.append("支付成功 ");
            } else if (arr[i] == 3) {
                builder.append("已关闭 ");
            } else if (arr[i] == 4) {
                builder.append("转入退款 ");
            }
        }
        return builder.toString();
    }

    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_ORDER_DATA);
        intentFilter.addAction(Constants.ACTION_ORDER_REFUND_DATA);
        mOrderBroadcastReceiver = new OrderBroadcastReceiver();
        getActivity().registerReceiver(mOrderBroadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(mOrderBroadcastReceiver);
    }

    class OrderBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.ACTION_ORDER_DATA.equals(action)) {
                mHandler.sendEmptyMessageDelayed(1, 4000);
            } else if (Constants.ACTION_ORDER_REFUND_DATA.equals(action)) {
                mHandler.sendEmptyMessage(1);
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
            MtaUtils.mtaId(getActivity(), "C001");
            /*try {
                String time = DateUtil.formartDateToYYMMDD(startTime);
                if (DateUtil.isToday(time)) {
                    mTvDate.setText("今日");
                } else {
                    mTvDate.setText(time);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            initTimePicker();*/
//            mTvDate.setText("今日");
            if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                mTvNull.setVisibility(View.VISIBLE);
            } else {
                initData();
            }
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        isRefreshed = true;
                        Map<String, Object> map = getRequestMap("15", "1", startTime, endTime, userId, storeMerchantId,
                                apiProvider, tradeStatus, refundStatus,otherType, tradeTypeList, sceneList);
                        getGetBills(map, false);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    break;
            }
        }
    };
}
