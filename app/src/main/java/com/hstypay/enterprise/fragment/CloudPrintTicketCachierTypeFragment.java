package com.hstypay.enterprise.fragment;

import com.hstypay.enterprise.activity.cloudprint.CloudPrintTicketTypeSetActivity;

/**
 * author: kuangzeyu2019
 * date: 2020/9/18
 * time: 10:20
 * desc: 云打印小票類型--收銀員
 */
public class CloudPrintTicketCachierTypeFragment extends CloudPrintTicketTypeBaseFragment{
    @Override
    protected int getPagerType() {
        return CloudPrintTicketTypeSetActivity.BIND_TYPE_CACHIER;
    }
}
