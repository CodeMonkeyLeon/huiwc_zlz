package com.hstypay.enterprise.fragment;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintTicketSetActivity;
import com.hstypay.enterprise.activity.cloudprint.CloudPrintTicketTypeSetActivity;
import com.hstypay.enterprise.adapter.TicketTypeSetRvAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.BaseBean;
import com.hstypay.enterprise.bean.CloudPrintDetailBean;
import com.hstypay.enterprise.bean.QueryAvailabbleInfoBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: kuangzeyu2019
 * date: 2020/9/18
 * time: 10:24
 * desc: 云打印关联设置，云播报关联设置
 */
public abstract class CloudPrintTicketTypeBaseFragment extends BaseFragment {

    private CloudPrintTicketTypeSetActivity mContext;
    private View mRootView;
    private RecyclerView mRvTicketTypeSet;
    private SafeDialog mLoadDialog;
    private CloudPrintDetailBean.DataBean mDetailBean;
    private TicketTypeSetRvAdapter mAdapter;
    private  List<QueryAvailabbleInfoBean.BindDeviceBean> mBindDeviceBeans;
    private int mSelectPosition;
    private List<QueryAvailabbleInfoBean.BindDeviceBean> searchBindDeviceBeans = new ArrayList<>();
    private boolean mIsSearchOperation;//是否是搜索操作
    private View mTvEmptyViewCloudPrint;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (CloudPrintTicketTypeSetActivity) getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.fragment_cloud_print_ticket_type, container, false);
        initView();
        initData();
        initEvent();
        return mRootView;
    }

    private void initEvent() {
        mAdapter.setOnBindSwitchClickListener(new TicketTypeSetRvAdapter.OnBindSwitchClickListener() {
            @Override
            public void onBind(int type, int position) {
                if (MyApplication.getIsCasher()) {
                    //只有商户和店长才可以操作关联收款码、收款设备、收银员，收银员只能查看。
                    MyToast.showToastShort(getString(R.string.device_set_not_access));
                }else {
                    if (!NetworkUtils.isNetworkAvailable(mContext)) {
                        MyToast.showToastShort(getString(R.string.network_exception));
                        return;
                    } else {
                        mSelectPosition = position;
                        DialogUtil.safeShowDialog(mLoadDialog);
                        Map<String, Object> map = new HashMap<>();
                        if (mIsSearchOperation){
                            map.put("bindId", searchBindDeviceBeans.get(position).getBindId());
                        }else {
                            map.put("bindId", mBindDeviceBeans.get(position).getBindId());
                        }
                        map.put("classId", mDetailBean.getId());
                        map.put("bindType", type);
                        String physicFlag = null;
                        if (mIsSearchOperation){
                            physicFlag = searchBindDeviceBeans.get(position).getPhysicFlag();
                        }else {
                            physicFlag = mBindDeviceBeans.get(position).getPhysicFlag();
                        }
                        if("0".equals(physicFlag)){
                            map.put("physicFlag", "1");//关联
                        }else  if("1".equals(physicFlag)){
                            map.put("physicFlag", "0");//不关联
                        }
                        if (Constants.INTENT_NAME_CLOUD_VOICE_SET.equals(mContext.getIntentName())){
                            //云播报
                            map.put("classType","2");//业务类型1、打印机、2、语音播报
                        }else  if (Constants.INTENT_NAME_CLOUD_PRINT_SET.equals(mContext.getIntentName())){
                            //云打印
                            map.put("classType","1");//业务类型1、打印机、2、语音播报
                        }
                        ServerClient.newInstance(mContext).bindDevice(mContext, Constants.TAG_CLOUD_DEVICE_BINDDEVICE+"_"+getPagerType(), map);
                    }
                }

            }
        });

    }



    private void initData() {
        mLoadDialog = mContext.getLoadDialog(mContext, getString(R.string.public_loading), false);
        mDetailBean = mContext.getDetailBean();
        mAdapter = new TicketTypeSetRvAdapter(mContext,getPagerType());
        mRvTicketTypeSet.setAdapter(mAdapter);
        queryAvailabbleInfo();
    }

    /**
     * 查询需要设置的收款码、收款设备、收银员列表
     */
    private void queryAvailabbleInfo() {
        if (!NetworkUtils.isNetworkAvailable(mContext)) {
            MyToast.showToastShort(getString(R.string.network_exception));
            return;
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("classId", mDetailBean.getId());
            map.put("bindType",""+getPagerType());
            map.put("storeMerchantId",mDetailBean.getStoreMerchantId());
            if (Constants.INTENT_NAME_CLOUD_VOICE_SET.equals(mContext.getIntentName())){
                //云播报
                map.put("classType","2");//业务类型1、打印机、2、语音播报
            }else  if (Constants.INTENT_NAME_CLOUD_PRINT_SET.equals(mContext.getIntentName())){
                //云打印
                map.put("classType","1");//业务类型1、打印机、2、语音播报
            }
            ServerClient.newInstance(mContext).queryAvailabbleInfo(mContext, Constants.TAG_CLOUD_DEVICE_QUERYAVAILABBLEINFO+"_"+getPagerType(), map);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if(event.getTag().equals(Constants.TAG_CLOUD_DEVICE_QUERYAVAILABBLEINFO+"_"+getPagerType())) {
            //查询云打印机需要关联的收款码、收款设备、收银员三个列表
            DialogUtil.safeCloseDialog(mLoadDialog);
            QueryAvailabbleInfoBean msg = (QueryAvailabbleInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    mContext.getLoginDialog(mContext, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mBindDeviceBeans = msg.getData();
                    if (mBindDeviceBeans!=null && mBindDeviceBeans.size()>0){
                        mAdapter.setData(mBindDeviceBeans);
                        mIsSearchOperation = false;
                    }else {
                        mRvTicketTypeSet.setVisibility(View.GONE);
                        mTvEmptyViewCloudPrint.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }else if (event.getTag().equals(Constants.TAG_CLOUD_DEVICE_BINDDEVICE+"_"+getPagerType())){
            DialogUtil.safeCloseDialog(mLoadDialog);
            BaseBean msg = (BaseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.network_exception));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    mContext.getLoginDialog(mContext, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    QueryAvailabbleInfoBean.BindDeviceBean bindDeviceBean;
                    if (mIsSearchOperation){
                        bindDeviceBean = searchBindDeviceBeans.get(mSelectPosition);
                    }else {
                        bindDeviceBean = mBindDeviceBeans.get(mSelectPosition);
                    }
                    String physicFlag = bindDeviceBean.getPhysicFlag();
                    if("0".equals(physicFlag)){
                        bindDeviceBean.setPhysicFlag("1");
                    }else  if("1".equals(physicFlag)){
                        bindDeviceBean.setPhysicFlag("0");
                    }
                    if (mIsSearchOperation){
                        mAdapter.setData(searchBindDeviceBeans);
                    }else {
                        mAdapter.setData(mBindDeviceBeans);
                    }
                    break;
            }

        }
    }

    protected abstract int getPagerType();

    private void initView() {
        mRvTicketTypeSet = mRootView.findViewById(R.id.rv_ticket_type_set);
        mRvTicketTypeSet.setLayoutManager(new LinearLayoutManager(mContext));
        mTvEmptyViewCloudPrint = mRootView.findViewById(R.id.tv_empty_view_cloud_print);

    }

    /**
     * 搜索
     * @param key
     */
    public void search(String key){
        if (mBindDeviceBeans==null||mBindDeviceBeans.size()==0){
            return;
        }
        if (TextUtils.isEmpty(key)){
            mAdapter.setData(mBindDeviceBeans);
            mIsSearchOperation = false;
            mRvTicketTypeSet.setVisibility(View.VISIBLE);
            mTvEmptyViewCloudPrint.setVisibility(View.GONE);
            return;
        }
        searchBindDeviceBeans.clear();
        for (QueryAvailabbleInfoBean.BindDeviceBean bindDeviceBean:mBindDeviceBeans){
            String bindId = bindDeviceBean.getBindId();
            String sn = bindDeviceBean.getSn();
            String cashPointName = bindDeviceBean.getCashPointName();
            String cashierName = bindDeviceBean.getCashierName();
            if (bindId !=null && bindId.contains(key)){
                searchBindDeviceBeans.add(bindDeviceBean);
            }else if (sn != null && sn.contains(key)){
                searchBindDeviceBeans.add(bindDeviceBean);
            }else if (cashPointName!=null && cashPointName.contains(key)){
                searchBindDeviceBeans.add(bindDeviceBean);
            }else if (cashierName !=null && cashierName.contains(key)){
                searchBindDeviceBeans.add(bindDeviceBean);
            }
        }
        if (searchBindDeviceBeans.size()>0){
            mAdapter.setData(searchBindDeviceBeans);
            mIsSearchOperation = true;
        }else {
            mRvTicketTypeSet.setVisibility(View.GONE);
            mTvEmptyViewCloudPrint.setVisibility(View.VISIBLE);
        }
    }
}
