package com.hstypay.enterprise.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.factory.FragmentFactory;

public class HolderFragment extends Fragment {
    private static final String TAG = "HolderFragment";

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		int position = getArguments().getInt("position");
		Fragment childFragment = getChildFragment(position);
		FragmentManager manager = getChildFragmentManager();
		manager.popBackStackImmediate(null, 1);
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.replace(R.id.fl_content_container, childFragment);
		transaction.addToBackStack(null);
		transaction.commit();
		
		return View.inflate(getContext(), R.layout.common_container, null);
	}
	

	public Fragment getChildFragment(int position){
		return FragmentFactory.getFragment(position);
	}

	public boolean goBack() {
		FragmentManager manager = getChildFragmentManager();
		if (manager.getBackStackEntryCount() > 1) {
			manager.popBackStack();
			return true;
		} else {
			return false;
		}
	}
	public void goForward(BaseFragment fragment) {

		if (fragment.preGoForward()) {
			FragmentManager childFragmentManager = getChildFragmentManager();
			FragmentTransaction transaction = childFragmentManager.beginTransaction();
			transaction.replace(R.id.fl_content_container, fragment);
			transaction.addToBackStack(this.getClass().getSimpleName());
			transaction.commit();
		}
	}
}