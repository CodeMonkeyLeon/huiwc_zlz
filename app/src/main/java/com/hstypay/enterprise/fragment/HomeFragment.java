package com.hstypay.enterprise.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.HomePopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SelectUpdateDialog;
import com.hstypay.enterprise.Widget.UpdateSuccessNoticeDialog;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.CollectActivity1;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.MessageActivity;
import com.hstypay.enterprise.activity.PayActivity;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.QuestionActivity;
import com.hstypay.enterprise.activity.RefundDetailActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.activity.bill.BillActivity;
import com.hstypay.enterprise.activity.paySite.InstructionPaySiteActivity;
import com.hstypay.enterprise.activity.paySite.PaySiteListActivity;
import com.hstypay.enterprise.adapter.HomeFragmentAdapter;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentHeaderViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentMerchantPagerViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentNoticeViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentReceiveListViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentReceiveTableViewHolder;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentTradeViewHolder;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.BillsBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.LoanDetailBean;
import com.hstypay.enterprise.bean.UrlBean;
import com.hstypay.enterprise.bean.MchtalkBean;
import com.hstypay.enterprise.bean.MerChantModifyBean;
import com.hstypay.enterprise.bean.MessageData;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PushModeBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.ReportTradeTrendBean;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.BaiduMtjUtils;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.HomeGuideHelp;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MenuClickUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PayBeanUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aiven.guide.view.SmartGuide;

/**
 * @author kuangzeyu
 * @time 2021/3/9
 * @desc 首页
 */
public class HomeFragment extends BaseFragment implements MenuItem.OnHomeMenuItemClickListener, HomeFragmentReceiveListViewHolder.OnHomeReceiveItemClickListener, HomeFragmentHeaderViewHolder.OnHomeHeaderIconClickListener, HomeFragmentNoticeViewHolder.OnNoticeClickListener, HomeFragmentReceiveTableViewHolder.OnReceiveStatictisMoreClickListener, HomeFragmentMerchantPagerViewHolder.OnHomeMchTalkItemClickListener, HomeFragmentTradeViewHolder.OnTradeItemClickListener {
    public static final String TAG = HomeFragment.class.getSimpleName();
    private Context mContext;
    private View mRootView;
    private LinearLayout mLlContent;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    public RecyclerView mRvHomeFragment;
    private HomeFragmentAdapter mAdapter;
    private OrderBroadcastReceiver mOrderBroadcastReceiver;
    private List<Integer> mViewTypeList = new ArrayList<>();//首页view的类型数量
    private List<List<MenuItem>> mLlMenuBean = new ArrayList<>();//首页功能菜单数据的集合
    private SafeDialog mLoadDialog;
    private MainActivity mMainActivity;
    private String otherType = "";
    private int refreshCount;//记录刷新时接口请求的次数
    private String mStoreName;
    private int initRequestCount;//记录初始时接口请求的次数
    private SmartGuide mSmartGuide;
    private List<MenuItem> mPreferHomeList;//保存在文件中的菜单集合+1个更多按钮
    private SelectUpdateDialog mSelectUpdateDialog;
    private SelectDialog mSelectDialog;
    private UpdateSuccessNoticeDialog mUpdateSuccessNoticeDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        refreshCount = 0;
        initRequestCount = 0;
        StatusBarUtil.setTranslucentStatusFontIconDark(getActivity(), mRootView);
        //首页菜单数据
        getHomeMenuData();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            //今日交易数据
            initRequestCount++;
            getHomeTodayData();
            //收款记录请求
            initRequestCount++;
            getBillData();
            //近7天的收款统计报表请求
            initRequestCount++;
            getTradeStatisticsByDayTrendings();
            //商家说数据请求
            if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_BUSINESS_SAID)) {
                initRequestCount++;
                getMchTalkData();
            }
            ServerClient.newInstance(mContext).queryMchHbfqEnable(mContext, Constants.TAG_HBFQ_ENABLE, null);
        }

        registerBroadcast();
    }

    @Override
    protected void onFragmentInVisible() {
        super.onFragmentInVisible();
        if (mOrderBroadcastReceiver != null) {
            try {
                getActivity().unregisterReceiver(mOrderBroadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.fragment_home, container, false);
        StatusBarUtil.setTranslucentStatusFontIconDark(getActivity(), mRootView);
        initView();
        initData();
        initEvent();
        return mRootView;
    }

    private void initEvent() {
        mAdapter.setOnHomeMenuItemClickListener(this);
        mAdapter.setOnHomeReceiveItemClickListener(this);
        mAdapter.setOnHomeHeaderIconClickListener(this);
        mAdapter.setOnNoticeClickListener(this);
        mAdapter.setOnReceiveStatictisMoreClickListener(this);
        mAdapter.setOnHomeMchTalkItemClickListener(this);
        mAdapter.setOnTradeItemClickListener(this);

        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                        refreshCount = 0;
                        //今日交易数据
                        refreshCount++;
                        getHomeTodayData();
                        //收款记录请求
                        refreshCount++;
                        getBillData();
                        //近7天的收款统计报表请求
                        refreshCount++;
                        getTradeStatisticsByDayTrendings();
                        //商家说数据请求
                        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_BUSINESS_SAID)) {
                            refreshCount++;
                            getMchTalkData();
                        }
                        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_NOTICE)) {
                            refreshCount++;
                            homeMessage();
                        }
                    } else {
                        setRefreshState(800);
                    }
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    setRefreshState(800);
                }
            }

            @Override
            public void onLoading() {
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
            }
        });
    }

    private void setRefreshState(long delay) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.finishRefresh();
            }
        }, delay);
    }

    private void initData() {
        otherType = Constants.HPAY_RECEIVE_ORDER;
        mMainActivity = (MainActivity) getActivity();
        mLoadDialog = MainActivity.instance.mLoadDialog;
        boolean guideOver = SpStayUtil.getBoolean(mContext, Constants.SP_SMART_GUIDE_OVER);
        if (!guideOver) {
            //还没有向导完成,设置页面不能点击，不能切换到其它fragment
            RelativeLayout rl_main = mMainActivity.findViewById(R.id.rl_activity_main);
            rl_main.setVisibility(View.VISIBLE);
            rl_main.setOnClickListener(null);
            mHandler.sendEmptyMessageDelayed(2, 2000);
        }
        mAdapter = new HomeFragmentAdapter(getActivity());
        mRvHomeFragment.setAdapter(mAdapter);

        mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_HEADER);
        mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_TRADE);
        //mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_NOTICE);//通知默认先不显示
        mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_FUNCTION);
        mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_RECEIVE_LIST);
        mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_RECEIVE_TABLE);
        mAdapter.setViewTypeList(mViewTypeList);
        mAdapter.setVisible(View.GONE);

        initValue(false);//false等页面可见时会再次刷新UI

        homeRequest();
    }

    public void initValue(boolean upDateUI) {
        //控制商家说的展示
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_BUSINESS_SAID)) {
            if (!mViewTypeList.contains(HomeFragmentAdapter.VIEW_TYPE_MERCHANT_PAGER)) {
                mViewTypeList.add(HomeFragmentAdapter.VIEW_TYPE_MERCHANT_PAGER);//添加商家说内容
                mAdapter.setViewTypeList(mViewTypeList);
            }
        } else {
            if (mViewTypeList.contains(HomeFragmentAdapter.VIEW_TYPE_MERCHANT_PAGER)) {
                mViewTypeList.remove(Integer.valueOf(HomeFragmentAdapter.VIEW_TYPE_MERCHANT_PAGER));//移除商家说内容
                mAdapter.setViewTypeList(mViewTypeList);
            }
        }
        initMenuDataUI(upDateUI);
        //商户名称
        if (!MyApplication.getIsCasher()) {
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                mAdapter.setHeaderData(MyApplication.getMerchantName());
            } else {
                mAdapter.setHeaderData("");
            }
        }

    }


    /**
     * 初始化或刷新菜单数据及UI
     */
    public void initMenuDataUI(boolean needUpDateUI) {
        MenuHelper.setMenuData();//更新菜单数据
        if (needUpDateUI) {
            getHomeMenuData();//更新首页菜单UI
        }
        //更新应用菜单UI
        EventBus.getDefault().post(new NoticeEvent(Constants.TAG_UPDATE_MENU, null));
    }


    //获取并显示首页菜单数据
    private void getHomeMenuData() {
        mLlMenuBean.clear();
        mPreferHomeList = MenuHelper.getPreferHomeList();
        MenuItem editItem = new MenuItem();
        editItem.setId(MenuItem.ITEM_ID_EDIT);
        mPreferHomeList.remove(editItem);//移除编辑菜单
        if (mPreferHomeList.size() < 5) {
            MenuItem beanMore = new MenuItem(MenuItem.ITEM_NAME_MORE, MenuItem.ITEM_ID_MORE);
            mPreferHomeList.add(beanMore);//添加更多按钮菜单
            mLlMenuBean.add(mPreferHomeList);
        } else {
            //大于等于五个菜单，需求不需要添加更多按钮菜单
            List<MenuItem> menuItems1 = mPreferHomeList.subList(0, 5);
            List<MenuItem> menuItems2 = mPreferHomeList.subList(5, mPreferHomeList.size());
            if (menuItems1 != null && menuItems1.size() > 0) {
                mLlMenuBean.add(menuItems1);
            }
            if (menuItems2 != null && menuItems2.size() > 0) {
                mLlMenuBean.add(menuItems2);
            }
        }
        mAdapter.setLlMenuBean(mLlMenuBean);
    }

    private void homeRequest() {
        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                //消息网络请求
                initRequestCount++;
                ServerClient.newInstance(mContext).queryMchHbfqEnable(mContext, Constants.TAG_HBFQ_ENABLE, null);
                ServerClient.newInstance(mContext).queryScoEnable(mContext, Constants.TAG_OPEN_SCAN_ORDER, null);
                ServerClient.newInstance(mContext).openFuncard(mContext, Constants.TAG_OPEN_FUNCARD, null);
                ServerClient.newInstance(mContext).openFuncardVerification(mContext, Constants.TAG_OPEN_FUNCARD_VERIFICATION, null);
                ServerClient.newInstance(mContext).queryDaoJiaEnable(mContext, Constants.TAG_DAOJIA_ENABLE, null);//查询渠道是否开通威到家
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST)
                        && !SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM)) {
                    ServerClient.newInstance(mContext).getPushMode(mContext, "TAG_PUSH_MODE", null);
                }
                if (MyApplication.getIsMerchant()) {//商户
                    ServerClient.newInstance(mContext).checkVerifyStatus(mContext, Constants.TAG_CHECK_VERIFY_STATUS, null);
                    ServerClient.newInstance(mContext).whiteList(mContext, Constants.TAG_WHITE_LIST, null);
                } else {
                    //其它角色
//                    ServerClient.newInstance(MyApplication.getContext()).queryDaojiaEdition(mContext, Constants.TAG_QUERY_DAOJIA_HOME, null);//直接查询商户是否开通威到家付费详情
                    if (TextUtils.isEmpty(mStoreName)) {
                        //获取展示的名称
                        ServerClient.newInstance(mContext).storePort(mContext, Constants.STORE_PORT_TAG, null);
                    }
                }
                if (!MyApplication.getIsCasher())
                    haveOpenSvcApp("TAG_PAY_SITE_OPEN_STATUS");
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    /**
     * 应用开通状态查询
     */
    private void haveOpenSvcApp(String tag) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(((MainActivity)getActivity()).mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("appCode", "CASH-POINT");
            ServerClient.newInstance(MyApplication.getContext()).haveOpenSvcApp(MyApplication.getContext(), tag, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 近七日交易趋势
     */
    private void getTradeStatisticsByDayTrendings() {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", DateUtil.getPreSevenDayDate());
        map.put("endTime", DateUtil.getTodayDateOfMonth());
        if (MyApplication.getIsMerchant()) {
            map.put("merchantId", MyApplication.getMechantId());
        }
        if (MyApplication.getIsManager()) {
            map.put("shopManagerId", MyApplication.getEmpId());
        }
        if (MyApplication.getIsCasher()) {
            map.put("cashierNo", MyApplication.getEmpId());
        }
        ServerClient.newInstance(MyApplication.getContext()).getTradeStatisticsByDayTrendings(MyApplication.getContext(), Constants.TAG_DAY_TRADE_TREND_HOME, map);
    }


    //获取收款记录即账单数据
    private void getBillData() {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", "5");
        map.put("currentPage", "1");
        map.put("startTime", DateUtil.getBeforeThirtyDays() + " 00:00:00");
        map.put("endTime", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");
        //map.put("otherType", otherType);
        map.put("cashPointType", 1);
        if (MyApplication.getIsCasher()) {
            map.put("userId", MyApplication.getUserId());
        }
        int[] tradeStatus = {2, 4};
        map.put("tradeStatusList", tradeStatus);
        ServerClient.newInstance(MyApplication.getContext()).getBills(MyApplication.getContext(), Constants.TAG_GET_BILLS_HOME, map);
    }

    /**
     * 注册个推收款的广播
     */
    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_ORDER_DATA);
        //intentFilter.addAction(Constants.ACTION_ORDER_REFUND_DATA);
        mOrderBroadcastReceiver = new OrderBroadcastReceiver();
        getActivity().registerReceiver(mOrderBroadcastReceiver, intentFilter);
    }


    class OrderBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.ACTION_ORDER_DATA.equals(action)) {
                //刷新交易数据
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    //收款记录请求
                    getBillData();
                    mRvHomeFragment.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //今日交易数据
                            getHomeTodayData();
                        }
                    }, 1000);
                    mRvHomeFragment.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //近7天的收款统计报表请求
                            if (isVisible())
                                getTradeStatisticsByDayTrendings();
                        }
                    }, 10000);
                }
            }
        }
    }

    //获取今日交易数据
    private void getHomeTodayData() {
        /*if (MyApplication.getIsCasher()) {
            String startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
            String endTime = DateUtil.formatTime(System.currentTimeMillis());
            Map<String, Object> map = new HashMap<>();
            map.put("tab", 1);
            map.put("startTime", startTime);
            map.put("endTime", endTime);
            if (MyApplication.getIsCasher() && !TextUtils.isEmpty(MyApplication.getUserId())) {
                map.put("userId", MyApplication.getUserId());
            }
            ServerClient.newInstance(mContext).getReport(mContext, Constants.TAG_HOME_TODAY_DATA, map);
        } else {
            ServerClient.newInstance(mContext).homeTodayData(mContext, Constants.TAG_HOME_TODAY_DATA);
        }*/
        Map<String, Object> map = new HashMap<>();
        if (MyApplication.getIsCasher() && !TextUtils.isEmpty(MyApplication.getUserId())) {
            map.put("userId", MyApplication.getUserId());
        }
        ServerClient.newInstance(mContext).homeTodayData(mContext, Constants.TAG_HOME_TODAY_DATA, map);
    }


    //获取通知数据
    private void homeMessage() {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", "15");
        map.put("currentPage", "1");
        ServerClient.newInstance(MyApplication.getContext()).getMessage(MyApplication.getContext(), Constants.TAG_GET_HOME_MESSAGE, map);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //门店信息返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHomeFragmentEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.STORE_PORT_TAG)) {
            StoreBean bean = (StoreBean) event.getMsg();
            ((MainActivity) getActivity()).dismissLoading();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (MyApplication.getIsCasher()) {
                                if (!TextUtils.isEmpty(bean.getData().getStoreName())) {
                                    if (mAdapter != null) {
                                        mStoreName = bean.getData().getStoreName();
                                        mAdapter.setHeaderData(mStoreName);
                                    }
                                } else {
                                    if (mAdapter != null) {
                                        mAdapter.setHeaderData("");
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_CHECK_VERIFY_STATUS)) {
            MerChantModifyBean msg = (MerChantModifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.CHECK_VERIFY_STATUS_TRUE:
                    if (msg.getData() != null && msg.getData().getExamineRemark() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_EXAMINE_REMARK, msg.getData().getExamineRemark());
                    } else {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_EXAMINE_REMARK, "");
                    }
                    if (msg.getData() != null) {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_EXAMINE_STATUS, msg.getData().getExamineStatus());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE, msg.getData().getAccountType());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_COMPANY_MERCHANT_TYPE, msg.getData().getAccountAttribute());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, msg.getData().getAttestationStatus());//商户认证状态
                    } else {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS, 0);//商户认证状态
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_EXAMINE_STATUS, -1);
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE, -1);
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_COMPANY_MERCHANT_TYPE, -1);
                    }
                    initMenuDataUI(true);
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_LOAN_DETAIL)) {
            LoanDetailBean msg = (LoanDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    mMainActivity.dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    ((MainActivity) getActivity()).dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        getAgainUrl();
                    } else {
                        getFirstUrl();
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_LOAN_FIRST) || event.getTag().equals(Constants.TAG_LOAN_AGAIN)) {
            (mMainActivity).dismissLoading();
            UrlBean msg = (UrlBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && !TextUtils.isEmpty(msg.getData())) {
                        Intent intent = new Intent(getActivity(), RegisterActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, msg.getData());
                        intent.putExtra(Constants.REGISTER_INTENT_URL, Constants.LOAN_TO_REGISTER);
                        startActivity(intent);
                    } else {
                        MyToast.showToastShort(UIUtils.getString(R.string.error_url_illegal));
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_WHITE_LIST)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_WHITE, msg.getData().isWhiteListEnabled());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_WHITE, false);
                    }
                    initMenuDataUI(true);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_HBFQ_ENABLE)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_HUABEI_WHITE, msg.getData().isEnable());
                        //getData(mList);
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_HUABEI_WHITE, false);
                    }
                    initMenuDataUI(true);
                    break;
            }
        }
        if (event.getTag().equals("TAG_PUSH_MODE") || event.getTag().equals("TAG_PUSH_MODE_AGAIN")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PushModeBean msg = (PushModeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (event.getTag().equals("TAG_PUSH_MODE_AGAIN"))
                        MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (event.getTag().equals("TAG_PUSH_MODE_AGAIN")) {
                        if (msg.getError() != null) {
                            if (msg.getError().getCode() != null) {
                                if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (msg.getError().getMessage() != null) {
                                        ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                    }
                                } else {
                                    if (msg.getError().getMessage() != null) {
                                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        /*String pushMode = msg.getData().getPushFrequencys().getTypeCode();
                        String showTime = SpStayUtil.getString(MyApplication.getContext(), Constants.SP_SHOW_TIME + MyApplication.getUserId());
                        if (TextUtils.isEmpty(showTime)
                                || "EVERY_DAY".equals(pushMode) && DateUtil.afterDayFirst(showTime)
                                || "EVERY_WEEK".equals(pushMode) && DateUtil.afterWeekFirst(showTime)
                                || "EVERY_LOGIN".equals(pushMode))*/
                        if ((event.getTag().equals("TAG_PUSH_MODE") && msg.getData().getPushMode() == 1)
                                || event.getTag().equals("TAG_PUSH_MODE_AGAIN"))
                            showPlatformUpdateDialog(msg.getData());
                    }
                    break;
            }
        }
        if (event.getTag().equals("TAG_SWITCH_NEW_PLATFORM")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showUpdatedSuccessDialog();
                    mAdapter.setVisible(View.GONE);
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_WHITE_LIST_AGAIN)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    (mMainActivity).dismissLoading();
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    (mMainActivity).dismissLoading();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_WHITE, msg.getData().isWhiteListEnabled());
                        if (MyApplication.isWhite()) {
                            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                ServerClient.newInstance(MyApplication.getContext()).loanDetail(MyApplication.getContext(), Constants.TAG_LOAN_DETAIL, null);
                            } else {
                                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                            }
                        } else {
                            MyToast.showToast(ToastHelper.toStr(R.string.error_not_loan), Toast.LENGTH_SHORT);
                        }
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_WHITE, false);
                    }
                    initMenuDataUI(true);
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_OPEN_MEMBER)) {
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, msg.getData().getRetCode());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, msg.getData().isStatisticsCode());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, false);
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, false);
                    }
                    initMenuDataUI(true);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_OPEN_SCAN_ORDER)
                || event.getTag().equals(Constants.TAG_DAOJIA_ENABLE)
                || event.getTag().equals(Constants.TAG_OPEN_FUNCARD)
                || event.getTag().equals(Constants.TAG_OPEN_FUNCARD_VERIFICATION)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (event.getTag().equals(Constants.TAG_OPEN_SCAN_ORDER)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_OPEN_SCAN_ORDER, (boolean) msg.getData());
                        } else if (event.getTag().equals(Constants.TAG_DAOJIA_ENABLE)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, (boolean) msg.getData());
                        } else if (event.getTag().equals(Constants.TAG_OPEN_FUNCARD)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_ENABLE, (boolean) msg.getData());
                        } else if (event.getTag().equals(Constants.TAG_OPEN_FUNCARD_VERIFICATION)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, (boolean) msg.getData());
                        }
                    } else {
                        if (event.getTag().equals(Constants.TAG_OPEN_SCAN_ORDER)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_OPEN_SCAN_ORDER, false);
                        } else if (event.getTag().equals(Constants.TAG_DAOJIA_ENABLE)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, false);
                        } else if (event.getTag().equals(Constants.TAG_OPEN_FUNCARD)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_ENABLE, false);
                        } else if (event.getTag().equals(Constants.TAG_OPEN_FUNCARD_VERIFICATION)) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, false);
                        }
                    }
                    initMenuDataUI(true);
                    break;
            }
        } else if (event.getTag().equals("TAG_HOME_PAY_SITE_OPEN_STATUS") || event.getTag().equals("TAG_PAY_SITE_OPEN_STATUS")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                        if (msg.getError().getMessage() != null) {
                            ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                        }
                    } else {
                        if (msg.getError().getMessage() != null) {
                            MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_CASH_POINT_OPEN, msg.getData().getServiceStatus());
                        if(event.getTag().equals("TAG_HOME_PAY_SITE_OPEN_STATUS")) {
                            if ("2".equals(MyApplication.getCashPointOpenStatus())) {
                                startActivity(new Intent(getActivity(), PaySiteListActivity.class));
                            } else {
                                startActivity(new Intent(getActivity(), InstructionPaySiteActivity.class).putExtra(Constants.INTENT_OUT_URL, "file:///android_asset/instruction_pay_site.html"));
                            }
                        }
                    } else {
                        ToastUtil.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
    }

    private void getFirstUrl() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).loanFrist(MyApplication.getContext(), Constants.TAG_LOAN_FIRST, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getAgainUrl() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).loanAgain(MyApplication.getContext(), Constants.TAG_LOAN_AGAIN, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }


    //今日数据返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTodayData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_HOME_TODAY_DATA)) {
            ReportBean mHomedatas = (ReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.GET_REPORT_FALSE:
                    break;
                case Constants.GET_REPORT_TRUE:
                    if (mHomedatas.getData() != null) {
                        mAdapter.setTodayTradeData(mHomedatas);
                    }
                    break;
            }
        }
        refreshCount--;
        if (refreshCount == 0) {
            setRefreshState(500);
        }

    }

    //首页消息返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHomeNotice(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_HOME_MESSAGE)) {
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    LogUtil.i("zhouwei", getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    LogUtil.i("zhouwei", "ON_EVENT_FALSE");
                    mViewTypeList.remove(Integer.valueOf(HomeFragmentAdapter.VIEW_TYPE_NOTICE));
                    mAdapter.setViewTypeList(mViewTypeList);
                    break;
                case Constants.ON_EVENT_TRUE:
                    MessageData msg = (MessageData) event.getMsg();
                    if (msg.getData().getDataList() != null && msg.getData().getDataList().size() > 0) {
                        if (!mViewTypeList.contains(HomeFragmentAdapter.VIEW_TYPE_NOTICE)) {
                            if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_NOTICE)) {
                                int tradePosition = mAdapter.findItemPosition(HomeFragmentAdapter.VIEW_TYPE_TRADE);
                                mViewTypeList.add(tradePosition + 1, HomeFragmentAdapter.VIEW_TYPE_NOTICE);
                            } else {
                                mViewTypeList.remove(Integer.valueOf(HomeFragmentAdapter.VIEW_TYPE_NOTICE));
                            }
                            mAdapter.setViewTypeList(mViewTypeList);
                        }
                        List<MessageData.DataEntity.DataList> dataList = msg.getData().getDataList();
                        mAdapter.setMessageData(dataList);
                    } else {
                        mViewTypeList.remove(Integer.valueOf(HomeFragmentAdapter.VIEW_TYPE_NOTICE));
                        mAdapter.setViewTypeList(mViewTypeList);
                    }
                    break;
            }
            if (mSmartGuide != null && mSmartGuide.isShowing()) {
                //防止新手导航的蒙层已经展示了，消息栏再插进来，导致菜单栏处的蒙层镂空位置不对，需要重新显示下
                mSmartGuide.clearLayers();
                mSmartGuide.dismiss();
                HomeGuideHelp.showGuideStep1(HomeFragment.this, mContext, mPreferHomeList != null && mPreferHomeList.size() > 1);//大于1是因为只有1个更多的菜单时不会显示菜单栏
            }
        }
    }

    //收款记录账单返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBillDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_BILLS_HOME)) {
            //DialogUtil.safeCloseDialog(mLoadDialog);
            BillsBean msg = (BillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    mAdapter.setBillList(null, false);
                    break;
                case Constants.GET_BILLS_FALSE:
                    mAdapter.setBillList(null, true);
                    break;
                case Constants.GET_BILLS_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        List<BillsBean.DataEntity> data = msg.getData();
                        if (data.size() > 5) {
                            data = data.subList(0, 5);
                        }
                        mAdapter.setBillList(data, true);
                    } else {
                        mAdapter.setBillList(null, true);
                    }
                    break;
            }

        }

        initRequestCount--;
        if (initRequestCount == 0) {
            //所有请求完成，开始弹窗新手导航引导
            boolean guideOver = SpStayUtil.getBoolean(mContext, Constants.SP_SMART_GUIDE_OVER);
            if (!guideOver) {
                mHandler.sendEmptyMessageDelayed(1, 1000);//延迟是为了等UI更新完位置确定
            }
        }
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                View mFirstFunctionMenuView = mRootView.findViewById(R.id.id_menu_function_first);
                if (mPreferHomeList != null && mPreferHomeList.size() > 1 && mFirstFunctionMenuView == null) {//辅助判断UI更新完成。大于1是因为只有1个更多的菜单时不会显示菜单栏
                    sendEmptyMessageDelayed(1, 500);
                } else {
                    try {
                        if (isVisible()) {
                            mSmartGuide = HomeGuideHelp.showGuideStep1(HomeFragment.this, mContext, mPreferHomeList != null && mPreferHomeList.size() > 1);
                            SpStayUtil.putBoolean(mContext, Constants.SP_SMART_GUIDE_OVER, true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        RelativeLayout rl_main = mMainActivity.findViewById(R.id.rl_activity_main);
                        rl_main.setVisibility(View.GONE);
                    }
                }
            } else if (msg.what == 2) {
                //防止页面不能点击
                RelativeLayout rl_main = mMainActivity.findViewById(R.id.rl_activity_main);
                rl_main.setVisibility(View.GONE);
            }
        }
    };


    //收款统计数据返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTradeStatisticsEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_DAY_TRADE_TREND_HOME)) {
            ReportTradeTrendBean msg = (ReportTradeTrendBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mAdapter.setReportTradeTrendData(msg);
                    break;
            }
        }
    }


    //商家说数据返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMchTalkEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_HOME_MCHTALK)) {
            MchtalkBean msg = (MchtalkBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg != null && msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        List<MchtalkBean.MchTalkItemBean> data = msg.getData().getData();
                        if (data.size() > 2) {
                            data = data.subList(0, 2);
                        }
                        mAdapter.setMerChantNewsItemBeans(data);
                    }
                    break;
            }
        }
    }

    //获取商家说的数据
    private void getMchTalkData() {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", "15");
        map.put("currentPage", "1");
        ServerClient.newInstance(MyApplication.getContext()).getMchTalk(MyApplication.getContext(), Constants.TAG_GET_HOME_MCHTALK, map);
    }

    private void initView() {
        mSwipeRefreshLayout = mRootView.findViewById(R.id.swipeRefreshLayout);
        mLlContent = mRootView.findViewById(R.id.ll_content);
        mSwipeRefreshLayout.setRefreshEnable(true);
        mSwipeRefreshLayout.setLoadmoreEnable(false);
        mRvHomeFragment = mRootView.findViewById(R.id.rv_home_fragment);
        mRvHomeFragment.setLayoutManager(new LinearLayoutManager(getContext()));

        mRvHomeFragment.getItemAnimator().setAddDuration(0);
        mRvHomeFragment.getItemAnimator().setChangeDuration(0);
        mRvHomeFragment.getItemAnimator().setMoveDuration(0);
        mRvHomeFragment.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) mRvHomeFragment.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    @Override
    public void onMenuItemClick(MenuItem menuItem) {
        //功能菜单点击
        if (menuItem.getId() == MenuItem.ITEM_ID_MORE) {
            //更多
            mMainActivity.setRadioBtnChecked(R.id.rb_bottom_app);
        } else {
            MenuClickUtil.clickMenu(mMainActivity, menuItem);
        }
    }

    @Override
    public void onReceiveRecordItemClick(BillsBean.DataEntity dataEntity) {
        //收款记录item点击
        LogUtil.d(TAG, "onReceiveRecordItemClick");
        if (dataEntity != null) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                Map<String, Object> map = new HashMap<>();
                if (dataEntity.getApiProvider() == 7 || dataEntity.getApiProvider() == 9) {
                    if (!StringUtils.isEmptyOrNull(dataEntity.getOrderNo())) {
                        map.put("orderNo", dataEntity.getOrderNo());
                    }
                    String storeMerchantId = "";
                    if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant()) {
                        storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
                    } else if (MyApplication.getIsCasher()) {
                        storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                    }
                    map.put("storeMerchantId", storeMerchantId);
                    map.put("type", dataEntity.getApiProvider());
                    ServerClient.newInstance(getActivity()).vipBillDetail(mContext, Constants.TAG_GET_ORDER_DETAIL_HOME, map);
                } else {
                    if (!StringUtils.isEmptyOrNull(dataEntity.getOrderNo())) {
                        map.put("orderNo", dataEntity.getOrderNo());
                    }
                    if (!StringUtils.isEmptyOrNull(dataEntity.getRefundNo())) {
                        map.put("refundNo", dataEntity.getRefundNo());
                    }
                    ServerClient.newInstance(getActivity()).getOrderDetail(mContext, Constants.TAG_GET_ORDER_DETAIL_HOME, map);
                }
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    //订单详情返回数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderDetail(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_ORDER_DETAIL_HOME)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    mMainActivity.getLoginDialog(mMainActivity, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    mMainActivity.showCommonNoticeDialog(mMainActivity, msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        if (Constants.HPAY_REFUND_ORDER.equals(otherType)) {
                            intent.putExtra(Constants.INTENT_TRADE_DETAIL, PayBeanUtil.getTradeDetail(data));
                            intent.putExtra(Constants.INTENT_BILL_DATA, data);
                            intent.setClass(MyApplication.getContext(), RefundDetailActivity.class);
                        } else {
                            intent.putExtra(Constants.INTENT_BILL_DATA, data);
                            intent.setClass(MyApplication.getContext(), PayDetailActivity.class);
                        }
                        startActivity(intent);
//                        startActivityForResult(intent, Constants.REQUEST_BILL_DETAIL);
                    }
                    break;
            }
        }
    }

    @Override
    public void onMoreRecordClick() {
        //更多收款记录点击
        LogUtil.d(TAG, "onMoreRecordClick");
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
        } else {
            startActivity(new Intent(getActivity(), BillActivity.class));
        }

    }

    //客服
    @Override
    public void onClickNotice() {
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
        } else {
            Intent intent = new Intent(getActivity(), QuestionActivity.class);
            intent.putExtra(Constants.REGISTER_INTENT, ConfigUtil.getContactUsUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onClickUpdate() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(mContext).getPushMode(mContext, "TAG_PUSH_MODE_AGAIN", null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //收款
    @Override
    public void onClickMoney() {
        //收款
        MtaUtils.mtaId(getActivity(), "D001");
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
        } else {
            if (MyApplication.isStoreNull()) {
                MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
            } else {
                startActivity(new Intent(getActivity(), PayActivity.class));
            }
        }
    }

    @Override
    public void onClickScan(View view) {
        HomePopupWindow homePopupWindow = new HomePopupWindow(new HomePopupWindow.OnSubmitListener() {
            @Override
            public void submit(int index) {
                switch (index) {
                    case 0:
                        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            scanLogin(Constants.INTENT_NAME_SCAN_LOGIN);
                        }
                        break;
                    case 1:
                        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            scanLogin(Constants.INTENT_NAME_SCAN_VERIFY);
                        }
                        break;
                    case 2:
                        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            scanLogin(Constants.INTENT_NAME_SCAN_REFUND);
                        }
                        break;
                }
            }
        }, getActivity(), MyApplication.isOpenFuncard());
        homePopupWindow.showAsDropDown(view);
    }

    private void showPlatformUpdateDialog(PushModeBean.DataBean data) {
        mSelectUpdateDialog = new SelectUpdateDialog(getActivity(), ((MainActivity) getActivity()).mRlContent, data);
        DialogHelper.resizeFull(getActivity(), mSelectUpdateDialog);
        mSelectUpdateDialog.setOnClickCancelListener(() -> {
            mSelectUpdateDialog.dismiss();
            mSelectUpdateDialog = null;
            mAdapter.setVisible(View.VISIBLE);
            BaiduMtjUtils.eventId("升级引导_考虑一下");
            BaiduMtjUtils.eventEnd("升级引导_停留时长");
        });
        mSelectUpdateDialog.setOnClickOkListener(() -> {
            showUpdateNoticeDialog();
            BaiduMtjUtils.eventId("升级引导_立刻升级");
            BaiduMtjUtils.eventEnd("升级引导_停留时长");
        });
        mSelectUpdateDialog.show();
        BaiduMtjUtils.eventId("升级引导_弹出");
        BaiduMtjUtils.eventStart("升级引导_停留时长");
        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_SHOW_TIME + MyApplication.getUserId(), DateUtil.formatYYMD(SystemClock.currentThreadTimeMillis()));
    }

    private void showUpdateNoticeDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(getActivity(), getString(R.string.dialog_update_title), getString(R.string.dialog_update_notice), "返回", "确认升级", R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    mSelectUpdateDialog.dismiss();
                    mSelectUpdateDialog = null;
                    BaiduMtjUtils.eventId("升级引导_确认升级");
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        ServerClient.newInstance(mContext).switchNewPlatform(mContext, "TAG_SWITCH_NEW_PLATFORM", null);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            });
            mSelectDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    mAdapter.setVisible(View.VISIBLE);
                    BaiduMtjUtils.eventId("升级引导_返回");
                }
            });
            DialogHelper.resize(getActivity(), mSelectDialog);
        }
        mSelectDialog.show();
    }

    private void showUpdatedSuccessDialog() {
        if (mUpdateSuccessNoticeDialog == null) {
            mUpdateSuccessNoticeDialog = new UpdateSuccessNoticeDialog(getActivity());
        }
        mUpdateSuccessNoticeDialog.show();
    }

    public void setNoticeVisible(int visible) {
        if (mAdapter == null || mViewTypeList == null || mViewTypeList.isEmpty())
            return;
        if (visible == View.VISIBLE) {
            int tradePosition = mAdapter.findItemPosition(HomeFragmentAdapter.VIEW_TYPE_TRADE);
            mViewTypeList.add(tradePosition + 1, HomeFragmentAdapter.VIEW_TYPE_NOTICE);
        } else {
            mViewTypeList.remove(Integer.valueOf(HomeFragmentAdapter.VIEW_TYPE_NOTICE));
        }
        mAdapter.setViewTypeList(mViewTypeList);
    }

    private void scanLogin(String intentName) {
        Intent intent = new Intent(getActivity(), CaptureActivity.class);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        startActivity(intent);
    }

    //点击通知
    @Override
    public void onNoticeClick() {
        //消息
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
        } else {
            startActivity(new Intent(getActivity(), MessageActivity.class));
        }
    }

    //点击交易数据
    @Override
    public void onTradeItemClick() {
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
        } else {
            startActivity(new Intent(getActivity(), CollectActivity1.class));
        }
    }


    //收款统计查看更多
    @Override
    public void onMoreReceiveClick() {
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
        } else {
            startActivity(new Intent(getActivity(), CollectActivity1.class));
        }
    }


    @Override
    public void onMchTalkItemClick(MchtalkBean.MchTalkItemBean mchTalkItemBean) {
        //商家说条目点击
        Intent intent = new Intent(getActivity(), RegisterActivity.class);
        intent.putExtra(Constants.REGISTER_INTENT, mchTalkItemBean.getStaticPageAccessPath());
        startActivity(intent);
    }

    @Override
    public void onMoreTalkClick() {
        //商家说更多
        mMainActivity.setRadioBtnChecked(R.id.rb_bottom_mch_talk);
    }

    public RecyclerView getRvHomeFragment() {
        return mRvHomeFragment;
    }

    @Override
    public void onDestroy() {
        SpStayUtil.putBoolean(mContext, Constants.SP_SMART_GUIDE_OVER, true);
        super.onDestroy();
    }
}
