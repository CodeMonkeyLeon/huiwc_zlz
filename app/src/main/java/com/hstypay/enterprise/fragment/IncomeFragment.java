package com.hstypay.enterprise.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.DateChoiceActivity;
import com.hstypay.enterprise.activity.IncomeDetailActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.adapter.ReportIncomeAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.ReportDetailBean;
import com.hstypay.enterprise.bean.ReportIncomeBean;
import com.hstypay.enterprise.bean.ReportMonthBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:15
 * @描述: ${TODO}
 */

public class IncomeFragment extends BaseFragment implements ReportIncomeAdapter.OnRecyclerViewItemClickListener, RadioGroup.OnCheckedChangeListener {
    private View mView;
    private TextView mTvTitle, mTvNull, mTvIncomeDate;
    private RecyclerView mRvReport;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private ReportIncomeAdapter mAdapter;
    private List<ReportDetailBean> mList;
    private RadioButton mRbDay, mRbMonth;
    private RadioGroup mRgTitle;
    private RelativeLayout mRlIncomeDate;
    private boolean isRefreshed;//刷新
    private int tab = 1;
    private String startTime = DateUtil.getBeforeDate() + " 00:00:00";
    private String endTime = DateUtil.getBeforeDate() + " 23:59:59";

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        ((MainActivity) getActivity()).dismissLoading();
        mView = inflater.inflate(R.layout.fragment_income, container, false);
        StatusBarUtil.setImmersiveStatusBar(getActivity(),true, mView);
        initView(mView);
        initEvent();
        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            initData();
        }
        return mView;
    }

    public void initView(View view) {
        mTvTitle = (TextView) view.findViewById(R.id.tv_title);//online
        mTvNull = (TextView) view.findViewById(R.id.tv_not_data);

        mRgTitle = (RadioGroup) view.findViewById(R.id.rg_title);
        mRbDay = (RadioButton) view.findViewById(R.id.rb_title_report_day);
        mRbMonth = (RadioButton) view.findViewById(R.id.rb_title_report_month);

        mRlIncomeDate = (RelativeLayout) view.findViewById(R.id.rl_income_date);
        mTvIncomeDate = (TextView) view.findViewById(R.id.tv_income_date);

        mTvIncomeDate.setText(DateUtil.getBeforeDate());
        if (tab == 1) {
            mRlIncomeDate.setVisibility(View.VISIBLE);
        } else if (tab == 2) {
            mRlIncomeDate.setVisibility(View.GONE);
        }

        initRecyclerView(view);
        initSwipeRefreshLayout(view);
    }

    private void initEvent() {
        mRgTitle.setOnCheckedChangeListener(this);
        mTvIncomeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MtaUtils.mtaId(getActivity(),"K002");
                Intent intentDate = new Intent(getActivity(), DateChoiceActivity.class);
                intentDate.putExtra(Constants.INTENT_NAME, Constants.INTENT_INCOME_CHOICE_DATE);
                startActivityForResult(intentDate, Constants.REQUEST_CHOICE_DATE);
            }
        });
    }

    private void initRecyclerView(View view) {
        mRvReport = (RecyclerView) view.findViewById(R.id.rv_income_report);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRvReport.setLayoutManager(mLinearLayoutManager);
        mRvReport.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setLoadmoreEnable(false);
        if (AppHelper.getSwipeRefresh()) {
            mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    if (tab == 1) {
                        getReportDay(false, startTime, endTime);
                    } else if (tab == 2) {
                        getReportMonth(false);
                    }
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onLoading() {
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initData() {
        mList = new ArrayList<>();
        mAdapter = new ReportIncomeAdapter(getActivity(), mList);
        mAdapter.setOnItemClickListener(this);
        mRvReport.setAdapter(mAdapter);
        getReportDay(true, startTime, endTime);
    }

    private void getReportDay(boolean showDialog, String startTime, String endTime) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                ((MainActivity) getActivity()).showNewLoading(true, getString(R.string.public_loading));
            }
            Map<String, Object> map = new HashMap<>();
            map.put("startTime", startTime);
            map.put("endTime", endTime);
            ServerClient.newInstance(MyApplication.getContext()).reportDay(MyApplication.getContext(), Constants.TAG_REPORT_DAY, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getReportMonth(boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                ((MainActivity) getActivity()).showNewLoading(true, getString(R.string.public_loading));
            }
            ServerClient.newInstance(MyApplication.getContext()).reportMonth(MyApplication.getContext(), Constants.TAG_REPORT_MONTH, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK && requestCode == Constants.REQUEST_CHOICE_DATE) {
            Bundle extras = data.getExtras();
            String time = extras.getString(Constants.RESULT_CHOICE_DATE_INTENT);
            if (!StringUtils.isEmptyOrNull(time)) {
                String t[] = time.split("\\|");
                try {
                    if (DateUtil.formartDateToMMDD(t[0]).equals(DateUtil.formartDateToMMDD(t[1]))) {
                        mTvIncomeDate.setText(DateUtil.formartDateToYYMMDD(t[0]));
                    } else {
                        mTvIncomeDate.setText(DateUtil.formartDateToYYMMDD(t[0]) + " " + getString(R.string.tv_least) + " "
                                + DateUtil.formartDateToYYMMDD(t[1]));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //开始时间与结束时间发送到服务器获取数据
                startTime = t[0];
                endTime = t[1];
                LogUtil.d("date=" + startTime + "///" + endTime);
                getReportDay(true, startTime, endTime);
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        if (tab == 2) {
            Intent intent = new Intent(getActivity(), IncomeDetailActivity.class);
            intent.putExtra(Constants.INTENT_REPORT_TAB, tab);
            intent.putExtra(Constants.INTENT_REPORT_MONTH, mList.get(position).getPayTradeTime());
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), IncomeDetailActivity.class);
            intent.putExtra(Constants.INTENT_REPORT_TAB, tab);
            intent.putExtra(Constants.INTENT_REPORT_START_TIME, startTime);
            intent.putExtra(Constants.INTENT_REPORT_END_TIME, endTime);
            startActivity(intent);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_REPORT_DAY)) {
            ReportIncomeBean msg = (ReportIncomeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, 500);
                    mList.clear();
                    if (msg != null && msg.getData() != null) {
                        mList.add(msg.getData());
                        mTvNull.setVisibility(View.GONE);
                    } else {
                        ReportDetailBean reportDetailBean = new ReportDetailBean();
                        reportDetailBean.setStartTime(startTime);
                        reportDetailBean.setEndTime(endTime);
                        mList.add(reportDetailBean);
                        mTvNull.setVisibility(View.GONE);
                    }
                    mAdapter.notifyDataSetChanged();
                    break;
            }
            ((MainActivity) getActivity()).dismissLoading();
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_REPORT_MONTH)) {
            ReportMonthBean msg = (ReportMonthBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, 500);
                    mList.clear();
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().size() > 0) {
                            mList.addAll(msg.getData());
                            mTvNull.setVisibility(View.GONE);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                    break;
            }
            ((MainActivity) getActivity()).dismissLoading();
            isRefreshed = false;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rb_title_report_day:
                MtaUtils.mtaId(getActivity(),"K001");
                tab = 1;
                getReportDay(true, startTime, endTime);
                mRlIncomeDate.setVisibility(View.VISIBLE);
                break;
            case R.id.rb_title_report_month:
                MtaUtils.mtaId(getActivity(),"K003");
                tab = 2;
                getReportMonth(true);
                mRlIncomeDate.setVisibility(View.GONE);
                break;
        }
    }

    private void setErrorState(boolean isRefreshed, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
            StatusBarUtil.setImmersiveStatusBar(getActivity(),true, mView);
        }
    }
}