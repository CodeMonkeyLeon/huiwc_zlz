package com.hstypay.enterprise.fragment;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.adapter.HomeMerchantNewsAdapter;
import com.hstypay.enterprise.adapter.viewholder.HomeFragmentMerchantPagerViewHolder;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.MchtalkBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kuangzeyu
 * @time 2021/3/22
 * @desc 商家说
 */
public class MchTalkFragment extends BaseFragment implements HomeFragmentMerchantPagerViewHolder.OnHomeMchTalkItemClickListener {
    private View mRootView;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRvMchTalkFragment;
    private SafeDialog mLoadDialog;

    public static final int mPageSize = 15;
    protected int mCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private TextView mTvNotData;

    private List<MchtalkBean.MchTalkItemBean> mMchTalkItemBeans = new ArrayList<>();
    private HomeMerchantNewsAdapter mHomeMerchantNewsAdapter;

    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        StatusBarUtil.setTranslucentStatusFontIconDark(getActivity(), mRootView);
        mCurrentPage = 1;
        getMchTalkData();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.fragment_mch_talk, container, false);
        StatusBarUtil.setTranslucentStatusFontIconDark(getActivity(),  mRootView);
        initView();
        initData();
        initEvent();
        return mRootView;
    }

    private void initEvent() {
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getMchTalkData();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    //mCurrentPage++;//要在获取数据成功后才能++，否则失败了又得--回去
                    getMchTalkData();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

        mHomeMerchantNewsAdapter.setOnHomeMchTalkItemClickListener(this);

    }

    private void initData() {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);

        mHomeMerchantNewsAdapter = new HomeMerchantNewsAdapter(getContext());
        mRvMchTalkFragment.setAdapter(mHomeMerchantNewsAdapter);

    }

    private void initView() {
        mSwipeRefreshLayout = mRootView.findViewById(R.id.swipeRefreshLayout);
        mRvMchTalkFragment = mRootView.findViewById(R.id.rv_mch_talk_fragment);
        mTvNotData = mRootView.findViewById(R.id.tv_not_data);
        mSwipeRefreshLayout.setRefreshEnable(true);
        mSwipeRefreshLayout.setLoadmoreEnable(true);
        mRvMchTalkFragment.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    //获取商家说的数据
    private void getMchTalkData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())){
            if (!(isPullRefresh || isLoadmore || mMchTalkItemBeans.size()>0)){
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mPageSize);
            map.put("currentPage", mCurrentPage);
            ServerClient.newInstance(MyApplication.getContext()).getMchTalk(MyApplication.getContext(), Constants.TAG_GET_MCH_MCHTALK, map);
        }else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //商家说数据返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpListManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MCH_MCHTALK)){
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            MchtalkBean msg = (MchtalkBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity)(getActivity())).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && msg.getData() != null && msg.getData().getData().size()>0) {
                        if (mCurrentPage==1){
                            mMchTalkItemBeans.clear();
                        }
                        mMchTalkItemBeans.addAll(msg.getData().getData());
                        mHomeMerchantNewsAdapter.setData(mMchTalkItemBeans,1);

                        mCurrentPage++;

                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_nore));
                        }else {
                            mMchTalkItemBeans.clear();//使后面会展示空视图
                            mHomeMerchantNewsAdapter.setData(mMchTalkItemBeans,1);
                        }
                    }

                    if (mMchTalkItemBeans !=null && mMchTalkItemBeans.size()>0){
                        mTvNotData.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    }else{
                        mTvNotData.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                    }
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     * */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onMchTalkItemClick(MchtalkBean.MchTalkItemBean mchTalkItemBean) {
        //商家说条目点击
        Intent intent = new Intent(getActivity(), RegisterActivity.class);
        intent.putExtra(Constants.REGISTER_INTENT, mchTalkItemBean.getStaticPageAccessPath());
        startActivity(intent);
    }

    @Override
    public void onMoreTalkClick() {

    }
}
