package com.hstypay.enterprise.fragment;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.adapter.MsgRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.MessageData;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:15
 * @描述: ${TODO}
 */

public class MessageFragment extends BaseFragment implements MsgRecyclerAdapter.OnRecyclerViewItemClickListener {
    private View mView;
    private ImageView mIvBack;
    private TextView mButton, mTvTitle, mTvNull;
    private RecyclerView mRvMesasage;
    //private static final String GET_MESSAGE_TAG = "get_message_tag";
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private MsgRecyclerAdapter mMsgRecyclerAdapter;
    private List<MessageData.DataEntity.DataList> mMsgList;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多


    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        ((MainActivity) getActivity()).dismissLoading();
        currentPage = 2;
        mView = inflater.inflate(R.layout.fragment_meassge, container, false);
        StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        initView(mView);
        initEvent();
        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            initData();
        }
        return mView;
    }

    public void initView(View view) {
        mIvBack = (ImageView) view.findViewById(R.id.iv_back);
        mButton = view.findViewById(R.id.button_title);
        mTvTitle = (TextView) view.findViewById(R.id.tv_title);
        mTvNull = (TextView) view.findViewById(R.id.tv_not_data);

        mTvTitle.setText("通知");
        mIvBack.setVisibility(View.INVISIBLE);
        mButton.setVisibility(View.INVISIBLE);

        initRecyclerView(view);
        initSwipeRefreshLayout(view);
    }

    private void initRecyclerView(View view) {
        mRvMesasage = (RecyclerView) view.findViewById(R.id.rv_message);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRvMesasage.setLayoutManager(mLinearLayoutManager);
        mRvMesasage.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        /*mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        mSwipeRefreshLayout.setHeaderView(R.layout.refresh_view);*/
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    getMessage("15", "1");
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getMessage(pageSize + "", currentPage + "");
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void initEvent() {
    }

    public void initData() {
        mMsgList = new ArrayList<>();
        mMsgRecyclerAdapter = new MsgRecyclerAdapter(getActivity(), mMsgList);
        mMsgRecyclerAdapter.setOnItemClickListener(this);
        mRvMesasage.setAdapter(mMsgRecyclerAdapter);

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ((MainActivity) getActivity()).showNewLoading(true, getString(R.string.public_loading));
            getMessage(pageSize + "", "1");
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getMessage(String pageSize, String currentPage) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize", pageSize);
        map.put("currentPage", currentPage);
        ServerClient.newInstance(MyApplication.getContext()).getMessage(MyApplication.getContext(), Constants.TAG_GET_MESSAGE, map);
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetMessage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MESSAGE)) {
            MessageData msg = (MessageData) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    setErrorState(isRefreshed, isLoadmore, 500);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity)getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() == null) {
                        mTvNull.setVisibility(View.VISIBLE);
                    } else {
                        if (msg.getData().getDataList() != null && msg.getData().getDataList().size() > 0) {
                            mMsgList.addAll(msg.getData().getDataList());
                            mMsgRecyclerAdapter.notifyDataSetChanged();
                            hasNotReadView(mMsgList);
                            mTvNull.setVisibility(View.GONE);
                        } else {
                            if (isLoadmore) {
                                MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                                mTvNull.setVisibility(View.GONE);
                            }else{
                                mTvNull.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    break;
            }
            ((MainActivity) getActivity()).dismissLoading();
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mMsgList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.setLoadmoreEnable(true);
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onItemClick(int position) {
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            ((BaseActivity)getActivity()).openDialog();
        } else {
            mMsgList.get(position).setIsRead(1);
            hasNotReadView(mMsgList);
            Intent intent = new Intent(getActivity(), RegisterActivity.class);
            intent.putExtra(Constants.REGISTER_INTENT, Constants.H5_MSG_DETAIL_URL + mMsgList.get(position).getId());
            getActivity().startActivity(intent);
        }
    }

    private void hasNotReadView(List<MessageData.DataEntity.DataList> mMsgList) {
        boolean hasNotRead = false;
        for (int i = 0; i < mMsgList.size(); i++) {
            if (mMsgList.get(i).getIsRead() == 0) {
                hasNotRead = true;
                break;
            }
        }
        if (hasNotRead) {
            LogUtil.d("hasNotRead1=" + hasNotRead);
            ((MainActivity) getActivity()).setStateShow();
        } else {
            LogUtil.d("hasNotRead2=" + hasNotRead);
            ((MainActivity) getActivity()).setStateHide();
        }
    }

}