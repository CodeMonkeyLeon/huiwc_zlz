package com.hstypay.enterprise.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.FilterPledgePopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDatePopupWindow;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.pledge.PledgeBillFindActivity;
import com.hstypay.enterprise.activity.pledge.PledgeDetailActivity;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.PledgeBillRecyclerAdapter;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.bean.PledgeBillsBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class PledgeBillFragment extends BaseFragment implements View.OnClickListener {
    private RecyclerView mRvBill, mStoreRecyclerView, mCashierRecyclerView;
    private ImageView mIvFilter, mIvScan, mIvCashierArrow, mIvShopArrow;
    private LinearLayout mLlDate, mLlFilterTop, mLlShop, mLlCashier, mLlInput, mShopPopLayout, mCashierPopLayout;
    private TextView mTvDate, mTvShop, mTvCashier, mTvNull;
    private SelectDatePopupWindow mDatePopupWindow;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;

    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private PledgeBillRecyclerAdapter mBillRecyclerAdapter;
    private List<PledgeBillsBean.DataBean> billList;

    private String startTime;
    private String endTime;
    private String userId;
    private String storeMerchantId;
    private int[] apiProvider;
    private int[] tradeStatus;
    private List<Integer> tradeTypeList;
    private String otherType;
    private TimePickerView pvTime;
    private View mView;
    private OrderBroadcastReceiver mOrderBroadcastReceiver;
    private SafeDialog mLoadDialog;
    private boolean mShopSwitchArrow, mCashierSwitchArrow;
    private Animation rotate;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DataEntity> mCashierList;
    private List<DataEntity> mOriginCashierList;
    private EditTextDelete mEtStoreInput, mEtCashierInput;
    private ShopRecyclerAdapter mStoreAdapter;
    private CashierRecyclerAdapter mCashierAdapter;
    private String mShopType;
    private RadioGroup mRgType;
    private RadioButton mRbAllStore, mRbDirectStore, mRbJoinStore;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        registerBroadcast();
        mView = inflater.inflate(R.layout.fragment_pledge_bill, container, false);
        StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
        initView(mView);
        initEvent();
        initData();
        return mView;
    }

    public void initView(View view) {
        mLoadDialog = ((BaseActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mIvScan = view.findViewById(R.id.iv_scan);
        mLlDate = view.findViewById(R.id.ll_date);
        mTvDate = view.findViewById(R.id.tv_date);
        mLlShop = view.findViewById(R.id.ll_shop);
        mTvShop = view.findViewById(R.id.tv_shop);
        mLlCashier = view.findViewById(R.id.ll_cashier);
        mTvCashier = view.findViewById(R.id.tv_cashier);
        mIvCashierArrow = view.findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = view.findViewById(R.id.iv_shop_arrow);
        mIvFilter = view.findViewById(R.id.iv_filter);
        mLlFilterTop = view.findViewById(R.id.bill_filter_top);
        mLlInput = view.findViewById(R.id.ly_input);
        mTvNull = view.findViewById(R.id.tv_not_data);

        mRgType = view.findViewById(R.id.rg_type);
        mRbAllStore = view.findViewById(R.id.rb_all_store);
        mRbDirectStore = view.findViewById(R.id.rb_direct_store);
        mRbJoinStore = view.findViewById(R.id.rb_join_store);
        mShopPopLayout = view.findViewById(R.id.shop_pop_layout);
        mEtStoreInput = view.findViewById(R.id.et_store_input);
        mStoreRecyclerView = view.findViewById(R.id.recyclerView_store);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mStoreRecyclerView.setLayoutManager(storeLinearLayoutManager);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);

        mCashierPopLayout = view.findViewById(R.id.cashier_pop_layout);
        mEtCashierInput = view.findViewById(R.id.et_cashier_input);
        CustomLinearLayoutManager cashierLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mCashierRecyclerView = view.findViewById(R.id.recyclerView_cashier);
        mCashierRecyclerView.setLayoutManager(cashierLinearLayoutManager);
        mEtCashierInput.setClearImage(R.mipmap.ic_search_clear);

        initRecyclerView(view);
        initSwipeRefreshLayout(view);
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -3);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        //时间选择器
        pvTime = new TimePickerView.Builder(getActivity(), new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date);
                /*if (DateUtil.isToday(time)) {
                    mTvDate.setText("今日");
                } else {*/
                mTvDate.setText(time);
                //}
                startTime = time + " 00:00:00";
                endTime = time + " 23:59:59";
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    billList.clear();
                    Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, otherType, tradeTypeList);
                    currentPage = 2;
                    getGetBills(map, true);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("" , "" , "" , "" , "" , "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年" , "月" , "日" , null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    private void initRecyclerView(View view) {
        mRvBill = view.findViewById(R.id.bill_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRvBill.setLayoutManager(mLinearLayoutManager);
        mRvBill.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, otherType, tradeTypeList);
                    getGetBills(map, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    Map<String, Object> map = getRequestMap(pageSize + "" , currentPage + "" , startTime, endTime, userId,
                            storeMerchantId, apiProvider, tradeStatus, otherType, tradeTypeList);
                    getGetBills(map, false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    public void initEvent() {
        mLlDate.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlCashier.setOnClickListener(this);
        mIvScan.setOnClickListener(this);
        mIvFilter.setOnClickListener(this);
        mLlInput.setOnClickListener(this);
        mRbAllStore.setOnClickListener(this);
        mRbDirectStore.setOnClickListener(this);
        mRbJoinStore.setOnClickListener(this);
    }

    public void initData() {
        mShopType = "";
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mCashierList = new ArrayList<>();
        mOriginCashierList = new ArrayList<>();
        mTvDate.setText(DateUtil.formatYYMD(System.currentTimeMillis()));
        initTimePicker();
        billList = new ArrayList<>();
        mBillRecyclerAdapter = new PledgeBillRecyclerAdapter(MyApplication.getContext(), billList);
        mBillRecyclerAdapter.setOnItemClickListener(new PledgeBillRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
//                MtaUtils.mtaId(PledgeBillActivity.this, "C002");
                PledgeBillsBean.DataBean dataBean = billList.get(position);
                if (dataBean != null) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        Map<String, Object> map = new HashMap<>();
                        map.put("authNo" , dataBean.getAuthNo());
                        ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_BILL, map);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mRvBill.setAdapter(mBillRecyclerAdapter);
        initStore();
        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtCashierInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData(mEtCashierInput.getText().toString().trim());
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtCashierInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData("");
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
        tradeStatus = new int[]{};
        apiProvider = null;
        tradeTypeList = null;
        otherType = null;
        getBill();
    }

    private void getBill() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> requestMap = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                    apiProvider, tradeStatus, otherType, tradeTypeList);
            billList.clear();
            currentPage = 2;
            getGetBills(requestMap, true);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void initStore() {
        if (MyApplication.getIsAdmin() || MyApplication.getIsMerchant() /*|| MyApplication.getIsManager()*/) {
            storeMerchantId = SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_ID, "");
            if (TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpStayUtil.getString(MyApplication.getContext(), MyApplication.getUserId() + Constants.SP_ADMIN_CHECK_STORE_NAME, ""));
            }
            userId = "";
        } else if (MyApplication.getIsCasher()) {
            storeMerchantId = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            mIvCashierArrow.setVisibility(View.GONE);
            mTvCashier.setText(MyApplication.getRealName());
            mLlCashier.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            mLlShop.setEnabled(false);
            userId = MyApplication.getUserId();
        }
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            if ("12".equals(MyApplication.getMerchantType())) {
                //TODO 押金收款暂不修改
//                mRgType.setVisibility(View.VISIBLE);
                mRgType.setVisibility(View.GONE);
            }
            mShopList.addAll(mOriginShopList);
        } else {
            mRgType.setVisibility(View.GONE);
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    private void setCashierData(String search) {
        mCashierList.clear();
        if (TextUtils.isEmpty(search)) {
            mCashierList.addAll(mOriginCashierList);
        } else {
            for (int i = 0; i < mOriginCashierList.size(); i++) {
                if (mOriginCashierList.get(i).getRealName().contains(search)) {
                    mCashierList.add(mOriginCashierList.get(i));
                }
            }
        }
    }

    private Map<String, Object> getRequestMap(String pageSize, String currentPage, String startTime, String endTime,
                                              String userId, String storeMerchantId, int[] apiProvider, int[] tradeStatus, String otherType, List<Integer> tradeTypeList) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageSize" , pageSize);
        map.put("currentPage" , currentPage);
        map.put("startTime" , startTime);
        map.put("endTime" , endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId" , userId);
        if (!TextUtils.isEmpty(mShopType))
            map.put("merchantType" , mShopType);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId" , storeMerchantId);
        if (apiProvider != null && apiProvider.length > 0)
            map.put("apiProviderList" , apiProvider);
        if (tradeStatus != null && tradeStatus.length > 0)
            map.put("tradeStatusList" , tradeStatus);
        if (tradeTypeList != null && tradeTypeList.size() > 0)
            map.put("tradeTypeList" , tradeTypeList);
        if (!TextUtils.isEmpty(otherType)) {
            map.put("otherType" , otherType);
        }
        return map;
    }

    public void getGetBills(Map<String, Object> map, boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            ServerClient.newInstance(MyApplication.getContext()).getPledgeBills(MyApplication.getContext(), Constants.TAG_PLEDGE_BILLS, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_BILLS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        billList.addAll(msg.getData());
                        mBillRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        mBillRecyclerAdapter.notifyDataSetChanged();
                        if (isLoadmore) {
                            mTvNull.setVisibility(View.GONE);
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        } else {
                            mTvNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_BILL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeBillsBean msg = (PledgeBillsBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        Intent intent = new Intent(getActivity(), PledgeDetailActivity.class);
                        intent.putExtra(Constants.INTENT_PLEDGE_PAY_BEAN, msg.getData().get(0));
                        startActivity(intent);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_BILL_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        if (mRbAllStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_shop));
                            dataEntity.setStoreId("");
                            mOriginShopList.add(dataEntity);
                            mOriginShopList.addAll(msg.getData());
                            mShopList.addAll(mOriginShopList);
                        } else if (mRbDirectStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_direct_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        } else if (mRbJoinStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_join_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        }
                        mStoreAdapter = new ShopRecyclerAdapter(getActivity(), mShopList, storeMerchantId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                if (mRgType.getVisibility() == View.VISIBLE) {
                                    switch (mRgType.getCheckedRadioButtonId()) {
                                        case R.id.rb_all_store:
                                            mShopType = "";
                                            break;
                                        case R.id.rb_direct_store:
                                            mShopType = "21";
                                            break;
                                        case R.id.rb_join_store:
                                            mShopType = "22";
                                            break;
                                    }
                                } else {
                                    mShopType = "";
                                }
                                storeMerchantId = mShopList.get(position).getStoreId();
                                userId = "";
                                mTvShop.setText(mShopList.get(position).getStoreName());
                                mTvCashier.setText(getString(R.string.tv_all_user));
                                closeArrow(1);
                                getBill();
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);

                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_BILL_CASHIER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        DataEntity dataEntity = new DataEntity();
                        dataEntity.setRealName(getString(R.string.tv_all_user));
                        dataEntity.setUserId("");
                        mOriginCashierList.add(dataEntity);
                        mOriginCashierList.addAll(msg.getData());
                        mCashierList.addAll(mOriginCashierList);
                        mCashierAdapter = new CashierRecyclerAdapter(getActivity(), mCashierList);
                        mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                userId = mCashierList.get(position).getUserId();
                                mTvCashier.setText(mCashierList.get(position).getRealName());
                                closeArrow(2);
                                getBill();
                            }
                        });
                        mCashierRecyclerView.setAdapter(mCashierAdapter);
                        mCashierAdapter.setSelected(userId);
                        openArrow(2);
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            billList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_date://选择日期
                pvTime.show();
                break;
            case R.id.ll_shop:
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mShopSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_cashier:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                } else {
                    loadCashierData();
                }
                break;
            case R.id.iv_filter://选择筛选条件
                showFilterPop();
                break;
            case R.id.iv_scan://扫一扫
                Intent intent = new Intent(getActivity(), CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_SCAN_PLEDGE);
                startActivity(intent);
                break;
            case R.id.ly_input://搜索
                startActivity(new Intent(getActivity(), PledgeBillFindActivity.class));
                break;
            case R.id.rb_all_store:
                loadShopData("");
                break;
            case R.id.rb_direct_store:
                loadShopData("21");
                break;
            case R.id.rb_join_store:
                loadShopData("22");
                break;
            default:
                break;
        }
    }

    private void loadShopData(String shopType) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (mRbAllStore.isChecked())
                mOriginShopList.clear();
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize" , "10000");
            map.put("currentPage" , "1");
            if (!TextUtils.isEmpty(shopType))
                map.put("merchantType" , shopType);
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_PLEDGE_BILL_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadCashierData() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginCashierList.clear();
            mCashierList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize" , "10000");
            map.put("currentPage" , "1");
            if (!TextUtils.isEmpty(mShopType))
                map.put("merchantType" , mShopType);
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId" , storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), Constants.TAG_PLEDGE_BILL_CASHIER, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mShopSwitchArrow = false;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mEtCashierInput.setText("");
                mCashierSwitchArrow = false;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                /*if ("12".equals(MyApplication.getMerchantType())) {
                    //TODO 押金收款暂不修改
//                    mRgType.setVisibility(View.VISIBLE);
                    mRgType.setVisibility(View.GONE);
                } else {*/
                    mRgType.setVisibility(View.GONE);
//                }
                mShopSwitchArrow = true;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.VISIBLE);
                loadShopData(mShopType);
                break;
            case 2:
                mCashierSwitchArrow = true;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void showFilterPop() {
        FilterPledgePopupWindow popupWindow = new FilterPledgePopupWindow(getActivity(), apiProvider, tradeStatus, new FilterPledgePopupWindow.HandleBtn() {
            @Override
            public void handleOkBtn(int[] apiProvider, int[] tradeStatus) {
                PledgeBillFragment.this.apiProvider = apiProvider;
                PledgeBillFragment.this.tradeStatus = tradeStatus;
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    billList.clear();
                    Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                            apiProvider, tradeStatus, otherType, tradeTypeList);
                    currentPage = 2;
                    getGetBills(map, true);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        });
        popupWindow.showAsDropDown(mLlFilterTop);
    }

    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_PLEDGE_DATA);
        intentFilter.addAction(Constants.ACTION_PLEDGE_UNFREEZE_DATA);
        mOrderBroadcastReceiver = new OrderBroadcastReceiver();
        getActivity().registerReceiver(mOrderBroadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIvShopArrow.clearAnimation();
        mIvCashierArrow.clearAnimation();
        getActivity().unregisterReceiver(mOrderBroadcastReceiver);
    }

    class OrderBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.ACTION_PLEDGE_DATA.equals(action)) {
                mHandler.sendEmptyMessageDelayed(1, 1000);
            } else if (Constants.ACTION_PLEDGE_UNFREEZE_DATA.equals(action)) {
                mHandler.sendEmptyMessage(1);
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        isRefreshed = true;
                        Map<String, Object> map = getRequestMap("15" , "1" , startTime, endTime, userId, storeMerchantId,
                                apiProvider, tradeStatus, otherType, tradeTypeList);
                        getGetBills(map, false);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    break;
            }
        }
    };

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getBill();
        }
    }
}
