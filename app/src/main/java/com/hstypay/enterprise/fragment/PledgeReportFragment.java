package com.hstypay.enterprise.fragment;

import static android.content.Context.BIND_AUTO_CREATE;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.DateStartEndPopupWindow;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.BluePrintUtil;
import com.hstypay.enterprise.utils.print.CloudPrintUtil;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintPledgeSum;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class PledgeReportFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout mLlData, mLlShop, mLlSalesman, mShopPopLayout, mCashierPopLayout;
    private TextView mTvPledgeMoney, mTvPledgeCount, mTvPledgeFreezeMoney, mTvPledgeFreezeCount, mTvPledgePayMoney, mTvPledgePayCount, mTvPledgeNoFreezeMoney;
    private ImageView mIvCashierArrow, mIvShopArrow;
    private EditTextDelete mEtStoreInput, mEtCashierInput;
    private RecyclerView mStoreRecyclerView, mCashierRecyclerView;
    private RadioGroup mRgType;
    private RadioButton mRbAllStore, mRbDirectStore, mRbJoinStore;
    private ShopRecyclerAdapter mStoreAdapter;
    private CashierRecyclerAdapter mCashierAdapter;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DataEntity> mCashierList;
    private List<DataEntity> mOriginCashierList;
    private boolean mShopSwitchArrow, mCashierSwitchArrow;
    private Animation rotate;
    private String startTime;
    private String endTime;
    private String userId = "";
    private String storeMerchantId = "";
    private String mShopType = "";
    private List<ReportBean.DataEntity.ListEntity> mList;
    private String storeName;
    private String cashierName;
    private PosPrintUtil mPosPrintUtil;
    private Printer printer;
    private TextView tv_date;
    private Button mBtnPrint;
    private TextView mTvShop, mTvCashier, mTvNull;
    private ScrollView mSvReport;
    private PledgeReportBean.DataBean mInfo;
    private View mView;
    private IWoyouService woyouService;
    private ReportBroadcastReceiver mReportBroadcastReceiver;
    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;
    private SafeDialog mLoadDialog;

    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(getActivity(), intent));
        boolean flag = false;
        flag = getActivity().bindService(eintent, conn, BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        getActivity().bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    /**
     * 绑定商米打印服务
     */
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        getActivity().startService(intent);
        getActivity().bindService(intent, connService, BIND_AUTO_CREATE);
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_pledge_report, container, false);
        StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
        initView(mView);
        initEvent();
        initData();
        initPosPrint();
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindDeviceService();
        } else */
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_REPORT_DATA);
        mReportBroadcastReceiver = new ReportBroadcastReceiver();
        getActivity().registerReceiver(mReportBroadcastReceiver, intentFilter);
    }

    protected void bindPayServer() {
        Intent intent = new Intent(((BaseActivity) getActivity()).PAY_SERVICE_ACTION);
        intent.setPackage(((BaseActivity) getActivity()).SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        getActivity().bindService(intent, payConnection, BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        /*if ("liandi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            unbindDeviceService();
        }*/
        getActivity().unregisterReceiver(mReportBroadcastReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyPosPrint();
    }

    private void destroyPosPrint() {
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unregisterReceiver(mPrtReceiver);
        }
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil((BaseActivity) getActivity(), mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(getActivity(), PrintPledgeSum.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            getActivity().registerReceiver(mPrtReceiver, filter);
        }
    }

    public void initView(View view) {
        mLoadDialog = ((BaseActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mLlData = view.findViewById(R.id.ll_date);
        mLlShop = view.findViewById(R.id.ll_shop);
        mTvShop = view.findViewById(R.id.tv_shop);
        mTvCashier = view.findViewById(R.id.tv_cashier);
        mLlSalesman = view.findViewById(R.id.ll_cashier);
        tv_date = view.findViewById(R.id.tv_date);
        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
        setDateView(startTime, endTime);

        mTvPledgeMoney = view.findViewById(R.id.tv_pledge_money);//预授权金额
        mTvPledgeCount = view.findViewById(R.id.tv_pledge_count);//预授权笔数
        mTvPledgeFreezeMoney = view.findViewById(R.id.tv_pledge_freeze_money);//累计解冻金额
        mTvPledgeFreezeCount = view.findViewById(R.id.tv_pledge_freeze_count);//累计解冻笔数
        mTvPledgePayMoney = view.findViewById(R.id.tv_pledge_pay_money);//累计转支付金额
        mTvPledgePayCount = view.findViewById(R.id.tv_pledge_pay_count);//累计转支付笔数
        mTvPledgeNoFreezeMoney = view.findViewById(R.id.tv_pledge_no_freeze_money);//未解冻金额

        mBtnPrint = view.findViewById(R.id.btn_print);
        mSvReport = view.findViewById(R.id.sv_report);
        mTvNull = view.findViewById(R.id.tv_null);
        mSvReport.setVisibility(View.GONE);
        mTvNull.setVisibility(View.GONE);
        mIvCashierArrow = view.findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = view.findViewById(R.id.iv_shop_arrow);

        mRgType = view.findViewById(R.id.rg_type);
        mRbAllStore = view.findViewById(R.id.rb_all_store);
        mRbDirectStore = view.findViewById(R.id.rb_direct_store);
        mRbJoinStore = view.findViewById(R.id.rb_join_store);
        mShopPopLayout = view.findViewById(R.id.shop_pop_layout);
        mEtStoreInput = view.findViewById(R.id.et_store_input);
        mStoreRecyclerView = view.findViewById(R.id.recyclerView_store);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mStoreRecyclerView.setLayoutManager(storeLinearLayoutManager);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);

        mCashierPopLayout = view.findViewById(R.id.cashier_pop_layout);
        mEtCashierInput = view.findViewById(R.id.et_cashier_input);
        CustomLinearLayoutManager cashierLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mCashierRecyclerView = view.findViewById(R.id.recyclerView_cashier);
        mCashierRecyclerView.setLayoutManager(cashierLinearLayoutManager);
        mEtCashierInput.setClearImage(R.mipmap.ic_search_clear);

        ((BaseActivity) getActivity()).setButtonWhite(mBtnPrint);

        if (MyApplication.getIsCasher()) {
            mIvCashierArrow.setVisibility(View.GONE);
            mTvCashier.setText(MyApplication.getRealName());
            userId = MyApplication.getUserId();
            mLlSalesman.setEnabled(false);
            mIvShopArrow.setVisibility(View.GONE);
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            storeMerchantId = MyApplication.getDefaultStore();
            mLlShop.setEnabled(false);
        } else {
            userId = "";
        }
    }

    private void setDateView(String startTime, String endTime) {
        try {
            if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formartDateToMMDD(endTime))) {
                tv_date.setText(DateUtil.formartDateToMMDD(startTime));
            } else {
                tv_date.setText(DateUtil.formartDateToMMDD(startTime) + " " + getString(R.string.tv_least) + " "
                        + DateUtil.formartDateToMMDD(endTime));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void initEvent() {
        mLlData.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlSalesman.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mRbAllStore.setOnClickListener(this);
        mRbDirectStore.setOnClickListener(this);
        mRbJoinStore.setOnClickListener(this);

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtCashierInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData(mEtCashierInput.getText().toString().trim());
                        mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtCashierInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData("");
                        mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
    }

    public void initData() {
        storeName = "全部门店";
        cashierName = UIUtils.getString(R.string.tv_all_user);
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mCashierList = new ArrayList<>();
        mOriginCashierList = new ArrayList<>();
        mList = new ArrayList<>();
        getReport(getRequestMap(startTime, endTime, userId, storeMerchantId), true);
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            if ("12".equals(MyApplication.getMerchantType())) {
                //TODO 押金收款暂不修改
//                mRgType.setVisibility(View.VISIBLE);
                mRgType.setVisibility(View.GONE);
            }
            mShopList.addAll(mOriginShopList);
        } else {
            mRgType.setVisibility(View.GONE);
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    private void setCashierData(String search) {
        mCashierList.clear();
        if (TextUtils.isEmpty(search)) {
            mCashierList.addAll(mOriginCashierList);
        } else {
            for (int i = 0; i < mOriginCashierList.size(); i++) {
                if (mOriginCashierList.get(i).getRealName().contains(search)) {
                    mCashierList.add(mOriginCashierList.get(i));
                }
            }
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_PLEDGE_REPORT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PledgeReportBean msg = (PledgeReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_REPORT_TRUE://请求成功
                    if (MyApplication.getIsCasher()) {
                        sendTodayDataBroadcast(getActivity());
                    }
                    if (msg.getData() != null) {
                        mInfo = msg.getData();
                        mInfo.setCashierName(cashierName);
                        mInfo.setStoreName(storeName);
                        mTvNull.setVisibility(View.GONE);
                        mSvReport.setVisibility(View.VISIBLE);
                        setView(msg.getData());
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSvReport.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_COLLECT_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        if (mRbAllStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_shop));
                            dataEntity.setStoreId("");
                            mOriginShopList.add(dataEntity);
                            mOriginShopList.addAll(msg.getData());
                            mShopList.addAll(mOriginShopList);
                        } else if (mRbDirectStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_direct_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        } else if (mRbJoinStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_join_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        }
                        mStoreAdapter = new ShopRecyclerAdapter(getActivity(), mShopList, storeMerchantId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                if (mRgType.getVisibility() == View.VISIBLE) {
                                    switch (mRgType.getCheckedRadioButtonId()) {
                                        case R.id.rb_all_store:
                                            mShopType = "";
                                            break;
                                        case R.id.rb_direct_store:
                                            mShopType = "21";
                                            break;
                                        case R.id.rb_join_store:
                                            mShopType = "22";
                                            break;
                                    }
                                } else {
                                    mShopType = "";
                                }
                                storeMerchantId = mShopList.get(position).getStoreId();
                                userId = "";
                                mTvShop.setText(mShopList.get(position).getStoreName());
                                mTvCashier.setText(getString(R.string.tv_all_user));
                                closeArrow(1);
                                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId), true);
                                } else {
                                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                                }
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);

                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PLEDGE_COLLECT_CASHIER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        DataEntity dataEntity = new DataEntity();
                        dataEntity.setRealName(getString(R.string.tv_all_user));
                        dataEntity.setUserId("");
                        mOriginCashierList.add(dataEntity);
                        mOriginCashierList.addAll(msg.getData());
                        mCashierList.addAll(mOriginCashierList);
                        mCashierAdapter = new CashierRecyclerAdapter(getActivity(), mCashierList);
                        mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                userId = mCashierList.get(position).getUserId();
                                mTvCashier.setText(mCashierList.get(position).getRealName());
                                closeArrow(2);
                                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId), true);
                                } else {
                                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                                }
                            }
                        });
                        mCashierRecyclerView.setAdapter(mCashierAdapter);
                        mCashierAdapter.setSelected(userId);
                        openArrow(2);
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
    }

    private void showPopupDateWindow() {
        DateStartEndPopupWindow moneyPopupWindow = new DateStartEndPopupWindow(getActivity(), startTime, endTime, new DateStartEndPopupWindow.HandleTv() {
            @Override
            public void getDate(String startTime, String endTime) {
                PledgeReportFragment.this.startTime = startTime;
                PledgeReportFragment.this.endTime = endTime;
                setDateView(startTime, endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId), true);
            }
        });
        moneyPopupWindow.showAtLocation(mBtnPrint, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(mShopType))
            map.put("merchantType", mShopType);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }

    public void getReport(Map<String, Object> map, boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            if (showDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            ServerClient.newInstance(MyApplication.getContext()).getPledgeReport(MyApplication.getContext(), Constants.TAG_PLEDGE_REPORT, map);
        } else {
            mTvNull.setText("网络异常");
            mTvNull.setVisibility(View.VISIBLE);
            mSvReport.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_date:
                MtaUtils.mtaId(getActivity(), "1016");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {
                    showPopupDateWindow();
                }
                break;
            case R.id.ll_shop:
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mShopSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_cashier:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                } else {
                    loadCashierData();
                }
                break;
            case R.id.btn_print:
                MtaUtils.mtaId(getActivity(), "1019");
                mInfo.setStoreMerchantId(storeMerchantId);
                bluePrint();
                break;
            case R.id.rb_all_store:
                loadShopData("");
                break;
            case R.id.rb_direct_store:
                loadShopData("21");
                break;
            case R.id.rb_join_store:
                loadShopData("22");
                break;
            default:
                break;
        }
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mShopSwitchArrow = false;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mEtCashierInput.setText("");
                mCashierSwitchArrow = false;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                /*if ("12".equals(MyApplication.getMerchantType())) {
                    //TODO 押金收款暂不修改
//                    mRgType.setVisibility(View.VISIBLE);
                    mRgType.setVisibility(View.GONE);
                } else {*/
                    mRgType.setVisibility(View.GONE);
//                }
                mShopSwitchArrow = true;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.VISIBLE);
                loadShopData(mShopType);
                break;
            case 2:
                mCashierSwitchArrow = true;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void loadShopData(String shopType) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (mRbAllStore.isChecked())
                mOriginShopList.clear();
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(shopType))
                map.put("merchantType", shopType);
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_PLEDGE_COLLECT_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadCashierData() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginCashierList.clear();
            mCashierList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(mShopType))
                map.put("merchantType", mShopType);
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), Constants.TAG_PLEDGE_COLLECT_CASHIER, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void bluetoothPrint(PledgeReportBean.DataBean info) {
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            ((BaseActivity) getActivity()).showPrintDialog(1);
            return;
        }
        new Thread() {
            @Override
            public void run() {
                super.run();
                if (MyApplication.bluetoothSocket != null && MyApplication.bluetoothSocket.isConnected()) {
                    BluePrintUtil.printPledgeReportSum(info);
                } else {
                    if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
                        boolean isSucc = BluePrintUtil.blueConnent(1, getActivity());
                        if (isSucc) {
                            BluePrintUtil.printPledgeReportSum(info);
                        } else {
                            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((BaseActivity) getActivity()).showPrintDialog(1);
                                    }
                                });

                            } else {
                                //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                                LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                            }
                        }
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((BaseActivity) getActivity()).showPrintDialog(1);
                            }
                        });

                    }
                }
            }
        }.start();

    }

    public void showCommonNoticeDialog(Activity activity, String content) {
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, content, "");
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void bluePrint() {
        if (mInfo != null) {
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
            if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                mInfo.setStartTime(startTime);
                long now = System.currentTimeMillis();
                if (now < DateUtil.getTime(endTime,"yyyy-MM-dd HH:mm:ss")) {
                    mInfo.setEndTime(DateUtil.formatTime(now, "yyyy-MM-dd HH:mm:ss"));
                } else {
                    mInfo.setEndTime(endTime);
                }
            } else {
                mInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                mInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }
            mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
            mInfo.setCashierName(mTvCashier.getText().toString().trim());
            mInfo.setStoreName(mTvShop.getText().toString().trim());
            LogUtil.d("date===" + mInfo.getStartTime() + "//////" + mInfo.getEndTime() + "////" + mInfo.getPrintTime());
            if (AppHelper.getAppType() == 1) {
                mPosPrintUtil.printPledgeReport(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mInfo);
            } else if (AppHelper.getAppType() == 2) {
//                bluetoothPrint(mInfo);
                CloudPrintUtil.cloudPrint((BaseActivity) getActivity(), new CloudPrintUtil.OnGetCloudDevicesListCallBack() {
                    @Override
                    public void startCloudPrint(List<DeviceBean> cloudDevices) {
                        CloudPrintUtil.startCloudPrint((BaseActivity)getActivity(),cloudDevices,mInfo);
                    }

                    @Override
                    public void startBluetoothPrint() {
                        bluetoothPrint(mInfo);
                    }
                });
            }
        } else {
            showCommonNoticeDialog(getActivity(), getString(R.string.print_data_error));
        }
    }

    public void setView(PledgeReportBean.DataBean dataEntity) {
        if (dataEntity != null) {
            mTvPledgeMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumPreMoney() / 100d));
            mTvPledgeCount.setText(dataEntity.getCntPreMoney() + "");
            mTvPledgeFreezeMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumFreeMoney() / 100d));
            mTvPledgeFreezeCount.setText(dataEntity.getCntFreeMoney() + "");
            mTvPledgePayMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumPayMoney() / 100d));
            mTvPledgePayCount.setText(dataEntity.getCntPayMoney() + "");
            //mTvPledgeNoFreezeMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSumNotFreeMoney() / 100d));
        }
    }

    class ReportBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId), false);
        }
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId), true);
        }
    }

}
