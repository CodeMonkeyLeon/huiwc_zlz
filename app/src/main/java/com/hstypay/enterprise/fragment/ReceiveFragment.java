package com.hstypay.enterprise.fragment;

import static android.app.Activity.RESULT_OK;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.NoticeDialog;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SingleLineZoomTextView;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.PayDetailActivity;
import com.hstypay.enterprise.activity.QrCodeActivity;
import com.hstypay.enterprise.activity.RemarkActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.activity.storeCode.StaticCodeImageActivity;
import com.hstypay.enterprise.activity.vanke.OrderCouponInfoActivity;
import com.hstypay.enterprise.activity.vanke.OrderPayInfoActivity;
import com.hstypay.enterprise.adapter.HuabeiRateAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.EasypayInfo;
import com.hstypay.enterprise.bean.HbfqRateBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.MerChantModifyBean;
import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.PosAccountBean;
import com.hstypay.enterprise.bean.PosOrderBean;
import com.hstypay.enterprise.bean.StoreBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.VoiceBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.bean.YinshangPosBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.bean.vanke.CouponListBean;
import com.hstypay.enterprise.bean.vanke.CouponTypeListBean;
import com.hstypay.enterprise.bean.vanke.VipInfoBean;
import com.hstypay.enterprise.bean.vanke.VipInfoData;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.ShapeSelectorUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.ToastUtil;
import com.hstypay.enterprise.utils.UIUtils;
import com.igexin.sdk.PushManager;
import com.ums.anypay.service.IOnTransEndListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class ReceiveFragment extends BaseFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private View mViewLine, mViewCursor;
    private ImageView ivBack, mIvStoreName, mIvVankeVip, mIvVankeCoupon;
    private LinearLayout num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, lay_point, lay_add, lay_clear, lay_delete, scan_pay;
    private TextView tv_money, mTvRemark, mTvKeyPay/*, mTvSixMoney, mTvSixRate, mTvTwelveMoney, mTvTwelveRate*/;
    private SingleLineZoomTextView tv_calculate;
    private Context context;
    private RecyclerView mRecyclerViewRate;
    private ScrollView mSvHbfq, mSvVankeCoupon;

    private LinearLayout tv_choice_store, mLlHbfqDetail, mLlKeyboardView, mLlMoney;
    private RelativeLayout mRlUnion, mRlHuabei, mRlView, mRlPayType, mRlReceiveQrcode, mRlReceiveBcard, mRlReceiveScan, mRlVankeVip, mRlVankeCoupon;
    private TextView mTv_store_name;
    private String store_Id = "";

    private StringBuilder calculateBuilder;
    private double totalMoney;
    private Button mBtnPay;
    private String mPayRemark = "";
    private RadioButton mCbHbfq, mCbUnion;
    private View mViewUnion, mViewHbfq;
    private boolean enable;
    private String mHbFqNum = "";
    private boolean isRateFree;
    private List<HbfqRateBean.DataBean> mRateData;
    private SelectDialog mSelectDialog;
    private SelectDialog mSyncDialog;
    private HuabeiRateAdapter mAdapter;
    private long lastInputTimeMillis;
    private boolean firstRequest;
    private View mView;
    private SafeDialog mLoadDialog, mSyncLoadDialog;
    private PosAccountBean.DataEntity mPosAccountBean;
    private PosOrderBean.DataEntity posOrderBean;
    private Intent mResultData;
    private EditText mEtVankeVip;
    private CheckBox mCbMaoku, mCbMeituan;
    private List<CouponInfoData> mCouponList;
    private ObjectAnimator objectAnimator;
    private VipInfoData mVipInfo;
    private Handler mHandler;
    private YinshangPosBean mYinshangPosBean;
    private boolean cancelTask = false;
    private long timeCount = 5;
    private int count = 1;
    private String dialogMessage = null;
    private NoticeDialog showDialog;
    private String mInterProviderType = "-1";//券提供方类型
    private ResetBroadcastReceiver mResetBroadcastReceiver;
    private String mMallCouponType;
    private SelectDialog mGetCardPayResultDialog;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        ((MainActivity) getActivity()).dismissLoading();
        mView = inflater.inflate(R.layout.fragment_receive, container, false);
        StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
        context = MyApplication.getContext();
        initView(mView);
        initListener();
        initData();
        return mView;
    }

    private void initListener() {
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.btn_remark:
                        Intent intent = new Intent(getActivity(), RemarkActivity.class);
                        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
                        startActivityForResult(intent, Constants.REQUEST_PAY_REMARK);
                        break;
                    //支付
                    case R.id.ly_to_scan_pay:
                    case R.id.btn_confirm:
                        MtaUtils.mtaId(MyApplication.getContext(), "B002");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            if (MyApplication.isStoreNull()) {
                                MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                            } else {
                                AppHelper.execVibrator(context);
                                if (AppHelper.getApkType() == 1) {
                                    toCaptureActivity();
                                } else {
                                    if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) ||
                                            Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                                        if (TextUtils.isEmpty(mEtVankeVip.getText().toString().trim()) /*&& TextUtils.isEmpty(mEtVankeCoupon.getText().toString().trim())*/) {
                                            if (mRlVankeCoupon.getVisibility() == View.VISIBLE && hasChecked(mCbMaoku, mCbMeituan)) {
                                                toOrderCouponInfo();
                                            } else {
                                                toOrderInfoActivity();
                                            }
                                        } else {
                                            getUserInfo(mEtVankeVip.getText().toString().trim());
                                        }
                                    } else {
                                        setPayType(View.VISIBLE);
                                    }
                                }
                            }
                        }
                        break;
                    //选择门店
                    case R.id.tv_choice_store:
                        MtaUtils.mtaId(MyApplication.getContext(), "B003");
                        Intent intentShop = new Intent(getActivity(), ShopActivity.class);
                        intentShop.putExtra(Constants.INTENT_STORE_ID, store_Id);
                        intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                        startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                        break;
                    case R.id.view_union:
                        mCbUnion.setChecked(true);
                        mCbHbfq.setChecked(false);
                        mLlHbfqDetail.setVisibility(View.GONE);
                        break;
                    case R.id.view_hbfq:
                        if (!enable) {
                            mCbHbfq.setChecked(false);
                            mCbUnion.setChecked(true);
                            MyToast.showToastShort(getString(R.string.et_hit_code_money));
                        } else {
                            if (!mCbHbfq.isChecked()) {
                                mCbUnion.setChecked(false);
                                mCbHbfq.setChecked(true);
                                getHbfqRateList(totalMoney);
                                if (!firstRequest && mLlKeyboardView.getVisibility() == View.VISIBLE) {
                                    setKeyboardView(false);
                                }
                                firstRequest = true;
                            }
                        }
                        break;
                    case R.id.ll_money:
                        mEtVankeVip.clearFocus();
//                mEtVankeCoupon.clearFocus();
                        setKeyboardView(mLlKeyboardView.getVisibility() == View.GONE);
                        break;
                    case R.id.rl_receive_qrcode:
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            if (MyApplication.isStoreNull()) {
                                MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                            } else {
                                AppHelper.execVibrator(context);
                                toQrcodeActivity();
                            }
                        }
                        break;
                    case R.id.rl_receive_scan:
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            if (MyApplication.isStoreNull()) {
                                MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                            } else {
                                AppHelper.execVibrator(context);
                                toCaptureActivity();
                            }
                        }
                        break;
                    case R.id.rl_receive_bcard:
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            if (MyApplication.isStoreNull()) {
                                MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                            } else {
                                AppHelper.execVibrator(context);
                                if (totalMoney <= 0) {
                                    MyToast.showToastShort(getString(R.string.please_input_amount));
                                } else {
                                    String money = String.valueOf(BigDecimal.valueOf(totalMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
                                    bCardPayment(money);
                                }
                            }
                        }
                        break;
                    case R.id.rl_pay_type:
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            setPayType(View.GONE);
                        }
                        break;
                    case R.id.iv_vanke_vip:
                        capture(Constants.INTENT_VANKE_VIP);
                        break;
                    case R.id.iv_vanke_coupon:
                        capture(Constants.INTENT_VANKE_COUPON);
                        break;
                }
            }
        };
        ivBack.setOnClickListener(onSingleClickListener);
        num_0.setOnClickListener(this);
        num_1.setOnClickListener(this);
        num_2.setOnClickListener(this);
        num_3.setOnClickListener(this);
        num_4.setOnClickListener(this);
        num_5.setOnClickListener(this);
        num_6.setOnClickListener(this);
        num_7.setOnClickListener(this);
        num_8.setOnClickListener(this);
        num_9.setOnClickListener(this);
        lay_point.setOnClickListener(this);
        lay_add.setOnClickListener(this);
        lay_clear.setOnClickListener(this);
        lay_delete.setOnClickListener(this);
        scan_pay.setOnClickListener(onSingleClickListener);
        if (!MyApplication.getIsCasher()) {
            tv_choice_store.setOnClickListener(onSingleClickListener);
            mIvStoreName.setVisibility(View.VISIBLE);
        } else {
            mIvStoreName.setVisibility(View.INVISIBLE);
        }
        mTvRemark.setOnClickListener(onSingleClickListener);
        mViewUnion.setOnClickListener(onSingleClickListener);
        mViewHbfq.setOnClickListener(onSingleClickListener);
        mLlMoney.setOnClickListener(onSingleClickListener);
        mBtnPay.setOnClickListener(onSingleClickListener);
        mRlReceiveQrcode.setOnClickListener(onSingleClickListener);
        mRlReceiveScan.setOnClickListener(onSingleClickListener);
        mRlReceiveBcard.setOnClickListener(onSingleClickListener);
        mRlPayType.setOnClickListener(onSingleClickListener);
        mIvVankeVip.setOnClickListener(onSingleClickListener);
        mIvVankeCoupon.setOnClickListener(onSingleClickListener);
        mCbMeituan.setOnCheckedChangeListener(this);
        mCbMaoku.setOnCheckedChangeListener(this);
        mEtVankeVip.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setKeyboardView(false);
                }
            }
        });
        /*mEtVankeCoupon.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setKeyboardView(false);
                }
            }
        });*/
        registerBroadcast();
    }

    private void initView(View view) {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mSyncLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_order_sync), false);
        ivBack = (ImageView) view.findViewById(R.id.iv_back);
        num_0 = (LinearLayout) view.findViewById(R.id.num_0);
        num_1 = (LinearLayout) view.findViewById(R.id.num_1);
        num_2 = (LinearLayout) view.findViewById(R.id.num_2);
        num_3 = (LinearLayout) view.findViewById(R.id.num_3);
        num_4 = (LinearLayout) view.findViewById(R.id.num_4);
        num_5 = (LinearLayout) view.findViewById(R.id.num_5);
        num_6 = (LinearLayout) view.findViewById(R.id.num_6);
        num_7 = (LinearLayout) view.findViewById(R.id.num_7);
        num_8 = (LinearLayout) view.findViewById(R.id.num_8);
        num_9 = (LinearLayout) view.findViewById(R.id.num_9);
        lay_point = (LinearLayout) view.findViewById(R.id.lay_point);
        lay_add = (LinearLayout) view.findViewById(R.id.lay_add);
        lay_clear = (LinearLayout) view.findViewById(R.id.lay_clear);
        lay_delete = (LinearLayout) view.findViewById(R.id.lay_delete);
        scan_pay = (LinearLayout) view.findViewById(R.id.ly_to_scan_pay);
        tv_money = (TextView) view.findViewById(R.id.tv_money);
        tv_calculate = (SingleLineZoomTextView) view.findViewById(R.id.tv_calculate);

        tv_choice_store = (LinearLayout) view.findViewById(R.id.tv_choice_store);
        mTv_store_name = (TextView) view.findViewById(R.id.tv_store_name);
        mIvStoreName = (ImageView) view.findViewById(R.id.iv_store_name);
        mTvRemark = view.findViewById(R.id.btn_remark);

        mSvHbfq = view.findViewById(R.id.sv_hbfq);
        mRlUnion = view.findViewById(R.id.rl_union);
        mRlHuabei = view.findViewById(R.id.rl_huabei);
        mViewLine = view.findViewById(R.id.view_line_huabei);
        mLlHbfqDetail = (LinearLayout) view.findViewById(R.id.ll_hbfq_detail);
        mCbHbfq = view.findViewById(R.id.cb_hbfq);
        mCbUnion = view.findViewById(R.id.cb_union);
        mViewUnion = view.findViewById(R.id.view_union);
        mViewHbfq = view.findViewById(R.id.view_hbfq);
        mLlMoney = view.findViewById(R.id.ll_money);
        mRlView = view.findViewById(R.id.rl_view);
        mBtnPay = view.findViewById(R.id.btn_confirm);
        mLlKeyboardView = view.findViewById(R.id.keyboard_view);
        mRecyclerViewRate = view.findViewById(R.id.recyclerView_huabei_rate);

        mRlReceiveQrcode = view.findViewById(R.id.rl_receive_qrcode);
        mRlReceiveScan = view.findViewById(R.id.rl_receive_scan);
        mRlReceiveBcard = view.findViewById(R.id.rl_receive_bcard);
        mRlPayType = view.findViewById(R.id.rl_pay_type);

        mSvVankeCoupon = view.findViewById(R.id.sv_vanke_coupon);
        mRlVankeVip = view.findViewById(R.id.rl_vanke_vip);
        mRlVankeCoupon = view.findViewById(R.id.rl_vanke_coupon);
        mEtVankeVip = view.findViewById(R.id.et_vanke_vip);
        mCbMaoku = view.findViewById(R.id.cb_coupon_maoku);
        mCbMeituan = view.findViewById(R.id.cb_coupon_meituan);
//        mEtVankeCoupon = view.findViewById(R.id.et_vanke_coupon);
        mIvVankeVip = view.findViewById(R.id.iv_vanke_vip);
        mIvVankeCoupon = view.findViewById(R.id.iv_vanke_coupon);
        mViewCursor = view.findViewById(R.id.view_cursor);
        mTvKeyPay = view.findViewById(R.id.tv_one);

        mIvVankeCoupon.setAlpha(0.5f);
        mIvVankeCoupon.setEnabled(false);
        ivBack.setVisibility(View.INVISIBLE);
        enable = false;
        setPayButton(enable);

        initCursorAnimator();
        objectAnimator.start();
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.WJY)) {
            mBtnPay.setText(R.string.tv_enter);
            mTvKeyPay.setText(R.string.tv_enter);
            mSvHbfq.setVisibility(View.GONE);
        } else {
            mBtnPay.setText(R.string.title_pay);
            mTvKeyPay.setText(R.string.title_pay);
            mSvVankeCoupon.setVisibility(View.GONE);
        }
    }

    private void initData() {
        mHandler = new Handler();
        mCouponList = new ArrayList<>();
        calculateBuilder = new StringBuilder();
        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            store_Id = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
            if (!TextUtils.isEmpty(store_Id)) {
                getStoreVoice(store_Id);
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                ServerClient.newInstance(context).queryMchHbfqEnable(context, Constants.TAG_HBFQ_ENABLE, null);
                if (MyApplication.getIsMerchant()) {
                    ServerClient.newInstance(context).checkVerifyStatus(context, Constants.TAG_CHECK_VERIFY_STATUS, null);
                }
                if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) ||
                        Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    couponTypeList();
                }
                if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    getPayCardAccount();
                }
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
        mRateData = new ArrayList<>();
        setRecyclerView();
    }

    private void getHbfqRateList(double money) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ((MainActivity) getActivity()).showNewLoading(true, getString(R.string.public_loading));
            String moneyString = String.valueOf(BigDecimal.valueOf(money).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            Map<String, Object> map = new HashMap<>();
            map.put("mchId", MyApplication.getMechantId());
            map.put("amount", moneyString);
            ServerClient.newInstance(MyApplication.getContext()).hbfqRateList(MyApplication.getContext(), Constants.TAG_HBFQ_RATE_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 商户已开通优惠券类型接口
     */
    private void couponTypeList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).couponTypeList(MyApplication.getContext(), "TAG_YS_COUPON_TYPE_LIST", null);
        }
    }

    /**
     * 获取POS账号密码
     */
    private void getPayCardAccount() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.HPAY_CARD_YINSHANG);
            } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.HUIFUTIANXIA);
            } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.ESAYCARD);
            }
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("sn", AppHelper.getYinshangSN());
            } else {
                map.put("sn", AppHelper.getDeviceId());
            }
            ServerClient.newInstance(MyApplication.getContext()).getPayCardThirdChannelId(MyApplication.getContext(), Constants.TAG_POS_ACCOUNT, map);
        }
    }

    /**
     * 保存POS账号密码
     */
    private void savePosRecord() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.HPAY_CARD_YINSHANG);
            } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.HUIFUTIANXIA);
            } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("thirdApiCode", Constants.ESAYCARD);
            }
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                map.put("sn", AppHelper.getYinshangSN());
            } else {
                map.put("sn", AppHelper.getDeviceId());
            }
            ServerClient.newInstance(MyApplication.getContext()).savePosRecord(MyApplication.getContext(), Constants.TAG_POS_RECORD, map);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            mTv_store_name.setVisibility(View.INVISIBLE);
        } else {
            String storeName = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME);
            mTv_store_name.setVisibility(View.VISIBLE);
            if (storeName.length() > 10) {
                mTv_store_name.setText(storeName.substring(0, 9) + "...");
            } else {
                mTv_store_name.setText(storeName);
            }
            store_Id = SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID);
            if (TextUtils.isEmpty(store_Id)) {
                ((MainActivity) getActivity()).showNewLoading(true, getString(R.string.public_loading));
                ServerClient.newInstance(MyApplication.getContext()).storePort(MyApplication.getContext(), Constants.STORE_PORT_TAG, null);
            }
        }
        mVipInfo = null;
        mCouponList.clear();
//        reset();
        if (!TextUtils.isEmpty(SpStayUtil.getString(MyApplication.getContext(), "BCARD_ORDER_NO"))) {//如果不为空，说明没有走onActivityResult方法,弹框提示刷卡单需要同步
            getPayResult();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_YS_COUPON_TYPE_LIST")) {
            CouponTypeListBean msg = (CouponTypeListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mRlVankeCoupon.setVisibility(View.VISIBLE);
                        mIvVankeCoupon.setAlpha(0.5f);
                        mIvVankeCoupon.setEnabled(false);
                        for (int i = 0; i < msg.getData().size(); i++) {
                            if ("pay.mallcoo.micropay".equals(msg.getData().get(i).getApiCode())) {
                                mMallCouponType = msg.getData().get(i).getInterProviderType();
                                mCbMaoku.setVisibility(View.VISIBLE);
                            } else if ("pay.scrm.micropay".equals(msg.getData().get(i).getApiCode())) {
                                SpUtil.putBoolean(MyApplication.getContext(), "SP_YXX_PROVIDER_TYPE", true);
                                mMallCouponType = msg.getData().get(i).getInterProviderType();
                                mCbMaoku.setVisibility(View.VISIBLE);
                            } else if ("pay.meituan.micropay".equals(msg.getData().get(i).getApiCode())) {
                                mCbMeituan.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_PUT_STORE)) {
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.PUT_STORE_FALSE:
                    if (msg != null && msg.getError() != null) {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                    }
                    break;
                case Constants.PUT_STORE_TRUE:
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_HBFQ_RATE_LIST)) {
            ((MainActivity) getActivity()).dismissLoading();
            HbfqRateBean msg = (HbfqRateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        mRateData.clear();
                        mRateData.addAll(msg.getData());
                        if (msg.getData().size() > 0) {
                            /*mHbFqNum = mRateData.get(0).getHbfqNum();
                            isRateFree = mRateData.get(0).isFreeFee();*/
                            setData(mRateData);
                            isRateFree = isRateFree(mRateData);
                            mLlHbfqDetail.setVisibility(View.VISIBLE);
                            setRecyclerView();
                        } else {
                            mLlHbfqDetail.setVisibility(View.GONE);
                        }
                    } else {
                        mLlHbfqDetail.setVisibility(View.GONE);
                        ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.error_data));
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.GETUI_PUSH_VOICE_LIST_TAG)) {
            VoiceBean msg = (VoiceBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (msg.getData().get(0).getPushClose() == 1) {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, true);
                        } else {
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_STORE_VOICE, false);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.STORE_PORT_TAG)) {
            ((MainActivity) getActivity()).dismissLoading();
            StoreBean bean = (StoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (bean.getError() != null) {
                        if (bean.getError().getCode() != null) {
                            if (bean.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (bean.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), bean.getError().getMessage());
                                }
                            } else {
                                if (bean.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), bean.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (bean != null) {
                        if (bean.getData() != null) {
                            if (bean.getData().getStoreName() != null) {
                                String storeName = bean.getData().getStoreName();
                                if (!TextUtils.isEmpty(storeName)) {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
                                    mTv_store_name.setVisibility(View.VISIBLE);
                                    if (storeName.length() > 10) {
                                        mTv_store_name.setText(storeName.substring(0, 9) + "...");
                                    } else {
                                        mTv_store_name.setText(storeName);
                                    }
                                } else {
                                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, "");
                                    mTv_store_name.setVisibility(View.INVISIBLE);
                                }
                            }
                            if (!TextUtils.isEmpty(bean.getData().getStoreId())) {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, false);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, bean.getData().getStoreId());
                                store_Id = bean.getData().getStoreId();
                                getStoreVoice(store_Id);
                            } else {
                                SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_STORE_NULL, true);
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, "");
                                store_Id = "";
                            }
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_CHECK_VERIFY_STATUS)) {
            MerChantModifyBean msg = (MerChantModifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.CHECK_VERIFY_STATUS_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CHECK_VERIFY_STATUS_TRUE:
                    if (msg.getData() != null && msg.getData().getExamineRemark() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_EXAMINE_REMARK, msg.getData().getExamineRemark());
                    } else {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_EXAMINE_REMARK, "");
                    }
                    if (msg.getData() != null) {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_EXAMINE_STATUS, msg.getData().getExamineStatus());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE, msg.getData().getAccountType());
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_COMPANY_MERCHANT_TYPE, msg.getData().getAccountAttribute());
                    } else {
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_EXAMINE_STATUS, -1);
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE, -1);
                        SpUtil.putInt(MyApplication.getContext(), Constants.SP_COMPANY_MERCHANT_TYPE, -1);
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_POS_ACCOUNT)) {
            PosAccountBean msg = (PosAccountBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mPosAccountBean = msg.getData();
                    if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        startYinshangSign(mPosAccountBean.getThirdMerchantId());
                    } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        if (mPosAccountBean != null && !TextUtils.isEmpty(mPosAccountBean.getThirdMerchantId())) {
                            posAutoLogin(mPosAccountBean.getThirdMerchantId());
                        }
                    } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                            || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                        if (!DateUtil.getDate().equals(MyApplication.getSignDate())) {
                            startActivityWizarposSign();
                        } else {
                            startActivityWizarposMerInfo();
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_HBFQ_ENABLE)) {
            ((MainActivity) getActivity()).dismissLoading();
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_HUABEI_WHITE, msg.getData().isEnable());
                        mViewLine.setVisibility(MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ) ? View.VISIBLE : View.GONE);
                        mRlHuabei.setVisibility(MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ) ? View.VISIBLE : View.GONE);
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_HUABEI_WHITE, false);
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_POS_BCARD_PAY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PosOrderBean msg = (PosOrderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    posOrderBean = msg.getData();
                    if (msg.getData() != null) {
                        SpStayUtil.putString(MyApplication.getContext(), "BCARD_ORDER_NO", posOrderBean.getOrderNo());
                        String money = String.valueOf(BigDecimal.valueOf(totalMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
                        if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                            startYinshangCard(money, msg.getData().getOrderNo());
                        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                                || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                            startActivityWizarposCard(BigDecimal.valueOf(totalMoney).setScale(2, BigDecimal.ROUND_HALF_UP).toString(), msg.getData().getOrderNo());
                        } else {
                            startArouseCardPay(msg.getData(), money);
                        }
                    } else {
                        ToastHelper.showInfo(getActivity(), getString(R.string.error_arouse_bcard_pay));
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.NOTIFY_PAY_EASYPAY)) {
            DialogUtil.safeCloseDialog(mSyncLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (cancelTask) {
                        showSyncResult();
                    } else {
                        syncOrderState();
                    }
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastHelper.showInfo(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    remove();
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_RECEIVE_COUPON_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponInfoBean msg = (CouponInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    CouponInfoData data = msg.getData();
                    if (data != null) {
                        boolean isExist = false;
                        for (int i = 0; i < mCouponList.size(); i++) {
                            if (mCouponList.get(i).getCouponCode().equals(data.getCouponCode())) {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                            mCouponList.add(0, data);
                        toOrderCouponInfo();
                    } else {
                        MyToast.showToastShort(getString(R.string.toast_coupon_null_data));
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_RECEIVE_VIP_COUPON)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CouponListBean msg = (CouponListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<CouponInfoData> couponList = msg.getData();
                    if (couponList != null && couponList.size() > 0) {
                        mCouponList.addAll(couponList);
                    }
                    /*if (!TextUtils.isEmpty(mEtVankeCoupon.getText().toString().trim())) {
                        addCanUse(mEtVankeCoupon.getText().toString().trim());
                    } else {*/
                    toOrderCouponInfo();
//                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_RECEIVE_VIP_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            VipInfoBean msg = (VipInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    mVipInfo = msg.getData();
                    if (mVipInfo != null) {
                        if ("0".equals(mInterProviderType) || "2".equals(mInterProviderType)) {//商场券
                            getCanUseByMallCardNo(mVipInfo);
                        } else {
                            toOrderCouponInfo();
                        }
                    } else {
                        ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.error_vip_info));
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_RECEIVE_ORDER_DETAIL") || event.getTag().equals("TAG_AUTO_ORDER_DETAIL")) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PayBean msg = (PayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    PayBean.DataBean data = msg.getData();
                    if (data != null) {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.INTENT_BILL_DATA, data);
                        intent.setClass(getActivity(), PayDetailActivity.class);
                        if (event.getTag().equals("TAG_AUTO_ORDER_DETAIL")) {
                            //传个标识，直接同步
                            intent.putExtra(Constants.INTENT_NAME, "TAG_AUTO_ORDER_DETAIL");
                        }
                        startActivity(intent);
                    }
                    break;
            }
        }

    }

    private void toOrderCouponInfo() {
        if (TextUtils.isEmpty(store_Id)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(getActivity(), OrderCouponInfoActivity.class);
        intent.putExtra(Constants.INTENT_VANKE_COUPON, (Serializable) mCouponList);
        intent.putExtra(Constants.INTENT_VANKE_VIP, mVipInfo);
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        startActivity(intent);
//        reset();
    }

    /**
     * 汇付POS登录
     *
     * @param memberId
     */
    private void posAutoLogin(String memberId) {
        StringBuilder builderUri = new StringBuilder();
        switch (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) {
            case Constants.HUIFU:
                builderUri.append("payment://com.pnr.pospp/autoLogin?");
                builderUri.append("memberId=").append(memberId);
                LogUtil.d("builderUri====posAutoLogin=" + builderUri.toString());
                startActivityLogin(builderUri.toString());
                break;
        }
    }

    /**
     * 唤起pos登录
     *
     * @param uriStr
     */
    private void startActivityLogin(String uriStr) {
        Uri uri = Uri.parse(uriStr);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivityForResult(intent, Constants.REQUEST_BCARD_LOGIN);
    }

    /**
     * 汇付刷卡
     *
     * @param posOrderBean
     * @param money
     */
    private void startArouseCardPay(PosOrderBean.DataEntity posOrderBean, String money) {
        StringBuilder builderUri = new StringBuilder();
        switch (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)) {
            case Constants.HUIFU:
                builderUri.append("payment://com.pnr.pospp/payment?");
                builderUri.append("channelId=acquire");
                builderUri.append("&ordAmt=").append(money);
                if (!TextUtils.isEmpty(posOrderBean.getReqOrderNo())) {
                    builderUri.append("&merOrdId=").append(posOrderBean.getReqOrderNo());
                }
                if (!TextUtils.isEmpty(mPayRemark)) {
                    builderUri.append("&ordRemark=").append(mPayRemark);
                }
                if (!TextUtils.isEmpty(posOrderBean.getBody())) {
                    builderUri.append("&goodsDesc=").append(posOrderBean.getBody());
                }
                if (!TextUtils.isEmpty(posOrderBean.getRetNotifyUrl())) {
                    builderUri.append("&bgRetUrl=").append(posOrderBean.getRetNotifyUrl());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosMemberId())) {
                    builderUri.append("&memberId=").append(MyApplication.getPosMemberId());
                }
                if (!TextUtils.isEmpty(MyApplication.getPosEmpAccount())) {
                    builderUri.append("&merOperId=").append(MyApplication.getPosEmpAccount());
                }
                builderUri.append("&merOperIdPrinted=").append("0");
                if (!TextUtils.isEmpty(posOrderBean.getExt1())) {
                    builderUri.append("&accSplitBunch=").append(posOrderBean.getExt1());
                }
                if (!TextUtils.isEmpty(posOrderBean.getExt2())) {
                    builderUri.append("&merPiv=").append(posOrderBean.getExt1());
                }
                /*if (!TextUtils.isEmpty("")){
                    builderUri.append("&outPrintData=").append("");
                }*/
                LogUtil.d("builderUri====payment=" + builderUri.toString());
                startActivityCard(builderUri.toString());
                break;
        }
    }

    /**
     * 唤起刷卡支付
     *
     * @param uriStr
     */
    private void startActivityCard(String uriStr) {
        Uri uri = Uri.parse(uriStr);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivityForResult(intent, Constants.REQUEST_BCARD_PAY);
    }

    /**
     * 唤起银商刷卡支付
     */
    private void startYinshangCard(String money, String orderNo) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "全民惠";
        String transAppId = "消费";
        JSONObject transData = new JSONObject();
        try {
            transData.put("amt", money);//金额
            transData.put("appId", APPID);//appId
            transData.put("isNeedPrintReceipt", false);//交易结束后自动打单
            transData.put("extOrderNo", orderNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
                mYinshangPosBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                if (mYinshangPosBean != null) {
                    if ("0".equals(mYinshangPosBean.getResultCode())) {
                        if (mYinshangPosBean.getTransData() != null && "00".equals(mYinshangPosBean.getTransData().getResCode())) {
                            if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                MyToast.showToastShort(getString(R.string.network_exception));
                                syncOrderState();
                            } else {
                                payYinshangNotify(mYinshangPosBean);
                            }
                        } else {
                            syncDialog(mYinshangPosBean.getTransData().getResDesc());
                        }
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), mYinshangPosBean.getResultMsg());
                    }
                } else {
                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "支付回调数据有误，请确认支付结果！");
                }
//                reset();
            }
        };
        com.ums.AppHelper.callTrans(getActivity(), transAppName, transAppId, transData, listener);
    }

    private void syncDialog(String resDesc) {
        String title;
        if (TextUtils.isEmpty(resDesc)) {
            title = "支付失败，如有异议，请至订单详情页同步查询";
        } else {
            title = resDesc + "，如有异议，请至订单详情页同步查询";
        }
        SelectDialog syncSelectDialog = new SelectDialog(getActivity(), title, "取消", "去查询", R.layout.select_common_dialog);
        syncSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                queryOrder("TAG_RECEIVE_ORDER_DETAIL");
            }
        });
        syncSelectDialog.show();
    }

    private void getPayResult() {
        if (mGetCardPayResultDialog == null) {
            mGetCardPayResultDialog = new SelectDialog(getActivity(), "您有一笔刷卡订单未获取到收款结果，是否立即获取结果？", "忽略", "获取", R.layout.select_common_dialog);
            mGetCardPayResultDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    queryOrder("TAG_AUTO_ORDER_DETAIL");
                }
            });
            mGetCardPayResultDialog.setOnClickCancelListener(new SelectDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
                }
            });
            DialogHelper.resize(getActivity(), mGetCardPayResultDialog, 0.9f);
        }
        mGetCardPayResultDialog.show();
    }

    /**
     * 唤起易生刷卡支付
     */
    private void startActivityWizarposCard(String money, String orderNo) {
        Map<String, String> map = new HashMap<>();
        map.put("option", "consume");
        map.put("paytype", "card");
        map.put("amount", money);
        map.put("orderNo", orderNo);
        startActivityForResult(map, Constants.REQUEST_BCARD_PAY);
    }

    /**
     * 慧银pos签到
     */
    private void startActivityWizarposSign() {
        Map<String, String> map = new HashMap<>();
        map.put("option", "sign");
        startActivityForResult(map, Constants.REQUEST_POS_SIGN);
    }

    /**
     * 慧银pos获取商户信息
     */
    private void startActivityWizarposMerInfo() {
        Map<String, String> map = new HashMap<>();
        map.put("option", "getMerInfo");
        startActivityForResult(map, Constants.REQUEST_POS_MERCHANT_INFO);
    }

    private void startActivityForResult(Map<String, String> map, int requestCode) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).unbindDeviceService();
        }
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("net.worthtech.worthcasher", "net.worthtech.worthcasher.activity.PayService"));
        for (Map.Entry<String, String> arg : map.entrySet()) {
            intent.putExtra(arg.getKey(), arg.getValue());
        }
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_remark:
                Intent intent = new Intent(getActivity(), RemarkActivity.class);
                intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
                startActivityForResult(intent, Constants.REQUEST_PAY_REMARK);
                break;
            case R.id.num_0:
                AppHelper.execVibrator(context);
                calculateProcess(0);
                break;
            case R.id.num_1:
                AppHelper.execVibrator(context);
                calculateProcess(1);
                break;
            case R.id.num_2:
                AppHelper.execVibrator(context);
                calculateProcess(2);
                break;
            case R.id.num_3:
                AppHelper.execVibrator(context);
                calculateProcess(3);
                break;
            case R.id.num_4:
                AppHelper.execVibrator(context);
                calculateProcess(4);
                break;
            case R.id.num_5:
                AppHelper.execVibrator(context);
                calculateProcess(5);
                break;
            case R.id.num_6:
                AppHelper.execVibrator(context);
                calculateProcess(6);
                break;
            case R.id.num_7:
                AppHelper.execVibrator(context);
                calculateProcess(7);
                break;
            case R.id.num_8:
                AppHelper.execVibrator(context);
                calculateProcess(8);
                break;
            case R.id.num_9:
                AppHelper.execVibrator(context);
                calculateProcess(9);
                break;
            //加号
            case R.id.lay_add:
                AppHelper.execVibrator(context);
                calculateProcess("+");
                break;
            //全删
            case R.id.lay_clear:
                AppHelper.execVibrator(context);
                if (calculateBuilder.toString().contains("+")) {
                    showDialog();
                } else {
                    clearData();
                }
                break;
            //点
            case R.id.lay_point:
                AppHelper.execVibrator(context);
                calculateProcess(".");
                break;
            //删除
            case R.id.lay_delete:
                AppHelper.execVibrator(context);
                deleteData();
                break;
        }
        LogUtil.d("number_calculate==" + calculateBuilder.toString());
    }

    private void initCursorAnimator() {
        objectAnimator = ObjectAnimator.ofFloat(mViewCursor, "alpha", 1f, 0f);
        objectAnimator.setDuration(1000);
        objectAnimator.setRepeatMode(ValueAnimator.RESTART);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
    }

    private void setFocusable(EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        InputMethodManager inputManager =
                (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(editText, 0);
        setKeyboardView(false);
    }

    private void getUserInfo(String vipNo) {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("mallCardNo", vipNo);
            map.put("storeMerchantId", store_Id);
            ServerClient.newInstance(MyApplication.getContext()).getUserInfo(MyApplication.getContext(), Constants.TAG_RECEIVE_VIP_INFO, map);
        }
    }

    private void getCanUseByMallCardNo(VipInfoData vipInfo) {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("mallCardNo", vipInfo.getMallCardNo());
            map.put("bizUid", vipInfo.getBizUid());
            map.put("storeMerchantId", store_Id);
            String money = String.valueOf(BigDecimal.valueOf(totalMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            map.put("orderAmount", money);
            if (mInterProviderType != null && !"-1".equals(mInterProviderType))
                map.put("interProviderType", mInterProviderType);
            ServerClient.newInstance(MyApplication.getContext()).getCanUseByMallCardNo(MyApplication.getContext(), Constants.TAG_RECEIVE_VIP_COUPON, map);
        }
    }

    private void addCanUse(String code) {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), ToastHelper.toStr(R.string.network_exception));
        } else {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("couponCode", code);
            map.put("storeMerchantId", store_Id);
            ServerClient.newInstance(MyApplication.getContext()).addCanUse(MyApplication.getContext(), Constants.TAG_RECEIVE_COUPON_INFO, map);
        }
    }

    private void capture(String intentName) {
        if (TextUtils.isEmpty(store_Id)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        if (totalMoney <= 0) {
            MyToast.showToast(UIUtils.getString(R.string.please_input_amount), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(context, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_NAME, intentName);
        intent.putExtra(Constants.INTENT_VIP_NUMBER, mEtVankeVip.getText().toString().trim());
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        if (mLlHbfqDetail.getVisibility() == View.VISIBLE) {
            intent.putExtra(Constants.INTENT_HBFQ, mHbFqNum);
            intent.putExtra(Constants.INTENT_IS_RATE_FREE, isRateFree);
        }
        if (mRlVankeCoupon.getVisibility() == View.VISIBLE) {
            intent.putExtra(Constants.INTENT_COUPON_PROVIDER_TYPE, mInterProviderType);
        }
        startActivity(intent);
//        reset();
    }
    /**
     * 银商签到
     *
     * @param thirdMerchantId
     */
    private void startYinshangSign(final String thirdMerchantId) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "公共资源";
        String transAppId = "签到";
        JSONObject transData = new JSONObject();
        try {
            transData.put("appId", APPID);//appId
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
                YinshangPosBean yinshangPosBean = new Gson().fromJson(reslutmsg, YinshangPosBean.class);
                if (yinshangPosBean.getTransData() != null) {
                    if (yinshangPosBean.getTransData().getTerminalNo() != null) {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, yinshangPosBean.getTransData().getTerminalNo());
                    } else {
                        SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, "");
                    }
                    if (thirdMerchantId != null && thirdMerchantId.equals(yinshangPosBean.getTransData().getMerchantNo())) {
                        mRlReceiveBcard.setVisibility(View.VISIBLE);
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, true);
                    } else {
                        mRlReceiveBcard.setVisibility(View.GONE);
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, false);
                        MyToast.showToastShort("第三方商户号有误！(" + thirdMerchantId + ")与(" + yinshangPosBean.getTransData().getMerchantNo() + ")不一致");
                    }
                } else {
                    MyToast.showToastShort("签到失败！");
                }

            }
        };
        com.ums.AppHelper.callTrans(getActivity(), transAppName, transAppId, transData, listener);
    }

    private void setPayType(int visibility) {
        Animation animation;
        if (visibility == View.VISIBLE) {
            animation = AnimationUtils.loadAnimation(MyApplication.getContext(), R.anim.push_bottom_in);
        } else {
            animation = AnimationUtils.loadAnimation(MyApplication.getContext(), R.anim.push_bottom_out);
        }
        animation.setInterpolator(new LinearInterpolator());
        animation.setFillAfter(true);
        mRlPayType.startAnimation(animation);
        mRlPayType.setVisibility(visibility);
        if (visibility == View.GONE) {
            mRlPayType.clearAnimation();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            String storeName = shopBean.getStoreName();
            String storeId = shopBean.getStoreId();
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_ID, storeId);
            SpUtil.putString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, storeName);
            LogUtil.d("SP_DEFAULT_STORE_ID=" + storeId + ",,SP_DEFAULT_STORE_NAME=" + storeName);
            store_Id = storeId;
            if (storeName.length() > 10) {
                mTv_store_name.setText(storeName.substring(0, 9) + "...");
            } else {
                mTv_store_name.setText(storeName);
            }
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("storeName", storeName);
                map.put("storeId", storeId);
                ServerClient.newInstance(MyApplication.getContext()).postStore(MyApplication.getContext(), Constants.TAG_PUT_STORE, map);
                getStoreVoice(store_Id);
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PAY_REMARK) {
            mPayRemark = data.getStringExtra(Constants.RESULT_PAY_REMARK);
        } else if (requestCode == Constants.REQUEST_BCARD_PAY) {
            if (data != null) {
                try {
                    LogUtil.e("REQUEST_BCARD_PAY", URLDecoder.decode(data.toURI(), "UTF-8"));
                } catch (Exception e) {

                }
                String message;
                String responseCode;
                if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                        || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                    message = data.getStringExtra("respmsg");
                    responseCode = data.getStringExtra("result");
                    mResultData = data;
                    if ("00".equals(responseCode)) {//支付成功
                        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                            MyToast.showToastShort(getActivity(), getString(R.string.network_exception));
                            syncOrderState();
                        } else {
                            payNotify(data);
                        }
                    } else {
                        syncDialog(message);
                    }
                } else {
                    message = data.getStringExtra("message");
                    responseCode = data.getStringExtra("responseCode");
                    if (!"00".equals(responseCode) && !TextUtils.isEmpty(message)) {
                        ToastHelper.showInfo(getActivity(), message);
                    }
                }
                if ("00".equals(responseCode)) {
                    clearData();
                    mPayRemark = "";
                    setPayType(View.GONE);
                }
            } else {
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "支付回调数据有误，请确认支付结果！");
            }
            SpStayUtil.removeKey("BCARD_ORDER_NO");//如果走到这里，则说明拿到刷卡返回结果，移除后则不进行提示同步
        } else if (requestCode == Constants.REQUEST_POS_SIGN) {
            if (data != null) {
                try {
                    LogUtil.e("REQUEST_POS_SIGN", URLDecoder.decode(data.toURI(), "UTF-8"));
                } catch (Exception e) {

                }
                String responseCode = data.getStringExtra("result");
                if ("00".equals(responseCode)) {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_SIGN_DATE, DateUtil.getDate());
                    startActivityWizarposMerInfo();
                } else {
                    MyToast.showToastShort(getString(R.string.error_activity_sign_failed));
                }
            } else {
                MyToast.showToastShort(UIUtils.getString(R.string.error_third_activity_result));
            }
        } else if (requestCode == Constants.REQUEST_POS_MERCHANT_INFO) {
            try {
                LogUtil.e("REQUEST_POS_MERCHANT_INFO", URLDecoder.decode(data.toURI(), "UTF-8"));
            } catch (Exception e) {

            }
            if (data != null) {
                String mer_name = data.getStringExtra("mer_name");
                String mer_id = data.getStringExtra("mer_id");
                String ter_id = data.getStringExtra("ter_id");
                if (!TextUtils.isEmpty(ter_id)) {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, ter_id);
                } else {
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_DEVICE_NO, "");
                }
                if (mer_id.equals(mPosAccountBean.getThirdMerchantId())) {
                    mRlReceiveBcard.setVisibility(View.VISIBLE);
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, true);
                } else {
                    mRlReceiveBcard.setVisibility(View.GONE);
                    SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_BCARD_OPEN, false);
                    MyToast.showToastShort("第三方商户号有误！(" + mPosAccountBean.getThirdMerchantId() + ")与(" + mer_id + ")不一致");
                }
            }
        } else if (requestCode == Constants.REQUEST_POS_RECEIVE
                || requestCode == Constants.REQUEST_POS_RECEIVE_SCAN
                || requestCode == Constants.REQUEST_YINSHANG_POS_RECEIVE) {
            reset();
        } else if (requestCode == Constants.REQUEST_BCARD_LOGIN) {
            if (data != null) {
                String responseCode = data.getStringExtra("responseCode");
                SpUtil.putString(MyApplication.getContext(), Constants.SP_POS_LOGIN_STATUS, responseCode);
                String memberId = data.getStringExtra("memberId");
                SpUtil.putString(MyApplication.getContext(), Constants.SP_POS_MEMBER_ID, memberId);
                String empAccount = data.getStringExtra("empAccount");
                SpUtil.putString(MyApplication.getContext(), Constants.SP_POS_EMP_ACCOUNT, empAccount);
                if ("00".equals(responseCode)) {
                    mRlReceiveBcard.setVisibility(View.VISIBLE);
                    if ("0".equals(mPosAccountBean.getStatus())) {
                        savePosRecord();
                    }
                } else {
                    mRlReceiveBcard.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * 重置，清除缓存数据
     */
    private void reset() {
        clearData();
        mVipInfo = null;
        mCouponList.clear();
        mEtVankeVip.setText("");
        mEtVankeVip.clearFocus();
        mPayRemark = "";
        mCbUnion.setChecked(true);
        mCbHbfq.setChecked(false);
        mLlHbfqDetail.setVisibility(View.GONE);
        setPayType(View.GONE);
        mCbMaoku.setChecked(false);
        mCbMeituan.setChecked(false);

        cancelTask = false;
        timeCount = 5;
        count = 1;
    }

    /**
     * 刷卡支付
     *
     * @param money
     */
    private void bCardPayment(String money) {
        DialogUtil.safeShowDialog(mLoadDialog);
        Map<String, Object> map = new HashMap<>();
        map.put("money", money);
        map.put("storeMerchantId", MyApplication.getDefaultStore());
        if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            map.put("thirdApiCode", Constants.HPAY_CARD_YINSHANG);
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
        } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            map.put("thirdApiCode", Constants.ESAYCARD);
//            map.put("deviceInfo", AppHelper.getDeviceInfo());
        } else if (Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            map.put("thirdApiCode", Constants.ESAYCARD);
//            map.put("deviceInfo", Constants.HPAY_POS_EASYPAY);
        } else {
            map.put("thirdApiCode", Constants.HUIFUTIANXIA);
//            map.put("deviceInfo", Constants.HPAY_POS_HUIFU);
        }
        map.put("deviceInfo", StringUtils.getDeviceInfo(AppHelper.getDeviceInfo()));
        if (!TextUtils.isEmpty(AppHelper.getIPAddress(MyApplication.getContext()))) {
            map.put("mchCreateIp", AppHelper.getIPAddress(MyApplication.getContext()));
        }
        if (!TextUtils.isEmpty(mPayRemark)) {
            map.put("attach", mPayRemark);
        }
        if (!TextUtils.isEmpty(StringUtils.getDeviceInfo(AppHelper.getSN()))) {
            map.put("opDeviceId", StringUtils.getDeviceInfo(AppHelper.getSN()));
        }
        ServerClient.newInstance(MyApplication.getContext()).bCardPayment(MyApplication.getContext(), Constants.TAG_POS_BCARD_PAY, map);
    }

    /**
     * 易生刷卡支付通知
     *
     * @param data
     */
    private void payNotify(Intent data) {
        if (data != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", data.getStringExtra("orderNo"));
            if (!TextUtils.isEmpty(data.getStringExtra("amount"))) {
                map.put("totalFee", new BigDecimal(data.getStringExtra("amount")).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            }
            map.put("cardType", data.getStringExtra("paytype"));
            if (posOrderBean != null) {
                map.put("storeMerchantId", posOrderBean.getStoreMerchantId());
            }
            map.put("payType", data.getStringExtra("paytype"));
            String printInfo = data.getStringExtra("printInfo");
            if (!TextUtils.isEmpty(printInfo)) {
                Gson gson = new Gson();
                EasypayInfo easypayInfo = gson.fromJson(printInfo, EasypayInfo.class);
                map.put("timeEnd", DateUtil.formartTradeTime(easypayInfo.getDate_time()));
                map.put("voucherNo", easypayInfo.getTrace());
                map.put("batchId", easypayInfo.getBatch_no());
                map.put("refNo", easypayInfo.getRefer_no());
                map.put("payCardId", easypayInfo.getCard_no());
            }
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), Constants.NOTIFY_PAY_EASYPAY, map);
        }
    }

    /**
     * 银商刷卡支付通知
     *
     * @param bean
     */
    private void payYinshangNotify(YinshangPosBean bean) {
        if (bean != null) {
            DialogUtil.safeShowDialog(mSyncLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("transactionId", bean.getTransData().getExtOrderNo());
            if (!TextUtils.isEmpty(bean.getTransData().getAmt())) {
                map.put("totalFee", new BigDecimal(bean.getTransData().getAmt()).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
            }
            map.put("cardType", bean.getTransData().getCardType());
            if (posOrderBean != null) {
                map.put("storeMerchantId", posOrderBean.getStoreMerchantId());
            }
            map.put("payType", bean.getTransData().getCardType());
            map.put("timeEnd", DateUtil.formartTradeTime(bean.getTransData().getDate() + " " + bean.getTransData().getTime(), "yyyy/MM/dd HH:mm:ss"));
            map.put("voucherNo", bean.getTransData().getTraceNo());
            map.put("batchId", bean.getTransData().getBatchNo());
            map.put("refNo", bean.getTransData().getRefNo());
            map.put("payCardId", bean.getTransData().getCardNo());
            ServerClient.newInstance(MyApplication.getContext()).payNotify(MyApplication.getContext(), Constants.NOTIFY_PAY_EASYPAY, map);
        }
    }

    /**
     * 正扫支付
     */
    private void toQrcodeActivity() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Intent intent = new Intent();
            intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
            intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
            intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
            if (mLlHbfqDetail.getVisibility() == View.VISIBLE) {
                intent.putExtra(Constants.INTENT_HBFQ, mHbFqNum);
                intent.putExtra(Constants.INTENT_IS_RATE_FREE, isRateFree);
                intent.setClass(getActivity(), QrCodeActivity.class);
            } else {
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_POS_QRCODE);
                intent.setClass(getActivity(), StaticCodeImageActivity.class);
            }
            startActivityForResult(intent, Constants.REQUEST_POS_RECEIVE);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    //扫描二维码
    private void toCaptureActivity() {
        if (TextUtils.isEmpty(store_Id)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(context, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        if (mLlHbfqDetail.getVisibility() == View.VISIBLE) {
            intent.putExtra(Constants.INTENT_HBFQ, mHbFqNum);
            intent.putExtra(Constants.INTENT_IS_RATE_FREE, isRateFree);
        }
        startActivityForResult(intent, Constants.REQUEST_POS_RECEIVE_SCAN);
    }

    //银商订单详情页面
    private void toOrderInfoActivity() {
        if (TextUtils.isEmpty(store_Id)) {
            MyToast.showToast(UIUtils.getString(R.string.dialog_pay_store_error), Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(context, OrderPayInfoActivity.class);
        intent.putExtra(Constants.INTENT_PAY_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_ACTUAL_MONEY, totalMoney);
        intent.putExtra(Constants.INTENT_QRCODE_STOREMERCHANTID, store_Id);
        intent.putExtra(Constants.INTENT_PAY_REMARK, mPayRemark);
        startActivity(intent);
//        reset();
    }

    private void clearData() {
        totalMoney = 0;
        calculateBuilder.delete(0, calculateBuilder.length());
        tv_money.setText(getString(R.string.tv_money_zero));
        tv_money.setTextColor(UIUtils.getColor(R.color.line_color));
        tv_calculate.setText(getString(R.string.tv_money_zero));
        tv_calculate.setTextColor(UIUtils.getColor(R.color.line_color));
        enable = false;
        setPayButton(enable);
        mLlHbfqDetail.setVisibility(View.GONE);
    }

    private void deleteData() {
        if (calculateBuilder.length() > 0) {
            calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            tv_calculate.setText(calculateBuilder.toString());
        }
        calculateResult();
    }

    private void calculateProcess(Object object) {
        if (object instanceof Integer) {
            if ((calculateBuilder.toString().lastIndexOf("0") == 0 && calculateBuilder.length() == 1) ||
                    (calculateBuilder.toString().lastIndexOf("0") == calculateBuilder.length() - 1
                            && calculateBuilder.toString().lastIndexOf("+") == calculateBuilder.length() - 2)) {
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
            }
            if (calculateBuilder.toString().contains(".")) {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (calculateBuilder.length() >= 3) {
                    if (calculateBuilder.toString().endsWith(".00")) {
                        calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                    } else if (pointIndex == calculateBuilder.length() - 3 &&
                            calculateBuilder.toString().lastIndexOf("+") != calculateBuilder.length() - 1) {
                        return;
                    }
                }
            }
        }
        if (object instanceof String) {
            if (TextUtils.isEmpty(calculateBuilder)) {
                if (object.equals("+")) {
                    return;
                }
                if (object.equals(".")) {
                    calculateBuilder.append("0");
                }
            } else {
                int pointIndex = calculateBuilder.toString().lastIndexOf(".");
                if (pointIndex == calculateBuilder.length() - 1) {
                    if (object.equals("+")) {
                        calculateBuilder.append("0");
                    }
                }
                if (!calculateBuilder.toString().contains("+")) {
                    if (calculateBuilder.toString().contains(".") && object.equals(".")) {
                        return;
                    }
                } else {
                    int addIndex = calculateBuilder.toString().lastIndexOf("+");
                    if (addIndex == calculateBuilder.length() - 1) {
                        if (object.equals("+")) {
                            return;
                        }
                        if (object.equals(".")) {
                            calculateBuilder.append("0");
                        }
                    } else {
                        if (pointIndex > addIndex && object.equals(".")) {
                            return;
                        }
                    }
                }
            }
        }
        calculateBuilder.append(object);
        calculateResult();
        tv_calculate.setText(calculateBuilder.toString());
    }

    private void calculateResult() {
        if (TextUtils.isEmpty(calculateBuilder)) {
            tv_money.setText(getString(R.string.tv_money_zero));
            tv_calculate.setText(getString(R.string.tv_money_zero));
            tv_calculate.setTextColor(UIUtils.getColor(R.color.line_color));
            enable = false;
            totalMoney = 0;
            setPayButton(enable);
        } else {
            BigDecimal bigSum = null;
            if (calculateBuilder.toString().contains("+")) {
                String[] numbers = calculateBuilder.toString().split("\\+");
                if (numbers.length == 1) {
                    LogUtil.d("number1==", numbers[0]);
                    bigSum = new BigDecimal(numbers[0]);
                } else {
                    bigSum = new BigDecimal(0);
                    if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                        numbers[numbers.length - 1] = numbers[numbers.length - 1] + "0";
                    }
                    for (int i = 0; i < numbers.length; i++) {
                        LogUtil.d("number2==", numbers[i] + "--" + i);
                        BigDecimal big = new BigDecimal(numbers[i]);
                        bigSum = bigSum.add(big);
                        LogUtil.d("number3==", bigSum.doubleValue() + "");
                    }
                }
            } else {
                String calculateString = calculateBuilder.toString();
                if (calculateBuilder.lastIndexOf(".") == calculateBuilder.length() - 1) {
                    bigSum = new BigDecimal(calculateString + "0");
                } else {
                    bigSum = new BigDecimal(calculateString);
                }
            }
            if (bigSum.doubleValue() > 300000) {
                MyToast.showToast(UIUtils.getString(R.string.tv_pay_exceed_money), Toast.LENGTH_SHORT);
                calculateBuilder.deleteCharAt(calculateBuilder.length() - 1);
                return;
            }
            if (bigSum.doubleValue() <= 0) {
                enable = false;
            } else {
                enable = true;
            }
            setPayButton(enable);
            totalMoney = bigSum.doubleValue();
            tv_money.setText(DateUtil.formatPaseMoneyUtil(bigSum));
//            setHbfqDetail(mRateData, totalMoney);
            requestHbfqDetail();
        }
        tv_money.setTextColor(UIUtils.getColor(enable ? R.color.tv_pay_total_money : R.color.line_color));
        tv_calculate.setTextColor(TextUtils.isEmpty(calculateBuilder) ? UIUtils.getColor(R.color.line_color) : UIUtils.getColor(R.color.tv_pay_total_money));
    }

    private void setPayButton(boolean enable) {
        if (enable) {
            Drawable rectangleDefault = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            Drawable rectanglePressed = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectanglePressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
            scan_pay.setBackground(stateListDrawable);
            scan_pay.setEnabled(true);
            mBtnPay.setBackgroundResource(R.drawable.shape_rectangle_blue_22);
            mBtnPay.setEnabled(true);
        } else {
            Drawable rectangleUnable = ShapeSelectorUtils.createRectangleDefault(MyApplication.getContext(), MyApplication.themeColor());
            rectangleUnable.setAlpha(102);
            scan_pay.setBackground(rectangleUnable);
            scan_pay.setEnabled(false);
            mBtnPay.setBackgroundResource(R.drawable.shape_rectangle_blue_unable_22);
            mBtnPay.setEnabled(false);
        }
        if (!enable) {
//            mCbHbfq.setChecked(false);
            mLlHbfqDetail.setVisibility(View.GONE);
        }

    }

    public void showDialog() {
        if (mSelectDialog == null) {
            mSelectDialog = new SelectDialog(getActivity(), getString(R.string.dialog_notice_clear), R.layout.select_common_dialog);
            mSelectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    clearData();
                }
            });
        }
        mSelectDialog.show();
    }

    private void getStoreVoice(String storeId) {
        Map<String, Object> voiceMap = new HashMap<>();
        voiceMap.put("storeMerchantId", storeId);
        String clientid = PushManager.getInstance().getClientid(MyApplication.getContext());
        if (!StringUtils.isEmptyOrNull(clientid)) {
            voiceMap.put("pushDeviceId", clientid);
        }
        voiceMap.put("client", Constants.REQUEST_CLIENT_APP);
        ServerClient.newInstance(MyApplication.getContext()).getuiPushVoiceList(MyApplication.getContext(), Constants.GETUI_PUSH_VOICE_LIST_TAG, voiceMap);
    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRecyclerViewRate.setLayoutManager(linearLayoutManager);
        mRecyclerViewRate.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new HuabeiRateAdapter(MyApplication.getContext(), mRateData, mHbFqNum);
        mRecyclerViewRate.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new HuabeiRateAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mHbFqNum = mRateData.get(position).getHbfqNum();
                isRateFree = mRateData.get(position).isFreeFee();
                setRecyclerView();
            }
        });
    }

    private void setKeyboardView(final boolean show) {
        Animation rotate;
        if (show) {
            rotate = AnimationUtils.loadAnimation(MyApplication.getContext(), R.anim.bottom_dialog_enter);
        } else {
            rotate = AnimationUtils.loadAnimation(MyApplication.getContext(), R.anim.bottom_dialog_exit);
        }
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(false);
        mLlKeyboardView.startAnimation(rotate);
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (show) {
                    mLlKeyboardView.setVisibility(View.VISIBLE);
                    mBtnPay.setVisibility(View.GONE);
                    objectAnimator.start();
                    mViewCursor.setVisibility(View.VISIBLE);
                } else {
                    mLlKeyboardView.setVisibility(View.GONE);
                    mBtnPay.setVisibility(View.VISIBLE);
                    objectAnimator.cancel();
                    mViewCursor.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void requestHbfqDetail() {
        if (mCbHbfq.isChecked()) {
            if (System.currentTimeMillis() - lastInputTimeMillis > 500) {
                getHbfqRateList(totalMoney);
            }
            lastInputTimeMillis = System.currentTimeMillis();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLlKeyboardView.clearAnimation();
    }

    private void setData(List<HbfqRateBean.DataBean> rateData) {
        int hbfqNum = 0;
        if (rateData != null && rateData.size() > 0) {
            for (int i = 0; i < rateData.size(); i++) {
                if (mHbFqNum.equals(rateData.get(i).getHbfqNum())) {
                    return;
                }
                if (i == 0) {
                    hbfqNum = Integer.valueOf(rateData.get(i).getHbfqNum());
                }
                if (rateData.get(i).isFreeFee() && (hbfqNum < Integer.valueOf(rateData.get(i).getHbfqNum()))) {
                    hbfqNum = Integer.valueOf(rateData.get(i).getHbfqNum());
                }
            }
        }
        mHbFqNum = String.valueOf(hbfqNum);
    }

    private boolean isRateFree(List<HbfqRateBean.DataBean> rateData) {
        boolean isRateFree = false;
        if (rateData != null && rateData.size() > 0) {
            for (int i = 0; i < rateData.size(); i++) {
                if (mHbFqNum.equals(rateData.get(i).getHbfqNum())) {
                    isRateFree = rateData.get(i).isFreeFee();
                    return isRateFree;
                }
            }
        }
        return isRateFree;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            StatusBarUtil.setImmersiveStatusBar(getActivity(), true);
        }
    }

    private void syncOrderState() {
        LogUtil.d("Jeremy---333");
        if (count < 5) {
            timeCount = 5;
            count++;
            showDialog = new NoticeDialog(getActivity(), dialogMessage, "取消同步", R.layout.notice_dialog_check_order);
            showDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    cancelTask = true;
                    toSyncOrderStatus();
                    mHandler.removeCallbacks(myRunnable);
                }
            });
            DialogHelper.resize(getActivity(), showDialog);
            showDialog.show();
            mHandler.post(myRunnable);
        } // 3次5秒，5次7秒查询后，如果还未返回成功，调用冲正接口
        else {
            cancelTask = true;
            toSyncOrderStatus();
        }
    }

    private void toSyncOrderStatus() {
        if (!NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            MyToast.showToastShort(getString(R.string.network_exception));
            if (cancelTask) {
                showSyncResult();
            } else {
                syncOrderState();
            }
        } else {
            if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                payYinshangNotify(mYinshangPosBean);
            } else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.MIBAGPAY_LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
                payNotify(mResultData);
            }
        }
    }

    private void showSyncResult() {
        mSyncDialog = new SelectDialog(getActivity(), "订单状态同步失败", "取消", "继续同步", R.layout.select_common_dialog);
        mSyncDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                cancelTask = true;
                toSyncOrderStatus();
            }
        });
        DialogHelper.resize(getActivity(), mSyncDialog, 0.9f);
        mSyncDialog.show();
    }

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && showDialog != null) {
                showDialog.setMessage(getString(R.string.dialog_order_sync) + timeCount
                        + getString(R.string.dialog_sync_start) + (count) + getString(R.string.dialog_sync_end));
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (showDialog != null && showDialog.isShowing()) {
                    showDialog.dismiss();
                }
                if (count == 5)
                    cancelTask = true;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toSyncOrderStatus();
                    }
                });
            }
        }
    };

    private void remove() {
        if (showDialog != null) {
            showDialog.dismiss();
        }
        if (mSyncDialog != null) {
            mSyncDialog.dismiss();
        }
        mHandler.removeCallbacks(myRunnable);
    }

    private void queryOrder(String tag) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            if ("TAG_AUTO_ORDER_DETAIL".equals(tag)) {
                map.put("orderNo", SpStayUtil.getString(MyApplication.getContext(), "BCARD_ORDER_NO"));
            } else {
                if (!StringUtils.isEmptyOrNull(posOrderBean.getOrderNo())) {
                    map.put("orderNo", posOrderBean.getOrderNo());
                }
            }
            ServerClient.newInstance(MyApplication.getContext()).getOrderDetail(MyApplication.getContext(), tag, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_coupon_maoku:
                if (isChecked) {
                    mInterProviderType = mMallCouponType;
                    mCbMeituan.setChecked(false);
                }
                break;
            case R.id.cb_coupon_meituan:
                if (isChecked) {
                    mInterProviderType = "1";
                    mCbMaoku.setChecked(false);
                }
                break;
        }
        if (hasChecked(mCbMaoku, mCbMeituan)) {//印享星，不支持扫码查券
            if ("2".equals(mInterProviderType)) {
                mIvVankeCoupon.setAlpha(0.5f);
                mIvVankeCoupon.setEnabled(false);
            } else {
                mIvVankeCoupon.setAlpha(1f);
                mIvVankeCoupon.setEnabled(true);
            }
        } else {
            mInterProviderType = "-1";
            mIvVankeCoupon.setAlpha(0.5f);
            mIvVankeCoupon.setEnabled(false);
        }
    }

    private boolean hasChecked(CheckBox... cbs) {
        for (CheckBox cb : cbs) {
            if (cb.isChecked())
                return true;
        }
        return false;
    }

    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_RECEIVE_RESET_DATA);
        mResetBroadcastReceiver = new ResetBroadcastReceiver();
        getActivity().registerReceiver(mResetBroadcastReceiver, intentFilter);
    }

    class ResetBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.ACTION_RECEIVE_RESET_DATA.equals(action)) {
                reset();
            }
        }
    }

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(mResetBroadcastReceiver);
        super.onDestroyView();
    }
}
