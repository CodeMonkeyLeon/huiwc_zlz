package com.hstypay.enterprise.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.draw.MyMarkerView;
import com.hstypay.enterprise.Widget.pie.BasePieView;
import com.hstypay.enterprise.Widget.pie.PieDataView;
import com.hstypay.enterprise.Widget.pie.PieView1;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.ShopTypeActivity;
import com.hstypay.enterprise.activity.reportDate.CalendarSinglePickActivity;
import com.hstypay.enterprise.activity.reportDate.ChoiceMonthActivity;
import com.hstypay.enterprise.activity.reportDate.ChoiceWeekActivity;
import com.hstypay.enterprise.adapter.ReportDataAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.ReportData;
import com.hstypay.enterprise.bean.ReportDataView;
import com.hstypay.enterprise.bean.ReportPercentBean;
import com.hstypay.enterprise.bean.ReportSelectDayBean;
import com.hstypay.enterprise.bean.ReportSelectMonthBean;
import com.hstypay.enterprise.bean.ReportSelectWeekBean;
import com.hstypay.enterprise.bean.ReportTradeTrendBean;
import com.hstypay.enterprise.bean.SelectDateBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.TrendBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.zng.common.utils.Logger;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xclcharts.common.DensityUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:15
 * @描述: ${TODO}
 */

public class ReportDataFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    private View mView;
    private RadioGroup mRgTitle, mRgTrend, mRgTrade, mRgDevice;
    private RadioButton mRbDay, mRbWeek, mRbMonth, mRbTrendMoney, mRbTrendCount, mRbTradeMoney, mRbTradeCount, mRbDeviceMoney, mRbDeviceCount;
    private ImageView mIvLineDay, mIvLineWeek, mIvLineMonth, mIvTradeTrend, mIvArrow, mIvInstruction;
    private TextView mTvDate, mTvStore, mTvMoney, mTvTradeTrendLast, mTvCompare, mTvTradeTrendDate, mTvSelectTrendDate, mTvSelectTrendMoney, mTvTradeNull, mTvDeviceNull;
    private LinearLayout mLlTradeTrend, mLlDate, mLlStore, mLlTradePercent, mLlDevicePercent, mLlBook;
    private RecyclerView mRecyclerView;
    private PieView1 mPvTrade, mPvDevice;
    private PieDataView mPvTradeData, mPvDeviceData;
    private List<ReportData> mList;
    private ReportDataAdapter mAdapter;
    private Animation rotate;
    private boolean mSwitchArrow = false;
    private SafeDialog mLoadDialog;
    private String startTime;
    private String endTime;
    private int mParentPos = -1;
    private int mChildPos = -1;
    private String storeMerchantId = "";

    private int color[] = new int[]{Color.rgb(66, 164, 231), Color.rgb(86, 123, 231),
            Color.rgb(70, 236, 249), Color.rgb(250, 203, 70), Color.rgb(251, 128, 88),
            Color.rgb(161, 190, 36), Color.rgb(250, 50, 50), Color.rgb(180, 108, 56),
            Color.rgb(190, 240, 140), Color.rgb(100, 160, 135)};
    private ReportDataView.DataBean mReportDataView;
    private ReportPercentBean mPayPercent;
    private ReportPercentBean mDevicePercent;
    private int mDateType;
    private int mTrendDateType;
    private int mPayPercentType;
    private int mDevicePercentType;
    private String mShopType;
    private ReportTradeTrendBean mReportTradeTrendBean;
    private List<TrendBean> mTrendList;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_report_data, container, false);
        StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        initView(mView);
        initEvent();
//        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
        initData();
//        }
        return mView;
    }

    public void initView(View view) {
        mLoadDialog = ((BaseActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mRgTitle = view.findViewById(R.id.rg_title);
        mRbDay = view.findViewById(R.id.rb_day);
        mRbWeek = view.findViewById(R.id.rb_week);
        mRbMonth = view.findViewById(R.id.rb_month);

        mIvLineDay = view.findViewById(R.id.iv_line_day);
        mIvLineWeek = view.findViewById(R.id.iv_line_week);
        mIvLineMonth = view.findViewById(R.id.iv_line_month);

        mLlDate = view.findViewById(R.id.ll_date);
        mTvDate = view.findViewById(R.id.tv_date);
        mLlStore = view.findViewById(R.id.ll_store);
        mTvStore = view.findViewById(R.id.tv_store);

        mTvMoney = view.findViewById(R.id.tv_money);
        mLlBook = view.findViewById(R.id.ll_book);
        mIvInstruction = view.findViewById(R.id.iv_instruction);
        mLlTradeTrend = view.findViewById(R.id.ll_trade_trend);
        mTvCompare = view.findViewById(R.id.tv_compare);
        mTvTradeTrendLast = view.findViewById(R.id.tv_trade_trend_last);
        mIvTradeTrend = view.findViewById(R.id.iv_trade_trend);

        mRecyclerView = view.findViewById(R.id.recyclerView);
        mIvArrow = view.findViewById(R.id.iv_arrow);

        mTvTradeTrendDate = view.findViewById(R.id.tv_trade_trend_date);
        mTvSelectTrendDate = view.findViewById(R.id.tv_select_trend_date);
        mTvSelectTrendMoney = view.findViewById(R.id.tv_select_trend_money);
        mRgTrend = view.findViewById(R.id.rg_trend);
        mRbTrendMoney = view.findViewById(R.id.rb_trend_money);
        mRbTrendCount = view.findViewById(R.id.rb_trend_count);
//        mLineChart = mView.findViewById(R.id.line_chart);

        mRgTrade = view.findViewById(R.id.rg_trade);
        mRbTradeMoney = view.findViewById(R.id.rb_trade_money);
        mRbTradeCount = view.findViewById(R.id.rb_trade_count);
        mPvTrade = view.findViewById(R.id.pv_trade);
        mPvTradeData = view.findViewById(R.id.pv_trade_data);
        mTvTradeNull = view.findViewById(R.id.tv_trade_null);
        mLlTradePercent = view.findViewById(R.id.ll_trade_percent);

        mRgDevice = view.findViewById(R.id.rg_device);
        mRbDeviceMoney = view.findViewById(R.id.rb_device_money);
        mRbDeviceCount = view.findViewById(R.id.rb_device_count);
        mPvDevice = view.findViewById(R.id.pv_device);
        mPvDeviceData = view.findViewById(R.id.pv_device_data);
        mTvDeviceNull = view.findViewById(R.id.tv_device_null);
        mLlDevicePercent = view.findViewById(R.id.ll_device_percent);

        mTvCompare.setText(R.string.tv_compare_yesterday);
        mLlBook.setVisibility(ConfigUtil.getMiniEnable() ? View.VISIBLE : View.GONE);
    }

    private void initEvent() {
        mLlDate.setOnClickListener(this);
        mLlStore.setOnClickListener(this);
        mIvArrow.setOnClickListener(this);
        mIvInstruction.setOnClickListener(this);
        mLlBook.setOnClickListener(this);

        mRgTitle.setOnCheckedChangeListener(this);
        mRgTrend.setOnCheckedChangeListener(this);
        mRgTrade.setOnCheckedChangeListener(this);
        mRgDevice.setOnCheckedChangeListener(this);
    }

    public void initData() {
        color[0] = getResources().getColor(R.color.theme_color);
        startTime = DateUtil.getDate();
        endTime = DateUtil.getDate();
        mTvDate.setText(DateUtil.getDate());
        mList = new ArrayList<>();
        mAdapter = new ReportDataAdapter((BaseActivity) getActivity(), mList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListener(new ReportDataAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int index) {
                switch (index) {
                    case 4:
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "当前周期的退款成功的金额总计");
                        break;
                    case 6:

                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "交易净额 = 实收金额-退款金额；交易净额不是银行划账到商户银行卡金额，银行划账到商户银行卡金额见结算金额");

                        break;
                    case 7:
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "含商家配置的红包活动，优惠券活动，会员折扣等营销活动");
                        break;
                    case 8:
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "实收金额 = 交易金额 - 优惠金额");
                        break;
                    case 9:
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "手续费是支付通道手续费，查询的实时数据仅供参考，以次日11:00后的数据为准。服务费是除手续外的其他费用（如企业饭卡服务费、花呗分期手续费等）");
                        break;
                }
            }
        });

        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                getTradeStatistics();
                getHourTradeStatistics();
                getTradeStatisticsByProvider();
                getTradeStatisticsByTermType();
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    private void setLineChart(ReportTradeTrendBean reportTradeTrendBean) {
        mTrendList = new ArrayList<>();
        if (mDateType == 1) {//周报
            if (reportTradeTrendBean.getData() != null && reportTradeTrendBean.getData().size() > 0) {
                List<ReportTradeTrendBean.DataBean> data = reportTradeTrendBean.getData();
                for (int i = 0; i < data.size(); i++) {
                    TrendBean trendBean = new TrendBean();
                    trendBean.setSuccessCount(data.get(i).getSuccessCount());
                    trendBean.setSuccessFee(data.get(i).getSuccessFee());
                    trendBean.setxMsg(DateUtil.formartDate(data.get(i).getPayTradeTime(), "yyyy-MM-dd", "MM.dd"));
                    LogUtil.d("aaaaa===" + DateUtil.formartDate(data.get(i).getPayTradeTime(), "yyyy-MM-dd", "MM.dd"));
                    mTrendList.add(trendBean);
                }
            }
        } else if (mDateType == 2) {//月报
            if (reportTradeTrendBean.getData() != null && reportTradeTrendBean.getData().size() > 0) {
                List<ReportTradeTrendBean.DataBean> data = reportTradeTrendBean.getData();
                for (int i = 0; i < data.size(); i++) {
                    TrendBean trendBean = new TrendBean();
                    trendBean.setSuccessCount(data.get(i).getSuccessCount());
                    trendBean.setSuccessFee(data.get(i).getSuccessFee());
                    trendBean.setxMsg(DateUtil.formartDate(data.get(i).getPayTradeTime(), "yyyy-MM-dd", "MM.dd"));
                    mTrendList.add(trendBean);
                }
            }
        } else {
            for (int i = 0; i < 24; i++) {
                TrendBean trendBean = new TrendBean();
                trendBean.setSuccessCount(0);
                trendBean.setSuccessFee(0);
                trendBean.setxMsg(i + "h");
                mTrendList.add(trendBean);
            }
            if (reportTradeTrendBean.getData() != null && reportTradeTrendBean.getData().size() > 0) {
                List<ReportTradeTrendBean.DataBean> data = reportTradeTrendBean.getData();
                for (int i = 0; i < data.size(); i++) {
                    Integer indexOf = -1;
                    try {
                        indexOf = Integer.valueOf(data.get(i).getTradeHour());
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    if (indexOf > -1 && indexOf < 24) {
                        mTrendList.get(indexOf).setSuccessFee(data.get(i).getSuccessFee());
                        mTrendList.get(indexOf).setSuccessCount(data.get(i).getSuccessCount());
                    }
                }
            }
        }
        createLineChartOne(mTrendList);
    }

    void createLineChartOne(List<TrendBean> infoList) {
        LinearLayout lay_line1 = mView.findViewById(R.id.lay_line1);
        boolean isMoney;
        if (mTrendDateType == 1) {
            isMoney = false;
            mTvSelectTrendMoney.setText(DateUtil.formatCountByLong(infoList.get(0).getSuccessCount()) + "笔");
        } else {
            isMoney = true;
            mTvSelectTrendMoney.setText(DateUtil.formatMoneyByLong(infoList.get(0).getSuccessFee()) + "元");
        }
        mTvSelectTrendDate.setText(infoList.get(0).getxMsg());

        lay_line1.removeAllViews();
        LineChart lineChart = new LineChart(getActivity());
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListenerImp());
        setLineChart(lineChart, isMoney, infoList);
        lay_line1.addView(lineChart);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) lineChart.getLayoutParams();
        params.width = DensityUtil.dip2px(getActivity(), 300);
        params.height = DensityUtil.dip2px(getActivity(), 240);
    }

    class OnChartValueSelectedListenerImp implements OnChartValueSelectedListener {
        @Override
        public void onNothingSelected() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            float vale = e.getVal();

            if (mTrendDateType == 1) {
                float value = vale;
                mTvSelectTrendMoney.setText(value + "笔");
            } else {
                mTvSelectTrendMoney.setText(DateUtil.formatMoneyUtils(BigDecimal.valueOf(vale).longValue()) + "元");
            }
            mTvSelectTrendDate.setText(mTrendList.get(e.getXIndex()).getxMsg());
//                mTvSelectTrendDate.setText(DateUtil.getWholeMonthDays(startTime).get((e.getXIndex())));
        }
    }

    private void setLineChart(LineChart mChart, final boolean isMoney, final List<TrendBean> infoList) {

        mChart.setDrawGridBackground(false);
        mChart.setHighlightEnabled(false);
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(true);
        mChart.setPinchZoom(false);
        mChart.setBackgroundColor(Color.WHITE);
        mChart.setClickable(false);
        //设置边框
        mChart.setDrawBorders(false);
        mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_view);
        mChart.setMarkerView(mv);
        mChart.setDescription("");
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(1);
        xAxis.setAxisLineColor(Color.parseColor("#cccccc"));
        xAxis.setAxisLineWidth(1);
        xAxis.setTextSize(UIUtils.dp2px(2.5f));
        xAxis.setTextColor(R.color.home_text);
        final List<String> thirtyDays = DateUtil.getPartMonthDays(startTime);
        final List<String> partHour = DateUtil.getPartHour();
        if (mDateType == 1) {
            xAxis.setValueFormatter(new XAxisValueFormatter() {
                @Override
                public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
//                    original = infoList.get(index).getxMsg();
                    return infoList.get(index).getxMsg();
                }
            });
        } else if (mDateType == 2) {
            xAxis.setValueFormatter(new XAxisValueFormatter() {

                @Override
                public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                    String xValue = "";
                    switch (Integer.parseInt(original)) {
                        case 1://
                            xValue = thirtyDays.get(0);
                            break;
                        case 5://
                            xValue = thirtyDays.get(1);
                            break;
                        case 10://
                            xValue = thirtyDays.get(2);
                            break;
                        case 15://
                            xValue = thirtyDays.get(3);
                            break;
                        case 20://
                            xValue = thirtyDays.get(4);
                            break;
                        case 25://
                            xValue = thirtyDays.get(5);
                            break;
                        case 30://
                            xValue = thirtyDays.get(6);
                            break;
                        default:
                            xValue = "";
                            break;

                    }
                    return xValue;
                }
            });
        } else {
            xAxis.setValueFormatter(new XAxisValueFormatter() {

                @Override
                public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                    String xValue = "";
                    switch (index) {
                        case 0://
                            xValue = partHour.get(0);
                            break;
                        case 4://
                            xValue = partHour.get(1);
                            break;
                        case 8://
                            xValue = partHour.get(2);
                            break;
                        case 12://
                            xValue = partHour.get(3);
                            break;
                        case 16://
                            xValue = partHour.get(4);
                            break;
                        case 20://
                            xValue = partHour.get(5);
                            break;
                        default:
                            xValue = "";
                            break;

                    }
                    return xValue;
                }
            });
        }

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines();

        leftAxis.setAxisMinValue(0f);
        leftAxis.setTextColor(R.color.home_text);
        leftAxis.setLabelCount(6, true);
        leftAxis.setTextSize(UIUtils.dp2px(3));
        leftAxis.setGridColor(Color.parseColor("#cccccc"));
        //        leftAxis.setGridLineWidth(1);
        if (isMoney) {
            leftAxis.setValueFormatter(new YAxisValueFormatter() {

                @Override
                public String getFormattedValue(float value, YAxis yAxis) {
                    //                    String marValue = DateUtil.formatMoneyUtils(value);
                    if (value == 0f) {
                        return "";
                    }
                    double farVale = value / 100;
                    Logger.i("hehui", "farVale-->" + farVale);
                    return formatDouble1(farVale) + "";
                }
            });
        }
        leftAxis.setStartAtZero(false);
        leftAxis.setDrawAxisLine(true);
        leftAxis.setAxisLineColor(Color.parseColor("#cccccc"));
        leftAxis.setAxisLineWidth(1);
        mChart.getLegend().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);

        setData(infoList.size(), infoList, mChart, leftAxis, isMoney);

        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
    }

    /**
     * @param d
     * @return
     */
    public double formatDouble1(double d) {
        return (double) Math.round(d * 100) / 100;
    }

    private void setData(int count, List<TrendBean> infoList, LineChart mChart, YAxis leftAxis, boolean isMoney) {
        List<Long> maxVal = new ArrayList<Long>();
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < count; i++) {
            xVals.add((i) + "");
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < count; i++) {
            if (isMoney) {
                maxVal.add(infoList.get(i).getSuccessFee());
                yVals.add(new Entry((infoList.get(i).getSuccessFee()), i));
            } else {
                maxVal.add(infoList.get(i).getSuccessCount());
                yVals.add(new Entry((infoList.get(i).getSuccessCount()), i));
            }
        }
        long max = Collections.max(maxVal);
        if (max > 0) {
            //            if(max >0 && max < 50){
            //                leftAxis.setAxisMaxValue(50);
            //            }else if (max >=50 && max <100) {
            //                leftAxis.setAxisMaxValue(100);
            //            }else if (max >=100 && && max <100) {
            //
            //            }
            if (max > 1000) {
                leftAxis.setAxisMaxValue(max + 1000f);
            } else {
                leftAxis.setAxisMaxValue(max);
            }
        } else {
            leftAxis.setAxisMaxValue(0);
        }
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "str");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        //        set1.enableDashedLine(10f, 5f, 0f);
        //        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(getResources().getColor(R.color.line_data_set_fill_color));
//        set1.setColor(Color.parseColor("#4182E2"));
        set1.setCircleColor(Color.RED);
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setFillAlpha(45);
        set1.setFillColor(getResources().getColor(R.color.line_data_set_fill_color));
//        set1.setFillColor(Color.parseColor("#4182E2"));
        set1.setDrawFilled(true);
        set1.setDrawCubic(true);
        set1.setDrawCircles(false);
        set1.setHighLightColor(Color.parseColor("#ff9742"));
        set1.setHighlightLineWidth(1.5f);

        //        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);
        data.setDrawValues(false);
        // set data
        mChart.setData(data);
    }

    /**
     * @param reportPercent 数据源
     * @param percentType   金额/笔数
     */
    private void setPayPercentData(ReportPercentBean reportPercent, int percentType) {
        if (reportPercent.getData() != null && reportPercent.getData().size() > 0) {
            mPvTrade.setTitle("交易分布");
            mPvTrade.setDisplayDigits(2);
            mPvTradeData.setColumnNumber(1);
            mPvTrade.attachPieDataView(mPvTradeData);
            //数量
            int count = reportPercent.getData().size();
            float[] value = new float[count];
            String[] name = new String[count];
            int[] color = new int[count];

            for (int i = 0; i < count; i++) {
                if (percentType == 1) {
                    value[i] = reportPercent.getData().get(i).getSuccessCount();
                    name[i] = reportPercent.getData().get(i).getApiProviderCnt() + "  " + DateUtil.formatMoney(reportPercent.getData().get(i).getCountRelativePercentage()) + "%";
                } else {
                    value[i] = reportPercent.getData().get(i).getSuccessFee();
                    name[i] = reportPercent.getData().get(i).getApiProviderCnt() + "  " + DateUtil.formatMoney(reportPercent.getData().get(i).getAmountRelativePercentage()) + "%";
                }
                color[i] = this.color[i];
            }
            //设置
            mPvTrade.setAnimStyle(BasePieView.ANIM_STYLE_TRANSLATE);
            mPvTrade.setData(value, name, color, true);
            mTvTradeNull.setVisibility(View.GONE);
            mLlTradePercent.setVisibility(View.VISIBLE);
        } else {
            mTvTradeNull.setVisibility(View.VISIBLE);
            mLlTradePercent.setVisibility(View.GONE);
        }
    }

    /**
     * @param reportPercent 数据源
     * @param percentType   金额/笔数
     */
    private void setDevicePercentData(ReportPercentBean reportPercent, int percentType) {
        if (reportPercent.getData() != null && reportPercent.getData().size() > 0) {
            mPvDevice.setTitle("终端分布");
            mPvDevice.setDisplayDigits(2);
            mPvDeviceData.setColumnNumber(1);
            mPvDevice.attachPieDataView(mPvDeviceData);
            //数量
            int count = reportPercent.getData().size();
            float[] value = new float[count];
            String[] name = new String[count];
            int[] color = new int[count];

            for (int i = 0; i < count; i++) {
                if (percentType == 1) {
                    value[i] = reportPercent.getData().get(i).getSuccessCount();
                    name[i] = reportPercent.getData().get(i).getTermTypeCnt() + "  " + DateUtil.formatMoney(reportPercent.getData().get(i).getCountRelativePercentage()) + "%";
                } else {
                    value[i] = reportPercent.getData().get(i).getSuccessFee();
                    name[i] = reportPercent.getData().get(i).getTermTypeCnt() + "  " + DateUtil.formatMoney(reportPercent.getData().get(i).getAmountRelativePercentage()) + "%";
                }
                color[i] = this.color[i];
            }
            //设置
            mPvDevice.setAnimStyle(BasePieView.ANIM_STYLE_TRANSLATE);
            mPvDevice.setData(value, name, color, true);
            mTvDeviceNull.setVisibility(View.GONE);
            mLlDevicePercent.setVisibility(View.VISIBLE);
        } else {
            mTvDeviceNull.setVisibility(View.VISIBLE);
            mLlDevicePercent.setVisibility(View.GONE);
        }
    }

    private void getSelectMonthList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
//            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = getSelectDateMap();
            ServerClient.newInstance(MyApplication.getContext()).getSelectMonthList(MyApplication.getContext(), Constants.TAG_SELECT_MONTH_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getSelectDayList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
//            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = getSelectDateMap();
            ServerClient.newInstance(MyApplication.getContext()).getSelectDayList(MyApplication.getContext(), Constants.TAG_SELECT_DAY_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getSelectWeekList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
//            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = getSelectDateMap();
            ServerClient.newInstance(MyApplication.getContext()).getSelectWeekList(MyApplication.getContext(), Constants.TAG_SELECT_WEEK_LIST, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private Map<String, Object> getSelectDateMap() {
        Map<String, Object> map = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -6);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        calendar.set(year, month, 1);
        Calendar calendarNow = Calendar.getInstance();
        calendarNow.add(Calendar.DAY_OF_MONTH, -1);
        map.put("startTime", DateUtil.formatDate(calendar.getTime()));
        switch (mDateType) {
            case 0:
                map.put("endTime", DateUtil.getDate());
                break;
            case 1:
                map.put("endTime", DateUtil.getSundayOfThisWeek());
                break;
            case 2:
                map.put("endTime", DateUtil.getMonthLastDay());
                break;
        }
        if (MyApplication.getIsMerchant()) {
            map.put("merchantId", MyApplication.getMechantId());
        }
        if (MyApplication.getIsManager()) {
            map.put("shopManagerId", MyApplication.getEmpId());
        }
        if (MyApplication.getIsCasher()) {
            map.put("cashierNo", MyApplication.getEmpId());
        }
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeMerchantId", storeMerchantId);
        }
        if ("12".equals(MyApplication.getMerchantType()) && !TextUtils.isEmpty(mShopType)) {
            map.put("merchantType", mShopType);
        }
        return map;
    }

    private Map<String, Object> getDateMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (MyApplication.getIsMerchant()) {
            map.put("merchantId", MyApplication.getMechantId());
        }
        if (MyApplication.getIsManager()) {
            map.put("shopManagerId", MyApplication.getEmpId());
        }
        if (MyApplication.getIsCasher()) {
            map.put("cashierNo", MyApplication.getEmpId());
        }
        if ("12".equals(MyApplication.getMerchantType()) && !TextUtils.isEmpty(mShopType)) {
            map.put("merchantType", mShopType);
        }
        if (!TextUtils.isEmpty(storeMerchantId)) {
            map.put("storeMerchantId", storeMerchantId);
        }
        return map;
    }

    /**
     * 数据概览
     */
    private void getTradeStatistics() {
        Map<String, Object> map = getDateMap();
        ServerClient.newInstance(MyApplication.getContext()).getTradeStatistics(MyApplication.getContext(), Constants.TAG_TRADE_STATISTICS, map);
    }

    /**
     * 近七日交易趋势
     */
    private void getTradeStatisticsByDayTrendings() {
        Map<String, Object> map = getDateMap();
        ServerClient.newInstance(MyApplication.getContext()).getTradeStatisticsByDayTrendings(MyApplication.getContext(), Constants.TAG_DAY_TRADE_TREND, map);
    }

    /**
     * 单日交易趋势
     */
    private void getHourTradeStatistics() {
        Map<String, Object> map = getDateMap();
        ServerClient.newInstance(MyApplication.getContext()).getHourTradeStatistics(MyApplication.getContext(), Constants.TAG_HOUR_TRADE_TREND, map);
    }

    /**
     * 支付占比
     */
    private void getTradeStatisticsByProvider() {
        Map<String, Object> map = getDateMap();
        ServerClient.newInstance(MyApplication.getContext()).getTradeStatisticsByProvider(MyApplication.getContext(), Constants.TAG_PAY_PERCENT, map);
    }

    /**
     * 终端占比
     */
    private void getTradeStatisticsByTermType() {
        Map<String, Object> map = getDateMap();
        ServerClient.newInstance(MyApplication.getContext()).getTradeStatisticsByTermType(MyApplication.getContext(), Constants.TAG_DEVICE_PERCENT, map);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SELECT_MONTH) {
            Bundle extras = data.getExtras();
            SelectDateBean selectDateBean = (SelectDateBean) extras.getSerializable(Constants.RESULT_SELECT_DATE);
            if (selectDateBean != null) {
                startTime = selectDateBean.getStartTime();
                endTime = selectDateBean.getEndTime();
                mParentPos = selectDateBean.getParentPos();
                mChildPos = selectDateBean.getChildPos();
            } else {
                startTime = DateUtil.getLastMonth()[0];
                endTime = DateUtil.getLastMonth()[1];
                mParentPos = -1;
                mChildPos = -1;
            }
            mTvDate.setText(DateUtil.formartDate(startTime, "yyyy-MM-dd", "yyyy-MM"));
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                getTradeStatistics();
                getTradeStatisticsByDayTrendings();
                getTradeStatisticsByProvider();
                getTradeStatisticsByTermType();
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SELECT_WEEK) {
            Bundle extras = data.getExtras();
            SelectDateBean selectDateBean = (SelectDateBean) extras.getSerializable(Constants.RESULT_SELECT_DATE);
            if (selectDateBean != null) {
                startTime = selectDateBean.getStartTime();
                endTime = selectDateBean.getEndTime();
                mParentPos = selectDateBean.getParentPos();
                mChildPos = selectDateBean.getChildPos();
            } else {
                startTime = DateUtil.getLastWeek()[0];
                endTime = DateUtil.getLastWeek()[1];
                mParentPos = -1;
                mChildPos = -1;
            }
            mTvDate.setText(DateUtil.formartDate(startTime, "yyyy-MM-dd", "MM.dd")
                    + "~" + DateUtil.formartDate(endTime, "yyyy-MM-dd", "MM.dd"));
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                getTradeStatistics();
                getTradeStatisticsByDayTrendings();
                getTradeStatisticsByProvider();
                getTradeStatisticsByTermType();
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SELECT_DAY) {
            Bundle extras = data.getExtras();
            SelectDateBean selectDateBean = (SelectDateBean) extras.getSerializable(Constants.RESULT_SELECT_DATE);
            if (selectDateBean != null) {
                startTime = selectDateBean.getStartTime();
                endTime = selectDateBean.getEndTime();
                mParentPos = selectDateBean.getParentPos();
                mChildPos = selectDateBean.getChildPos();
            } else {
                startTime = DateUtil.getBeforeDate();
                endTime = DateUtil.getBeforeDate();
                mParentPos = -1;
                mChildPos = -1;
            }
            mTvDate.setText(startTime);
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                getTradeStatistics();
                getHourTradeStatistics();
                getTradeStatisticsByProvider();
                getTradeStatisticsByTermType();
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mShopType = extras.getString(Constants.INTENT_STORE_DATA_TYPE);
            mTvStore.setText(shopBean.getStoreName());
            storeMerchantId = shopBean.getStoreId();
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                getTradeStatistics();
                getTradeStatisticsByDayTrendings();
                getTradeStatisticsByProvider();
                getTradeStatisticsByTermType();
            } else {
                MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_TRADE_STATISTICS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportDataView msg = (ReportDataView) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mReportDataView = msg.getData();
                    if (mReportDataView != null) {
                        mTvMoney.setText(UIUtils.getString(R.string.tx_mark) + DateUtil.formatMoneyByLong(mReportDataView.getSettlementFee()));
                        if (mReportDataView.getSettlementFeeLinkRelative() > 0) {
                            mTvTradeTrendLast.setText(DateUtil.formatMoney(Math.abs(mReportDataView.getSettlementFeeLinkRelative())) + "%");
                            mTvTradeTrendLast.setTextColor(UIUtils.getColor(R.color.tv_status_refunding));
                            mIvTradeTrend.setImageResource(R.mipmap.icon_data_up);
                            mLlTradeTrend.setBackgroundResource(R.drawable.shape_data_up_bg);
                            mIvTradeTrend.setVisibility(View.VISIBLE);
                        } else if (mReportDataView.getSettlementFeeLinkRelative() < 0) {
                            mTvTradeTrendLast.setText(DateUtil.formatMoney(Math.abs(mReportDataView.getSettlementFeeLinkRelative())) + "%");
                            mTvTradeTrendLast.setTextColor(UIUtils.getColor(R.color.tv_data_down));
                            mIvTradeTrend.setImageResource(R.mipmap.icon_data_down);
                            mLlTradeTrend.setBackgroundResource(R.drawable.shape_data_down_bg);
                            mIvTradeTrend.setVisibility(View.VISIBLE);
                        } else {
                            mTvTradeTrendLast.setText("--");
                            mTvTradeTrendLast.setTextColor(UIUtils.getColor(R.color.home_value_text));
                            mLlTradeTrend.setBackgroundResource(R.color.translucent);
                            mIvTradeTrend.setVisibility(View.GONE);
                        }
                        closeArrow();
                        mIvArrow.setVisibility(View.VISIBLE);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mIvArrow.setVisibility(View.INVISIBLE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_PAY_PERCENT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportPercentBean msg = (ReportPercentBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mPayPercent = msg;
                    setPayPercentData(mPayPercent, mPayPercentType);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_DEVICE_PERCENT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportPercentBean msg = (ReportPercentBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mDevicePercent = msg;
                    setDevicePercentData(mDevicePercent, mDevicePercentType);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_SELECT_DAY_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportSelectDayBean msg = (ReportSelectDayBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    Intent intent = new Intent(getActivity(), CalendarSinglePickActivity.class);
                    intent.putExtra(Constants.INTENT_REPORT_DAY_DATA, msg);
                    intent.putExtra(Constants.INTENT_PARENT_POSITION, mParentPos);
                    intent.putExtra(Constants.INTENT_CHILD_POSITION, mChildPos);
                    startActivityForResult(intent, Constants.REQUEST_SELECT_DAY);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_SELECT_MONTH_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportSelectMonthBean msg = (ReportSelectMonthBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    Intent intent = new Intent(getActivity(), ChoiceMonthActivity.class);
                    intent.putExtra(Constants.INTENT_REPORT_MONTH_DATA, msg);
                    intent.putExtra(Constants.INTENT_PARENT_POSITION, mParentPos);
                    intent.putExtra(Constants.INTENT_CHILD_POSITION, mChildPos);
                    startActivityForResult(intent, Constants.REQUEST_SELECT_MONTH);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_SELECT_WEEK_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportSelectWeekBean msg = (ReportSelectWeekBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    Intent intent = new Intent(getActivity(), ChoiceWeekActivity.class);
                    intent.putExtra(Constants.INTENT_REPORT_WEEK_DATA, msg);
                    intent.putExtra(Constants.INTENT_PARENT_POSITION, mParentPos);
                    intent.putExtra(Constants.INTENT_CHILD_POSITION, mChildPos);
                    startActivityForResult(intent, Constants.REQUEST_SELECT_WEEK);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_DAY_TRADE_TREND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportTradeTrendBean msg = (ReportTradeTrendBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mReportTradeTrendBean = msg;
                    setLineChart(mReportTradeTrendBean);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_HOUR_TRADE_TREND)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportTradeTrendBean msg = (ReportTradeTrendBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mReportTradeTrendBean = msg;
                    setLineChart(mReportTradeTrendBean);
                    break;
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
            return;
        }
        switch (checkedId) {
            case R.id.rb_day:
//                mTvTradeTrendDate.setText(R.string.tv_trend_seven_days);
                mTvDate.setText(DateUtil.getDate());
                mIvLineDay.setVisibility(View.VISIBLE);
                mIvLineWeek.setVisibility(View.INVISIBLE);
                mIvLineMonth.setVisibility(View.INVISIBLE);
                startTime = DateUtil.getDate();
                endTime = DateUtil.getDate();
                mTvCompare.setText(R.string.tv_compare_yesterday);
                mDateType = 0;
                mParentPos = -1;
                mChildPos = -1;
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getTradeStatistics();
                    getHourTradeStatistics();
                    getTradeStatisticsByProvider();
                    getTradeStatisticsByTermType();
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.rb_week:
//                mTvTradeTrendDate.setText(R.string.tv_trend_week);
                mIvLineDay.setVisibility(View.INVISIBLE);
                mIvLineWeek.setVisibility(View.VISIBLE);
                mIvLineMonth.setVisibility(View.INVISIBLE);
                startTime = DateUtil.getMondayOfThisWeek();
                endTime = DateUtil.getSundayOfThisWeek();
//                startTime = DateUtil.getLastWeek()[0];
//                endTime = DateUtil.getLastWeek()[1];
                mTvDate.setText(DateUtil.formartDate(startTime, "yyyy-MM-dd", "MM.dd")
                        + "~" + DateUtil.formartDate(endTime, "yyyy-MM-dd", "MM.dd"));
                mTvCompare.setText(R.string.tv_compare_week);
                mDateType = 1;
                mParentPos = -1;
                mChildPos = -1;
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getTradeStatistics();
                    getTradeStatisticsByDayTrendings();
                    getTradeStatisticsByProvider();
                    getTradeStatisticsByTermType();
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.rb_month:
//                mTvTradeTrendDate.setText(R.string.tv_trend_month);
                mIvLineDay.setVisibility(View.INVISIBLE);
                mIvLineWeek.setVisibility(View.INVISIBLE);
                mIvLineMonth.setVisibility(View.VISIBLE);
                startTime = DateUtil.getMonthFirstDay();
                endTime = DateUtil.getMonthLastDay();

//                startTime = DateUtil.getLastMonth()[0];
//                endTime = DateUtil.getLastMonth()[1];

                mTvDate.setText(DateUtil.formartDate(startTime, "yyyy-MM-dd", "yyyy-MM"));
                mTvCompare.setText(R.string.tv_compare_month);
                mDateType = 2;
                mParentPos = -1;
                mChildPos = -1;
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getTradeStatistics();
                    getTradeStatisticsByDayTrendings();
                    getTradeStatisticsByProvider();
                    getTradeStatisticsByTermType();
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.rb_trend_money:
                mTrendDateType = 0;
                setLineChart(mReportTradeTrendBean);
                break;
            case R.id.rb_trend_count:
                mTrendDateType = 1;
                setLineChart(mReportTradeTrendBean);
                break;
            case R.id.rb_trade_money:
                mPayPercentType = 0;
                setPayPercentData(mPayPercent, mPayPercentType);
                break;
            case R.id.rb_trade_count:
                mPayPercentType = 1;
                setPayPercentData(mPayPercent, mPayPercentType);
                break;
            case R.id.rb_device_money:
                mDevicePercentType = 0;
                setDevicePercentData(mDevicePercent, mDevicePercentType);
                break;
            case R.id.rb_device_count:
                mDevicePercentType = 1;
                setDevicePercentData(mDevicePercent, mDevicePercentType);
                break;
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        }
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            ((BaseActivity) getActivity()).openDialog();
            return;
        }
        switch (v.getId()) {
            case R.id.ll_date:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getDateData(mDateType);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
                break;
            case R.id.ll_store:
                Intent intentShop = new Intent(getActivity(), ShopTypeActivity.class);
                if (TextUtils.isEmpty(storeMerchantId)) {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                } else {
                    intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                }
                intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_BILL_SHOP);
                intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, mShopType);
                startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                break;
            case R.id.iv_arrow:
                if (mReportDataView != null) {
                    if (mSwitchArrow) {
                        closeArrow();
                    } else {
                        openArrow();
                    }
                }
                break;
            case R.id.iv_instruction:
                StringBuilder builder = new StringBuilder();
                builder.append("结算金额=实收金额-退款金额-手续费-服务费");
                builder.append("，查询到的实时数据仅供参考，以次日11:00后的数据为准");
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), "结算金额详细", builder.toString());
                break;
            case R.id.ll_book:
                toMiniProgram();
                break;
        }
    }

    private void toMiniProgram() {
        if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
            String appId = "wx6e411e48e4dacb00"; // 填应用AppId
            IWXAPI api = WXAPIFactory.createWXAPI(getActivity(), appId);
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = "gh_1d2ba02ad88f"; // 填小程序原始id
            req.path = "pages/my/my?userId=" + MyApplication.getUserId() + "&phone=" + MyApplication.getLoginPhone();
            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
            api.sendReq(req);
        } else {
            MyToast.showToastShort("请先安装微信！");
        }
    }

    private void getDateData(int dateType) {
        switch (dateType) {
            case 0:
                getSelectDayList();
                break;
            case 1:
                getSelectWeekList();
                break;
            case 2:
                getSelectMonthList();
                break;
        }
    }

    private void setListData(ReportDataView.DataBean reportDataView) {
        mList.clear();
        ReportData reportData1 = new ReportData();
        reportData1.setTitle("交易金额(元)");
        reportData1.setValue(DateUtil.formatMoneyByLong(reportDataView.getSuccessFee()));
        reportData1.setIndex(1);
        reportData1.setPercent(reportDataView.getSuccessFeeLinkRelative());
        mList.add(reportData1);
        ReportData reportData2 = new ReportData();
        reportData2.setTitle("交易笔数");
        reportData2.setValue(DateUtil.formatCountByLong(reportDataView.getSuccessCount()));
        reportData2.setIndex(2);
        reportData2.setPercent(reportDataView.getSuccessCountLinkRelative());
        mList.add(reportData2);
        ReportData reportData3 = new ReportData();
        reportData3.setTitle("笔单价(元)");
        reportData3.setValue(DateUtil.formatMoneyByLong(reportDataView.getEachSuccessFee()));
        reportData3.setIndex(3);
        reportData3.setPercent(reportDataView.getEachSuccessFeeLinkRelative());
        mList.add(reportData3);
        ReportData reportData7 = new ReportData();
        reportData7.setTitle("优惠金额(元)");
        reportData7.setValue(DateUtil.formatMoneyByLong(reportDataView.getMchDiscountsFee()));
        reportData7.setIndex(7);
        reportData7.setPercent(reportDataView.getMchDiscountsFeeLinkRelative());
        mList.add(reportData7);
        ReportData reportData8 = new ReportData();
        reportData8.setTitle("实收金额(元)");
        reportData8.setIndex(8);
        reportData8.setValue(DateUtil.formatMoneyByLong(reportDataView.getActualFee()));
        reportData8.setPercent(reportDataView.getActualFeeLinkRelative());
        mList.add(reportData8);
        ReportData reportData4 = new ReportData();
        reportData4.setTitle("退款金额(元)");
        reportData4.setValue(DateUtil.formatMoneyByLong(reportDataView.getRefundFee()));
        reportData4.setIndex(4);
        reportData4.setPercent(reportDataView.getRefundFeeLinkRelative());
        mList.add(reportData4);
        ReportData reportData5 = new ReportData();
        reportData5.setTitle("退款笔数");
        reportData5.setValue(DateUtil.formatCountByLong(reportDataView.getRefundCount()));
        reportData5.setIndex(5);
        reportData5.setPercent(reportDataView.getRefundCountLinkRelative());
        mList.add(reportData5);

        ReportData reportData6 = new ReportData();
//        reportData6.setTitle("结算金额(元)");
        reportData6.setTitle("交易净额(元)");
//        reportData6.setValue(DateUtil.formatMoneyByLong(reportDataView.getSettlementFee()));
        reportData6.setValue(DateUtil.formatMoneyByLong(reportDataView.getNetFee()));
        reportData6.setIndex(6);
//        reportData6.setPercent(reportDataView.getSettlementFeeLinkRelative());
        reportData6.setPercent(reportDataView.getNetFeeLinkRelative());
        mList.add(reportData6);

        ReportData reportData9 = new ReportData();
        if (MyApplication.isOpenFuncard()) {
            reportData9.setTitle(getActivity().getResources().getString(R.string.tx_report_fw_money));
            reportData9.setValue(DateUtil.formatMoneyByLong(reportDataView.getCommissionFee() + reportDataView.getFzFee()));
        } else {
            if (reportDataView.getFzFee() > 0) {
                reportData9.setTitle(getActivity().getResources().getString(R.string.tx_report_commission_fw_money));
                reportData9.setValue(DateUtil.formatMoneyByLong(reportDataView.getCommissionFee()) + "/" + DateUtil.formatMoneyByLong(reportDataView.getFzFee()));
            } else {
                reportData9.setTitle(getActivity().getResources().getString(R.string.tx_report_commission_money));
                reportData9.setValue(DateUtil.formatMoneyByLong(reportDataView.getCommissionFee()));
            }
        }

        reportData9.setIndex(9);
        reportData9.setPercent(reportDataView.getCommissionFeeLinkRelative());
        mList.add(reportData9);
    }

    private void setListDataHide(ReportDataView.DataBean reportDataView) {
        mList.clear();
        ReportData reportData1 = new ReportData();
        reportData1.setTitle("交易金额(元)");
        reportData1.setValue(DateUtil.formatMoneyByLong(reportDataView.getSuccessFee()));
        reportData1.setIndex(1);
        reportData1.setPercent(reportDataView.getSuccessFeeLinkRelative());
        mList.add(reportData1);
        ReportData reportData2 = new ReportData();
        reportData2.setTitle("交易笔数");
        reportData2.setValue(DateUtil.formatCountByLong(reportDataView.getSuccessCount()));
        reportData2.setIndex(2);
        reportData2.setPercent(reportDataView.getSuccessCountLinkRelative());
        mList.add(reportData2);
        ReportData reportData3 = new ReportData();
        reportData3.setTitle("笔单价(元)");
        reportData3.setValue(DateUtil.formatMoneyByLong(reportDataView.getEachSuccessFee()));
        reportData3.setIndex(3);
        reportData3.setPercent(reportDataView.getEachSuccessFeeLinkRelative());
        mList.add(reportData3);
    }

    public void closeArrow() {
        setListDataHide(mReportDataView);
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mIvArrow.startAnimation(rotate);
        mSwitchArrow = false;
        mAdapter.notifyDataSetChanged();
    }

    private void openArrow() {
        setListData(mReportDataView);
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mIvArrow.startAnimation(rotate);
        mSwitchArrow = true;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIvArrow.clearAnimation();
    }
}