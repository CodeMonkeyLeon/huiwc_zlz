package com.hstypay.enterprise.fragment;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.BIND_AUTO_CREATE;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.DateStartEndPopupWindow;
import com.hstypay.enterprise.Widget.EditTextDelete;
import com.hstypay.enterprise.Widget.FilterCashierPopupWindow;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.CashierActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.adapter.CashierRecyclerAdapter;
import com.hstypay.enterprise.adapter.CollectTypeAdapter;
import com.hstypay.enterprise.adapter.DataEntity;
import com.hstypay.enterprise.adapter.ShopRecyclerAdapter;
import com.hstypay.enterprise.adapter.paySite.PaySiteChoiceAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.CashierOfStoreBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.StoreListBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.bean.paySite.PaySiteBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.PosPrintUtil;
import com.hstypay.enterprise.utils.print.xdl.N900Device;
import com.hstypay.enterprise.utils.print.ybx.PrintSum;
import com.hstypay.enterprise.utils.print.yipos.PayServiceManager;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.AidlDeviceService;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.ums.upos.sdk.system.BaseSystemManager;
import com.zng.common.PrintUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.impl.WeiposImpl;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:09
 * @描述: ${TODO}
 */

public class ReportFragment extends BaseFragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private LinearLayout mLlDate, mLlShop, mLlSalesman, mLlSite, mLlReportPay, mShopPopLayout, mCashierPopLayout, mSitePopLayout, mLlCommissionFee;
    private TextView mTvTotal, mTvTotalPay, mTvTotalPayNum, mTvRealMoney, mTvPayCouponMoney, mTvTotalRefund, mTvTotalRefundNum, mTvNotice, mTvTitle;
    private TextView mTvVerifyActuralMoney, mTvVerifyCouponMoney, mTvVerifyRefund, mTvVerifyRefundNum, mTvVerifyMoney, mTvVerifyTotalMoney, mTvVerifyTotalCount;
    private ImageView mIvBack, mIvCashierArrow, mIvShopArrow, mIvSiteArrow, mIvFilter;
    private String startTime;
    private String endTime;
    private String userId;
    private String mSiteId = "";
    private String storeMerchantId;
    private List<ReportBean.DataEntity.ListEntity> mList;
    private int tab = 1;
    private String storeName;
    private String cashierName;
    private PosPrintUtil mPosPrintUtil;
    private Printer printer;
    private TextView tv_date;
    private Button mBtnPrint;
    private TextView mTvShop, mTvCashier, mTvSite, mTvNull, mTvShopNull;
    private ScrollView mSvReport;
    private RecyclerView mRecyclerView, mStoreRecyclerView, mCashierRecyclerView, mSiteRecyclerView;
    private CollectTypeAdapter mAdapter;
    private ReportBean.DataEntity mInfo;
    private View mView;
    private Activity mMainActivity;
    private IWoyouService woyouService;
    private ReportBroadcastReceiver mReportBroadcastReceiver;
    private RadioGroup mRgTitle;
    private String mShopType;
    private RadioGroup mRgType;
    private RadioButton mRbAllStore, mRbDirectStore, mRbJoinStore;
    private EditTextDelete mEtStoreInput, mEtCashierInput, mEtSiteInput;
    private ShopRecyclerAdapter mStoreAdapter;
    private CashierRecyclerAdapter mCashierAdapter;
    private boolean mShopSwitchArrow, mCashierSwitchArrow, mSiteSwitchArrow;
    private Animation rotate;
    private List<StoreListBean.DataEntity> mShopList;
    private List<StoreListBean.DataEntity> mOriginShopList;
    private List<DataEntity> mCashierList;
    private List<DataEntity> mOriginCashierList;
    private SafeDialog mLoadDialog;
    private List<PaySiteBean> mSiteList;
    private List<PaySiteBean> mOriginSiteList;
    private PaySiteChoiceAdapter mSiteAdapter;
    private FilterCashierPopupWindow popupWindow;

    private PrintUtils mPrintUtils;
    private AidlPayService payServiceManager;
    private AidlPrint aidlPrint;
    private N900Device n900Device;
    private com.chinapnr.aidl.deviceService.AidlDeviceService deviceService;
    private com.chinapnr.aidl.printer.AidlPrinter printerModule;

    public static final String LKL_SERVICE_ACTION = "lkl_cloudpos_mid_service";
    private AidlPrinter printerDev = null;
    private final static String PRNT_ACTION = "android.prnt.message";
    private Intent intentService;

    private TextView mTvCommissionFee, mTvSettlementFee, mTvCommissionTitle;
    private LinearLayout mLlJsLayout;
    private View mLayoutVipTab;//会员消费layout
    private View mLayoutFkTab;//饭卡消费layout
    private TextView mTvFkSettlementMoney;//饭卡结算金额
    private TextView mTvFkPayMoney;//饭卡交易金额
    private TextView mTvFkPayCount;//饭卡交易笔数
    private TextView mTvFkSettlementRate;//饭卡结算率
    private TextView mTvFkSettlementMoneyContent;//饭卡结算金额
    private TextView mTvFkRefundTotalCount;//饭卡退款笔数
    private TextView mTvFkRefundTotalMoney;//饭卡退款金额
    private TextView mTvFkSettlementTopTitle;//饭卡结算金额顶部标题


    private BroadcastReceiver mPrtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int ret = intent.getIntExtra("ret", 0);
            if (ret == -1)
                MyToast.showToastShort("打印机缺纸！");
        }
    };

    /**
     * 服务连接成功时回调
     *
     * @param serviceManager
     * @createtor：Administrator
     * @date:2015-8-4 上午7:38:08
     */
    public void onDeviceConnected(AidlDeviceService serviceManager) {
        try {
            printerDev = AidlPrinter.Stub.asInterface(serviceManager.getPrinter());
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    ;

    //设别服务连接桥
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder serviceBinder) {
            LogUtil.d("aidlService服务连接成功");
            if (serviceBinder != null) { //绑定成功
                AidlDeviceService serviceManager = AidlDeviceService.Stub.asInterface(serviceBinder);
                onDeviceConnected(serviceManager);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogUtil.d("AidlService服务断开了");
        }
    };

    //绑定服务 拉卡拉
    public void bindService() {
        Intent intent = new Intent();
        intent.setAction(LKL_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(getActivity(), intent));
        boolean flag = false;
        flag = getActivity().bindService(eintent, conn, BIND_AUTO_CREATE);
        if (flag) {
            LogUtil.d("服务绑定成功");
        } else {
            LogUtil.d("服务绑定失败");
        }
    }

    /**
     * 获取启动服务的Intent
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        //根据intent信息，获取相匹配的服务
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        //保证服务的唯一性
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }
        //获取组件信息，创建组件名
        ResolveInfo serviceInfo = resolveInfos.get(0);
        LogUtil.d("PackageName", resolveInfos.size() + "");
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 汇付绑定服务参数
     **/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            deviceService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /**
             *总设备服务的初始化在 ServiceConnection 的监听回调里实现
             **/
            deviceService = com.chinapnr.aidl.deviceService.AidlDeviceService.Stub.asInterface(service);
            try {
                printerModule = com.chinapnr.aidl.printer.AidlPrinter.Stub.asInterface(deviceService.getPrinter());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 绑定汇付打印服务
     **/
    private void bindHuifuService() {
        Intent intent = new Intent();
        intent.setAction("com.chinapnr.DeviceService"); // service 的 action
        intent.setPackage("com.chinapnr.npos.service"); // service 的包名
        getActivity().bindService(intent, serviceConnection, BIND_AUTO_CREATE); // 绑定 service
    }

    private ServiceConnection connService = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            woyouService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            woyouService = IWoyouService.Stub.asInterface(service);
        }
    };

    /**
     * 绑定商米打印服务
     */
    private void bindSmService() {
        Intent intent = new Intent();
        intent.setPackage("woyou.aidlservice.jiuiv5");
        intent.setAction("woyou.aidlservice.jiuiv5.IWoyouService");
        getActivity().startService(intent);
        getActivity().bindService(intent, connService, BIND_AUTO_CREATE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMainActivity = getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        ((MainActivity) getActivity()).dismissLoading();
        mView = inflater.inflate(R.layout.fragment_collect, container, false);
        StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
        initView(mView);
        initEvent();
        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
            mTvNull.setVisibility(View.VISIBLE);
        } else {
            initData();
        }
        initPosPrint();
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBtnPrint.setEnabled(true);
        /*if (Constants.LIANDI.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            ((BaseActivity)getActivity()).bindDeviceService();
        }else */
        if ("yipos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindPayServer();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_REPORT_DATA);
        mReportBroadcastReceiver = new ReportBroadcastReceiver();
        getActivity().registerReceiver(mReportBroadcastReceiver, intentFilter);
    }

    protected void bindPayServer() {
        Intent intent = new Intent(((BaseActivity) getActivity()).PAY_SERVICE_ACTION);
        intent.setPackage(((BaseActivity) getActivity()).SERVICE_PACKAGE_NAME);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        getActivity().bindService(intent, payConnection, BIND_AUTO_CREATE);
    }

    private ServiceConnection payConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bindPayServer();
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PayServiceManager.init(service);
            initAidl();
        }
    };

    private void initAidl() {
        payServiceManager = PayServiceManager.getInstance();
        if (payServiceManager != null) {
            try {
                aidlPrint = AidlPrint.Stub.asInterface(payServiceManager.doPrint());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        /*if (Constants.LIANDI.contains(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            ((MainActivity) getActivity()).unbindDeviceService();
        }*/
        getActivity().unregisterReceiver(mReportBroadcastReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        destroyPosPrint();
    }

    private void destroyPosPrint() {
        if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unbindService(connService);
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unbindService(conn);
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unbindService(serviceConnection);
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            getActivity().unregisterReceiver(mPrtReceiver);
        /*} else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {*/
        /*} else if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
            WizarPrinterUtil.with().close();*/
        } else if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                //登出，以免占用U架构服务
                try {
                    BaseSystemManager.getInstance().deviceServiceLogout();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        /*else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            n900Device.disconnect();
        }*/
    }

    private void initPosPrint() {
        mPosPrintUtil = new PosPrintUtil((BaseActivity) getActivity(), mBtnPrint);
        if ("wpos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            try {
                // 设备可能没有打印机，open会抛异常
                printer = WeiposImpl.as().openPrinter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("shangmi".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            /** 商米v1打印机相关 */
            bindSmService();
        } else if ("lepos".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            mPrintUtils = new PrintUtils(MyApplication.getContext());
        } else if ("lkl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindService();
        } else if (Constants.HUIFU.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            bindHuifuService();
        } else if ("ybx".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            intentService = new Intent(getActivity(), PrintSum.class);
            IntentFilter filter = new IntentFilter();
            filter.addAction(PRNT_ACTION);
            getActivity().registerReceiver(mPrtReceiver, filter);
        /*} else if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {*/
        }/* else if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
            WizarPrinterUtil.with().open();
        }*/

        /*else if ("xdl".equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            n900Device = N900Device.getInstance((BaseActivity) getActivity());
            ((BaseActivity)getActivity()).connectDevice(n900Device);
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        deviceServiceLogin();
    }

    //联迪银商打印初始化
    private void deviceServiceLogin() {
        if (Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                try {
                    BaseSystemManager.getInstance().deviceServiceLogin(
                            getActivity(), null, "99999998",//设备ID，生产找后台配置
                            arg0 -> {//arg0可见ServiceResult.java
                                if (0 == arg0 || 2 == arg0 || 100 == arg0) {//0：登录成功，有相关参数；2：登录成功，无相关参数；100：重复登录。

                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void initView(View view) {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mTvTitle = view.findViewById(R.id.tv_title);
        mLlDate = (LinearLayout) view.findViewById(R.id.ll_date);
        mLlShop = (LinearLayout) view.findViewById(R.id.ll_shop);
        mTvShop = (TextView) view.findViewById(R.id.tv_shop);
        mTvCashier = (TextView) view.findViewById(R.id.tv_cashier);
        mLlSalesman = (LinearLayout) view.findViewById(R.id.ll_cashier);
        mLlSite = (LinearLayout) view.findViewById(R.id.ll_pay_site);
        mTvSite = (TextView) view.findViewById(R.id.tv_pay_site);
        mIvFilter = view.findViewById(R.id.iv_filter);
        mIvSiteArrow = (ImageView) view.findViewById(R.id.iv_pay_site_arrow);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        //tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + getString(R.string.public_today) + ")");
        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
        setDateView(startTime, endTime);

        mTvTotal = (TextView) view.findViewById(R.id.tv_total);//总金额、收款的交易净额
        mTvTotalPay = (TextView) view.findViewById(R.id.tv_total_pay);//总收款金额、收款的交易金额
        mTvTotalPayNum = (TextView) view.findViewById(R.id.tv_order_pay_num);//总收款笔数、收款的交易笔数
        mTvRealMoney = (TextView) view.findViewById(R.id.tv_real_money);//实收笔数
        mTvPayCouponMoney = (TextView) view.findViewById(R.id.tv_merchant_coupon_money);//商家优惠笔数
        mTvTotalRefund = (TextView) view.findViewById(R.id.tv_refund_total);//总退款金额
        mTvTotalRefundNum = (TextView) view.findViewById(R.id.tv_refund_total_num);//总退款笔数
        mTvCommissionFee = (TextView) view.findViewById(R.id.tvCommissionFee);//手续费
        mTvSettlementFee = (TextView) view.findViewById(R.id.tvSettlementFee);//结算金额
//        mLlCommissionFee = view.findViewById(R.id.llCommissionFee);//ll手续费
        mTvCommissionTitle = view.findViewById(R.id.tv_commission_title);//手续费标题
        mLlJsLayout = view.findViewById(R.id.ll_js_layout);//ll结算金额

        mTvVerifyActuralMoney = (TextView) view.findViewById(R.id.tv_verify_total);//核销净额
        mTvVerifyTotalMoney = (TextView) view.findViewById(R.id.tv_verify_total_pay);//核销交易金额
        mTvVerifyTotalCount = (TextView) view.findViewById(R.id.tv_verify_pay_num);//核销交易笔数
        mTvVerifyMoney = (TextView) view.findViewById(R.id.tv_report_verify_money);//核销实际金额
        mTvVerifyCouponMoney = (TextView) view.findViewById(R.id.tv_verify_merchant_coupon_money);//会员卡商家优惠金额
        mTvVerifyRefund = (TextView) view.findViewById(R.id.tv_verify_refund_total);//核销退款总金额
        mTvVerifyRefundNum = (TextView) view.findViewById(R.id.tv_verify_refund_total_num);//核销退款总笔数

        mTvFkSettlementMoney = view.findViewById(R.id.tv_fk_settlement_money);//饭卡结算金额
        mTvFkPayCount = view.findViewById(R.id.tv_fk_pay_count);//饭卡交易金额
        mTvFkPayMoney = view.findViewById(R.id.tv_fk_pay_money);//饭卡交易笔数
        mTvFkSettlementRate = view.findViewById(R.id.tv_fk_settlement_rate);//饭卡结算率
        mTvFkSettlementMoneyContent = view.findViewById(R.id.tv_fk_settlement_money_content);//饭卡结算金额
        mTvFkRefundTotalCount = view.findViewById(R.id.tv_fk_refund_total_count);//饭卡退款笔数
        mTvFkRefundTotalMoney = view.findViewById(R.id.tv_fk_refund_total_money);//饭卡退款金额
        mTvFkSettlementTopTitle = view.findViewById(R.id.tv_fk_settlement_top_title);//饭卡结算金额顶部标题

        mBtnPrint = (Button) view.findViewById(R.id.btn_print);
        mIvBack = (ImageView) view.findViewById(R.id.iv_back);
        mSvReport = (ScrollView) view.findViewById(R.id.sv_report);
        mTvNull = (TextView) view.findViewById(R.id.tv_null);
        mSvReport.setVisibility(View.GONE);
        mTvNull.setVisibility(View.GONE);
        mIvBack.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mIvCashierArrow = (ImageView) view.findViewById(R.id.iv_cashier_arrow);
        mIvShopArrow = (ImageView) view.findViewById(R.id.iv_shop_arrow);

        mRgTitle = (RadioGroup) view.findViewById(R.id.rg_title);
        mLlReportPay = (LinearLayout) view.findViewById(R.id.ll_report_pay);
        mLayoutVipTab = view.findViewById(R.id.layout_vip_tab);
        mLayoutFkTab = view.findViewById(R.id.layout_fk_tab);

        mRgType = view.findViewById(R.id.rg_type);
        mRbAllStore = view.findViewById(R.id.rb_all_store);
        mRbDirectStore = view.findViewById(R.id.rb_direct_store);
        mRbJoinStore = view.findViewById(R.id.rb_join_store);
        mShopPopLayout = view.findViewById(R.id.shop_pop_layout);
        mEtStoreInput = view.findViewById(R.id.et_store_input);
        mTvShopNull = view.findViewById(R.id.tv_shop_null);
        mStoreRecyclerView = view.findViewById(R.id.recyclerView_store);
        CustomLinearLayoutManager storeLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mStoreRecyclerView.setLayoutManager(storeLinearLayoutManager);
        mEtStoreInput.setClearImage(R.mipmap.ic_search_clear);

        mCashierPopLayout = view.findViewById(R.id.cashier_pop_layout);
        mEtCashierInput = view.findViewById(R.id.et_cashier_input);
        CustomLinearLayoutManager cashierLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mCashierRecyclerView = view.findViewById(R.id.recyclerView_cashier);
        mCashierRecyclerView.setLayoutManager(cashierLinearLayoutManager);
        mEtCashierInput.setClearImage(R.mipmap.ic_search_clear);

        mSitePopLayout = view.findViewById(R.id.site_pop_layout);
        mEtSiteInput = view.findViewById(R.id.et_site_input);
        CustomLinearLayoutManager siteLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mSiteRecyclerView = view.findViewById(R.id.recyclerView_site);
        mSiteRecyclerView.setLayoutManager(siteLinearLayoutManager);
        mEtSiteInput.setClearImage(R.mipmap.ic_search_clear);

        ((BaseActivity) getActivity()).setButtonWhite(mBtnPrint);
        mTvTitle.setText(R.string.tv_report);

        mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);
    }

    private void setDateView(String startTime, String endTime) {
        try {
            if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formartDateToMMDD(endTime))) {
                /*if (DateUtil.formartDateToMMDD(startTime).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                    tv_date.setText(DateUtil.formartDateToMMDD(startTime) + "(今天)");
                } else {*/
                tv_date.setText(DateUtil.formartDateToMMDD(startTime));
//                }
            } else {
                tv_date.setText(DateUtil.formartDateToMMDD(startTime) + " " + getString(R.string.tv_least) + " "
                        + DateUtil.formartDateToMMDD(endTime));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void initEvent() {
        mLlDate.setOnClickListener(this);
        mLlShop.setOnClickListener(this);
        mLlSalesman.setOnClickListener(this);
        mLlSite.setOnClickListener(this);
        mBtnPrint.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mRgTitle.setOnCheckedChangeListener(this);

        mRbAllStore.setOnClickListener(this);
        mRbDirectStore.setOnClickListener(this);
        mRbJoinStore.setOnClickListener(this);
        mIvFilter.setOnClickListener(this);

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData(mEtStoreInput.getText().toString().trim());
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setShopData("");
                        if (mStoreAdapter != null)
                            mStoreAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtCashierInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData(mEtCashierInput.getText().toString().trim());
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtCashierInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setCashierData("");
                        if (mCashierAdapter != null)
                            mCashierAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });
        mEtSiteInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setSiteData(mEtSiteInput.getText().toString().trim());
                        if (mSiteAdapter != null)
                            mSiteAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                    return true;
                }
                return false;
            }
        });
        mEtSiteInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        setSiteData("");
                        if (mSiteAdapter != null)
                            mSiteAdapter.notifyDataSetChanged();
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
            }
        });

        mView.findViewById(R.id.tv_title_click_js).setOnClickListener(this);
        mView.findViewById(R.id.tv_title_click_yh).setOnClickListener(this);
        mView.findViewById(R.id.tv_title_click_shishou).setOnClickListener(this);
        mView.findViewById(R.id.tv_title_click_tk).setOnClickListener(this);
        mView.findViewById(R.id.tv_title_click_jyze).setOnClickListener(this);
        mTvCommissionTitle.setOnClickListener(this);
        mTvFkSettlementTopTitle.setOnClickListener(this);
    }

    public void initData() {
        storeName = "全部门店";
        cashierName = UIUtils.getString(R.string.tv_all_user);
        mShopType = "";
        mShopList = new ArrayList<>();
        mOriginShopList = new ArrayList<>();
        mCashierList = new ArrayList<>();
        mOriginCashierList = new ArrayList<>();
        mSiteList = new ArrayList<>();
        mOriginSiteList = new ArrayList<>();
        mList = new ArrayList<>();
        mAdapter = new CollectTypeAdapter(mList);
        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        if (MyApplication.getIsCasher()) {
            mIvShopArrow.setVisibility(View.GONE);
            storeMerchantId = MyApplication.getDefaultStore();
            mLlShop.setEnabled(false);
            if (TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""))) {
                mTvShop.setText("全部门店");
            } else {
                mTvShop.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_DEFAULT_STORE_NAME, ""));
            }
            if (TextUtils.isEmpty(MyApplication.getOpCodeReport())) {
                mLlSalesman.setEnabled(false);
                mIvCashierArrow.setVisibility(View.GONE);
            }
            mTvCashier.setText(MyApplication.getRealName());
//            mLlCommissionFee.setVisibility(View.GONE);
            setSettlementAndCommitFeeVisible(false);
            userId = MyApplication.getUserId();
            cashierName = MyApplication.getRealName();
        } else {
            userId = "";
            storeMerchantId = "";
            cashierName = getString(R.string.tv_all_user);
        }
        if (tab == 1) {
            //收款
            mLlReportPay.setVisibility(View.VISIBLE);
            mLayoutVipTab.setVisibility(View.GONE);
            mLayoutFkTab.setVisibility(View.GONE);
        } else if (tab == 2) {
            //会员消费
            mLlReportPay.setVisibility(View.GONE);
            mLayoutVipTab.setVisibility(View.VISIBLE);
            mLayoutFkTab.setVisibility(View.GONE);
        } else if (tab == 3) {
            //饭卡消费
            mLlReportPay.setVisibility(View.GONE);
            mLayoutVipTab.setVisibility(View.GONE);
            mLayoutFkTab.setVisibility(View.VISIBLE);
        }
//        mRgTitle.setVisibility(MyApplication.showCollectMemberData() ? View.VISIBLE : View.GONE);
        setTabVisible(MyApplication.isOpenMember(), MyApplication.isOpenFuncardVerification());
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            ServerClient.newInstance(getContext()).isOpenMember(getContext(), Constants.TAG_COLLECT_MEMBER, null);
            ServerClient.newInstance(getContext()).openFuncardVerification(getContext(), Constants.TAG_COLLECT_FUNCARD, null);
        }

    }


    /**
     * 设置tab可见否
     *
     * @param vipTabVisible 会员消费tab是否可见
     * @param fkTabVisible  饭卡消费tab是否可见
     */
    private void setTabVisible(boolean vipTabVisible, boolean fkTabVisible) {
        if (!vipTabVisible && !fkTabVisible) {
            mRgTitle.setVisibility(View.GONE);
        } else {
            mRgTitle.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.rb_title_verify).setVisibility(vipTabVisible ? View.VISIBLE : View.GONE);
            mView.findViewById(R.id.rb_title_fun_card).setVisibility(fkTabVisible ? View.VISIBLE : View.GONE);
        }
    }

    private void setShopData(String search) {
        mShopList.clear();
        if (TextUtils.isEmpty(search)) {
            if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                mRgType.setVisibility(View.VISIBLE);
            }
            mShopList.addAll(mOriginShopList);
        } else {
            mRgType.setVisibility(View.GONE);
            for (int i = 0; i < mOriginShopList.size(); i++) {
                if (mOriginShopList.get(i).getStoreName().contains(search)) {
                    mShopList.add(mOriginShopList.get(i));
                }
            }
        }
    }

    private void setSiteData(String search) {
        mSiteList.clear();
        if (TextUtils.isEmpty(search)) {
            mSiteList.addAll(mOriginSiteList);
        } else {
            for (int i = 0; i < mOriginSiteList.size(); i++) {
                if (mOriginSiteList.get(i).getCashPointName().contains(search)) {
                    mSiteList.add(mOriginSiteList.get(i));
                }
            }
        }
    }

    private void setCashierData(String search) {
        mCashierList.clear();
        if (TextUtils.isEmpty(search)) {
            mCashierList.addAll(mOriginCashierList);
        } else {
            for (int i = 0; i < mOriginCashierList.size(); i++) {
                if (mOriginCashierList.get(i).getRealName().contains(search)) {
                    mCashierList.add(mOriginCashierList.get(i));
                }
            }
        }
    }

    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mEtStoreInput.setText("");
                mShopSwitchArrow = false;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.GONE);
                break;
            case 2:
                mEtCashierInput.setText("");
                mCashierSwitchArrow = false;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.GONE);
                break;
            case 3:
                mEtSiteInput.setText("");
                mSiteSwitchArrow = false;
                mIvSiteArrow.startAnimation(rotate);
                mSitePopLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                if ("12".equals(MyApplication.getMerchantType()) && MyApplication.getIsMerchant()) {
                    mRgType.setVisibility(View.VISIBLE);
                } else {
                    mRgType.setVisibility(View.GONE);
                }
                if (mRgType.getVisibility() == View.VISIBLE) {
                    switch (mShopType) {
                        case "":
                            mRgType.clearCheck();
                            mRbAllStore.setChecked(true);
                            break;
                        case "21":
                            mRgType.clearCheck();
                            mRbDirectStore.setChecked(true);
                            break;
                        case "22":
                            mRgType.clearCheck();
                            mRbJoinStore.setChecked(true);
                            break;
                    }
                }
                mShopSwitchArrow = true;
                mIvShopArrow.startAnimation(rotate);
                mShopPopLayout.setVisibility(View.VISIBLE);
                loadShopData(mShopType);
                break;
            case 2:
                mCashierSwitchArrow = true;
                mIvCashierArrow.startAnimation(rotate);
                mCashierPopLayout.setVisibility(View.VISIBLE);
                break;
            case 3:
                mSiteSwitchArrow = true;
                mIvSiteArrow.startAnimation(rotate);
                mSitePopLayout.setVisibility(View.VISIBLE);
                paySiteList();
                break;
        }
    }

    private void paySiteList() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginSiteList.clear();
            mSiteList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).paySiteList(MyApplication.getContext(), Constants.TAG_COLLECT_SITE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadShopData(String shopType) {
        //门店网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
//            if (mRbAllStore.isChecked())
//                mOriginShopList.clear();
            mShopList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(shopType))
                map.put("merchantType", shopType);
            ServerClient.newInstance(MyApplication.getContext()).findStore(MyApplication.getContext(), Constants.TAG_COLLECT_STORE, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void loadCashierData() {
        //收银员网络请求
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mOriginCashierList.clear();
            mCashierList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", "10000");
            map.put("currentPage", "1");
            if (!TextUtils.isEmpty(mShopType))
                map.put("merchantType", mShopType);
            if (!TextUtils.isEmpty(storeMerchantId)) {
                map.put("storeId", storeMerchantId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getCashierOfStore(MyApplication.getContext(), Constants.TAG_COLLECT_CASHIER, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_REPORT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ReportBean msg = (ReportBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.GET_REPORT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_REPORT_TRUE://请求成功
                    if (MyApplication.getIsCasher()) {
                        sendTodayDataBroadcast(getActivity());
                    }
                    if (msg.getData() != null) {
                        mInfo = msg.getData();
                        mInfo.setType(tab);
                        mInfo.setCashierName(cashierName);
                        mInfo.setStoreName(storeName);
                        if (msg.getData().getList() != null && msg.getData().getList().size() > 0) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mList.clear();
                            mList.addAll(msg.getData().getList());
                            Collections.sort(mList);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                        }
                        mTvNull.setVisibility(View.GONE);
                        mSvReport.setVisibility(View.VISIBLE);
                        setView(msg.getData());
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSvReport.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        StoreListBean.DataEntity dataEntity = new StoreListBean().new DataEntity();
                        if (mRbAllStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_shop));
                            dataEntity.setStoreId("");
                            mOriginShopList.clear();
                            mOriginShopList.add(dataEntity);
                            mOriginShopList.addAll(msg.getData());
                            mShopList.addAll(mOriginShopList);
                        } else if (mRbDirectStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_direct_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        } else if (mRbJoinStore.isChecked()) {
                            dataEntity.setStoreName(getString(R.string.tv_all_join_shop));
                            dataEntity.setStoreId("");
                            mShopList.add(dataEntity);
                            mShopList.addAll(msg.getData());
                        }
                        mStoreAdapter = new ShopRecyclerAdapter(getActivity(), mShopList, storeMerchantId);
                        mStoreAdapter.setOnItemClickListener(new ShopRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                if (mRgType.getVisibility() == View.VISIBLE) {
                                    switch (mRgType.getCheckedRadioButtonId()) {
                                        case R.id.rb_all_store:
                                            mShopType = "";
                                            break;
                                        case R.id.rb_direct_store:
                                            mShopType = "21";
                                            break;
                                        case R.id.rb_join_store:
                                            mShopType = "22";
                                            break;
                                    }
                                } else {
                                    mShopType = "";
                                }
                                storeMerchantId = mShopList.get(position).getStoreId();
                                mTvShop.setText(mShopList.get(position).getStoreName());
                                if (!MyApplication.getIsCasher()) {
                                    mTvCashier.setText(getString(R.string.tv_all_user));
                                    cashierName = getString(R.string.tv_all_user);
                                    userId = "";
                                } else {
                                    mTvCashier.setText(MyApplication.getRealName());
                                    cashierName = MyApplication.getRealName();
                                    userId = MyApplication.getUserId();
                                }
                                mTvSite.setText(getString(R.string.tv_all_site));
                                mSiteId = "";
                                closeArrow(1);
                                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    DialogUtil.safeShowDialog(mLoadDialog);
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                }
                            }
                        });
                        mStoreRecyclerView.setAdapter(mStoreAdapter);
                        mTvShopNull.setVisibility(View.GONE);
                        mStoreRecyclerView.setVisibility(View.VISIBLE);
                    } else {
                        mTvShopNull.setVisibility(View.VISIBLE);
                        mStoreRecyclerView.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_CASHIER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CashierOfStoreBean msg = (CashierOfStoreBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.ERROR_CODE_NO_RIGH)) {
                                DataEntity dataEntity = new DataEntity();
                                dataEntity.setRealName(getString(R.string.tv_all_user));
                                dataEntity.setUserId("");
                                mOriginCashierList.add(dataEntity);
                                mCashierList.addAll(mOriginCashierList);
                                mCashierAdapter = new CashierRecyclerAdapter(getActivity(), mCashierList);
                                mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        userId = mCashierList.get(position).getUserId();
                                        mTvCashier.setText(mCashierList.get(position).getRealName());
                                        setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                        closeArrow(2);
                                        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                            DialogUtil.safeShowDialog(mLoadDialog);
                                            getReport(getRequestMap(startTime, endTime, userId, null));
                                        }
                                    }
                                });
                                mCashierRecyclerView.setAdapter(mCashierAdapter);
                                mCashierAdapter.setSelected(userId);
                                openArrow(2);
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        DataEntity dataEntity = new DataEntity();
                        dataEntity.setRealName(getString(R.string.tv_all_user));
                        dataEntity.setUserId("");
                        mOriginCashierList.add(dataEntity);
                        mOriginCashierList.addAll(msg.getData());
                        mCashierList.addAll(mOriginCashierList);
                        mCashierAdapter = new CashierRecyclerAdapter(getActivity(), mCashierList);
                        mCashierAdapter.setOnItemClickListener(new CashierRecyclerAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                userId = mCashierList.get(position).getUserId();
//                                mLlCommissionFee.setVisibility(TextUtils.isEmpty(userId) ? View.VISIBLE : View.GONE);
                                setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                mTvCashier.setText(mCashierList.get(position).getRealName());
                                closeArrow(2);
                                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    DialogUtil.safeShowDialog(mLoadDialog);
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                }
                            }
                        });
                        mCashierRecyclerView.setAdapter(mCashierAdapter);
                        mCashierAdapter.setSelected(userId);
                        openArrow(2);
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_SITE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            PaySiteList msg = (PaySiteList) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(Constants.ERROR_CODE_NO_RIGH)) {
                                PaySiteBean dataEntity = new PaySiteBean();
                                dataEntity.setCashPointName(getString(R.string.tv_all_site));
                                dataEntity.setId("");
                                PaySiteBean otherSite = new PaySiteBean();
                                otherSite.setCashPointName(getString(R.string.tv_other_site));
                                otherSite.setId("tv_other_site");
                                mOriginSiteList.add(dataEntity);
                                mOriginSiteList.add(otherSite);
                                mSiteList.addAll(mOriginSiteList);
                                mSiteAdapter = new PaySiteChoiceAdapter(getActivity(), mSiteList, mSiteId);
                                mSiteAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        mSiteId = mSiteList.get(position).getId();
                                        mTvSite.setText(mSiteList.get(position).getCashPointName());
                                        setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                        closeArrow(3);
                                        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                            DialogUtil.safeShowDialog(mLoadDialog);
                                            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                        }
                                    }
                                });
                                mSiteRecyclerView.setAdapter(mSiteAdapter);
//                                openArrow(3);
                            } else if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getCashPointList() != null) {
                        PaySiteBean dataEntity = new PaySiteBean();
                        dataEntity.setCashPointName(getString(R.string.tv_all_site));
                        dataEntity.setId("");
                        PaySiteBean otherSite = new PaySiteBean();
                        otherSite.setCashPointName(getString(R.string.tv_other_site));
                        otherSite.setId("tv_other_site");
                        mOriginSiteList.add(dataEntity);
                        mOriginSiteList.add(otherSite);
                        mOriginSiteList.addAll(msg.getData().getCashPointList());
                        mSiteList.addAll(mOriginSiteList);
                        mSiteAdapter = new PaySiteChoiceAdapter(getActivity(), mSiteList, mSiteId);
                        mSiteAdapter.setOnItemClickListener(new PaySiteChoiceAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                mSiteId = mSiteList.get(position).getId();
                                mTvSite.setText(mSiteList.get(position).getCashPointName());
                                setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                                closeArrow(3);
                                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                                    DialogUtil.safeShowDialog(mLoadDialog);
                                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                                }
                            }
                        });
                        mSiteRecyclerView.setAdapter(mSiteAdapter);
//                        openArrow(3);
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_MEMBER)) {//查询会员开通
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, msg.getData().getRetCode());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, msg.getData().isStatisticsCode());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, false);
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, false);
                    }
//                    mRgTitle.setVisibility(MyApplication.showCollectMemberData() ? View.VISIBLE : View.GONE);
                    setTabVisible(MyApplication.isOpenMember(), MyApplication.isOpenFuncardVerification());
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_COLLECT_FUNCARD)) {//查询饭卡开通
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, (boolean) msg.getData());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_FUNCARD_VERIFICATION_ENABLE, false);
                    }
//                    mRgTitle.setVisibility(MyApplication.showCollectMemberData() ? View.VISIBLE : View.GONE);
                    setTabVisible(MyApplication.isOpenMember(), MyApplication.isOpenFuncardVerification());

                    break;
            }
        }
    }

    private void showPopupDateWindow() {
        DateStartEndPopupWindow moneyPopupWindow = new DateStartEndPopupWindow(getActivity(), startTime, endTime, new DateStartEndPopupWindow.HandleTv() {
            @Override
            public void getDate(String startTime, String endTime) {
                ReportFragment.this.startTime = startTime;
                ReportFragment.this.endTime = endTime;
                setDateView(startTime, endTime);
                LogUtil.d("time===" + startTime + ",----" + endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        });
        moneyPopupWindow.showAtLocation(mBtnPrint, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    private Map<String, Object> getRequestMap(String startTime, String endTime, String userId, String storeMerchantId) {
        Map<String, Object> map = new HashMap<>();
        map.put("tab", tab);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        if (TextUtils.isEmpty(mSiteId)) {
            map.put("cashPointType", 1);
        } else if ("tv_other_site".equals(mSiteId)) {
            map.put("cashPointType", 2);
        } else {
            map.put("cashPointType", 3);
            map.put("cashPointId", mSiteId);
        }
        if (!TextUtils.isEmpty(userId))
            map.put("userId", userId);
        if (!TextUtils.isEmpty(mShopType))
            map.put("merchantType", mShopType);
        if (!TextUtils.isEmpty(storeMerchantId))
            map.put("storeMerchantId", storeMerchantId);
        return map;
    }

    public void getReport(Map<String, Object> map) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mList.clear();
            ServerClient.newInstance(MyApplication.getContext()).getReport(MyApplication.getContext(), Constants.TAG_GET_REPORT, map);
        } else {
            //MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
            mTvNull.setText("网络异常");
            mTvNull.setVisibility(View.VISIBLE);
            mSvReport.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_date:
                MtaUtils.mtaId(getActivity(), "1016");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                   ((BaseActivity)getActivity()).openDialog();
                } else {
                    /*Intent intentDate = new Intent(getActivity(), DateChoiceActivity.class);
                    startActivityForResult(intentDate, Constants.REQUEST_CHOICE_DATE);*/
                    showPopupDateWindow();
                }
                break;
            /*case R.id.ll_shop:
                MtaUtils.mtaId(getActivity(), "1017");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                   ((BaseActivity)getActivity()).openDialog();
                } else {
                    Intent intentShop = new Intent(getActivity(), ShopActivity.class);
                    if (TextUtils.isEmpty(storeMerchantId)) {
                        intentShop.putExtra(Constants.INTENT_STORE_ID, "");
                    } else {
                        intentShop.putExtra(Constants.INTENT_STORE_ID, storeMerchantId);
                    }
                    intentShop.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_COLLECT_SHOP);
                    intentShop.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    startActivityForResult(intentShop, Constants.REQUEST_SHOP_BEAN_CODE);
                }
                break;
            case R.id.ll_cashier:
                MtaUtils.mtaId(getActivity(), "1018");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                   ((BaseActivity)getActivity()).openDialog();
                } else {
                    Intent intentCashier = new Intent(getActivity(), CashierActivity.class);
                    if (TextUtils.isEmpty(userId)) {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                    } else {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                    }
                    intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                    intentCashier.putExtra(Constants.INTENT_STORE_DATA_TYPE, 1);
                    startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                }
                break;*/
            case R.id.ll_shop:
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                }
                if (mShopSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_cashier:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                } else {
                    loadCashierData();
                }
                break;
            case R.id.ll_pay_site:
                if (mShopSwitchArrow) {
                    closeArrow(1);
                }
                if (mCashierSwitchArrow) {
                    closeArrow(2);
                }
                if (mSiteSwitchArrow) {
                    closeArrow(3);
                } else {
                    openArrow(3);
                }
                break;
            case R.id.btn_print:
                MtaUtils.mtaId(getActivity(), "1019");
                //bluetoothPrint();
                mInfo.setStoreMerchantId(storeMerchantId);
                LogUtil.d("print----1");
                bluePrint();
                break;
            case R.id.rb_all_store:
                loadShopData("");
                break;
            case R.id.rb_direct_store:
                loadShopData("21");
                break;
            case R.id.rb_join_store:
                loadShopData("22");
                break;
            case R.id.iv_filter:
                showFilterPop();
                break;
            case R.id.tv_title_click_js:
                //结算金额
                StringBuilder builder = new StringBuilder();
                builder.append(getResources().getString(R.string.question_tip_report_settlement_front));
                builder.append(getResources().getString(R.string.question_tip_report_settlement_behind));
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), builder.toString());
                break;
            case R.id.tv_title_click_yh:
                //优惠金额
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getResources().getString(R.string.question_tip_report_yh));
                break;
            case R.id.tv_title_click_shishou:
                //实收金额
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getResources().getString(R.string.question_tip_report_shishou));
                break;
            case R.id.tv_title_click_tk:
                //退款金额
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getResources().getString(R.string.question_tip_report_tk));
                break;
            case R.id.tv_title_click_jyze:
                //交易净额
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getResources().getString(R.string.question_tip_report_jyze));
                break;
            case R.id.tv_commission_title:
                //手续费
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getResources().getString(R.string.question_tip_report_commission));
                break;
            case R.id.tv_fk_settlement_top_title:
                //饭卡结算金额
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getResources().getString(R.string.question_tip_report_fk_settlement));
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_title_pay:
                mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);
                mIvFilter.setVisibility(View.VISIBLE);
                mLlReportPay.setVisibility(View.VISIBLE);
                mLayoutVipTab.setVisibility(View.GONE);
                mLayoutFkTab.setVisibility(View.GONE);
                tab = 1;
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;
            case R.id.rb_title_verify:
                mLlSite.setVisibility("2".equals(MyApplication.getCashPointOpenStatus()) ? View.VISIBLE : View.GONE);
                mIvFilter.setVisibility(View.VISIBLE);
                mLlReportPay.setVisibility(View.GONE);
                mLayoutVipTab.setVisibility(View.VISIBLE);
                mLayoutFkTab.setVisibility(View.GONE);
                tab = 2;
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;
            case R.id.rb_title_fun_card:
                mLlSite.setVisibility(View.GONE);
                mIvFilter.setVisibility(View.GONE);
                mLlReportPay.setVisibility(View.GONE);
                mLayoutVipTab.setVisibility(View.GONE);
                mLayoutFkTab.setVisibility(View.VISIBLE);
                tab = 3;
                if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                }
                break;

        }
    }

    /**
     * 设置结算金额和手续费是否可见
     * 因为通过筛选的收银点和收银员会查询不到结算金额和手续费金额，只有全部的收银点和全部的收银员才有
     *
     * @param visible
     */
    private void setSettlementAndCommitFeeVisible(boolean visible) {
        if (visible) {
            mLlJsLayout.setVisibility(View.VISIBLE);
            mTvCommissionTitle.setVisibility(View.VISIBLE);
            mTvCommissionFee.setVisibility(View.VISIBLE);
        } else {
            mLlJsLayout.setVisibility(View.GONE);
            mTvCommissionTitle.setVisibility(View.INVISIBLE);
            mTvCommissionFee.setVisibility(View.INVISIBLE);
        }
    }

    public void showFilterPop() {
        if (popupWindow == null) {
            popupWindow = new FilterCashierPopupWindow(getActivity(), new FilterCashierPopupWindow.HandleBtn() {
                @Override
                public void handleOkBtn(String cashierName, String userId) {
                    ReportFragment.this.userId = userId;
                    ReportFragment.this.cashierName = cashierName;
                    setSettlementAndCommitFeeVisible(TextUtils.isEmpty(userId) && (TextUtils.isEmpty(mSiteId)));
                    if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
                        DialogUtil.safeShowDialog(mLoadDialog);
                        getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
                    }
                }
            });
            popupWindow.setOnClickCashier(new FilterCashierPopupWindow.OnClickCashier() {
                @Override
                public void onClickCashier() {
                    Intent intentCashier = new Intent(getActivity(), CashierActivity.class);
                    intentCashier.putExtra(Constants.REQUEST_CASHIER_INTENT, storeMerchantId);
                    if (TextUtils.isEmpty(userId)) {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, "");
                    } else {
                        intentCashier.putExtra(Constants.INTENT_CASHIER_ID, userId);
                    }
                    startActivityForResult(intentCashier, Constants.REQUEST_CASHIER_CODE);
                }
            });
        }
        popupWindow.setCashier(userId, cashierName);
        popupWindow.showAsDropDown(mLlDate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_SHOP_BEAN_CODE) {
            Bundle extras = data.getExtras();
            StoreListBean.DataEntity shopBean = (StoreListBean.DataEntity) extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT);
            mTvShop.setText(shopBean.getStoreName());
            storeName = shopBean.getStoreName();
            storeMerchantId = shopBean.getStoreId();
            if (!MyApplication.getIsCasher()) {
                mTvCashier.setText(getString(R.string.tv_all_user));
                cashierName = getString(R.string.tv_all_user);
                userId = "";
            } else {
                mTvCashier.setText(MyApplication.getRealName());
                cashierName = MyApplication.getRealName();
                userId = MyApplication.getUserId();
            }
            mTvSite.setText(getString(R.string.tv_all_site));
            mSiteId = "";
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));

        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CASHIER_CODE) {
            /*Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            mTvCashier.setText(cashierBean.getRealName());
            userId = cashierBean.getUserId();
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            cashierName = cashierBean.getRealName();*/
            Bundle extras = data.getExtras();
            DataEntity cashierBean = (DataEntity) extras.getSerializable(Constants.RESULT_CASHIER_INTENT);
            popupWindow.setCashier(cashierBean.getUserId(), cashierBean.getRealName());
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CHOICE_DATE) {
            Bundle extras = data.getExtras();
            String time = extras.getString(Constants.RESULT_CHOICE_DATE_INTENT);
            if (!StringUtils.isEmptyOrNull(time)) {
                String t[] = time.split("\\|");
                startTime = t[0];
                endTime = t[1];
                setDateView(startTime, endTime);
                LogUtil.d("date=" + startTime + "///" + endTime);
                getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            }
        }
    }

    public void bluePrint() {
        LogUtil.d("print----2");
        try {
            if (mInfo != null) {
                LogUtil.d("print----3");
                if (!StringUtils.isEmptyOrNull(startTime) && !StringUtils.isEmptyOrNull(endTime)) {
                    mInfo.setStartTime(startTime);
                    long now = System.currentTimeMillis();
                    if (now < DateUtil.getTime(endTime,"yyyy-MM-dd HH:mm:ss")) {
                        mInfo.setEndTime(DateUtil.formatTime(now, "yyyy-MM-dd HH:mm:ss"));
                    } else {
                        mInfo.setEndTime(endTime);
                    }
                } else {
                    mInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                    mInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
                }
                mInfo.setPrintTime(DateUtil.formatTime(System.currentTimeMillis()));
                mInfo.setCashierName(mTvCashier.getText().toString().trim());
                mInfo.setStoreName(mTvShop.getText().toString().trim());
                LogUtil.d("date===" + mInfo.getStartTime() + "//////" + mInfo.getEndTime() + "////" + mInfo.getPrintTime());

                mPosPrintUtil.printReport(printer, woyouService, mPrintUtils, printerDev, aidlPrint, intentService, printerModule, mInfo);

            } else {
                ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.print_data_error));
            }
        } catch (Exception e) {
            LogUtil.d("print----4");
            e.printStackTrace();
        }
    }

    public void setView(ReportBean.DataEntity dataEntity) {
        if (dataEntity != null) {
            if (tab == 3) {//饭卡消费
                mTvFkSettlementMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
                mTvFkPayMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
                mTvFkPayCount.setText(dataEntity.getSuccessCount() + "");
                mTvFkSettlementRate.setText(TextUtils.isEmpty(dataEntity.getSettleRate()) ? "--" : dataEntity.getSettleRate() + "");
                mTvFkSettlementMoneyContent.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
                mTvFkRefundTotalCount.setText(dataEntity.getRefundCount() + "");
                mTvFkRefundTotalMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));
            } else if (tab == 2) {//会员消费
                mTvVerifyActuralMoney.setText(DateUtil.formatMoneyUtil((dataEntity.getPayFee() - dataEntity.getRefundFee()) / 100d));
                mTvVerifyTotalMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
                mTvVerifyTotalCount.setText(dataEntity.getSuccessCount() + "");
                mTvVerifyMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getPayFee() / 100d));
                mTvVerifyCouponMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getMchDiscountsFee() / 100d));
                mTvVerifyRefund.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));
                mTvVerifyRefundNum.setText(dataEntity.getRefundCount() + "");

                mTvCommissionFee.setText(DateUtil.formatMoneyUtil(dataEntity.getCommissionFee() / 100d));
                mTvSettlementFee.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
            } else {//收款
                mTvTotal.setText(DateUtil.formatMoneyUtil((dataEntity.getPayFee() - dataEntity.getRefundFee()) / 100d));
                mTvTotalPay.setText(DateUtil.formatMoneyUtil(dataEntity.getSuccessFee() / 100d));
                mTvTotalPayNum.setText(dataEntity.getSuccessCount() + "");
                mTvRealMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getPayFee() / 100d));
                mTvPayCouponMoney.setText(DateUtil.formatMoneyUtil(dataEntity.getMchDiscountsFee() / 100d));
                mTvTotalRefund.setText(DateUtil.formatMoneyUtil(dataEntity.getRefundFee() / 100d));
                mTvTotalRefundNum.setText(dataEntity.getRefundCount() + "");

                if (MyApplication.isOpenFuncard()) {
                    mTvCommissionTitle.setText(getResources().getString(R.string.tx_report_fw_money));
                    mTvCommissionFee.setText(DateUtil.formatMoneyUtil((dataEntity.getCommissionFee() / 100d) + (dataEntity.getFzSuccessFee() / 100d)));
                } else {
                    if (dataEntity.getFzSuccessFee() > 0) {
                        mTvCommissionTitle.setText(getResources().getString(R.string.tx_report_commission_fw_money));
                        mTvCommissionFee.setText(DateUtil.formatMoneyUtil(dataEntity.getCommissionFee() / 100d) + "/" + DateUtil.formatMoneyUtil(dataEntity.getFzSuccessFee() / 100d));
                    } else {
                        mTvCommissionTitle.setText(getResources().getString(R.string.tx_report_commission_money));
                        mTvCommissionFee.setText(DateUtil.formatMoneyUtil(dataEntity.getCommissionFee() / 100d));
                    }
                }
                mTvSettlementFee.setText(DateUtil.formatMoneyUtil(dataEntity.getSettlementFee() / 100d));
            }
        }
    }

    public boolean isToday(String time) {
        boolean today = false;
        try {
            if (DateUtil.formartDateToMMDD(time).equals(DateUtil.formatMD(System.currentTimeMillis()))) {
                today = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return today;
    }


    class ReportBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
        }
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            StatusBarUtil.setImmersiveStatusBar(getActivity(), true, mView);
            getReport(getRequestMap(startTime, endTime, userId, storeMerchantId));
            /*if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {*/
            /*if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
                WizarPrinterUtil.with().open();
            }*/
        } /*else {
            *//*if (Constants.WJY_WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))
                    || Constants.WIZARPOS.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY))) {*//*
            if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
                WizarPrinterUtil.with().close();
            }
        }*/
    }
}
