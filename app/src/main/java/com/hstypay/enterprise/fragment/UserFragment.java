package com.hstypay.enterprise.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.Widget.SelectPicPopupWindow;
import com.hstypay.enterprise.activity.CaptureActivity;
import com.hstypay.enterprise.activity.ClipImageActivity;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.activity.EmpEnsureActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.MessageActivity;
import com.hstypay.enterprise.activity.QuestionActivity;
import com.hstypay.enterprise.activity.ShopActivity;
import com.hstypay.enterprise.activity.auth.RealNameAuthActivity;
import com.hstypay.enterprise.activity.RegisterActivity;
import com.hstypay.enterprise.activity.SettingActivity;
import com.hstypay.enterprise.activity.ShareActivity;
import com.hstypay.enterprise.activity.SignInfoActivity;
import com.hstypay.enterprise.activity.UpLimitActivity;
import com.hstypay.enterprise.activity.bankcard.BankCardActivity;
import com.hstypay.enterprise.activity.huabei.HBDataActivity;
import com.hstypay.enterprise.activity.merchantInfo.MerchantInfoActivity;
import com.hstypay.enterprise.activity.pledge.PledgePayActivity;
import com.hstypay.enterprise.activity.store.StoreManageActivity;
import com.hstypay.enterprise.activity.storeCode.SetMoneyActivity;
import com.hstypay.enterprise.activity.storeCode.StaticCodeImageActivity;
import com.hstypay.enterprise.activity.storeCode.StoreCodeListActivity;
import com.hstypay.enterprise.activity.vanke.CouponExchangeActivity;
import com.hstypay.enterprise.activity.vipCard.ShareVipActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.InvitateBean;
import com.hstypay.enterprise.bean.LicenseDetailBean;
import com.hstypay.enterprise.bean.MaterialStatusBean;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.listener.OnSingleClickListener;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.ImagePase;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.PermissionUtils;
import com.hstypay.enterprise.utils.ScreenUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.squareup.picasso.Picasso;
import com.v5kf.client.lib.V5ClientConfig;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.pos.pager
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:16
 * @描述: ${TODO}
 */

public class UserFragment extends BaseFragment {
    private View mView;
    private RelativeLayout mLlMerchantManage;
    private LinearLayout mLlSetting, mLlBankCard, mLlMyUpLimit, mLlHuabei, mLlCheckStatus, mLlUserStatus, mLlVisitorStatus;
    private LinearLayout mLlAuthFailedNotice, mLlMerchantInfo, mLlPledge, mLlSign, mLlService, mLlStoreCode, mLlMessage;
    private LinearLayout mLlInviteFriends, mLlVipCard, mLlDeviceManage, mLlMchNotice, mLlCouponExchange;

    private Activity mMainActivity;
//    private ImageView mIvForward;

    private static final int IDENTITY_MERCHANT = 0;//商户
    private static final int IDENTITY_ADMIN = 1;//管理员
    private static final int IDENTITY_CASHIER = 2;//收银员
    private static final int IDENTITY_MANAGER = 3;//管理员
    private static final int IDENTITY_VISITOR = 4;//未进件

    private int examineStatus = -1;//审核状态
    private TextView mTvMchName, mTvTitle, mTvMchCheckStatus, mTvUserRole, mTvLoginName, mTvVisitorPhone, mTvVisitorStatus, mTvMchNoticeBefore, mTvMchNotice;
    private ImageView mIvPhoto, mIvCheckStatus, mIvMerchantInfo;
    private MyBroadCast mMyBroadCast;
    private SafeDialog mLoadDialog;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    public static final int REQUEST_CROP_PHOTO = 0x1003;
    private File tempFile;
    private Uri originalUri;
    private int type = 1;
    private int identity = -1;
    private ImageView mIvRealCheckMch;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMainActivity = getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        ((MainActivity) getActivity()).dismissLoading();
        mView = inflater.inflate(R.layout.fragment_user, container, false);
        StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        initView(mView);
        initEvent();
        initData();
        registerBoradcastReceiver();
        initV5Chat();
        return mView;
    }

    public void initView(View view) {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mLlMerchantManage = view.findViewById(R.id.rl_merchant_manage);
        mLlSetting = (LinearLayout) view.findViewById(R.id.setting);
        mLlMerchantInfo = (LinearLayout) view.findViewById(R.id.ll_merchant_info);
        mLlAuthFailedNotice = (LinearLayout) view.findViewById(R.id.ll_auth_failed_notice);
        mLlPledge = (LinearLayout) view.findViewById(R.id.pledge_pay);
        mLlSign = (LinearLayout) view.findViewById(R.id.ll_sign_contact);
        mLlService = (LinearLayout) view.findViewById(R.id.service);
        mLlStoreCode = (LinearLayout) view.findViewById(R.id.store_code);
        mLlMessage = (LinearLayout) view.findViewById(R.id.message);

        mLlHuabei = (LinearLayout) view.findViewById(R.id.my_huabei);
        mLlVipCard = (LinearLayout) view.findViewById(R.id.my_vip_card);
        mLlBankCard = (LinearLayout) view.findViewById(R.id.ll_bank_card);
        mLlMyUpLimit = (LinearLayout) view.findViewById(R.id.my_up_limit);
        mLlInviteFriends = (LinearLayout) view.findViewById(R.id.my_invite_friends);
        mLlDeviceManage = (LinearLayout) view.findViewById(R.id.device_manage);
        mLlCouponExchange = (LinearLayout) view.findViewById(R.id.coupon_exchange);//兑换券

//        mIvForward = (ImageView) view.findViewById(R.id.iv_title);
        mTvTitle = view.findViewById(R.id.tv_title);
        mIvPhoto = (ImageView) view.findViewById(R.id.iv_photo);
        mIvMerchantInfo = (ImageView) view.findViewById(R.id.iv_merchant_info);
        mTvMchName = (TextView) view.findViewById(R.id.tv_mch_name);
        mTvMchCheckStatus = (TextView) view.findViewById(R.id.tv_mch_check_status);
        mIvCheckStatus = view.findViewById(R.id.iv_check_status);
        mLlCheckStatus = view.findViewById(R.id.ll_check_status);
        mTvUserRole = (TextView) view.findViewById(R.id.tv_user_role);
        mTvLoginName = (TextView) view.findViewById(R.id.tv_login_name);

        mLlUserStatus = view.findViewById(R.id.ll_user_status);
        mLlVisitorStatus = view.findViewById(R.id.ll_visitor_status);
        mTvVisitorPhone = view.findViewById(R.id.tv_visitor_telephone);
        mTvVisitorStatus = view.findViewById(R.id.tv_visitor_check_status);
        mTvMchNotice = view.findViewById(R.id.tv_mch_notice);
        mTvMchNoticeBefore = view.findViewById(R.id.tv_mch_notice_before);
        mLlMchNotice = view.findViewById(R.id.ll_mch_notice);
        mIvRealCheckMch = view.findViewById(R.id.iv_real_check_mch);

        mTvMchName.setText(MyApplication.getMerchantName());
        if (StringUtils.getStrLength(MyApplication.getMerchantName()) > 20) {
            mTvMchName.setText(StringUtils.splitStr(MyApplication.getMerchantName(), 18) + "...");
        } else {
            mTvMchName.setText(MyApplication.getMerchantName());
        }
        if (!TextUtils.isEmpty(MyApplication.getMechantId())) {
            mTvLoginName.setText(MyApplication.getMechantId());
        } else {
            mTvLoginName.setText(MyApplication.getUsername());
        }
        mTvVisitorPhone.setText(MyApplication.getLoginPhone());
        mTvMchNotice.getPaint().setAntiAlias(true);//抗锯齿
    }


    public void initEvent() {
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.coupon_exchange://兑换券
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, CouponExchangeActivity.class));
                        }
                        break;
                    case R.id.pledge_pay://押金收款
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, PledgePayActivity.class));
                        }
                        break;
                    case R.id.ll_sign_contact://签约信息
                        MtaUtils.mtaId(getActivity(), "L002");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, SignInfoActivity.class));
                        }
                        break;
                    case R.id.setting://设置
                        mMainActivity.startActivity(new Intent(mMainActivity, SettingActivity.class));
                        break;
                    case R.id.service://联系客服
                        MtaUtils.mtaId(getActivity(), "Q001");
                /*if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    ((BaseActivity)getActivity()).openDialog();
                } else {*/
                        Intent intent = new Intent(mMainActivity, QuestionActivity.class);
                        intent.putExtra(Constants.REGISTER_INTENT, ConfigUtil.getContactUsUrl());
                        mMainActivity.startActivity(intent);
//                }
                        break;
                    case R.id.device_manage://设备管理
//                MtaUtils.mtaId(getActivity(), "R001");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, DeviceListActivity.class));
                        }
                        break;
                    case R.id.ll_merchant_info://商户信息
                        MtaUtils.mtaId(getActivity(), "L001");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, MerchantInfoActivity.class));
                        }
                        break;
                    case R.id.store_code://门店二维码
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, StoreCodeListActivity.class));
                        }
                        break;
                    case R.id.store_manage://门店管理
                        MtaUtils.mtaId(getActivity(), "M001");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, StoreManageActivity.class));
                        }
                        break;
                    case R.id.message://消息
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, MessageActivity.class));
                        }
                        break;
                    case R.id.my_invite_friends://邀请好友
                        MtaUtils.mtaId(getActivity(), "P001");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            invitationCode();
                        }
                        break;
                    case R.id.my_vip_card://会员卡
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
//                    mMainActivity.startActivity(new Intent(mMainActivity, ShareVipActivity.class));
                            if (MyApplication.getIsMerchant() || MyApplication.getIsCasher()) {
                                mMainActivity.startActivity(new Intent(mMainActivity, ShareVipActivity.class));
                            } else {
                                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                    Map<String, Object> map = new HashMap<>();
                                    map.put("pageSize", 1000);
                                    map.put("currentPage", 1);
                                    map.put("merchantDataType", 1);
                                    ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_VIP_STORE_LIST, map);
                                } else {
                                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                        break;
                    case R.id.ll_bank_card://我的银行卡
                        MtaUtils.mtaId(getActivity(), "O001");
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            mMainActivity.startActivity(new Intent(mMainActivity, BankCardActivity.class));
                        }
                        break;
                    case R.id.my_up_limit://商户提额
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
//                    materialStatus();
                        }
                    case R.id.my_huabei://花呗分期
                        if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                            ((BaseActivity) getActivity()).openDialog();
                        } else {
                            Intent intentHuabei = new Intent(mMainActivity, HBDataActivity.class);
                            intentHuabei.putExtra(Constants.INTENT_NAME, Constants.INTENT_USER_HUABEI);
                            mMainActivity.startActivity(intentHuabei);
                        }
                        break;
                    case R.id.ll_mch_notice:
                        startActivity(new Intent(getActivity(), RealNameAuthActivity.class));
                        break;
                    case R.id.ll_visitor_status:
                        //未认证
                        if (getResources().getString(R.string.wei_ren_zhen).equals(mTvVisitorStatus.getText().toString())) {
                            toRegisterSecond();
                        }
                        break;
                    case R.id.ll_check_status:
                        int status = SpUtil.getInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS);
                        if (getResources().getString(R.string.wei_ren_zhen).equals(mTvMchCheckStatus.getText().toString())) {
                            toRegisterSecond();
                        }
                        break;
                    default:
                        break;
                }
            }
        };
        mLlSetting.setOnClickListener(onSingleClickListener);
        mLlMerchantInfo.setOnClickListener(onSingleClickListener);
        mLlPledge.setOnClickListener(onSingleClickListener);
        mLlSign.setOnClickListener(onSingleClickListener);
        mLlService.setOnClickListener(onSingleClickListener);
        mLlStoreCode.setOnClickListener(onSingleClickListener);
        mLlMessage.setOnClickListener(onSingleClickListener);
        mLlBankCard.setOnClickListener(onSingleClickListener);
        mLlVipCard.setOnClickListener(onSingleClickListener);
        mLlMyUpLimit.setOnClickListener(onSingleClickListener);
        mLlInviteFriends.setOnClickListener(onSingleClickListener);
        mLlDeviceManage.setOnClickListener(onSingleClickListener);
        mLlHuabei.setOnClickListener(onSingleClickListener);
        mLlMchNotice.setOnClickListener(onSingleClickListener);
        mLlCouponExchange.setOnClickListener(onSingleClickListener);
        mLlVisitorStatus.setOnClickListener(onSingleClickListener);
        mLlCheckStatus.setOnClickListener(onSingleClickListener);
    }

    private void initData() {
        if (AppHelper.getAppType() == 1) {
            mLlPledge.setVisibility("2".equals(MyApplication.getPledgeOpenStatus()) && MyApplication.getConfig().contains(Constants.CONFIG_DEPOSIT) ? View.VISIBLE : View.GONE);
            mLlHuabei.setVisibility(MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ) ? View.VISIBLE : View.GONE);
            mLlVipCard.setVisibility(MyApplication.isOpenMember() ? View.VISIBLE : View.GONE);
            //TODO 兑换券暂时关闭
            /*if (AppHelper.getApkType() == 3 && SpUtil.getBoolean(MyApplication.getContext(),"SP_YXX_PROVIDER_TYPE")) {//万科系APP,且开通商场券-印享星
                mLlCouponExchange.setVisibility(View.VISIBLE);
            }*/
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        examineStatus = MyApplication.getExamineStatus();
        setView();
        //1-无微信支付类型； 2-未拿到交易识别码；3-已获取交易识别码，无V3证书，联系客服实名；4-已获取交易识别码，有V3证书，去实名；5-已实名
        //1-无支付宝支付类型； 2-无交易识别码；3-资料未提交；4-商家资料审核中；5-商家资料审核不通过;6-已获取交易识别码，去实名;7-已实名
        /*if (MyApplication.getWxrexStatus() == 3) {
            mTvMchNotice.setText(R.string.tv_mch_notice_service_btn);
            mTvMchNoticeBefore.setText(R.string.tv_mch_notice_service);
            mLlMchNotice.setVisibility(View.VISIBLE);
        } else if (MyApplication.getWxrexStatus() == 4) {
            mTvMchNotice.setText(R.string.tv_mch_notice_real_btn);
            mTvMchNoticeBefore.setText(R.string.tv_mch_notice_real);
            mLlMchNotice.setVisibility(View.VISIBLE);
        } else {
            mLlMchNotice.setVisibility(View.GONE);
        }*/
        if (MyApplication.getAlirexStatus() == 3 || MyApplication.getAlirexStatus() == 4
                || MyApplication.getAlirexStatus() == 5 || MyApplication.getAlirexStatus() == 6
                || MyApplication.getWxrexStatus() == 3 || MyApplication.getWxrexStatus() == 4) {
            mTvMchNotice.setText(R.string.tv_mch_notice_real_btn);
            mTvMchNoticeBefore.setText(R.string.tv_mch_notice_real);
            mLlMchNotice.setVisibility(View.VISIBLE);
        } else {
            mLlMchNotice.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        destoryBoradcastReceiver();
    }

    private void invitationCode() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).invitationCode(MyApplication.getContext(), Constants.TAG_INVITATION_CODE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    //去认证
    private void toRegisterSecond() {
        Intent intentRegister = new Intent(getActivity(), RegisterActivity.class);
        intentRegister.putExtra(Constants.REGISTER_INTENT, Constants.H5_BASE_URL + ConfigUtil.getRegisterSecondUrl());
        startActivity(intentRegister);
    }

    public void setView() {
        if (mLlService == null)
            return;
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_HELP_CENTER)) {
            mLlService.setVisibility(View.VISIBLE);
        } else {
            mLlService.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_SETTING)) {
            mLlSetting.setVisibility(View.VISIBLE);
        } else {
            mLlSetting.setVisibility(View.GONE);
        }
        if (AppHelper.getAppType() == 1 && (MyApplication.getConfig().contains(Constants.CONFIG_DEVICE) || MyApplication.getConfig().contains(Constants.CONFIG_SOUND) || MyApplication.getConfig().contains(Constants.CONFIG_DECCA))) {
            mLlDeviceManage.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_MERCHANT_INFO)) {
            mLlMerchantInfo.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            mLlUserStatus.setVisibility(View.GONE);
            mLlVisitorStatus.setVisibility(View.VISIBLE);
        } else {
            mLlUserStatus.setVisibility(View.VISIBLE);
            mLlVisitorStatus.setVisibility(View.GONE);
        }
        getUserFlag();
        switch (identity) {
            case IDENTITY_MERCHANT:
                mLlCheckStatus.setVisibility(View.VISIBLE);
                mTvUserRole.setText(getString(R.string.tv_merchant));
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_NOTICE)) {
                    mLlMessage.setVisibility(View.VISIBLE);
                } else {
                    mLlMessage.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_MANAGE)) {
                    mLlMerchantManage.setVisibility(View.VISIBLE);
                }

                switch (SpUtil.getInt(MyApplication.getContext(), Constants.SP_ATTESTATION_STATUS)) {
                    case 0://未认证 资料未补件
                        mTvMchCheckStatus.setText(getResources().getString(R.string.wei_ren_zhen));
                        mLlAuthFailedNotice.setVisibility(View.GONE);
                        break;
                    case 1://已认证 资料已补件，审核通过，修改待审核，修改待审核不通过，可交易或部分可交易
                        mTvMchCheckStatus.setText("已认证");
                        mIvCheckStatus.setImageResource(R.mipmap.icon_check_successful);
                        mIvCheckStatus.setVisibility(View.VISIBLE);
                        mLlAuthFailedNotice.setVisibility(View.GONE);

                        mIvRealCheckMch.setVisibility(View.VISIBLE);
                        mLlCheckStatus.setVisibility(View.GONE);
                        break;
                    case 2://认证中 资料已补件，待审核，审核通过，不可交易
                        mTvMchCheckStatus.setText("认证中");
                        mIvCheckStatus.setImageResource(R.mipmap.checking_icon);
                        mIvCheckStatus.setVisibility(View.VISIBLE);
                        mLlAuthFailedNotice.setVisibility(View.GONE);
                        break;
                    case 3://未认证 资料已补件，审核不通过，不可交易（有失败原因）
                        mTvMchCheckStatus.setText(getResources().getString(R.string.wei_ren_zhen));
                        mIvCheckStatus.setVisibility(View.GONE);
                        mLlAuthFailedNotice.setVisibility(View.VISIBLE);
                        break;
                }
                if (examineStatus == 1) {
                    if (AppHelper.getAppType() == 2) {
                        int merchantType = SpUtil.getInt(MyApplication.getContext(), Constants.SP_MERCHANT_TYPE);
                        if (merchantType == 1) {//企业
                            mLlMyUpLimit.setVisibility(View.GONE);
                        } else if (merchantType == 2) {//个人
                            mLlMyUpLimit.setVisibility(View.GONE);
                        }
                        if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_BANK_CARD)) {
                            mLlBankCard.setVisibility(View.VISIBLE);
                        } else {
                            mLlBankCard.setVisibility(View.GONE);
                        }
                    } else if (AppHelper.getAppType() == 1) {
                        //pos 相机，相册权限获取不到
                        mLlBankCard.setVisibility(View.GONE);
                        mLlMyUpLimit.setVisibility(View.GONE);
                    }
                }
                mTvMchCheckStatus.setVisibility(View.VISIBLE);
                if (AppHelper.getAppType() == 2) {
                    if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_SIGN_INFO)) {
                        mLlSign.setVisibility(View.VISIBLE);
                    } else {
                        mLlSign.setVisibility(View.GONE);
                    }
                    mLlStoreCode.setVisibility(View.GONE);
                } else if (AppHelper.getAppType() == 1) {
                    if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
                        //屏蔽万科银商
                        if (!Constants.WJY_YINSHANG.equals(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY)))
                            mLlStoreCode.setVisibility(View.VISIBLE);
                    }
                    if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_SIGN_INFO)) {
                        mLlSign.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case IDENTITY_ADMIN:
            case IDENTITY_MANAGER:
                mLlCheckStatus.setVisibility(View.GONE);
                mTvUserRole.setText(getString(R.string.tv_manager));
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_NOTICE)) {
                    mLlMessage.setVisibility(View.VISIBLE);
                } else {
                    mLlMessage.setVisibility(View.GONE);
                }
                mLlMerchantManage.setVisibility(View.GONE);
                mTvMchCheckStatus.setVisibility(View.GONE);
                mLlMerchantInfo.setEnabled(false);
                if (AppHelper.getAppType() == 1) {
                    if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
                        mLlStoreCode.setVisibility(View.VISIBLE);
                    } else {
                        mLlStoreCode.setVisibility(View.GONE);
                    }
                }
                break;
            case IDENTITY_CASHIER:
                mLlCheckStatus.setVisibility(View.GONE);
                mTvUserRole.setText(getString(R.string.string_role_store_cashier));
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_NOTICE)) {
                    mLlMessage.setVisibility(View.VISIBLE);
                } else {
                    mLlMessage.setVisibility(View.GONE);
                }
                mLlMerchantManage.setVisibility(View.GONE);
                mTvMchCheckStatus.setVisibility(View.GONE);
                mLlMerchantInfo.setEnabled(false);
                if (AppHelper.getAppType() == 1) {
                    if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
                        mLlStoreCode.setVisibility(View.VISIBLE);
                    } else {
                        mLlStoreCode.setVisibility(View.GONE);
                    }
                }
                break;
            case IDENTITY_VISITOR:
                mTvUserRole.setText(getString(R.string.tv_visitor));
                mLlCheckStatus.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_MERCHANT_INFO)) {
                    mLlMerchantManage.setVisibility(View.VISIBLE);
                } else {
                    mLlMerchantManage.setVisibility(View.GONE);
                }
                mTvUserRole.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_SIGN_INFO)) {
                    mLlSign.setVisibility(View.VISIBLE);
                }
                if (AppHelper.getAppType() == 1) {
                    if (!TextUtils.isEmpty(MyApplication.getConfig()) && MyApplication.getConfig().contains(Constants.CONFIG_STORE_QRCODE)) {
                        mLlStoreCode.setVisibility(View.VISIBLE);
                    } else {
                        mLlStoreCode.setVisibility(View.GONE);
                    }
                }
                break;
            default:

                break;
        }
    }

    public void getUserFlag() {
        if (TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            identity = 4;
        } else {
            if (MyApplication.getIsMerchant()) {
                identity = 0;
            } else if (MyApplication.getIsAdmin()) {
                identity = 1;
            } else if (MyApplication.getIsCasher()) {
                identity = 2;
            } else if (MyApplication.getIsManager()) {
                identity = 3;
            }
        }
    }

    public void registerBoradcastReceiver() {
        IntentFilter myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(Constants.ACTION_MERCHANT_STATUS);
        //注册广播
        mMyBroadCast = new MyBroadCast();
        getActivity().registerReceiver(mMyBroadCast, myIntentFilter);
    }

    public void destoryBoradcastReceiver() {
        getActivity().unregisterReceiver(mMyBroadCast);
    }

    public class MyBroadCast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.ACTION_MERCHANT_STATUS)) {
                examineStatus = MyApplication.getExamineStatus();
                setView();
            }
        }
    }

    public void materialStatus() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).materialStatus(MyApplication.getContext(), Constants.TAG_STATUS_UP_LIMIT, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getUpLimitInfo() {
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).licenseDetail(MyApplication.getContext(), Constants.TAG_LICENSE_DETAIL, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LICENSE_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LicenseDetailBean msg = (LicenseDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    Intent intent = new Intent(mMainActivity, UpLimitActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.INTENT_LICENSE_DETAL, msg.getData());
                    intent.putExtras(bundle);
                    mMainActivity.startActivity(intent);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_INVITATION_CODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InvitateBean msg = (InvitateBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        if (msg.getData() != null) {
                            Intent intent = new Intent(mMainActivity, ShareActivity.class);
                            intent.putExtra(Constants.INTENT_CODE_URL, msg.getData().getUrl());
                            intent.putExtra(Constants.INTENT_CODE_TIMEOUT, msg.getData().getTimeout());
                            mMainActivity.startActivity(intent);
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_STATUS_UP_LIMIT)) {
            MaterialStatusBean info = (MaterialStatusBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (info != null) {
                        if (info.getError() != null) {
                            if (info.getError().getCode() != null) {
                                if (info.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (info.getError().getMessage() != null) {
                                        ((MainActivity) getActivity()).getLoginDialog(getActivity(), info.getError().getMessage());
                                    }
                                } else {
                                    if (info.getError().getMessage() != null) {
                                        ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), info.getError().getMessage());
                                    }
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (info.getData() != null) {
                        MaterialStatusBean.DataEntity data = info.getData();
                        if (data.getExamineStatus() == 1) {
                            getUpLimitInfo();
                        } else {
                            if ("1".equals(data.getStandby8())) {
                                getUpLimitInfo();
                            } else {
                                ((MainActivity) getActivity()).showCommonNoticeDialog(getActivity(), UIUtils.getString(R.string.toast_not_material_change));
                            }
                        }
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_OPEN_MEMBER)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, msg.getData().getRetCode());
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, msg.getData().isStatisticsCode());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_OPEN_MEMBER, false);
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_SHOW_COLLECT_MEMBER, false);
                    }
                    mLlVipCard.setVisibility(MyApplication.isOpenMember() ? View.VISIBLE : View.GONE);
                    break;
            }
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(getActivity(), new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        MyToast.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }
        });

        picPopupWindow.showAtLocation(mIvPhoto, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    String imageUrl;

    private void startCamrae() throws Exception {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";

        tempFile = new File(imageUrl);
        if (!tempFile.getParentFile().exists()) {
            tempFile.getParentFile().mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            //如果是则使用FileProvider
            originalUri = FileProvider.getUriForFile(getActivity(),
                    Constants.FILE_PROVIDER, tempFile);
        } else {
            originalUri = Uri.fromFile(tempFile);
        }
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    Uri uri = data.getData();
                    gotoClipActivity(uri);
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    gotoClipActivity(Uri.fromFile(tempFile));
                    break;
                case REQUEST_CROP_PHOTO:  //剪切图片返回
                    if (resultCode == Activity.RESULT_OK) {
                        final Uri clipUri = data.getData();
                        if (clipUri == null) {
                            return;
                        }
                        String cropImagePath = AppHelper.getPicPath(clipUri);
                        Bitmap bitMap = BitmapFactory.decodeFile(cropImagePath);
                        /*if (type == 1) {
                            mIvPhoto.setImageBitmap(bitMap);
                        } else {*/
                        mIvPhoto.setImageBitmap(bitMap);
//                        }
                        //此处后面可以将bitMap转为二进制上传后台网络
                        //......

                    }
                    break;
            }
        }
    }

    /**
     * 打开截图界面
     */
    public void gotoClipActivity(Uri uri) {
        if (uri == null) {
            return;
        }
        Intent intent = new Intent();
        intent.setClass(getActivity(), ClipImageActivity.class);
        intent.putExtra(Constants.INTENT_CLIP_TYPE, type);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_CROP_PHOTO);
    }

    /**
     * 初始化V5KF 界面
     */
    private void initV5Chat() {
        // V5客服系统客户端配置
        V5ClientConfig config = V5ClientConfig.getInstance(MyApplication.getContext());
        V5ClientConfig.USE_HTTPS = true; // 使用加密连接，默认true
        V5ClientConfig.SOCKET_TIMEOUT = 20000; // 请求超时时间
        config.setHeartBeatEnable(true); // 是否允许发送心跳包保活
        config.setHeartBeatTime(30000); // 心跳包间隔时间ms
        config.setShowLog(true); // 显示日志，默认为true
        config.setLogLevel(V5ClientConfig.LOG_LV_DEBUG); // 显示日志级别，默认为全部显示

        /*if (!TextUtils.isEmpty(MyApplication.getRealName())) {
            config.setNickname(StringUtils.hideFirstName(MyApplication.getRealName()));
        } else if (!TextUtils.isEmpty(MyApplication.getUsername())) {
            config.setNickname(StringUtils.hideLoginName(MyApplication.getUsername()));
        }*/
        config.setNickname(MyApplication.getMechantId());
        config.setGender(0); // 设置用户性别: 0-未知  1-男  2-女
        // 设置用户头像URL
        //config.setAvatar("http://debugimg-10013434.image.myqcloud.com/fe1382d100019cfb572b1934af3d2c04/thumbnail");
        config.setVip(0); // 设置用户VIP等级（0-5）
        /**
         *【建议】设置用户OpenId，以识别不同登录用户，不设置则默认由SDK生成，替代v1.2.0之前的uid,
         *  openId将透传到座席端(建议使用含字母数字和下划线的字符串，尽量不用特殊字符，若含特殊字符系统会进行URL encode处理)
         *	若您是旧版本SDK用户，只是想升级，为兼容旧版，避免客户信息改变可继续使用config.setUid，可不用openId
         */
        config.setOpenId(MyApplication.getUserId());
        //config.setUid(uid); //【弃用】请使用setOpenId替代
        // 设置device_token：集成第三方推送(腾讯信鸽、百度云推)时设置此参数以在离开会话界面时接收推送消息
        //config.setDeviceToken(XGPushConfig.getToken(getApplicationContext())); // 【建议】设置deviceToken

        // 客户信息键值对（JSONObject）
        JSONObject customContent = new JSONObject();
        try {
            customContent.put("APP名称", AppHelper.getAppName(MyApplication.getContext()));
            customContent.put("APP本版号", AppHelper.getVerName(MyApplication.getContext()));
            customContent.put("安卓系统版本", AppHelper.getAndroidSDKVersionName());
            customContent.put("手机型号", Build.BRAND + ";" + Build.MODEL);
            if (!TextUtils.isEmpty(AppHelper.getIPAddress(MyApplication.getContext())))
                customContent.put("IP", AppHelper.getIPAddress(MyApplication.getContext()));
            customContent.put("APP_CHANNEL", AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // 设置客户信息（自定义JSONObjectjian键值对，开启会话前设置）
        config.setUserInfo(customContent);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            StatusBarUtil.setTranslucentStatus(getActivity(), mView);
            if (AppHelper.getAppType() == 1) {
                mLlPledge.setVisibility("2".equals(MyApplication.getPledgeOpenStatus()) && MyApplication.getConfig().contains(Constants.CONFIG_DEPOSIT) ? View.VISIBLE : View.GONE);
                mLlHuabei.setVisibility(MyApplication.isHuabeiWhite() && MyApplication.getConfig().contains(Constants.CONFIG_HBFQ) ? View.VISIBLE : View.GONE);
            }
        }
    }
}
