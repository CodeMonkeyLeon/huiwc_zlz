package com.hstypay.enterprise.fragment;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CustomLinearLayoutManager;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.vipCard.ShareVipActivity;
import com.hstypay.enterprise.activity.vipNew.ShareVipWechatActivity;
import com.hstypay.enterprise.activity.vipNew.VipInfoActivity;
import com.hstypay.enterprise.adapter.MsgRecyclerAdapter;
import com.hstypay.enterprise.adapter.vip.VipAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.HSVipCollectBean;
import com.hstypay.enterprise.bean.HSVipListBean;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.bean.WhiteListBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.NumberUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StatusBarUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/6/28 10:15
 * @描述: ${TODO}
 */

public class VipFragment extends BaseFragment implements MsgRecyclerAdapter.OnRecyclerViewItemClickListener, VipAdapter.OnItemClickListener, View.OnClickListener {
    private View mView;
    private ImageView mIvClean, mIvVipCard;
    private EditText mEtInput;
    private TextView mTvTitle, mTvTotalVipCount, mTvAddVipCount;
    private RecyclerView mRecyclerView;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private VipAdapter mAdapter;
    private List<HSVipListBean.DataEntity.DataListEntity> mList;
    private RelativeLayout mRlVip, mRlInput;
    private LinearLayout mLlVipOpen, mLlVipEmpty, mLlVipList;
    private int currentPage = 2;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private SafeDialog mLoadDialog;
    private ImageView mIvBack;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        currentPage = 2;
        mView = inflater.inflate(R.layout.fragment_vip, container, false);
        StatusBarUtil.setTranslucentStatus(getActivity(), mView);
        initView(mView);
        initEvent();
        if (!TextUtils.isEmpty(MyApplication.getIsSuccessData())) {
            initData();
        }
        return mView;
    }

    public void initView(View view) {
        mLoadDialog = ((BaseActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mTvTitle = view.findViewById(R.id.tv_title);
        mRlVip = view.findViewById(R.id.rl_vip);
        mRlInput = view.findViewById(R.id.rl_input);
        mLlVipOpen = view.findViewById(R.id.ll_vip_open);
        mTvTotalVipCount = view.findViewById(R.id.tv_total_vip_count);
        mTvAddVipCount = view.findViewById(R.id.tv_add_vip_count);
        mIvClean = view.findViewById(R.id.iv_clean);
        mEtInput = view.findViewById(R.id.et_user_input);
        mLlVipEmpty = view.findViewById(R.id.ll_vip_empty);
        mLlVipList = view.findViewById(R.id.ll_vip_list);
        mIvVipCard = view.findViewById(R.id.iv_get_vip_card);
        mIvBack = view.findViewById(R.id.iv_back);

        mTvTitle.setText("会员");

        initRecyclerView(view);
        initSwipeRefreshLayout(view);
    }

    private void initRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    queryMemberCollect(false);
                    getInitData(false);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getVipList(pageSize, currentPage);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void initEvent() {
        mIvVipCard.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mIvClean.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
                if (s.length() == 0) {
                    getInitData(true);
                    mIvClean.setVisibility(View.GONE);
                } else {
                    mIvClean.setVisibility(View.VISIBLE);
                }
            }
        });
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getInitData(true);
                    return true;
                }
                return false;
            }
        });

        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    public void initData() {
        mList = new ArrayList<>();
        mAdapter = new VipAdapter(getActivity(), mList);
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        setView();
    }

    private void setView() {
        if (MyApplication.isOpenMember()) {
            if ((SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM) || SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST)) && !SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CARD_CONFIG_FINISH)) {
                getCardconfigquery();
            } else {
                mRlVip.setVisibility(View.VISIBLE);
                mIvVipCard.setVisibility(View.VISIBLE);
                mLlVipOpen.setVisibility(View.GONE);
                queryMemberCollect(true);
                getInitData(true);
            }
        } else {
            mRlVip.setVisibility(View.GONE);
            mIvVipCard.setVisibility(View.GONE);
            mLlVipOpen.setVisibility(View.VISIBLE);
        }
    }

    private void queryMemberCollect(boolean isShowDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (isShowDialog)
                DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).queryMemberCollect(MyApplication.getContext(), Constants.TAG_HS_VIP_COLLECT, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getVipList(int pageSize, int currentPage) {
        Map<String, Object> map = new HashMap<>();
        map.put("limit", pageSize);
        map.put("offset", currentPage);
        if (!TextUtils.isEmpty(mEtInput.getText().toString().trim()))
            map.put("mobile", mEtInput.getText().toString().trim());
        ServerClient.newInstance(MyApplication.getContext()).vipList(MyApplication.getContext(), Constants.TAG_HS_VIP_LIST, map);
    }

    /**
     * 卡配置查询
     */
    private void getCardconfigquery() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).cardconfigquery(MyApplication.getContext(), Constants.TAG_CARD_CONFIG_QUERY, null);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private void getInitData(boolean showDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (showDialog)
                DialogUtil.safeShowDialog(mLoadDialog);
            mList.clear();
            isRefreshed = true;
            currentPage = 1;
            getVipList(pageSize, currentPage);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CARD_CONFIG_QUERY)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            WhiteListBean msg = (WhiteListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(),msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && "1".equals(msg.getData().getFinishConfig())) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CARD_CONFIG_FINISH, true);
                        mRlVip.setVisibility(View.VISIBLE);
                        mIvVipCard.setVisibility(View.VISIBLE);
                        mLlVipOpen.setVisibility(View.GONE);
                        queryMemberCollect(true);
                        getInitData(true);
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_CARD_CONFIG_FINISH, false);
                        mRlVip.setVisibility(View.GONE);
                        mIvVipCard.setVisibility(View.GONE);
                        mLlVipOpen.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_HS_VIP_COLLECT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            HSVipCollectBean msg = (HSVipCollectBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        mTvTotalVipCount.setText(NumberUtil.formatNumber(msg.getData().getTotalMembers()));
                        mTvAddVipCount.setText(NumberUtil.formatNumber(msg.getData().getNewMemberToday()));
                        if (msg.getData().getTotalMembers() > 0) {
                            mRlInput.setVisibility(View.VISIBLE);
                        } else {
                            mRlInput.setVisibility(View.GONE);
                        }
                    } else {
                        mTvTotalVipCount.setText("0");
                        mTvAddVipCount.setText("0");
                        mRlInput.setVisibility(View.GONE);
                        mLlVipEmpty.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_HS_VIP_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            HSVipListBean msg = (HSVipListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    setErrorState(isRefreshed, isLoadmore, 500);
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().getDataList() != null && msg.getData().getDataList().size() > 0) {
                        mList.addAll(msg.getData().getDataList());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            MyToast.showToast(UIUtils.getString(R.string.no_nore), Toast.LENGTH_SHORT);
                        }
                    }
                    if (mList.size() > 0) {
                        mLlVipEmpty.setVisibility(View.GONE);
                    } else {
                        mLlVipEmpty.setVisibility(View.VISIBLE);
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        } else if (event.getTag().equals(Constants.TAG_MEMBER_WECAHT_CODE_URL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToastShort(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        getActivity().startActivity(new Intent(getActivity(), ShareVipWechatActivity.class).putExtra(Constants.INTENT_CODE_URL,msg.getData().toString()));
                    } else {
                        ((BaseActivity) getActivity()).showCommonNoticeDialog(getActivity(), getString(R.string.error_qrcode));
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            mSwipeRefreshLayout.setLoadmoreEnable(true);
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), VipInfoActivity.class);
        intent.putExtra(Constants.INTENT_VIP_CARD_ID, mList.get(position).getCardId());
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_clean:
                mEtInput.setText("");
                break;
            case R.id.iv_get_vip_card:
                //2.0平台注册或者1.0平台注册后切换到2.0平台
                getVipCardCode();
                break;
        }
    }

    private void getVipCardCode() {
        if (SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_PLATFORM_WHITE_LIST)
                || SpUtil.getBoolean(MyApplication.getContext(), Constants.IS_CHANGE_TO_NEW_PLATFORM)) {
            if (!NetworkUtils.isNetworkAvailable(getActivity())) {
                MyToast.showToastShort(getString(R.string.network_exception));
            } else {
                DialogUtil.safeShowDialog(mLoadDialog);
                ServerClient.newInstance(getActivity()).queryNewMemberCode(getActivity(), Constants.TAG_MEMBER_WECAHT_CODE_URL, null);
            }
        } else {
            if (MyApplication.getIsMerchant() || MyApplication.getIsCasher()) {
                getActivity().startActivity(new Intent(getActivity(), ShareVipActivity.class));
            } else {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    ((BaseActivity) getActivity()).loadDialog(getActivity(), UIUtils.getString(R.string.public_loading));
                    Map<String, Object> map = new HashMap<>();
                    map.put("pageSize", 1000);
                    map.put("currentPage", 1);
                    map.put("merchantDataType", 1);
                    ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_VIP_STORE_LIST, map);
                } else {
                    MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                }
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setView();
        }
    }
}