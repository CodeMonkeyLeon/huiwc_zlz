package com.hstypay.enterprise.fragment;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.setting.VoiceSetActivity;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;

import static android.app.Activity.RESULT_OK;

/**
 * @author zeyu.kuang
 * @time 2020/8/10
 * @desc 语音播报设置，废弃
 */
public abstract class VoiceSetBaseFragment extends BaseFragment implements View.OnClickListener{

    protected VoiceSetActivity mContext;
    protected View mRootView;
    protected ImageView mIvVoiceSwitch;//语音播报开关
    protected RelativeLayout mRlVoiceSetStore;//rl门店选择
    protected TextView mTvVoiceStoreCount;//tv门店选择个数
    protected String mVoiceSelectStoreCount;//已选择的播报门店数量
    protected SafeDialog mLoadDialog;
    protected TextView mTv_voice_set_type_title;//语音播报设置项标题
    protected TextView mTv_voice_set_tip;//语音播报设置项提示

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (VoiceSetActivity) getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = LayoutInflater.from(mContext).inflate(R.layout.fragment_voice_set, container, false);
        initView();
        initData();
        initEvent();
        return mRootView;
    }


    protected void initEvent() {
        mIvVoiceSwitch.setOnClickListener(this);
        mRlVoiceSetStore.setOnClickListener(this);
    }

    protected void initData() {

    }

    protected void initView() {
        mLoadDialog = mContext.getLoadDialog(mContext, getString(R.string.public_loading), false);
        mTv_voice_set_type_title = mRootView.findViewById(R.id.tv_voice_set_type_title);
        mIvVoiceSwitch = mRootView.findViewById(R.id.iv_voice_switch);
        mTv_voice_set_tip = mRootView.findViewById(R.id.tv_voice_set_tip);
        mRlVoiceSetStore = mRootView.findViewById(R.id.rl_voice_set_store);
        mTvVoiceStoreCount = mRootView.findViewById(R.id.tv_voice_store_count);
        mTvVoiceStoreCount.setVisibility(View.GONE);
        setSwitchView(getVoiceInitSwitch());
        if (!TextUtils.isEmpty(mVoiceSelectStoreCount)){
            setSelectStoreCountView(mVoiceSelectStoreCount);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_voice_switch:
                onClickVoiceSwitch();
                break;
            case R.id.rl_voice_set_store:
                onClickSelectVoiceStore();
                break;
            default:
                break;
        }
    }

    /**
     * 设置语音开关状态
     * @param openVoice 本地开关状态
     */
    public void setSwitchView(boolean openVoice) {
        mIvVoiceSwitch.setImageResource(openVoice ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
//        mRlVoiceSetStore.setVisibility(openVoice ? View.VISIBLE : View.GONE);
    }

    /**
     * 更新选中的门店个数
     * */
    public void setSelectStoreCountView(String voiceDaoJiaStoreCount) {
        mVoiceSelectStoreCount = voiceDaoJiaStoreCount;
        if (mTvVoiceStoreCount!=null){
            mTvVoiceStoreCount.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(mVoiceSelectStoreCount)||Integer.parseInt(mVoiceSelectStoreCount)<=0) {
                mTvVoiceStoreCount.setText("未选择");
            } else {
                mTvVoiceStoreCount.setText("已选择" + mVoiceSelectStoreCount + "家");
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_VOICE_STORE) {
            if (data!=null){
                int selectCount = data.getIntExtra(Constants.INTENT_VOICE_SELECT_STORE_COUNT, Constants.SELECT_STORE_COUNT_DEFAULT);
                if (selectCount != Constants.SELECT_STORE_COUNT_DEFAULT){
                    setSelectStoreCountView(""+selectCount);
                }else {
                    LogUtil.d("门店列表获取失败，不需要改变选择门店的个数");
                }
            }
        }
    }

    /**
     * 点击了门店选择
     */
    protected abstract void onClickSelectVoiceStore();

    /**
     * 点击了语音播报开关按钮
     */
    protected abstract void onClickVoiceSwitch();

    /**
     * 获取语音开关初始状态值
     * @return
     */
    protected abstract boolean getVoiceInitSwitch();
}
