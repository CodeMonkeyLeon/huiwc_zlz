package com.hstypay.enterprise.fragment;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.setting.VoiceSetActivity;
import com.hstypay.enterprise.activity.setting.VoiceStoreListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.Info;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SpUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static android.app.Activity.RESULT_OK;

/**
 * @author zeyu.kuang
 * @time 2020/8/10
 * @desc 设置威到家语音播报的fragment
 */
public class VoiceSetDaoJiaFragment extends BaseFragment implements View.OnClickListener {

    protected VoiceSetActivity mContext;
    protected View mRootView;
    protected ImageView mIvVoiceSwitch;//语音播报开关
    protected RelativeLayout mRlVoiceSetStore;//rl门店选择
    protected TextView mTvVoiceStoreCount;//tv门店选择个数
    protected String mVoiceSelectStoreCount;//已选择的播报门店数量
    protected SafeDialog mLoadDialog;
    protected TextView mTv_voice_set_type_title;//语音播报设置项标题
    protected TextView mTv_voice_set_tip;//语音播报设置项提示

    private FrameLayout mFlVoiceSetDaojia;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (VoiceSetActivity) getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = LayoutInflater.from(mContext).inflate(R.layout.fragment_voice_set_daojia, container, false);
        initView();
        initData();
        initEvent();
        return mRootView;
    }

    protected void initEvent() {
        mIvVoiceSwitch.setOnClickListener(this);
        mRlVoiceSetStore.setOnClickListener(this);
    }

    protected void initData() {
        setSwitchView();
        if (!TextUtils.isEmpty(mVoiceSelectStoreCount)) {
            setSelectStoreCountView(mVoiceSelectStoreCount);
        }
        if (!MyApplication.isOpenSendHome()) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).queryDaoJiaEnable(MyApplication.getContext(), Constants.TAG_VOICE_SET_QUERY_DAOJIA, null);
        } else {
            setDaojiaVoiceVisibility(true);
        }
    }

    protected void initView() {
        mLoadDialog = mContext.getLoadDialog(mContext, getString(R.string.public_loading), false);
        mTv_voice_set_type_title = mRootView.findViewById(R.id.tv_voice_set_type_title);
        mIvVoiceSwitch = mRootView.findViewById(R.id.iv_voice_switch);
        mTv_voice_set_tip = mRootView.findViewById(R.id.tv_voice_set_tip);
        mRlVoiceSetStore = mRootView.findViewById(R.id.rl_voice_set_store);
        mRlVoiceSetStore.setVisibility(MyApplication.isOpenDaoJiaOrderVoice() ? View.VISIBLE : View.GONE);
        mTvVoiceStoreCount = mRootView.findViewById(R.id.tv_voice_store_count);
        mTvVoiceStoreCount.setVisibility(View.GONE);

        mFlVoiceSetDaojia = mContext.findViewById(R.id.fl_voice_set_daojia);
        mFlVoiceSetDaojia.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_voice_switch:
                onClickVoiceSwitch();
                break;
            case R.id.rl_voice_set_store:
                onClickSelectVoiceStore();
                break;
            default:
                break;
        }
    }

    /**
     * 点击了门店选择
     */
    protected void onClickSelectVoiceStore() {
        Intent intentVoiceStore = new Intent(getActivity(), VoiceStoreListActivity.class);
        intentVoiceStore.putExtra(Constants.INTENT_TYPE_SELECT_STORE_VOICE, Constants.INTENT_VALUE_VOICE_ORDER_DAOJIA);
        startActivityForResult(intentVoiceStore, Constants.REQUEST_VOICE_STORE);
    }

    /**
     * 点击了语音播报开关按钮
     */
    protected void onClickVoiceSwitch() {
        if (MyApplication.isOpenDaoJiaOrderVoice()) {
            MyApplication.setOpenDaoJiaOrderVoice(false);
        } else {
            MyApplication.setOpenDaoJiaOrderVoice(true);
        }
        setSwitchView();
        mRlVoiceSetStore.setVisibility(MyApplication.isOpenDaoJiaOrderVoice() ? View.VISIBLE : View.GONE);
    }

    /**
     * 设置到家语音播报的开关状态
     */
    public void setSwitchView() {
        mIvVoiceSwitch.setImageResource(MyApplication.isOpenDaoJiaOrderVoice() ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_VOICE_STORE) {
            if (data != null) {
                int selectCount = data.getIntExtra(Constants.INTENT_VOICE_SELECT_STORE_COUNT, Constants.SELECT_STORE_COUNT_DEFAULT);
                if (selectCount != Constants.SELECT_STORE_COUNT_DEFAULT) {
                    setSelectStoreCountView("" + selectCount);
                } else {
                    LogUtil.d("门店列表获取失败，不需要改变选择门店的个数");
                }
            }
        }
    }

    /**
     * 更新选中的门店个数
     */
    public void setSelectStoreCountView(String voiceDaoJiaStoreCount) {
        mVoiceSelectStoreCount = voiceDaoJiaStoreCount;
        if (mTvVoiceStoreCount != null) {
            mTvVoiceStoreCount.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(mVoiceSelectStoreCount) || Integer.parseInt(mVoiceSelectStoreCount) <= 0) {
                mTvVoiceStoreCount.setText("未选择");
            } else {
                mTvVoiceStoreCount.setText("已选择" + mVoiceSelectStoreCount + "家");
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDaojiaVoice(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VOICE_SET_QUERY_DAOJIA)) {//查询是否开通到家业务的返回
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    MyToast.showToast(getString(R.string.net_error), Toast.LENGTH_SHORT);
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    MyToast.showToast(msg.getError().getMessage(), Toast.LENGTH_SHORT);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, (boolean) msg.getData());
                    } else {
                        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_DAOJIA_ENABLE, false);
                    }
                    setDaojiaVoiceVisibility(MyApplication.isOpenSendHome());
                    break;
                default:
                    break;
            }
        }
    }

    private void setDaojiaVoiceVisibility(boolean visibility) {
        mFlVoiceSetDaojia.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

}
