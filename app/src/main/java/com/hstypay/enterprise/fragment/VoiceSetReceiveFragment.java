package com.hstypay.enterprise.fragment;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SafeDialog;
import com.hstypay.enterprise.activity.PayResultActivity;
import com.hstypay.enterprise.activity.setting.VoiceSetActivity;
import com.hstypay.enterprise.activity.setting.VoiceStoreListActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.bean.ActiveBean;
import com.hstypay.enterprise.bean.ShareBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.UIUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static android.app.Activity.RESULT_OK;

/**
 * @author zeyu.kuang
 * @time 2020/8/10
 * @desc 设置收款语音播报的fragment
 */
public class VoiceSetReceiveFragment extends BaseFragment implements View.OnClickListener{

    protected VoiceSetActivity mContext;
    protected View mRootView;
    protected ImageView mIvVoiceSwitch;//收款语音播报总开关
    protected RelativeLayout mRlVoiceSetStore;//rl门店选择
    protected TextView mTvVoiceStoreCount;//tv门店选择个数
    protected String mVoiceSelectStoreCount;//已选择的播报门店数量
    protected SafeDialog mLoadDialog;
    protected TextView mTv_voice_set_type_title;//语音播报设置项标题

    private LinearLayout mLlCancelPayWarnVoiceSet;//防逃单语音播报设置
    private ImageView mIvVoiceSwitchCancelPay;//支付防逃单开关
    private LinearLayout mLlNotApiVoiceSet;//非API交易
    private ImageView mIvVoiceSwitchNotApi;//非API交易播报开关
    private LinearLayout mLlApiVoiceSet;//API交易
    private ImageView mIvVoiceSwitchApi;//API交易播报开关
    private TextView mTvVoiceSetTip;//语音播报提示语
    private TextView tvTitleNotApiVoice;
    private boolean apiVoiceBroadcastButton;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (VoiceSetActivity) getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = LayoutInflater.from(mContext).inflate(R.layout.fragment_voice_set_receive, container, false);
        initView();
        initData();
        initEvent();
        return mRootView;
    }

    protected void initEvent() {
        mIvVoiceSwitch.setOnClickListener(this);
        mRlVoiceSetStore.setOnClickListener(this);
        mIvVoiceSwitchNotApi.setOnClickListener(this);
        mIvVoiceSwitchApi.setOnClickListener(this);
        mIvVoiceSwitchCancelPay.setOnClickListener(this);
    }

    protected void initData() {
        setSwitchView();
        if (!TextUtils.isEmpty(mVoiceSelectStoreCount)){
            setSelectStoreCountView(mVoiceSelectStoreCount);
        }
        setNotApiPaySwitchView();
        setApiPaySwitchView();
        setCancelPaySwitchView();

        queryApiPayEnable();

    }

    //查询是否配置了API交易的推送
    private void queryApiPayEnable(){
        if (NetworkUtils.isNetworkAvailable(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).printActive(MyApplication.getContext(), Constants.TAG_API_TRADE_VOICE_ENABLE, null);
        } else {
            MyToast.showToastShort(UIUtils.getString(R.string.network_exception));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_API_TRADE_VOICE_ENABLE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ActiveBean info = (ActiveBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE:
                    apiVoiceBroadcastButton = info.getData().isApiVoiceBroadcastButton();
                    if (apiVoiceBroadcastButton){
                        //配置了API交易的推送
                        mLlApiVoiceSet.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
                        tvTitleNotApiVoice.setText(getResources().getString(R.string.tv_voice_set_not_api));
                    }else {
                        //没配置API交易的推送
                        mLlApiVoiceSet.setVisibility(View.GONE);
                        tvTitleNotApiVoice.setText(getResources().getString(R.string.voice_notice_collection));
                        MyApplication.setOpenApiVoice(false);

                    }
                    break;
            }
        }
    }

    protected void initView() {
        mLoadDialog = mContext.getLoadDialog(mContext, getString(R.string.public_loading), false);
        mTv_voice_set_type_title = mRootView.findViewById(R.id.tv_voice_set_type_title);
        mIvVoiceSwitch = mRootView.findViewById(R.id.iv_voice_switch);
        mTvVoiceSetTip = mRootView.findViewById(R.id.tv_voice_set_tip);
        mTvVoiceSetTip.setText(MyApplication.isOpenAllVoice()?getResources().getString(R.string.tv_voice_pay_open_tip):getResources().getString(R.string.tv_voice_pay_close_tip));
        mRlVoiceSetStore = mRootView.findViewById(R.id.rl_voice_set_store);
        mRlVoiceSetStore.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mTvVoiceStoreCount = mRootView.findViewById(R.id.tv_voice_store_count);
        mTvVoiceStoreCount.setVisibility(View.GONE);
        //非API交易
        mLlNotApiVoiceSet = mRootView.findViewById(R.id.ll_not_api_voice_set);
        mLlNotApiVoiceSet.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mIvVoiceSwitchNotApi = mRootView.findViewById(R.id.iv_voice_switch_not_vpi);
        tvTitleNotApiVoice = mRootView.findViewById(R.id.tv_title_not_api_voice);
        //API交易
        mLlApiVoiceSet = mRootView.findViewById(R.id.ll_api_voice_set);
        mLlApiVoiceSet.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mIvVoiceSwitchApi = mRootView.findViewById(R.id.iv_voice_switch_vpi);
        //防逃单
        mLlCancelPayWarnVoiceSet = mRootView.findViewById(R.id.ll_cancel_pay_warn_voice_set);
        mLlCancelPayWarnVoiceSet.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mIvVoiceSwitchCancelPay = mRootView.findViewById(R.id.iv_voice_switch_cancel_pay);


        mLlApiVoiceSet.setVisibility(View.GONE);
        tvTitleNotApiVoice.setText(getResources().getString(R.string.voice_notice_collection));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_voice_switch:
                onClickVoiceSwitch();
                break;
            case R.id.rl_voice_set_store:
                onClickSelectVoiceStore();
                break;
            case R.id.iv_voice_switch_cancel_pay:
                onClickCancelPay();
                break;
            case R.id.iv_voice_switch_not_vpi:
                onClickNotApiVoiceSwitch();
                break;
            case R.id.iv_voice_switch_vpi:
                onClickApiVoiceSwitch();
                break;
            default:
                break;
        }
    }

    /**
     * 设置收款语音播报总开关的状态
     */
    public void setSwitchView() {
        mIvVoiceSwitch.setImageResource(MyApplication.isOpenAllVoice() ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }

    /**
     * 设置api收款的语音播报开关的状态
     */
    private void setApiPaySwitchView() {
        mIvVoiceSwitchApi.setImageResource(MyApplication.isOpenApiVoice() ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }

    /**
     * 设置非api收款的语音播报开关的状态，非API交易即以前的语音播报的收款交易
     */
    private void setNotApiPaySwitchView() {
        mIvVoiceSwitchNotApi.setImageResource(MyApplication.isOpenNotApiVoice() ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }

    /**
     * 设置防逃单语音播报开关的状态
     */
    private void setCancelPaySwitchView() {
        mIvVoiceSwitchCancelPay.setImageResource(MyApplication.isOpenCancelPayWarnVoice() ? R.mipmap.ic_switch_open : R.mipmap.ic_switch_close);
    }


    /**
     * 点击了语音播报总开关按钮
     */
    private void onClickVoiceSwitch() {
        if (MyApplication.isOpenAllVoice()) {
            MyApplication.setOpenAllVoice(false);
        } else {
            MyApplication.setOpenAllVoice(true);
        }
        setSwitchView();
        mLlCancelPayWarnVoiceSet.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mLlNotApiVoiceSet.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mLlApiVoiceSet.setVisibility(apiVoiceBroadcastButton && MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mRlVoiceSetStore.setVisibility(MyApplication.isOpenAllVoice()?View.VISIBLE:View.GONE);
        mTvVoiceSetTip.setText(MyApplication.isOpenAllVoice()?getResources().getString(R.string.tv_voice_pay_open_tip):getResources().getString(R.string.tv_voice_pay_close_tip));
    }

    /**
     * 点击了非api交易的播报开关按钮
     */
    private void onClickNotApiVoiceSwitch() {
        if (MyApplication.isOpenNotApiVoice()) {
            MyApplication.setOpenNotApiVoice(false);
        } else {
            MyApplication.setOpenNotApiVoice(true);
        }
        setNotApiPaySwitchView();
    }

    /**
     * 点击了api交易的播报开关按钮
     */
    private void onClickApiVoiceSwitch() {
        if (MyApplication.isOpenApiVoice()) {
            MyApplication.setOpenApiVoice(false);
        } else {
            MyApplication.setOpenApiVoice(true);
        }
        setApiPaySwitchView();
    }

    /**
     * 点击了防逃单语音播报开关按钮（逃单就是取消支付的意思）
     */
    private void onClickCancelPay() {
        if (MyApplication.isOpenCancelPayWarnVoice()) {
            MyApplication.setOpenCancelPayWarnVoice(false);
        } else {
            MyApplication.setOpenCancelPayWarnVoice(true);
        }
        setCancelPaySwitchView();
    }


    /**
     * 点击了门店选择
     */
    private void onClickSelectVoiceStore() {
        Intent intentVoiceStore = new Intent(getActivity(), VoiceStoreListActivity.class);
        intentVoiceStore.putExtra(Constants.INTENT_TYPE_SELECT_STORE_VOICE,Constants.INTENT_VALUE_VOICE_COLLECTION);
        startActivityForResult(intentVoiceStore, Constants.REQUEST_VOICE_STORE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_VOICE_STORE) {
            if (data!=null){
                int selectCount = data.getIntExtra(Constants.INTENT_VOICE_SELECT_STORE_COUNT, Constants.SELECT_STORE_COUNT_DEFAULT);
                if (selectCount != Constants.SELECT_STORE_COUNT_DEFAULT){
                    setSelectStoreCountView(""+selectCount);
                }else {
                    LogUtil.d("门店列表获取失败，不需要改变选择门店的个数");
                }
            }
        }
    }

    /**
     * 更新选中的门店个数
     * mTvVoiceStoreCount==null时要等fragment创建好了再给它赋值
     * */
    public void setSelectStoreCountView(String voiceDaoJiaStoreCount) {
        mVoiceSelectStoreCount = voiceDaoJiaStoreCount;
        if (mTvVoiceStoreCount!=null){
            mTvVoiceStoreCount.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(mVoiceSelectStoreCount)||Integer.parseInt(mVoiceSelectStoreCount)<=0) {
                mTvVoiceStoreCount.setText("未选择");
            } else {
                mTvVoiceStoreCount.setText("已选择" + mVoiceSelectStoreCount + "家");
            }
        }
    }
}
