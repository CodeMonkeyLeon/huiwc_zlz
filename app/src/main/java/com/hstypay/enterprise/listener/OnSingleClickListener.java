package com.hstypay.enterprise.listener;

import android.view.View;

public abstract class OnSingleClickListener implements View.OnClickListener {

    //两次点击按钮的最小间隔，目前为1000

    private static long mDelayTime = 1000;
    private long lastClickTime;
    private int viewId;

    public abstract void onSingleClick(View view);

    public OnSingleClickListener(){
    }

    public OnSingleClickListener(long delayTime){
        mDelayTime = delayTime;
    }

    @Override

    public void onClick(View v) {
        long curClickTime = System.currentTimeMillis();
        if (v.getId() != viewId) {
            viewId = v.getId();
            lastClickTime = 0;
        }
        if ((curClickTime - lastClickTime) >= mDelayTime) {
            lastClickTime = curClickTime;
            onSingleClick(v);
        }

    }
}