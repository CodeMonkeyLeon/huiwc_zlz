package com.hstypay.enterprise.manager;

import android.content.Context;

import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.manager
 * @创建者: Jeremy
 * @创建时间: 2017/6/30 14:10
 * @描述: ${TODO}
 */

public class InfoManager {
    private static String isAdmin = "";
    // 商户id
    private static String merchantId = "";

    /**
     * 是否是商户=1还是管理员=0
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setIsAdmin(Context context,String sign)
    {
        if (!StringUtils.isEmptyOrNull(sign))
        {
            SpUtil.putString(context,"isAdmin" + getMchId(context), sign);
        }
    }

    /**
     * 是否是商户=1还是管理员=0
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getIsAdmin(Context context)
    {
        if (!StringUtils.isEmptyOrNull(InfoManager.isAdmin))
            return InfoManager.isAdmin;

        return SpUtil.getString(context,"isAdmin" + getMchId(context), InfoManager.isAdmin);
    }

    /**
     * 商户号
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static String getMchId(Context context)
    {
        if (!StringUtils.isEmptyOrNull(InfoManager.merchantId))
        {
            return InfoManager.merchantId;
        }
        return SpUtil.getString(context,"mchId", InfoManager.merchantId);
    }

    /**
     * 商户号
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setMchId(Context context,String mchId)
    {
        SpUtil.putString(context,"mchId", mchId);
    }
}
