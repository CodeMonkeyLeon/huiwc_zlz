package com.hstypay.enterprise.network;

import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.network
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/02/25 13:53
 * @描述: ${TODO}
 */

public class MyTrustManager implements X509TrustManager {
    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        LogUtil.d("getServerCert().getPublicKey()==");
        if (chain == null) {
            throw new IllegalArgumentException("Check Server x509Certificates is null");
        }

        if (chain.length == 0) {
            throw new IllegalArgumentException("Check Server x509Certificates is empty");
        }

        for (X509Certificate x509Certificate : chain) {
            //检查服务器证书签名是否有问题
            x509Certificate.checkValidity();
            try {
                //和App预处理证书做对比
                X509Certificate serverCert = getServerCert();
                if (serverCert != null) {
                    x509Certificate.verify(serverCert.getPublicKey());
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    private X509Certificate getServerCert() {
        InputStream certInputStream = null;
        try {
            String certName = Constants.CERT_NAME;
            certInputStream = new BufferedInputStream(MyApplication.getContext().getAssets().open(certName));
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            X509Certificate serverCert = (X509Certificate) certificateFactory.generateCertificate(certInputStream);
            return serverCert;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (certInputStream != null) {
                try {
                    certInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
