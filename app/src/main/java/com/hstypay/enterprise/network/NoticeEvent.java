package com.hstypay.enterprise.network;

/**
 * Created by admin on 2017/7/22.
 */
public class NoticeEvent {

    private String tag;
    private String cls;
    private Object mMsg;

    public NoticeEvent(String tag,String cls) {
        this.cls = cls;
        this.tag = tag;
    }

    public NoticeEvent(String tag, String cls, Object msg) {

        this.cls = cls;
        this.mMsg = msg;
        this.tag = tag;
    }

    public Object getMsg() {
        return mMsg;
    }

    public String getCls() {
        return cls;
    }

    public String getTag() {
        return tag;
    }
}

