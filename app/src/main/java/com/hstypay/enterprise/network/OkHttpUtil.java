package com.hstypay.enterprise.network;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.webkit.WebSettings;

import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.Response;

/**
 * Created by admin on 2017/7/22.
 */
public class OkHttpUtil {

    private static Context context = MyApplication.getContext();
    private static OkHttpUtil okHttpUtil;
    public static final MediaType mediaType = MediaType.parse("application/json;charset=utf-8");

    private OkHttpUtil(Context context) {
        this.context = context;
    }

    public static OkHttpUtil newInstance(Context context) {
        if (okHttpUtil == null) {
            okHttpUtil = new OkHttpUtil(context);
        }
        return okHttpUtil;
    }

    //登录之前的网络请求
    public static void postString(Context context, String url, final String tag, String data, String time, final OnResponse databack) {

        OkHttpUtils.postString().addHeader("hs-data-random", time)
                .addHeader("oemPrefix", ConfigUtil.getUserHeader())
                .addHeader("api-version", "1.1.0")
                .addHeader("User-Agent", "brand/"+getBrand() +" model/"+getModel()+ AppHelper.getUserAgent(MyApplication.getContext()))
                .mediaType(mediaType).tag(context).url(url)
                .content(data).build().connTimeOut(30000).readTimeOut(30000).writeTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_NET_ERROR));
            }

            @Override
            public void onResponse(String response, int id) {
                databack.onStringResponse(response, id);
                LogUtil.i("zhouwei", tag+"=数据+" + response);

            }
        });

    }

    //登录之前的网络请求
    public static void postString(Context context, String url, final String tag, String data, String time,long requestTimeOut, final OnResponse databack) {

        OkHttpUtils.postString().addHeader("hs-data-random", time)
                .addHeader("oemPrefix", ConfigUtil.getUserHeader())
                .addHeader("api-version", "1.1.0")
                .addHeader("User-Agent", "brand/"+getBrand() +" model/"+getModel()+AppHelper.getUserAgent(MyApplication.getContext()))
                .mediaType(mediaType).tag(context).url(url)
                .content(data).build().connTimeOut(requestTimeOut).readTimeOut(requestTimeOut).writeTimeOut(requestTimeOut).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_NET_ERROR));
            }

            @Override
            public void onResponse(String response, int id) {
                databack.onStringResponse(response, id);
                LogUtil.i("zhouwei", tag+"=数据+" + response);

            }
        });

    }

    //登录网络请求
    public static void postCall(Context context, String url, final String tag, String data, String time, final OnResponse databack) {
        LogUtil.i("zhouwei", tag+"=url= +"  + url);
        OkHttpUtils.postString().addHeader("hs-data-random", time + "")
                .addHeader("oemPrefix", ConfigUtil.getUserHeader())
                .addHeader("api-version", "1.1.0")
                .addHeader("User-Agent", "brand/"+getBrand() +" model/"+getModel()+AppHelper.getUserAgent(MyApplication.getContext()))
                .mediaType(mediaType).tag(context).url(url)
                .content(data).build().connTimeOut(30000).readTimeOut(30000).writeTimeOut(30000).execute(new Callback() {
            @Override
            public Object parseNetworkResponse(Response response, int id) throws Exception {
                saveCookies(response);
                String responseString = response.body().string();
                databack.onStringResponse(responseString, id);
                LogUtil.i("zhouwei", tag+"=数据+"  + responseString);
                return response;
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                LogUtil.d("aaaaa" + e.getMessage());
                EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_NET_ERROR));
            }

            @Override
            public void onResponse(Object response, int id) {
            }
        });
    }

    //postString网络请求，免密登录网络请求
    public static void autoPostCall(Context context, String url, final String tag, String data, String time, final OnResponse databack) {
        OkHttpUtils.postString().addHeader("hs-data-random", time)
                .addHeader("oemPrefix", ConfigUtil.getUserHeader())
                .addHeader("api-version", "1.1.0").
                addHeader("User-Agent", "brand/"+getBrand() +" model/"+getModel()+AppHelper.getUserAgent(MyApplication.getContext()))
                .mediaType(mediaType).tag(context).url(url)
                .content(data).build().connTimeOut(30000).readTimeOut(30000).writeTimeOut(30000).execute(new Callback() {
            @Override
            public Object parseNetworkResponse(Response response, int id) throws Exception {
                saveCookies(response);
                String responseString = response.body().string();
                LogUtil.i("zhouwei", tag+"=数据+"  + responseString);
                databack.onStringResponse(responseString, id);
                return response;
            }
            @Override
            public void onError(Call call, Exception e, int id) {
                LogUtil.d("auto",e.toString());
                EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_NET_ERROR));
            }

            @Override
            public void onResponse(Object response, int id) {
            }
        });
    }
    //登录之后的网络请求

    public static void postData(Context context, String url, final String tag, String data, String time, final OnResponse databack) {
        OkHttpUtils.postString().addHeader("hs-data-random", time)
                .addHeader("oemPrefix", ConfigUtil.getUserHeader())
                .addHeader("api-version", "1.1.0")
                .addHeader("User-Agent", "brand/"+getBrand() +" model/"+getModel()+AppHelper.getUserAgent(MyApplication.getContext()))
                .mediaType(mediaType).tag(context).url(url)
                .content(data).build().connTimeOut(30000).readTimeOut(30000).writeTimeOut(30000).execute(new Callback() {
            @Override
            public Object parseNetworkResponse(Response response, int id) throws Exception {
                //saveCookies(response);
                String responseString = response.body().string();
                databack.onStringResponse(responseString, id);
                return response;
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                LogUtil.i("zhouwei", tag+"=数据+"  + e);
                EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_NET_ERROR));
                databack.onError(e);
            }

            @Override
            public void onResponse(Object response, int id) {
            }
        });
    }

    /**
     * 传入的  Activity  作为标记，用于取消 网络请求
     *
     * @param activity
     * @param url
     * @param map
     * @param databack
     */
    public static void get(Activity activity, String url, Map<String, String> map, final OnResponse databack) {
        OkHttpUtils
                .get()
                .url(url)
                .tag(activity)
                .params(map)
                .build()
                .connTimeOut(30000).readTimeOut(30000).writeTimeOut(30000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        // EventBus.getDefault().post(new NoticeEvent(Constants.MSG_NET_WRONG));
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        databack.onStringResponse(response, id);
                    }
                });
    }

    //, Map<String, String> map
    public static void post(String url, final OnResponse databack) {
        OkHttpUtils.post()
                .url(url)
                //.params(map)
                .build()
                .connTimeOut(30000).readTimeOut(30000).writeTimeOut(30000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        //EventBus.getDefault().post(new NoticeEvent(Constants.MSG_NET_WRONG));
                    }
                    @Override
                    public void onResponse(String response, int id) {
                        databack.onStringResponse(response, id);

                    }
                });
    }


    public interface OnResponse {
        void onStringResponse(String response, int cancalTag);
        default void onError(Exception e){};
    }

    private static void saveCookies(Response response) {
        Headers headers = response.headers();
        List<String> cookies = headers.values("Set-cookie");
        LogUtil.i("zhouwei", "cookies====>>>>" + cookies + "");
        StringBuilder cookieBuilder = new StringBuilder();
        for (int i = 0; i < cookies.size(); i++) {
            if(i==cookies.size()-1) {
                cookieBuilder.append(cookies.get(i));
            }else{
                cookieBuilder.append(cookies.get(i)).append("<<<->>>");
            }
        }
        SpUtil.putString(MyApplication.getContext(), Constants.LOGIN_COOKIE, cookieBuilder.toString());//保存cookie提供给h5

        for (String str : cookies) {
            if (str.startsWith("SKEY")) {
                String skeyString = str.split(";")[0];
                String skey = skeyString.split("=")[1];
                SpUtil.putString(MyApplication.getContext(),Constants.SKEY,skey);
                LogUtil.d("SKEY:" , SpUtil.getString(MyApplication.getContext(),Constants.SKEY,""));
            }
            if (str.startsWith("SAUTHID")) {
                String sAuthIdString = str.split(";")[0];
                String sAuthId = sAuthIdString.split("=")[1];
                SpUtil.putString(MyApplication.getContext(),Constants.SAUTHID,sAuthId);
                LogUtil.d("SAUTHID:" , SpUtil.getString(MyApplication.getContext(),Constants.SAUTHID,""));
            }
        }
    }

    public static void saveFile(String url,String fileName,final String tag,Context context){
        Log.d("fileName---",url+",,,"+fileName);
        OkHttpUtils.get().url(url).build().connTimeOut(20000).readTimeOut(20000).writeTimeOut(20000).execute(
                new FileCallBack(context.getExternalFilesDir("adv").getAbsolutePath(), fileName)
                {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.SAVE_IMAGE_FALSE));
                        LogUtil.d("Constants---",Constants.SAVE_IMAGE_FALSE);
                        LogUtil.d("Exception---",e.getMessage());
                    }

                    @Override
                    public void onResponse(File response, int id) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.SAVE_IMAGE_TRUE));
                        LogUtil.d("Constants---",Constants.SAVE_IMAGE_TRUE);
                        LogUtil.d("Constants---response--",response.getAbsolutePath());
                    }
                });
    }

    private static String getUserAgent() {
        String userAgent = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                userAgent = WebSettings.getDefaultUserAgent(context);
            } catch (Exception e) {
                userAgent = System.getProperty("http.agent");
            }
        } else {
            userAgent = System.getProperty("http.agent");
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0, length = userAgent.length(); i < length; i++) {
            char c = userAgent.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                sb.append(String.format("\\u%04x", (int) c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static String getModel(){
        return Build.MODEL.replaceAll(" ","-");
    }

    private static String getBrand(){
        return Build.BRAND.replaceAll(" ","-");
    }
}
