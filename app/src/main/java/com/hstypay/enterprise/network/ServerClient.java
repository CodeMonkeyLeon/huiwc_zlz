package com.hstypay.enterprise.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.*;
import com.hstypay.enterprise.bean.coupon.CouponHsInfoBean;
import com.hstypay.enterprise.bean.coupon.CouponHsListBean;
import com.hstypay.enterprise.bean.paySite.PaySiteList;
import com.hstypay.enterprise.bean.vanke.CouponCheckBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoBean;
import com.hstypay.enterprise.bean.vanke.CouponListBean;
import com.hstypay.enterprise.bean.vanke.CouponReverseBean;
import com.hstypay.enterprise.bean.vanke.CouponTypeListBean;
import com.hstypay.enterprise.bean.vanke.CouponVerifyBean;
import com.hstypay.enterprise.bean.vanke.VipInfoBean;
import com.hstypay.enterprise.network.OkHttpUtil.OnResponse;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SignUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.qiezzi.choseviewlibrary.bean.AddressBean;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017/7/22.
 */
public class ServerClient {
    private static ServerClient mServerClient;
    private static OkHttpUtil httpUtils = null;
    private static Gson gson = null;
    private Context context;
    //时间戳
    public static final long times = System.currentTimeMillis();
    String time = times + "";

    private ServerClient(Context context) {
        this.context = context;
    }

    public static ServerClient newInstance(Context context) {
        if (mServerClient == null) {
            mServerClient = new ServerClient(context);
        }
        if (httpUtils == null) {
            httpUtils = OkHttpUtil.newInstance(context);
        }
        if (gson == null) {
            gson = new Gson();
        }
        return mServerClient;
    }

    //登录接口
    public void login(final Context context, final String tag, Map<String,Object> map) {
        String url = Constants.BASE_URL + "login";

        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postCall(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "登录入口=" + response);
                LoginBean info = HstyCallBack(response, LoginBean.class);
                if (info != null) {

                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }

                }
            }
        });
    }

    /**
     * 选择商户免密登录
     *
     * @param context
     * @param tag
     * @param map
     */
    public void mchLogin(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "mch/choose/login";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postCall(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "选择商户免密登录==" + response);
                LoginBean info = HstyCallBack(response, LoginBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 商户验证码登录
     *
     * @param context
     * @param tag
     * @param map
     */
    public void msgLogin(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "sms/login";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postCall(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户验证码登录==" + response);
                LoginBean info = HstyCallBack(response, LoginBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 获取图形验证码
     *
     * @param context
     * @param tag
     * @param verificationCodeType 获取验证码类型 1:登陆 2:找回密码
     * @param userName             用户姓名
     */
    public void getIdentifyingCode(Context context, final String tag, int verificationCodeType, String userName) {
        String url = Constants.BASE_URL + "getIdentifyingCode";
        Map<String, Object> map = new HashMap<>();
        map.put("foo", "@ctitle");
        map.put("verificationCodeType", verificationCodeType);
        map.put("userName", userName);
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", " 获取图形验证码=" + response);
                IdentifyCodeBean info = HstyCallBack(response, IdentifyCodeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GETIDENTIFYINGCODE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GETIDENTIFYINGCODE_TRUE, info));
                }
            }
        });
    }


    //获取商户列表
    public void getLoginMerchant(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "telphone/orglist";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户号列表=" + response);
                MerchantIdBean info = HstyCallBack(response, MerchantIdBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //获取短信验证码
    public void getCode(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "password/sms/send";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SEND_PHONE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SEND_PHONE_TRUE, info));
                }
            }
        });
    }

    //验证短信验证码
    public void checkCode(Context context, final String tag, String telphone, String code) {
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "password/sms/check";
        map.put("phone", telphone);
        map.put("code", code);
        if (ConfigUtil.getConfig()) {
            map.put("orgId", Constants.ORG_ID);
            map.put("orgType", Constants.ORG_TYPE);
            map.put("appCode", Constants.APP_CODE + Constants.ORG_ID);
        }
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_CODE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_CODE_TRUE, info));
                }
            }
        });

    }

    //重置密码
    public void resetPwd(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "password/reset";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //免密登录
    public void autoPwd(final Context context, final String tag) {
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "login/auto";
        //map.put("", "");
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.autoPostCall(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "免密登录入口=" + response);
                LoginBean info = HstyCallBack(response, LoginBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTO_PWD_FALSE, info));
                    } else {
                        if (info.getData() != null) {
                            boolean isShowWSY = info.getData().isShowWSY();
                            SpUtil.putBoolean(context, Constants.IS_SHOW_WSY, isShowWSY);
                            SpUtil.putString(context, Constants.SIGN_KEY, info.getData().getSignKey());//签名的key
                            SpUtil.putBoolean(context, Constants.IS_PLATFORM_WHITE_LIST, info.getData().isStandardFlag());
                            SpUtil.putBoolean(context, Constants.IS_CHANGE_TO_NEW_PLATFORM, info.getData().isStandardIndexFlag());
                            SpUtil.putBoolean(MyApplication.getContext(), Constants.IS_BELONG_HSHF, info.getData().isBelonedHSHFFlag());
                            if (info.getData().getUser() != null) {
                                SpUtil.putInt(context, Constants.SP_ATTESTATION_STATUS, info.getData().getUser().getAttestationStatus());//商户认证状态
                                int userId = info.getData().getUser().getUserId();
                                SpUtil.putString(context, Constants.USER_ID, userId + "");//用户id
                                SpStayUtil.putString(context, Constants.SP_STAY_USER_ID, userId + "");//用户id
                                SpUtil.putInt(context, Constants.SP_WXREG_STATUS, info.getData().getUser().getWxRegStatus());
                                SpUtil.putInt(context, Constants.SP_ALIREG_STATUS, info.getData().getUser().getAliRegStatus());

                                boolean isUser = info.getData().getUser().isMerchantFlag();
                                SpUtil.putBoolean(context, Constants.IS_MERCHANT_FLAG, isUser);//是否是超管
                                //是否进件成功
                                String orgId = info.getData().getUser().getOrgId();
                                SpUtil.putString(context, Constants.IS_SUCCESS_DATA, orgId);
                                //是否是商户
                                boolean isAdmin = info.getData().getUser().isAdminFlag();
                                SpUtil.putBoolean(context, Constants.IS_ADMIN_FLAG, isAdmin);
                                //是否是收银员
                                boolean isCashier = info.getData().getUser().isCasherFlag();
                                SpUtil.putBoolean(context, Constants.IS_CASHER_FLAG, isCashier);
                                //是否是店长
                                boolean isManager = info.getData().getUser().isStoreManagerFlag();
                                SpUtil.putBoolean(context, Constants.IS_MANAGER_FLAG, isManager);
                                //realname
                                String realName = info.getData().getUser().getRealName();
                                SpUtil.putString(context, Constants.REALNAME, realName);
                                SpUtil.putBoolean(context, Constants.NEED_CHANGE_PWD, info.getData().getUser().isUpdatePwdFlag());
                                SpUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE, info.getData().getUser().getLoginPhone());
                                //用户名
                                SpUtil.putString(context, Constants.APP_USERNAME, info.getData().getUser().getUserName());
                                SpUtil.putString(context, Constants.APP_LOGIN_NAME, info.getData().getUser().getLoginName());

                                if (info.getData().getUser().getEmp() != null) {
                                    if (!StringUtils.isEmptyOrNull(info.getData().getUser().getEmp().getEmpId())) {
                                        SpUtil.putString(context, Constants.SP_EMP_ID, info.getData().getUser().getEmp().getEmpId());
                                    } else {
                                        SpUtil.putString(context, Constants.SP_EMP_ID, "");
                                    }
                                }
                                //商户号
                                if (info.getData().getUser().getMerchant() != null) {
//                                    SpUtil.putInt(context, Constants.SP_PLEDGE_STATUS, info.getData().getUser().getMerchant().getPreauthStatus());
                                    if (!StringUtils.isEmptyOrNull(info.getData().getUser().getMerchant().getRewardFlage())) {
                                        SpUtil.putBoolean(context, Constants.IS_BOUNTY_MERCHANT, true);
                                    } else {
                                        SpUtil.putBoolean(context, Constants.IS_BOUNTY_MERCHANT, false);
                                    }
                                    if (!StringUtils.isEmptyOrNull(info.getData().getUser().getMerchant().getMerchantId())) {
                                        SpUtil.putString(context, Constants.APP_MERCHANT_ID, info.getData().getUser().getMerchant().getMerchantId());
                                    } else {
                                        SpUtil.putString(context, Constants.APP_MERCHANT_ID, "");
                                    }
                                    if (!StringUtils.isEmptyOrNull(info.getData().getUser().getMerchant().getMerchantType())) {
                                        SpUtil.putString(context, Constants.APP_MERCHANT_TYPE, info.getData().getUser().getMerchant().getMerchantType());
                                    } else {
                                        SpUtil.putString(context, Constants.APP_MERCHANT_TYPE, "");
                                    }
                                    if (!StringUtils.isEmptyOrNull(info.getData().getUser().getMerchant().getMerchantName())) {
                                        String merchant_name = info.getData().getUser().getMerchant().getMerchantName();//商户名称
                                        SpUtil.putString(context, Constants.MERCHANT_NAME, merchant_name);
                                    } else {
                                        SpUtil.putString(context, Constants.MERCHANT_NAME, "");
                                    }
                                }
                            } else {
                                SpUtil.putInt(context, Constants.SP_ATTESTATION_STATUS, 0);//商户认证状态
                            }
                        }
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTO_PWD_TRUE, info));
                    }
                }
            }
        });
    }

    //首页今日数据
    public void homeTodayData(Context context, final String tag, Map<String, Object> map) {
        //today/data
        String url = Constants.BASE_URL + "order/today/data";
        //map.put("", "");
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "首页今日数据==" + response);
                ReportBean info = HstyCallBack(response, ReportBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_REPORT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_REPORT_TRUE, info));
                }
            }
        });
    }

    //首页广告网络请求
    public void homeAdvertising(Context context, final String tag) {
        //today/data
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "advertisement/index/carousel/img";
        //String url = Constants.BASE_URL + "merchant/index/carousel/img";
        //map.put("", "");
        //String data = SignUtil.getInstance().loginAppSign(map, time, time);
        //String data=SignUtil.getInstance().createAppSign(map,time);
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "首页广告数据==" + response);
                HomeAdvertisingBean info = HstyCallBack(response, HomeAdvertisingBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_ADVERTISING_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_ADVERTISING_TRUE, info));
                }
            }
        });
    }

    //首页消息接口
    public void homeNotice(Context context, final String tag) {
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "notice/find/nearby/notice";
        //map.put("", "");
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "首页消息数据==" + response);
                HomeNoticeBean info = HstyCallBack(response, HomeNoticeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_NOTICE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_NOTICE_TRUE, info));
                }
            }
        });
    }

    //首页读取消息接口
    public void homeReaderNotice(Context context, final String tag, String id) {
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "notice/reader";
        map.put("id", id);
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "首页已读消息数据==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_READER_NOTICCE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_READER_NOTICCE_TRUE, info));
                }
            }
        });
    }

    //收银员管理接口
    public void CashierManagement(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashier/find/cashiers";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员管理数据==" + response);
                CashierManageBean info = HstyCallBack(response, CashierManageBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_MANAGEMENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_MANAGEMENT_TRUE, info));
                }
            }
        });
    }

    //收银员冻结
    public void freezeCashier(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashier/frozen";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员冻结==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_FREEZE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_FREEZE_TRUE, info));
                }
            }
        });
    }

    //收银员添加
    public void addCashier(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashier/add/cashier";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员添加数据==" + response);
                CashierBean info = HstyCallBack(response, CashierBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ADD_CASHIER_MANAGE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ADD_CASHIER_MANAGE_TRUE, info));
                }
            }
        });
    }

    //获取消息列表
    public void getMessage(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "notice/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "消息列表==" + response);
                MessageData info = HstyCallBack(response, MessageData.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //商家说
    public void getMchTalk(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "mchtalk/client/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商家说列表==" + response);
                MchtalkBean info = HstyCallBack(response, MchtalkBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }


    //阅读状态
    public void setReadState(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "notice/reader";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.d("Jeremy", "阅读状态---" + response);
                ReadData info = HstyCallBack(response, ReadData.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //获取商户信息
    public void getMerchantInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取商户信息==" + response);
                MerchantInfoBean info = HstyCallBack(response, MerchantInfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_INFO_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_INFO_TRUE, info));
                }
            }
        });
    }

    //查询商户剩余修改结算信息次数
    public void getSettleChangeCount(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/account/changeCount";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询商户剩余修改结算信息次数==" + response);
//                MerchantInfoBean info = HstyCallBack(response, MerchantInfoBean.class);
                SettleChangeCountBean info = HstyCallBack(response, SettleChangeCountBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_SETTLE_CHANGE_COUNT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_SETTLE_CHANGE_COUNT_TRUE, info));
                }
            }
        });
    }

    //获取行业类别
    public void getIndustryType(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/find/industry";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.d("Jeremy", "获取行业类别" + response);
                IndustryBean info = HstyCallBack(response, IndustryBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_INDUSTRY_TYPE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_INDUSTRY_TYPE_TRUE, info));
                }
            }
        });
    }

    //获取地址
    public void getAddress(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/find/all/area/grading";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.d("Jeremy", "获取地址" + response);
                AddressBean info = HstyCallBack(response, AddressBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_ADDRESS_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_ADDRESS_TRUE, info));
                }
            }
        });
    }

    //提交修改资料
    public void submitMerchantInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/editBasicData";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.d("Jeremy", "提交修改资料==" + response);
                MerChantModifyBean info = HstyCallBack(response, MerChantModifyBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SUBMIT_MERCHANT_INFO_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SUBMIT_MERCHANT_INFO_TRUE, info));
                }
            }
        });
    }


    //验证商户资料是否允许修改
    public void queryEnabledChange(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/enabledChange";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.d("Jeremy", "验证商户资料是否允许修改==" + response);
                QueryEnabledChangeBean info = HstyCallBack(response, QueryEnabledChangeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_ENABLED_CHANGE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_ENABLED_CHANGE_TRUE, info));
                }
            }
        });
    }

    //支付接口
    public void pay(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "unifiedMicroPay";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支付接口" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.PAY_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.PAY_TRUE, info));
                    }
                }
            }
        });
    }

    public void pledgePay(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "authMicropay";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "预授权接口" + response);
                PledgePayBean info = HstyCallBack(response, PledgePayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店接口 默認收款門店
    public void storePort(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/find/gathering/store";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店接口=" + response);
                StoreBean info = HstyCallBack(response, StoreBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改密码接口
    public void changePwd(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/modify/pwd";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改密码接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.LOGIN_CHANGE_PWD_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.LOGIN_CHANGE_PWD_TRUE, info));
                    }
                }
            }
        });
    }

    //商品名称接口
    public void getGoodsName(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/find/pay/goodsname";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询商品名称接口=" + response);
                GoodsNameBean info = HstyCallBack(response, GoodsNameBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_GOODS_NAME_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_GOODS_NAME_TRUE, info));
                    }
                }
            }
        });
    }

    //修改商品名称接口
    public void goodsName(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/modify/pay/goodsname";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商品名称接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GOODS_NAME_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GOODS_NAME_TRUE, info));
                    }
                }
            }
        });
    }

    //商户简称详情接口
    public void shortNameDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/merchantName/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户简称详情接口=" + response);
                ShortNameBean info = HstyCallBack(response, ShortNameBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改商户简称接口
    public void changeShortName(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/editMerchantName";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改商户简称接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店列表接口
    public void choiceStore(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/findStores";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "选择门店接口=" + response);
                StoreListBean info = HstyCallBack(response, StoreListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店列表接口
    public void findStore(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/v2/findStores";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "选择门店V2接口=" + response);
                StoreListBean info = HstyCallBack(response, StoreListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    //门店分组列表接口
    public void choiceStoreGroup(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/group/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店分组列表接口=" + response);
                StoreGroupBean info = HstyCallBack(response, StoreGroupBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店详情接口
    public void storeDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店详情接口=" + response);
                StoreDetailBean info = HstyCallBack(response, StoreDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店冻结接口
    public void storeFreeze(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/frozen";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店冻结接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店编辑接口
    public void editStore(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/edit";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店编辑接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店添加接口
    public void addStore(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/add";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店添加接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //员工列表接口
    public void empList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/empManager/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "员工列表接口=" + response);
                EmployeeManagerBean info = HstyCallBack(response, EmployeeManagerBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //添加员工
    public void addEmployee(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/emp/add";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "添加员工接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //店长列表接口
    public void managerList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "storeManager/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "店长列表接口=" + response);
                ManagerListBean info = HstyCallBack(response, ManagerListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //未绑定店长门店列表
    public void unbindManagerList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/unbindManagerList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "未绑定店长门店列表=" + response);
                StoreListBean info = HstyCallBack(response, StoreListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //新增店长
    public void addManager(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "storeManager/add";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "新增店长=" + response);
                CashierBean info = HstyCallBack(response, CashierBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //店长详情
    public void managerDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "storeManager/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "店长详情=" + response);
                ManagerDetailBean info = HstyCallBack(response, ManagerDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //冻结店长
    public void freezeManager(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "storeManager/frozen";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "冻结店长=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //编辑店长
    public void editManager(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "storeManager/edit";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "编辑店长=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //根据门店查询收银员账单
    public void getCashierOfStore(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/find/casher/ofstore";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "选择门店 接口=" + response);
                CashierOfStoreBean info = HstyCallBack(response, CashierOfStoreBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //门店二维码
    public void getStoreCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/find/store/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "二维码接口=" + response);
                CodeListBean info = HstyCallBack(response, CodeListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.STORE_CODE_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.STORE_CODE_TRUE, info));
                    }
                }
            }
        });
    }

    //订单查询接口
    public void checkOrderState(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "queryOrderDetail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询接口=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_ORDER_STATE_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_ORDER_STATE_TRUE, info));
                    }
                }
            }
        });
    }

    //订单冲正接口
    public void orderReverse(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "reverse";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "订单冲正接口=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ORDER_REVERSE_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ORDER_REVERSE_TRUE, info));
                    }
                }
            }
        });
    }

    //账单接口
    public void getBills(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/pay/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "账单接口=" + response);
                BillsBean info = HstyCallBack(response, BillsBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BILLS_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BILLS_TRUE, info));
                    }
                }
            }
        });
    }

    //账单接口
    public void getPledgeBills(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "orderPreauth/pay/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "预授权账单接口=" + response);
                PledgeBillsBean info = HstyCallBack(response, PledgeBillsBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //订单详情接口
    public void getOrderDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "订单详情接口=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //撤销退款
    public void cancelRefund(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "refund/cancel";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "撤销退款=" + response);
                BaseBean info = HstyCallBack(response, BaseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //刷卡订单冲正
    public void bcardReverse(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "reverseByPwd";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "刷卡订单冲正=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //退款接口
    public void getRefund(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "refund";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "退款接口=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    //查询订单接口
    public void getBillsByNumber(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/orderno/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询订单接口=" + response);
                BillsBean info = HstyCallBack(response, BillsBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BILLS_BY_NUMBER_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BILLS_BY_NUMBER_TRUE, info));
                    }
                }
            }
        });
    }

    //报表接口
    public void getReport(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/report/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "报表接口=" + response);
                ReportBean info = HstyCallBack(response, ReportBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_REPORT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_REPORT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 资金预授权报表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getPledgeReport(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "orderPreauth/report/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "预授权报表接口=" + response);
                PledgeReportBean info = HstyCallBack(response, PledgeReportBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_REPORT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_REPORT_TRUE, info));
                    }
                }
            }
        });
    }

    //绑定接口
    public void bindCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/storeBindQrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定二维码接口=" + response);
                BindCodeBean info = HstyCallBack(response, BindCodeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.BIND_CODE_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.BIND_CODE_TRUE, info));
                    }
                }
            }
        });
    }

    //收银员编辑
    public void cashierEdit(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashier/edit/cashier";
        //map.put("", "");
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员编辑==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_EDIT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_EDIT_TRUE, info));
                }
            }
        });
    }

    //收银员详情
    public void cashierDetail(Context context, final String tag, Map<String, Object> map) {

        String url = Constants.BASE_URL + "cashier/info";
        //map.put("", "");
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员详情==" + response);
                CashierDetailBean info = HstyCallBack(response, CashierDetailBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_DETAIL_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CASHIER_DETAIL_TRUE, info));
                }
            }
        });
    }

    //收银员权限接口
    public void cashierPermission(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashier/allot/cashier/permission";
        //map.put("", "");
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员权限数据==" + response);
                CashierDetailBean info = HstyCallBack(response, CashierDetailBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //上传默认门店接口
    public void postStore(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/save/gathering/store";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "上传默认门店数据==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.PUT_STORE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.PUT_STORE_TRUE, info));
                }
            }
        });
    }

    //查询语音播报门店选择数量
    public void getStoreSettingCount(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pushUser/storeSettingCount";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询语音播报门店选择数量==" + response);
                StoreSettingCountBean countBean = HstyCallBack(response, StoreSettingCountBean.class);
                if (!countBean.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, countBean));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, countBean));
                }
            }
        });
    }

    //语音播报初始化推送
    public void getuiPushVoice(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "voice/add/pushuser";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "语音播报初始化数据==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //语音播报推送门店列表
    public void getuiPushVoiceList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "voice/find/voices";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "语音播报推送门店列表==" + response);
                VoiceBean info = HstyCallBack(response, VoiceBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //威到家语音播报推送门店列表
    public void getVoicePushStoreList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pushUser/storeSettingList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "威到家语音播报推送门店列表==" + response);
                VoiceBean info = HstyCallBack(response, VoiceBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //推送语音播报门店开关设置
    public void getuiPushVoiceSet(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "voice/edit/voice/set";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "推送语音播报门店开关设置==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ADD_CASHIER_MANAGE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ADD_CASHIER_MANAGE_TRUE, info));
                }
            }
        });
    }

    //到家订单语音播报门店开关设置
    public void pushUserSwitch(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pushUser/switch";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "到家订单语音播报门店开关设置==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ADD_CASHIER_MANAGE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ADD_CASHIER_MANAGE_TRUE, info));
                }
            }
        });
    }


    public void scanThirdOrder(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/third/oreder/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "扫描查单数据==" + response);
                ThirdOrderBean info = HstyCallBack(response, ThirdOrderBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SCAN_THIRD_ORDER_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SCAN_THIRD_ORDER_TRUE, info));
                }
            }
        });
    }

    //查询审核状态
    public void checkVerifyStatus(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/query/merchant/status";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "审核状态==" + response);
                MerChantModifyBean info = HstyCallBack(response, MerChantModifyBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_VERIFY_STATUS_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_VERIFY_STATUS_TRUE, info));
                }
            }
        });
    }

    //版本升级接口
    public void updateVersion(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/find/app/version";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "版本升级==" + response);
                UpdateVersionBean info = HstyCallBack(response, UpdateVersionBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //版本升级接口2.0
    public void updateVersion2(Context context, final String tag, Map<String, Object> map) {
        LogUtil.i("Jeremy", "版本升级2request==" + gson.toJson(map));
        String url = Constants.BASE_URL + "set/find/app/channel/version";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "版本升级2==" + response);
                UpdateVersionBean info = HstyCallBack(response, UpdateVersionBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //查询二维码
    public void checkQrcode(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/query/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询二维码==" + response);
                QrcodeBean info = HstyCallBack(response, QrcodeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_QRCODE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_QRCODE_TRUE, info));
                }
            }
        });
    }


    //app退出接口
    public void Loginout(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "logout";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "app退出==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //删除语音播报cid接口
    public void deletePushVoiceDeviceId(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "voice/delete/deviceid";
        //String data = SignUtil.getInstance().createAppSign(map, time);
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "语音播报删除cid==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //查询费率
    public void queryRate(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/query/sign/contract";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询费率==" + response);
                RateBean info = HstyCallBack(response, RateBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_RATE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_RATE_TRUE, info));
                }
            }
        });
    }

    //查开通的支付方式
    public void queryPayType(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "queryPayType";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查开通的支付方式==" + response);
                QueryPayTypeBean info = HstyCallBack(response, QueryPayTypeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_RATE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_RATE_TRUE, info));
                }
            }
        });
    }

    //查询充值签约费率信息
    public void queryVipRechargeRate(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/query/charge/sign/contract";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询充值签约费率==" + response);
                RateBean info = HstyCallBack(response, RateBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //语音验证码
    public void getVoiceCode(Context context, final String tag, String telphone) {
        String url = Constants.BASE_URL + "password/voice/sms/send";
        Map<String, Object> map = new HashMap<>();
        map.put("phone", telphone);
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "语音验证码==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_VOICE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_VOICE_TRUE, info));
                }
            }
        });
    }

    //签约状态查询
    public void querySignStatus(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/query/sign/contract/status";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "签约状态查询==" + response);
                SignStatus info = HstyCallBack(response, SignStatus.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_SIGN_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_SIGN_TRUE, info));
                }
            }
        });
    }

    //签约状态上传
    public void putSignStatus(Context context, final String tag, Map<String, Object> map) {
        //String url = Constants.BASE_URL + "merchant/sign/contract";
        String url = Constants.BASE_URL + "merchant/sign/member/contract";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "签约状态上传==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //定位查询
    public void questLocation(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/query/location/info";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "定位查询==" + response);
                LocationBean info = HstyCallBack(response, LocationBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUEST_LOCATION_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUEST_LOCATION_TRUE, info));
                }
            }
        });
    }

    //下载广告图片
    public void downloadImg(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "advertisement/app/start/carousel/img";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, 15000, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "下载广告图片==" + response);
                LaunchAdvertiseBean info = HstyCallBack(response, LaunchAdvertiseBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    public void getChangeBankMsg(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/bankcode/sms/send";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "更换银行卡短信验证==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    // 绑定/更换手机号---短信验证码
    public void getBindingMsg(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "phone/verifycode/send";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定短信验证==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    // 绑定/更换手机号---语音验证码
    public void getBindingVoice(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "phone/voice/verifycode/send";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定语音验证==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    // 绑定手机号
    public void bindingTel(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "login/phone/bind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定手机号==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_TRUE, info));
                }
            }
        });
    }

    // 更换手机号
    public void changeBindingTel(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/modify/phone";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "更换手机号==" + response);
                Info info = HstyCallBack(response, Info.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //验证身份
    public void authentication(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/authentication";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "验证身份=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTHENTICATION_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTHENTICATION_TRUE, info));
                    }
                }
            }
        });
    }

    //验证退款密码，撤销/全额退款
    public void verifyRefundPwd(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "v2/authReverse";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "验证退款密码=" + response);
            Info info = HstyCallBack(response, Info.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTHENTICATION_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTHENTICATION_TRUE, info));
                }
            }
        });
    }

    //查询退款密码
    public void queryRefundPwd(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/config/query";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "查询退款密码=" + response);
            Type type = new TypeToken<ApiRes<RefundPwd>>() {
            }.getType();
            ApiRes<RefundPwd> res = gson.fromJson(response, type);
            if (res != null) {
                if (!res.getStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, res));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, res));
                }
            }
        });
    }


    //营业执照详情
    public void licenseDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/license/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "营业执照详情=" + response);
                LicenseDetailBean info = HstyCallBack(response, LicenseDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //编辑营业执照
    public void editLicense(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/editLicense";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "编辑营业执照=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //限额数据
    public void limitDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/risk/control";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "限额数据=" + response);
                LimitBean info = HstyCallBack(response, LimitBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //我的银行卡
    public void myBankCard(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/myBankCard";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "我的银行卡=" + response);
                MyCardBean info = HstyCallBack(response, MyCardBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //银行卡详情
    public void bankCardDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/myBankCard/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "银行卡详情=" + response);
                CardDetailBean info = HstyCallBack(response, CardDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //编辑银行卡
    public void editBankCard(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/editBankCard";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "编辑银行卡=" + response);
                ChangeBankBean info = HstyCallBack(response, ChangeBankBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //银行列表
    public void bankList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/find/banks";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "银行列表=" + response);
                BankListBean info = HstyCallBack(response, BankListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //支行列表
    public void bankBranchList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/find/bank/branchs";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支行列表=" + response);
                BranchListBean info = HstyCallBack(response, BranchListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //邀请码启用状态
    public void invitationEnable(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "invitationcode/enabled";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "邀请码启用状态=" + response);
                InvitateBean info = HstyCallBack(response, InvitateBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //邀请码注册链接
    public void invitationCode(final Context context, final String tag, Map<String, Object> map) {
        /*String url = Constants.BASE_URL + "invitationcode/url";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "邀请码注册链接=" + response);
                InvitateBean info = HstyCallBack(response, InvitateBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });*/
    }

    //收集用户信息
    public void collectInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "collect/user/info";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收集用户信息=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //商户资料审核状态
    public void materialStatus(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/query/merchant/material/status";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户资料审核状态=" + response);
                MaterialStatusBean info = HstyCallBack(response, MaterialStatusBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //小票活动
    public void printActive(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "set/system/parameter";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "小票活动=" + response);
                ActiveBean info = HstyCallBack(response, ActiveBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //商户贷款
    public void loanDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "loans/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户贷款详情=" + response);
                LoanDetailBean info = HstyCallBack(response, LoanDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //初次申请贷款
    public void loanFrist(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "loans/apply";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "初次申请贷款=" + response);
                UrlBean info = HstyCallBack(response, UrlBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //再次申请贷款
    public void loanAgain(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "loans/reentry";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "再次申请贷款=" + response);
                UrlBean info = HstyCallBack(response, UrlBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //商户白名单
    public void whiteList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/whiteListEnabled";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户白名单=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //硬件设备绑定
    public void deviceBind(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/bind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "硬件设备绑定=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

  /*  //硬件设备列表
    public void deviceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "硬件设备列表=" + response);
                DevicesBean info = HstyCallBack(response, DevicesBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }*/

    //门店设备列表
    public void storeDeviceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/find/device/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店设备列表=" + response);
                StoreDevicesBean info = HstyCallBack(response, StoreDevicesBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void isOpenMember(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "open/member";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "是否开通会员=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void queryDrawMember(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "queryDrawMember";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "领取会员卡链接=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void queryStoreVipCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "baseMchMember/storeSpreadUrlQuery";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "领取会员卡链接2=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void reportDay(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/day/count";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "日结统计=" + response);
                ReportIncomeBean info = HstyCallBack(response, ReportIncomeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void reportDayDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/day/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "日结详情=" + response);
                ReportIncomeBean info = HstyCallBack(response, ReportIncomeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void reportMonth(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/month/count";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "月结统计=" + response);
                ReportMonthBean info = HstyCallBack(response, ReportMonthBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void reportMonthDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/month/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "月结详情=" + response);
                ReportIncomeBean info = HstyCallBack(response, ReportIncomeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员活动创建
    public void vipActiveCreate(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/add";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员活动创建=" + response);
                CommonBean info = HstyCallBack(response, CommonBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员活动列表
    public void vipActiveList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员活动列表=" + response);
                VipActiveListBean info = HstyCallBack(response, VipActiveListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员活动详情
    public void vipActiveInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/info";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员活动详情=" + response);
                VipActiveInfoBean info = HstyCallBack(response, VipActiveInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员活动状态编辑
    public void vipActiveStatusEdit(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/state/update";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员活动状态编辑=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员活动是否可创建
    public void vipActiveCreateEnable(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/existActivityStatus";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询是否可创建=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员活动编辑
    public void vipActiveEdit(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/update";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员活动编辑=" + response);
                CommonBean info = HstyCallBack(response, CommonBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //生成物料图片
    public void createMaterialImage(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activity/create/material";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "生成物料=" + response);
                MaterialBean info = HstyCallBack(response, MaterialBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员卡查询
    public void searchVipCard(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/member/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡查询=" + response);
                VipCardListBean info = HstyCallBack(response, VipCardListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员卡手动核销
    public void vipVerification(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pay/manual/payment";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡手动核销=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员卡人工充值
    public void vipRecharge(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pay/manual/charge/money";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡人工充值=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //次卡核销
    public void vipCountVerification(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pay/number/payment";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "次卡核销=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //查询人工充值/次卡核销账单详情
    public void vipBillDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "order/recharge/payment/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡账单详情=" + response);
                PayBean info = HstyCallBack(response, PayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void vipPayWhite(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pay/white/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "手动核销白名单=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void getBountyList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "reward/reportList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "奖励金账单列表=" + response);
                BountyDataBean info = HstyCallBack(response, BountyDataBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void getBountyCollect(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "reward/reportData";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "奖励金统计数据=" + response);
                ShareBean info = HstyCallBack(response, ShareBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void bountyRefundEnable(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "precheck";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "奖励金账单可退=" + response);
                ShareBean info = HstyCallBack(response, ShareBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    public void bountyMoney(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "reward/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "奖励金额=" + response);
                ShareBean info = HstyCallBack(response, ShareBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 获取账号密码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getPayCardAccount(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/getPayCardAccount";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "账号密码=" + response);
                PosAccountBean info = HstyCallBack(response, PosAccountBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 获取第三方商户号
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getPayCardThirdChannelId(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/getPayCardThirdChannelId";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "第三方商户号=" + response);
                PosAccountBean info = HstyCallBack(response, PosAccountBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 保存记录
     *
     * @param context
     * @param tag
     * @param map
     */
    public void savePosRecord(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/savePayCardSn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "保存记录=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 刷卡支付提交订单
     *
     * @param context
     * @param tag
     * @param map
     */
    public void bCardPayment(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "bankCardPay";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "刷卡消费=" + response);
                PosOrderBean info = HstyCallBack(response, PosOrderBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 预授权订单查询
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryAuthDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "queryAuthDetail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "预授权订单查询=" + response);
                PledgePayBean info = HstyCallBack(response, PledgePayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 解冻订单查询
     *
     * @param context
     * @param tag
     * @param map
     */
    public void authOptquery(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "authOptquery";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "解冻订单查询=" + response);
                PledgePayBean info = HstyCallBack(response, PledgePayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 预授权撤销
     *
     * @param context
     * @param tag
     * @param map
     */
    public void authReverse(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "authReverse";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "预授权撤销=" + response);
                PledgePayBean info = HstyCallBack(response, PledgePayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 预授权转支付
     *
     * @param context
     * @param tag
     * @param map
     */
    public void authPay(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "authPay";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "预授权转支付=" + response);
                PledgePayBean info = HstyCallBack(response, PledgePayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 预授权转支付
     *
     * @param context
     * @param tag
     * @param map
     */
    public void authPay2(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "v2/authPay";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "预授权转支付=" + response);
            PledgePayBean info = HstyCallBack(response, PledgePayBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 云播报&云打印设备列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudDeviceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "云播报设备列表=" + response);
                CloudDevicesBean info = HstyCallBack(response, CloudDevicesBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /***
     * 打印小票前查询云打印列表
     * */
    public void cloudDeviceListForPrint(final Context context, final String tag, Map<String, Object> map, OnResponse databack) {
        String url = Constants.BASE_URL + "merchant/device/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, databack);
    }

    /**
     * 云打印
     */
    public void cloudPrint(final Context context, final String tag, Map<String, Object> map, OnResponse databack) {
        String url = Constants.BASE_URL + "cloudprint/msgprint";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, databack);
    }

    /**
     * 绑定汇播报
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudBind(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/bind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定汇播报=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 汇播报设置
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudSet(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/setUp";
        String data = SignUtil.getInstance().createAppSign(map, time);
        LogUtil.d("data====" + data);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "汇播报设置=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 云打印设置
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudPrintSet(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/updateSn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        LogUtil.d("data====" + data);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "云打印设置=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 汇播报详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/deviceDeatil";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "云播报详情=" + response);
                CloudDetailBean info = HstyCallBack(response, CloudDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 云打印详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudPrintDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/querySn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "云打印详情=" + response);
                CloudPrintDetailBean info = HstyCallBack(response, CloudPrintDetailBean.class);
                SetUp setUpBean = HstyCallBack(info.getData().getSetUp(), SetUp.class);
                info.getData().setSetUpBean(setUpBean);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 云打印详情2
     * 与deviceDeatil接口查的是同一个表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cloudPrintDetail2(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/queryPrinterDetailBySn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "云打印详情2=" + response);
                CloudPrintDetailBean info = HstyCallBack(response, CloudPrintDetailBean.class);
                SetUp setUpBean = HstyCallBack(info.getData().getSetUp(), SetUp.class);
                info.getData().setSetUpBean(setUpBean);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 云打印 清空云打印机的待打印数据
     *
     * @param context
     * @param tag
     * @param map
     */
    public void clearCloudPrintWaitCount(Context context, String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cloudprint/clearMsg";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "清空云打印机的待打印数据=" + response);
                CloudPrintClearDataBean info = HstyCallBack(response, CloudPrintClearDataBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 云打印机设置
     *
     * @param context
     * @param tag
     * @param map
     */
    public void updatePrinterBysn(Context context, String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/deviceSettingBysn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "云打印机设置=" + response);
                BaseBean info = HstyCallBack(response, BaseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 查询云打印机需要关联的收款码、收款设备、收银员三个列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryAvailabbleInfo(Context context, String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "device/queryAvailabbleInfo";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询云打印机需要关联的收款码、=" + response);
                QueryAvailabbleInfoBean info = HstyCallBack(response, QueryAvailabbleInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 绑定设备
     *
     * @param context
     * @param tag
     * @param map
     */
    public void bindDevice(Context context, String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "device/bind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定设备=" + response);
                BaseBean info = HstyCallBack(response, BaseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 门店收款码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void qrcodeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/find/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "门店收款码=" + response);
                StoreCodeListBean info = HstyCallBack(response, StoreCodeListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 登录二维码状态
     *
     * @param context
     * @param tag
     * @param map
     */
    public void loginCodeStatus(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "qrcodeLogin/handleStatus";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "登录二维码状态=" + response);
                ScanLoginBean info = HstyCallBack(response, ScanLoginBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 二维码登录
     *
     * @param context
     * @param tag
     * @param map
     */
    public void qrcodeLogin(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "qrcodeLogin";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "二维码登录=" + response);
                ScanLoginBean info = HstyCallBack(response, ScanLoginBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 申请升级服务
     *
     * @param context
     * @param tag
     * @param map
     */
    public void applyUpdateService(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "baseMchMember/eppcm/applyUpgrade";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "申请升级服务=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 花呗分期费率
     *
     * @param context
     * @param tag
     * @param map
     */
    public void hbfqRateList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/pay/hbfqFreeDetail";
        String data = gson.toJson(map);
//        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "花呗分期费率=" + response);
                HbfqRateBean info = HstyCallBack(response, HbfqRateBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 花呗分期统计
     *
     * @param context
     * @param tag
     * @param map
     */
    public void hbfqCollect(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "hbfq/query/statistics";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "花呗分期统计=" + response);
                HbfqDataBean info = HstyCallBack(response, HbfqDataBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 花呗分期统计
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryMchHbfqEnable(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "queryMchHbfqEnable";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "是否可用花呗分期=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 商户是否开通扫码点单
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryScoEnable(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "sco/queryScoEnable";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户是否开通扫码点单=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 获取扫码点单地址
     *
     * @param context
     * @param tag
     * @param map
     */
    public void mchOauth(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "scoAuth/mchOauth";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取扫码点单地址=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 扫码点单推送列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void orderPushList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "sco/push/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "扫码点单推送列表=" + response);
                VoiceBean info = HstyCallBack(response, VoiceBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 扫码点单推送设置
     *
     * @param context
     * @param tag
     * @param map
     */
    public void orderPushSet(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "sco/edit/push/set";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "扫码点单推送设置=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收款设备关联员工列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void linkEmployeeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/find/user/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收款设备关联员工列表=" + response);
                LinkEmployeeBean info = HstyCallBack(response, LinkEmployeeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收款设备关联员工
     *
     * @param context
     * @param tag
     * @param map
     */
    public void deviceLink(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/bind/cashier";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收款设备关联员工=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 微收银激活码收银员绑定
     *
     * @param context
     * @param tag
     * @param map
     */
    public void activationCodeUserBind(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "activationcode/bindCashier";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "收银员绑定=" + response);
            Info info = HstyCallBack(response, Info.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 语音播报总开关
     *
     * @param context
     * @param tag
     * @param map
     */
    public void voiceAllSwitch(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "voice/edit/voice/sum/set";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "语音播报总开关=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 查询收银员权限
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryCashierLimit(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "query/cashier/perm";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询收银员权限=" + response);
                CashierLimitBean info = HstyCallBack(response, CashierLimitBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 风控白名单
     *
     * @param context
     * @param tag
     * @param map
     */
    public void riskWhiteCheck(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "risk/white/check";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "风控白名单=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 功能配置
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getConfig(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "org/config";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "功能配置=" + response);
                FuncBean info = HstyCallBack(response, FuncBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 易生刷卡支付通知
     *
     * @param context
     * @param tag
     * @param map
     */
    public void payNotify(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "pay/notify";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "易生刷卡支付通知=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 易生刷卡退款通知
     *
     * @param context
     * @param tag
     * @param map
     */
    public void refundNotify(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "refund/notify";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "易生刷卡退款通知=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 易生刷卡撤销通知
     *
     * @param context
     * @param tag
     * @param map
     */
    public void reverseNotify(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "reverse/notify";
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "易生刷卡撤销通知=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收款单列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getReceiptList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "receipt/order/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收款单列表=" + response);
                ReceiptListBean info = HstyCallBack(response, ReceiptListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收款单创建
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getReceiptCreate(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "receipt/order/create";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收款单创建=" + response);
                ReceiptBean info = HstyCallBack(response, ReceiptBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收款单关闭
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getReceiptClose(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "receipt/order/close";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收款单关闭=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收款单详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getReceiptDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "receipt/order/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收款单详情=" + response);
                ReceiptBean info = HstyCallBack(response, ReceiptBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 商户是否开通到家
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryDaoJiaEnable(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "daoJiaAuth/queryDaoJiaEnable";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户是否开通到家=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 到家跳转链接
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getDaojiaUrl(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "daoJiaAuth/getJumpUrl";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "到家跳转链接=" + response);
                SendHomeBean info = HstyCallBack(response, SendHomeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 商户开通付费详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryDaojiaEdition(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "daojia/queryEdition";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户开通付费详情=" + response);
                VlifeOpenDetailBean info = HstyCallBack(response, VlifeOpenDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 开通到家
     *
     * @param context
     * @param tag
     * @param map
     */
    public void openDaojia(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "daoJiaAuth/openMchV2";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "开通到家=" + response);
                VlifeOpenBean info = HstyCallBack(response, VlifeOpenBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 报表选择日期【月】
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getSelectMonthList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getSelectMonthList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "报表选择日期【月】=" + response);
                ReportSelectMonthBean info = HstyCallBack(response, ReportSelectMonthBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 报表选择日期【日】
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getSelectDayList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getSelectDayList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
//                LogUtil.e("Jeremy", "报表选择日期【日】=" + response);
                ReportSelectDayBean info = HstyCallBack(response, ReportSelectDayBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 报表选择日期【周】
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getSelectWeekList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getSelectWeekList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "报表选择日期【周】=" + response);
                ReportSelectWeekBean info = HstyCallBack(response, ReportSelectWeekBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 报表数据概览
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getTradeStatistics(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getTradeStatistics";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "报表数据概览=" + response);
                ReportDataView info = HstyCallBack(response, ReportDataView.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 近七日交易趋势
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getTradeStatisticsByDayTrendings(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getTradeStatisticsByDayTrendings";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "近七日交易趋势=" + response);
                ReportTradeTrendBean info = HstyCallBack(response, ReportTradeTrendBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 单日交易趋势
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getHourTradeStatistics(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getHourTradeStatistics";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "单日交易趋势=" + response);
                ReportTradeTrendBean info = HstyCallBack(response, ReportTradeTrendBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 支付占比
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getTradeStatisticsByProvider(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getTradeStatisticsByProvider";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支付占比=" + response);
                ReportPercentBean info = HstyCallBack(response, ReportPercentBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 终端占比
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getTradeStatisticsByTermType(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/v2/app/report/getTradeStatisticsByTermType";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "终端占比=" + response);
                ReportPercentBean info = HstyCallBack(response, ReportPercentBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 猫酷会员信息
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getUserInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/getUserInfo";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "猫酷会员信息=" + response);
                VipInfoBean info = HstyCallBack(response, VipInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 猫酷会员权益折扣
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getDiscountInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/getDiscountInfo";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "猫酷会员权益折扣=" + response);
                VipInfoBean info = HstyCallBack(response, VipInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 查询猫酷会员可用券
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getCanUseByMallCardNo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/getCanUseByMallCardNo";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询猫酷会员可用券=" + response);
                CouponListBean info = HstyCallBack(response, CouponListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 猫酷券核销
     *
     * @param context
     * @param tag
     * @param map
     */
    public void batchUseByVCodes(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/batchUseByVCodes";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "猫酷券核销=" + response);
                CouponVerifyBean info = HstyCallBack(response, CouponVerifyBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 猫酷券核销撤销
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cancelUse(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/cancelUse";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "猫酷券核销撤销=" + response);
                CouponReverseBean info = HstyCallBack(response, CouponReverseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 扫码加猫酷券
     *
     * @param context
     * @param tag
     * @param map
     */
    public void addCanUse(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/addCanUse";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "扫码加猫酷券=" + response);
                CouponInfoBean info = HstyCallBack(response, CouponInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 核销券选定校验
     *
     * @param context
     * @param tag
     * @param map
     */
    public void checkByVCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/checkByVCode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "核销券选定校验=" + response);
                CouponCheckBean info = HstyCallBack(response, CouponCheckBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 删除店长
     *
     * @param context
     * @param tag
     * @param map
     */
    public void deleteManager(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "storeManager/clear";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "删除店长=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 删除收银员
     *
     * @param context
     * @param tag
     * @param map
     */
    public void deleteCashier(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashier/clear/cashier";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "删除收银员=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void paySiteList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/query";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-列表=" + response);
                PaySiteList info = HstyCallBack(response, PaySiteList.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-新增
     *
     * @param context
     * @param tag
     * @param map
     */
    public void addPaySite(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/create";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-新增=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getSiteDevice(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-详情=" + response);
                PaySiteList info = HstyCallBack(response, PaySiteList.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-关联设备
     *
     * @param context
     * @param tag
     * @param map
     */
    public void linkDevice(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/associate";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-关联设备=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-设备列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getSiteDeviceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/deviceList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-设备列表=" + response);
                PaySiteList info = HstyCallBack(response, PaySiteList.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-扫码关联收银点
     *
     * @param context
     * @param tag
     * @param map
     */
    public void captureSiteLink(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/manualAssociate";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-扫码关联收银点=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-扫码关联收银点
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cancelSiteLink(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "cashPoint/disassociate";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-扫码关联收银点=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 收银点-白名单
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cashPointCheck(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "risk/cashPoint/white/check";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银点-白名单=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //发送短信验证接口
    public void send(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/smsCode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "发送短信验证=" + response);
                SendMsgBean info = HstyCallBack(response, SendMsgBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改法人信息
    public void legalInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/editLegalPersonInfo";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改法人信息=" + response);
                SendMsgBean info = HstyCallBack(response, SendMsgBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改结算卡信息
    public void bankaccountInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/editBankAccount";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改结算卡信息=" + response);
                SendMsgBean info = HstyCallBack(response, SendMsgBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //支付类型
    public void payTypeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/getPayType";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支付类型=" + response);
                PayTypeStatusBean info = HstyCallBack(response, PayTypeStatusBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //微信实名认证查询
    public void getWxRealNameAuth(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/getRealNameAuth";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "微信实名认证查询=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //支付宝实名认证查询
    public void getAliRealNameAuth(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/getAliRealNameAuth";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支付宝实名认证查询=" + response);
                AliRealNameAuthBean info = HstyCallBack(response, AliRealNameAuthBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改记录详情
    public void getModifyRecordDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/queryUpdateRecord";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改记录详情=" + response);
                ModifyRecordBean info = HstyCallBack(response, ModifyRecordBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改记录列表
    public void getModifyRecordList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/v2/queryUpdateRecordState";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改记录列表=" + response);
                ModifyRecordBean info = HstyCallBack(response, ModifyRecordBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 会员列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void vipList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "card/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员列表=" + response);
                HSVipListBean info = HstyCallBack(response, HSVipListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 会员数量统计
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryMemberCollect(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "card/queryMemberCollect";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员数量统计=" + response);
                HSVipCollectBean info = HstyCallBack(response, HSVipCollectBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 会员详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryCardDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "card/queryCardDetail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员详情=" + response);
                HSVipInfoBean info = HstyCallBack(response, HSVipInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 会员卡二维码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryNewMemberCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "queryDrawMember/new";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡二维码=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 会员卡卡套配置查询服务
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cardconfigquery(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "card/cardconfigquery";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡卡套配置查询服务=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 商户已开通优惠券类型接口
     *
     * @param context
     * @param tag
     * @param map
     */
    public void couponTypeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "coupon/mch/enable";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "商户已开通优惠券类型接口=" + response);
            CouponTypeListBean info = HstyCallBack(response, CouponTypeListBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 饭卡团购券详情查询
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryCoupon(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/queryCoupon";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "饭卡团购券详情查询=" + response);
            CouponHsInfoBean info = HstyCallBack(response, CouponHsInfoBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 饭卡团购券核销
     *
     * @param context
     * @param tag
     * @param map
     */
    public void couponUse(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "funcard/couponUse";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "饭卡团购券核销=" + response);
            CouponHsInfoBean info = HstyCallBack(response, CouponHsInfoBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 饭卡团购券列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryCouponList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "funcard/queryCouponList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "饭卡团购券列表=" + response);
            CouponHsListBean info = HstyCallBack(response, CouponHsListBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 是否开通饭卡
     *
     * @param context
     * @param tag
     * @param map
     */
    public void openFuncard(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "open/funcard";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "是否开通饭卡=" + response);
            Info info = HstyCallBack(response, Info.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 商户是否同时开通饭卡和饭卡核销
     *
     * @param context
     * @param tag
     * @param map
     */
    public void openFuncardVerification(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "open/funcard/verification";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "是否开通饭卡和饭卡核销=" + response);
            Info info = HstyCallBack(response, Info.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 设备类型列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void deviceTypeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/getAppShowCategoryList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "设备类型列表=" + response);
                DeviceTypeBean info = HstyCallBack(response, DeviceTypeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 已绑定设备
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getDeviceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/newList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "已绑定设备=" + response);
                DeviceListBean info = HstyCallBack(response, DeviceListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 根据sn查询设备信息
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getDeviceInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/device/querySn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "根据sn查询设备信息=" + response);
                DeviceInfoBean info = HstyCallBack(response, DeviceInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 凯盈电子发票
     * 获取绑定状态
     */
    public void queryMchStatusEleTicket(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "crestv/queryMchStatus";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "电子发票_获取绑定状态=" + response);
                QueryMchStatusEleTicketBean info = HstyCallBack(response, QueryMchStatusEleTicketBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 凯盈电子发票
     * 终端设备绑定
     */
    public void deviceBindEleTicket(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "crestv/deviceBind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "电子发票_终端设备绑定=" + response);
                BaseBean<String> info = HstyCallBack(response, BaseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 凯盈电子发票
     * 牌码绑定
     */
    public void boardCodeBindEleTicket(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "crestv/qrcodeBind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "电子发票_终牌码绑定=" + response);
                BaseBean<String> info = HstyCallBack(response, BaseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 凯盈电子发票
     * 商户入驻入口
     */
    public void getManagerIndexUrlEleTicket(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "crestv/index";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "电子发票_商户入驻入口=" + response);
                BaseBean<String> info = HstyCallBack(response, BaseBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 刷卡数据同步埋点
     *
     * @param context
     * @param tag
     * @param map
     */
    public void bcardResultTrack(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/track/h5";
        String data = new Gson().toJson(map);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "刷卡数据同步埋点=" + response);
        });
    }


    /**
     * 功能权限
     *
     * @param context
     * @param tag
     * @param map
     */
    public void newConfig(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.H5_BASE_URL + "/app/org/newConfig";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "功能权限=" + response);
            ConfigBean info = HstyCallBack(response, ConfigBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    /**
     * 应用列表--查询
     *
     * @param context
     * @param tag
     * @param map
     */
    public void querySvcAppList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "svc/querySvcAppList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "应用列表--查询=" + response);
                SvcAppListBean info = HstyCallBack(response, SvcAppListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 应用开通
     *
     * @param context
     * @param tag
     * @param map
     */
    public void openFunction(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "svc/open";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "应用开通=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 应用开通查询
     *
     * @param context
     * @param tag
     * @param map
     */
    public void haveOpenSvcApp(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "svc/haveOpenSvcApp";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "应用开通查询=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 带金额收款码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getMoneyQrcode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/get/fixed/amount/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "带金额收款码=" + response);
                QrImageBean info = HstyCallBack(response, QrImageBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 换绑收款码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void changeBindQrcode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/change/bind/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "换绑收款码=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 解绑收款码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void unbindQrcode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "store/unbind/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "解绑收款码=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 获取商户推送新平台信息
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getPushMode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "get/push/mode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取商户推送新平台信息=" + response);
                PushModeBean info = HstyCallBack(response, PushModeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 商户切换2.0
     *
     * @param context
     * @param tag
     * @param map
     */
    public void switchNewPlatform(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "switch/new/platform";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户切换2.0=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 更新推送新平台提示频率
     *
     * @param context
     * @param tag
     * @param map
     */
    public void updatePushFrequency(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "update/push/frequency";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "更新推送新平台提示频率=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 查询对应员工角色是否具备退款权限
     *
     * @param context
     * @param tag
     * @param map
     */
    public void userPermission(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "auth/query/user/permission";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询对应员工角色是否具备退款权限=" + response);
                WhiteListBean info = HstyCallBack(response, WhiteListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 员工重置密码
     *
     * @param context
     * @param tag
     * @param map
     */
    public void empResetPwd(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "emp/reset/password";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "员工重置密码=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 智慧经营
     *
     * @param context
     * @param tag
     * @param map
     */
    public void smartManagement(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "market/loginH5";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "智慧经营=" + response);
                UrlBean info = HstyCallBack(response, UrlBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }
    /**
     * 固定二维码设置
     *
     * @param context
     * @param tag
     * @param map
     */
    public void fixedQRCodeSet(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/fixedQRCodeSet";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "固定二维码设置=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }
    /**
     * 固定二维码设置查询
     *
     * @param context
     * @param tag
     * @param map
     */
    public void fixedQRCodeSetQuery(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "merchant/fixedQRCodeSetQuery";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "固定二维码设置查询=" + response);
                QrcodeSetDetailBean info = HstyCallBack(response, QrcodeSetDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 泛型返回，在xutils的 httputils 中 访问网络成功后，onSuccess方法中调用
     *
     * @param arg0
     * @param
     * @param <T>
     * @return
     */
    public <T> T HstyCallBack(String arg0, Class clazz) {
        return (T) gson.fromJson(arg0, clazz);
    }
}
