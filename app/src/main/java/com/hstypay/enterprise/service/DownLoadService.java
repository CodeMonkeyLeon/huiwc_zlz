package com.hstypay.enterprise.service;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.ProgressBean;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.SpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by admin on 2017/8/12.
 */

public class DownLoadService extends Service {
    /**
     * 安卓系统下载类
     **/
    private boolean isInstall = false;
    private String appurl;
    //private String DOWNLOADPATH = "/hwc/";//下载路径，如果不定义自己的路径，6.0的手机不自动安装
    private String mVersionName;
    private String mIntentName;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //EventBus.getDefault().register(this);
        appurl = intent.getStringExtra("appurl");
        mVersionName = intent.getStringExtra("versionName");
        mIntentName = intent.getStringExtra(Constants.INTENT_NAME);
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + AppHelper.getDownloadApkDir();
        File file = new File(path);
        if (file.exists()) {
            if (!file.delete())
                LogUtil.d("Jeremy-删除文件失败");
        }
        try {
            // 调用下载
            LogUtil.d("版本升级---" + appurl);
            downloadApp(appurl);
            //downloadApp("http://" + appurl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

//=============================================================

    //Eventbus接收数据,服务返回
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onUpdate(NoticeEvent event) {
        if (event.getTag().equals(Constants.UPDATE_INSTALL_TAG)) {
            String msg = (String) event.getMsg();
            switch (event.getCls()) {
                case Constants.GET_ORDER_DETAIL_TRUE:
                    if (msg != null) {
                        if (msg.equals("install_app")) {
                            //installAPK(manager.getUriForDownloadedFile(mDownId), MyApplication.getContext());
                            isInstall = true;
                        }
                    }
                    break;
            }
        }
    }

    public void downloadApp(final String url) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File file = null;
                InputStream is = null;
                FileOutputStream fos = null;
                try {
                    LogUtil.d("版本升级---" + url);
                    Request request = new Request.Builder()
                            .url(url)
                            .build();
                    Response response = new OkHttpClient().newCall(request).execute();
                    String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath() + AppHelper.getDownloadApkDir();
                    if (response.isSuccessful()) {
                        byte[] buf = new byte[2048];
                        int len = 0;

                        long total = response.body().contentLength();
                        SpUtil.putString(MyApplication.getContext(), "length", String.valueOf(total));
                        file = new File(SDPath);
                        if (!file.getParentFile().exists()) {
                            LogUtil.d("父文件夹不存在，创建之");
                            file.getParentFile().mkdirs();
                        }
                        if (!file.exists()) {
                            if (!file.createNewFile())
                                LogUtil.d("Jeremy-创建文件失败");
                        } else {
                            if (file.length() == total) {
                                EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOAD_SUCCESS, file));
                                return;
                            } else {
                                if (!file.delete())
                                    LogUtil.d("Jeremy-删除文件失败");
                                file = new File(SDPath);
                                if (!file.createNewFile())
                                    LogUtil.d("Jeremy-创建文件失败");
                            }
                        }
                        is = response.body().byteStream();
                        fos = new FileOutputStream(file, true);
                        ProgressBean progressBean = new ProgressBean();
                        progressBean.setTotalSize(total);
                        long sum = 0;
                        while ((len = is.read(buf)) != -1) {
                            fos.write(buf, 0, len);
                            sum += len;
                            progressBean.setDownSize(sum);
                            EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOADING, progressBean));
                        }
                        fos.flush();
                        fos.close();
                        is.close();
                        Log.d("h_bl", "文件下载成功");
                        SpStayUtil.putString(MyApplication.getContext(), Constants.UPDATE_APP_URL, SDPath);
                        SpStayUtil.putString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME, mVersionName);
                        EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOAD_SUCCESS, file));
                        stopSelf();
                    }
                } catch (Exception e) {
                    Log.d("h_bl", "文件下载失败" + e.toString());
                    e.printStackTrace();
                    EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOAD_FAILED));
                    if (!file.delete())
                        LogUtil.d("Jeremy-删除文件失败");
                } finally {
                    if (fos !=null) {
                        try {
                            fos.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if(is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

}
