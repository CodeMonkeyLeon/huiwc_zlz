
package com.hstypay.enterprise.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.hstypay.enterprise.BuildConfig;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PrintContentBean;
import com.hstypay.enterprise.bean.PushDataBean;
import com.hstypay.enterprise.bean.PushModel;
import com.hstypay.enterprise.broadcast.MerchantStatusReceiver;
import com.hstypay.enterprise.broadcast.NotificationClickReceiver;
import com.hstypay.enterprise.broadcast.WebviewReceiver;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.voice.VoiceUtils;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTNotificationMessage;
import com.igexin.sdk.message.GTTransmitMessage;

import org.greenrobot.eventbus.EventBus;

public class GetuiReciverService extends GTIntentService {

    private PushModel mPushdata;

    @Override
    public void onReceiveServicePid(Context context, int i) {

    }

    @Override
    public void onReceiveClientId(Context context, String s) {
        LogUtil.i("Jeremy", "onReceiveClientId=client1==" + s);
        SpUtil.putString(context, Constants.GETUI_CLIENT_ID, s);
        /*Map<String, Object> map = new HashMap<>();
        if (AppHelper.getAppType() == 1) {
            map.put("client", Constants.REQUEST_CLIENT_POS);
        } else if (AppHelper.getAppType() == 2) {
            map.put("client", Constants.REQUEST_CLIENT_APP);
        }
        map.put("pushDeviceId", s);*/
        //ServerClient.newInstance(MyApplication.getContext()).getuiPushVoice(MyApplication.getContext(),Constants.GETUI_PUSH_VOICE_TAG,map);
        if (!TextUtils.isEmpty(s)) {
            sendReceiveClientIdBroadcast(context, s);
        }
    }

    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage gtTransmitMessage) {
        byte[] payload = gtTransmitMessage.getPayload();

        Log.e("zhouwei", "接收状1====" + gtTransmitMessage.getMessageId());
        Log.e("zhouwei", "接收状2====" + gtTransmitMessage.toString());
        Log.e("zhouwei", "接收状3====" + gtTransmitMessage.getPkgName());
        Log.e("zhouwei", "接收状4====" + gtTransmitMessage.getAppid());
        Log.e("zhouwei", "接收状5====" + new String(gtTransmitMessage.getPayload()));

        String data = new String(payload);
        LogUtil.d("data====" + data);
        Gson gson = new Gson();
        mPushdata = gson.fromJson(data, PushModel.class);
        if (mPushdata != null) {
            LogUtil.d("time====" + (System.currentTimeMillis() - mPushdata.getCreateDate()));
            if ((System.currentTimeMillis() - mPushdata.getCreateDate()) < 3 * 60000) {
                if (mPushdata.getMessageKind() != 80002 || MyApplication.getIsMerchant()) {
                    if (mPushdata.getMessageKind() == 70002) {
                        //扫码点餐打印信息
                        if (!TextUtils.isEmpty(mPushdata.getContent())) {
                            PrintContentBean bean = gson.fromJson(mPushdata.getContent(), PrintContentBean.class);
                            EventBus.getDefault().post(new NoticeEvent(Constants.TAG_PRINT_ORDER_CONTENT, Constants.ON_EVENT_TRUE, bean));
                        }
                    } else {
                        showNotify(context, mPushdata);
                    }
                }
            }
        }
    }

    private synchronized void playFile(final String fileName, final long ms) {
        if (VoiceUtils.with(this).getIsPlay()) {
            LogUtil.d("play-voice===" + "正在播放语音 ");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Thread.sleep(100);//休眠0.1秒
                        playFile(fileName, ms);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                    /**
                     * 要执行的操作
                     */
                }
            }.start();
        } else {
            LogUtil.d("play-voice===" + "不冲突");
            VoiceUtils.with(this).playFile(fileName, ms);
        }
    }

    private synchronized void play(final String str) {
        if (VoiceUtils.with(this).getIsPlay()) {
            LogUtil.d("正在播放语音 ");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Thread.sleep(100);//休眠0.1秒
                        play(str);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                    /**
                     * 要执行的操作
                     */
                }
            }.start();
        } else {
            LogUtil.d("不冲突");
            VoiceUtils.with(this).play(str, true);
        }
    }


    private void showNotify(Context context, PushModel data) {
        /*NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);*/
        int id = (int) (System.currentTimeMillis() / 1000);
        Intent clickIntent = null;
        if (data.getMessageKind() == 90002 || data.getMessageKind() == 90003 || data.getMessageKind() == 90005) {
            //非API交易收款成功语音播放
            //90002正扫固定二维码的收款成功(正扫)，90003反扫收款成功(取消了原有的反扫收款成功主动播报语音功能，改为推送播报),90005押金收款成功
            //if ((data.getMessageKind() == 90002 && MyApplication.isOpenAllVoice()) || ((data.getMessageKind() == 90003 || data.getMessageKind() == 90005) && MyApplication.isOpenAllVoice() &&  MyApplication.isOpenVoice())) {
            if (MyApplication.isOpenAllVoice() && MyApplication.isOpenNotApiVoice()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        play(mPushdata.getTradeMoney());
                    }
                }).start();
            }
            sendOrderBroadcast(context);
            //sendTodayDataBroadcast(context);
            if (!MyApplication.getIsCasher()) {
                sendTodayDataBroadcast(this);
            }
            sendReportDataBroadcast(context);
            clickIntent = new Intent(context, NotificationClickReceiver.class);//点击通知之后要发送的广播
            clickIntent.putExtra(Constants.INTENT_KEY_ORDERNO, data.getOrderNo());
            clickIntent.putExtra(Constants.INTENT_KEY_MESSAGEKIND, data.getMessageKind());
        } else if (data.getMessageKind() == 90006) {
            //API交易收款成功语音播放
            if (MyApplication.isOpenAllVoice() && MyApplication.isOpenApiVoice()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        play(mPushdata.getTradeMoney());
                    }
                }).start();
            }
            sendOrderBroadcast(context);
            //sendTodayDataBroadcast(context);
            if (!MyApplication.getIsCasher()) {
                sendTodayDataBroadcast(this);
            }
            sendReportDataBroadcast(context);
            clickIntent = new Intent(context, NotificationClickReceiver.class);//点击通知之后要发送的广播
            clickIntent.putExtra(Constants.INTENT_KEY_ORDERNO, data.getOrderNo());
            clickIntent.putExtra(Constants.INTENT_KEY_MESSAGEKIND, data.getMessageKind());
        } else if (data.getMessageKind() == 90004) {
            //用户取消支付,防逃单语音
            LogUtil.d("play-voice===" + "用户取消支付");
            if (MyApplication.isOpenAllVoice() && MyApplication.isOpenCancelPayWarnVoice()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        playFile("cancel_pay", 4000);
                    }
                }).start();
            }
            clickIntent = new Intent(context, NotificationClickReceiver.class);//点击通知之后要发送的广播
            clickIntent.putExtra(Constants.INTENT_KEY_MESSAGEKIND, data.getMessageKind());

        } else if (data.getMessageKind() == 60002) {//到家消息推送
            if (MyApplication.isOpenDaoJiaOrderVoice()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        playFile(mPushdata.getVoiceContent(), 4000);
                    }
                }).start();
            }
            if (Build.VERSION.SDK_INT >= 26 && MyApplication.getContext().getApplicationInfo().targetSdkVersion > 22) {
                if (Settings.canDrawOverlays(MyApplication.getContext())) {
                    sendExamineBroadcast(data, BuildConfig.APPLICATION_ID + ".RegisterActivity");
                }
            } else {
                PackageManager pm = context.getPackageManager();
                boolean permission = (PackageManager.PERMISSION_GRANTED == pm.checkPermission("android.permission.SYSTEM_ALERT_WINDOW", context.getPackageName()));
                /*if (permission | "Xiaomi".equals(Build.MANUFACTURER) || "vivo".equals(Build.MANUFACTURER)) {
                    sendExamineBroadcast(data,BuildConfig.APPLICATION_ID+".RegisterActivity");
                } else {*/
                sendExamineBroadcast(data, BuildConfig.APPLICATION_ID + ".RegisterActivity");
//                }
            }
            clickIntent = new Intent(context, WebviewReceiver.class);//点击通知之后要发送的广播
            PushDataBean pushDataBean = new Gson().fromJson(data.getData(), PushDataBean.class);
            if (pushDataBean != null && !TextUtils.isEmpty(pushDataBean.getUrl())) {
                clickIntent.putExtra("url", pushDataBean.getUrl());
            }
        } else if (data.getMessageKind() == 80002) {
            //商户审核状态
            SpUtil.putInt(MyApplication.getContext(), Constants.SP_EXAMINE_STATUS, data.getExamineStatus());
            Intent mIntent = new Intent(Constants.ACTION_MERCHANT_STATUS);
            sendBroadcast(mIntent);
            if (Build.VERSION.SDK_INT >= 26 && MyApplication.getContext().getApplicationInfo().targetSdkVersion > 22) {
                if (Settings.canDrawOverlays(MyApplication.getContext())) {
                    sendExamineBroadcast(data, BuildConfig.APPLICATION_ID + ".MerchantInfoActivity");
                }
            } else {
                PackageManager pm = context.getPackageManager();
                boolean permission = (PackageManager.PERMISSION_GRANTED == pm.checkPermission("android.permission.SYSTEM_ALERT_WINDOW", context.getPackageName()));
               /* if (permission | "Xiaomi".equals(Build.MANUFACTURER) || "vivo".equals(Build.MANUFACTURER)) {
                    sendExamineBroadcast(data,BuildConfig.APPLICATION_ID+".MerchantInfoActivity");
                } else {*/
                sendExamineBroadcast(data, BuildConfig.APPLICATION_ID + ".MerchantInfoActivity");
//                }
            }
            clickIntent = new Intent(context, MerchantStatusReceiver.class);//点击通知之后要发送的广播
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), id, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
       /* notification.setLatestEventInfo(this, contentTitle, contentText, contentIntent);
        notificationManager.notify(id, notification);*/
        Notification notification = null;

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ONE_ID = getPackageName();
            Uri mUri = Settings.System.DEFAULT_NOTIFICATION_URI;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ONE_ID, "driver", NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription("description");
            manager.createNotificationChannel(mChannel);
            mChannel.setSound(mUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
            notification = new Notification.Builder(this, CHANNEL_ONE_ID)
                    .setChannelId(CHANNEL_ONE_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(data.getTitle())// 设置通知栏标题
                    .setContentText(data.getContent())
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setAutoCancel(true)
                    .build();
        } else {
            // 提升应用权限
            notification = new Notification.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(data.getTitle())// 设置通知栏标题
                    .setContentText(data.getContent())
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setAutoCancel(true)

                    .build();
        }
        if (data.getMessageKind() == null) {
            manager.notify(0, notification);
        } else {
            manager.notify(data.getMessageKind(), notification);
        }
        /*mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(data.getTitle())// 设置通知栏标题
                .setContentText(data.getContent())
                // <span style="font-family: Arial;">/设置通知栏显示内容</span>
                .setContentIntent(pendingIntent)
                // 设置通知栏点击意图
                // .setNumber(number) //设置通知集合的数量
                //            .setTicker("测试通知来啦")
                // 通知首次出现在通知栏，带上升动画效果的
                //            .setWhen(System.currentTimeMillis())
                // 通知产生的时间，会在通知信息里显示，一般是系统获取到的时间
                //            .setPriority(Notification.PRIORITY_DEFAULT)
                // 设置该通知优先级
                // .setAutoCancel(true)//设置这个标志当用户单击面板就可以让通知将自动取消
                .setOngoing(true)
                // ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                // 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合
                // Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音 // requires VIBRATE
                // permission
                .setSmallIcon(R.mipmap.ic_launcher);// 设置通知小ICON

        mNotificationManager.notify(0, mBuilder.build());*/
    }

    private void sendExamineBroadcast(PushModel data, String activityName) {
        Intent intent = new Intent(Constants.ACTION_PUSH_STATUS);
        intent.putExtra("title", data.getTitle());
        intent.putExtra("content", data.getContent());
        PushDataBean pushDataBean = new Gson().fromJson(data.getData(), PushDataBean.class);
        if (pushDataBean != null && !TextUtils.isEmpty(pushDataBean.getUrl())) {
            intent.putExtra("url", pushDataBean.getUrl());
        }
        intent.putExtra("activityName", activityName);
        intent.setComponent(new ComponentName(AppHelper.getAppPackageName(MyApplication.getContext()),
                "com.hstypay.enterprise.broadcast.PushReceiver"));
        sendBroadcast(intent);
    }

    private void sendOrderBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_ORDER_DATA);
        context.sendBroadcast(intent);
    }

    private void sendTodayDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_TODAY_DATA);
        context.sendBroadcast(intent);
    }

    private void sendReportDataBroadcast(Context context) {
        Intent intent = new Intent(Constants.ACTION_REPORT_DATA);
        context.sendBroadcast(intent);
    }

    private void sendReceiveClientIdBroadcast(Context context, String clientId) {
        Intent intent = new Intent(Constants.ACTION_RECEIVED_CLIENT_ID_PUSH);
        intent.putExtra(Constants.INTENT_KEY_CLIENTID, clientId);
        context.sendBroadcast(intent);
    }

    @Override
    public void onReceiveOnlineState(Context context, boolean b) {


        LogUtil.i("zhouwei", "推送状态" + b);
    }

    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage gtCmdMessage) {

    }

    @Override
    public void onNotificationMessageArrived(Context context, GTNotificationMessage gtNotificationMessage) {

    }

    @Override
    public void onNotificationMessageClicked(Context context, GTNotificationMessage gtNotificationMessage) {

    }

}

