package com.hstypay.enterprise.service;

import android.app.DownloadManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.network.NoticeEvent;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.SpStayUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

/**
 * Created by admin on 2017/8/12.
 */

public class UpdateService extends Service {
    /**
     * 安卓系统下载类
     **/
    private DownloadManager manager;
    private boolean isInstall = false;
    /**
     * 接收下载完的广播
     **/
    private DownloadCompleteReceiver receiver;
    private String appurl;
    private String DOWNLOADPATH = "/hwc/";//下载路径，如果不定义自己的路径，6.0的手机不自动安装
    private long mDownId;
    private int mVersionCode;
    private long mEnqueueId = 0;
    private DownloadChangeObserver downloadObserver;
    public static final Uri CONTENT_URI = Uri.parse("content://downloads/my_downloads");

    /**
     * 初始化下载器
     **/
    private void initDownManager() {
        manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        receiver = new DownloadCompleteReceiver();
        //String url=appurl;
        String url = "http://" + appurl;
        LogUtil.d("版本升级apkUrl---", url);
        DownloadManager.Request down = new DownloadManager.Request(Uri.parse(url));
        down.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE
                | DownloadManager.Request.NETWORK_WIFI);
        down.setAllowedOverRoaming(false);
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(url));
        down.setMimeType(mimeString);
        down.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);//VISIBILITY_VISIBLE显示下载界面
        down.setVisibleInDownloadsUi(false);
        down.setDestinationInExternalPublicDir(DOWNLOADPATH, "hwc.apk");
        down.setTitle("汇旺财");
        mEnqueueId = manager.enqueue(down);
        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        downloadObserver = new DownloadChangeObserver(null);
        getContentResolver().registerContentObserver(CONTENT_URI, true, downloadObserver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //EventBus.getDefault().register(this);
        appurl = intent.getStringExtra("appurl");
        //String versionCode = intent.getStringExtra("versionCode");
        mVersionCode = intent.getIntExtra("versionCode", 0);

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + DOWNLOADPATH + "hwc.apk";
        String s[] = path.split("[/]");
        for (int i = 0; i < s.length; i++) {
            String s1 = s[3];
        }

        File file = new File(path);
        if (file.exists()) {
            if (!file.delete()) {
                LogUtil.d("Jeremy-删除文件失败");
            }
        }
        try {
            // 调用下载
            initDownManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (receiver != null)
            // 注销下载广播
            unregisterReceiver(receiver);
        EventBus.getDefault().unregister(this);
        getContentResolver().unregisterContentObserver(downloadObserver);
        super.onDestroy();
    }

    // 接受下载完成后的intent
    class DownloadCompleteReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //判断是否下载完成的广播
            if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                MyApplication.isupdate = true;
                //获取下载的文件id
                mDownId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                if (manager.getUriForDownloadedFile(mDownId) != null) {
                    Uri uriForDownloadedFile = manager.getUriForDownloadedFile(mDownId);
                    LogUtil.d("版本升级--"+uriForDownloadedFile);
                    SpStayUtil.putString(context, Constants.UPDATE_APP_URL, uriForDownloadedFile + "");
                    SpStayUtil.putInt(context, Constants.UPDATE_VERSION_NAME, mVersionCode);
                    EventBus.getDefault().post(new NoticeEvent(Constants.UPDATE_SERVICE_TAG, Constants.GET_ORDER_DETAIL_TRUE, uriForDownloadedFile));

                } else {
                    //Toast.makeText(context, "下载失败", Toast.LENGTH_SHORT).show();
                }
                //停止服务并关闭广播
                UpdateService.this.stopSelf();
            } else {
                //EventBus.getDefault().post(new NoticeEvent(Constants.UPDATE_SERVICE_TAG, Constants.GET_ORDER_DETAIL_FALSE, "downloadSuccess"));
                LogUtil.d("");
            }
        }

        private void installAPK(Uri apk, Context context) {
            if (Build.VERSION.SDK_INT < 23) {
                Intent intents = new Intent();
                intents.setAction("android.intent.action.VIEW");
                intents.addCategory("android.intent.category.DEFAULT");
                intents.setType("application/vnd.android.package-archive");
                intents.setData(apk);
                intents.setDataAndType(apk, "application/vnd.android.package-archive");
                intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intents);

            } else {
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + DOWNLOADPATH + "hwc.apk");
                if (file.exists()) {
                    openFile(file, context);
                }
            }
        }

        private void installAPK(Context context) {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + DOWNLOADPATH + "hwc.apk");
            if (file.exists()) {
                openFile(file, context);
            } else {
                Toast.makeText(context, "下载失败", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void openFile(File file, Context context) {
        Intent intent = new Intent();
        // intent.addFlags(268435456);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction("android.intent.action.VIEW");
        String type = getMIMEType(file);
        intent.setDataAndType(Uri.fromFile(file), type);

        try {
            context.startActivity(intent);
        } catch (Exception var5) {
            var5.printStackTrace();
            Toast.makeText(context, "没有找到打开此类文件的程序", Toast.LENGTH_SHORT).show();
        }
    }

    public String getMIMEType(File var0) {
        String var1 = "";
        String var2 = var0.getName();
        String var3 = var2.substring(var2.lastIndexOf(".") + 1, var2.length()).toLowerCase();
        var1 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(var3);
        return var1;
    }

//=============================================================

    //Eventbus接收数据,服务返回
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onUpdate(NoticeEvent event) {
        if (event.getTag().equals(Constants.UPDATE_INSTALL_TAG)) {
            String msg = (String) event.getMsg();
            switch (event.getCls()) {
                case Constants.GET_ORDER_DETAIL_TRUE:
                    if (msg != null) {
                        if (msg.equals("install_app")) {
                            //installAPK(manager.getUriForDownloadedFile(mDownId), MyApplication.getContext());
                            isInstall = true;
                        }
                    }
                    break;
            }
        }
    }

    class DownloadChangeObserver extends ContentObserver {
        public DownloadChangeObserver(Handler handler) {
            super(handler);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onChange(boolean selfChange) {
            queryDownloadStatus();
        }
    }

    private void queryDownloadStatus() {
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(mEnqueueId);
        Cursor c = manager.query(query);
        if (c != null && c.moveToFirst()) {
            int fileSizeIdx = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
            int bytesDLIdx = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
            int fileSize = c.getInt(fileSizeIdx);
            int bytesDL = c.getInt(bytesDLIdx);
            LogUtil.d("版本升级===", bytesDL + "--" + fileSize);
            EventBus.getDefault().post(new NoticeEvent(Constants.UPDATE_PROGRESS_TAG, fileSize+"", bytesDL));
        }
    }

}
