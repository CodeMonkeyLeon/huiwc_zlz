package com.hstypay.enterprise.service.live;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.HandlerManager;
import com.hstypay.enterprise.utils.LogUtil;

import java.util.Timer;
import java.util.TimerTask;

public class ForegroundLiveService extends Service {

    private static final String TAG = "ForegroundLiveService";
    public static final int NOTIFICATION_ID = 0x11;
    public static final String SETTINGS_ACTION = "android.settings.APPLICATION_DETAILS_SETTINGS";

    private PushTimerTask mTimerTask;

    private boolean isTag = true;

    private boolean isStop =false;

    Handler handler = new Handler()
    {
        public void handleMessage(android.os.Message msg)
        {

            if (msg.what == HandlerManager.FOREGROUNDLIVE)
            {
                isStop = true;
            }

        };
    };

    public ForegroundLiveService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "ForegroundLiveService ->onCreate");
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SETTINGS_ACTION);
        broadcastIntent.setData(Uri.fromParts("package", getPackageName(), null));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ONE_ID = getPackageName();
            Uri mUri = Settings.System.DEFAULT_NOTIFICATION_URI;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ONE_ID, "driver", NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription("description");
            mChannel.setShowBadge(false);
//            mChannel.setSound(mUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
            NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(mChannel);

            notification = new Notification.Builder(this, CHANNEL_ONE_ID)
                    .setChannelId(CHANNEL_ONE_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("收款及语音播报接收中，请勿关闭")
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            // 提升应用权限
            notification = new Notification.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("收款及语音播报接收中，请勿关闭")
                    .setContentIntent(pendingIntent)
                    .build();
        }
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
//        startForeground(10000, notification);
        /*String voice = PreferenceUtil.getString("voice", "open");
        if (voice.equalsIgnoreCase("open")) {
        }*/
        startForeground(NOTIFICATION_ID, notification);
        //API 18以上，发送Notification并将其置为前台后，启动InnerService
        /*Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("汇旺财");
        builder.setContentText("收款及语音播报接收中，请勿关闭");

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SETTINGS_ACTION);
        broadcastIntent.setData(Uri.fromParts("package", getPackageName(), null));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        Log.i("hehui","ForegroundLiveService onCreate");
        String voice = PreferenceUtil.getString("voice", "open");
        if (voice.equalsIgnoreCase("open")) {
            startForeground(NOTIFICATION_ID, builder.build());
        }*/
        HandlerManager.registerHandler(HandlerManager.FOREGROUNDLIVE,handler);
//            startService(new Intent(this, InnerService.class));
//        }
    }
    private  Timer timer = new Timer();
    class PushTimerTask extends TimerTask{

        @Override
        public void run() {
            try {
                //orderPushReport();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.i("hehui","ForegroundLiveService onStartCommand");

        try {
            if(timer != null){
                timer.schedule(new PushTimerTask(),5*60*1000L,30*60*1000L);

            }else{
                if(mTimerTask != null){
                    mTimerTask.cancel();
                }
                timer = new Timer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        Log.i(TAG, TAG + "---->onDestroy,stop service "+isStop);
        if(!isStop){
            Intent intent = new Intent(getApplicationContext(), ForegroundLiveService.class);
            startService(intent);
        }else{
            startService(new Intent(this, InnerService.class));
        }
    }

    public static class InnerService extends Service {
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public void onCreate() {
            super.onCreate();
            Log.i(TAG, "Service ->onCreate");
            //发送与KeepLiveService中ID相同的Notification，然后将其取消并取消自己的前台显示
            /*Notification.Builder builder = new Notification.Builder(this);
            builder.setSmallIcon(R.mipmap.ic_launcher);*/
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(SETTINGS_ACTION);
            broadcastIntent.setData(Uri.fromParts("package", getPackageName(), null));
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Notification notification = null;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String CHANNEL_ONE_ID = getPackageName();
                Uri mUri = Settings.System.DEFAULT_NOTIFICATION_URI;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ONE_ID, "driver", NotificationManager.IMPORTANCE_LOW);
                mChannel.setDescription("description");
                mChannel.setShowBadge(false);
//            mChannel.setSound(mUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
                NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                manager.createNotificationChannel(mChannel);

                notification = new Notification.Builder(this, CHANNEL_ONE_ID)
                        .setChannelId(CHANNEL_ONE_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText("收款及语音播报接收中，请勿关闭")
                        .setContentIntent(pendingIntent)
                        .build();
            } else {
                // 提升应用权限
                notification = new Notification.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText("收款及语音播报接收中，请勿关闭")
                        .setContentIntent(pendingIntent)
                        .build();
            }
            notification.flags = Notification.FLAG_ONGOING_EVENT;
            notification.flags |= Notification.FLAG_NO_CLEAR;
            notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
            startForeground(NOTIFICATION_ID, notification);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    stopForeground(true);
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.cancel(NOTIFICATION_ID);
                    stopSelf();
                }
            }, 100);
        }
    }
}
