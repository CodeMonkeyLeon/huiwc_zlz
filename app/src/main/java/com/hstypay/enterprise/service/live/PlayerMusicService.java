package com.hstypay.enterprise.service.live;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.HandlerManager;
import com.hstypay.enterprise.utils.LogUtil;

public class PlayerMusicService extends Service {
    private final static String TAG = "hehui";
    private MediaPlayer mMediaPlayer;
    private boolean isStop =false;
    public static final int NOTIFICATION_ID = 0x11;
    public static final String SETTINGS_ACTION = "android.settings.APPLICATION_DETAILS_SETTINGS";

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.FOREGROUNDLIVE) {
                isStop = true;
            }
        };
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SETTINGS_ACTION);
        broadcastIntent.setData(Uri.fromParts("package", getPackageName(), null));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ONE_ID = getPackageName();
            Uri mUri = Settings.System.DEFAULT_NOTIFICATION_URI;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ONE_ID, "driver", NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription("description");
            mChannel.setShowBadge(false);
//            mChannel.setSound(mUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
            NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(mChannel);

            notification = new Notification.Builder(this, CHANNEL_ONE_ID)
                    .setChannelId(CHANNEL_ONE_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("收款及语音播报接收中，请勿关闭")
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            // 提升应用权限
            notification = new Notification.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("收款及语音播报接收中，请勿关闭")
                    .setContentIntent(pendingIntent)
                    .build();
        }
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
        startForeground(NOTIFICATION_ID, notification);
        try{
            LogUtil.i(TAG, TAG + "---->onCreate,start mc service");
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.silent);
            mMediaPlayer.setLooping(true);
            HandlerManager.registerHandler(HandlerManager.FOREGROUNDLIVE,handler);
            LogUtil.i(TAG, TAG + "---->onCreated over");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.i(TAG, TAG + "---->onStartCommand");
        new Thread(new Runnable() {
            @Override
            public void run() {
                startPlayMusic();
            }

        }).start();
        return START_STICKY;
    }

    private void startPlayMusic() {
        if (mMediaPlayer != null) {
            LogUtil.i(TAG, "mMediaPlayer is playing " + mMediaPlayer.getDuration());
        }
        if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
            LogUtil.i(TAG, "start play music ");
            mMediaPlayer.start();
        }
    }

    private void stopPlayMusic() {
        if (mMediaPlayer != null) {
            LogUtil.i(TAG, "stop music ");
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopPlayMusic();
        LogUtil.i(TAG, TAG + "---->onDestroy,stop service");
        if(!isStop){
            Intent intent = new Intent(getApplicationContext(), PlayerMusicService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        }
    }
}
