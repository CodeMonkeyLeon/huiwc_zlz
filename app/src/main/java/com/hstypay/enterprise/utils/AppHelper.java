package com.hstypay.enterprise.utils;

import static android.content.Context.TELEPHONY_SERVICE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.vanke.DeviceInfo;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;


@SuppressLint("NewApi")
public class AppHelper {
    private static final String TAG = "AppHelper";

    public static boolean isSdcardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getCacheDir()
            throws Exception {
        if (!isSdcardExist()) {
            throw new Exception("SD卡状态异常，请检查文件读写权限！");
        }
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        if (getApkType() == 1) {
            path += Constants.FILE_CACHE_ROOT_ZYT;
        } else if (getApkType() == 2) {
            path += Constants.FILE_CACHE_ROOT_YIJIU;
        } else if (getApkType() == 3) {
            path += Constants.FILE_CACHE_ROOT_WJY;
        } else if (getApkType() == 5) {
            path += Constants.FILE_CACHE_ROOT_DYZF;
        } else if (getApkType() == 0) {
            path += Constants.FILE_CACHE_ROOT;
        } else {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            if (!StringUtils.isEmptyOrNull(installChannel)){
                path += "/"+installChannel+"/";
            }else {
                path += Constants.FILE_CACHE_ROOT;
            }
        }
        File dir = new File(path);
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                throw new Exception("创建文件缓存目录失败");
            }
        }
        return path;
    }

    public static String getImageCacheDir(String filePath) {
        String path;
        try {
            path = getCacheDir() + System.currentTimeMillis() + ImageUtil.getFileType(filePath);
        } catch (Exception e) {
            e.printStackTrace();
            path = "";
        }
        return path;
    }


    public static String getImgCacheDir()
            throws Exception {
        return FileUtils.getAppCache();
    }

    /**
     * 创建缓存目录
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getAppCacheDir()
            throws Exception {
        if (!isSdcardExist()) {
            throw new Exception("SD卡不存在");
        }
        File root = new File(FileUtils.getRootPath());
        LogUtil.d("path1===" + root);
        if (!root.exists()) {
            if (!root.mkdirs()) {
                throw new Exception("创建文件缓存目录失败");
            }
        }
        String path = FileUtils.getAppCache();
        LogUtil.d("path===" + path);
        File dir = new File(path);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new Exception("创建文件缓存目录失败");
            }
        }
        String logPath = FileUtils.getAppLog();
        File logDir = new File(logPath);
        if (!logDir.exists()) {
            if (!logDir.mkdirs()) {
                throw new Exception("创建log文件缓存目录失败");
            }
        }
        String appPath = FileUtils.getAppPath();
        File f = new File(appPath);
        if (!f.exists()) {
            if (!f.mkdirs()) {
                throw new Exception("创建APP文件缓存目录失败");
            }
        }

        return path;
    }

    /**
     * 获取数据库的存放路径，策略为 1.有卡，则主DB存放在应用程序的目录下
     * ，辅DB防止卡的目录下。 2.无卡，则主辅DB都放在应用程序目录下。
     *
     * @param mainAble 是否有卡
     * @return
     * @throws
     */
    public static String getDBPath(boolean mainAble, String dbName) {
        if (isSdcardExist())//false)// //不允许 读取数据库文件
        {
            // 有卡
            if (mainAble) {
                return MyApplication.getContext().getDatabasePath(dbName).getAbsolutePath();
            } else {
                try {
                    String path = getCacheDir() + "databases/";
                    File dir = new File(path);
                    if (!dir.exists()) {
                        if (!dir.mkdirs()) {
                            Log.e(TAG, "--->create databases dir fail!");
                            return null;
                        }
                    }
                    return path + dbName;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        } else {
            // 无卡
            return MyApplication.getContext().getDatabasePath(dbName).getAbsolutePath();
        }
    }

    /**
     * 保存图片到本地
     *
     * @param fileDirect 存放路径
     * @param bitmap     数据
     * @return 图片存储路径
     */
    public static String saveImag(String fileDirect, Bitmap bitmap, CompressFormat format, String fileName) {
        File file = new File(fileDirect);
        String filePath = fileDirect + File.separatorChar + fileName;
        if (!file.exists())
            file.mkdirs();
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(filePath));
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (dos != null)
                    dos.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        bitmap.compress(format, 100, dos);
        return filePath;
    }

    /**
     * 版本
     *
     * @param context
     * @return
     */
    public static int getVerCode(Context context) {
        int verCode = -1;
        try {
            verCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return verCode;
    }

    public static String getVerName(Context context) {
        String verName = "";
        try {
            verName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return verName;
    }

    public static String getOsVersion() {
        return android.os.Build.DISPLAY;
    }

    public static int getAndroidSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String getAndroidSDKVersionName() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * 程序名称
     *
     * @param context
     * @return
     */
    public static String getAppName(Context context) {
        String verName = context.getResources().getText(R.string.app_name).toString();
        return verName;
    }

    public static String getAppPackageName(Context context) {
        String pName = "";
        try {
            pName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return pName;
    }

    /*@SuppressLint("MissingPermission")
    public static String getImei(Context context) {

        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
            String imei = tm.getDeviceId();
            if (imei == null) {
                imei = "";
            }
            return imei;
        } catch (Exception e) {

            return System.currentTimeMillis() + "";
        }

    }*/

    /*@SuppressLint("MissingPermission")
    public static String getImsi(Context context) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        String imsi = mTelephonyMgr.getSubscriberId();
        if (imsi == null) {
            imsi = "";
        }
        return imsi;
    }*/

    /**
     * 手机品牌跟
     */
    public static String[] getBrand() {
        String brand = android.os.Build.BRAND;
        String mType = android.os.Build.MODEL;
        return new String[]{brand, mType};

    }

    // ////////////////////////////////////////////////////////////////////
    private static final String[] PHONES_PROJECTION = new String[]{Phone._ID, Phone.DISPLAY_NAME, Phone.NUMBER,
            Phone.CONTACT_ID, Photo.TIMES_CONTACTED, Phone.LAST_TIME_CONTACTED};

    /**
     * 实现文本复制功能
     *
     * @param text
     */
    public static void copy(String text, Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(text);
    }

    /**
     * 实现粘贴功能
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static String paste(Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        return cmb.getText().toString().trim();
    }

    /**
     * 震动
     *
     * @param context
     */
    public static void execVibrator(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 10, 20, 30}; // 停止 开启 停止 开启
        vibrator.vibrate(pattern, -1); //重复两次上面的pattern 如果只想震动一次，index设为
    }

    public static String getNetworkType(Context context) {
        String strNetworkType = "UnKnown";
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getType() == 1) {
            strNetworkType = "WIFI";
        } else if (activeNetworkInfo != null && activeNetworkInfo.getType() == 0) {
            String subtypeName = activeNetworkInfo.getSubtypeName();
            switch (((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkType()) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    strNetworkType = "2G";
                    break;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD: //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP: //api<13 : replace by 15
                    strNetworkType = "3G";
                    break;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    strNetworkType = "4G";
                    break;
                default:
                    if (subtypeName.equalsIgnoreCase("TD-SCDMA") || subtypeName.equalsIgnoreCase("WCDMA") || subtypeName.equalsIgnoreCase("CDMA2000")) {
                        strNetworkType = "3G";
                        break;
                    }
                    strNetworkType = subtypeName;
                    break;
            }
        }
        return strNetworkType;
    }

    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值 ， 或者异常)，则返回值为空
     */
    public static String getAppMetaData(Context ctx, String key) {
        if (ctx == null || TextUtils.isEmpty(key)) {
            return null;
        }
        String resultData = "";
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return resultData;
    }

    /**
     * @return app类型 1 POS，2，phone
     */
    public static int getAppType() {
        int appType = 0;
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case "wpos":
                case "shangmi":
                case "lepos":
                case "lkl":
                case "ybx":
                case "a920":
                case "yipos":
                case "hdy":
                case "xdl":
                case Constants.HUIFU:
                case Constants.LIANDI:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                case Constants.WIZARPOS:
                case Constants.MIBAGPAY_LIANDI:
                    appType = 1;
                    break;
                default:
                    appType = 2;
            }
        }
        return appType;
    }

    /**
     * 交易时候上送deviceInfo字段对应的值
     * 需要和后端确认
     * @return
     */
    public static String getDeviceInfo() {
        String deviceInfo = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case "wpos":
                case "shangmi":
                case "lepos":
                case "lkl":
                case "ybx":
                case "a920":
                case "yipos":
                case "hdy":
                case "xdl":
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    deviceInfo = Constants.PAY_CLIENT_POS + "_" + getSN();
                    break;
                case Constants.HUIFU:
                    deviceInfo = Constants.HPAY_POS_HUIFU + "_" + getSN();
                    break;
                case Constants.LIANDI:
                case Constants.WIZARPOS:
                case Constants.MIBAGPAY_LIANDI:
                    deviceInfo = Constants.HPAY_POS_EASYPAY + "_" + getSN();
                    break;
                case Constants.WJY_WIZARPOS:
                    deviceInfo = Constants.HPAY_POS_Q2 + MyApplication.getDeviceNo();
                    break;
                case Constants.WJY_YINSHANG:
                    deviceInfo = Constants.HPAY_POS_YINSHANG + MyApplication.getDeviceNo();
                    break;
                default:
                    deviceInfo = Constants.PAY_CLIENT_APP;
                    break;
            }
        }
        return deviceInfo;
    }

    /**
     * 区分是否是行业APP
     *
     * @return 0汇旺财APP， 其它为OEM定制版APP
     */
    public static int getApkType() {
        int apkType = 0;
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    apkType = 3;
                    break;
                case Constants.YIJIU:
                    apkType = 2;
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    apkType = 1;
                    break;
                case Constants.YSYF:
                    apkType = 4;
                    break;
                case Constants.DYZF:
                    apkType = 5;
                    break;
                case Constants.BDZF:
                    apkType = 6;
                    break;
                case Constants.HSTYEMS:
                    apkType = 7;
                    break;
                case Constants.KINGDEEPAY:
                    apkType = 8;
                    break;
                case Constants.WZFHYPAY:
                    apkType = 9;
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    apkType = 10;
                    break;
                default:
                    apkType = 0;
                    break;
            }
        }
        return apkType;
    }

    /**
     * 中银通pos
     *
     * @return
     */
    public static boolean isZytPos() {
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            if (Constants.ZYTPAY_LIANDI.equals(installChannel) || Constants.ZYTPAY_XGD.equals(installChannel) || Constants.ZYTPAY_XDL_N900.equals(installChannel)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 区分是否是行业APP
     *
     * @return
     */
    public static boolean getSwipeRefresh() {
        boolean showGif = false;
        /*String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                case Constants.YIJIU:
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.YSYF:
                case Constants.DYZF:
                    showGif = false;
                    break;
                default:
                    showGif = false;
                    break;
            }
        }*/
        return showGif;
    }

    /**
     * 区分UserAgent
     *
     * @return
     */
    public static String getUserAgent(Context context) {
        String userAgent = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    userAgent = " wjyfPay/" + VersionUtils.getVersionCode(context) + " (wjyfPay_app_android)";
                    break;
                case Constants.YIJIU:
                    userAgent = " e9Pay/" + VersionUtils.getVersionCode(context) + " (e9Pay_app_android)";
                    break;
                case Constants.YSYF:
                    userAgent = " ysyfPay/" + VersionUtils.getVersionCode(context) + " (ysyfPay_app_android)";//
                    break;
                case Constants.BDZF:
                    userAgent = " bdzfPay/" + VersionUtils.getVersionCode(context) + " (bdzfPay_app_android)";//
                    break;
                case Constants.DYZF:
                    userAgent = " dyzfPay/" + VersionUtils.getVersionCode(context) + " (dyzfPay_app_android)";
                    break;
                case Constants.HSTYEMS:
                    userAgent = " hstyems/" + VersionUtils.getVersionCode(context) + " (hstyemsPay_app_android)";
                    break;
                case Constants.KINGDEEPAY:
                    userAgent = " kingdeepay/" + VersionUtils.getVersionCode(context) + " (kingdeepay_app_android)";
                    break;
                case Constants.WZFHYPAY:
                    userAgent = " wzfhyPay/" + VersionUtils.getVersionCode(context) + " (wzfhyPay_app_android)";
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    userAgent = " mibagPay/" + VersionUtils.getVersionCode(context) + " (mibagPay_app_android)";
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    userAgent = " zytpay/" + VersionUtils.getVersionCode(context) + " (zytpay_app_android)";
                    break;
                default:
                    userAgent = " hstypay/" + VersionUtils.getVersionCode(context) + " (hsty_app_android)";
                    break;
            }
        }
        return userAgent;
    }

    /**
     * 区分是否是行业APP
     *
     * @return
     */
    public static String getHtmlString() {
        String htmlString = "";
        if (!StringUtils.isEmptyOrNull(Constants.ORG_ID)){
            htmlString = "?orgId="+Constants.ORG_ID+"&orgType="+Constants.ORG_TYPE;
        }
        return htmlString;
    }

    /**
     * 区分是否是行业APP
     *
     * @return
     */
    public static String getHtmlAndString() {
        String htmlString = "";
        if (!StringUtils.isEmptyOrNull(Constants.ORG_ID)){
            htmlString = "orgId="+Constants.ORG_ID+"&orgType="+Constants.ORG_TYPE+"&";
        }
        return htmlString;
    }

    /**
     * 返回图片地址
     *
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public static String getPicPath(Uri originalUri) {
        ContentResolver mContentResolver = MyApplication.getContext().getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        } else if ((originalUri + "").contains("/data")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        } else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        } else {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
                    && originalUri.toString().contains("documents")) {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor = mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column,
                        sel,
                        new String[]{id},
                        null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            } else {

                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }

        }
        return originalPath;
    }

    /**
     * 返回手机运营商名称，在调用支付前调用作判断
     *
     * @param context
     * @return
     */
    public static String getProvidersName(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "";
        }
        String ProvidersName = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String IMSI = telephonyManager.getSubscriberId();
        Log.i("qweqwes", "运营商代码" + IMSI);
        if (IMSI != null) {
            if (IMSI.startsWith("46000") || IMSI.startsWith("46002") || IMSI.startsWith("46007")) {
                ProvidersName = "中国移动";
            } else if (IMSI.startsWith("46001") || IMSI.startsWith("46006")) {
                ProvidersName = "中国联通";
            } else if (IMSI.startsWith("46003")) {
                ProvidersName = "中国电信";
            }
            Log.i("qweqwes", "运营商" + ProvidersName);
            return ProvidersName;
        } else {
            return "";
        }
    }

    public static boolean isWeixinAvilible(Context context) {
        final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 判断qq是否可用
     *
     * @param context
     * @return
     */
    public static boolean isQQClientAvailable(Context context) {
        final PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mobileqq")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isServiceRunning(Context context, String serviceName) {
        // 校验服务是否还存在
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningServiceInfo> services = am.getRunningServices(100);
        for (RunningServiceInfo info : services) {
            // 得到所有正在运行的服务的名称
            String name = info.service.getClassName();
            if (serviceName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 设备初始化生成的随机码+设备序列号，组成唯一标识
     * 恢复出厂设置id会变化
     *
     * @return
     */
    public static String getDeviceId() {
        String androidID = Settings.Secure.getString(MyApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidID + Build.SERIAL;
    }

    /**
     * 获取SN
     *
     * @return
     */
    public static String getSN() {
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (Constants.WJY_WIZARPOS.equals(installChannel) || Constants.WJY_YINSHANG.equals(installChannel))
            return MyApplication.getDeviceNo();
        String serial = "";
        //通过android.os获取sn号
        try {
            serial = android.os.Build.SERIAL;
            if (!serial.equals("") && !serial.equals("unknown")) return serial;
        } catch (Exception e) {
            serial = "";
        }

        //通过反射获取sn号
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialno");
            if (!serial.equals("") && !serial.equals("unknown")) return serial;

            //9.0及以上无法获取到sn，此方法为补充，能够获取到多数高版本手机 sn
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) serial = Build.getSerial();
        } catch (Exception e) {
            serial = "";
        }
        return serial;
    }

    /**
     * 获取银商SN
     */
    public static String getYinshangSN() {
        String deviceInfoString = com.ums.AppHelper.getBaseSysInfo(MyApplication.getContext());
        if (!TextUtils.isEmpty(deviceInfoString)) {
            DeviceInfo deviceInfo = new Gson().fromJson(deviceInfoString, DeviceInfo.class);
            return deviceInfo.getSN();
        }
        return "";
    }

    public static String getIPAddress(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {//当前使用2G/3G/4G网络
                try {
                    //Enumeration<NetworkInterface> en=NetworkInterface.getNetworkInterfaces();
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }

            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {//当前使用无线网络
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ipAddress = intIP2StringIP(wifiInfo.getIpAddress());//得到IPV4地址
                return ipAddress;
            }
        } else {
            //当前无网络连接,请在设置中打开网络
        }
        return null;
    }

    /**
     * 将得到的int类型的IP转换为String类型
     *
     * @param ip
     * @return
     */
    public static String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }

    public static String getDownloadApkDir() {

        /*if (AppHelper.getApkType() == 0) {
            return Constants.DOWNLOADPATH + Constants.APK_FILE_NAME;
        }else if (AppHelper.getApkType() == 1) {
            return Constants.DOWNLOADPATH_ZYT + Constants.APK_ZYT_FILE_NAME;
        } else if (AppHelper.getApkType() == 2) {
            return Constants.DOWNLOADPATH_YIJIU + Constants.APK_YIJIU_FILE_NAME;
        } else if (AppHelper.getApkType() == 3) {
            return Constants.DOWNLOADPATH_WJY + Constants.APK_WJY_FILE_NAME;
        } else if (AppHelper.getApkType() == 5) {
            return Constants.DOWNLOADPATH_DYZF + Constants.APK_DYZF_FILE_NAME;
        } else if (AppHelper.getApkType() == 6) {
            return Constants.DOWNLOADPATH_BDZF + Constants.APK_BDZF_FILE_NAME;
        } else if (AppHelper.getApkType() == 7) {
            return Constants.DOWNLOADPATH_HSTYEMS + Constants.APK_HSTYEMS_FILE_NAME;
        } else if (AppHelper.getApkType() == 8) {
            return Constants.DOWNLOADPATH_KINGDEEPAY + Constants.APK_KINGDEEPAY_FILE_NAME;
        } else {*/
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            if (!StringUtils.isEmptyOrNull(installChannel)){
                return  "/"+installChannel+"/"+installChannel+".apk";
            }else {
                return Constants.DOWNLOADPATH + Constants.APK_FILE_NAME;
            }
//        }
    }

    public static void call(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}