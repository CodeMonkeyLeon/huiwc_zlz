package com.hstypay.enterprise.utils;

import android.content.Context;

import com.baidu.mobstat.StatService;
import com.hstypay.enterprise.app.MyApplication;

import java.util.Properties;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils
 * @创建者: Jeremy
 * @创建时间: 2018/9/25 15:41
 * @描述: ${TODO}
 */
public class BaiduMtjUtils {

    public static void eventId(String eventName) {
        StatService.onEvent(MyApplication.getContext(), MD5.getMD5_16(eventName), eventName);
    }

    // 先调用start
    public static void eventStart(String eventName) {
        StatService.onEventStart(MyApplication.getContext(), MD5.getMD5_16(eventName), eventName);
    }

    // 结束时调用end
    public static void eventEnd(String eventName) {
        StatService.onEventEnd(MyApplication.getContext(), MD5.getMD5_16(eventName), eventName);
    }

    public static void eventPro(Context context, String eventId, Properties prop) {
    }
}
