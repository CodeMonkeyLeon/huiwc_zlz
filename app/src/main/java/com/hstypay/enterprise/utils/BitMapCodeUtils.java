package com.hstypay.enterprise.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.Log;

import com.hstypay.enterprise.app.MyApplication;

import java.util.Random;

/**
 * @author kuangzeyu
 * @time 2021/3/2
 * @desc 生成图形验证码
 */
public class BitMapCodeUtils {
    private static final char[] CHARS = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    };

    private static BitMapCodeUtils mCodeUtils;
    private int mPaddingLeft, mPaddingTop;
    private StringBuilder mBuilder = new StringBuilder();
    private Random mRandom = new Random();

    //参数设置
    private  int DEFAULT_CODE_LENGTH = 4;//随机验证码的长度  这里是4位
    private  int DEFAULT_FONT_SIZE = 60;//字体大小
    private  int DEFAULT_LINE_NUMBER = 3;//多少条干扰线
    private  int BASE_PADDING_LEFT = 20; //左边距
    private  int RANGE_PADDING_LEFT = 20;//左边距范围值
    private  int BASE_PADDING_TOP = 22;//上边距
    private  int RANGE_PADDING_TOP = 15;//上边距范围值
    private  int DEFAULT_WIDTH = 300;//默认宽度.图片的总宽
    private  int DEFAULT_HEIGHT = 100;//默认高度.图片的总高
    private  int DEFAULT_COLOR = 0xDF;//默认背景颜色值

    private String code;

    public static BitMapCodeUtils getInstance() {
        if(mCodeUtils == null) {
            mCodeUtils = new BitMapCodeUtils();
        }
        return mCodeUtils;
    }

    /**
     *
     * @param originalCode 要生成验证码的字符串， 当为空时则随机生成6位
     * @param width 初始宽度
     * @param height 初始高度
     * @return
     */
    public Bitmap createBitmap(String originalCode,int width,int height) {
        mPaddingLeft = 0; //每次生成验证码图片时初始化
        mPaddingTop = 0;
        int length = originalCode.length();
        if (width>0){
            DEFAULT_WIDTH = width;
        }
        if (height>0){
            DEFAULT_HEIGHT = height;
        }
        Bitmap bitmap = Bitmap.createBitmap(DEFAULT_WIDTH, DEFAULT_HEIGHT, Config.ARGB_8888);
        bitmap.setDensity(MyApplication.getContext().getResources().getDisplayMetrics().densityDpi);
        Canvas canvas = new Canvas(bitmap);
        code = TextUtils.isEmpty(originalCode)?createCode():originalCode;
//        canvas.drawColor(Color.rgb(DEFAULT_COLOR, DEFAULT_COLOR, DEFAULT_COLOR));//绘制背景
        canvas.drawColor(Color.TRANSPARENT);
        Paint paint = new Paint();
        paint.setTextSize(DEFAULT_FONT_SIZE);

        for (int i = 0; i < code.length(); i++) {
            randomTextStyle(paint);
            randomPadding();
            if ((mPaddingLeft+BASE_PADDING_LEFT)>=DEFAULT_WIDTH){
                DEFAULT_WIDTH += BASE_PADDING_LEFT;
                bitmap = createBitmap(originalCode,DEFAULT_WIDTH,height);
                break;
            }else {
//                canvas.drawText(code.charAt(i) + "" , mPaddingLeft, mPaddingTop, paint);
                canvas.drawText(code.charAt(i) + "" , mPaddingLeft, DEFAULT_HEIGHT*3/4, paint);
            }
        }
        //干扰线
        for (int i = 0; i < DEFAULT_LINE_NUMBER; i++) {
            drawLine(canvas, paint);
        }
        canvas.save();
        //canvas.save(Canvas.ALL_SAVE_FLAG);//保存
        canvas.restore();
        int width1 = bitmap.getWidth();
        return bitmap;
    }
    /**
     * 得到图片中的随机验证码字符串
     * @return
     */
    public String getCode() {
        return code;
    }

    //生成随机验证码
    private String createCode() {
        mBuilder.delete(0, mBuilder.length()); //使用之前首先清空内容
        for (int i = 0; i < DEFAULT_CODE_LENGTH; i++) {
            mBuilder.append(CHARS[mRandom.nextInt(CHARS.length)]);
        }
        return mBuilder.toString();
    }

    //生成干扰线
    private void drawLine(Canvas canvas, Paint paint) {
        int color = randomColor();
        int startX = mRandom.nextInt(DEFAULT_WIDTH);
        int startY = mRandom.nextInt(DEFAULT_HEIGHT);
        int stopX = mRandom.nextInt(DEFAULT_WIDTH);
        int stopY = mRandom.nextInt(DEFAULT_HEIGHT);
        paint.setStrokeWidth(1);
        paint.setColor(color);
        canvas.drawLine(startX, startY, stopX, stopY, paint);
    }

    //随机颜色
    /*private int randomColor() {
        mBuilder.delete(0, mBuilder.length()); //使用之前首先清空内容
        String haxString;
        for (int i = 0; i < 3; i++) {
            haxString = Integer.toHexString(mRandom.nextInt(0xFF));
            if (haxString.length() == 1) {
                haxString = "0" + haxString;
            }
            mBuilder.append(haxString);
        }
        LogUtil.d("tagtag","#" + mBuilder.toString());
        return Color.parseColor("#" + mBuilder.toString());
    }*/
    //随机颜色
    public int randomColor() {
        //红色
        String red;
        //绿色
        String green;
        //蓝色
        String blue;
        //生成红色颜色代码
        //Integer.toHexString(random.nextInt(256)).toUpperCase();
        red = Integer.toHexString(mRandom.nextInt(100)+10).toUpperCase();
        //生成绿色颜色代码
        green = Integer.toHexString(mRandom.nextInt(100)+30).toUpperCase();
        //生成蓝色颜色代码
        blue = Integer.toHexString(mRandom.nextInt(100)+50).toUpperCase();

        //判断红色代码的位数
        red = red.length()==1 ? "0" + red : red ;
        //判断绿色代码的位数
        green = green.length()==1 ? "0" + green : green ;
        //判断蓝色代码的位数
        blue = blue.length()==1 ? "0" + blue : blue ;
        //生成十六进制颜色值
        String color = "#"+red+green+blue;

        return Color.parseColor(color);
    }


    //随机文本样式
    private void randomTextStyle(Paint paint) {
        int color = randomColor();
        paint.setColor(color);
        paint.setFakeBoldText(mRandom.nextBoolean());  //true为粗体，false为非粗体
        float skewX = mRandom.nextInt(5) / 10;//倾斜
        skewX = mRandom.nextBoolean() ? skewX : -skewX;
        paint.setTextSkewX(skewX); //float类型参数，负数表示右斜，整数左斜
//        paint.setUnderlineText(true); //true为下划线，false为非下划线
//        paint.setStrikeThruText(true); //true为删除线，false为非删除线
    }

    //随机间距
    private void randomPadding() {
        mPaddingLeft += BASE_PADDING_LEFT + mRandom.nextInt(RANGE_PADDING_LEFT);
//        mPaddingTop = BASE_PADDING_TOP + mRandom.nextInt(RANGE_PADDING_TOP);
        mPaddingTop = DensityUtils.dip2px(MyApplication.getContext(),BASE_PADDING_TOP);//+ mRandom.nextInt(RANGE_PADDING_TOP);

    }
}
