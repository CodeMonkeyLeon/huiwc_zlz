package com.hstypay.enterprise.utils;

import android.os.Build;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;

public class ConfigUtil {
    /**
     * 打印二维码title
     *
     * @return
     */
    public static String getPrintCodeTitle() {
        String title = UIUtils.getString(R.string.print_hwc_scan_title);
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                case Constants.YIJIU:
                case Constants.YSYF:
                case Constants.DYZF:
                case Constants.BDZF:
                case Constants.HSTYEMS:
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.KINGDEEPAY:
                case Constants.WZFHYPAY:
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    title = UIUtils.getString(R.string.print_zyt_scan_title);
                    break;
                default:
                    title = UIUtils.getString(R.string.print_hwc_scan_title);
                    break;
            }
        }
        return title;
    }

    /**
     * 打印二维码是否可开票
     *
     * @return
     */
    public static boolean printCodeBillEnable() {
        boolean openBillEnable = true;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                case Constants.YIJIU:
                case Constants.YSYF:
                case Constants.DYZF:
                case Constants.BDZF:
                case Constants.HSTYEMS:
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.KINGDEEPAY:
                case Constants.WZFHYPAY:
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    openBillEnable = false;
                    break;
                default:
                    openBillEnable = true;
                    break;
            }
        }
        return openBillEnable;
    }

    /**
     * 是否打印二维码
     * @return
     */
    public static boolean printOrderNoEnable() {
        boolean printOrderNoEnable = true;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    printOrderNoEnable = false;
                    break;
                default:
                    printOrderNoEnable = true;
                    break;
            }
        }
        return printOrderNoEnable;
    }

    /**
     * 是否请求升级接口
     *
     * @return
     */
    public static boolean updateVersionEnable() {
        boolean updateEnable;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case "wpos":
                case "shangmi":
                case "lepos":
                case "lkl":
                case "ybx":
                case "a920":
                case "yipos":
                case "hdy":
                case "xdl":
                case Constants.HUIFU:
                case Constants.WIZARPOS:
                case Constants.LIANDI:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                case Constants.MIBAGPAY_LIANDI:
                    updateEnable = false;
                    break;
                default:
                    updateEnable = true;

            }
        } else {
            updateEnable = false;
        }
        return updateEnable;
    }

    /**
     * 是否请求启动页广告接口
     *
     * @return
     */
    public static boolean advertisementEnable() {
        boolean openAdvEnable = true;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                case Constants.YIJIU:
                case Constants.YSYF:
                case Constants.DYZF:
                case Constants.BDZF:
                case Constants.HSTYEMS:
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.KINGDEEPAY:
                case Constants.WZFHYPAY:
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    openAdvEnable = false;
                    break;
                default:
                    break;
            }
        }
        return openAdvEnable;
    }

    /**
     * 是否接受推送
     *
     * @return
     */
    public static boolean getuiEnable() {
        boolean isPushEnable = true;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    isPushEnable = true;
                    break;
                case Constants.YIJIU:
                case Constants.YSYF:
                case Constants.DYZF:
                case Constants.BDZF:
                case Constants.HSTYEMS:
                case Constants.KINGDEEPAY:
                case Constants.WZFHYPAY:
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    isPushEnable = true;
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    isPushEnable = false;
                    break;
                default:
                    isPushEnable = true;
                    break;
            }
        }
        return isPushEnable;
    }

    /**
     * 是否验证登录渠道号
     *
     * @return
     */
    public static boolean verifyServiceId() {
        boolean verifyServiceId = false;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    verifyServiceId = false;
                    break;
                case Constants.YIJIU:
                    verifyServiceId = false;
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    verifyServiceId = true;
                    break;
                case Constants.YSYF:
                case Constants.DYZF:
                case Constants.BDZF:
                case Constants.HSTYEMS:
                case Constants.KINGDEEPAY:
                case Constants.WZFHYPAY:
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    verifyServiceId = false;
                    break;
                default:
                    verifyServiceId = false;
                    break;
            }
        }
        return verifyServiceId;
    }

    /**
     * 是否获取配置信息
     *
     * @return true:对应的渠道需要请求配置功能信息和登录检查渠道id(共用域名的OEM项目)， false:对应的渠道使用默认的配置功能信息
     */
    public static boolean getConfig() {
        boolean getConfigEnable = false;
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    getConfigEnable = false;
                    break;
                case Constants.YIJIU:
                    getConfigEnable = true;
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    getConfigEnable = true;
                    break;
                case Constants.YSYF:
                case Constants.DYZF:
                case Constants.BDZF:
                case Constants.HSTYEMS:
                case Constants.KINGDEEPAY:
                case Constants.WZFHYPAY:
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    getConfigEnable = true;
                    break;
                default:
                    getConfigEnable = false;
                    break;
            }
        }
        return getConfigEnable;
    }


    //获取OEM测试定制的渠道ID
    public static String getTestOrgId() {
        String orgId = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.YSYF:
                    orgId = "1050000032";//开发环境1050000064  测试环境1050000032
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    orgId = "10100000222";
                    break;
                case Constants.YIJIU:
                    orgId = "1010000401";
                    break;
                case Constants.DYZF:
                    orgId = "1010000948";
                    break;
                case Constants.BDZF:
                    orgId = "1050000193";
                    break;
                case Constants.HSTYEMS:
                    break;
                case Constants.KINGDEEPAY:
                    orgId = "1050000043";
                    break;
                case Constants.WZFHYPAY:
                    //TODO
                    orgId = "1050000045";
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    orgId = "1050000048";
                    break;
                default:
                    break;
            }
        }
        return orgId;
    }

    //获取OEM测试定制的渠道类型
    public static String getTestOrgType() {
        String orgType = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.YSYF:
                    orgType = "5";
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.DYZF:
                    orgType = "1";
                    break;
                case Constants.YIJIU:
                    orgType = "2";
                    break;
                case Constants.BDZF:
                    orgType = "5";
                    break;
                case Constants.HSTYEMS:
                    break;
                case Constants.KINGDEEPAY:
                    orgType = "5";
                    break;
                case Constants.WZFHYPAY:
                    //TODO
                    orgType = "5";
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    orgType = "5";
                    break;
                default:
                    break;
            }
        }
        return orgType;
    }

    //获取OEM正式定制的渠道ID
    public static String getReleaseOrgId() {
        String orgId = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.YSYF:
                    orgId = "1050000031";//灰度1050000030  生产1050000031
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    orgId = "1010000611";
                    break;
                case Constants.YIJIU:
                    orgId = "1010000350";
                    break;
                case Constants.DYZF:
                    orgId = "1010000948";
                    break;
                case Constants.BDZF:
                    orgId = "1050000039";
                    break;
                case Constants.HSTYEMS:
                    orgId = "1010001178";
                    break;
                case Constants.KINGDEEPAY:
                    orgId = "1050000043";
                    break;
                case Constants.WZFHYPAY:
                    orgId = "1050000045";
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    orgId = "1050000048";
                    break;
                default:
                    break;
            }
        }
        return orgId;
    }

    //获取OEM正式定制的渠道类型
    public static String getReleaseOrgType() {
        String orgType = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.YSYF:
                    orgType = "5";
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                case Constants.DYZF:
                    orgType = "1";
                    break;
                case Constants.YIJIU:
                    orgType = "2";
                    break;
                case Constants.BDZF:
                    orgType = "5";
                    break;
                case Constants.HSTYEMS:
                    orgType = "1";
                    break;
                case Constants.KINGDEEPAY:
                    orgType = "5";
                    break;
                case Constants.WZFHYPAY:
                    orgType = "5";
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    orgType = "5";
                    break;
                default:
                    break;
            }
        }
        return orgType;
    }


    /**
     * 升级上传的渠道类型
     *
     * @return
     */
    public static String getAppChannel() {
        String appChannel = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                    appChannel = "WK";
                    break;
                case Constants.YIJIU:
                    appChannel = "YJ";
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    appChannel = "ZYT";
                    break;
                case Constants.YSYF:
                    appChannel = "EMS";
                    break;
                case Constants.DYZF:
                    appChannel = "KTS";
                    break;
                case Constants.BDZF:
                    appChannel = "JIBEI";
                    break;
                case Constants.HSTYEMS:
                    appChannel = "hstyems";
                    break;
                case Constants.KINGDEEPAY:
                    appChannel = "KINGDEE";
                    break;
                case Constants.WZFHYPAY:
                    appChannel = "WZFHY";
                    break;
                case Constants.MIBAGPAY:
                case Constants.MIBAGPAY_LIANDI:
                    appChannel = "MIBAG";
                    break;
                default:
                    appChannel = "HS";
                    break;
            }
        }
        return appChannel;
    }


    /**
     * 区分接口APP版本
     *
     * @return
     */
    public static String getUserHeader() {
        String userAgent = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY:
                case Constants.WJY_WIZARPOS:
                case Constants.WJY_YINSHANG:
                    userAgent = "";
                    break;
                case Constants.YIJIU:
                    userAgent = "YJ";
                    break;
                case Constants.ZYTPAY:
                case Constants.ZYTPAY_LIANDI:
                case Constants.ZYTPAY_XGD:
                case Constants.ZYTPAY_XDL_N900:
                    userAgent = "";
                    break;
                case Constants.WZFHYPAY:
                    userAgent = "WZF";
                    break;
                default:
                    userAgent = "";
                    break;
            }
        }
        return userAgent;
    }

    /**
     * 联系我们URL
     *
     * @return
     */
    public static String getContactUsUrl() {
        String url = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.HWC:
                    url = Constants.URL_CONTACT_US;
                    break;
                default:
                    url = Constants.URL_CONTACT_YSYF;
                    break;
            }
        }
        return url;
    }

    /**
     * 刷卡支付通道
     *
     * @return
     */
    public static String getThirdApiCode() {
        String thirdApiCode = "";
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case Constants.WJY_YINSHANG:
                    thirdApiCode = Constants.HPAY_CARD_YINSHANG;
                    break;
                case Constants.WJY_WIZARPOS:
                case Constants.WIZARPOS:
                case Constants.LIANDI:
                case Constants.MIBAGPAY_LIANDI:
                    thirdApiCode = Constants.ESAYCARD;
                    break;
                case Constants.HUIFU:
                    thirdApiCode = Constants.HPAY_POS_HUIFU;
                    break;
            }
        }
        return thirdApiCode;
    }

    /**
     * 注册url
     *
     * @return
     */
    public static String getRegisterFirstUrl() {
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            if (installChannel.contains(Constants.WJY)) {
                return Constants.H5_BASE_URL + "/static/WebH5/register/contact.html" + AppHelper.getHtmlString();
            }
        }
        return Constants.H5_BASE_URL + "/static/merchant-register/register.html#/hwc-register" + AppHelper.getHtmlString();//h5注册
    }

    /**
     * 进件url
     *
     * @return
     */
    public static String getRegisterSecondUrl() {
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            if (installChannel.contains(Constants.WJY)) {
                return "/static/WebH5/register/trader.html" + AppHelper.getHtmlString();//h5进件
            }
        }
        return "/static/merchant-register/register.html#/certification-type?" + AppHelper.getHtmlAndString() + "userId=" + MyApplication.getUserId() + "&platform=hwc";//h5进件
    }

    /**
     * 是否可唤起小程序
     *
     * @return
     */
    public static boolean getMiniEnable() {
        if (AppHelper.getApkType() == 0 && AppHelper.getAppType() != 1) {
            return true;
        }
        return false;
    }

    /**
     * 手机厂商型号
     * @return
     */
    public static String getModel() {
        if (Build.BRAND != null && Build.MODEL!=null) {
            return Build.BRAND+"/"+Build.MODEL;
        }
        return "";
    }

    private static String getAppMetaData() {
        return AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
    }
}
