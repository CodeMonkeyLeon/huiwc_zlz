package com.hstypay.enterprise.utils;

import com.hstypay.enterprise.BuildConfig;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 14:02
 * @描述: 常量类
 */
public class Constants {
    public static String H5_BASE_URL = BuildConfig.H5_BASE_URL;//正式环境
    public static String CERT_NAME = BuildConfig.CERT_NAME;
    public static String SERVICE_PROVIDER_ID = BuildConfig.SERVICE_PROVIDER_ID;//测试登录渠道号
    public static String ORG_ID = ConfigUtil.getReleaseOrgId();//"10100000222";//OEM测试定制的渠道类型
    public static String ORG_TYPE = ConfigUtil.getReleaseOrgType();//"1";//OEM测试定制的渠道类型
    public static String HS_APP_MANAGE_PLATFORM = "HS_APP_MANAGE_PLATFORM_";//合作伙伴类型
    public static String HS_APP_SERVICE_PROVIDER = "HS_APP_SERVICE_PROVIDER_";//省、市运营
    public static String HS_APP_MERCHANT = "HS_APP_MERCHANT_";//商户
    public static String APP_CODE = HS_APP_MERCHANT;//OEM定制的渠道类型
    public static String BASE_URL = H5_BASE_URL + "/app/";
    public static String H5_MSG_DETAIL_URL = H5_BASE_URL + "/static/WebH5/news/news.html?" + AppHelper.getHtmlAndString() + "newsId=";
    public static String H5_STATIC_QRCODE = H5_BASE_URL + "/app/payMoney?";
    public static String URL_PRINT_QRCODE = H5_BASE_URL + "/static/qOrder/dist/index.html#/order?orderNo=";

    public static String URL_BLUETOOTH_INSTRUCTION = H5_BASE_URL + "/static/WebH5/print/print.html" + AppHelper.getHtmlString();//蓝牙使用说明的h5页面
    public static String URL_VOICE_INSTRUCTION = H5_BASE_URL + "/static/WebH5/voice/voice.html" + AppHelper.getHtmlString();//语音使用说明的h5页面

    public static String URL_REGISTER_FIRST = ConfigUtil.getRegisterFirstUrl();//h5注册
    public static String URL_SIGN_PROTOCOL = H5_BASE_URL + "/static/WebH5/register/protocol_hsty.html" + AppHelper.getHtmlString();//h5汇旺财协议
    public static String URL_COOPERATION_PROTOCOL = H5_BASE_URL + "/static/WebH5/hwc_cooperation_agreement/hwc_cooperation_agreement.html" + AppHelper.getHtmlString();//h5汇旺财平台合作协议
    public static String URL_CHANGE_CARD_ITEM = H5_BASE_URL + "/static/WebH5/notice/notice_change.html" + AppHelper.getHtmlString();//h5更改银行卡注意事项
    public static String URL_CONTACT_US = H5_BASE_URL + "/static/WebH5/opinion_feedback/contact_us.html" + AppHelper.getHtmlString();//h5联系我们(汇旺财)
    public static String URL_CONTACT_YSYF = H5_BASE_URL + "/static/WebH5/contact_us/contact_us.html" + AppHelper.getHtmlString();//h5联系我们(oem)
    public static String URL_VIP_ACTIVE_PREVIEW_ONE = H5_BASE_URL + "/static/WebH5/product_introduction/poster1.html?" + AppHelper.getHtmlAndString() + "poster=";//h5活动预览样式一
    public static String URL_VIP_ACTIVE_PREVIEW_TWO = H5_BASE_URL + "/static/WebH5/product_introduction/poster2.html?" + AppHelper.getHtmlAndString() + "poster=";//h5活动预览样式二
    public static String URL_BANNER_HUABEI = H5_BASE_URL + "/static/WebH5/hbfq/hbfqIntro.html" + AppHelper.getHtmlString();//h5花呗分期详情
    public static String URL_HUABEI_QUESTION = H5_BASE_URL + "/static/WebH5/hbfq/hbfqFaq.html" + AppHelper.getHtmlString();//h5花呗分期常见问题

    public static String URL_USER_PROTOCOL = H5_BASE_URL + "/static/WebH5/protocol/protocol_privacy.html" + AppHelper.getHtmlString();//用户隐私协议
    public static String URL_INFO_PROTOCOL = H5_BASE_URL + "/static/WebH5/protocol/protocol_register.html" + AppHelper.getHtmlString();//个人信息收集协议
    public static String URL_CHANGE_BANK_PROTOCOL = H5_BASE_URL + "/static/WebH5/protocol/protocol_acc.html" + AppHelper.getHtmlString();//更改结算账户协议

    public static void setH5BaseUrl(String h5BaseUrl, String serviceProviderId, String orgId,String orgType, String appCode) {
        H5_BASE_URL = h5BaseUrl;
        SERVICE_PROVIDER_ID = serviceProviderId;
        ORG_ID = orgId;
        ORG_TYPE = orgType;
        if ("1".equals(appCode)) {
            APP_CODE = HS_APP_MANAGE_PLATFORM;
        }
        if ("2".equals(appCode)) {
            APP_CODE = HS_APP_SERVICE_PROVIDER;
        }
        if ("3".equals(appCode)) {
            APP_CODE = HS_APP_MERCHANT;
        }
        BASE_URL = H5_BASE_URL + "/app/";
        H5_MSG_DETAIL_URL = H5_BASE_URL + "/static/WebH5/news/news.html?" + AppHelper.getHtmlAndString() + "newsId=";
        H5_STATIC_QRCODE = H5_BASE_URL + "/app/payMoney?";
        URL_PRINT_QRCODE = H5_BASE_URL + "/static/qOrder/dist/index.html#/order?orderNo=";

        URL_BLUETOOTH_INSTRUCTION = H5_BASE_URL + "/static/WebH5/print/print.html" + AppHelper.getHtmlString();//蓝牙使用说明的h5页面
        URL_VOICE_INSTRUCTION = H5_BASE_URL + "/static/WebH5/voice/voice.html" + AppHelper.getHtmlString();//语音使用说明的h5页面

        URL_REGISTER_FIRST = ConfigUtil.getRegisterFirstUrl();//h5注册

        URL_SIGN_PROTOCOL = H5_BASE_URL + "/static/WebH5/register/protocol_hsty.html" + AppHelper.getHtmlString();//h5汇旺财协议
        URL_COOPERATION_PROTOCOL = H5_BASE_URL + "/static/WebH5/hwc_cooperation_agreement/hwc_cooperation_agreement.html" + AppHelper.getHtmlString();//h5汇旺财平台合作协议
        URL_CHANGE_CARD_ITEM = H5_BASE_URL + "/static/WebH5/notice/notice_change.html" + AppHelper.getHtmlString();//h5更改银行卡注意事项
        URL_CONTACT_US = H5_BASE_URL + "/static/WebH5/opinion_feedback/contact_us.html" + AppHelper.getHtmlString();//h5联系我们
        URL_CONTACT_YSYF = H5_BASE_URL + "/static/WebH5/contact_us/contact_us.html" + AppHelper.getHtmlString();//h5联系我们
        URL_VIP_ACTIVE_PREVIEW_ONE = H5_BASE_URL + "/static/WebH5/product_introduction/poster1.html?" + AppHelper.getHtmlAndString() + "poster=";//h5活动预览样式一
        URL_VIP_ACTIVE_PREVIEW_TWO = H5_BASE_URL + "/static/WebH5/product_introduction/poster2.html?" + AppHelper.getHtmlAndString() + "poster=";//h5活动预览样式二
        URL_BANNER_HUABEI = H5_BASE_URL + "/static/WebH5/hbfq/hbfqIntro.html" + AppHelper.getHtmlString();//h5花呗分期详情
        URL_HUABEI_QUESTION = H5_BASE_URL + "/static/WebH5/hbfq/hbfqFaq.html" + AppHelper.getHtmlString();//h5花呗分期常见问题
        URL_USER_PROTOCOL = H5_BASE_URL + "/static/WebH5/protocol/protocol_privacy.html" + AppHelper.getHtmlString();//用户隐私协议
        URL_INFO_PROTOCOL = H5_BASE_URL + "/static/WebH5/protocol/protocol_register.html" + AppHelper.getHtmlString();//个人信息收集协议
        URL_CHANGE_BANK_PROTOCOL = H5_BASE_URL + "/static/WebH5/protocol/protocol_acc.html" + AppHelper.getHtmlString();//更改结算账户协议
    }

    public static final String APP_USERNAME = "app_username";
    public static final String APP_LOGIN_NAME = "app_login_name";
    public static final String APP_MERCHANT_ID = "app_merchant_id";//商户号
    public static final String APP_MERCHANT_TYPE = "app_merchant_type";//商户类型
    public static final String IS_BOUNTY_MERCHANT = "is_bounty_merchant";//营销商户
    public static final String SP_EMP_ID = "sp_emp_id";//雇员id
    public static final String GETUI_CLIENT_ID = "getui_client_id";//个推设备id
    public static final String ACTION_MERCHANT_STATUS = "action_merchant_status";
    public static final String ACTION_PUSH_STATUS = "intent.action.ACTION_PUSH_STATUS";
    public static final String NEED_CHANGE_PWD = "need_change_pwd";
    public static final String REGISTER_ALLOW_CLOSE = "register_allow_close";
    public static final String REGISTER_ALWAYS_ALLOW_CLOSE = "register_always_allow_close";//一直显示关闭图标
    public static final String REGISTER_ALLOW_CACHE = "register_always_allow_cache";//不清缓存
    public static final String APP_META_DATA_KEY = "InstallChannel";
    public static final String REQUEST_CLIENT_APP = "HPAY_AND";
    public static final String REQUEST_CLIENT_POS = "HPAY_POS";
    public static final String PAY_CLIENT_APP = "HPAY_ANDROID";
    public static final String PAY_CLIENT_POS = "HPAY_POS";

    public static final String HPAY_REFUND_ORDER = "HPAY_REFUND_ORDER";//退款单
    public static final String HPAY_RECEIVE_ORDER = "HPAY_RECEIVE_ORDER";//收款单


    public static boolean isDebug = true;
    public static final String URL_DEFAULT_BANNER_BOUNTY = "url_default_banner_bounty";//默认营销奖励的url
    public static final String URL_DEFAULT_BANNER_HUABEI = "url_default_banner_huabei";//默认花呗图片banner的url
    public static final String FILE_PROVIDER = BuildConfig.APPLICATION_ID + ".fileProvider";
    public static final String SP_SAVE_FILE = "hstypay";
    public static final String SP_SAVE_FILE_STAY = "hstypay_stay";

    public static final String SP_SAVE_FILE_ZYT = "zytpay";
    public static final String SP_SAVE_FILE_STAY_ZYT = "zytpay_stay";

    public static final String SP_LOGIN_TELEPHONE = "sp_login_telephone";//绑定的手机号
    public static final String SP_ADMIN_CHECK_STORE_ID = "sp_admin_check_store_id";
    public static final String SP_ADMIN_CHECK_STORE_NAME = "sp_admin_check_store_name";
    public static final String SP_IS_NOT_FIRST_USE = "sp_is_not_first_use";//是否不是第一次使用
    public static final String SP_GOODS_NAME = "sp_goods_name";//商品名称
    public static final String SP_SHORT_NAME = "sp_short_name";//商户简称/名称
    public static final String SP_EXAMINE_REMARK = "sp_examine_remark";//审核不通过原因
    public static final String SP_EXAMINE_STATUS = "sp_examine_status";//审核状态
    public static final String SP_MERCHANT_TYPE = "sp_merchant_type";//商户类型
    public static final String SP_COMPANY_MERCHANT_TYPE = "sp_company_merchant_type";//企业商户类型
    public static final String SP_DEFAULT_STORE_NAME = "sp_default_store_name";//默认门店名
    public static final String SP_DEFAULT_STORE_ID = "sp_default_store_id";//默认门店id
    public static final String SP_LOGIN_RECORD = "sp_login_record";//登录名
    public static final String SP_IS_UPDATE = "sp_is_update";//是否需要更新
    public static final String SP_IS_NOTICE_CLOSED = "sp_is_notice_closed";//是否首页消息关闭
    public static final String SP_IS_QRCODE_NOTICE_CLOSED = "sp_is_qrcode_notice_closed";//是否首页消息关闭
    public static final String SP_MONEY_LIMIT = "sp_money_limit";//单笔限额
    public static final String SP_AUTO_STATUS = "sp_auto_status";//免密登录状态
    public static final String SP_PRINT_ACTIVE_ENABLE = "sp_print_active_enable";//小票活动是否可用
    public static final String SP_PRINT_ACTIVE_URL = "sp_print_active_url";//小票活动url
    public static final String SP_PRINT_ACTIVE_TITLE = "sp_print_active_title";//小票活动标题
    public static final String SP_PRINT_TYPE_TWO = "sp_print_type_two";//是否打印2联
    public static final String SP_IS_WHITE = "sp_is_white";//是否白名单
    public static final String SP_IS_OPEN_MEMBER = "sp_is_open_member";//是否开通会员
    public static final String SP_SHOW_COLLECT_MEMBER = "sp_show_collect_member";//是否显示统计页面会员数据
    public static final String SP_STORE_VOICE = "sp_store_voice";//是否开启了门店语音播报
    public static final String SP_POS_LOGIN_STATUS = "sp_pos_login_status";//pos登录状态
    public static final String SP_POS_MEMBER_ID = "sp_pos_member_id";//pos商户号
    public static final String SP_POS_EMP_ACCOUNT = "sp_pos_emp_account";//pos登录账号
    public static final String SP_PLEDGE_OPEN_STATUS = "sp_pledge_status";//预授权开通状态
    public static final String SP_HIDE_PLEDGE_REFUND = "sp_hide_pledge_refund";//隐藏退押金指引
    public static final String SP_PRINT_DEVICE = "sp_print_device";//打印设备
    public static final String SP_PRINT_DEVICE1 = "sp_print_device1";//打印设备
    public static final String SP_PRINT_DEVICE2 = "sp_print_device2";//打印设备
    public static final String SP_PRINT_DEVICE3 = "sp_print_device3";//打印设备
    public static final String SP_OPEN_SCAN_ORDER = "sp_open_scan_order";//是否开通扫码点单
    public static final String SP_PRINT_PAY_ORDER = "sp_print_pay_order";//是否打印结账单
    public static final String SP_PRINT_CLIENT_ORDER = "sp_print_client_order";//是否打印客户单
    public static final String SP_PRINT_KITCHEN_ORDER = "sp_print_kitchen_order";//是否打印厨房单
    public static final String SP_PRINT_KITCHEN_COUNT = "sp_print_kitchen_count";//厨房单是否合并
    public static final String SP_HUABEI_WHITE = "sp_huabei_white";//花呗白名单
    public static final String SP_SHOW_TIME = "sp_show_time";//2.0弹框时间
    public static final String SP_OPEN_VOICE = "sp_open_voice";//语音播报
    public static final String SP_OPEN_DAOJIA_VOICE = "sp_open_daojia_voice";//是否开启到家订单推送语音播报
    public static final String SP_OPEN_CANCEL_PAY_VOICE = "sp_open_cancel_pay_voice";//是否开启防逃单语音播报
    public static final String SP_OPEN_NOT_API_VOICE = "sp_open_not_api_voice";//非API交易语音播报开关
    public static final String SP_OPEN_API_VOICE = "sp_open_api_voice";//API交易语音播报开关
    public static final String SP_AUTO_PRINT = "sp_auto_print";//自动打印
    public static final String SP_LOCATION_LATITUDE = "sp_location_latitude";//纬度
    public static final String SP_LOCATION_LONGITUDE = "sp_location_longitude";//经度
    public static final String SP_RISK_WHITE = "sp_risk_white";//风控白名单
    public static final String SP_CASHPOINT_WHITE = "sp_cashpoint_white";//收银点白名单
    public static final String SP_THEME_COLOR = "sp_theme_color";//获取主题色
    public static final String SP_PROTOCOL_SHOWN = "sp_protocol_shown";//协议是否已展示
    public static final String SP_ALLOW_ROOT_CONTINUE = "sp_allow_root_continue";//允许继续访问
    public static final String SP_CONFIG = "sp_config";//功能配置
    public static final String SP_SIGN_DATE = "sp_sign_date";//wizarpos签到时间
    public static final String SP_FUKA_SIGN_DATE = "sp_fuka_sign_date";//wizarpos福卡签到时间
    public static final String SP_YINSHANG_SIGN_DATE = "sp_yinshang_sign_date";//银商签到时间
    public static final String SP_DAOJIA_ENABLE = "sp_daojia_enable";//渠道是否开通到家
    public static final String SP_FUNCARD_ENABLE = "sp_funcard_enable";//渠道是否开通饭卡
    public static final String SP_FUNCARD_VERIFICATION_ENABLE = "sp_funcard_verification_enable";//渠道是否开通饭卡核销
    public static final String SP_DAOJIA_BUSINESS_ENABLE = "sp_daojia_business_enable";//商户是否开通威到家付费详情
    public static final String SP_DEVICE_NO = "sp_device_no";//终端号
    public static final String SP_BCARD_OPEN = "sp_bcard_open";//刷卡刷脸开通
    public static final String SP_WXREG_STATUS = "sp_wxreg_status";//微信宝实名认证状态
    public static final String SP_ALIREG_STATUS = "sp_alireg_status";//支付宝实名认证状态
    public static final String SP_SIGN_APIPROVIDERS = "sp_sign_apiproviders";//签约了的支付方式
    public static final String SP_ALL_APIPROVIDERS = "sp_all_apiproviders";//签约了的支付方式
    public static final String SP_CASH_POINT = "SP_CASH_POINT";//收银点APPID
    public static final String SP_ALIPAY_ISTALLMENT = "SP_ALIPAY_ISTALLMENT";//花呗分期免息APPID
    public static final String SP_ALIPAY_ISTALLMENT_OPEN = "SP_ALIPAY_ISTALLMENT_OPEN";//花呗分期免息开通状态
    public static final String SP_CASH_POINT_OPEN = "SP_CASH_POINT_OPEN";//收银点开通状态
    public static final String SP_CACHE_INFO = "sp_cache_info";//配置信息

    public static final String CONFIG_REGISTER = "REGISTER";//注册功能
    public static final String CONFIG_FORGET = "FORGET";//忘记密码
    public static final String CONFIG_PAY = "PAY";//收款
    public static final String CONFIG_BILL = "BILL";//账单
    public static final String CONFIG_COUNT = "COUNT";//统计
    public static final String CONFIG_SCAN = "SCAN";//扫一扫
    public static final String CONFIG_NOTICE = "NOTICE";//通知
    public static final String CONFIG_HBFQ = "HBFQ";//花呗分期
    public static final String CONFIG_CASHER_MANAGE = "CASHER-MANAGE";//收银员管理
    public static final String CONFIG_STORE_QRCODE = "STORE-QRCODE";//门店二维码
    public static final String CONFIG_SCAN_ORDER_FOOD = "SCAN-ORDER-FOOD";//扫码点单
    public static final String CONFIG_DEPOSIT = "DEPOSIT";//押金收款
    public static final String CONFIG_REPORT = "REPORT";//报表
    public static final String CONFIG_MERCHANT_INFO = "MERCHANT-INFO";//商户信息
    public static final String CONFIG_SIGN_INFO = "SIGN-INFO";//签约信息
    public static final String CONFIG_STORE_MANAGE = "STORE-MANAGE";//门店管理
    public static final String CONFIG_SHOP_MANAGE = "SHOP-MANAGE";//店长管理
    public static final String CONFIG_BANK_CARD = "BANK-CARD";//我的银行卡
    public static final String CONFIG_HELP_CENTER = "HELP-CENTER";//帮助中心
    public static final String CONFIG_SETTING = "SETTING";//设置
    public static final String CONFIG_DEVICE = "DEVICE";//收款设备
    public static final String CONFIG_SOUND = "SOUND";//收款音箱
    public static final String CONFIG_DECCA = "DECCA";//动态台卡
    public static final String CONFIG_CASH_POINT = "CASH-POINT";//收银点
    public static final String CONFIG_GET_HOME = "GET_HOME";//威到家
    public static final String CONFIG_INVOICE_SERVICE = "INVOICE_SERVICE";//发票服务
    public static final String CONFIG_PC_CODE_MANAGER = "PC_CODE_MANAGER";//pc收银员激活码管理
    public static final String CONFIG_DEVICE_MANAGE = "DEVICE-MANAGE";//设备管理
    public static final String CONFIG_STAFF_MANAGE = "STAFF-MANAGE";//员工管理
    public static final String CONFIG_BUSINESS_SAID = "BUSINESS-SAID";//商家说


    public static final String CONFIG_ALL = Constants.CONFIG_REGISTER + "," + Constants.CONFIG_FORGET + "," + Constants.CONFIG_PAY
            + "," + Constants.CONFIG_BILL + ","+ Constants.CONFIG_COUNT + "," + Constants.CONFIG_SCAN
            + "," + Constants.CONFIG_NOTICE + "," + Constants.CONFIG_HBFQ + ","+ Constants.CONFIG_CASHER_MANAGE
            + "," + Constants.CONFIG_STORE_QRCODE + "," + Constants.CONFIG_SCAN_ORDER_FOOD + ","+ Constants.CONFIG_DEPOSIT
            + "," + Constants.CONFIG_REPORT + "," + Constants.CONFIG_MERCHANT_INFO + ","+ Constants.CONFIG_SIGN_INFO
            + "," + Constants.CONFIG_STORE_MANAGE + "," + Constants.CONFIG_SHOP_MANAGE + ","+ Constants.CONFIG_BANK_CARD
            + "," + Constants.CONFIG_HELP_CENTER + "," + Constants.CONFIG_SETTING + ","+ Constants.CONFIG_DEVICE
            + "," + Constants.CONFIG_SOUND + "," + Constants.CONFIG_DECCA + "," + Constants.CONFIG_CASH_POINT
            + "," + Constants.CONFIG_GET_HOME + "," + Constants.CONFIG_INVOICE_SERVICE + "," + Constants.CONFIG_PC_CODE_MANAGER
            + "," + Constants.CONFIG_DEVICE_MANAGE+ "," + Constants.CONFIG_STAFF_MANAGE+ "," + Constants.CONFIG_BUSINESS_SAID;

    /**
     * errorCode
     */
    public static final String ERROR_CODE_UPDATE_QRCODE = "app.commons.qrcode.bind.upgrade.tip.exception";//弹框错误码
    public static final String ERROR_CASH_POINT_DEVICE_UNBIND = "base.merchant.device.binded.null";//此设备还未绑定，请先绑定设备
    public static final String ERROR_CASH_POINT_DEVICE_LINKED = "base.cash.point.binded.other.exception";//此硬件已关联同门的其他收银点，是否更改关联当前收银点?
    public static final String ERROR_CODE_CONFIG = "com.base.material.not.null";//配置信息错误/未配置

    /**
     * requestCode
     */
    public static final int REQUESTCODE_SETMONEY = 1;
    public static final String REQUESTCODE_SETMONEY_REMARK = "money_remark";
    public static final String REQUESTCODE_SETMONEY_MONEY = "money_set";
    public static final int REQUEST_GET_SHOP = 2;
    public static final String REQUEST_GET_SHOP_NAME = "shop_name";
    public static final String INTENT_CASHIER_SHOP = "intent_cashier_shop";
    public static final String REGISTER_INTENT = "to_register_intent";
    public static final String INTENT_OUT_URL = "intent_out_url";
    public static final String REGISTER_INTENT_URL = "register_intent_url";
    public static final String LOAN_TO_REGISTER = "loan_to_register";
    public static final String WELCOME_TO_REGISTER = "welcome_to_register";
    public static final String WELCOME_TO_REGISTER_SECOND = "welcome_to_register_second";
    public static final int REQUEST_SHOP_BEAN_CODE = 3;
    public static final String RESULT_SHOP_BEAN_INTENT = "shop_bean_intent";
    public static final int REQUEST_CASHIER_CODE = 4;
    public static final int REQUEST_UPLOAD_IMAGE = 5;
    public static final String RESULT_UPLOAD_IMAGE_INTENT = "result_upload_image_intent";
    public static final String REQUEST_CASHIER_INTENT = "request_cashier_intent";//请求tag
    public static final String RESULT_CASHIER_INTENT = "result_cashier_intent";//返回tag
    public static final String RESULT_ORDER_NO = "result_order_no";//返回tag
    public static final int REQUEST_MANAGER_INFO = 6;
    public static String INTENT_NAME_MANAGER_INFO = "intent_name_manager_info";
    public static final int REQUEST_ADD_CASHIER = 7;
    public static final String RESULT_ADD_CASHIER = "result_add_cashier";
    public static final int REQUEST_BILL_DETAIL = 8;
    public static final String RESULT_DETAIL_INTENT = "result_detail_intent";//返回tag
    public static final int REQUEST_ADD_MANAGER = 9;
    public static final String RESULT_ADD_MANAGER = "result_add_manager";
    public static final String REQUEST_ADD_INTENT = "request_add_intent";
    public static final int REQUEST_EDIT_MANAGER = 10;
    public static final String REQUEST_MANAGER_INTENT = "request_manager_intent";
    public static final String RESULT_EDIT_MANAGER = "result_edit_manager";
    public static final String REQUEST_MANAGER_BIND_STORE = "request_manager_bind_store";
    public static final int REQUEST_ADD_STORE = 11;
    public static final int REQUEST_DETAIL_STORE = 22;
    public static final String RESULT_ADD_STORE = "result_add_store";
    public static final int REQUEST_EDIT_STORE = 12;
    public static final String RESULT_EDIT_STORE = "result_edit_store";
    public static final String REQUEST_STORE_INTENT = "request_store_intent";
    public static final int REQUEST_BANK_LIST = 13;
    public static final String RESULT_BANK_LIST = "result_bank_list";
    public static final String INTENT_BANK_LIST = "intent_bank_list";
    public static final int REQUEST_BANK_BRANCH_LIST = 14;
    public static final String RESULT_BANK_BRANCH_LIST = "result_bank_branch_list";
    public static final String INTENT_BANK_BRANCH_ID = "intent_bank_branch_id";
    public static final String INTENT_BANK_ID = "intent_bank_id";
    public static final String INTENT_BANK_PROVINCE = "intent_bank_province";
    public static final String INTENT_BANK_CITY = "intent_bank_city";
    public static final String INTENT_NAME_DEVICE_CAPTURE = "intent_name_device_capture";//收款设备-扫一扫
    public static final String INTENT_NAME_DYNAMIC_CAPTURE = "intent_name_dynamic_capture";//绑定动态台卡-扫一扫
    public static final String INTENT_NAME_FACE_CAPTURE = "intent_name_face_capture";//绑定刷脸-扫一扫
    public static final String INTENT_NAME_CLOUD_PRINT = "intent_name_cloud_print";//绑定云打印设备
    public static final String INTENT_NAME_QRCODE_SET = "intent_name_qrcode_set";//选择设置备注门店
    public static final int REQUEST_DEVICE_CAPTURE = 15;
    public static final int REQUEST_MERCHANT_CARD = 16;
    public static final String RESULT_MERCHANT_CARD_INTENT = "result_merchant_card_intent";
    public static final int REQUEST_CHOICE_DATE = 16;
    public static final String RESULT_CHOICE_DATE_INTENT = "result_choice_date_intent";
    public static final int REQUEST_ACTIVE_DETAIL = 17;
    public static final String RESULT_ACTIVE_DETAIL = "result_active_detail";
    public static final String INTENT_ACTIVE_DETAIL = "intent_active_detail";
    public static final int REQUEST_CASHIER_SHOP_CODE = 18;
    public static final int REQUEST_CASHIER_SHOP_EDIT_CODE = 19;
    public static final int REQUEST_PAY_REMARK = 20;
    public static final String RESULT_PAY_REMARK = "result_pay_remark";
    public static final String INTENT_PAY_REMARK = "intent_pay_remark";
    public static final String INTENT_HBFQ = "intent_hbfq";//花呗分期期数
    public static final String INTENT_IS_RATE_FREE = "intent_is_rate_free";//花呗分期是否免息
    public static final String INTENT_UNION_USER_FEE = "intent_union_user_fee";//花呗分期联合付息用户付息金额
    public static final String INTENT_COUPON_PROVIDER_TYPE = "intent_coupon_provider_type";//优惠券提供方类型
    public static final String INTENT_COUPON_TYPE = "intent_coupon_type";//优惠券类型
    public static final String INTENT_NAME_POS_CAPTURE = "intent_name_pos_capture";//POS机扫一扫
    public static final String INTENT_NAME_POS_QRCODE = "intent_name_pos_qrcode";//POS机收款码
    public static final String INTENT_NAME_STATIC_CODE = "intent_name_static_code";//门店收款码
    public static final String INTENT_RECEIPT_DETAIL = "intent_receipt_detail";//收款单详情
    public static final String INTENT_REPORT_DAY_DATA = "intent_report_day_data";//报表日数据
    public static final String INTENT_REPORT_WEEK_DATA = "intent_report_week_data";//报表周数据
    public static final String INTENT_REPORT_MONTH_DATA = "intent_report_month_data";//报表周数据
    public static final String INTENT_SELECT_DATE_TYPE = "intent_select_date_type";//选择时间类型
    public static final String INTENT_OUT_TRADE_NO = "intent_out_trade_no";//订单号
    public static final String INTENT_ = "intent_out_trade_no";//订单号

    public static final int REQUEST_BCARD_PAY = 21;
    public static final int REQUEST_BCARD_PRINT = 22;
    public static final int REQUEST_BCARD_LOGIN = 23;
    public static final int REQUEST_BCARD_REPRINT = 24;
    public static final int REQUEST_BCARD_REFUND = 25;
    public static final int REQUEST_POS_RECEIVE = 26;
    public static final int REQUEST_PLEDGE_UNFREEZE = 27;
    public static final int REQUEST_CLOUD_DEVICE_DETAIL = 28;
    public static final String RESULT_CLOUD_DEVICE_DETAIL = "result_cloud_device_detail";
    public static final String RESULT_CLOUD_PRINT_DEVICE_DETAIL = "result_cloud_print_device_detail";
    public static final String INTENT_CLOUD_DEVICE_DETAIL = "intent_cloud_device_detail";
    public static final int REQUEST_SYSTEM_OVERLAY_WINDOW = 29;
    public static final int REQUEST_INSTALL_PACKAGES = 30;
    public static final int REQUEST_SCAN_ORDER_STORE_LIST = 31;
    public static final String INTENT_SCAN_ORDER_STORE_LIST = "intent_scan_order_store_list";
    public static final String INTENT_VOICE_SET_STORE = "intent_voice_set_store";
    public static final int REQUEST_DEVICE_EMPLOYEE_LIST = 32;
    public static final String RESULT_DEVICE_EMPLOYEE_LIST = "result_device_employee_list";
    public static final String INTENT_DEVICE_EMPLOYEE_LIST = "intent_device_employee_list";
    public static final int REQUEST_VOICE_STORE = 33;
    public static final String RESULT_VOICE_STORE = "result_voice_store";
    public static final String INTENT_VOICE_SELECT_STORE_COUNT = "intent_voice_select_store_count";//语音播报门店选择的数量intent
    public static final int SELECT_STORE_COUNT_DEFAULT = -1;//门店列表获取失败,不需要改变语音播报门店选择个数的标识
    public static final String INTENT_VOICE_STORE = "intent_voice_store";
    public static final String INTENT_TYPE_SELECT_STORE_VOICE = "intent_type_select_store_voice";//选择门店语音播报设置的类型intent
    public static final int INTENT_VALUE_VOICE_COLLECTION = 1;//收款语音播报设置
    public static final int INTENT_VALUE_VOICE_ORDER_DAOJIA = 2;//到家语音播报设置
    public static final int REQUEST_CHANGE_BANK = 34;
    public static final String RESULT_CHANGE_BANK = "result_change_bank";
    public static final String INTENT_CHANGE_BANK = "intent_change_bank";
    public static final int REQUEST_POS_RECEIVE_SCAN = 35;//反扫
    public static final int REQUEST_POS_SIGN = 36;//pos签到
    public static final int REQUEST_POS_MERCHANT_INFO = 37;//pos获取第三方商户信息
    public static final int REQUEST_BCARD_REVERSE = 38;
    public static final int REQUEST_BCARD_CHECK = 39;
    public static final int REQUEST_CLOUD_DEVICE_PRINT_DETAIL = 40;
    public static final int REQUEST_DAOJIA_STORE_LIST = 41;
    public static final String INTENT_DAOJIA_STORE_LIST = "intent_daojia_store_list";
    public static final int REQUEST_H5_SCAN = 42;
    public static final String RESULT_H5_SCAN = "result_h5_scan";
    public static final String INTENT_H5_SCAN = "intent_h5_scan";
    public static final int REQUEST_SELECT_DAY = 43;
    public static final int REQUEST_SELECT_WEEK = 44;
    public static final int REQUEST_SELECT_MONTH = 45;
    public static final String RESULT_SELECT_DATE = "result_select_date";
    public static final int REQUEST_YINSHANG_POS_RECEIVE = 46;
    public static final int REQUEST_VANKE_COUPON = 47;
    public static final String RESULT_VANKE_COUPON = "result_vanke_coupon";
    public static final int REQUEST_CHANGE_NAME = 48;
    public static final String RESULT_CHANGE_NAME = "result_change_name";
    public static final String INTENT_CHANGE_NAME = "intent_change_name";
    public static final int REQUEST_MANAGER_DELETE = 49;
    public static final String RESULT_MANAGER_DELETE = "result_manager_delete";
    public static final int REQUEST_CAPTURE_LINK_SITE = 48;
    public static final int REQUEST_QRCODE_SET = 49;
    public static final int DIGIT_RMB_PAY_TYPE_API_CODE = 17;//数字人民币支付方式

    public static final String INTENT_PARENT_POSITION = "intent_parent_position";
    public static final String INTENT_CHILD_POSITION = "intent_child_position";
    public static final String INTENT_VIP_COUNT_VERIFICATION = "intent_vip_count_verification";
    public static final String INTENT_VIP_TRADE_TYPE = "intent_vip_trade_type";
    public static final String INTENT_VIP_CARD_INFO = "intent_vip_card_info";
    public static final String INTENT_VIP_CARD_ID = "intent_vip_card_id";
    public static final String INTENT_VIP_NUMBER = "intent_vip_number";
    public static final String INTENT_CAPTURE_PAY_DETAIL = "intent_capture_pay_detail";//账单详情
    public static final String INTENT_NAME_CLOUD_CAPTURE = "intent_name_cloud_capture";//云播报-扫一扫
    public static final String INTENT_NAME_SCAN_LOGIN = "intent_name_scan_login";//登录-扫一扫
    public static final String INTENT_NAME_SCAN_VERIFY = "intent_name_scan_verify";//登录-扫一扫
    public static final String INTENT_NAME_SCAN_REFUND = "intent_name_scan_refund";//登录-扫一扫
    public static final String INTENT_NAME_CLOUD_VOICE_SET = "intent_name_cloud_voice_set";//云播报设置
    public static final String INTENT_NAME_CLOUD_PRINT_SET = "intent_name_cloud_print_set";//云打印设置


    public static final String INTENT_VIP_STORE_LIST = "intent_vip_store_list";
    public static final String INTENT_VIP_STORE_ID = "intent_vip_store_id";
    public static final String INTENT_EXAMINE_STATUS = "intent_examine_status";//审核状态

    public static final String INTENT_REPORT_CHOICE_DATE = "intent_report_choice_date";
    public static final String INTENT_INCOME_CHOICE_DATE = "intent_income_choice_date";
    public static final String INTENT_VIP_ACTIVE_LIST = "intent_vip_active_list";
    public static final String INTENT_ADD_VIP_ACTIVE = "intent_add_vip_active";
    public static final String INTENT_EDIT_VIP_ACTIVE = "intent_edit_vip_active";
    public static final String INTENT_VIP_PAY_RESULT = "intent_vip_pay_result";
    public static final String INTENT_VIP_COUNT_CODE = "intent_vip_count_code";
    public static final String INTENT_LOAN = "intent_loan";
    public static final String INTENT_BILL_DATA = "intent_bill_data";
    public static final String INTENT_OTHER_TYPE = "intent_other_type";
    public static final String INTENT_TRADE_DETAIL = "intent_trade_detail";
    public static final String INTENT_RECEIVE_DEVICE_INFO = "intent_receive_device_info";
    public static final String INTENT_DYNAMIC_CODE_INFO = "intent_dynamic_code_info";
    public static final String INTENT_DAOJIA_EDITION = "intent_daojia_edition";
    public static final String INTENT_OEDER_INFO_RECEIVE = "intent_oeder_info_receive";//银商付款详情跳转首页
    public static final String INTENT_COUPON_VERIFY_SUCCESS = "intent_coupon_verify_success";//优惠券核销成功
    public static final String INTENT_COUPON_CANCEL_SUCCESS = "intent_coupon_cancel_success";//优惠券核销撤销成功
    public static final String INTENT_COUPON_CANCEL_FAILED = "intent_coupon_cancel_failed";//优惠券核销撤销失败
    public static final String INTENT_NOT_NEED_REVERSE_COUPON = "intent_not_need_reverse_coupon";//优惠券继续撤销
    public static final String INTENT_USER_ID = "intent_user_id";
    public static final String INTENT_USER_NAME = "intent_user_name";
    public static final String INTENT_USER_STORE = "intent_user_store";
    public static final String INTENT_FINISH = "intent_finish";
    public static final String INTENT_DELETE = "intent_delete";
    public static final String INTENT_SITE_TYPE = "intent_site_type";
    public static final String INTENT_MERCHANT_ID = "intent_merchant_id";
    public static final String INTENT_MERCHANT_NAME = "intent_merchant_name";
    public static final String INTENT_VIP_SUCCESS = "intent_vip_success";
    public static final String INTENT_MERCHANT_TEL = "intent_merchant_tel";
    public static final String INTENT_MODIFY_RECORD_ID = "intent_modify_record_id";
    public static String INTENT_MERCHANT_PAY_TYPE = "intent_merchant_pay_type";

    public static final String TAG_LOGIN = "tag_login";//登录标签tag
    public static final String MSG_NET_ERROR = "net_error";//网络请求错误
    public static final String MSG_DATA_ERROR = "data_error";//数据解析错误
    public static final String TAG_CHECK_PHONE = "tag_check_phone";//手机号码tag
    public static final String SEND_PHONE_FALSE = "send_phone_false";//手机号码错误
    public static final String SEND_PHONE_TRUE = "send_phone_true";//短信验证码成功
    public static final String TAG_CHECK_CODE = "tag_check_code";//短信验证码tag
    public static final String CHECK_CODE_TRUE = "check_code_true";//短信验证码正确
    public static final String CHECK_CODE_FALSE = "check_code_false";//短信验证码错误
    public static final String TAG_RESET_PWD = "tag_reset_pwd";//重置密码成功
    public static final String RESET_PWD_TRUE = "reset_pwd_true";//重置密码成功
    public static final String RESET_PWD_FALSE = "reset_pwd_false";//重置密码失败
    public static final String IS_SUCCESS_DATA = "is_success_data";//是否进件成功
    public static final String IS_STORE_NULL = "is_store_null";//是否门店为空
    public static final String TAG_AUTO_PWD = "tag_auto_pwd";//免密登录tag
    public static final String AUTO_PWD_FALSE = "auto_pwd_false";//免密登录失败
    public static final String AUTO_PWD_TRUE = "auto_pwd_true";//免密登录成功
    public static final String TAG_GET_MESSAGE = "tag_get_message";//获取消息tag
    public static final String TAG_GET_HOME_MESSAGE = "tag_get_home_message";//获取消息tag
    public static final String TAG_GET_HOME_MCHTALK = "tag_get_home_mchtalk";//获取商家说tag
    public static final String TAG_GET_MCH_MCHTALK = "tag_get_mch_mchtalk";//获取商家说tag

    public static final String SKEY = "SKEY";
    public static final String SAUTHID = "SAUTHID";
    public static final String SIGN_KEY = "sign_key";
    public static final String LOGIN_COOKIE = "login_cookie";
    public static final String MERCHANT_NAME = "merchant_name";//商户名称
    public static final String IS_MERCHANT_FLAG = "is_merchant_flag";//是否是商户isMerchantFlag
    public static final String USER_ID = "user_id";//登录获取的userId
    public static final String SP_STAY_USER_ID = "sp_stay_user_id";//登录获取的userId
    public static final String SP_ATTESTATION_STATUS = "sp_attestation_status";//商户认证状态

    public static final String TAG_GETIDENTIFYINGCODE = "tag_getidentifyingcode";//获取图形验证码tag
    public static final String TAG_FIND_GETIDENTIFYINGCODE = "tag_find_getidentifyingcode";//获取图形验证码tag
    public static final String GETIDENTIFYINGCODE_TRUE = "getidentifyingcode_true";//获取图形验证码成功
    public static final String GETIDENTIFYINGCODE_FALSE = "getidentifyingcode_false";//获取图形验证码失败

    public static final String TAG_HOME_TODAY_DATA = "tag_home_today_data";//首页今日数据tag

    public static final String TAG_HOME_ADVERTISING = "tag_home_advertising";//首页轮播图tag
    public static final String HOME_ADVERTISING_TRUE = "home_advertising_true";//首页轮播图成功
    public static final String HOME_ADVERTISING_FALSE = "home_advertising_false";//首页轮播图失败

    public static final String TAG_HOME_NOTICE = "tag_home_notice";//首页消息tag
    public static final String HOME_NOTICE_TRUE = "home_notice_true";//首页消息成功
    public static final String HOME_NOTICE_FALSE = "home_notice_false";//首页消息失败

    public static final String TAG_HOME_READER_NOTICCE = "tag_home_reader_noticce";//首页消息已读tag
    public static final String HOME_READER_NOTICCE_TRUE = "home_reader_notice_true";//首页消息已读成功
    public static final String HOME_READER_NOTICCE_FALSE = "home_reader_notice_false";//首页消息已读失败

    public static final String TAG_CASHIER_MANAGEMENT = "tag_cashier_management";//收银员管理tag
    public static final String CASHIER_MANAGEMENT_TRUE = "cashier_management_true";//收银员管理成功
    public static final String CASHIER_MANAGEMENT_FALSE = "cashier_management_false";//收银员管理失败

    public static final String TAG_ADD_CASHIER_MANAGE = "tag_add_cashier_manage";//添加收银员tag
    public static final String ADD_CASHIER_MANAGE_TRUE = "add_cashier_manage_true";//添加收银员成功
    public static final String ADD_CASHIER_MANAGE_FALSE = "add_cashier_manage_false";//添加收银员失败

    public static final String TAG_CASHIER_FREEZE = "tag_cashier_freeze";//收银员冻结tag
    public static final String CASHIER_FREEZE_TRUE = "cashier_freeze_true";//收银员冻结成功
    public static final String CASHIER_FREEZE_FALSE = "cashier_freeze_false";//收银员冻结失败

    public static final String TAG_GET_MERCHANT_INFO = "tag_get_merchant_info";//商户信息tag
    public static final String GET_MERCHANT_INFO_TRUE = "get_merchant_info_true";//商户信息获取成功
    public static final String GET_MERCHANT_INFO_FALSE = "get_merchant_info_false";//商户信息获取失败

    public static final String TAG_GET_SETTLE_CHANGE_COUNT = "tag_get_settle_change_count";//查询商户剩余修改结算信息次数tag
    public static final String GET_SETTLE_CHANGE_COUNT_TRUE = "get_settle_change_count_true";//查询商户剩余修改结算信息次数成功
    public static final String GET_SETTLE_CHANGE_COUNT_FALSE = "get_settle_change_count_false";//查询商户剩余修改结算信息次数失败


    public static final String TAG_GET_INDUSTRY_TYPE = "tag_get_industry_type";//行业类别tag
    public static final String GET_INDUSTRY_TYPE_TRUE = "get_industry_type_true";//行业类别获取成功
    public static final String GET_INDUSTRY_TYPE_FALSE = "get_industry_type_false";//行业类别获取失败

    public static final String TAG_GET_ADDRESS_PUBLIC = "tag_get_address_public";//地址获取tag
    public static final String TAG_GET_MERCHANT_ADDRESS = "tag_get_merchant_address";//地址获取tag
    public static final String TAG_GET_ADDRESS = "tag_get_address";//地址获取tag
    public static final String GET_ADDRESS_TRUE = "get_address_true";//地址获取成功
    public static final String GET_ADDRESS_FALSE = "get_address_false";//地址获取失败

    public static final String TAG_SUBMIT_MERCHANT_INFO = "tag_submit_merchant_info";//提交信息tag
    public static final String SUBMIT_MERCHANT_INFO_TRUE = "submit_merchant_info_true";//提交信息成功
    public static final String SUBMIT_MERCHANT_INFO_FALSE = "submit_merchant_info_false";//提交信息失败

    public static final String TAG_QUERY_ENABLED_CHANGE = "tag_query_Enabled_Change";//验证商户资料是否允许修改tag
    public static final String QUERY_ENABLED_CHANGE_TRUE = "query_Enabled_Change_true";//验证商户资料是否允许修改成功
    public static final String QUERY_ENABLED_CHANGE_FALSE = "query_Enabled_Change_false";//验证商户资料是否允许修改失败


    public static final String IS_ADMIN_FLAG = "is_admin_flag";//是否是管理员
    public static final String IS_CASHER_FLAG = "is_casher_flag";//是否是收银员
    public static final String IS_MANAGER_FLAG = "is_manager_flag";//是否是店长
    public static final String IS_SHOW_WSY = "IS_SHOW_WSY";//是否开通pc收银码
    public static final String IS_NEW_PLATFORM_REGISTER = "is_new_platform_register";//新老商户注册标识 true:2.0商户
    public static final String IS_PLATFORM_WHITE_LIST = "is_platform_white_list";//商户白名单
    public static final String IS_CHANGE_TO_NEW_PLATFORM = "is_change_to_new_platform";//登录所在平台标识 true:切换到2.0
    public static final String IS_BELONG_HSHF = "is_belong_hshf";//商户主体是否属于汇商合肥
    public static final String IS_CARD_CONFIG_FINISH = "is_card_config_finish";//卡配置完成
    public static final String REALNAME = "realname";//真实姓名

    public static final String PAY_URL = "http://lujiafa2.dev.swiftpass.cn/app/unifiedMicroPay";//支付接口unifiedMicroPay
    public static final String STORE_PORT_TAG = "store_port_tag";//门店Tag
    public static final String STORE_VIP_PORT_TAG = "store_vip_port_tag";//门店Tag
    public static final String PAY_TAG = "pay_tag";//支付Tag
    public static final String PERMISSIONS_REFUND = "permissions_refund";//permissions退款权限
    public static final String FUNC_CODE_REFUND = "func_code_refund";//funcCode退款权限
    public static final String PAY_TRUE = "pay_true";//支付成功
    public static final String PAY_FALSE = "pay_false";//支付失败
    public static final String NOTIFY_PAY_EASYPAY = "notify_pay_easypay";//易生刷卡支付通知
    public static final String NOTIFY_PAY_EASYPAY_CHECK = "notify_pay_easypay_check";//易生刷卡支付通知
    public static final String NOTIFY_REVERSE_EASYPAY = "notify_reverse_easypay";//易生刷卡撤销通知
    public static final String NOTIFY_REFUND_EASYPAY = "notify_refund_easypay";//易生刷卡退款通知

    public static final String NOTIFY_FACE_PAY = "notify_face_pay";//银商刷脸支付通知


    public static final String LOGIN_CHANGE_PWD_TAG = "login_change_pwd_tag";//登录后修改密码tag
    public static final String LOGIN_CHANGE_PWD_TRUE = "login_change_pwd_true";//登录后修改密码成功
    public static final String LOGIN_CHANGE_PWD_FALSE = "login_change_pwd_false";//登录后修改密码失败

    public static final String GET_GOODS_NAME_TAG = "goods_get_name_tag";//查询商品名称接口tag
    public static final String GET_GOODS_NAME_TRUE = "get_goods_name_true";//查询商品名称接口返回成功
    public static final String GET_GOODS_NAME_FALSE = "get_goods_name_false";//查询商品名称接口返回失败

    public static final String GOODS_NAME_TAG = "goods_name_tag";//商品名称接口tag
    public static final String GOODS_NAME_TRUE = "goods_name_true";//商品名称接口返回成功
    public static final String GOODS_NAME_FALSE = "goods_name_false";//商品名称接口返回失败

    public static final String CHECK_ORDER_STATE_TAG = "check_order_state_tag";//查询订单状态接口tag
    public static final String VIP_CHECK_ORDER_STATE_TAG = "vip_check_order_state_tag";//查询订单状态接口tag
    public static final String CHECK_ORDER_STATE_TRUE = "check_order_state_true";//查询订单状态接口返回成功
    public static final String CHECK_ORDER_STATE_FALSE = "check_order_state_false";//查询订单状态接口返回失败

    public static final String ORDER_REVERSE_TAG = "order_reverse_tag";//查询订单冲正tag
    public static final String VIP_ORDER_REVERSE_TAG = "vip_order_reverse_tag";//查询订单冲正tag
    public static final String ORDER_REVERSE_TRUE = "order_reverse_true";//查询订单冲正接口返回成功
    public static final String ORDER_REVERSE_FALSE = "order_reverse_false";//查询订单冲正接口返回失败

    public static final String TAG_CHOICE_STORE = "tag_choice_store";//门店列表tag
    public static final String TAG_CLOUD_CHOICE_STORE = "tag_cloud_choice_store";//门店列表tag
    public static final String TAG_STORE_CODE_CHOICE_STORE = "tag_store_code_choice_store";//门店列表tag
    public static final String TAG_RECEIVE_DEVICE_CHOICE_STORE = "tag_receive_device_choice_store";//门店列表tag
    public static final String TAG_CLOUD_PRINT_CHOICE_STORE = "tag_cloud_print_choice_store";//门店列表tag
    public static final String TAG_FACE_DEVICE_CHOICE_STORE = "tag_face_device_choice_store";//门店列表tag
    public static final String TAG_SITE_CHOICE_STORE = "tag_site_choice_store";//门店列表tag
    public static final String TAG_DYNAMIC_CHOICE_STORE = "tag_dynamic_choice_store";//门店列表tag
    public static final String TAG_PLEDGE_BILL_STORE = "tag_pledge_bill_store";
    public static final String TAG_PLEDGE_COLLECT_STORE = "tag_pledge_collect_store";
    public static final String TAG_BILL_STORE = "tag_bill_store";
    public static final String TAG_DEVICE_STORE = "tag_device_store";
    public static final String TAG_DEVICE_TYPE = "tag_device_type";
    public static final String TAG_COLLECT_STORE = "tag_collect_store";
    public static final String TAG_REPORT_STORE = "tag_report_store";
    public static final String TAG_PLEDGE_BILL_CASHIER = "tag_pledge_bill_cashier";
    public static final String TAG_PLEDGE_COLLECT_CASHIER = "tag_pledge_collect_cashier";
    public static final String TAG_BILL_CASHIER = "tag_bill_cashier";
    public static final String TAG_BILL_SITE = "tag_bill_site";
    public static final String TAG_COLLECT_CASHIER = "tag_collect_cashier";
    public static final String TAG_COLLECT_SITE = "tag_collect_site";

    public static final String TAG_GET_CASHIER = "tag_get_cashier";//收银员列表tag

    public static final String TAG_CHANGE_STORE_CODE = "tag_store_code";//门店二维码tag
    public static final String TAG_STORE_CODE = "tag_store_code";//门店二维码tag
    public static final String STORE_CODE_TRUE = "store_code_true";//门店二维码返回成功
    public static final String STORE_CODE_FALSE = "store_code_false";//门店二维码返回失败

    public static final String TAG_SET_READ_STATE = "tag_set_read_state";//设置可读状态tag

    public static final String TAG_GET_BILLS_HOME = "tag_get_bills_home";//账单tag

    public static final String TAG_GET_BILLS = "tag_get_bills";//账单tag
    public static final String GET_BILLS_TRUE = "get_bills_true";//账单查询成功
    public static final String GET_BILLS_FALSE = "get_bills_false";//账单查询失败

    public static final String TAG_GET_BILLS_BY_NUMBER = "tag_get_bills_by_number";//订单号查询账单tag
    public static final String GET_BILLS_BY_NUMBER_TRUE = "GET_BILLS_BY_NUMBER_TRUE";//订单号查询账单成功
    public static final String GET_BILLS_BY_NUMBER_FALSE = "GET_BILLS_BY_NUMBER_FALSE";//订单号查询账单失败

    public static final String TAG_GET_REPORT = "tag_get_report";//报表tag
    public static final String GET_REPORT_TRUE = "get_report_true";//报表成功
    public static final String GET_REPORT_FALSE = "get_report_false";//报表失败

    public static final String TAG_GET_ORDER_DETAIL_HOME = "tag_get_order_detail_home";//订单详情tag

    public static final String TAG_GET_ORDER_DETAIL = "tag_get_order_detail";//订单详情tag
    public static final String TAG_RECEIVE_ORDER_DETAIL = "tag_receive_order_detail";//订单详情tag
    public static final String GET_ORDER_DETAIL_TRUE = "get_order_detail_true";//订单详情查询成功

    public static final String TAG_GET_REFUND = "tag_get_refund";//退款tag
    public static final String TAG_BCARD_REVERSE = "tag_bcard_reverse";//刷卡冲正
    public static final String TAG_GET_REVERSE = "tag_get_reverse";//冲正

    public static final String TAG_SEARCH_ORDER_STATE = "tag_search_ordesr_state";//查询订单状态tag
    public static final String TAG_BILL_FIND_SEARCH_DETAIL = "tag_bill_find_search_detail";//账单搜索
    public static final String TAG_CAPTURE_ORDER_DETAIL = "tag_capture_order_detail";//账单搜索

    public static final String TAG_CASHIER_EDIT = "tag_cashier_edit";//收银员编辑tag
    public static final String CASHIER_EDIT_TRUE = "cashier_edit_true";//收银员编辑成功
    public static final String CASHIER_EDIT_FALSE = "cashier_edit_false";//收银员编辑失败

    public static final String TAG_CASHIER_DETAIL = "tag_cashier_detail";//收银员详情tag
    public static final String CASHIER_DETAIL_TRUE = "cashier_detail_true";//收银员详情成功
    public static final String CASHIER_DETAIL_FALSE = "cashier_detail_false";//收银员详情失败

    public static final String TAG_CASHIER_PERMISSION = "tag_cashier_permission";//收银员详情tag

    public static final String TAG_BIND_CODE = "tag_bind_code";//绑定二维码tag
    public static final String BIND_CODE_TRUE = "bind_code_true";//绑定二维码成功
    public static final String BIND_CODE_FALSE = "bind_code_false";//绑定二维码失败

    public static final String TAG_PUT_STORE = "tag_put_store";//上传默认门店tag
    public static final String PUT_STORE_TRUE = "put_store_true";//上传默认门店成功
    public static final String PUT_STORE_FALSE = "put_store_false";//上传默认门店失败

    public static final String TAG_SCAN_THIRD_ORDER = "tag_scan_third_order";//扫描查单tag
    public static final String SCAN_THIRD_ORDER_TRUE = "scan_third_order_true";//扫描查单成功
    public static final String SCAN_THIRD_ORDER_FALSE = "scan_third_order_false";//扫描查单失败

    public static final String TAG_CHECK_VERIFY_STATUS = "tag_check_verify_status";//查询审核状态tag
    public static final String CHECK_VERIFY_STATUS_TRUE = "check_verify_status_true";//查询审核状态成功
    public static final String CHECK_VERIFY_STATUS_FALSE = "check_verify_status_false";//查询审核状态失败

    public static final String TAG_CHECK_QRCODE = "tag_check_qrcode";//查询二维码tag
    public static final String CHECK_QRCODE_TRUE = "check_qrcode_true";//查询二维码成功
    public static final String CHECK_QRCODE_FALSE = "check_qrcode_false";//查询二维码失败

    public static final String TAG_QUERY_RATE = "tag_query_rate";//查询费率tag
    public static final String TAG_VIP_RECHARGE_RATE = "tag_vip_recharge_rate";//查询充值费率tag
    public static final String QUERY_RATE_TRUE = "query_rate_true";//查询费率成功
    public static final String QUERY_RATE_FALSE = "query_rate_false";//查询费率失败

    public static final String TAG_QUERY_PAYTYPE = "tag_query_paytype";//查询开通的支付方式


    public static final String TAG_GET_VOICE = "tag_get_voice";//获取语音验证码tag
    public static final String GET_VOICE_TRUE = "get_voice_true";//获取语音验证码成功
    public static final String GET_VOICE_FALSE = "get_voice_false";//获取语音验证码失败

    public static final String TAG_QUERY_SIGN = "tag_query_sign";//签约状态查询tag
    public static final String QUERY_SIGN_TRUE = "query_sign_true";//签约状态查询成功
    public static final String QUERY_SIGN_FALSE = "query_sign_false";//签约状态查询失败

    public static final String TAG_PUT_SIGN = "tag_put_sign";//签约状态上传tag

    public static final String TAG_QUEST_LOCATION = "tag_quest_location";//定位查询tag
    public static final String QUEST_LOCATION_TRUE = "quest_location_true";//定位查询成功
    public static final String QUEST_LOCATION_FALSE = "quest_location_false";//定位查询失败

    public static final String TAG_DOWNLOAD_IMAGE = "tag_download_image";//广告下载tag

    public static final String TAG_SAVE_IMAGE = "tag_save_image";//广告下载tag
    public static final String SAVE_IMAGE_TRUE = "save_image_true";//广告下载成功
    public static final String SAVE_IMAGE_FALSE = "save_image_false";//广告下载失败

    public static final String TAG_GET_BINDING_MSG = "tag_get_binding_msg";//获取绑定短信验证tag
    public static final String TAG_GET_BINDING_VOICE = "tag_get_binding_voice";//获取绑定语音验证tag

    public static final String TAG_GET_BINDING = "tag_get_binding";//绑定手机号tag
    public static final String GET_BINDING_FALSE = "get_binding_false";//绑定手机号失败
    public static final String GET_BINDING_TRUE = "get_binding_true";//绑定手机号成功

    public static final String TAG_AUTHENTICATION = "tag_authentication";//身份验证tag
    public static final String AUTHENTICATION_TRUE = "authentication_true";//身份验证成功
    public static final String AUTHENTICATION_FALSE = "authentication_false";//身份验证失败

    public static final String ON_EVENT_TRUE = "on_event_true";//请求成功
    public static final String ON_EVENT_FALSE = "on_event_false";//请求失败

    public static final String TAG_LICENSE_DETAIL = "tag_license_detail";//营业执照详情tag
    public static final String TAG_EDIT_LICENSE = "tag_edit_license";//编辑营业执照tag
    public static final String TAG_LIMIT_DETAIL = "tag_limit_detail";//限额详情tag
    public static final String TAG_SHORT_NAME_DETAIL = "tag_short_name_detail";//商户简称详情tag
    public static final String TAG_CHANGE_SHORT_NAME = "tag_change_short_name";//修改商户简称tag
    public static final String TAG_STORE_DETAIL = "tag_store_detail";//门店详情tag
    public static final String TAG_STORE_EMPLOYEE_DETAIL = "tag_store_employee_detail";//门店详情tag
    public static final String TAG_STORE_FREEZE = "tag_store_freeze";//门店冻结tag
    public static final String TAG_STORE_EDIT = "tag_store_edit";//门店编辑tag
    public static final String TAG_ADD_STORE = "tag_add_store";//添加门店tag
    public static final String TAG_ADD_EMPLOYEE = "tag_add_employee";//添加员工
    public static final String TAG_MANAGER_LIST = "tag_manager_list";//店长列表tag
    public static final String TAG_UNBIND_STORE_LIST = "tag_unbind_store_list";//未绑定门店tag
    public static final String TAG_ADD_MANAGER = "tag_add_manager";//添加店长tag
    public static final String TAG_MANAGER_DETAIL = "tag_manager_detail";//店长详情tag
    public static final String TAG_FREEZE_MANAGER = "tag_freeze_manager";//禁用店长tag
    public static final String TAG_EDIT_MANAGER = "tag_edit_manager";//编辑店长tag
    public static final String TAG_DELETE_MANAGER = "tag_delete_manager";//删除店长tag
    public static final String TAG_DELETE_CASHIER = "tag_delete_cashier";//删除收银员tag
    public static final String TAG_MY_CARD = "tag_my_card";//我的银行卡tag
    public static final String TAG_CARD_DETAIL = "tag_card_detail";//银行卡详情tag
    public static final String TAG_EDIT_CARD = "tag_edit_card";//编辑银行卡tag
    public static final String TAG_EDIT_CARD_VERIFY = "tag_edit_card_verify";//编辑银行卡验证tag
    public static final String TAG_BANK_LIST = "tag_bank_list";//银行列表tag
    public static final String TAG_BANK_BRANCH_LIST = "tag_bank_branch_list";//支行列表tag
    public static final String TAG_INVITATION_ENABLE = "tag_invitation_enable";//邀请码启用状态tag
    public static final String TAG_INVITATION_CODE = "tag_invitation_code";//邀请码注册链接tag
    public static final String TAG_CHANGE_BINGING = "tag_change_binging";//更换手机号tag
    public static final String TAG_COLLECT_INFO = "tag_collect_info";//收集信息tag
    public static final String TAG_MAIN_QUEST_LOCATION = "tag_main_quest_location";//首页定位查询tag
    public static final String TAG_STATUS_BANK_CARD = "tag_status_bank_card";//商户资料审核状态tag
    public static final String TAG_STATUS_UP_LIMIT = "tag_status_up_limit";//商户资料审核状态tag
    public static final String TAG_STATUS_SHORT_NAME = "tag_status_short_name";//商户资料审核状态tag
    public static final String TAG_PRINT_ACTIVE = "tag_print_active";//小票活动tag
    public static final String TAG_LOAN_DETAIL = "tag_loan_detail";//贷款详情tag
    public static final String TAG_LOAN_FIRST = "tag_loan_first";//初次申请贷款tag
    public static final String TAG_LOAN_AGAIN = "tag_loan_again";//再次申请贷款tag
    public static final String TAG_WHITE_LIST = "tag_white_list";//商户白名单tag
    public static final String TAG_WHITE_LIST_AGAIN = "tag_white_list_again";//商户白名单tag
    public static final String TAG_DEVICE_BIND = "tag_device_bind";//绑定设备tag
    public static final String TAG_DEVICE_LIST = "tag_device_list";//绑定设备列表tag
    public static final String TAG_STORE_VOICE = "tag_store_voice";//门店语音tag
    public static final String TAG_OPEN_MEMBER = "tag_open_member";//开通会员tag
    public static final String TAG_COLLECT_MEMBER = "tag_collect_member";//是否显示会员消费统计数据
    public static final String TAG_COLLECT_FUNCARD = "tag_collect_FUNCARD";//是否显示饭卡消费统计数据
    public static final String TAG_MEMBER_URL = "tag_member_url";//领取会员卡链接tag
    public static final String TAG_MEMBER_WECAHT_CODE_URL = "tag_member_wecaht_code_url";//领取小程序会员码链接tag
    public static final String TAG_STORE_MEMBER_URL = "tag_store_member_url";//领取会员卡链接tag
    public static final String TAG_SELECT_DAY_LIST = "tag_select_day_list";//选择日期【日】
    public static final String TAG_SELECT_WEEK_LIST = "tag_select_week_list";//选择日期【月】
    public static final String TAG_SELECT_MONTH_LIST = "tag_select_month_list";//选择日期【周】
    public static final String TAG_TRADE_STATISTICS = "tag_trade_statistics";//数据概览
    public static final String TAG_DAY_TRADE_TREND_HOME = "tag_day_trade_trend_home";//近七日交易趋势
    public static final String TAG_DAY_TRADE_TREND = "tag_day_trade_trend";//近七日交易趋势
    public static final String TAG_HOUR_TRADE_TREND = "tag_hour_trade_trend";//单日交易趋势
    public static final String TAG_PAY_PERCENT = "tag_pay_percent";//支付占比
    public static final String TAG_DEVICE_PERCENT = "tag_device_percent";//终端占比
    public static final String TAG_REPORT_DAY = "tag_report_day";//日报统计
    public static final String TAG_REPORT_DAY_DETAIL = "tag_report_day_detail";//日报详情
    public static final String TAG_REPORT_MONTH = "tag_report_month";//月报统计
    public static final String TAG_REPORT_MONTH_DETAIL = "tag_report_month_detail";//月报详情
    public static final String TAG_VIP_ACTIVE_CREATE = "tag_vip_active_create";//会员活动创建
    public static final String TAG_VIP_ACTIVE_LIST = "tag_vip_active_list";//会员活动列表
    public static final String TAG_VIP_ACTIVE_DETAIL = "tag_vip_active_detail";//会员活动详情
    public static final String TAG_VIP_ACTIVE_STATUS_EDIT = "tag_vip_active_status_edit";//会员活动状态编辑
    public static final String TAG_VIP_ACTIVE_EDIT = "tag_vip_active_edit";//会员活动编辑
    public static final String TAG_VIP_STORE_LIST = "tag_vip_store_list";//门店列表
    public static final String TAG_VIP_CARD_STORE_LIST = "tag_vip_card_store_list";//门店列表
    public static final String TAG_SCAN_ORDER_STORE_LIST = "tag_scan_order_store_list";//门店列表
    public static final String TAG_CREATE_MATERIAL = "tag_create_material";//生成物料
    public static final String TAG_VIP_ACTIVE_CREATE_ENABLE = "tag_vip_active_create_enable";//是否可创建
    public static final String TAG_VIP_CARD_LIST = "tag_vip_card_list";//会员卡列表
    public static final String TAG_VIP_PAY = "tag_vip_pay";//会员卡交易
    public static final String TAG_VIP_COUNT_VERIFICATION = "tag_vip_count_verification";//次卡核销
    public static final String TAG_VIP_PAY_WHITE = "tag_vip_pay_white";//次卡核销
    public static final String TAG_BOUNTY_REFUND_ENABLE = "tag_bounty_refund_enable";//奖励账单是否可退
    public static final String TAG_GET_BOUNTY_LIST = "tag_get_bounty_list";//营销奖励账单列表
    public static final String TAG_GET_BOUNTY_COLLECT = "tag_get_bounty_collect";//营销奖励统计
    public static final String TAG_GET_BOUNTY_MONEY = "tag_get_bounty_money";//营销奖励金额
    public static final String TAG_ORDER_FROM_REFUND = "tag_order_from_refund";//从退款详情页面获取账单详情
    public static final String TAG_GET_LOGIN_MERCHANT = "tag_get_login_merchant";//登录商户号
    public static final String TAG_GET_RESET_MERCHANT = "tag_get_reset_merchant";//商户号忘记密码
    public static final String TAG_POS_ACCOUNT = "tag_pos_account";//获取POS内部账号密码
    public static final String TAG_PAY_POS_ACCOUNT = "tag_pay_pos_account";//获取POS内部账号密码
    public static final String TAG_POS_RECORD = "tag_pos_record";//上传POS绑定记录
    public static final String TAG_POS_BCARD_PAY = "tag_pos_bcard_pay";//刷卡支付
    public static final String TAG_POS_YS_BCARD_PAY = "tag_pos_ys_bcard_pay";//银商刷卡支付
    public static final String TAG_POS_FACE_PAY = "tag_pos_face_pay";//人脸支付
    public static final String TAG_POS_BCARD_REFUND_CANCELED = "tag_pos_bcard_refund_canceled";//刷卡支付退款取消
    public static final String TAG_AUTHENTICATION_REFUND = "tag_authentication_refund";//刷卡支付重新退款
    public static final String TAG_AUTHENTICATION_PLEDGE_UNFREEZE = "tag_authentication_pledge_unfreeze";//解冻验证
    public static final String TAG_PLEDGE_BILLS = "tag_pledge_bills";//预授权账单列表
    public static final String TAG_PLEDGE_BILL = "tag_pledge_bill";//预授权账单详情
    public static final String TAG_PUSH_PLEDGE_BILL = "tag_push_pledge_bill";//推送预授权账单详情
    public static final String TAG_PLEDGE_REPORT = "tag_pledge_report";//预授权报表
    public static final String TAG_PLEDGE_PAY = "tag_pledge_pay";//资金预授权
    public static final String TAG_PLEDGE_PAY_DETAIL = "tag_pledge_pay_detail";//资金预授权订单
    public static final String TAG_PLEDGE_CAPTURE_DETAIL = "tag_pledge_capture_detail";//资金预授权订单
    public static final String TAG_PLEDGE_PAY_SYNCHRONY = "tag_pledge_pay_synchrony";//资金解冻同步
    public static final String TAG_PLEDGE_DETAIL_CHECK = "tag_pledge_detail_check";//资金预授权订单查询
    public static final String TAG_PLEDGE_CAPTURE_REVERSE = "tag_pledge_capture_reverse";//预授权撤销
    public static final String TAG_PLEDGE_UNFREEZE_REVERSE = "tag_pledge_unfreeze_reverse";//预授权撤销
    public static final String TAG_PLEDGE_REVERSE = "tag_pledge_pay_reverse";//预授权撤销
    public static final String TAG_PLEDGE_CHECK_REVERSE = "tag_pledge_check_reverse";//预授权撤销
    public static final String TAG_PLEDGE_TO_PAY = "tag_pledge_to_pay";//预授权转支付
    public static final String TAG_PLEDGE_FIND_BILLS = "tag_pledge_find_bills";//预授权订单列表查找
    public static final String TAG_PLEDGE_FIND_BILL = "tag_pledge_find_bill";//预授权订单查找
    public static final String TAG_PLEDGE_SCAN_BILL = "tag_pledge_scan_bill";//预授权订单扫一扫查找
    public static final String TAG_SCAN_LOGIN_STATUS = "tag_scan_login_status";//扫码登录状态
    public static final String TAG_SCAN_LOGIN_STATUS_CANCEL = "tag_scan_login_status_cancel";//取消扫码登录状态
    public static final String TAG_SCAN_LOGIN = "tag_scan_login";//扫码登录
    public static final String TAG_DAOJIA_STORE_LIST = "tag_daojia_store_list";//门店列表
    public static final String TAG_OPEN_DAOJIA_STORE = "tag_open_daojia_store";//门店列表
    public static final String TAG_QUERY_COUPON = "tag_query_coupon";//券详情查询
    public static final String TAG_QUERY_COUPON_VERIFY = "tag_query_coupon_verify";//券核销
    public static final String TAG_QUERY_COUPON_VERIFY_LIST = "tag_query_coupon_verify_list";//券核销列表

    public static final String TAG_CLOUD_DEVICE_LIST = "tag_cloud_device_list";//云播报设备列表
    public static final String TAG_CLOUD_PRINT_LIST = "tag_cloud_print_list";//云打印设备列表
    public static final String TAG_CLOUD_DEVICE_BIND = "tag_cloud_device_bind";//云播报设备绑定
    public static final String TAG_CLOUD_DEVICE_UNBIND = "tag_cloud_device_unbind";//云播报设备解绑
    public static final String TAG_SIMPLE_POS_DEVICE_UNBIND = "tag_simple_pos_device_unbind";//简易POS设备解绑
    public static final String TAG_FACE_PAY_DEVICE_UNBIND = "tag_face_pay_device_unbind";//刷脸设备解绑
    public static final String TAG_DYNAMIC_SET_DEVICE_UNBIND = "tag_dynamic_set_device_unbind";//电子台卡设备解绑
    public static final String TAG_CLOUD_DEVICE_DETAIL = "tag_cloud_device_detail";//云播报设备详情
    public static final String TAG_DYNAMIC_DEVICE_DETAIL = "tag_dynamic_device_detail";//动态台卡设备详情
    public static final String TAG_CLOUD_DEVICE_CLEARWAITCOUNT = "tag_cloud_device_clear_count";//云打印清空数据
    public static final String TAG_CLOUD_DEVICE_UPDATEPRINTERBYSN = "tag_cloud_device_updateprinterbysn";//云打印机设置
    public static final String TAG_CLOUD_DEVICE_QUERYAVAILABBLEINFO = "tag_cloud_device_queryAvailabbleInfo";//查询需要设置的收款码、收款设备、收银员三个列表
    public static final String TAG_CLOUD_DEVICE_BINDDEVICE = "tag_cloud_device_binddevice";//打印机关联设置
    public static final String TAG_CLOUD_PRINT_MSGPRINT = "tag_cloud_print_msgprint";//云打印开始打印小票
    public static final String TAG_UPDATE_MENU = "tag_update_menu";

    public static final String TAG_CLOUD_DEVICE_SET = "tag_cloud_device_set";//云播报设备设置
    public static final String TAG_CLOUD_DEVICE_PRINT_DETAIL = "tag_cloud_device_print_detail";//云打印设备详情
    public static final String TAG_CLOUD_DEVICE_PRINT_SET = "tag_cloud_device_print_set";//云打印设备设置
    public static final String TAG_CLOUD_DEVICE_CASHIER_SET = "tag_cloud_device_cashier_set";//云播报门店收银员设置
    public static final String TAG_STORE_CODE_LIST = "tag_store_code_list";//门店二维码
    public static final String TAG_OPEN_SCAN_ORDER = "tag_open_scan_order";//是否开通扫码点单
    public static final String TAG_OPEN_FUNCARD = "tag_open_funcard";//是否开通饭卡
    public static final String TAG_OPEN_FUNCARD_VERIFICATION = "tag_open_funcard_verification";//是否开通饭卡核销
    public static final String TAG_SCAN_ORDER_URL = "tag_scan_order_url";//扫码点单地址
    public static final String TAG_PRINT_ORDER_CONTENT = "tag_print_order_content";//打印内容
    public static final String TAG_PUSH_ORDER_LIST = "tag_push_order_list";//扫码点单订单推送列表
    public static final String TAG_PUSH_ORDER_SET = "tag_push_order_set";//扫码点单订单推送设置
    public static final String TAG_VIP_UPDATE_STATUS = "tag_vip_update_status";//商户会员升级状态
    public static final String TAG_APPLY_VIP_UPDATE = "tag_apply_vip_update";//商户会员升级申请
    public static final String TAG_HBFQ_RATE_LIST = "tag_hbfq_rate_list";//花呗分期费率
    public static final String TAG_HBFQ_COLLECT = "tag_hbfq_collect";//花呗分期统计
    public static final String TAG_HBFQ_ENABLE = "tag_hbfq_enable";//花呗分期可用
    public static final String TAG_LINK_EMPLOYEE_LIST = "tag_link_employee_list";//收款设备关联员工列表
    public static final String TAG_LINK_EMPLOYEE = "tag_link_employee";//收款设备关联员工
    public static final String TAG_VOICE_ALL_SWITCH = "tag_voice_all_switch";//语音播报总开关
    public static final String TAG_CASHIER_DETAIL_CHOICE_STORE = "tag_cashier_detail_choice_store";//收银员详情选择门店tag
    public static final String TAG_QUERY_CASHIER_LIMIT = "tag_query_cashier_limit";//查询收银员权限
    public static final String TAG_DYNAMIC_CODE_LIST = "tag_dynamic_code_list";//动态台卡
    public static final String TAG_RISK_WHITE = "tag_risk_white";//风控白名单
    public static final String TAG_CASHPOINT_WHITE = "tag_cashpoint_white";//收银点白名单
    public static final String TAG_CHANGE_BANK_CODE = "tag_change_bank_code";//更改银行卡验证码
    public static final String TAG_CONFIG = "tag_config";//功能配置
    public static final String TAG_RECEIPT_LIST = "tag_receipt_list";//收款单列表
    public static final String TAG_RECEIPT_CREATE = "tag_receipt_create";//收款单创建
    public static final String TAG_RECEIPT_SHARE = "tag_receipt_share";//收款单分享
    public static final String TAG_RECEIPT_CLOSE = "tag_receipt_close";//收款单关闭
    public static final String TAG_RECEIPT_DETAIL = "tag_receipt_detail";//收款单详情
    public static final String TAG_DAOJIA_ENABLE = "tag_daojia_enable";//商户是否开通到家
    public static final String TAG_DAOJIA_URL = "tag_daojia_url";//到家URL地址
    public static final String TAG_OPEN_DAOJIA_URL = "tag_open_daojia_url";//到家URL地址
    public static final String TAG_QUERY_DAOJIA = "tag_query_daojia";//商户开通付费详情
    public static final String TAG_VOICE_SET_QUERY_DAOJIA = "tag_voice_set_query_daojia";//网络请求tag语音播报设置里查询商户开通付费详情
    public static final String TAG_QUERY_DAOJIA_OPEN = "tag_query_daojia_open";//商户开通付费详情
    public static final String TAG_OPEN_DAOJIA = "tag_open_daojia";//商户开通到家
    public static final String TAG_PAY_SITE_LIST = "tag_pay_site_list";//收银点列表
    public static final String TAG_BIND_SITE_LIST = "tag_bind_site_list";//绑定收银点列表选择
    public static final String TAG_CHOICE_SITE_LIST = "tag_choice_site_list";//收银点列表选择
    public static final String TAG_PAY_SITE_ADD = "tag_pay_site_add";//收银点新增
    public static final String TAG_SITE_DEVICE = "tag_site_device";//收银点已绑定列表
    public static final String TAG_SITE_DEVICE_LIST = "tag_site_device_list";//收银点关联查询列表
    public static final String TAG_LINK_SITE_DEVICE = "tag_link_site_device";//收银点关联设备
    public static final String TAG_CAPTURE_LINK_DEVICE = "tag_capture_link_device";//收银点扫码关联设备
    public static final String TAG_CAPTURE_LINK_QRCODE = "tag_capture_link_qrcode";//收银点扫码关联收款码
    public static final String TAG_CANCEL_LINK = "tag_cancel_link";//收银点取消关联
    public static final String TAG_CAPTURE_VIP_INFO = "tag_capture_vip_info";//猫酷会员信息
    public static final String TAG_CAPTURE_VIP_COUPON = "tag_capture_vip_coupon";//猫酷会员券
    public static final String TAG_CAPTURE_COUPON_INFO = "tag_capture_coupon_info";//猫酷优惠券信息
    public static final String TAG_RECEIVE_VIP_INFO = "tag_receive_vip_info";//猫酷会员信息
    public static final String TAG_RECEIVE_VIP_COUPON = "tag_receive_vip_coupon";//猫酷会员券
    public static final String TAG_RECEIVE_COUPON_INFO = "tag_receive_coupon_info";//猫酷优惠券信息
    public static final String TAG_VIP_RIGHT = "tag_vip_right";//猫酷会员权益
    public static final String TAG_COUPON_CHECK = "tag_coupon_check";//猫酷优惠券校验
    public static final String TAG_COUPON_VERIFY = "tag_coupon_verify";//猫酷优惠券核销
    public static final String TAG_COUPON_CANCEL = "tag_coupon_cancel";//猫酷优惠券核销撤销
    public static final String TAG_COUPON_CONTINUE_CANCEL = "tag_coupon_continue_cancel";//猫酷优惠券核销继续撤销
    public static final String TAG_SEND_MSG = "tag_send_msg";//发送短信tag
    public static final String TAG_LEGAL_INFO = "tag_legal_info";//修改法人tag
    public static final String TAG_BANK_ACCOUNT_INFO = "tag_bank_account_info";//结算银行卡
    public static final String TAG_BANK_ACCOUNT_INFO_PUBLIC = "tag_bank_account_info_public";//修改结算卡tag
    public static final String TAG_SEND_MSG_PUBLIC = "tag_send_msg_public";//发送短信tag
    public static final String TAG_PAY_TYPE = "tag_pay_type";//支付类型
    public static final String TAG_MODIFY_RECORD_LIST = "tag_modify_record_list";//商户资料修改记录列表
    public static final String TAG_MODIFY_RECORD_DETAIL = "tag_modify_record_detail";//商户资料修改记录详情
    public static final String TAG_WX_REAL_NAME_AUTH = "tag_wx_real_name_auth";//微信实名认证
    public static final String TAG_ALI_REAL_NAME_AUTH = "tag_ali_real_name_auth";//支付宝实名认证
    public static final String TAG_HS_VIP_LIST = "tag_hs_vip_list";//汇商VIP列表
    public static final String TAG_CARD_CONFIG_QUERY = "tag_card_config_query";//卡配置查询
    public static final String TAG_HS_VIP_COLLECT = "tag_hs_vip_collect";//汇商VIP数量统计
    public static final String TAG_HS_VIP_INFO = "tag_hs_vip_info";//汇商VIP信息
    public static final String TAG_CANCEL_REFUND = "tag_cancel_refund";//撤销退款
    public static final String TAG_QUERY_MCHSTATUS_ELETICKET = "tag_query_mchstatus_eleticket";//电子发票获取绑定状态
    public static final String TAG_DEVICE_BIND_ELETICKET = "tag_device_bind_eleticket";//电子发票设备绑定
    public static final String TAG_BOARDCODE_BIND_ELETICKET = "tag_boardcode_bind_eleticket";//电子发票牌码绑定
    public static final String TAG_GET_MANAGER_INDEXURL_ELETICKET = "tag_get_manager_indexurl_eleticket";//电子发票商户入驻入口
    public static final String TAG_API_TRADE_VOICE_ENABLE = "tag_api_trade_voice_enable";//API交易语音开关


    /************************H5页面*******************/

    public static final String MAIN_UPDATE_PROGRESS_TAG = "main_update_progress_tag";
    public static final String UPDATE_PROGRESS_TAG = "update_progress_tag";
    public static final String UPDATE_DOWNLOADING = "update_downloading";
    public static final String UPDATE_DOWNLOAD_SUCCESS = "update_download_success";
    public static final String UPDATE_DOWNLOAD_FAILED = "update_download_failed";
    public static final String INTENT_INSTRUCTION = "instruction";

    public static  String ZFB_MARKETING="https://hdalipay66.swiftpass.cn/merchantSignup/index.html?merchantId=hs_";

    public static String INTENT_CAPTURE_REFUND = "intent_capture_refund";
    public static String INTENT_QRCODE_STORE = "intent_qrcode_store";
    public static String INTENT_QRCODE_INFO = "intent_qrcode_info";
    public static String INTENT_QRCODE_STOREMERCHANTID = "intent_qrcode_storemerchantid";
    public static String INTENT_PAY_MONEY = "intent_pay_money";
    public static String INTENT_USED_COUPON = "intent_used_coupon";
    public static String INTENT_COUPON_MONEY = "intent_coupon_money";
    public static String INTENT_CUT_MONEY = "intent_cut_money";
    public static String INTENT_VIP_RIGHT_MONEY = "intent_vip_right_money";
    public static String INTENT_ACTUAL_MONEY = "intent_actual_money";
    public static String INTENT_TOTAL_COUPON_MONEY = "intent_total_coupon_money";
    public static String INTENT_QRCODE_ID = "intent_qrcode_id";
    public static String INTENT_SHOW_QRCODE_ID = "intent_show_qrcode_id";
    public static String INTENT_QRCODE_STORENAME = "intent_qrcode_storename";
    public static String INTENT_BIND_STORE_ID = "intent_bind_store_id";
    public static String INTENT_BIND_STORE_NAME = "intent_bind_store_name";
    public static String INTENT_SITE_NAME = "intent_site_name";
    public static String INTENT_SITE_ID = "intent_site_id";
    public static String INTENT_NAME = "intent_name";
    public static String INTENT_NAME_BIND = "intent_name_bind";
    public static String INTENT_NAME_SCAN_BILL = "intent_name_scan_bill";
    public static String INTENT_NAME_BILL_SHOP = "intent_name_bill_shop";//账单-门店
    public static String INTENT_NAME_COLLECT_SHOP = "intent_name_collect_shop";//报表-门店
    public static String INTENT_NAME_PAY_SHOP = "intent_name_pay_shop";//收款-门店
    public static String INTENT_NAME_MERCHANT_UPLOAD = "intent_name_merchant_upload";//商户-上传图片
    public static String INTENT_NAME_CASHIER_MULTISHOP = "intent_name_cashier_multishop";//收银员-选择门店
    public static String INTENT_NAME_CAPTURE_PAYRESULT = "intent_name_capture_payresult";//扫一扫-订单详情
    public static String INTENT_NAME_FACE_PAYRESULT = "intent_name_face_payresult";//pos刷脸-订单详情
    public static String INTENT_NAME_CHECK_VERIFY = "intent_name_check_verify";//验证密码-审核中
    public static String INTENT_LICENSE_DETAL = "intent_license_detal";//营业执照详情
    public static String INTENT_SHORT_NAME_DETAL = "intent_short_name_detal";//商户简称详情
    public static String INTENT_BANK_DETAIL = "intent_bank_detail";//银行卡详情
    public static String INTENT_CODE_URL = "intent_code_url";//分享详情
    public static String INTENT_CODE_TIMEOUT = "intent_code_timeout";//分享详情
    public static String INTENT_CLIP_TYPE = "intent_clip_type";
    public static String INTENT_REPORT_TAB = "intent_report_tab";
    public static String INTENT_REPORT_MONTH = "intent_report_month";
    public static String INTENT_REPORT_START_TIME = "intent_report_start_time";
    public static String INTENT_REPORT_END_TIME = "intent_report_end_time";
    public static String INTENT_REFUND_MONEY = "intent_can_refund_money";
    public static String INTENT_REMARK = "intent_remark";
    public static String INTENT_UNFREEZE_MONEY = "intent_unfreeze_money";
    public static String INTENT_PLEDGE_PAY_MONEY = "intent_pledge_pay_money";
    public static String INTENT_ACTIVE_TYPE = "intent_active_type";
    public static String INTENT_ACTIVE_DATA = "intent_active_data";
    public static String INTENT_ACTIVE_INFO = "intent_active_info";
    public static String INTENT_ACTIVE_EDIT = "intent_active_edit";
    public static String INTENT_ORDER_REFUND = "intent_order_refund";//去退款
    public static String INTENT_MATERIAL_ACTIVE_INFO = "intent_material_active_info";
    public static String INTENT_EDIT_ACTIVE_INFO = "intent_edit_active_info";
    public static String INTENT_CAPTURE_PLEDGE = "intent_capture_pledge";
    public static String INTENT_CAPTURE_SCAN_LOGIN = "intent_capture_scan_login";
    public static String INTENT_USER_HUABEI = "intent_user_huabei";//我的进入花呗数据
    public static String INTENT_NAME_FROM = "intent_name_from";
    public static String INTENT_NAME_PIC_BGSQ_NET = "intent_name_pic_bgsq_net";//变更申请函
    public static String INTENT_NAME_PIC_SFSQ_NET = "intent_name_pic_sfsq_net";
    public static String INTENT_CHANGEACOUNTCOUNT = "intent_changeacountcount";

    public static String INTENT_VIP_RECHARGE_BILL = "intent_vip_recharge_bill";
    public static String INTENT_VIP_COUNT_BILL = "intent_vip_count_bill";

    public static String GETUI_PUSH_VOICE_TAG = "getui_push_voice_tag";//语音播报初始化tag
    public static String GETUI_PUSH_VOICE_LIST_TAG = "getui_push_voice_list_tag";//语音播报推送门店列表tag
    public static String GETUI_PUSH_VOICE_STORESETTINGLIST_TAG = "getui_push_voice_storesettinglist_tag";//威到家语音播报推送门店列表tag
    public static String GETUI_PUSH_VOICE_SET_TAG = "getui_push_voice_set_tag";//语音播报开关设置tag
    public static String GETUI_PUSH_VOICE_PUSHUSERSWITCH_TAG = "getui_push_voice_pushuserswitch_tag";//到家订单语音播报门店开关设置tag
    public static String GETUI_STORE_COUNT_TAG = "getui_store_count_tag";//查询语音播报门店选择数量
    public static String INTENT_GET_GOODS_NAME = "intent_get_goods_name";//商品名称
    public static String INTENT_GUIDE_LOGIN = "intent_guide_login";//向导-登录
    public static String INTENT_MAIN_AUTHENTICATION = "intent_main_authentication";//首页-验证身份
    public static String INTENT_SHORT_AUTHENTICATION = "intent_short_authentication";//设置(商户简称)-验证身份
    public static String INTENT_TELEPHONE_AUTHENTICATION = "intent_telephone_authentication";//设置(更换手机号)-验证身份
    public static String INTENT_RESET_PWD_TELPHONE = "intent_reset_pwd_telphone";//重置密码
    public static String INTENT_RESET_MERCHANT_ID = "intent_reset_merchant_id";//重置密码商户号
    public static String INTENT_LOGIN_PWD = "intent_login_pwd";//登录密码密码
    public static String INTENT_PLEDGE_PAY_BEAN = "intent_pledge_pay_bean";//预授权bean
    public static String INTENT_NAME_SCAN_PLEDGE = "intent_name_scan_pledge";
    public static String INTENT_DEVICE_SN = "intent_device_sn";//设备sn
    public static String INTENT_DEVICE_ID= "intent_device_id";//设备sn
    public static String INTENT_PRINT_DEVICE_TYPE = "intent_print_device_type";//打印设备类型
    public static String INTENT_CAPTURE_SITE_DEVICE = "intent_capture_site_device";//扫码关联收银点
    public static String INTENT_CAPTURE_SITE_QRCODE = "intent_capture_site_qrcode";//扫码关联收银点
    public static String INTENT_SITE_BINDING = "intent_site_binding";//绑定关联收银点
    public static String INTENT_VANKE_VIP = "intent_vanke_vip";//猫酷会员
    public static String INTENT_VANKE_COUPON = "intent_vanke_coupon";//猫酷优惠券
    public static String INTENT_ADD_VANKE_COUPON = "intent_add_vanke_coupon";//猫酷优惠券
    public static String INTENT_KEY_ORDERNO = "orderNo";
    public static String INTENT_KEY_MESSAGEKIND = "messageKind";
    public static String INTENT_KEY_CLIENTID = "intent_receiver_clientid";


    public static final String ORDER_DETAIL_REVERSE_TAG = "order_detail_reverse_tag";//订单详情冲正tag
    public static final String ORDER_DETAIL_CHECK_STATE_TAG = "order_detail_check_state_tag";//订单详情查询状态tag

    public static final String AUTO_PWD_FREE_LOGIN_ERROR = "auto_pwd_free_login_error";//免密登录过期
    public static String INTENT_STORE_ID = "intent_store_id";
    public static String INTENT_STORE_DATA_TYPE = "intent_store_data_type";
    public static String INTENT_CASHIER_ID = "intent_cashier_id";

    public static String UPDATE_VERSION_TAG = "update_version_tag";//版本升级的tag
    public static String UPDATE_SERVICE_TAG = "update_service_tag";
    public static boolean DEVICE_LIST_REFRESH_TAG = false;

    public static String UPDATE_INSTALL_TAG = "update_install_tag";
    public static String AUTO_IMAGER_TAG = "auto_image_tag";
    public static String UPDATE_APP_URL = "update_app_url";
    public static String UPDATE_VERSION_NAME = "update_version_name";
    public static String CASHIER_REFUND_TAG = "cashier_refund_tag";//收银员退款权限
    public static String CASHIER_BILL_TAG = "cashier_bill_tag";//收银员查看流水权限
    public static String CASHIER_REPORT_TAG = "cashier_report_tag";//收银员查看统计权限
    public static String MAIN_UPDATE_VERSION_TAG = "main_update_version_tag";//首页版本升级的tag
    public static String UPDATE_VERSION_CANCEL = "update_version_cancel";//首页版本升级取消
    public static String LOGINOUT_TAG = "app_loginout_tag";//app退出tag
    public static String DELETE_PUSH_VOICE_DEVICEID_TAG = "delete_device_id_tag";//删除设备Id tag
    public static String PUSH_VOICE_DETAIL_TAG = "push_voice_detail_tag";//推送信息跳转到详情页面

    public static final String FILE_LOG_DIR = "/logs/";
    public static final String FILE_CACHE_ROOT = "/hstypay/";
    public static final String APK_FILE_NAME = "hwc.apk";
    public static final String DOWNLOADPATH = "/hwc/";
    public static final String FILE_CACHE_ROOT_ZYT = "/zytpay/";
    public static final String DOWNLOADPATH_ZYT = "/zyt/";
    public static final String APK_ZYT_FILE_NAME = "zytpay.apk";
    public static final String FILE_CACHE_ROOT_YIJIU = "/yijiu/";
    public static final String DOWNLOADPATH_YIJIU = "/yijiu/";
    public static final String APK_YIJIU_FILE_NAME = "yijiu.apk";
    public static final String FILE_CACHE_ROOT_WJY = "/wjy/";
    public static final String DOWNLOADPATH_WJY = "/wjy/";
    public static final String APK_WJY_FILE_NAME = "wjy.apk";
    public static final String FILE_CACHE_ROOT_DYZF = "/dyzf/";
    public static final String DOWNLOADPATH_DYZF = "/dyzf/";
    public static final String APK_DYZF_FILE_NAME = "dyzf.apk";
    public static final String DOWNLOADPATH_BDZF = "/bdzf/";
    public static final String APK_BDZF_FILE_NAME = "bdzf.apk";
    public static final String DOWNLOADPATH_HSTYEMS = "/hstyems/";
    public static final String APK_HSTYEMS_FILE_NAME = "hstyems.apk";
    public static final String DOWNLOADPATH_KINGDEEPAY = "/kingdeepay/";
    public static final String APK_KINGDEEPAY_FILE_NAME = "kingdeepay.apk";

    //attestationStatus:
    public static final int HPMerchantAuthenticateStatusUndefine = 0;//未认证:资料未补件 或 审核不通过；
    public static final int HPMerchantAuthenticateStatusPass = 1;//已认证:资料已补件，审核通过，可交易或部分可交易
    public static final int HPMerchantAuthenticateStatusProccess = 2;//认证中:资料已补件，审核通过，不可交易
    public static final int HPMerchantAuthenticateStatusInvaild = 3;// 未认证 资料已补件，审核不通过，不可交易（有失败原因）
    //tradeState:
    public static final int HPMerchantTradeNone = 0; //不可交易
    public static final int HPMerchantTradeAvailable = 10;//可交易
    public static final int HPMerchantTradeSomeAvailable = 20;//部分可交易

    public static final String ACTION_ORDER_REFUND_DATA = "com.hstypay.action.ACTION_ORDER_REFUND_DATA";
    public static final String ACTION_ORDER_DATA = "com.hstypay.action.ACTION_ORDER_DATA";
    public static final String ACTION_RECEIVE_RESET_DATA = "com.hstypay.action.ACTION_RECEIVE_RESET_DATA";
    public static final String ACTION_TODAY_DATA = "com.hstypay.action.ACTION_TODAY_DATA";
    public static final String ACTION_REPORT_DATA = "com.hstypay.action.ACTION_REPORT_DATA";
    public static final String ACTION_RECEIVED_CLIENT_ID_PUSH = "com.hstypay.action.RECEIVED_CLIENT_ID_PUSH";//可以获取到个推cid的广播


    public static final String ACTION_PLEDGE_UNFREEZE_DATA = "com.hstypay.action.ACTION_PLEDGE_UNFREEZE_DATA";
    public static final String ACTION_PLEDGE_DATA = "com.hstypay.action.ACTION_PLEDGE_DATA";
    /************************刷卡支付*******************/
    public static final String HUIFU = "huifu";
    public static final String WIZARPOS = "wizarpos";
    public static final String HPAY_POS_HUIFU = "HPAY_POS_HUIFU";
    public static final String HUIFUTIANXIA = "pay.huifu.bcard";
    public static final String HPAY_POS_EASYPAY = "HPAY_POS_EASYPAY";
    public static final String ESAYCARD = "pay.easypay.bcard";
    public static final String HPAY_CARD_YINSHANG = "pay.ums.bcard";
    public static final String HPAY_POS_YINSHANG = "HPAY_POS_UMS_";
    public static final String HPAY_POS_Q2 = "HPAY_POS_Q2_";

    public static final String HWC = "hwc";
    public static final String HSTYPAY = "hstypay";
    public static final String ZYTPAY = "zytpay";
    public static final String ZYTPAY_LIANDI = "zytpay_liandi";
    public static final String ZYTPAY_XGD = "zytpay_xgd";
    public static final String ZYTPAY_XDL_N900 = "zytpay_xdl_n900";
    public static final String LIANDI = "liandi";
    public static final String YIJIU = "yijiu";
    public static final String WJY = "wjy";
    public static final String WJY_WIZARPOS = "wjy_wizarpos";
    public static final String WJY_YINSHANG = "wjy_yinshang";
    public static final String YSYF = "ysyf";//宁波邮政甬上有福
    public static final String DYZF = "dyzf";//凯特斯大云支付
    public static final String BDZF = "bdzf";//贝迪支付
    public static final String HSTYEMS = "hstyems";//汇商通邮
    public static final String KINGDEEPAY = "kingdeepay";//金蝶支付
    public static final String WZFHYPAY = "wzfhyPay";//微之付
    public static final String MIBAGPAY = "mibagPay";//米袋子
    public static final String MIBAGPAY_LIANDI = "mibagpay_liandi";//米袋子-联迪易生支付

    /************************POS型号*******************/
    public static final String LANDI_APOS_A8 = "LANDI/APOS A8";//联迪A8
    public static final String WIZARPOS_Q2 = "wizarPOS/WIZARPOS Q2";//慧银Q2
    /************************银联刷脸支付*******************/

    public static final String WECHAT_FACEPAY = "pay.wechat.facepay.ums";//银商微信刷脸
    public static final String WECHAT_FACEPAY_MICROPAY = "pay.wechat.facepay.ums.micropay";//银商微信刷脸
    public static final String UNIONPAY_FACEPAY = "pay.unionpay.facepay";//银联POS刷脸扫码支付
    public static final String UNIONPAY_FACEPAY_MICROPAY = "pay.unionpay.facepay.micropay";//银联POS刷脸扫码支付
    public static final String UNIONPAY_FUKA_BCARD = "pay.fuka.bcard";//银商福卡刷卡支付
    public static final String UNIONPAY_WIZARD_FUKA_BCARD = "pay.fuka.wizarpos.bcard";//慧银福卡刷卡支付

    public static final String WJYS_APPID = "9a2e4b7e292143d9a8361be65abb9ccb";//银商appId(com.hsty.wankepay)
//    public static final String WJYS_APPID = "c6e5147ffd07447885f6142e6352586c";//银商appId(com.hsty.vankepay)
    /************************收款单*******************/

    public static final String RECEIPT_STORE_NAME = "receipt_name";//收款名
    public static final String ALIPAY_SHARE_ID = "2021001142674385";//支付宝分享key

    /************************错误码*******************/

    public static final String ERROR_CODE_NO_RIGH = "no.right.to.view";
    public static final String INTENT_NAME_BIND_UKEY_DEVICE = "intent_name_bind_ukey_device";//绑定ukey盒子设备-扫一扫
    public static final String INTENT_NAME_BIND_UKEY_BOARDCODE = "intent_name_bind_ukey_boardcode";//绑定ukey牌码-扫一扫
    public static final String INTENT_NAME_EDIT_STORE_DETAIL = "edit_store_detail";//编辑门店信息
    public static final String INTENT_DETAIL_STORE_ID = "detail_store_id";//门店id
    public static final String INTENT_DETAIL_STORE_NAME = "detail_store_name";//门店name
    public static final String RESULT_STORE_GROUP_BEAN_INTENT = "store_group_bean_intent";//选择的门店分组信息
    public static final String STORE_MANAGER_BEAN_INTENT = "store_manager_bean_intent";//选择的店长信息
    public static final String RESULT_ROLE_BEAN_INTENT = "role_bean_intent";//选择角色信息
    public static final String INTENT_GROUP_ID = "intent_group_id";//分组id
    public static final String INTENT_STORE_USER_ID = "intent_store_user_id";//店长id
    public static final String TAG_EMPLOYEE_LIST = "tag_employee_list";//员工列表tag
    public static final int ROLE_STORE_MANAGER = 4;//店长
    public static final String ROLE_STORE_NICK_MANAGER = "STORE_MANAGER";//店长
    public static final int ROLE_STORE_CASHIER = 2;  //收银员
    public static final String ROLE_STORE_NICK_CASHIER = "CASHIER";  //收银员
    public static final String LOGIN_ERROR_CODE_VERIFY_ERROR = "app.commons.verifycode.error.exception";//验证码错误
    public static final String LOGIN_ERROR_CODE_VERIFY_NULL = "app.commons.verifycode.null.exception";//验证码为空
    public static final String LOGIN_CLIENT_NEED_UPDATE_PWD = "client.login.need.update.pwd";//商户免密登录第一次需要输入密码登录
    public static final String INTENT_VALUE_ADD_STORE = "intent_value_add_store";//新增编辑门店
    public static final String INTENT_VALUE_STORE_ADD_EMPLOYEE = "intent_value_store_add_employee";//门店新增员工


    public static final class ApiProviderCode{//支付方式的APICODE
        public static final int WX = 1;//微信支付收款,tradeType == 1是会员卡充值
        public static final int ZFB = 2;//支付宝收款
        public static final int QQ = 4;//QQ钱包收款
        public static final int UNIONPAY = 5;//银联支付收款
        public static final int MEMBER = 6;//会员卡消费
        public static final int VIP_RECHARGE = 7;//人工充值
        public static final int VIP_VERIFICATION = 8;//会员卡核销
        public static final int VIP_COUNT_VERIFICATION = 9;//次卡核销
        public static final int CARD_PAY = 10;//刷卡支付收款
        public static final int FUKA = 11;
        public static final int POS_UNION_FACE = 13;//银联pos刷脸支付
        public static final int POS_WECHAT_FACE = 15;//微信POS刷脸支付收款
        public static final int DIGITAL_RMB = 17;//数字人民币收款
        public static final int OTHER_3 = 3;//其他支付收款
        public static final int OTHER_9 = 99;//其他支付收款
    }
    public static final int MENUNUM = 5;//首页每行多少个菜单
    public static final String SP_SMART_GUIDE_OVER = "sp_smart_guide_over";//新手向导完成
    public static final String SP_NAME_MENU_VERSION = "sp_name_menu_version";//菜单版本号
}

