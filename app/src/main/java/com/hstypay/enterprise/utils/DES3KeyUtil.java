package com.hstypay.enterprise.utils;

import android.content.Context;

import com.hstypay.enterprise.utils.RSA.Base64Utils;
import com.hstypay.enterprise.utils.RSA.RSAUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;

public class DES3KeyUtil {
    public static String secretKeyString ="qlJ7RcWyCH/k7E1+cen1PKzRXku62baKBQYLE8jJRWQUOhsdQbzYgl3ieMUw7ZSrh0Db5LH0EGAkB2SkGg" +
            "/57hr6yncZR7As+VhijQTGcHtUCdWPq/IxM/xeExwHNdGcwkMocn7TO3/w9onWULJcXSAS6fl6sftXuyiyOUsiOFo=";

    public static String secretIvString ="TVzhiyiQRJrSvM8iH0gSyi4SJHi1EUOzCBi5g/WRSaZNxilWTtJYww4iwmfXUPL3ud/Wn3XnmnG3ggW87C" +
            "+qV0V3S+KWS5dSN2RVCAl6atVvxNWZny0yAEMIcEI4ChX5RF2d5gqsa72U5TBNB9zkXzZBv29N0HRFOPIz5wz3x3M=";
    /**
     * 加密des秘钥
     * @return
     */
    public static void setDES3Key(Context context,String key){
        InputStream inPublic = null;
        try
        {
            // 从文件中得到公钥
            inPublic = context.getAssets().open("rsa_public_key.pem");
            PublicKey publicKey = RSAUtils.loadPublicKey(inPublic);
            //分段加密
            byte[] encryptByte = RSAUtils.RSAEncode(key.getBytes(), publicKey);

            // 为了方便观察吧加密后的数据用base64加密转一下，要不然看起来是乱码,所以解密是也是要用Base64先转换
            String afterencrypt = Base64Utils.encode(encryptByte);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inPublic != null) {
                try {
                    inPublic.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 加密des秘钥
     * @return
     */
    public static String getDES3Key(Context context,String encryptContent){
        InputStream inPrivate = null;
        try {
            // 从文件中得到私钥
            inPrivate = context.getAssets().open("rsa_private_key_pkcs8.pem");
            PrivateKey privateKey = RSAUtils.loadPrivateKey(inPrivate);
            // 因为RSA加密后的内容经Base64再加密转换了一下，所以先Base64解密回来再给RSA解密
//                    byte[] decryptByte = RSAUtils.decryptData(Base64Utils.decode(encryptContent), privateKey);
            //分段解密
            byte[] decryptByte = RSAUtils.RSADecode(Base64Utils.decode(encryptContent), privateKey);
            return new String(decryptByte);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inPrivate != null) {
                try {
                    inPrivate.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
}
