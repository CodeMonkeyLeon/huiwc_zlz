package com.hstypay.enterprise.utils;

/**
 * Created with IntelliJ IDEA.
 * User: ALAN
 * Date: 13-9-20
 * Time: 下午7:39
 * To change this template use File | Settings | File Templates.
 */

import android.os.Environment;
import android.text.TextUtils;

import com.hstypay.enterprise.app.MyApplication;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtils {

    /**
     * 获取单个文件的MD5值！
     *
     * @param file
     * @return
     */

    public static String getFileMD5(File file) {
        if (!file.isFile()) {
            return null;
        }
        MessageDigest digest = null;
        FileInputStream in = null;
        byte buffer[] = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {

                    e.printStackTrace();

                }
            }
        }
        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }

    /**
     * Description: 通过文件长度获取文件大小字符串（如xxMB）
     *
     * @param fileLength
     * @return
     */
    public static String getSizeStr(long fileLength) {
        String strSize = "";
        try {
            if (fileLength >= 1024 * 1024 * 1024) {
                strSize = (float) Math.round(10f * fileLength / (1024 * 1024 * 1024)) / 10 + "G";
            } else if (fileLength >= 1024 * 1024) {
                strSize = (float) Math.round(10 * fileLength / (1024 * 1024 * 1.0)) / 10 + "M";
            } else if (fileLength >= 1024) {
                strSize = (float) Math.round(10f * fileLength / (1024)) / 10 + "K";
            } else if (fileLength >= 0) {
                strSize = fileLength + "B";
            } else {
                strSize = "0B";
            }
        } catch (Exception e) {
            e.printStackTrace();
            strSize = "0B";
        }
        return strSize;
    }

    /**
     * Description: 通过文件修改时间获取文件修改时间字符串（如xxMB）
     *
     * @return
     */
    public static String getModifyTimeStr(long modifyTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date(modifyTime));
    }

    /**
     * Description: 删除文件
     *
     * @param fileName
     */
    public static void deleteFile(String fileName) {
        if (!TextUtils.isEmpty(fileName)) {
            File file = new File(fileName);
            if (file.exists()) {
                if (!file.delete())
                    LogUtil.d("delete failed--" + fileName);
            }
        }
    }

    //    /**指定目录删除指定文件后缀*/
    //    public static void deleteFileBySuffix(String path, String suffix)
    //    {
    //        File demifile = new File(getRootPath());
    //        if (demifile.isDirectory())
    //        {
    //            demifile.delete();
    //        }
    //        File file = new File(path);
    //        File tempFile = null;
    //        File[] files = file.listFiles();
    //        if (files == null)
    //        {
    //            return;
    //        }
    //        for (int i = 0; i < files.length; i++)
    //        {
    //            tempFile = files[i];
    //            if (tempFile.exists() && tempFile.getName().endsWith(suffix))
    //            {
    //                tempFile.delete();
    //            }
    //        }
    //    }

    /**
     * 返回跟目录
     */
    public static String getRootPath() {
        if (AppHelper.getApkType() == 0) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT;
        }else if (AppHelper.getApkType() == 1) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_ZYT;
        } else if (AppHelper.getApkType() == 2) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_YIJIU;
        } else if (AppHelper.getApkType() == 3) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_WJY;
        } else if (AppHelper.getApkType() == 5) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_DYZF;
        }else {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            if (!StringUtils.isEmptyOrNull(installChannel)){
                return Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+installChannel+"/";
            }else {
                return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT;
            }
        }

    }

    /**
     * 判断SDcard是否存在
     */
    public static boolean isSdcardExist() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * 如果SDcard存在，则返回SDcard上的普通文件的目录 否则，失败返回null
     */
    public static String makeDir(String dir) {
        if (!isSdcardExist()) {
            return null;
        }
        File file = new File(dir);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return null;
            }
        }
        return dir;
    }

    public static File makeDirFile(String dir) {
        String dirPath = makeDir(dir);
        if (dirPath != null) {
            return new File(dirPath);
        } else {
            return null;
        }
    }

    //    public static void delFileOrDirectory(String filepath)
    //        throws java.io.IOException
    //    {
    //        File f = new File(filepath);//定义文件路径
    //        if (f.exists() && f.isDirectory())
    //        {//判断是文件还是目录
    //            if (f.listFiles().length == 0)
    //            {//若目录下没有文件则直接删除
    //                f.delete();
    //            }
    //            else
    //            {//若有则把文件放进数组，并判断是否有下级目录
    //                File delFile[] = f.listFiles();
    //                int i = f.listFiles().length;
    //                for (int j = 0; j < i; j++)
    //                {
    //                    if (delFile[j].isDirectory())
    //                    {
    //                        delFileOrDirectory(delFile[j].getAbsolutePath());//递归调用del方法并取得子目录路径
    //                    }
    //                    delFile[j].delete();//删除文件
    //                }
    //            }
    //        }
    //    }

    public static String getAppFiledImg(long fileId) {
        return getRootPath() + "/download/cache/" + fileId;
    }

    public static String getAppFiled(String fileId) {
        return getRootPath() + "/download/cache/" + fileId;
    }

    public static String getAppCache() {
        return getRootPath() + "download/cache/";
    }

    public static String getAppPath() {
        //        return getRootPath() + "/download/app/";
        if (AppHelper.getApkType() == 0) {
            return defaultDownloadPath;
        }else if (AppHelper.getApkType() == 1) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_ZYT;
        } else if (AppHelper.getApkType() == 2) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_YIJIU;
        } else if (AppHelper.getApkType() == 3) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_WJY;
        } else if (AppHelper.getApkType() == 5){
            return Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.FILE_CACHE_ROOT_DYZF;
        }else {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            if (!StringUtils.isEmptyOrNull(installChannel)){
                return Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+installChannel+"/";
            }else {
                return defaultDownloadPath;
            }
        }
    }

    public static String getAppLog() {
        return getRootPath() + "/logs/";
    }

    /**
     * 下载地址
     */
    public static String DOWNLOAD_URL = "http://jime-files.b0.upaiyun.com/v1/";

    /**
     * 默认下载存储路径
     */
    public static final String defaultDownloadPath = Environment.getExternalStorageDirectory().getAbsolutePath()
            + Constants.FILE_CACHE_ROOT;

    /**
     * 获取应用下log文件
     */
    public static File getLogFile(String logName) {
        String root = FileUtils.makeDir(FileUtils.getRootPath() + Constants.FILE_LOG_DIR);
        File file = new File(root + logName);
        return file;
    }

    /**
     * 读取文件
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readTextFile(File file)
            throws IOException {
        String text = null;
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            text = readTextInputStream(is);
        } finally {
            if (is != null) {
                try {

                    is.close();
                } catch (Exception e) {
                }
            }
        }
        return text;
    }

    /**
     * 从流中读取文件
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static String readTextInputStream(InputStream is)
            throws IOException {
        StringBuffer strbuffer = new StringBuffer();
        String line;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is));
            while ((line = reader.readLine()) != null) {
                strbuffer.append(line).append("\r\n");
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return strbuffer.toString();
    }

    /**
     * 将文本内容写入文件
     *
     * @param file
     * @param str
     * @throws IOException
     */
    public static void writeTextFile(File file, String str)
            throws IOException {
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(new FileOutputStream(file));
            out.write(str.getBytes());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        }
    }

    public static boolean delAllFile(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(path + tempList[i]);
            } else {
                temp = new File(path + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                if (!temp.delete())
                    LogUtil.d("Jeremy-删除文件失败");
            }
            if (temp.isDirectory()) {
                delAllFile(path + "/" + tempList[i]);//先删除文件夹里面的文件
                //delFolder(path + "/" + tempList[i]);//再删除空文件夹
                flag = true;
            }
        }
        return flag;
    }

    public static void getFileName(String path) {
        File f = new File(path);
        if (!f.exists()) {
            System.out.println(path + " not exists");
            return;
        }

        File fa[] = f.listFiles();
        for (int i = 0; i < fa.length; i++) {
            File fs = fa[i];
            if (fs.isDirectory()) {
                LogUtil.d("Constants--getName--", fs.getName() + " [目录]");
            } else {
                LogUtil.d("Constants--getName--", fs.getName());
            }
        }
    }
}
