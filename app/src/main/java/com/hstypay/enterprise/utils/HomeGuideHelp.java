package com.hstypay.enterprise.utils;

import android.content.Context;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.base.BaseFragment;
import com.hstypay.enterprise.fragment.HomeFragment;

import aiven.guide.view.SmartGuide;
import aiven.guide.view.clip.CustomClip;
import aiven.guide.view.clip.ViewRectClip;
import aiven.guide.view.face.IntroPanel;
import aiven.guide.view.layer.GuidView;
import aiven.guide.view.util.SmartUtils;

/**
 * @author kuangzeyu
 * @time 2021/3/25
 * @desc 新手引导
 */
public class HomeGuideHelp {
    private static final String TAG_LAYER_NOTICE_HEADER = "layer_notice_header";//头部客服图标的蒙层tag
    private static final String TAG_LAYER_MONEY_HEADER = "layer_money_header";//头部收款图标的蒙层tag
    private static final String TAG_LAYER_RECEIVE_FUNCTION = "layer_receive_function";//功能菜单的蒙层tag
    private static final String TAG_LAYER_RECEIVE_ITEM = "layer_receive_item";//收款记录的蒙层tag
    private static final String TAG_LAYER_STATISTICS_MCH = "layer_statistics_mch";//收款统计和商家说的蒙层tag

    /**
     * 新手引导一
     * @param needGuideFirstMenu  是否需要引导第一个功能菜单，当有功能菜单时需要引导。
     */
    public static SmartGuide showGuideStep1(HomeFragment fragment, Context mContext,boolean needGuideFirstMenu) {
        //构建引导
        SmartGuide smartGuide = SmartGuide.newGuide(fragment);
        smartGuide
                .initBaseColor(0X99000000);//设置引蒙层背景颜色

        if (needGuideFirstMenu){
            //新建一个引导
            smartGuide.newLayer(TAG_LAYER_RECEIVE_FUNCTION)
            //创建一个镂空区域
                .buildViewRectClip(new SmartGuide.ClipPositionBuilder<ViewRectClip>() {
                    @Override
                    public ViewRectClip buildTarget() {
                        return ViewRectClip.newClipPos()
                                .setDstView(R.id.item_function_home_location)
                                .setPadding(SmartUtils.dip2px(mContext,6))
                                .setOffsetY(SmartUtils.dip2px(mContext,-5))
                                .clipRadius(SmartUtils.dip2px(mContext,50));
                    }
                })
                .over() ;
        }
        smartGuide.newLayer(TAG_LAYER_NOTICE_HEADER)
                .buildViewRectClip(new SmartGuide.ClipPositionBuilder<ViewRectClip>() {
                    @Override
                    public ViewRectClip buildTarget() {
                        return ViewRectClip.newClipPos()
                                .setDstView(R.id.iv_notice_header_home)
                                .setPadding(SmartUtils.dip2px(mContext,5))
                                .setOffsetY(SmartUtils.dip2px(mContext,0))
                                .clipRadius(SmartUtils.dip2px(mContext,30));
                    }
                })
                .over()
                .newLayer(TAG_LAYER_MONEY_HEADER)
                .buildViewRectClip(new SmartGuide.ClipPositionBuilder<ViewRectClip>() {
                    @Override
                    public ViewRectClip buildTarget() {
                        return ViewRectClip.newClipPos()
                                .setDstView(R.id.iv_money_header_home)
                                .setPadding(SmartUtils.dip2px(mContext,5))
                                .setOffsetY(SmartUtils.dip2px(mContext,0))
                                .clipRadius(SmartUtils.dip2px(mContext,30));
                    }
                })
                .buildIntroPanel(new SmartGuide.IntroPanelBuilder() {
                    @Override
                    public IntroPanel buildFacePanel() {
                        return IntroPanel.newIntroPanel(mContext)
                                //设置介绍图片与clipInfo的对齐信息
                                .setIntroBmp(R.mipmap.mask_kefu_sk)
                                .setAlign(SmartGuide.AlignX.ALIGN_RIGHT,SmartGuide.AlignY.ALIGN_BOTTOM)
                                .setSize(SmartUtils.dip2px(mContext,170),SmartUtils.dip2px(mContext,58))
                                .setOffset(SmartUtils.dip2px(mContext,-130),SmartUtils.dip2px(mContext,5));
                    }
                })
                .setOnGuidClickListener(new SmartGuide.OnGuidClickListener() {
                    @Override
                    public boolean emptyErrorClicked(SmartGuide smartGuide) {
                        showGuideStep2(fragment,smartGuide,mContext);
                        return false;//返回true，引导消失，false不消失
                    }

                    @Override
                    public void clipClicked(SmartGuide guide, GuidView view, String tag) {
                        showGuideStep2(fragment,guide,mContext);
                    }

                    @Override
                    public void introClicked(SmartGuide guide, GuidView view, String tag) {
                        showGuideStep2(fragment,guide,mContext);
                    }
                })
                .show();
        return smartGuide;
    }


    /**
     * 新手引导步骤2
     * */
    private static void showGuideStep2(HomeFragment fragment,SmartGuide guide, Context mContext){
        fragment.getRvHomeFragment().postDelayed(new Runnable() {
            @Override
            public void run() {
                int itemCount = fragment.getRvHomeFragment().getAdapter().getItemCount();
                if (itemCount>2){
                    fragment.getRvHomeFragment().scrollBy(0,SmartUtils.dip2px(mContext,60));
                }
                guide.clearLayers();
                guide.newLayer(TAG_LAYER_RECEIVE_ITEM)
                        //创建一个镂空区域
                        .buildViewRectClip(new SmartGuide.ClipPositionBuilder<ViewRectClip>() {
                            @Override
                            public ViewRectClip buildTarget() {
                                return ViewRectClip.newClipPos()
                                        .setDstView(R.id.ll_receive_recode_item)
//                                .setPadding(SmartUtils.dip2px(mContext,5))
                                        .clipRadius(SmartUtils.dip2px(mContext,4));
                            }
                        })
                        .buildIntroPanel(new SmartGuide.IntroPanelBuilder() {
                            @Override
                            public IntroPanel buildFacePanel() {
                                return IntroPanel.newIntroPanel(mContext)
                                        //设置介绍图片与clipInfo的对齐信息
                                        .setIntroBmp(R.mipmap.guide_step_two)
                                        .setAlign(SmartGuide.AlignX.ALIGN_LEFT,SmartGuide.AlignY.ALIGN_TOP)
                                        .setSize(SmartUtils.dip2px(mContext,240),SmartUtils.dip2px(mContext,53))
                                        .setOffset(SmartUtils.dip2px(mContext,-260),0);
                            }
                        })
                        .setOnGuidClickListener(new SmartGuide.OnGuidClickListener() {
                            @Override
                            public boolean emptyErrorClicked(SmartGuide smartGuide) {
                                if (itemCount>2){
                                    fragment.getRvHomeFragment().scrollBy(0,SmartUtils.dip2px(mContext,-60));
                                }
                                showGuideStep3(smartGuide,mContext);
                                return false;
                            }

                            @Override
                            public void clipClicked(SmartGuide guide, GuidView view, String tag) {
                                if (itemCount>2){
                                    fragment.getRvHomeFragment().scrollBy(0,SmartUtils.dip2px(mContext,-60));
                                }
                                showGuideStep3(guide,mContext);
                            }

                            @Override
                            public void introClicked(SmartGuide guide, GuidView view, String tag) {
                                if (itemCount>2){
                                    fragment.getRvHomeFragment().scrollBy(0,SmartUtils.dip2px(mContext,-60));
                                }
                                showGuideStep3(guide,mContext);
                            }
                        })
                        .show();

            }
        },100 );

    }


    /**
     * 新手引导步骤3
     * */
    private static void showGuideStep3(SmartGuide guide, Context mContext){
        guide.clearLayers();
        guide.newLayer(TAG_LAYER_STATISTICS_MCH)
                //创建一个镂空区域
                .buildCustomClip(new SmartGuide.ClipPositionBuilder<CustomClip>() {
                    @Override
                    public CustomClip buildTarget() {
                        return CustomClip.newClipPos()
                                .setAlignX(SmartGuide.AlignX.ALIGN_RIGHT)//设置定位水平定位偏向（设置镂空区域坐标的x原点为在屏幕的右侧）
                                .setAlignY(SmartGuide.AlignY.ALIGN_TOP)//设置定位垂直定位偏向（设置镂空区域坐标的y原点为在屏幕的上侧）
                                .setOffsetX(SmartUtils.getScreenWidth(mContext)/2)//根据水平定位偏向设置偏移，如果未ALIGN_LEFT,则是距离屏幕左侧偏移量，如果是ALIGN_RIGHT 则是距离屏幕右侧偏移量
                                //设置镂空裁剪区域尺寸
                                .setClipSize(SmartUtils.dip2px(mContext,1),SmartUtils.dip2px(mContext,1))
                                .clipRadius(SmartUtils.dip2px(mContext,1));
                    }
                })
                .buildIntroPanel(new SmartGuide.IntroPanelBuilder() {
                    @Override
                    public IntroPanel buildFacePanel() {
                        return IntroPanel.newIntroPanel(mContext)
                                //设置介绍图片与clipInfo的对齐信息
                                .setIntroBmp(R.mipmap.guide_step_three)
                                .setAlign(SmartGuide.AlignX.ALIGN_LEFT,SmartGuide.AlignY.ALIGN_BOTTOM)
                                .setSize(SmartUtils.dip2px(mContext,248),SmartUtils.dip2px(mContext,190))
                                .setOffset(SmartUtils.dip2px(mContext,-124),SmartUtils.dip2px(mContext,190));
                    }
                })
                .setOnGuidClickListener(new SmartGuide.OnGuidClickListener() {
                    @Override
                    public boolean emptyErrorClicked(SmartGuide smartGuide) {
                        return true;
                    }

                    @Override
                    public void clipClicked(SmartGuide guide, GuidView view, String tag) {
                        guide.dismiss();
                    }

                    @Override
                    public void introClicked(SmartGuide guide, GuidView view, String tag) {
                       guide.dismiss();
                    }
                })
                .show();
    }
}
