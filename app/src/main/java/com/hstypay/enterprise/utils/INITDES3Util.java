package com.hstypay.enterprise.utils;

import android.content.Context;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * 3DES加密工具类
 */
public class INITDES3Util {
    // 密钥
    private static String SECRETKEY = "";
    // 向量
    private static String IV = "01234567";
    // 加解密统一使用的编码方式
    private final static String encoding = "utf-8";

    public INITDES3Util(String s, String v) {
        this.SECRETKEY = s;
        this.IV = v;
    }

    /**
     * 3DES加密
     *
     * @param plainText 普通文本
     * @return
     */
    public String encrypt(String plainText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(SECRETKEY.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);

        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");
        IvParameterSpec ips = new IvParameterSpec(IV.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);
        byte[] encryptData = cipher.doFinal(plainText.getBytes(encoding));
        return Base64Util.encode(encryptData);
    }

    /**
     * 3DES解密
     *
     * @param encryptText 加密文本
     * @return
     */
    public String decrypt(String encryptText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(SECRETKEY.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);
        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");
        IvParameterSpec ips = new IvParameterSpec(IV.getBytes());
        cipher.init(Cipher.DECRYPT_MODE, deskey, ips);

        byte[] decryptData = cipher.doFinal(Base64Util.decode(encryptText));

        return new String(decryptData, encoding);
    }

    /**
     * 3DES加密
     *
     * @param secretKey 秘钥
     * @param iv        偏移向量
     * @param plainText 普通文本
     * @return
     * @throws Exception
     */
    public static String encryptString(String secretKey, String iv,
                                       String plainText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(secretKey.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);

        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");
        IvParameterSpec ips = new IvParameterSpec(iv.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);
        byte[] encryptData = cipher.doFinal(plainText.getBytes(encoding));
        return Base64Util.encode(encryptData);
    }

    /**
     * 3DES解密
     *
     * @param secretKey   秘钥
     * @param iv          偏移向量
     * @param encryptText 密文
     * @return
     * @throws Exception
     */
    public static String decryptString(String secretKey, String iv,
                                       String encryptText) throws Exception {
        Key deskey = null;
        DESedeKeySpec spec = new DESedeKeySpec(secretKey.getBytes());
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
        deskey = keyfactory.generateSecret(spec);
        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");
        IvParameterSpec ips = new IvParameterSpec(iv.getBytes());
        cipher.init(Cipher.DECRYPT_MODE, deskey, ips);

        byte[] decryptData = cipher.doFinal(Base64Util.decode(encryptText));

        return new String(decryptData, encoding);
    }

    /**
     * 3DES解码后解密
     *
     * @param secretKey   秘钥
     * @param iv          偏移向量
     * @param encryptText 密文
     * @return
     * @throws Exception
     */
    public static String decryptStringURLDecoder(String secretKey, String iv,
                                                 String encryptText) throws Exception {
        String retJsonStr = decryptString(secretKey, iv,
                URLDecoder.decode(encryptText));
        return retJsonStr;
    }

    /**
     * URLEncoder编码加密信息
     *
     * @param secretKey
     * @param iv
     * @param plainText
     * @return
     * @throws Exception
     */
    public static String encryptStringURLEncoder(String secretKey, String iv,
                                                 String plainText) throws Exception {
        String base64Str = encryptString(secretKey, iv, plainText);
        return URLEncoder.encode(base64Str);
    }


    public static void main(String[] args) throws Exception {
        String result = "";
        INITDES3Util desSource = new INITDES3Util("INbSvyvOTkSkcRNSc8HpHIat", "drS66rwt");
        //JSONObject outData = new JSONObject();
        //JSONObject resultOutData = new JSONObject();
        String outputStr = "123456";
        String para = desSource.encrypt(outputStr);//加密json串
        System.out.println(para);
        String decryptString = INITDES3Util.encryptString("INbSvyvOTkSkcRNSc8HpHIat", "drS66rwt", "sysadmin");
        System.out.println(decryptString);

    }

    public static String getSecretContent(Context context, String secretContent) {
        try {
            return new INITDES3Util(DES3KeyUtil.getDES3Key(context, DES3KeyUtil.secretKeyString)
                    , DES3KeyUtil.getDES3Key(context, DES3KeyUtil.secretIvString)).encrypt(secretContent);
        } catch (Exception e) {
            e.printStackTrace();
            ToastUtil.showToastShort("加密异常！");
        }
        return "";
    }
}