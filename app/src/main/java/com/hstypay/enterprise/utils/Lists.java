package com.hstypay.enterprise.utils;

import java.util.List;

/**
 * Created by smartTop on 2016/10/19.
 * list的工具类
 */

public class Lists {
    public Lists() {
    }

    public static boolean isEmpty(List list) {
        return list == null || list.size() == 0;
    }

    public static boolean notEmpty(List list) {
        return list != null && list.size() > 0;
    }

    public static String json = "{\"industry1\":[{\"code\":\"001\",\"id\":1,\"industry2\":[{\"code\":\"001\",\"id\":1,\"industry3\":[{\"code\":\"001\",\"id\":1,\"name\":\"水果\"},{\"code\":\"002\",\"id\":2,\"name\":\"生鲜\"}],\"name\":\"零售\"},{\"code\":\"002\",\"id\":2,\"industry3\":[{\"code\":\"001\",\"id\":1,\"name\":\"小吃\"},{\"code\":\"002\",\"id\":2,\"name\":\"外卖\"},{\"code\":\"003\",\"id\":3,\"name\":\"堂食\"}],\"name\":\"餐厅\"}],\"name\":\"食品\"},{\"code\":\"002\",\"id\":2,\"industry2\":[{\"code\":\"001\",\"id\":1,\"industry3\":[{\"code\":\"001\",\"id\":1,\"name\":\"java\"},{\"code\":\"002\",\"id\":2,\"name\":\"c++\"},{\"code\":\"003\",\"id\":3,\"name\":\"android\"},{\"code\":\"004\",\"id\":4,\"name\":\"ios\"},{\"code\":\"005\",\"id\":5,\"name\":\"php\"},{\"code\":\"006\",\"id\":6,\"name\":\"python\"}],\"name\":\"软件\"},{\"code\":\"002\",\"id\":2,\"industry3\":[{\"code\":\"001\",\"id\":1,\"name\":\"无人机\"},{\"code\":\"002\",\"id\":2,\"name\":\"机器人\"},{\"code\":\"003\",\"id\":3,\"name\":\"路由器\"},{\"code\":\"004\",\"id\":4,\"name\":\"交换机\"}],\"name\":\"硬件\"},{\"code\":\"003\",\"id\":3,\"industry3\":[{\"code\":\"001\",\"id\":1,\"name\":\"小米盒子\"},{\"code\":\"002\",\"id\":2,\"name\":\"华为门禁\"},{\"code\":\"003\",\"id\":3,\"name\":\"伴生活\"},{\"code\":\"004\",\"id\":4,\"name\":\"智慧餐厅\"}],\"name\":\"物联网\"}],\"name\":\"IT\"}]}";
}
