package com.hstypay.enterprise.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.hstypay.enterprise.app.MyApplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hsty.wankepay.utils
 * @创建者: Jeremy
 * @创建时间: 2017/8/28 14:49
 * @描述: ${TODO}
 */

public class LocationUtil {
    public static String cityName;  //城市名
    private static Geocoder geocoder;   //此对象能通过经纬度来获取相应的城市等信息

    private OnLocationListener mOnLocationListener;
    private LocationManager locationManager;
    static LocationUtil locationUtil;

    public static LocationUtil getInstance() {
        if (locationUtil == null) {
            locationUtil = new LocationUtil();
        }
        return locationUtil;
    }

    public interface OnLocationListener {
        void location(double lat, double lng);
    }

    public void setOnClickOkListener(OnLocationListener onLocationListener) {
        this.mOnLocationListener = onLocationListener;
    }

    /**
     * 通过地理坐标获取城市名  其中CN分别是city和name的首字母缩写
     *
     * @param context
     */
    public void getCNBylocation(Context context) {
        try {
            geocoder = new Geocoder(context);
            //用于获取Location对象，以及其他

            String serviceName = Context.LOCATION_SERVICE;
            //实例化一个LocationManager对象
            locationManager = (LocationManager) context.getSystemService(serviceName);
            //provider的类型
            String provider = LocationManager.GPS_PROVIDER;

            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);   //高精度
            criteria.setAltitudeRequired(false);    //不要求海拔
            criteria.setBearingRequired(false); //不要求方位
            criteria.setCostAllowed(true); //不允许有话费
            criteria.setPowerRequirement(Criteria.POWER_LOW);   //低功耗

            provider = locationManager.getBestProvider(criteria, true);
            //通过最后一次的地理位置来获得Location对象
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = locationManager.getLastKnownLocation(provider);
            /*
             * 第二个参数表示更新的周期，单位为毫秒；第三个参数的含义表示最小距离间隔，单位是米
             * 设定每30秒进行一次自动定位
             */
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 50, locationListener);
            //questLocation(location);
            updateWithNewLocation(location);

            /*
             * 第二个参数表示更新的周期，单位为毫秒；第三个参数的含义表示最小距离间隔，单位是米
             * 设定每30秒进行一次自动定位
             */
            //locationManager.requestLocationUpdates(provider, 30000, 50, locationListener);
            //移除监听器，在只有一个widget的时候，这个还是适用的
//        locationManager.removeUpdates(locationListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void questLocation(Location location) {
        double lat = 0;
        double lng = 0;
        if (location != null) {
            lat = location.getLatitude();//纬度
            lng = location.getLongitude();//经度
            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOCATION_LATITUDE, lat + "");
            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOCATION_LONGITUDE, lng + "");
            if (mOnLocationListener != null) {
                mOnLocationListener.location(lat, lng);
            }
        } else {
            LogUtil.d("无法获取地理信息");
        }
    }

    /**
     * 方位改变时触发，进行调用
     */
    private LocationListener locationListener = new LocationListener() {
        String tempCityName;

        public void onLocationChanged(Location location) {

            tempCityName = updateWithNewLocation(location);
            if ((tempCityName != null) && (tempCityName.length() != 0)) {
                cityName = tempCityName;
            }
        }

        public void onProviderDisabled(String provider) {
            tempCityName = updateWithNewLocation(null);
            if ((tempCityName != null) && (tempCityName.length() != 0)) {
                cityName = tempCityName;
            }
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    /**
     * 更新location
     *
     * @param location
     * @return cityName
     */
    @SuppressLint("MissingPermission")
    private String updateWithNewLocation(Location location) {
        /*if (location==null){
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 50, locationListener);
        }*/
        String mcityName = "";
        double lat = 0;
        double lng = 0;
        if (location != null) {
            lat = location.getLatitude();//纬度
            lng = location.getLongitude();//经度
            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOCATION_LATITUDE, lat + "");
            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOCATION_LONGITUDE, lng + "");
            if (mOnLocationListener != null) {
                mOnLocationListener.location(lat, lng);
            }
        } else {
            LogUtil.d("无法获取地理信息");
        }
        List<Address> addList = new ArrayList<>();
        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        } else {
            LogUtil.d("无法获取地理信息1");
        }

        try {
            addList = geocoder.getFromLocation(lat, lng, 1);    //解析经纬度

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addList != null && addList.size() > 0) {
            for (int i = 0; i < addList.size(); i++) {
                Address add = addList.get(i);
                mcityName += add.getLocality();
            }
        }
        if (mcityName.length() != 0) {
            return mcityName.substring(0, (mcityName.length() - 1));
        } else {
            return mcityName;
        }
    }
}
