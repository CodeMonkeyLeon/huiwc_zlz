package com.hstypay.enterprise.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {

    public static String md5s(String plainText) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();

            int i;

            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * 16位MD5加密
     * @param readyEncryptStr
     * @return
     */
    public final static String getMD5_16(String readyEncryptStr) {
        if(readyEncryptStr != null){
            return MD5.md5s(readyEncryptStr).substring(8, 24);
        }else{
            return null;
        }
    }

}
