package com.hstypay.enterprise.utils;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.activity.DeviceListActivity;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.activity.PayActivity;
import com.hstypay.enterprise.activity.SmartManagementActivity;
import com.hstypay.enterprise.activity.bounty.BountyActivity;
import com.hstypay.enterprise.activity.coupon.CouponVerifyListActivity;
import com.hstypay.enterprise.activity.employee.EmployeeManagerActivity;
import com.hstypay.enterprise.activity.huabei.HBDataActivity;
import com.hstypay.enterprise.activity.paySite.PaySiteListActivity;
import com.hstypay.enterprise.activity.pledge.PledgePayActivity;
import com.hstypay.enterprise.activity.receipt.ReceiptActivity;
import com.hstypay.enterprise.activity.store.StoreManageActivity;
import com.hstypay.enterprise.activity.storeCode.StoreCodeListActivity;
import com.hstypay.enterprise.activity.tinycashier.PCCashierSnManageActivity;
import com.hstypay.enterprise.activity.vipActivity.VipActiveActivity;
import com.hstypay.enterprise.activity.vipActivity.VipServiceActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.editmenu.MenuHelper;
import com.hstypay.enterprise.editmenu.entity.MenuItem;
import com.hstypay.enterprise.network.ServerClient;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kuangzeyu
 * @time 2021/3/22
 * @desc
 */
public class MenuClickUtil {

    public static void clickMenu(MainActivity context, MenuItem menuItem){
        switch (menuItem.getId()) {
            case MenuHelper.MenuItemId.ACTIVE_ID://会员活动,会员折扣
                MtaUtils.mtaId(MyApplication.getContext(), "G001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, VipActiveActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.STORECODE_ID://门店收款码管理
                MtaUtils.mtaId(context, "J001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, StoreCodeListActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.CASHIER_ID://员工管理
                MtaUtils.mtaId(context, "I001");
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, EmployeeManagerActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.BOUNTY_ID://营销奖励
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, BountyActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.HB_ID://花呗分期
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, HBDataActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.PLEDGE_ID://资金预授权
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, PledgePayActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.SCANORDER_ID://扫码点单
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                        openDialog(context);
                    } else {
                        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                            ((BaseActivity) context).loadDialog((BaseActivity) context, UIUtils.getString(R.string.public_loading));
                            Map<String, Object> map = new HashMap<>();
                            map.put("pageSize", 15);
                            map.put("currentPage", 1);
                            map.put("merchantDataType", 1);
                            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_SCAN_ORDER_STORE_LIST, map);
                        } else {
                            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                        }
                    }
                }
                break;
            case MenuHelper.MenuItemId.DEVICE_ID://设备管理
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, DeviceListActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.DEVICE_MALL_ID://设备商城
                toMiniProgram(context);
                break;
            case MenuHelper.MenuItemId.RECEIPT_ID://收款单
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, ReceiptActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.SENDHOME_ID://威生活到家
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        ((BaseActivity) context).loadDialog((BaseActivity) context, UIUtils.getString(R.string.public_loading));
                        /*if (MyApplication.getIsMerchant()) {
                            //用户是商户,查询商户是否开通到家付费详情
                            ServerClient.newInstance(MyApplication.getContext()).queryDaojiaEdition(MyApplication.getContext(), Constants.TAG_QUERY_DAOJIA, null);
                        } else {
                            //用户不是商户，入口可见表示已经请求过开通详情了，直接去查询商户门店信息
                            ((MainActivity) context).queryDaoJiaStores();
                        }*/
                        ServerClient.newInstance(MyApplication.getContext()).queryDaoJiaEnable(MyApplication.getContext(), Constants.TAG_QUERY_DAOJIA, null);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
                break;

            case MenuHelper.MenuItemId.PAYSITE_ID://收银点
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    if ("2".equals(MyApplication.getCashPointOpenStatus())) {
                        context.startActivity(new Intent(context, PaySiteListActivity.class));
                    } else {
                        haveOpenSvcApp("TAG_HOME_PAY_SITE_OPEN_STATUS",context);
                    }
                }
                break;
            case MenuHelper.MenuItemId.PAY_ID://扫码收款
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    if (MyApplication.isStoreNull()) {
                        MyToast.showToastShort(UIUtils.getString(R.string.error_store_null));
                    } else {
                        context.startActivity(new Intent(context, PayActivity.class));
                    }
                }
                break;

            case MenuHelper.MenuItemId.VERIFY_ID://核销订单
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, CouponVerifyListActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.TICKETSERVICE_ID:
                //ukey发票服务
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        ((BaseActivity) context).loadDialog(context, UIUtils.getString(R.string.public_loading));
                        Map<String, Object> map = new HashMap<>();
                        ServerClient.newInstance(MyApplication.getContext()).queryMchStatusEleTicket(MyApplication.getContext(), Constants.TAG_QUERY_MCHSTATUS_ELETICKET, map);
                    } else {
                        MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
                    }
                }
                break;

            case MenuHelper.MenuItemId.VIP_SERVICE_ID://会员服务
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, VipServiceActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.STORE_MANAGE_ID://门店管理
                if (StringUtils.isEmptyOrNull(MyApplication.getIsSuccessData())) {
                    openDialog(context);
                } else {
                    context.startActivity(new Intent(context, StoreManageActivity.class));
                }
                break;
            case MenuHelper.MenuItemId.PC_CJ_ID://收银插件
                context.startActivity(new Intent(context, PCCashierSnManageActivity.class));
                break;
            case MenuHelper.MenuItemId.SMART_MANAGEMENT://智慧经营
                context.startActivity(new Intent(context, SmartManagementActivity.class));
                break;
        }
    }

    //弹出dialog去完善资料
    private static void openDialog(MainActivity context) {
        ((BaseActivity)context).openDialog();
    }

    /**
     * 应用开通状态查询
     */
    private static void haveOpenSvcApp(String tag,Context context) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(((MainActivity)context).mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("appCode", "CASH-POINT");
            ServerClient.newInstance(MyApplication.getContext()).haveOpenSvcApp(MyApplication.getContext(), tag, map);
        } else {
            MyToast.showToast(ToastHelper.toStr(R.string.network_exception), Toast.LENGTH_SHORT);
        }
    }

    private static void toMiniProgram(Context context) {
        if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
            String appId = "wx6e411e48e4dacb00"; // 填应用AppId
            IWXAPI api = WXAPIFactory.createWXAPI(context, appId);
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = "gh_1d2ba02ad88f"; // 填小程序原始id
            req.path = "pages/device-mall/home/home";  //拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
            api.sendReq(req);
        } else {
            MyToast.showToastShort("请先安装微信！");
        }
    }
}
