package com.hstypay.enterprise.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by admin on 2017/6/30.
 */

public class MoneyFormat {

    public static MoneyFormat getInstance()
    {
        return Container.instance;
    }

    private static class Container
    {
        public static MoneyFormat instance = new MoneyFormat();
    }

    /** 将金额格式分 */
    public String getMoney(String strMoney)
    {
        double money = Double.parseDouble(strMoney);
        double t = money * 100;
        DecimalFormat df = new DecimalFormat("0");
        return df.format(t);
    }

    public String formatMoney(String money) {
        if (money == null || money.length() < 1) {
            return "";
        }
        NumberFormat format = new DecimalFormat("###,###.##");
        double v = Utils.Double.tryParse(money, 0);
        String result = format.format(v);
        return result;
    }

}
