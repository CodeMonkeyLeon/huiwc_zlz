package com.hstypay.enterprise.utils

import java.math.BigDecimal


/**
 * @Author dean.zeng
 * @Description 金额工具类
 * @Date 2020-06-30 10:25
 */
object MoneyUtils {
    /** 金额为分的格式  */
    const val CURRENCY_FEN_REGEX = "\\-?[0-9]+"

    /**
     * @Title: changeF2Y
     * @Description: 将分为单位的转换为元 （除100）
     * @param amount
     * @return: String
     */
    @JvmStatic
    fun changeF2Y(amount: Long): String {
        return BigDecimal.valueOf(amount).divide(BigDecimal(100)).toString()
    }

    /**
     * @Title: changeY2F
     * @Description: 将元为单位的转换为分 （乘100）
     * @param amount
     * @return: String
     */
    @JvmStatic
    fun changeY2F(amount: Long): String {
        return BigDecimal.valueOf(amount).multiply(BigDecimal(100)).toString()
    }

}