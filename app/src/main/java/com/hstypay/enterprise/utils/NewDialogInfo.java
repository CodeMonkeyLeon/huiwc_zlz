/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hstypay.enterprise.R;


/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author
 * @version
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NewDialogInfo extends Dialog
{
    
    private Context context;
    private TextView title;
    private TextView content, content1, content2;
    private Button btnOk;
    private TextView btnCancel;
    private ViewGroup mRootView;
    private HandleBtn handleBtn;
    private HandleBtnCancle cancleBtn;
    private View line_img, line_img_bt;
    private OnItemLongDelListener mDelListener;
    private int position;
    private OnSubmitCouponListener mOnSubmitCouponListener;
    private TextView tv_title;
    
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public NewDialogInfo(Context context, String contentStr, String btnOkStr, int flagMore, String tv_title,
        HandleBtn handleBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));

        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.new_dialog_info, null);

        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        
        this.context = context;
        if (null != handleBtn)
        {
            this.handleBtn = handleBtn;
        }
        initView(contentStr, btnOkStr, flagMore, tv_title);
        
        setLinster(flagMore);
        
    }
    
    public void setBtnOkText(String string)
    {
        if (btnOk != null)
        {
            btnOk.setText(string);
        }
    }
    
    public void setMessage(String msg)
    {
        this.content.setText(msg);
    }
    
    public void setmDelListener(OnItemLongDelListener mDelListener, int position)
    {
        this.mDelListener = mDelListener;
        this.position = position;
    }
    
    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener)
    {
        this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster(final int flagMore)
    {
        
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();

            }
            
        });
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();

            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView(String contentStr, String btnOkStr, int flag, String title)
    {
        tv_title = (TextView)findViewById(R.id.tv_title);
        content = (TextView)findViewById(R.id.content);
        content1 = (TextView)findViewById(R.id.tv_content2);
        content2 = (TextView)findViewById(R.id.tv_content3);
        btnOk = (Button)findViewById(R.id.btnOk);
        
        btnCancel = (TextView)findViewById(R.id.btnCancel);
        
        line_img = (View)findViewById(R.id.line_img);
        
        line_img_bt = (View)findViewById(R.id.line_img_bt);
        if (!StringUtils.isEmptyOrNull(btnOkStr))
        {
            btnOk.setText(btnOkStr);
        }
        if (!StringUtils.isEmptyOrNull(contentStr))
        {
            content.setText(contentStr);
        }

        if (!StringUtils.isEmptyOrNull(title))
        {
            tv_title.setVisibility(View.VISIBLE);
            tv_title.setText(title);
        }
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnCancle
    {
        void handleCancleBtn();
    }
    
    /***
     * 
     * 长按删除优惠
     * 
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener
    {
        public void onItemLongDelMessage(int position);
    }
    
    /***
     * 
     * 发布和修改优惠价提示
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener
    {
        public void onSubmitCouponListenerOk();
        
        public void onSubmitCouponListenerCancel();
    }
}
