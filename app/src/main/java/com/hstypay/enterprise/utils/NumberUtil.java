package com.hstypay.enterprise.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/08/19 10:50
 * @描述: ${TODO}
 */
public class NumberUtil {
    /**
     * 数字格式化
     * @param number
     * @return
     */
    public static String formatNumber(double number) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.CHINA);
        numberFormat.setMinimumFractionDigits(0);
        numberFormat.setMaximumFractionDigits(0);
        return numberFormat.format(number);
    }
}
