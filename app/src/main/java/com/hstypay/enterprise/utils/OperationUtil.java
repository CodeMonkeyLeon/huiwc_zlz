package com.hstypay.enterprise.utils;

import java.math.BigDecimal;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils
 * @创建者: Jeremy
 * @创建时间: 2018/6/27 15:45
 * @描述: ${TODO}
 */
public class OperationUtil {
    public static String div(BigDecimal data1 ,long data2){
        if(data2==0){
            throw new IllegalArgumentException ("除数必须不为零");
        }
        BigDecimal b2 = new BigDecimal(data2);
        return data1.divide(b2).toString();
    }
}
