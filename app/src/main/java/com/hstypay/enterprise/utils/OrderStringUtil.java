package com.hstypay.enterprise.utils;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils
 * @创建者: Jeremy
 * @创建时间: 2019/3/18 15:39
 * @描述: ${TODO}
 */
public class OrderStringUtil {

    public static String getRefundStateString(int refundState) {
        String refundStateString = "";
        switch (refundState) {
            case 1:
            case 4:
            case 6:
                refundStateString = "退款中";
                break;
            case 3:
                refundStateString = "退款失败";
                break;
            case 2:
                refundStateString = "退款成功";
                break;
            case 5:
                refundStateString = "转入代发";
                break;
        }
        return refundStateString;
    }

    public static String getTradeStateString(int tradeState) {
        String tradeStateString;
        switch (tradeState) {
            case 1:
                tradeStateString = "未支付";
                break;
            case 2:
                tradeStateString = "支付成功";
                break;
            case 3:
                tradeStateString = "已关闭";
                break;
            case 4:
                tradeStateString = "转入退款";
                break;
            case 8:
            case 9:
                tradeStateString = "已撤销";
                break;
            case 10:
                tradeStateString = "撤销中";
                break;
            default:
                tradeStateString = "未知";
                break;
        }
        return tradeStateString;
    }

    public static String getPledgeStateString(String tradeStatus) {
        String tradeStateString;
        switch (tradeStatus) {
            case "1":
                tradeStateString = "未支付";
                break;
            case "2":
                tradeStateString = "交易成功";
                break;
            case "3":
                tradeStateString = "已关闭";
                break;
            case "4":
                tradeStateString = "转入退款";
                break;
            case "5":
                tradeStateString = "已解冻";
                break;
            case "8":
            case "9":
                tradeStateString = "已撤销";
                break;
            case "10":
                tradeStateString = "撤销中";
                break;
            default:
                tradeStateString = "未知";
                break;
        }
        return tradeStateString;
    }

    public static String getTradeTypeString(int tradeType) {
        String tradeTypeString;
        switch (tradeType) {
            case 1:
                tradeTypeString = "微信支付";
                break;
            case 2:
                tradeTypeString = "支付宝支付";
                break;
            case 3:
                tradeTypeString = "财付通支付";
                break;
            case 4:
                tradeTypeString = "QQ钱包支付";
                break;
            case 5:
                tradeTypeString = "银联支付";
                break;
            case 6:
                tradeTypeString = "会员卡消费";
                break;
            case 7:
                tradeTypeString = "人工充值";
                break;
            case 8:
                tradeTypeString = "手动核销";
                break;
            case 9:
                tradeTypeString = "次卡核销";
                break;
            case 10:
                tradeTypeString = "刷卡支付";
                break;
            case 11:
                tradeTypeString = "福卡支付";
                break;
            case 13:
                tradeTypeString = "银联POS刷脸支付";
                break;
            case 15:
                tradeTypeString = "微信POS刷脸支付";
                break;
            case 1111:
                tradeTypeString = "猫酷券支付";
                break;
            case 1112:
                tradeTypeString = "美团点评券支付";
                break;
            case 1113:
                tradeTypeString = "印享星券支付";
                break;
            /*case 12:
                tradeTypeString = "企业饭卡收款";
                break;*/
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE://建行数字人民币
                tradeTypeString = "数字人民币";
                break;
            default:
                tradeTypeString = "其他支付";
                break;
        }
        return tradeTypeString;
    }

    /**
     * 通过tradeType和apiCode返回支付类型 apiCode 只区分银联刷脸 微信刷脸
     * @param tradeType
     * @return
     */
    public static String tradeTypeFromString(int tradeType,String apiCode) {
        String tradeTypeString;
        switch (tradeType) {
            case 1:
                tradeTypeString = "微信支付";
                break;
            case 2:
                tradeTypeString = "支付宝支付";
                break;
            case 3:
                tradeTypeString = "财付通支付";
                break;
            case 4:
                tradeTypeString = "QQ钱包支付";
                break;
            case 5:
                tradeTypeString = "银联支付";
                break;
            case 6:
                tradeTypeString = "会员卡消费";
                break;
            case 7:
                tradeTypeString = "人工充值";
                break;
            case 8:
                tradeTypeString = "手动核销";
                break;
            case 9:
                tradeTypeString = "次卡核销";
                break;
            case 10:
                tradeTypeString = "刷卡支付";
                break;
            case 11:
                tradeTypeString = "福卡支付";
                break;
            case 13:
                if (apiCode.equals(Constants.WECHAT_FACEPAY)){
                    tradeTypeString = "微信POS刷脸支付";
                }else{
                    tradeTypeString = "银联POS刷脸支付";
                }
                break;
            case 15:
                tradeTypeString = "微信POS刷脸支付";
                break;
            case 1111:
                tradeTypeString = "猫酷券支付";
                break;
            case 1112:
                tradeTypeString = "美团点评券支付";
                break;
            case 1113:
                tradeTypeString = "印享星券支付";
                break;
            /*case 12:
                tradeTypeString = "企业饭卡收款";
                break;*/
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE://建行数字人民币
                tradeTypeString = "数字人民币";
                break;
            default:
                tradeTypeString = "其他支付";
                break;
        }
        return tradeTypeString;
    }

    public static String getTradeTypeString(int apiProvider, int tradeType) {
        String tradeTypeString;
        switch (apiProvider) {
            case 1:
                if (tradeType == 1) {
                    tradeTypeString = "会员卡充值";
                } else {
                    tradeTypeString = "微信支付";
                }
                break;
            case 2:
                tradeTypeString = "支付宝支付";
                break;
            case 3:
                tradeTypeString = "财付通支付";
                break;
            case 4:
                tradeTypeString = "QQ钱包支付";
                break;
            case 5:
                tradeTypeString = "银联支付";
                break;
            case 6:
                tradeTypeString = "会员卡消费";
                break;
            case 7:
                tradeTypeString = "人工充值";
                break;
            case 8:
                tradeTypeString = "手动核销";
                break;
            case 9:
                tradeTypeString = "次卡核销";
                break;
            case 10:
                tradeTypeString = "刷卡支付";
                break;
            case 11:
                tradeTypeString = "福卡支付";
                break;
            case 13:
                tradeTypeString = "银联POS刷脸支付";
                break;
            case 15:
                tradeTypeString = "微信POS刷脸支付";
                break;
            case 1111:
                tradeTypeString = "猫酷券支付";
                break;
            case 1112:
                tradeTypeString = "美团点评券支付";
                break;
            case 1113:
                tradeTypeString = "印享星券支付";
                break;
            case Constants.DIGIT_RMB_PAY_TYPE_API_CODE://建行数字人民币
                tradeTypeString = "数字人民币";
                break;
            default:
                tradeTypeString = "其他";
                break;
        }
        return tradeTypeString;
    }

    public static String getReportTradeTypeString(int apiProvider, int type) {
        String tradeTypeString;
        switch (apiProvider) {
            case 1:
                tradeTypeString = "微信支付";
                break;
            case 2:
                tradeTypeString = "支付宝";
                break;
            case 3:
                tradeTypeString = "财付通";
                break;
            case 4:
                tradeTypeString = "QQ钱包";
                break;
            case 5:
                tradeTypeString = "银联支付";
                break;
            case 6:
                if (type == 1){
                    tradeTypeString = "会员卡充值";
                }else {
                    tradeTypeString = "会员卡消费";
                }
                break;
            case 7:
                tradeTypeString = "人工充值";
                break;
            case 8:
                tradeTypeString = "手动核销";
                break;
            case 9:
                tradeTypeString = "次卡核销";
                break;
            case 10:
                tradeTypeString = "刷卡支付";
                break;
            case 11:
                tradeTypeString = "福卡支付";
                break;
            case 13:
                tradeTypeString = "银联POS刷脸支付";
                break;
            case 15:
                tradeTypeString = "微信POS刷脸支付";
                break;
            default:
                tradeTypeString = "其他";
                break;
        }
        return tradeTypeString;
    }

    public static String getTradeTypeTitleString(int tradeType) {
        String typeTitleString;
        switch (tradeType) {
            case 1:
                typeTitleString = "微信交易单号";
                break;
            case 2:
                typeTitleString = "支付宝交易单号";
                break;
            case 3:
                typeTitleString = "财付通交易单号";
                break;
            case 4:
                typeTitleString = "QQ钱包交易单号";
                break;
            case 5:
                typeTitleString = "银联交易单号";
                break;
            case 10:
                typeTitleString = "交易订单号";
                break;
            case 1111:
                typeTitleString = "商户订单号";
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                typeTitleString = "第三方单号";
                break;
        }
        return typeTitleString;
    }

    public static String getPledgeTypeTitleString(int tradeType) {
        String typeTitleString;
        switch (tradeType) {
            case 1:
                typeTitleString = "微信交易单号";
                break;
            case 2:
                typeTitleString = "支付宝交易单号";
                break;
            case 3:
                typeTitleString = "财付通交易单号";
                break;
            case 4:
                typeTitleString = "QQ钱包交易单号";
                break;
            case 5:
                typeTitleString = "银联交易单号";
                break;
            case 10:
                typeTitleString = "交易订单号";
                break;
            case 1111:
                typeTitleString = "商户订单号";
                break;
            default:
                typeTitleString = "第三方单号";
                break;
        }
        return typeTitleString;
    }

    public static String getPledgeTradeTypeString(int tradeType) {
        String tradeTypeString;
        switch (tradeType) {
            case 1:
                tradeTypeString = "微信";
                break;
            case 2:
                tradeTypeString = "支付宝";
                break;
            case 3:
                tradeTypeString = "财付通";
                break;
            case 4:
                tradeTypeString = "QQ钱包";
                break;
            case 5:
                tradeTypeString = "银联";
                break;
            case 10:
                tradeTypeString = "刷卡";
                break;
            case 11:
                tradeTypeString = "福卡";
                break;
            case 13:
                tradeTypeString = "银联POS刷脸";
                break;
            case 15:
                tradeTypeString = "微信POS刷脸";
                break;
            default:
                tradeTypeString = "其他";
                break;
        }
        return tradeTypeString;
    }
}
