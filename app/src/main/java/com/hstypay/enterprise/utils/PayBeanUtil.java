package com.hstypay.enterprise.utils;

import com.hstypay.enterprise.bean.PayBean;
import com.hstypay.enterprise.bean.RefundBean;
import com.hstypay.enterprise.bean.TradeDetailBean;

/**
 * @author zeyu.kuang
 * @time 2020/11/21
 * @desc
 */
public class PayBeanUtil {

    public static TradeDetailBean getTradeDetail(PayBean.DataBean data) {
        TradeDetailBean tradeDetailBean = new TradeDetailBean();
        if (data != null) {
            tradeDetailBean.setStoreMerchantIdCnt(data.getStoreMerchantIdCnt());
            tradeDetailBean.setOrderNo(data.getOrderNo());
            tradeDetailBean.setTransactionId(data.getTransactionId());
            tradeDetailBean.setTradeTime(data.getTradeTime());
            tradeDetailBean.setTradeState(data.getTradeState());
            tradeDetailBean.setCashierName(data.getCashierName());
            tradeDetailBean.setOpUserRealName(data.getOpUserRealName());
            tradeDetailBean.setApiProvider(data.getApiProvider());
            tradeDetailBean.setApiCode(data.getApiCode());
            tradeDetailBean.setMoney(data.getMoney());
            tradeDetailBean.setRealMoney(data.getRealMoney());
            tradeDetailBean.setPayMoney(data.getPayMoney());
            tradeDetailBean.setCouponFee(data.getMchDiscountsMoney());
            tradeDetailBean.setCouponInfoList(data.getCouponInfoList());
            tradeDetailBean.setAttach(data.getAttach());
            tradeDetailBean.setOutTradeNo(data.getOutTradeNo());
            tradeDetailBean.setReqOrderNo(data.getReqOrderNo());
            tradeDetailBean.setOriReqOrderNo(data.getOriReqOrderNo());
            tradeDetailBean.setVoucherNo(data.getVoucherNo());
            tradeDetailBean.setCashPointName(data.getCashPointName());
            tradeDetailBean.setDeviceSn(data.getTermNo());

            /*tradeDetailBean.setRefundNo(data.getRefundNo());
            tradeDetailBean.setRefundTime(data.getRefundTime());
            tradeDetailBean.setRefundUser(data.getRefundUser());
            tradeDetailBean.setRefundStatus(data.getRefundStatus());
            tradeDetailBean.setRefundMoney(data.getRefundMoney());
            tradeDetailBean.setRefundUpdateTime(data.getRefundUpdateTime());*/

            tradeDetailBean.setOpenid(data.getOpenid());
            tradeDetailBean.setThirdMerchantId(data.getThirdMerchantId());
            tradeDetailBean.setThirdOrderNo(data.getThirdOrderNo());
            tradeDetailBean.setTradeCode(data.getTradeCode());
            tradeDetailBean.setMdiscount(data.getMdiscount());

            if (data.getRefundDTOList() != null && data.getRefundDTOList().size() > 0) {
                for (RefundBean refundBean : data.getRefundDTOList()) {
                    if (refundBean.getRefundNo().equals(data.getRefundNo())) {
                        tradeDetailBean.setStandby4(refundBean.getStandby4());
                        tradeDetailBean.setRefundNo(refundBean.getRefundNo());
                        tradeDetailBean.setRefundTime(refundBean.getCreateTime());
                        tradeDetailBean.setRefundUser(refundBean.getRefundUser());
                        tradeDetailBean.setRefundStatus(refundBean.getRefundStatus());
                        tradeDetailBean.setRefundMoney(refundBean.getRefundMoney());
                        tradeDetailBean.setRefundUpdateTime(refundBean.getRefundTime());
                        tradeDetailBean.setPtRefundReason(refundBean.getPtRefundReason());
                        tradeDetailBean.setPtAuditStatus(refundBean.getPtAuditStatus());
                        tradeDetailBean.setDeviceSn(data.getTermNo());
                        break;
                    }
                }
            }
        }
        return tradeDetailBean;
    }
}
