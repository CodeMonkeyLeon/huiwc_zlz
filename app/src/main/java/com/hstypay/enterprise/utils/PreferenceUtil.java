package com.hstypay.enterprise.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;

import com.hstypay.enterprise.app.MyApplication;

/**
 * Description：SharedPreferences的管理类
 *
 */
public class PreferenceUtil {

    private static SharedPreferences mSharedPreferences = null;
    private static Editor mEditor = null;

    public static void init(Context context){
    	if (null == mSharedPreferences) {
    		mSharedPreferences = android.preference.PreferenceManager.getDefaultSharedPreferences(context) ;
    	}
    }
    
    public static void removeKey(String key){
        mEditor = mSharedPreferences.edit();
        mEditor.remove(key);
        mEditor.commit();
    }
    
    public static void removeAll(){
        mEditor = mSharedPreferences.edit();
        mEditor.clear();
        mEditor.commit();
    }

    public static void commitString(String key, String value){
        //mEditor = mSharedPreferences.edit();
        //mEditor.putString(key, value);
        //mEditor.commit();
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(Constants.SP_SAVE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }
    
    public static String getString(String key, String faillValue){
       // return mSharedPreferences.getString(key, faillValue);
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(Constants.SP_SAVE_FILE, Context.MODE_PRIVATE);
        return sp.getString(key, faillValue);
    }
    
    public static void commitInt(String key, int value){
        mEditor = mSharedPreferences.edit();
        mEditor.putInt(key, value);
        mEditor.commit();
    }
    
    public static int getInt(String key, int failValue){
        return mSharedPreferences.getInt(key, failValue);
    }
    
    public static void commitLong(String key, long value){
        mEditor = mSharedPreferences.edit();
        mEditor.putLong(key, value);
        mEditor.commit();
    }
    
    public static long getLong(String key, long failValue) {
        return mSharedPreferences.getLong(key, failValue);
    }
    
    public static void commitBoolean(String key, boolean value){
        //mEditor = mSharedPreferences.edit();
        //mEditor.putBoolean(key, value);
       // mEditor.commit();
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(Constants.SP_SAVE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static boolean getBoolean(String key, boolean failValue){
        SharedPreferences sp = MyApplication.getContext().getSharedPreferences(Constants.SP_SAVE_FILE, Context.MODE_PRIVATE);
        return sp.getBoolean(key, failValue);
    }
}
