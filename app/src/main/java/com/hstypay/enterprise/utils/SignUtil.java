/*
 * 文 件 名:  SignUtil.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-6-10
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hstypay.enterprise.app.MyApplication;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;

/**
 * 签名秘钥 帮助
 *
 * @author
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SignUtil {
    public static final MediaType JSON = MediaType.parse("application/json;charset=utf-8");

    private static SignUtil signUtil = null;

//    private static Gson gson = new Gson();
    private static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .create();
    private String mMd5SignKey;

    public static SignUtil getInstance() {
        if (signUtil == null) {
            signUtil = new SignUtil();
        }
        return signUtil;
    }

    // 微信红包sign
    public String createRedpackSign(Map<String, String> params) {
        paraFilter(params);
        StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
        buildPayParams(buf, params, false);
        String preStr = buf.toString();
        // return MD5Util.MD5Encode(preStr, "utf-8").toUpperCase();
        //return cn.swiftpass.enterprise.utils.MD5.md5s(preStr).toUpperCase();
        //        return MD5.md5s(preStr).toUpperCase();
        return null;
    }

    public String createSign(Map<String, String> params, String signKey) {
        params = paraFilter(params);
        StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
        buildPayParams(buf, params, false);
        buf.append("&key=" + signKey);
        String preStr = buf.toString();
        return MD5.md5s(preStr).toUpperCase();
    }

    public String loginAppSign(Map<String, Object> params, String signKey, String random) {
        String content = "";
        if (params != null && params.size() > 0) {
            content = gson.toJson(params);
            LogUtil.i("Jeremy-loginAppSign-","json数据=="+content);
        }
        mMd5SignKey = MD5.md5s(signKey);
        Map<String, String> resMap = new HashMap<String, String>();
        resMap.put("content", content);
        resMap.put("sign", MD5.md5s(mMd5SignKey + content + random));
        return gson.toJson(resMap);
    }
    
    public String createAppSign(Map<String, Object> params, String random) {
        String content = "";
        if (params != null && params.size() > 0) {
            content = gson.toJson(params);
            LogUtil.i("Jeremy","json数据=="+content);
        }

        String key = MyApplication.getSignKey() + MyApplication.getSkey();
        String mMd5SignKeys = MD5.md5s(key);
        Map<String, String> resMap = new HashMap<String, String>();
        String sings = MD5.md5s(mMd5SignKeys + content + random);
        resMap.put("content", content);
        resMap.put("sign", sings);
        return gson.toJson(resMap);
    }

    private void buildPayParams(StringBuilder sb, Map<String, String> payParams, boolean encoding) {
        List<String> keys = new ArrayList<String>(payParams.keySet());
        Collections.sort(keys);
        for (String key : keys) {
            sb.append(key).append("=");
            if (encoding) {
                sb.append(urlEncode(payParams.get(key)));
            } else {
                sb.append(payParams.get(key));
            }
            sb.append("&");
        }
        sb.setLength(sb.length() - 1);
    }

    /**
     * 过滤参数
     *
     * @param sArray
     * @return
     */
    public static Map<String, String> paraFilter(Map<String, String> sArray) {
        Map<String, String> result = new HashMap<String, String>(sArray.size());
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign") || key.equalsIgnoreCase("nns")) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }

    private String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (Throwable e) {
            return str;
        }
    }
}
