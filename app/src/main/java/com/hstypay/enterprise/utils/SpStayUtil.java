package com.hstypay.enterprise.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @项目名: SelfDinner
 * @包名: com.lwt.selfdinner.utils
 * @创建者: Ashin
 * @创建时间: 2016/10/24 15:46
 * @描述: 一般用于保存跟用户相关的设置，key是用户id。 如保存开关值
 */
public class SpStayUtil {
    private static SharedPreferences mSharedPreferences = null;
    private static SharedPreferences.Editor mEditor = null;

    public static void init(Context context) {
        if (null == mSharedPreferences) {
            if (AppHelper.getApkType() == 1) {
                mSharedPreferences = context.getSharedPreferences(Constants.SP_SAVE_FILE_STAY_ZYT, Context.MODE_PRIVATE);
            } else {
                mSharedPreferences = context.getSharedPreferences(Constants.SP_SAVE_FILE_STAY, Context.MODE_PRIVATE);
            }
        }
    }

    /**
     * 保存boolean值串到sp中
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putBoolean(Context context, String key, boolean value) {
        mEditor = mSharedPreferences.edit();
        mEditor.putBoolean(key, value);
        mEditor.commit();
    }

    /**
     * 获取sp中的boolean值
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getBoolean(Context context, String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }

    public static boolean getBoolean(Context context, String key) {
        return mSharedPreferences.getBoolean(key, false);
    }


    /**
     * 保存字符串到sp中
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putString(Context context, String key, String value) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(key, value);
        mEditor.commit();
    }

    /**
     * 获取sp中的字符串
     *
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static String getString(Context context, String key, String defValue) {
        return mSharedPreferences.getString(key, defValue);
    }

    public static String getString(Context context, String key) {
        return mSharedPreferences.getString(key, "");
    }

    //--------------------------------------------------------
    public static void putInt(Context context, String key, int value) {
        mEditor = mSharedPreferences.edit();
        mEditor.putInt(key, value);
        mEditor.commit();
    }

    public static int getInt(Context context, String key, int defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    public static int getInt(Context context, String key) {
        return getInt(context, key, 0);
    }

    public static void putFloat(Context context, String key, float value) {
        //获取编辑器
        mEditor = mSharedPreferences.edit();
        //保存
        mEditor.putFloat(key, value);
        //提交
        mEditor.commit();
    }

    public static float getFloat(Context context, String key, float defValue) {
        //获取sp对象
        return mSharedPreferences.getFloat(key, defValue);
    }

    public static float getFloat(Context context, String key) {
        //获取sp对象
        return getFloat(context, key, 0f);
    }

    public static void removeKey(String key) {
        mEditor = mSharedPreferences.edit();
        mEditor.remove(key);
        mEditor.commit();
    }

    public static void removeAll() {
        mEditor = mSharedPreferences.edit();
        mEditor.clear();
        mEditor.commit();
    }
}
