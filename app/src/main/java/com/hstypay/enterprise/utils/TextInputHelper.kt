package com.hstypay.enterprise.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView


/**
 * @Author dean.zeng
 * @Description 根据文字控制按钮是否可点击
 * @Date 2020-07-21 14:51
 **/
class TextInputHelper(val view: View) : TextWatcher {


    private lateinit var mViewSet: ArrayList<TextView>

    /**
     * 添加EditText或者TextView监听
     *
     * @param views     传入单个或者多个EditText或者TextView对象
     */
    fun addViews(vararg views: TextView) {
        if (!this::mViewSet.isInitialized) {
            mViewSet = arrayListOf()
            views.forEach {
                mViewSet.add(it)
                it.addTextChangedListener(this)
            }
        }
        afterTextChanged(null)
    }

    /**
     * 移除EditText监听，避免内存泄露
     */
    fun removeViews() {
        if (!this::mViewSet.isInitialized) {
            return
        }
        mViewSet.forEach {
            it.removeTextChangedListener(this)
        }
        mViewSet.clear()

    }

    override fun afterTextChanged(p0: Editable?) {
        if (!this::mViewSet.isInitialized) {
            return
        }
        view.isEnabled = !mViewSet.any { it.text.toString() == "" }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}