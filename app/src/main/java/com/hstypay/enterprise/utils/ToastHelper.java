package com.hstypay.enterprise.utils;

import android.app.Activity;
import android.widget.Toast;

import com.hstypay.enterprise.app.MyApplication;


public class ToastHelper
{
    
    public static String toStr(int resId)
    {
        return MyApplication.getContext().getString(resId);
    }
    
    public static void showInfo(final Activity activity, final String msg)
    {
        
        activity.runOnUiThread(new Runnable()
        {
            
            @Override
            public void run()
            {
                NewDialogInfo info = new NewDialogInfo(activity, msg, null, 2, null, null);
                DialogHelper.resize(activity, info);
                info.show();
            }
        });
    }
    
    public static void showInfo(String msg)
    {
        Toast.makeText(MyApplication.getContext(), msg, Toast.LENGTH_SHORT).show();

    }
    
    public static void showError(String msg)
    {
        Toast.makeText(MyApplication.getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
