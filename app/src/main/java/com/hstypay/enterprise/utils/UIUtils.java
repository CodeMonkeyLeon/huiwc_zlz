package com.hstypay.enterprise.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;

import com.hstypay.enterprise.app.MyApplication;


public class UIUtils {
    /**获得上下文*/
    public static Context getContext(){
        return MyApplication.getContext();
    }
    /**获取resource对象*/
    public static Resources getResource(){
        return getContext().getResources();
    }
    /**获取string.xml中的字符*/
    public static String getString(int resId){
        return getResource().getString(resId);
    }
    /**获取string.xml中的字符数组*/
    public static String[] getStrings(int resId){
        return getResource().getStringArray(resId);
    }
    /**获取color.xml中的颜色*/
    public static int getColor(int resId){
        //return getResource().getColor(resId);
        return ContextCompat.getColor(MyApplication.getContext(), resId);
    }
    public static ColorStateList getCheckebleColor(int resId){
        return getResource().getColorStateList(resId);
    }
    /**获取包名*/
    public static String getPackageName(){
        return getContext().getPackageName();
    }

    /**dp-->px*/
    public static int dp2px(int dp){
        //px/dp == denisty;
        //px/(ppi/160) = dp;
        float density = getResource().getDisplayMetrics().density;
        int px = (int) (dp*density+.5f);
        return px;
    }

    /**dp-->px*/
    public static float dp2px(float dp){
        float density = getResource().getDisplayMetrics().density;
        float px = dp*density;
        return px;
    }

    /**px-->dp*/
    public static int px2dp(int px){
        float density = getResource().getDisplayMetrics().density;
        int dp = (int) (px/density+.5f);
        return dp;
    }
}
