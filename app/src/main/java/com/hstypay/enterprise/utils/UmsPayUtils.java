package com.hstypay.enterprise.utils;

import android.app.Activity;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.hstypay.enterprise.bean.YinshangPosBean;
import com.ums.AppHelper;
import com.ums.anypay.service.IOnTransEndListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author dean.zeng
 * @Description 银商刷卡消费 福卡
 * @Date 2020-06-30 15:58
 **/
public class UmsPayUtils {

    private static final String APPID = Constants.WJYS_APPID;

    public static void callTrans(Activity activity, Builder builder, final UmsCallBack callBack) {
        Map<String, Object> transData = new HashMap<>();
        transData.put("appId", APPID);
        if (builder.type == Type.Consumption) {
            transData.put("amt", builder.amt);
        } else if (builder.type == Type.Revoke) {
            transData.put("orgTraceNo", builder.orgTraceNo);
        } else {
            transData.put("refNo", builder.refNo);
            transData.put("amt", builder.money);
            transData.put("date", DateUtil.formartTradeTimeFormat(builder.tradeTime,"MMdd"));
            transData.put("tradeYear", DateUtil.formartTradeTimeFormat(builder.tradeTime,"yyyy"));
        }
        transData.put("isShowResult", true);
        transData.put("isNeedPrintReceipt", false);
        transData.put("extOrderNo", builder.extOrderNo);
        JSONObject data = null;
        try {
            data = new JSONObject(JSON.toJSONString(transData));
            LogUtil.d("Jeremy-fuka-request-"+JSON.toJSONString(transData));
            AppHelper.callTrans(activity, "预付卡", builder.type.value, data, new IOnTransEndListener() {
                @Override
                public void onEnd(String s) {
                    LogUtil.d("Jeremy-" + s);
                    YinshangPosBean yinshangPosBean = new Gson().fromJson(s, YinshangPosBean.class);
                    callBack.response(yinshangPosBean);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 银商刷脸回调接口
     */
    public interface UmsCallBack {
        void response(YinshangPosBean posBean);
    }

    public static class Builder {
        Type type;
        String amt;
        String extOrderNo;

        String orgTraceNo;
        String refNo;
        String tradeTime;
        String money;

        public Builder(Type type, String extOrderNo) {
            this.type = type;

            this.extOrderNo = extOrderNo;
        }

        /**
         * 交易金额
         *
         * @param amt
         * @return
         */
        public Builder setAmt(String amt) {
            this.amt = amt;
            return this;
        }

        /**
         * 交易凭证号
         *
         * @param orgTraceNo
         * @return
         */
        public Builder setOrgTraceNo(String orgTraceNo) {
            this.orgTraceNo = orgTraceNo;
            return this;
        }

        /**
         * 原交易参考号
         *
         * @param refNo
         * @return
         */
        public Builder setRefNo(String refNo) {
            this.refNo = refNo;
            return this;
        }

        /**
         * 日期
         *
         * @param tradeTime
         * @return
         */
        public Builder setTradeTime(String tradeTime) {
            this.tradeTime = tradeTime;
            return this;
        }

        /**
         * 退货金额
         *
         * @param money
         * @return
         */
        public Builder setMoney(String money) {
            this.money = money;
            return this;
        }

    }

    public enum Type {
        Consumption("消费"),
        Revoke("撤销"),
        Refund("退货"),
        ;
        public String value;

        Type(String value) {
            this.value = value;
        }
    }

}
