package com.hstypay.enterprise.utils

import java.io.UnsupportedEncodingException
import java.net.URLDecoder

/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2019/10/11 17:06
 **/
object UrlDecode {

    fun decode(url: String): String {
        try {

            var prevURL = ""

            var decodeURL = url

            while (prevURL != decodeURL) {

                prevURL = decodeURL

                decodeURL = URLDecoder.decode(decodeURL, "UTF-8")

            }

            return decodeURL

        } catch (e: UnsupportedEncodingException) {
            return "Issue while decoding" + e.message
        }

    }

}