/*
 * 文 件 名:  BluePrintUtil.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:
 * 修改时间:
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.utils.print;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.SelectDialog;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.PrintContentBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * 打印模板
 *
 * @author he_hui
 * @version [版本号, 2017-4-20]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BluePrintUtil {
    /**
     * 打印纸一行最大的字节
     */
    private static final int LINE_BYTE_SIZE = 32;

    /**
     * 蓝牙UUID
     */
    public static UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static StringBuffer sb = new StringBuffer();

    /**
     * 打印收款统计、会员统计、饭卡消费的日结小票
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public static void printDateSum(ReportBean.DataEntity info) {
        if (MyApplication.bluetoothSocket == null)
            return;
        DataOutputStream os = null;
        try {
            os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());
            if (os == null) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuffer printBuffer = new StringBuffer();
        StringBuffer titlebuffer = new StringBuffer();
        String merChantName = TextUtils.isEmpty(MyApplication.getMerchantShortName()) ? MyApplication.getMerchantName() : MyApplication.getMerchantShortName();
        titlebuffer.append("\n" + merChantName + "\n");
        if (info.getType() == 1) {
            titlebuffer.append("\n" + UIUtils.getString(R.string.print_pay_report_title_new) + "\n\n");
        } else if (info.getType() == 2) {
            titlebuffer.append("\n" + UIUtils.getString(R.string.print_consume_report_title) + "\n\n");
        } else if (info.getType() == 3) {
            //饭卡
            titlebuffer.append("\n" + UIUtils.getString(R.string.print_fk_consume_report_title) + "\n\n");
        }
        BluePrintUtil.POS_S_Align(1, os);
        POS_S_TextOut(titlebuffer.toString(), "gbk", 0, 1, 1, 0, 0, os);
        /*BluePrintUtil.POS_S_Align(0, os);
        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                printBuffer.append(one + "\n");
                String two = sbf.substring(16, str.length());
                printBuffer.append(two + "\n");
            } else if (MyApplication.getMerchantName().length() > 11) {
                printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                printBuffer.append(MyApplication.getMerchantName() + "\n");
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
            }
        }*/
        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            printBuffer.append(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
        }
        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "全部门店" + "\n");
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "\n");
                printBuffer.append(one + "\n");
                String two = sbf.substring(16, str.length());
                printBuffer.append(two + "\n");
            } else if (info.getStoreName().length() > 11) {
                printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "\n");
                printBuffer.append(info.getStoreName() + "\n");
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + info.getStoreName() + "\n");
            }
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (!StringUtils.isEmptyOrNull(info.getCashierName())) {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printBuffer.append(cashierTitle + "：" + "\n");
                printBuffer.append(one + "\n");
                String two = sbf.substring(16, str.length());
                printBuffer.append(two + "\r\n");
            } else if (info.getCashierName().length() > 12) {
                printBuffer.append(cashierTitle + "：" + "\n");
                printBuffer.append(info.getCashierName() + "\n");
            } else {
                printBuffer.append(cashierTitle + "：" + info.getCashierName() + "\n");
            }
        }
        if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
            if (info.getCashierPointName().length() > 16) {
                printBuffer.append("收银点: \n");
                StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                String oneuser = sbf.substring(0, 16);
                printBuffer.append(oneuser + "\n");
                String twouser = sbf.substring(16, info.getCashierPointName().length());
                printBuffer.append(twouser + "\n");
            } else if (info.getCashierPointName().length() > 12) {
                printBuffer.append("收银点: \n");
                printBuffer.append(info.getCashierPointName() + "\n");
            } else {
                printBuffer.append("收银点: " + info.getCashierPointName() + "\n");
            }
        }

        printBuffer.append(UIUtils.getString(R.string.print_start_time_title) + "：" + info.getStartTime() + "\n");
        printBuffer.append(UIUtils.getString(R.string.print_end_time_title) + "：" + info.getEndTime() + "\n");

        printBuffer.append(UIUtils.getString(R.string.print_double_horizontal) + "\n");
        BluePrintUtil.POS_S_Align(0, os);
        POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

        if (info.getType() == 1) {
            setingPrint(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔" + "\n", os);
            setingPrint(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔" + "\n", os);
        } else if (info.getType() == 2) {
            setingPrint(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_consume_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔" + "\n", os);
            setingPrint(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔" + "\n", os);

            //setingPrint(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n", os);
            //setingPrint(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔" + "\n", os);

            //setingPrint(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次" + "\n", os);
            //setingPrint(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元\n", os);
            //setingPrint(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔" + "\n", os);
        } else if (info.getType() == 3) {
            setingPrint(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "\n" : info.getSettleRate() + "\n", os);
            setingPrint(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元\n", os);
            setingPrint(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔" + "\n", os);
            setingPrint(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔" + "\n", os);
        }

        if (info.getType() == 1) {
            List<ReportBean.DataEntity.ListEntity> list = info.getList();
            if (list != null && list.size() > 0) {
                POS_S_TextOut(UIUtils.getString(R.string.print_trade_type_list_title) + ": \n", "gbk", 0, 0, 0, 0, 0, os);
                POS_S_TextOut(UIUtils.getString(R.string.print_single_horizontal) + "\n", "gbk", 0, 0, 0, 0, 0, os);
                for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                    setingPrint(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元\n", os);
                    setingPrint(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔" + "\n", os);
                }
            }
        }

        POS_S_TextOut(UIUtils.getString(R.string.print_single_horizontal) + "\n", "gbk", 0, 0, 0, 0, 0, os);
        POS_S_TextOut(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", "gbk", 0, 0, 0, 0, 0, os);
        POS_S_TextOut(UIUtils.getString(R.string.print_client_sign_title) + "：\n\n\n\n\n", "gbk", 0, 0, 0, 0, 0, os);
        BluePrintUtil.POS_S_Align(1, os);
        POS_S_TextOut("\n" + "手动打印" + "\n\n\n\n\n", "gbk", 0, 0, 0, 0, 0, os);

    }

    /**
     * 预授权报表(押金统计)
     *
     * @param info
     */
    public static void printPledgeReportSum(PledgeReportBean.DataBean info) {
        if (MyApplication.bluetoothSocket == null)
            return;
        DataOutputStream os = null;
        try {
            os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());
            if (os == null) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuffer printBuffer = new StringBuffer();
        StringBuffer titlebuffer = new StringBuffer();
        String merChantName = TextUtils.isEmpty(MyApplication.getMerchantShortName()) ? MyApplication.getMerchantName() : MyApplication.getMerchantShortName();
        titlebuffer.append("\n" + merChantName + "\n");
        titlebuffer.append("\n" + UIUtils.getString(R.string.print_title_pledge_report) + "\n\n");
        BluePrintUtil.POS_S_Align(1, os);
        POS_S_TextOut(titlebuffer.toString(), "gbk", 0, 1, 1, 0, 0, os);
        /*if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                printBuffer.append(one + "\n");
                String two = sbf.substring(16, str.length());
                printBuffer.append(two + "\n");
            } else if (MyApplication.getMerchantName().length() > 11) {
                printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                printBuffer.append(MyApplication.getMerchantName() + "\n");
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
            }
        }*/
        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            printBuffer.append(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
        }
        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "全部门店" + "\n");
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "\n");
                printBuffer.append(one + "\n");
                String two = sbf.substring(16, str.length());
                printBuffer.append(two + "\n");
            } else if (info.getStoreName().length() > 11) {
                printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "\n");
                printBuffer.append(info.getStoreName() + "\n");
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + info.getStoreName() + "\n");
            }
        }
        if (!TextUtils.isEmpty(info.getStoreMerchantId())) {
            printBuffer.append(ToastHelper.toStr(R.string.store_code_title) + "：" + info.getStoreMerchantId() + "\n");
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (!StringUtils.isEmptyOrNull(info.getCashierName())) {
            if (info.getCashierName().length() > 15) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 15);
                printBuffer.append(cashierTitle + "：" + "\n");
                printBuffer.append(one + "\n");
                String two = sbf.substring(15, str.length());
                printBuffer.append(two + "\r\n");
            } else if (info.getCashierName().length() > 10) {
                printBuffer.append(cashierTitle + "：" + "\n");
                printBuffer.append(info.getCashierName() + "\n");
            } else {
                printBuffer.append(cashierTitle + "：" + info.getCashierName() + "\n");
            }
        }

        printBuffer.append(UIUtils.getString(R.string.print_start_time_title) + "：" + info.getStartTime() + "\n");

        printBuffer.append(UIUtils.getString(R.string.print_end_time_title) + "：" + info.getEndTime() + "\n");

        printBuffer.append(UIUtils.getString(R.string.print_double_horizontal) + "\n");
        BluePrintUtil.POS_S_Align(0, os);
        POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

        setingPrint(UIUtils.getString(R.string.print_pledge_money_title) + "：", DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元\n", os);
        setingPrint(UIUtils.getString(R.string.print_pledge_count_title) + "：", info.getCntPreMoney() + "笔\n", os);
        setingPrint(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：", DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元\n", os);
        setingPrint(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：", info.getCntPayMoney() + "笔" + "\n", os);
        setingPrint(UIUtils.getString(R.string.print_refund_money) + "：", DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元\n", os);
        setingPrint(UIUtils.getString(R.string.print_refund_count) + "：", info.getCntFreeMoney() + "笔" + "\n", os);
//        setingPrint(UIUtils.getString(R.string.print_pledge_no_freeze_money_title) + "：", DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元\n", os);

        POS_S_TextOut(UIUtils.getString(R.string.print_single_horizontal) + "\n", "gbk", 0, 0, 0, 0, 0, os);
        POS_S_TextOut(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", "gbk", 0, 0, 0, 0, 0, os);
        POS_S_TextOut(UIUtils.getString(R.string.print_client_sign_title) + "：\n\n\n\n\n", "gbk", 0, 0, 0, 0, 0, os);
        BluePrintUtil.POS_S_Align(1, os);
        POS_S_TextOut("\n" + "手动打印" + "\n\n\n\n\n", "gbk", 0, 0, 0, 0, 0, os);

    }

    /**
     * 打印两列
     *
     * @param leftText  左侧文字
     * @param rightText 右侧文字
     * @return
     */
    @SuppressLint("NewApi")
    public static String printTwoData(String leftText, String rightText) {
        StringBuilder sb = new StringBuilder();
        int leftTextLength = getBytesLength(leftText);
        int rightTextLength = getBytesLength(rightText);
        sb.append(leftText);

        // 计算两侧文字中间的空格
        int marginBetweenMiddleAndRight = LINE_BYTE_SIZE - leftTextLength - rightTextLength;

        for (int i = 0; i < marginBetweenMiddleAndRight; i++) {
            sb.append(" ");
        }
        sb.append(rightText);
        return sb.toString();
    }

    /**
     * 打印设置
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    static void setingPrint(String leftText, String rightText, DataOutputStream os) {
        POS_S_TextOut(printTwoData(leftText, rightText), "gbk", 0, 0, 0, 0, 0, os);
    }

    /**
     * 获取数据长度
     *
     * @param msg
     * @return
     */
    private static int getBytesLength(String msg) {
        return msg.getBytes(Charset.forName("GBK")).length;
    }

    /**
     * 打印设置 对齐方式
     * <功能详细描述>
     * param bluetooth
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void POS_S_Align(int align, DataOutputStream os) {
        if (align < 0 || align > 2)
            return;
        byte[] data = Cmd.ESCCmd.ESC_a_n;
        data[2] = (byte) align;
        write(data, 0, data.length, os);
    }

    /*@SuppressLint("NewApi")
    public static boolean blueConnent(String bluetooth) {
        try {
            BluetoothDevice btDev = null;
            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBtAdapter != null) {
                if (!mBtAdapter.isEnabled()) {//蓝牙没有打开
                    return false;
                }

                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        if (device.getAddress().equalsIgnoreCase(bluetooth)) {
                            btDev = device;
                        }
                    }
                } else {
                    return false;
                }

                if (btDev != null) {
                    BluetoothSocket bluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
                    if (bluetoothSocket != null) {
                        MyApplication.bluetoothSocket = bluetoothSocket;
                    }

                    if (mBtAdapter.isDiscovering())
                        //停止搜索
                        mBtAdapter.cancelDiscovery();
                    //如果当前socket处于非连接状态则调用连接
//                    if (!bluetoothSocket.isConnected()) {
                    //你应当确保在调用connect()时设备没有执行搜索设备的操作。
                    // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                    bluetoothSocket.close();
                    try {
                        bluetoothSocket.connect();

                            *//*MyApplication.setBlueDeviceName(btDev.getName());
                            MyApplication.setBlueDeviceNameAddress(btDev.getAddress());
                            MyApplication.setBlueState(true);*//*
                    } catch (Exception e) {
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastHelper.showInfo(ToastHelper.toStr(R.string.tx_blue_conn_fail));
                            }
                        });
                        Log.e("zw", "connet fail " + e);
                        return false;
                    }
//                    }
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

        return true;
    }*/

    /**
     * 连接蓝牙
     * 子线程中调用
     */
    @SuppressLint("NewApi")
    public static boolean blueConnent(int type, final Activity context) {
        try {
            BluetoothDevice btDev = null;
            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBtAdapter != null) {//检查是否打开蓝牙
                if (!mBtAdapter.isEnabled()) {
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SelectDialog selectDialog = new SelectDialog(context, UIUtils.getString(R.string.tx_blue_no_open)
                                    , UIUtils.getString(R.string.btnCancel), UIUtils.getString(R.string.to_open), R.layout.select_common_dialog);
                            selectDialog.setOnClickOkListener(new SelectDialog.OnClickOkListener() {
                                @Override
                                public void clickOk() {
                                    Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                    context.startActivityForResult(mIntent, 1);
                                }
                            });
                            DialogHelper.resize(context, selectDialog);
                            selectDialog.show();
                        }
                    });
                    return false;
                }
                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        if (device.getAddress().equalsIgnoreCase(MyApplication.getBluetoothAddress(type))) {
                            btDev = device;
                            break;
                        }
                    }
                } else {
                    return false;
                }
                if (btDev != null) {
                    BluetoothSocket bluetoothSocket = null;
                    switch (type) {
                        case 1:
                            if (MyApplication.bluetoothSocket == null && btDev.createRfcommSocketToServiceRecord(SPP_UUID) != null) {
                                MyApplication.bluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);

                            }
                            bluetoothSocket = MyApplication.bluetoothSocket;
                            break;
                        case 2:
                            if (MyApplication.bluetoothSocket2 == null && btDev.createRfcommSocketToServiceRecord(SPP_UUID) != null) {
                                MyApplication.bluetoothSocket2 = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
                            }
                            bluetoothSocket = MyApplication.bluetoothSocket2;
                            break;
                        case 3:
                            if (MyApplication.bluetoothSocket3 == null && btDev.createRfcommSocketToServiceRecord(SPP_UUID) != null) {
                                MyApplication.bluetoothSocket3 = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
                            }
                            bluetoothSocket = MyApplication.bluetoothSocket3;
                            break;
                    }
                    /*BluetoothSocket bluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
                    if (bluetoothSocket != null) {
                        MyApplication.bluetoothSocket = bluetoothSocket;
                    }*/

                    if (mBtAdapter.isDiscovering())
                        //停止搜索
                        mBtAdapter.cancelDiscovery();
                    //如果当前socket处于非连接状态则调用连接
                    if (!bluetoothSocket.isConnected()) {
                        //你应当确保在调用connect()时设备没有执行搜索设备的操作。
                        // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                        try {

                            if (context instanceof BaseActivity) {
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((BaseActivity) context).showNewLoading(true, ToastHelper.toStr(R.string.tx_blue_conn_deviceings));
                                    }
                                });
                            } else {
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MyToast.showToastShort(ToastHelper.toStr(R.string.tx_blue_conn_deviceings));
                                    }
                                });
                            }
                            //连接蓝牙，耗时操作
                            bluetoothSocket.connect();

                            if (context instanceof BaseActivity) {
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((BaseActivity) context).dismissLoading();
                                    }
                                });
                            }
                            return true;
                            /*MyApplication.setBlueDeviceName(btDev.getName());
                            MyApplication.setBlueDeviceNameAddress(btDev.getAddress());
                            MyApplication.setBlueState(true);*/
                        } catch (Exception e) {
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MyToast.showToastShort(ToastHelper.toStr(R.string.tx_blue_conn_fail));
                                    if (context instanceof BaseActivity) {
                                        ((BaseActivity) context).dismissLoading();
                                    }
                                }
                            });
                            Log.e("zw", "connet fail " + e);
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * <功能详细描述>
     * 蓝牙开始打印收款退款交易详情信息
     *
     * @param isAutoPrint 是否是自动打印
     * @see [类、类#方法、类#成员]
     * <p>
     * <p>
     * param billdata
     */
    public static void print(TradeDetailBean order, boolean isAutoPrint) {
        if (MyApplication.bluetoothSocket == null)
            return;
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());
            StringBuffer printBuffer = new StringBuffer();
            StringBuffer buffer = new StringBuffer();
            StringBuffer printCode = new StringBuffer();
            StringBuffer printEnd = new StringBuffer();

            //标题
            String merChantName = TextUtils.isEmpty(MyApplication.getMerchantShortName()) ? MyApplication.getMerchantName() : MyApplication.getMerchantShortName();
            if (!StringUtils.isEmptyOrNull(merChantName)) {
                buffer.append("\n" + merChantName + "\n");
                POS_S_Align(1, os);
                POS_S_TextOut(buffer.toString(), "gbk", 0, 1, 1, 0, 0, os);
            } else {
                if (order.getTradeType() == 1) {
                    buffer.append("\n" + UIUtils.getString(R.string.print_recharge_title) + "\n");
                } else if (order.getTradeType() == 2) {
                    buffer.append("\n" + UIUtils.getString(R.string.print_verification_title) + "\n");
                } else {
                    if (!order.isPay()) {
                        buffer.append("\n" + UIUtils.getString(R.string.print_refund_title) + "\n");
                    } else {
                        buffer.append("\n" + UIUtils.getString(R.string.print_pay_title) + "\n");
                    }
                }
                POS_S_Align(1, os);
                POS_S_TextOut(buffer.toString(), "gbk", 0, 1, 1, 0, 0, os);
            }
            printBuffer.append("\n" + order.getPartner() + "             请妥善保存");
            printBuffer.append("\n" + UIUtils.getString(R.string.print_double_horizontal) + "\n");
            POS_S_Align(0, os);
            /*if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                    printBuffer.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    printBuffer.append(two + "\r\n");
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                    printBuffer.append(MyApplication.getMerchantName() + "\n");
                } else {
                    printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                }
            }*/

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printBuffer.append(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
            }
            String storeNameTitle = ToastHelper.toStr(R.string.trade_store_name);
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printBuffer.append(storeNameTitle + "：" + "\n");
                    printBuffer.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    printBuffer.append(two + "\r\n");
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    printBuffer.append(storeNameTitle + "：" + "\n");
                    printBuffer.append(order.getStoreMerchantIdCnt() + "\n");
                } else {
                    printBuffer.append(storeNameTitle + "：" + order.getStoreMerchantIdCnt() + "\n");
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                if (order.getCashPointName().length() > 16) {
                    String str = order.getCashPointName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printBuffer.append("收银点: " + "\n");
                    printBuffer.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    printBuffer.append(two + "\r\n");
                } else if (order.getCashPointName().length() > 11) {
                    printBuffer.append("收银点: " + "\n");
                    printBuffer.append(order.getCashPointName() + "\n");
                } else {
                    printBuffer.append("收银点: " + order.getCashPointName() + "\n");
                }
            }
            /*if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                printBuffer.append(UIUtils.getString(R.string.tv_print_order_no) + "：\n");
                printBuffer.append(order.getOrderNo() + "\n");
            }

            if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                printBuffer.append(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + "：\n");
                printBuffer.append(order.getTransactionId() + "\n");
                printBuffer.append("支付方式：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n");
            }*/

            if (order.getTradeType() == 1) {
                printBuffer.append(UIUtils.getString(R.string.print_trade_model_title) + ":" + "充值" + "\n");
            } else if (order.getTradeType() == 2) {
                printBuffer.append(UIUtils.getString(R.string.print_trade_model_title) + ":" + "核销" + "\n");
            } else {
                if (!order.isPay()) {
                    printBuffer.append(UIUtils.getString(R.string.print_trade_model_title) + ":" + "退款" + "\n");
                } else {
                    printBuffer.append(UIUtils.getString(R.string.print_trade_model_title) + ":" + "收款" + "\n");
                }
            }

            if (!order.isPay()) {
                printBuffer.append(UIUtils.getString(R.string.print_refund_no_title) + ": \n");
                printBuffer.append(order.getRefundNo() + "\n");
                //退款
                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        printBuffer.append(refundUser + ": \n");
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        printBuffer.append(user + "\r\n");
                        String printuser = sbf.substring(16, order.getRefundUser().length());
                        printBuffer.append(printuser + "\r\n");
                    } else if (order.getRefundUser().length() > 12) {
                        printBuffer.append(refundUser + ": \n");
                        printBuffer.append(order.getRefundUser() + "\n");
                    } else {
                        printBuffer.append(refundUser + ": " + order.getRefundUser() + "\n");
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        printBuffer.append(refundUser + ": \n");
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        printBuffer.append(user + "\r\n");
                        String printUser = sbf.substring(16, order.getRefundUserRealName().length());
                        printBuffer.append(printUser + "\r\n");
                    } else if (order.getRefundUserRealName().length() > 12) {
                        printBuffer.append(refundUser + ": \n");
                        printBuffer.append(order.getRefundUserRealName() + "\n");
                    } else {
                        printBuffer.append(refundUser + ": " + order.getRefundUserRealName() + "\n");
                    }
                }

                /*printBuffer.append(UIUtils.getString(R.string.print_refund_no_title) + ": \n");
                printBuffer.append(order.getRefundNo() + "\n");*/

                printBuffer.append(UIUtils.getString(R.string.print_refund_time_title) + ": \n");
                printBuffer.append(order.getRefundTime() + "\n");

                printBuffer.append(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()) + "\n");
                if (order.getRefundMoney() > 0) {
                    printBuffer.append(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元\n");
                }
                printBuffer.append(UIUtils.getString(R.string.print_refund_instruction) + "\n");
            } else {
                //收款
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);//收银人员
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        printBuffer.append(cashierTitle + ": \n");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String oneuser = sbf.substring(0, 16);
                        printBuffer.append(oneuser + "\n");
                        String twouser = sbf.substring(16, order.getCashierName().length());
                        printBuffer.append(twouser + "\n");
                    } else if (order.getCashierName().length() > 12) {
                        printBuffer.append(cashierTitle + ": \n");
                        printBuffer.append(order.getCashierName() + "\n");
                    } else {
                        printBuffer.append(cashierTitle + ": " + order.getCashierName() + "\n");
                    }

                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        printBuffer.append(cashierTitle + ": \n");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneuser = sbf.substring(0, 16);
                        printBuffer.append(oneuser + "\n");
                        String twouser = sbf.substring(16, order.getOpUserRealName().length());
                        printBuffer.append(twouser + "\n");
                    } else if (order.getOpUserRealName().length() > 12) {
                        printBuffer.append(cashierTitle + ": \n");
                        printBuffer.append(order.getOpUserRealName() + "\n");
                    } else {
                        printBuffer.append(cashierTitle + ": " + order.getOpUserRealName() + "\n");
                    }
                }

                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    printBuffer.append(UIUtils.getString(R.string.tv_print_order_no) + ":\n");
                    printBuffer.append(order.getOrderNo() + "\n");
                }

                if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                    //printBuffer.append(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + "：\n");
                    printBuffer.append(UIUtils.getString(R.string.tv_print_order_no_trade) + ":\n");
                    printBuffer.append(order.getTransactionId() + "\n");
                }

                printBuffer.append(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()) + "\n");
                printBuffer.append(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n");
                try {
                    printBuffer.append(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeTime() + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //printBuffer.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                printBuffer.append(UIUtils.getString(R.string.print_order_money_title_new) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元\n");
                printBuffer.append(UIUtils.getString(R.string.print_coupon_money_title_new) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元\n");
                printBuffer.append(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元\n");
            }
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                printBuffer.append(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach() + "\n");
            }
            printBuffer.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
            //=========
            printBuffer.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
            if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
                printBuffer.append(UIUtils.getString(R.string.print_client_sign_title) + "：\n");
            }

            printBuffer.append("\n\n\n");
            BluePrintUtil.POS_S_Align(0, os);
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);


            if (!AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY).contains(Constants.WJY)) {
                if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                    String printNo;
                    if (ConfigUtil.printCodeBillEnable()) {
                        printNo = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                    } else {
                        printNo = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                    }
                    POS_EPSON_SetQRCodeV2(printNo, 5, 7, 4);
                }
            }
            if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printCode.append("\n" + UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        printCode.append(title + "\n");
                    }
                    printCode.append(" \n");
                    BluePrintUtil.POS_S_Align(0, os);
                    POS_S_TextOut(printCode.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    POS_EPSON_SetQRCodeV2(activeUrl, 5, 7, 4);
                    printEnd.append("\n");
                }
            }

            printEnd.append("\n");
            BluePrintUtil.POS_S_Align(0, os);
            POS_S_TextOut(printEnd.toString(), "gbk", 0, 0, 0, 0, 0, os);

            BluePrintUtil.POS_S_Align(1, os);
            if (isAutoPrint) {
                POS_S_TextOut("\n" + "自动打印" + "\n", "gbk", 0, 0, 0, 0, 0, os);
            } else {
                POS_S_TextOut("\n" + "手动打印" + "\n", "gbk", 0, 0, 0, 0, 0, os);
            }
            BluePrintUtil.POS_S_Align(0, os);
            POS_S_TextOut("\n\n\n", "gbk", 0, 0, 0, 0, 0, os);


        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /***
     * 蓝牙开始打印预授权押金收款，押金解冻的交易详情信息
     * */
    public static void pledgePrint(PledgePayBean.DataBean order, boolean isAutoPrint) {
        if (MyApplication.bluetoothSocket == null)
            return;
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());
            StringBuffer printBuffer = new StringBuffer();
            StringBuffer buffer = new StringBuffer();
            StringBuffer printCode = new StringBuffer();
            StringBuffer printEnd = new StringBuffer();

            /*if (order.isToPay()) {
                buffer.append("\n" + UIUtils.getString(R.string.print_title_pledge_pay) + "\n");
            } else {
                buffer.append("\n" + UIUtils.getString(R.string.print_title_pledge) + "\n");
            }*/

            //标题
            String merChantName = TextUtils.isEmpty(MyApplication.getMerchantShortName()) ? MyApplication.getMerchantName() : MyApplication.getMerchantShortName();
            if (!StringUtils.isEmptyOrNull(merChantName)) {
                buffer.append("\n" + merChantName + "\n");
                POS_S_Align(1, os);
                POS_S_TextOut(buffer.toString(), "gbk", 0, 1, 1, 0, 0, os);
            } else {
                if (order.isToPay()) {
                    buffer.append("\n" + UIUtils.getString(R.string.print_title_pledge_pay) + "\n");
                } else {
                    buffer.append("\n" + UIUtils.getString(R.string.print_title_pledge) + "\n");
                }
                POS_S_Align(1, os);
                POS_S_TextOut(buffer.toString(), "gbk", 0, 1, 1, 0, 0, os);
            }
            printBuffer.append("\n" + order.getPartner() + "             请妥善保存");
            printBuffer.append("\n" + UIUtils.getString(R.string.print_double_horizontal) + "\n");
            POS_S_Align(0, os);
            /*if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                    printBuffer.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    printBuffer.append(two + "\r\n");
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                    printBuffer.append(MyApplication.getMerchantName() + "\n");
                } else {
                    printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                }
            }*/

            //商户编号
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printBuffer.append(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
            }
            //交易门店
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + "：" + "\n");
                    printBuffer.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    printBuffer.append(two + "\r\n");
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + "：" + "\n");
                    printBuffer.append(order.getStoreMerchantIdCnt() + "\n");
                } else {
                    printBuffer.append(ToastHelper.toStr(R.string.trade_store_name) + "：" + order.getStoreMerchantIdCnt() + "\n");
                }
            }
            /*if (!TextUtils.isEmpty(order.getStoreMerchantId())) {
                printBuffer.append(ToastHelper.toStr(R.string.store_code_title) + "：" + order.getStoreMerchantId() + "\n");
            }*/

            //交易类型
            if (order.isToPay()) {
                printBuffer.append(UIUtils.getString(R.string.print_trade_model_title) + "：" + UIUtils.getString(R.string.type_pledge_consume) + "\n");
            } else {
                printBuffer.append(UIUtils.getString(R.string.print_trade_model_title) + "：" + UIUtils.getString(R.string.type_pledge_income) + "\n");
            }

            //收银人员、解冻人员
            String cashierTitle;
            if (order.isToPay()) {
                cashierTitle = UIUtils.getString(R.string.print_consumer_title);
            } else {
                cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            }
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 12) {
                    printBuffer.append(cashierTitle + ": " + "\n");
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String oneUser = sbf.substring(0, 12);
                    printBuffer.append(oneUser + "\n");
                    String twoUser = sbf.substring(12, order.getCashierName().length());
                    printBuffer.append(twoUser + "\n");
                }
                if (order.getCashierName().length() > 12) {
                    printBuffer.append(cashierTitle + ": " + "\n");
                    printBuffer.append(order.getCashierName() + "\n");
                } else {
                    printBuffer.append(cashierTitle + ": " + order.getCashierName() + "\n");
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                String opUserTitle;
                if (order.isToPay()) {
                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                } else {
                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                }
                if (order.getOpUserRealName().length() > 12) {
                    printBuffer.append(opUserTitle + "\n");
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String oneUser = sbf.substring(0, 12);
                    printBuffer.append(oneUser + "\n");
                    String twoUser = sbf.substring(12, order.getOpUserRealName().length());
                    printBuffer.append(twoUser + "\n");
                }
                if (order.getOpUserRealName().length() > 12) {
                    printBuffer.append(opUserTitle + "\n");
                    printBuffer.append(order.getOpUserRealName() + "\n");
                } else {
                    printBuffer.append(opUserTitle + order.getOpUserRealName() + "\n");
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                printBuffer.append(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()) + "\n");
            }
            //交易订单号
            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                //printBuffer.append(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：" + "\n");
                printBuffer.append(UIUtils.getString(R.string.tv_print_order_no_trade) + "：" + "\n");
                printBuffer.append(order.getOutTransactionId() + "\n");
            }
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                printBuffer.append(UIUtils.getString(R.string.tv_pledge_order_no) + ": " + "\n");
                printBuffer.append(order.getAuthNo() + "\n");
            }

            //交易状态
            printBuffer.append(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()) + "\n");

            if (!order.isToPay()) {
                //支付方式
                printBuffer.append(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n");
            }

            //交易时间
            try {
                printBuffer.append(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime() + "\n");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!order.isToPay()) {
                if (!TextUtils.isEmpty(order.getMoney())) {
                    printBuffer.append(UIUtils.getString(R.string.print_pledge_freeze_money_title_new) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元" + "\n");
                } else {
                    printBuffer.append(UIUtils.getString(R.string.print_pledge_freeze_money_title_new) + ": 0.00元" + "\n");
                }
            }
            if (order.isToPay()) {
                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                    printBuffer.append(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元" + "\n");
                } else {
                    printBuffer.append(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元" + "\n");
                }
                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                    printBuffer.append(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元" + "\n");
                } else {
                    printBuffer.append(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元" + "\n");
                }
            }

            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach()) && !order.isToPay()) {
                printBuffer.append(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach() + "\n");
            }

            if (order.isToPay()) {
                printBuffer.append(UIUtils.getString(R.string.print_refund_instruction) + "\n");
            }

            //=========
            printBuffer.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
            printBuffer.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
            if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
                printBuffer.append(UIUtils.getString(R.string.print_client_sign_title) + "：\n");
            }
            printBuffer.append("\n\n\n");
            BluePrintUtil.POS_S_Align(0, os);
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
            if (ConfigUtil.printCodeBillEnable()) {
                POS_EPSON_SetQRCodeV2(Constants.URL_PRINT_QRCODE + order.getAuthNo(), 5, 7, 4);
            } else {
                POS_EPSON_SetQRCodeV2(order.getAuthNo(), 5, 7, 4);
            }
            if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printCode.append("\n" + UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        printCode.append(title + "\n");
                    }
                    printCode.append(" \n");
                    BluePrintUtil.POS_S_Align(0, os);
                    POS_S_TextOut(printCode.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    POS_EPSON_SetQRCodeV2(activeUrl, 5, 7, 4);
                    printEnd.append("\n");
                }
            }
            if (!order.isToPay()) {
                printEnd.append("\n-------------------------------");
                printEnd.append("\n" + UIUtils.getString(R.string.print_pledge_notice));
            }

            printEnd.append("\n");
            BluePrintUtil.POS_S_Align(0, os);
            POS_S_TextOut(printEnd.toString(), "gbk", 0, 0, 0, 0, 0, os);

            BluePrintUtil.POS_S_Align(1, os);
            if (isAutoPrint) {
                POS_S_TextOut("\n" + "自动打印" + "\n", "gbk", 0, 0, 0, 0, 0, os);
            } else {
                POS_S_TextOut("\n" + "手动打印" + "\n", "gbk", 0, 0, 0, 0, 0, os);
            }
            BluePrintUtil.POS_S_Align(0, os);
            POS_S_TextOut("\n\n\n", "gbk", 0, 0, 0, 0, 0, os);

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void payOrderPrint(PrintContentBean bean, boolean isClient) {
        if (MyApplication.bluetoothSocket2 == null) {
            return;
        }
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket2.getOutputStream());
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(1));
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(2));
            PrintFormatUtils.printText(os, bean.getStoreName() + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(0));
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_scan_order_title) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(0));
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_desk_no_title), bean.getTableCode() + "\n", 10));
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_trade_time_title), bean.getPayTime() + "\n", 10));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_trade_no_title) + "\n");
            PrintFormatUtils.printText(os, bean.getOrderId() + "\n");

            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
            PrintFormatUtils.printText(os, PrintFormatUtils.printThreeData(
                    UIUtils.getString(R.string.print_order_dish_name_title),
                    UIUtils.getString(R.string.print_order_dish_count_title),
                    UIUtils.getString(R.string.print_order_dish_name_title) + "\n"));
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
            if (bean.getOrderGoods() != null && bean.getOrderGoods().size() > 0) {
                for (int i = 0; i < bean.getOrderGoods().size(); i++) {
                    PrintContentBean.OrderGoodsListBean dish = bean.getOrderGoods().get(i);
                    if (!TextUtils.isEmpty(dish.getGoodsName())) {
                        PrintFormatUtils.printText(os, PrintFormatUtils.printThreeData(dish.getGoodsName(), dish.getGoodsCount() + "", DateUtil.formatMoneyByLong(dish.getGoodsPrice()) + "元\n"));
                    }
                }
            }

            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_discount_title), DateUtil.formatMoneyByLong(bean.getDiscountAmt()) + "元\n"));
            if (bean.getEatSite() == 1) {
                PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_mode_title), UIUtils.getString(R.string.print_order_mode_here) + "\n"));
            } else if (bean.getEatSite() == 2) {
                PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_mode_title), UIUtils.getString(R.string.print_order_mode_go) + "\n"));
            }
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_trade_type_title), bean.getPayType() + "\n"));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(2));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_pay_money_title));
            PrintFormatUtils.printText(os, DateUtil.formatMoneyByLong(bean.getOrderAmt()) + "元\n");
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_real_money_title));
            PrintFormatUtils.printText(os, DateUtil.formatMoneyByLong(bean.getRealAmt()) + "元\n");

            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(0));
            if (!TextUtils.isEmpty(bean.getRemark()) || !TextUtils.isEmpty(bean.getUserPhone())) {
                PrintFormatUtils.printText(os, "-------------------------------\n");
                if (!TextUtils.isEmpty(bean.getUserPhone())) {
                    PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
                    PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData("取餐信息：", bean.getUserPhone() + "\n"));
                    PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
                }
                if (!TextUtils.isEmpty(bean.getRemark())) {
                    PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
                    PrintFormatUtils.printText(os, "备注：\n");
                    PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
                    PrintFormatUtils.printText(os, bean.getRemark() + "\n");
                }
            }

            if (isClient) {
                PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
                PrintFormatUtils.printText(os, "\n");
                PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(1));
                PrintFormatUtils.printText(os, bean.getStoreName() + UIUtils.getString(R.string.print_order_welcome) + "\n");
            }
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.printText(os, "\n");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void kitchenPrint(PrintContentBean bean) {
        if (MyApplication.bluetoothSocket3 == null) {
            return;
        }
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket3.getOutputStream());
            List<PrintContentBean.OrderGoodsListBean> dishes = bean.getOrderGoods();
            if (dishes != null && dishes.size() > 0) {
                for (int i = 0; i < dishes.size(); i++) {
                    if (MyApplication.isPrintKitchenCount()) {
                        kitchenDishPrint(bean, os, i, dishes.get(i).getGoodsCount());
                    } else {
                        for (int count = dishes.get(i).getGoodsCount(); count > 0; count--) {
                            kitchenDishPrint(bean, os, i, 1);
                        }
                    }
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private static void kitchenDishPrint(PrintContentBean bean, DataOutputStream os, int i, int dishCount) {
        PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(1));
        PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(2));
        PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_kitchen_order_title) + "\n");
        PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(0));
        PrintFormatUtils.printText(os, "\n");
        PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(0));
        PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_desk_no_title), bean.getTableCode() + "\n", 10));
        PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_trade_time_title), bean.getPayTime() + "\n", 10));

        PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
        PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(
                UIUtils.getString(R.string.print_order_dish_name_title),
                UIUtils.getString(R.string.print_order_dish_count_title) + "\n"));
        if (!TextUtils.isEmpty(bean.getOrderGoods().get(i).getGoodsName())) {
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(1));
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(bean.getOrderGoods().get(i).getGoodsName(), dishCount + "\n"));
            if (!TextUtils.isEmpty(bean.getOrderGoods().get(i).getRemarks())) {
                PrintFormatUtils.printText(os, "(" + bean.getOrderGoods().get(i).getRemarks() + ")" + "\n");
            }
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(0));
        }

        PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
        if (bean.getEatSite() == 1) {
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_mode_title), UIUtils.getString(R.string.print_order_mode_here) + "\n"));
        } else if (bean.getEatSite() == 2) {
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_mode_title), UIUtils.getString(R.string.print_order_mode_go) + "\n"));
        }

        PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(0));
        if (!TextUtils.isEmpty(bean.getRemark())) {
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_remark_title) + "：\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
            PrintFormatUtils.printText(os, bean.getRemark() + "\n");
        }
        PrintFormatUtils.printText(os, "\n");
        PrintFormatUtils.printText(os, "\n");
        PrintFormatUtils.printText(os, "\n");
    }

    // nFontType 0 标准 1 压缩 其他不指定
    public static void POS_S_TextOut(String pszString, String encoding, int nOrgx, int nWidthTimes, int nHeightTimes,
                                     int nFontType, int nFontStyle, DataOutputStream os) {
        if (MyApplication.bluetoothSocket == null)
            return;
        if (nOrgx > 65535 | nOrgx < 0 | nWidthTimes > 7 | nWidthTimes < 0 | nHeightTimes > 7 | nHeightTimes < 0
                | nFontType < 0 | nFontType > 4 | (pszString.length() == 0))
            return;

        Cmd.ESCCmd.ESC_dollors_nL_nH[2] = (byte) (nOrgx % 0x100);
        Cmd.ESCCmd.ESC_dollors_nL_nH[3] = (byte) (nOrgx / 0x100);

        byte[] intToWidth = {0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70};
        byte[] intToHeight = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
        Cmd.ESCCmd.GS_exclamationmark_n[2] = (byte) (intToWidth[nWidthTimes] + intToHeight[nHeightTimes]);

        byte[] tmp_ESC_M_n = Cmd.ESCCmd.ESC_M_n;
        if ((nFontType == 0) || (nFontType == 1))
            tmp_ESC_M_n[2] = (byte) nFontType;
        else
            tmp_ESC_M_n = new byte[0];

        // 字体风格
        // 暂不支持平滑处理
        Cmd.ESCCmd.GS_E_n[2] = (byte) ((nFontStyle >> 3) & 0x01);

        Cmd.ESCCmd.ESC_line_n[2] = (byte) ((nFontStyle >> 7) & 0x03);
        Cmd.ESCCmd.FS_line_n[2] = (byte) ((nFontStyle >> 7) & 0x03);

        Cmd.ESCCmd.ESC_lbracket_n[2] = (byte) ((nFontStyle >> 9) & 0x01);

        Cmd.ESCCmd.GS_B_n[2] = (byte) ((nFontStyle >> 10) & 0x01);

        Cmd.ESCCmd.ESC_V_n[2] = (byte) ((nFontStyle >> 12) & 0x01);

        byte[] pbString = null;
        try {
            pbString = pszString.getBytes(encoding);
        } catch (UnsupportedEncodingException e) {
            return;
        }

        byte[] data =
                byteArraysToBytes(new byte[][]{Cmd.ESCCmd.ESC_dollors_nL_nH, Cmd.ESCCmd.GS_exclamationmark_n, tmp_ESC_M_n,
                        Cmd.ESCCmd.GS_E_n, Cmd.ESCCmd.ESC_line_n, Cmd.ESCCmd.FS_line_n, Cmd.ESCCmd.ESC_lbracket_n,
                        Cmd.ESCCmd.GS_B_n, Cmd.ESCCmd.ESC_V_n, pbString});

        write(data, 0, data.length, os);

    }

    public static void POS_EPSON_SetQRCodeV2(String strCodedata, int nWidthX, int nVersion,
                                             int nErrorCorrectionLevel) {

        if (MyApplication.bluetoothSocket == null || StringUtils.isEmptyOrNull(strCodedata))
            return;
        if (nWidthX < 2 | nWidthX > 6 | nErrorCorrectionLevel < 1
                | nErrorCorrectionLevel > 4)
            return;

        byte[] bCodeData = null;
        DataOutputStream os = null;
        try {
            os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bCodeData = strCodedata.getBytes("GBK");
        } catch (UnsupportedEncodingException e) {
            return;
        }
        ;

        Cmd.ESCCmd.GS_w_n[2] = (byte) nWidthX;
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_67_n[7] = (byte) nVersion;
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_69_n[7] = (byte) (47 + nErrorCorrectionLevel);
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_80_m__d1dk[3] = (byte) ((bCodeData.length + 3) & 0xff);
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_80_m__d1dk[4] = (byte) (((bCodeData.length + 3) & 0xff00) >> 8);

        byte[] data = byteArraysToBytes(new byte[][]{
                Cmd.ESCCmd.GS_w_n, Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_67_n,
                Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_69_n,
                Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_80_m__d1dk, bCodeData,
                Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_fn_m});
        // POS_SetMotionUnit(100,50,os);
        BluePrintUtil.POS_S_Align(1, os);
        write(data, 0, data.length, os);

    }

    //打印条码
    @SuppressLint("NewApi")
    public static void printCode(String qrcode) {
        if (MyApplication.bluetoothSocket == null || StringUtils.isEmptyOrNull(qrcode))
            return;
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());

            byte[] bCodeData = null;
            try {
                bCodeData = qrcode.getBytes("GBK");
            } catch (UnsupportedEncodingException e) {
                return;
            }

            Cmd.ESCCmd.GS_w_n[2] = (byte) 6;
            //Cmd.ESCCmd.ESC_a_n[2]=(byte)1;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[3] = (byte) 10;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[4] = (byte) 4;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[5] = (byte) (bCodeData.length & 0xff);
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[6] = (byte) ((bCodeData.length & 0xff00) >> 8);
            // POS_S_Align(1,os);
            byte[] data = byteArraysToBytes(new byte[][]{Cmd.ESCCmd.GS_w_n, Cmd.ESCCmd.GS_k_m_v_r_nL_nH, bCodeData});
            //POS_S_TextOut(data.toString(), "gbk", 0, 1, 1, 0, 0, os);
            //POS_S_Align(1,os);
            POS_SetMotionUnit(50, 0, os);
            write(data, 0, data.length, os);
        } catch (IOException e) {
            Log.e("hehui", "sendMessage -->" + e);
        }
    }


    /**
     * 设置移动单位
     *
     * @param nHorizontalMU
     * @param nVerticalMU
     */
    public static void POS_SetMotionUnit(int nHorizontalMU, int nVerticalMU, DataOutputStream os) {
        if (nHorizontalMU < 0 || nHorizontalMU > 255 || nVerticalMU < 0
                || nVerticalMU > 255)
            return;

        byte[] data = Cmd.ESCCmd.GS_P_x_y;
        data[2] = (byte) nHorizontalMU;
        data[3] = (byte) nVerticalMU;
        write(data, 0, data.length, os);
    }

    /**
     * 打印发送文本消息
     *
     * @param message
     */
    @SuppressLint("NewApi")
    public static void sendMessage(String message) {
        if (MyApplication.bluetoothSocket == null || TextUtils.isEmpty(message))
            return;
        try {
            message += "\n";
            OutputStream outputStream = MyApplication.bluetoothSocket.getOutputStream();
            outputStream.write(message.getBytes("gbk"));
            outputStream.flush();
        } catch (IOException e) {
            Log.e("zw", "sendMessage -->" + e);
        }
    }

    /**
     * 把Bitmap转Byte
     */
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    /**
     * 打印二维码图片
     * <p>
     * param message
     */
    @SuppressLint("NewApi")
    public static void sendQrcodImage(String qrcode) {
        if (MyApplication.bluetoothSocket == null || StringUtils.isEmptyOrNull(qrcode))
            return;
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());

            byte[] bCodeData = null;
            try {
                bCodeData = qrcode.getBytes("GBK");
            } catch (UnsupportedEncodingException e) {
                return;
            }

            Cmd.ESCCmd.GS_w_n[2] = (byte) 6;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[3] = (byte) 10;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[4] = (byte) 4;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[5] = (byte) (bCodeData.length & 0xff);
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[6] = (byte) ((bCodeData.length & 0xff00) >> 8);

            byte[] data = byteArraysToBytes(new byte[][]{Cmd.ESCCmd.GS_w_n, Cmd.ESCCmd.GS_k_m_v_r_nL_nH, bCodeData});

            write(data, 0, data.length, os);
        } catch (IOException e) {
            Log.e("zw", "sendMessage -->" + e);
        }
    }

    /**
     * 将多个字节数组按顺序合并
     *
     * @param data
     * @return
     */
    public static byte[] byteArraysToBytes(byte[][] data) {

        int length = 0;
        for (int i = 0; i < data.length; i++)
            length += data[i].length;
        byte[] send = new byte[length];
        int k = 0;
        for (int i = 0; i < data.length; i++)
            for (int j = 0; j < data[i].length; j++)
                send[k++] = data[i][j];
        return send;
    }

    private static int write(byte[] buffer, int offset, int count, DataOutputStream os) {
        int cnt = 0;
        if (null != os) {
            try {
                os.write(buffer, offset, count);
                os.flush();
                cnt = count;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                if (os != null) {
                    try {
                        os.close();
                        os = null;
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

        return cnt;
    }

    /**
     * 打印二维码图片
     * <p>
     * param message
     */
    @SuppressLint("NewApi")
    public static void sendImage(Bitmap bitmap) {
        if (MyApplication.bluetoothSocket == null || bitmap == null)
            return;
        int idx = 0;
        int curcount = 0;
        byte ch;
        boolean canSend = true;

        try {

            Bitmap rszBitmap = ImageProcessing.resizeImage(bitmap, 100, 100);
            Bitmap grayBitmap = ImageProcessing.toGrayscale(rszBitmap);
            byte[] dithered = bitmapToBWPix(grayBitmap);

            //            byte[] b = bitmapToBWPix(bitmap);

            byte[] b = eachLinePixToCmd(dithered, 128, 0);
            int count = b.length;
            DataOutputStream outputStream = new DataOutputStream(MyApplication.bluetoothSocket.getOutputStream());
            DataInputStream is = new DataInputStream(MyApplication.bluetoothSocket.getInputStream());
            if (null != is) {
                is.skipBytes(is.available());
            }

            while (idx < b.length) {

                if ((null == is) || (null == outputStream))
                    break;

                if (is.available() > 0) {

                    ch = is.readByte();

                    Log.w("zw", "Receive char: " + ch);

                    if (0x13 == ch)
                        canSend = false;
                    else if (0x11 == ch)
                        canSend = true;
                    else
                        continue;
                }
                if (canSend) // 如果没有收到任何字节，继续发送
                {
                    if (count - idx > 128)
                        curcount = 128;
                    else
                        curcount = count - idx;

                    outputStream.write(b, 0 + idx, curcount);
                    idx += curcount;
                }
            }
            //            outputStream.write(b, 0, b.length);
            //            outputStream.flush();
        } catch (Exception e) {
            Log.e("zw", "sendMessage -->" + e);
        }
    }

    private static int[] p0 = {0, 0x80};

    private static int[] p1 = {0, 0x40};

    private static int[] p2 = {0, 0x20};

    private static int[] p3 = {0, 0x10};

    private static int[] p4 = {0, 0x08};

    private static int[] p5 = {0, 0x04};

    private static int[] p6 = {0, 0x02};

    private static byte[] eachLinePixToCmd(byte[] src, int nWidth, int nMode) {
        int nHeight = src.length / nWidth;
        int nBytesPerLine = nWidth / 8;
        byte[] data = new byte[nHeight * (8 + nBytesPerLine)];
        int offset = 0;
        int k = 0;
        for (int i = 0; i < nHeight; i++) {
            offset = i * (8 + nBytesPerLine);
            data[offset + 0] = 0x1d;
            data[offset + 1] = 0x76;
            data[offset + 2] = 0x30;
            data[offset + 3] = (byte) (nMode & 0x01);
            data[offset + 4] = (byte) (nBytesPerLine % 0x100);
            data[offset + 5] = (byte) (nBytesPerLine / 0x100);
            data[offset + 6] = 0x01;
            data[offset + 7] = 0x00;
            for (int j = 0; j < nBytesPerLine; j++) {
                data[offset + 8 + j] =
                        (byte) (p0[src[k]] + p1[src[k + 1]] + p2[src[k + 2]] + p3[src[k + 3]] + p4[src[k + 4]]
                                + p5[src[k + 5]] + p6[src[k + 6]] + src[k + 7]);
                k = k + 8;
            }
        }

        return data;
    }

    /**
     * 将ARGB图转换为二值图，0代表黑，1代表白
     *
     * @param mBitmap
     * @return
     */
    public static byte[] bitmapToBWPix(Bitmap mBitmap) {

        int[] pixels = new int[mBitmap.getWidth() * mBitmap.getHeight()];
        byte[] data = new byte[mBitmap.getWidth() * mBitmap.getHeight()];

        mBitmap.getPixels(pixels, 0, mBitmap.getWidth(), 0, 0, mBitmap.getWidth(), mBitmap.getHeight());

        // for the toGrayscale, we need to select a red or green or blue color
        ImageProcessing.format_K_dither16x16(pixels, mBitmap.getWidth(), mBitmap.getHeight(), data);

        return data;
    }

    /**
     * 打印一维条码
     *
     * @param str              打印条码字符
     * @param nType            条码类型(65~73)
     * @param nWidthX          条码宽度
     * @param nHeight          条码高度
     * @param nHriFontType     HRI字型
     * @param nHriFontPosition HRI位置
     * @return
     */
    public static byte[] getCodeBarCommand(String str, int nType, int nWidthX, int nHeight,
                                           int nHriFontType, int nHriFontPosition) {

        if (nType < 0x41 | nType > 0x49 | nWidthX < 2 | nWidthX > 6
                | nHeight < 1 | nHeight > 255 | str.length() == 0)
            return null;

        byte[] bCodeData = null;
        try {
            bCodeData = str.getBytes("GBK");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        byte[] command = new byte[bCodeData.length + 16];

        command[0] = 29;
        command[1] = 119;
        command[2] = ((byte) nWidthX);
        command[3] = 29;
        command[4] = 104;
        command[5] = ((byte) nHeight);
        command[6] = 29;
        command[7] = 102;
        command[8] = ((byte) (nHriFontType & 0x01));
        command[9] = 29;
        command[10] = 72;
        command[11] = ((byte) (nHriFontPosition & 0x03));
        command[12] = 29;
        command[13] = 107;
        command[14] = ((byte) nType);
        command[15] = (byte) (byte) bCodeData.length;
        System.arraycopy(bCodeData, 0, command, 16, bCodeData.length);
        return command;
    }
}
