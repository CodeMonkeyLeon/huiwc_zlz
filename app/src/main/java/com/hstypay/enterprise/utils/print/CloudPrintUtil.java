package com.hstypay.enterprise.utils.print;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.Widget.dialog.CustomViewBottomDialog;
import com.hstypay.enterprise.adapter.ChoiceCloudPrintDeviceDialogAdapter;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.CloudPrintFkStatisticsBean;
import com.hstypay.enterprise.bean.CloudPrintIncomeStatisticsBean;
import com.hstypay.enterprise.bean.CloudPrintPledgeConsumeBean;
import com.hstypay.enterprise.bean.CloudPrintPledgePayDetailBean;
import com.hstypay.enterprise.bean.CloudPrintPledgeReportBean;
import com.hstypay.enterprise.bean.CloudPrintResultBean;
import com.hstypay.enterprise.bean.CloudPrintVipStatisticsBean;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.bean.DeviceListBean;
import com.hstypay.enterprise.bean.MsgprintListBean;
import com.hstypay.enterprise.bean.PaymentMsgFormBean;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.RefundMsgFormBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.network.OkHttpUtil;
import com.hstypay.enterprise.network.ServerClient;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.NetworkUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SignUtil;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author zeyu.kuang
 * @time 2020/9/23
 * @desc 云打印
 */
public class CloudPrintUtil {
    //时间戳
    public static final long times = System.currentTimeMillis();
    public static String time = times + "";

    public interface OnGetCloudDevicesListCallBack{
        /**
         * 开始云打印
         * @param cloudDevices 选中关联的云打印机设备列表
         */
        void startCloudPrint(List<DeviceBean> cloudDevices);

        /**
         * 无云打印机时使用蓝牙打印
         */
        void startBluetoothPrint();
    }


    /**
     * 云打印入口方法
     * @param context
     * @param onGetCloudDevicesListCallBack
     */
    public static void cloudPrint(BaseActivity context,OnGetCloudDevicesListCallBack onGetCloudDevicesListCallBack){
        getCloudDevicesList(context,onGetCloudDevicesListCallBack);
    }


    /**
     * 获取云打印机设备列表数据
     * */
    private static void  getCloudDevicesList(BaseActivity context,OnGetCloudDevicesListCallBack onGetCloudDevicesListCallBack){
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            context.showNewLoading(true, context.getString(R.string.public_loading));
            /*Map<String, Object> map = new HashMap<>();
            map.put("pageSize" , 50);
            map.put("currentPage" , 1);
            map.put("search" , "");
            List<String> purposeCodeList = new ArrayList<>();
            purposeCodeList.add("5");
            purposeCodeList.add("13");
            map.put("purposeCodeList" , purposeCodeList);
            map.put("storeMerchantId" , "");//所有门店*/
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", 50);
            map.put("currentPage", 1);
            map.put("categoryCode", "CLOUD_PRINT");
            String url = Constants.BASE_URL + "merchant/device/newList";
            String data = SignUtil.getInstance().createAppSign(map, time);
            OkHttpUtil.postData(MyApplication.getContext(), url, "", data, time, new OkHttpUtil.OnResponse() {
                @Override
                public void onStringResponse(String response, int cancalTag) {
                    LogUtil.i("Jeremy", "获取云打印机列表数据=" + response);
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            context.dismissLoading();
                            DeviceListBean cloudDevicesBean = new Gson().fromJson(response, DeviceListBean.class);
                            if (cloudDevicesBean != null) {
                                if (cloudDevicesBean.isStatus()){
                                    if (cloudDevicesBean.getData() != null && cloudDevicesBean.getData().getData() != null && cloudDevicesBean.getData().getData().size() > 0) {
                                        //有云打印机设备
                                        List<DeviceBean> cloudDevices = cloudDevicesBean.getData().getData();
                                        String bluetoothName = MyApplication.getBluetoothName(1);
                                        if(!TextUtils.isEmpty(bluetoothName)){
                                            //有连接过一台蓝牙打印机
                                            DeviceBean dataBeanTitleBluetooth = new DeviceBean();
                                            dataBeanTitleBluetooth.setTitleView(true);
                                            dataBeanTitleBluetooth.setBluthDevice(true);
                                            cloudDevices.add(0,dataBeanTitleBluetooth);//添加标题

                                            DeviceBean dataBeanContentBluetooth = new DeviceBean();
                                            dataBeanContentBluetooth.setBluthPrintName(bluetoothName);
                                            dataBeanContentBluetooth.setBluthDevice(true);
                                            cloudDevices.add(1,dataBeanContentBluetooth);//添加蓝牙设备bean

                                            DeviceBean dataBeanTitleCloudPrint = new DeviceBean();
                                            dataBeanTitleCloudPrint.setTitleView(true);
                                            dataBeanTitleCloudPrint.setBluthDevice(false);
                                            cloudDevices.add(2,dataBeanTitleCloudPrint);//添加标题

                                            showChoiceCloudPrintDeviceDialog(context,cloudDevices,onGetCloudDevicesListCallBack);
                                        }else {
                                            //没有连接过蓝牙打印机，但绑定过云打印机
                                            if (cloudDevices.size()==1){
                                                //只有一台云打印机
                                                onGetCloudDevicesListCallBack.startCloudPrint(cloudDevices);
                                            }else {
                                                //云打印机超过一台
                                                DeviceBean dataBeanTitleCloudPrint = new DeviceBean();
                                                dataBeanTitleCloudPrint.setTitleView(true);
                                                dataBeanTitleCloudPrint.setBluthDevice(false);
                                                cloudDevices.add(0,dataBeanTitleCloudPrint);//添加标题
                                                showChoiceCloudPrintDeviceDialog(context,cloudDevices,onGetCloudDevicesListCallBack);
                                            }
                                        }
                                    }else {
                                        onGetCloudDevicesListCallBack.startBluetoothPrint();
                                    }

                                }else {
                                    onGetCloudDevicesListCallBack.startBluetoothPrint();
                                }
                            }else {
                                onGetCloudDevicesListCallBack.startBluetoothPrint();
                            }

                        }
                    });
                }

                @Override
                public void onError(Exception e) {
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            context.dismissLoading();
                            onGetCloudDevicesListCallBack.startBluetoothPrint();
                        }
                    });
                }
            });
        } else {
            onGetCloudDevicesListCallBack.startBluetoothPrint();
        }
    }


    /**
     * 弹窗选择打印机
     * */
    private static void showChoiceCloudPrintDeviceDialog(BaseActivity baseActivity,List<DeviceBean> cloudDevices,OnGetCloudDevicesListCallBack onGetCloudDevicesListCallBack) {
        CustomViewBottomDialog customViewBottomDialog = new CustomViewBottomDialog(baseActivity);
        customViewBottomDialog.setView(R.layout.dialog_choice_cloud_print_device);
        TextView tv_cancel = customViewBottomDialog.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消
                customViewBottomDialog.dismiss();
            }
        });

        RecyclerView rv_dialog_choice_device = customViewBottomDialog.findViewById(R.id.rv_dialog_choice_device);
        rv_dialog_choice_device.setLayoutManager(new LinearLayoutManager(baseActivity));
        ChoiceCloudPrintDeviceDialogAdapter choiceCloudPrintDeviceDialogAdapter = new ChoiceCloudPrintDeviceDialogAdapter(baseActivity);
        rv_dialog_choice_device.setAdapter(choiceCloudPrintDeviceDialogAdapter);
        choiceCloudPrintDeviceDialogAdapter.setCloudPrintDevices(cloudDevices);

        customViewBottomDialog.findViewById(R.id.tv_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //确认
                List<DeviceBean> cloudPrintDevices = choiceCloudPrintDeviceDialogAdapter.getCloudPrintDevices();
                List<DeviceBean> cloudPrintDevicesSelected = new ArrayList<>();
                int selectBluthPrinterCount = 0;//选择的蓝牙打印机数量（最多只有一台）
                for (DeviceBean dataBean:cloudPrintDevices){
                    boolean select = dataBean.isSelect();
                    if (select){
                        if (dataBean.isBluthDevice()){
                            selectBluthPrinterCount++;
                            onGetCloudDevicesListCallBack.startBluetoothPrint();
                        }else {
                            cloudPrintDevicesSelected.add(dataBean);
                        }
                    }
                }
                if ((cloudPrintDevicesSelected.size()+selectBluthPrinterCount)==0){
                    MyToast.showToast(ToastHelper.toStr(R.string.show_tip_select_printer), Toast.LENGTH_SHORT);
                }else {
                    customViewBottomDialog.dismiss();
                    onGetCloudDevicesListCallBack.startCloudPrint(cloudPrintDevicesSelected);
                }

            }
        });
        customViewBottomDialog.show();
    }


    /**
     * 云打印 收款日结统计和会员统计、饭卡统计 小票
     * @param baseActivity
     * @param selectedCloudPrintDevices 选中关联的云打印机设备列表
     * @param info 统计信息
     * */
    public static void startCloudPrint(BaseActivity baseActivity,List<DeviceBean> selectedCloudPrintDevices, ReportBean.DataEntity info) {
        if (selectedCloudPrintDevices==null || selectedCloudPrintDevices.size()==0){
            return;
        }
        Map<String, Object> map = new HashMap<>();
        if (info.getType()==1){
            //收款日结统计
            map.put("printType","3");
        }else if (info.getType() == 2){
            //会员统计
            map.put("printType","7");
        }else if (info.getType() == 3){
            //饭卡统计
            map.put("printType","8");
        }
        List<MsgprintListBean> list = new ArrayList<>();
        for (DeviceBean deviceBean:selectedCloudPrintDevices ){
            MsgprintListBean msgprintListBean = new MsgprintListBean();
            msgprintListBean.setDeviceId(""+deviceBean.getDeviceId());
            msgprintListBean.setFirm(deviceBean.getFirmCode());
            msgprintListBean.setSn(deviceBean.getSn());
            msgprintListBean.setStoreMerchantId(deviceBean.getStoreMerchantId());
            list.add(msgprintListBean);
        }
        map.put("list",list);
        String printMsgJson = null;
        if (info.getType()==1){
            //收款日结统计
            CloudPrintIncomeStatisticsBean cloudPrintIncomeStatisticsBean = new CloudPrintIncomeStatisticsBean();
            cloudPrintIncomeStatisticsBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
            cloudPrintIncomeStatisticsBean.setTitle(UIUtils.getString(R.string.print_pay_report_title_new));
            cloudPrintIncomeStatisticsBean.setMerchantId(MyApplication.getMechantId());
            cloudPrintIncomeStatisticsBean.setStoreName(info.getStoreName());
            cloudPrintIncomeStatisticsBean.setOpUserName(info.getCashierName());
            cloudPrintIncomeStatisticsBean.setStartTimeStr(info.getStartTime());
            cloudPrintIncomeStatisticsBean.setEndTimeStr(info.getEndTime());
            cloudPrintIncomeStatisticsBean.setTradeNetAmount(DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "");
            cloudPrintIncomeStatisticsBean.setDiscountAmount(DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "");
            cloudPrintIncomeStatisticsBean.setReceiptAmount(DateUtil.formatMoneyUtils(info.getPayFee()) + "");
            cloudPrintIncomeStatisticsBean.setRefundAmount(DateUtil.formatMoneyUtils(info.getRefundFee()) + "");
            cloudPrintIncomeStatisticsBean.setTradeAmount(DateUtil.formatMoneyUtils(info.getSuccessFee()) + "");
            cloudPrintIncomeStatisticsBean.setTradeNum(info.getSuccessCount() + "");
            cloudPrintIncomeStatisticsBean.setRefundNum(info.getRefundCount() + "" );
            List<ReportBean.DataEntity.ListEntity> listEntities = info.getList();
            if (listEntities != null && listEntities.size() > 0) {
                for (ReportBean.DataEntity.ListEntity itemInfo : listEntities) {
                    switch (itemInfo.getApiProvider()) {
                        case 1:
                            // "微信支付";
                            cloudPrintIncomeStatisticsBean.setWxPayAmount(DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "");
                            cloudPrintIncomeStatisticsBean.setWxPayNum(itemInfo.getSuccessCount() + "" );
                            break;
                        case 2:
                            // "支付宝支付";
                            cloudPrintIncomeStatisticsBean.setAliPayAmount(DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "");
                            cloudPrintIncomeStatisticsBean.setAliPayNum(itemInfo.getSuccessCount() + "" );
                            break;
                        case 5:
                            // "银联支付";
                            cloudPrintIncomeStatisticsBean.setUnionPayAmount(DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "");
                            cloudPrintIncomeStatisticsBean.setUnionPayNum(itemInfo.getSuccessCount() + "" );
                            break;
                        case 10:
                            // "刷卡支付";
                            cloudPrintIncomeStatisticsBean.setPayCardAmount(DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "");
                            cloudPrintIncomeStatisticsBean.setPayCardNum(itemInfo.getSuccessCount() + "" );
                            break;
                    }
                }
            }

            cloudPrintIncomeStatisticsBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            cloudPrintIncomeStatisticsBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(cloudPrintIncomeStatisticsBean);
        }else if (info.getType() == 2){
            //会员统计
            CloudPrintVipStatisticsBean cloudPrintVipStatisticsBean = new CloudPrintVipStatisticsBean();
            cloudPrintVipStatisticsBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
            cloudPrintVipStatisticsBean.setTitle(UIUtils.getString(R.string.print_consume_report_title));
            cloudPrintVipStatisticsBean.setMerchantId(MyApplication.getMechantId());
            cloudPrintVipStatisticsBean.setStoreName(info.getStoreName());
            cloudPrintVipStatisticsBean.setOpUserName(info.getCashierName());
            cloudPrintVipStatisticsBean.setStartTimeStr(info.getStartTime());
            cloudPrintVipStatisticsBean.setEndTimeStr(info.getEndTime());
            cloudPrintVipStatisticsBean.setTradeAmount(DateUtil.formatMoneyUtils(info.getSuccessFee()) + "");
            cloudPrintVipStatisticsBean.setDiscountAmount(DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "");
            cloudPrintVipStatisticsBean.setConsumerAmount(DateUtil.formatMoneyUtils(info.getPayFee()) + "");
            cloudPrintVipStatisticsBean.setIncome(DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "");
            cloudPrintVipStatisticsBean.setRefundAmount(DateUtil.formatMoneyUtils(info.getRefundFee()) + "");
            cloudPrintVipStatisticsBean.setTradeNum(info.getSuccessCount() + "" );
            cloudPrintVipStatisticsBean.setRefundNum(info.getRefundCount() + "");
            cloudPrintVipStatisticsBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            cloudPrintVipStatisticsBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(cloudPrintVipStatisticsBean);
        }else if (info.getType() == 3){
            //饭卡统计
            CloudPrintFkStatisticsBean cloudPrintFkStatisticsBean = new CloudPrintFkStatisticsBean();
            cloudPrintFkStatisticsBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
            cloudPrintFkStatisticsBean.setTitle(UIUtils.getString(R.string.print_fk_consume_report_title));
            cloudPrintFkStatisticsBean.setMerchantId(MyApplication.getMechantId());
            cloudPrintFkStatisticsBean.setStoreName(info.getStoreName());
            cloudPrintFkStatisticsBean.setOpUserName(info.getCashierName());
            cloudPrintFkStatisticsBean.setStartTimeStr(info.getStartTime());
            cloudPrintFkStatisticsBean.setEndTimeStr(info.getEndTime());
            cloudPrintFkStatisticsBean.setTradeAmount(DateUtil.formatMoneyUtils(info.getSuccessFee()) + "");
//            cloudPrintFkStatisticsBean.setDiscountAmount(DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "");
//            cloudPrintFkStatisticsBean.setConsumerAmount(DateUtil.formatMoneyUtils(info.getPayFee()) + "");
            cloudPrintFkStatisticsBean.setIncome(DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "");
            cloudPrintFkStatisticsBean.setRefundAmount(DateUtil.formatMoneyUtils(info.getRefundFee()) + "");
            cloudPrintFkStatisticsBean.setTradeNum(info.getSuccessCount() + "" );
            cloudPrintFkStatisticsBean.setRefundNum(info.getRefundCount() + "");
            cloudPrintFkStatisticsBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            cloudPrintFkStatisticsBean.setSettleAmount(DateUtil.formatMoneyUtils(info.getSettlementFee()));
            cloudPrintFkStatisticsBean.setSettleRate(info.getSettleRate()+"");
            cloudPrintFkStatisticsBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(cloudPrintFkStatisticsBean);

        }
        map.put("printMsg",printMsgJson);
        map.put("mchId",MyApplication.getMechantId());
        baseActivity.showNewLoading(true, baseActivity.getString(R.string.public_loading));
        ServerClient.newInstance(MyApplication.getContext()).cloudPrint(MyApplication.getContext(), Constants.TAG_CLOUD_PRINT_MSGPRINT, map, new OkHttpUtil.OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "开始云打印统计信息=" + response);
                CloudPrintResultBean cloudPrintResultBean = new Gson().fromJson(response,CloudPrintResultBean.class);
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        if (cloudPrintResultBean!=null){
                            if (!cloudPrintResultBean.isStatus() && cloudPrintResultBean.getError() != null){
                                baseActivity.showCommonNoticeDialog( baseActivity, cloudPrintResultBean.getError().getMessage());
                            }
                        }
                    }
                });
            }
            @Override
            public void onError(Exception e) {
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        MyToast.showToastShort(baseActivity.getString(R.string.net_error));
                    }
                });
            }
        });
    }


    /**
     * 云打印 押金统计信息 小票
     * @param baseActivity
     * @param selectedCloudPrintDevices 选中关联的云打印机设备列表
     * @param info 押金统计信息
     */
    public static void startCloudPrint(BaseActivity baseActivity,List<DeviceBean> selectedCloudPrintDevices, PledgeReportBean.DataBean info) {
        if (selectedCloudPrintDevices==null || selectedCloudPrintDevices.size()==0){
            return;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("printType","6");
        List<MsgprintListBean> list = new ArrayList<>();
        for (DeviceBean deviceBean:selectedCloudPrintDevices ){
            MsgprintListBean msgprintListBean = new MsgprintListBean();
            msgprintListBean.setDeviceId(""+deviceBean.getDeviceId());
            msgprintListBean.setFirm(deviceBean.getFirmCode());
            msgprintListBean.setSn(deviceBean.getSn());
            msgprintListBean.setStoreMerchantId(deviceBean.getStoreMerchantId());
            list.add(msgprintListBean);
        }
        map.put("list",list);
        CloudPrintPledgeReportBean cloudPrintPledgeReportBean = new CloudPrintPledgeReportBean();
        cloudPrintPledgeReportBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
        cloudPrintPledgeReportBean.setTitle(UIUtils.getString(R.string.print_title_pledge_report_new));
        cloudPrintPledgeReportBean.setMerchantId(MyApplication.getMechantId());
        cloudPrintPledgeReportBean.setStoreName(info.getStoreName());
        cloudPrintPledgeReportBean.setOpUserName(info.getCashierName());
        cloudPrintPledgeReportBean.setStartTimeStr(info.getStartTime());
        cloudPrintPledgeReportBean.setEndTimeStr(info.getEndTime());
        cloudPrintPledgeReportBean.setDepositAmount(DateUtil.formatMoneyUtils(info.getSumPreMoney())+ "");
        cloudPrintPledgeReportBean.setDepositNum(info.getCntPreMoney() + "");
        cloudPrintPledgeReportBean.setConsumeAmount(DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "");
        cloudPrintPledgeReportBean.setConsumeNum(info.getCntPayMoney() + "" );
        cloudPrintPledgeReportBean.setRefundAmount(DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "");
        cloudPrintPledgeReportBean.setRefundNum(info.getCntFreeMoney() + "");
        cloudPrintPledgeReportBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
        cloudPrintPledgeReportBean.setPrintMode(2);
        String printMsgJson = new Gson().toJson(cloudPrintPledgeReportBean);
        map.put("printMsg",printMsgJson);
        map.put("mchId",MyApplication.getMechantId());
        baseActivity.showNewLoading(true, baseActivity.getString(R.string.public_loading));
        ServerClient.newInstance(MyApplication.getContext()).cloudPrint(MyApplication.getContext(), Constants.TAG_CLOUD_PRINT_MSGPRINT, map, new OkHttpUtil.OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "开始云打印押金统计信息=" + response);
                CloudPrintResultBean cloudPrintResultBean = new Gson().fromJson(response,CloudPrintResultBean.class);
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        if (cloudPrintResultBean!=null){
                            if (!cloudPrintResultBean.isStatus() && cloudPrintResultBean.getError() != null){
                                baseActivity.showCommonNoticeDialog( baseActivity, cloudPrintResultBean.getError().getMessage());
                            }
                        }
                    }
                });
            }
            @Override
            public void onError(Exception e) {
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        MyToast.showToastShort(baseActivity.getString(R.string.net_error));
                    }
                });
            }
        });
    }

    /**
     * 开始云打印 普通收款/退款的交易详情 信息
     * @param baseActivity
     * @param selectedCloudPrintDevices 选中关联的云打印机设备列表
     * @param billdata 收款/退款的交易详情信息
     */
    public static void startCloudPrint(BaseActivity baseActivity,List<DeviceBean> selectedCloudPrintDevices, TradeDetailBean billdata) {
        if (selectedCloudPrintDevices==null || selectedCloudPrintDevices.size()==0){
            return;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("printType",billdata.isPay()?"1":"2");//1收款 2退款
        map.put("outRequestNo",billdata.isPay()?""+billdata.getOrderNo():""+billdata.getRefundNo());
        List<MsgprintListBean> list = new ArrayList<>();
        for (DeviceBean deviceBean:selectedCloudPrintDevices ){
            MsgprintListBean msgprintListBean = new MsgprintListBean();
            msgprintListBean.setDeviceId(""+deviceBean.getDeviceId());
            msgprintListBean.setFirm(deviceBean.getFirmCode());
            msgprintListBean.setSn(deviceBean.getSn());
            msgprintListBean.setStoreMerchantId(deviceBean.getStoreMerchantId());
            list.add(msgprintListBean);
        }
        map.put("list",list);
        String printMsgJson=null;
        if (billdata.isPay()){
            //收款
            PaymentMsgFormBean paymentMsgFormBean = new PaymentMsgFormBean();
            paymentMsgFormBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
            paymentMsgFormBean.setOrderNo(billdata.getOrderNo());
            paymentMsgFormBean.setMerchantId(MyApplication.getMechantId());
            paymentMsgFormBean.setStoreName(billdata.getStoreMerchantIdCnt());
            //交易类型
            //paymentMsgFormBean.setTradeType("收款");
            if (billdata.getTradeType() == 1) {
                paymentMsgFormBean.setTradeType("充值");

            } else if (billdata.getTradeType() == 2) {
                paymentMsgFormBean.setTradeType("核销");

            } else {
                paymentMsgFormBean.setTradeType("收款");
                /*if (!billdata.isPay()) {
                    paymentMsgFormBean.setTradeType("退款" );

                } else {
                    paymentMsgFormBean.setTradeType("收款");
                }*/
            }
            paymentMsgFormBean.setCashPoint(billdata.getCashPointName());
            paymentMsgFormBean.setOpUserName(billdata.getCashierName());
            paymentMsgFormBean.setTradeOrderNo(billdata.getTransactionId());
            paymentMsgFormBean.setTradeStatus( OrderStringUtil.getTradeStateString(billdata.getTradeState()));
            paymentMsgFormBean.setPayType( OrderStringUtil.tradeTypeFromString(billdata.getApiProvider(), billdata.getApiCode()));
            paymentMsgFormBean.setPayTimeStr(billdata.getTradeTime());
            paymentMsgFormBean.setTradeAmount(DateUtil.formatMoneyUtils(billdata.getMoney()) + "");
            paymentMsgFormBean.setDiscountAmount(DateUtil.formatMoneyUtils(billdata.getCouponFee()) + "");
            paymentMsgFormBean.setReceiptAmount(DateUtil.formatMoneyUtils(billdata.getRealMoney()) + "");
            paymentMsgFormBean.setRemark(billdata.getAttach());
            paymentMsgFormBean.setStubName(billdata.getPartner());
            paymentMsgFormBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            paymentMsgFormBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(paymentMsgFormBean);
        }else {
            //退款
            RefundMsgFormBean refundMsgFormBean = new RefundMsgFormBean();
            refundMsgFormBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
            refundMsgFormBean.setOrderNo(billdata.getRefundNo());
            refundMsgFormBean.setStubName(billdata.getPartner());
            refundMsgFormBean.setMerchantId(MyApplication.getMechantId());
            refundMsgFormBean.setStoreName(billdata.getStoreMerchantIdCnt());
            //交易类型
            //refundMsgFormBean.setTradeType("退款" );
            if (billdata.getTradeType() == 1) {
                refundMsgFormBean.setTradeType("充值" );

            } else if (billdata.getTradeType() == 2) {
                refundMsgFormBean.setTradeType("核销" );

            } else {
                refundMsgFormBean.setTradeType("退款" );
                /*if (!billdata.isPay()) {
                    refundMsgFormBean.setTradeType("退款" );

                } else {
                    refundMsgFormBean.setTradeType("收款");
                }*/
            }
            refundMsgFormBean.setOpUserName(billdata.getRefundUser());
            refundMsgFormBean.setRefundApplyTimeStr(billdata.getRefundTime());
            refundMsgFormBean.setRefundStatus(OrderStringUtil.getRefundStateString(billdata.getRefundStatus()));
            refundMsgFormBean.setRefundAmount(DateUtil.formatMoneyUtils(billdata.getRefundMoney())+ "");
            refundMsgFormBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            refundMsgFormBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(refundMsgFormBean);
        }
        map.put("printMsg",printMsgJson);
        map.put("mchId",MyApplication.getMechantId());
        baseActivity.showNewLoading(true, UIUtils.getString(R.string.public_loading));
        ServerClient.newInstance(MyApplication.getContext()).cloudPrint(MyApplication.getContext(), Constants.TAG_CLOUD_PRINT_MSGPRINT, map, new OkHttpUtil.OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "开始云打印普通收款退款交易详情信息=" + response);
                CloudPrintResultBean cloudPrintResultBean = new Gson().fromJson(response,CloudPrintResultBean.class);
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();

                        if (cloudPrintResultBean!=null){
                            if (!cloudPrintResultBean.isStatus() && cloudPrintResultBean.getError() != null){
                                baseActivity.showCommonNoticeDialog(baseActivity, cloudPrintResultBean.getError().getMessage());
                            }
                        }
                    }
                });
            }
            @Override
            public void onError(Exception e) {
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        MyToast.showToastShort(baseActivity.getResources().getString(R.string.net_error));
                    }
                });
            }
        });
    }


    /**
     * 开始云打印 预授权押金收款和押金解冻的交易信息 小票
     * @param baseActivity
     * @param cloudPrintDevicesSelected 选中关联的云打印机设备列表
     * @param pledgePayBean 押金收款或押金解冻的交易信息
     */
    public static void startCloudPrint(BaseActivity baseActivity,List<DeviceBean> cloudPrintDevicesSelected, PledgePayBean.DataBean pledgePayBean) {
        if (cloudPrintDevicesSelected==null || cloudPrintDevicesSelected.size()==0){
            return;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("printType",pledgePayBean.isToPay()?"5":"4");//押金解冻5，押金收款4
        map.put("outRequestNo",pledgePayBean.getAuthNo());
        List<MsgprintListBean> list = new ArrayList<>();
        for (DeviceBean deviceBean:cloudPrintDevicesSelected ){
            MsgprintListBean msgprintListBean = new MsgprintListBean();
            msgprintListBean.setDeviceId(""+deviceBean.getDeviceId());
            msgprintListBean.setFirm(deviceBean.getFirmCode());
            msgprintListBean.setSn(deviceBean.getSn());
            msgprintListBean.setStoreMerchantId(deviceBean.getStoreMerchantId());
            list.add(msgprintListBean);
        }
        map.put("list",list);
        String printMsgJson=null;
        if (!pledgePayBean.isToPay()){//押金收款
            CloudPrintPledgePayDetailBean cloudPrintPledgePayDetailBean = new CloudPrintPledgePayDetailBean();
            cloudPrintPledgePayDetailBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());

            cloudPrintPledgePayDetailBean.setOrderNo(pledgePayBean.getAuthNo());
            cloudPrintPledgePayDetailBean.setStubName(pledgePayBean.getPartner());
            cloudPrintPledgePayDetailBean.setMerchantId(MyApplication.getMechantId());
            cloudPrintPledgePayDetailBean.setStoreName(pledgePayBean.getStoreMerchantIdCnt());
            cloudPrintPledgePayDetailBean.setTradeType(UIUtils.getString(R.string.type_pledge_income));
            cloudPrintPledgePayDetailBean.setOpUserName(pledgePayBean.getCashierName());
            cloudPrintPledgePayDetailBean.setTradeOrderNo(pledgePayBean.getOutTransactionId());
            cloudPrintPledgePayDetailBean.setTradeStatus(OrderStringUtil.getPledgeStateString(pledgePayBean.getTradeStatus()));
            cloudPrintPledgePayDetailBean.setPayType(OrderStringUtil.tradeTypeFromString(pledgePayBean.getApiProvider(), pledgePayBean.getApiCode()));
            cloudPrintPledgePayDetailBean.setPayTimeStr(pledgePayBean.getTradeFinishTime());
            if (!TextUtils.isEmpty(pledgePayBean.getMoney())) {
                cloudPrintPledgePayDetailBean.setSumPreMoney(DateUtil.formatMoneyUtils(Utils.Long.tryParse(pledgePayBean.getMoney(), 0)) + "");
            } else {
                cloudPrintPledgePayDetailBean.setSumPreMoney("0.00");
            }
            cloudPrintPledgePayDetailBean.setRemark(pledgePayBean.getAttach());
            cloudPrintPledgePayDetailBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            cloudPrintPledgePayDetailBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(cloudPrintPledgePayDetailBean);
        }else {//押金解冻
            CloudPrintPledgeConsumeBean cloudPrintPledgeConsumeBean = new CloudPrintPledgeConsumeBean();
            cloudPrintPledgeConsumeBean.setMerchantName(TextUtils.isEmpty(MyApplication.getMerchantShortName())?MyApplication.getMerchantName():MyApplication.getMerchantShortName());
            cloudPrintPledgeConsumeBean.setMerchantId(MyApplication.getMechantId());
            if (!TextUtils.isEmpty(pledgePayBean.getSumPayMoney())) {
                cloudPrintPledgeConsumeBean.setConsumeAmount( DateUtil.formatMoneyUtils(Utils.Long.tryParse(pledgePayBean.getSumPayMoney(), 0)) + "");
            } else {
                cloudPrintPledgeConsumeBean.setConsumeAmount("0.00");
            }
            cloudPrintPledgeConsumeBean.setOpUserName(pledgePayBean.getCashierName());
            cloudPrintPledgeConsumeBean.setOrderNo(pledgePayBean.getAuthNo());
            cloudPrintPledgeConsumeBean.setPrintTimeStr(DateUtil.formatTime(System.currentTimeMillis()));
            if (!TextUtils.isEmpty(pledgePayBean.getSumFreeMoney())) {
                cloudPrintPledgeConsumeBean.setRefundAmount( DateUtil.formatMoneyUtils(Utils.Long.tryParse(pledgePayBean.getSumFreeMoney(), 0)) + "" );
            } else {
                cloudPrintPledgeConsumeBean.setRefundAmount("0.00");
            }
            cloudPrintPledgeConsumeBean.setRefundApplyTimeStr(pledgePayBean.getTradeFinishTime());
            cloudPrintPledgeConsumeBean.setStoreName(pledgePayBean.getStoreMerchantIdCnt());
            cloudPrintPledgeConsumeBean.setStubName(pledgePayBean.getPartner());
            cloudPrintPledgeConsumeBean.setTradeStatus(OrderStringUtil.getPledgeStateString(pledgePayBean.getTradeStatus()));
            cloudPrintPledgeConsumeBean.setTradeType(UIUtils.getString(R.string.type_pledge_consume));
            cloudPrintPledgeConsumeBean.setPrintMode(2);
            printMsgJson = new Gson().toJson(cloudPrintPledgeConsumeBean);
        }

        map.put("printMsg",printMsgJson);
        map.put("mchId",MyApplication.getMechantId());
        baseActivity.showNewLoading(true, UIUtils.getString(R.string.public_loading));
        ServerClient.newInstance(MyApplication.getContext()).cloudPrint(MyApplication.getContext(), Constants.TAG_CLOUD_PRINT_MSGPRINT, map, new OkHttpUtil.OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "开始云打印预授权押金收款或押金解冻的交易信息=" + response);
                CloudPrintResultBean cloudPrintResultBean = new Gson().fromJson(response,CloudPrintResultBean.class);
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        if (cloudPrintResultBean!=null){
                            if (!cloudPrintResultBean.isStatus() && cloudPrintResultBean.getError() != null){
                                baseActivity.showCommonNoticeDialog(baseActivity, cloudPrintResultBean.getError().getMessage());
                            }
                        }
                    }
                });
            }
            @Override
            public void onError(Exception e) {
                baseActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        baseActivity.dismissLoading();
                        MyToast.showToastShort(baseActivity.getResources().getString(R.string.net_error));
                    }
                });
            }
        });
    }

}
