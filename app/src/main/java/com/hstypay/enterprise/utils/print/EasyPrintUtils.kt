package com.hstypay.enterprise.utils.print

import android.content.ComponentName
import android.content.Intent
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.hstypay.enterprise.app.MyApplication
import com.hstypay.enterprise.base.BaseActivity
import com.hstypay.enterprise.bean.EasyPrintBean
import com.hstypay.enterprise.bean.TradeDetailBean
import com.hstypay.enterprise.utils.Constants
import com.hstypay.enterprise.utils.DateUtil
import com.hstypay.enterprise.utils.StringUtils


/**
 * 易生（联迪设备）自定义打印
 */
object EasyPrintUtils {

    /**
     * 自定义打印
     */
    @JvmStatic
    fun easyPayPrint(activity: BaseActivity, orderModel: TradeDetailBean, type: Int = 0) {
        activity.unbindDeviceService()
        val intent = Intent()
        intent.component = ComponentName("net.worthtech.worthcasher", "net.worthtech.worthcasher.activity.PayService")
        intent.putExtra("option", "print")
        val jsonContent = JsonObject()
        val jsonList = JsonArray()
        val list = arrayListOf<EasyPrintBean>()

        list.add(EasyPrintBean(1, "易生支付"))
        if (type == 0) {
            list.add(EasyPrintBean(2, "商户存根             POS签购单"))
        } else {
            list.add(EasyPrintBean(2, "持卡人存根           POS签购单"))
        }
        list.add(EasyPrintBean(2, "------------------------------"))
        if (!MyApplication.getMerchantName().isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "商户名称:"))
            list.add(EasyPrintBean(2, orderModel.merchantName))
        }
        if (!orderModel.thirdOrderNo.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "商户编号:"))
            list.add(EasyPrintBean(2, orderModel.thirdMerchantId))
        }
        if (!orderModel.storeMerchantIdCnt.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "交易门店:"))
            list.add(EasyPrintBean(2, orderModel.storeMerchantIdCnt))
        }
        if (!orderModel.tradeCode.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "终端编号:"))
            list.add(EasyPrintBean(2, orderModel.tradeCode))
        }
        if (!StringUtils.getDeviceNo(orderModel.deviceSn).isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "设备号:"))
            list.add(EasyPrintBean(2, StringUtils.getDeviceNo(orderModel.deviceSn)))
        }
        if (!orderModel.payCardBankName.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "发卡行:${orderModel.payCardBankName}"))
        }

        if (!orderModel.payCardId.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "卡号:"))
            list.add(EasyPrintBean(2, orderModel.payCardId))
        }

        if (!orderModel.payCardType.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "卡类型:${orderModel.payCardType}"))
        }
        list.add(EasyPrintBean(2, "交易类别:消费"))

        if (!orderModel.batchID.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "批次号:${orderModel.batchID}"))
        }
        if (!orderModel.voucherNo.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "流水号:${orderModel.voucherNo}"))
        }
        if (!orderModel.reqOrderNo.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "参考号:${orderModel.reqOrderNo}"))
        }
        if (!orderModel.tradeTime.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "交易时间:${orderModel.tradeTime}"))
        }
        list.add(EasyPrintBean(2, "交易金额:${DateUtil.formatMoneyUtils(orderModel.money)}"))
        if (!orderModel.thirdOrderNo.isNullOrEmpty()) {
            list.add(EasyPrintBean(2, "三方订单号:"))
            list.add(EasyPrintBean(2, orderModel.thirdOrderNo))
        }
        if (type == 1) {
            list.add(EasyPrintBean(2, "持卡人签名:"))
            list.add(EasyPrintBean(2, ""))
            list.add(EasyPrintBean(2, ""))
            list.add(EasyPrintBean(2, ""))
            list.add(EasyPrintBean(2, ""))
            list.add(EasyPrintBean(6, "本人确认以上交易，同意将其计入本卡账户"))
        }
        list.add(EasyPrintBean(2, ""))
        if (orderModel.isAutoPrint) {
            list.add(EasyPrintBean(6, "自动打印"))
        } else {
            list.add(EasyPrintBean(6, "手动打印"))
        }

        list.add(EasyPrintBean(6, "本人确认以上交易，同意将其计入本卡账户"))

        list.forEach {
            val item = JsonObject()
            item.addProperty("type", it.type)
            item.addProperty("content", it.content)
            jsonList.add(item)
        }
        jsonContent.add("list", jsonList)

        intent.putExtra("content", jsonContent.toString())
        activity.startActivityForResult(intent, Constants.REQUEST_BCARD_REPRINT)
    }
}