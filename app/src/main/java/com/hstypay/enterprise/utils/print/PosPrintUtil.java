package com.hstypay.enterprise.utils.print;

import static com.hstypay.enterprise.utils.UIUtils.getString;

import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.CommonNoticeDialog;
import com.hstypay.enterprise.activity.MainActivity;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.DeviceBean;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.CreateOneDiCodeUtil;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DialogHelper;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MtaUtils;
import com.hstypay.enterprise.utils.SpStayUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.a920.A920Printer;
import com.hstypay.enterprise.utils.print.a920.PrinterTester;
import com.hstypay.enterprise.utils.print.xdl.PrinterXdlUtil;
import com.hstypay.enterprise.utils.print.yipos.YiposPrintText;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.ums.anypay.service.IOnTransEndListener;
import com.zng.common.PrintUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.weipass.pos.sdk.IPrint;
import cn.weipass.pos.sdk.Printer;
import woyou.aidlservice.jiuiv5.ICallback;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print
 * @创建者: Jeremy
 * @创建时间: 2017/11/17 11:01
 * @描述: ${TODO}
 */

public class PosPrintUtil {

    private BaseActivity mActivity;
    private Button mButton;
    private PrinterApos1 printerSample;
    private PrinterSummary printerSummary;
    private Byte mPrintLanguage;
    private PrinterPledgeApos printerPledgeSample;
    private CommonNoticeDialog mWposDialog;

    public PosPrintUtil(BaseActivity activity, Button button) {
        this.mActivity = activity;
        this.mButton = button;
    }

    //打印预授权日结小票
    public void printPledgeReport(Printer printer, IWoyouService woyouService, PrintUtils printUtils, AidlPrinter printerDev, AidlPrint aidlPrint, Intent intentService, com.chinapnr.aidl.printer.AidlPrinter printerModule, PledgeReportBean.DataBean reportBean) {
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case "wpos":
                    wposPrintPledgeReport(printer, reportBean);
                    break;
                case Constants.LANDI_APOS_A8:
                    mActivity.bindDeviceService();
                    ldPrintPledgeReport(reportBean);
                    break;
                case "shangmi":
                    smPrintPledgeReport(woyouService, reportBean);
                    break;
                case "lepos":
                    leposPrintPledgeReport(printUtils, reportBean);
                    break;
                case "lkl":
                    lklPrintPledgeReport(printerDev, reportBean);
                    break;
                case "ybx":
                    ybxPrintPledgeReport(intentService, reportBean);
                    break;
                case "a920":
                    a920PrintPledgeReport(reportBean);
                    break;
                case "yipos":
                    yiPrintPledgeReport(aidlPrint, reportBean);
                    break;
                case "hdy"://海迪云
                    hdyPrintPledgeReport(reportBean);
                    break;
                case "xdl"://新大陆
                    xdlPrintPledgeReport(mActivity, reportBean);
                    break;
                case Constants.HUIFU://汇付
                    huifuPrintPledgeReport(mActivity, printerModule, reportBean);
                    break;
                case Constants.WIZARPOS://慧银
                    wizardposPrintPledgeReport(reportBean);
                case Constants.WJY_WIZARPOS://万科慧银
                case Constants.WJY_YINSHANG://万科银商
                    //TODO 银商pos打印方法
//                    wizardposPrintPledgeReport(reportBean);
                    break;
                /*default:
                    bluetoothPrint(billdata);
                    break;*/
            }
        }
    }

    private void wizardposPrintPledgeReport(PledgeReportBean.DataBean reportBean) {
        WizarPrinterUtil.with().printPledgeTotal(reportBean);
    }

    private void huifuPrintPledgeReport(BaseActivity activity, com.chinapnr.aidl.printer.AidlPrinter printerModule, PledgeReportBean.DataBean reportBean) {
        PrintHuifuUtil.with().printPledgeTotal(activity, printerModule, reportBean);
    }

    private void xdlPrintPledgeReport(BaseActivity activity, PledgeReportBean.DataBean reportBean) {
        PrinterXdlUtil.with().printPledgeTotal(activity, reportBean);
    }

    private void hdyPrintPledgeReport(PledgeReportBean.DataBean reportBean) {
        PrinterHdyUtil.with().printPledgeTotal(reportBean);
    }

    private void yiPrintPledgeReport(AidlPrint aidlPrint, PledgeReportBean.DataBean reportBean) {
        YiposPrintText.with().doPrint(aidlPrint, YiposPrintText.with().printJsonPledgeSum(reportBean), null);
    }

    private void a920PrintPledgeReport(PledgeReportBean.DataBean orderModel) {
        PrinterTester.getInstance().init();
        A920Printer.printPledgeTotal(mActivity, orderModel);
    }

    private void ybxPrintPledgeReport(Intent intentService, PledgeReportBean.DataBean orderModel) {
        intentService.putExtra("SPRT", orderModel);
        mActivity.startService(intentService);
    }

    private void lklPrintPledgeReport(final AidlPrinter printerDev, PledgeReportBean.DataBean orderModel) {
        PrintLKL.printPledgeSummary(printerDev, orderModel, MyApplication.getContext());
    }

    private void leposPrintPledgeReport(PrintUtils printUtils, PledgeReportBean.DataBean reportBean) {
        ZngPrintUtil.printPledgeTotal(MyApplication.getContext(), printUtils, reportBean);
    }

    private void smPrintPledgeReport(final IWoyouService woyouService, final PledgeReportBean.DataBean reportBean) {
        final ICallback callback = new ICallback.Stub() {

            @Override
            public void onRunResult(boolean success) throws RemoteException {
            }

            @Override
            public void onReturnString(final String value) throws RemoteException {
            }

            @Override
            public void onRaiseException(int code, final String msg) throws RemoteException {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });

            }
        };

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    PrintBillSuccess.pintPledgeSum(reportBean, woyouService, callback, null);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ldPrintPledgeReport(PledgeReportBean.DataBean reportBean) {
        printerSummary = new PrinterSummary(MyApplication.getContext(), reportBean) {
            @Override
            protected void onDeviceServiceCrash() {
                mActivity.bindDeviceService();
            }

            @Override
            protected void displayPrinterInfo(String info) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.d("1111111aaaaa");
                        setButton(true, mButton);
                    }
                });
            }

            @Override
            protected void onStartPrint() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.d("1111111ssss");
                        setButton(false, mButton);
                    }
                });
            }
        };
        printerSummary.startPrint();
    }


    private void wposPrintPledgeReport(Printer printer, PledgeReportBean.DataBean reportBean) {
        if (printer == null) {
            Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
            return;
        }

        printer.setOnEventListener(new IPrint.OnEventListener() {

            @Override
            public void onEvent(final int what, String in) {
                // TODO Auto-generated method stub
                final String info = in;
                // 回调函数中不能做UI操作，所以可以使用runOnUiThread函数来包装一下代码块
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        setButton(true, mButton);
                        final String message = PrintText
                                .getPrintErrorInfo(what, info);
                        if (message == null || message.length() < 1) {
                            return;
                        }
                        showResultInfo("打印", "打印结果信息", message);
                    }
                });
            }
        });
        setButton(false, mButton);
        PrintText.printPledgeTotal(mActivity, printer, reportBean);
    }

    //打印日结小票
    public void printReport(Printer printer, IWoyouService woyouService, PrintUtils printUtils, AidlPrinter printerDev, AidlPrint aidlPrint, Intent intentService, com.chinapnr.aidl.printer.AidlPrinter printerModule, ReportBean.DataEntity reportBean) {
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case "wpos":
                    wposPrintReport(printer, reportBean);
                    break;
                case Constants.LIANDI:
                case Constants.MIBAGPAY_LIANDI:
                case Constants.ZYTPAY_LIANDI:
                    mActivity.bindDeviceService();
                    ldPrintReport(reportBean);
                    break;
                case "shangmi":
                    smPrintReport(woyouService, reportBean);
                    break;
                case "lepos":
                    leposPrintReport(printUtils, reportBean);
                    break;
                case "lkl":
                    lklPrintReport(printerDev, reportBean);
                    break;
                case "ybx":
                    ybxPrintReport(intentService, reportBean);
                    break;
                case "a920":
                    a920PrintReport(reportBean);
                    break;
                case "yipos":
                    yiPrintReport(aidlPrint, reportBean);
                    break;
                case "hdy"://海迪云
                    hdyPrintReport(reportBean);
                    break;
                case "xdl"://新大陆
                    xdlPrintReport(mActivity, reportBean);
                    break;
                case Constants.HUIFU://汇付
                    huifuPrintReport(mActivity, printerModule, reportBean);
                    break;
                case Constants.WJY_WIZARPOS:
                case Constants.WIZARPOS:
                    if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                        YingShangPrinterUtil.printTotal(reportBean);
                    } else {
                        wizarPrintReport(reportBean);
                    }
                    break;
                case Constants.WJY_YINSHANG://万科银商
                    if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
                        wizarPrintReport(reportBean);
                    } else {
                        YingShangPrinterUtil.printTotal(reportBean);
                    }
                    break;
                /*default:
                    bluetoothPrint(billdata);
                    break;*/
            }
        }
    }

    private void yinshangPrintReport(ReportBean.DataEntity reportBean) {


        //YingShangPrinterUtil.with().printTotal(reportBean);
    }

    private void wizarPrintReport(ReportBean.DataEntity reportBean) {
        WizarPrinterUtil.with().printTotal(reportBean);
    }

    private void huifuPrintReport(BaseActivity activity, com.chinapnr.aidl.printer.AidlPrinter printerModule, ReportBean.DataEntity reportBean) {
        PrintHuifuUtil.with().printTotal(activity, printerModule, reportBean);
    }

    private void xdlPrintReport(BaseActivity activity, ReportBean.DataEntity reportBean) {
        PrinterXdlUtil.with().printTotal(activity, reportBean);
    }

    private void hdyPrintReport(ReportBean.DataEntity reportBean) {
        PrinterHdyUtil.with().printTotal(reportBean);
    }

    private void yiPrintReport(AidlPrint aidlPrint, ReportBean.DataEntity reportBean) {
        YiposPrintText.with().doPrint(aidlPrint, YiposPrintText.with().printJsonSum(reportBean), null);
    }

    private void a920PrintReport(ReportBean.DataEntity orderModel) {
        PrinterTester.getInstance().init();
        A920Printer.printTotal(mActivity, orderModel);
    }

    private void ybxPrintReport(Intent intentService, ReportBean.DataEntity orderModel) {
        intentService.putExtra("SPRT", orderModel);
        mActivity.startService(intentService);
    }

    private void lklPrintReport(final AidlPrinter printerDev, ReportBean.DataEntity orderModel) {
        PrintLKL.printSummary(printerDev, orderModel, MyApplication.getContext());
    }

    private void leposPrintReport(PrintUtils printUtils, ReportBean.DataEntity reportBean) {
        ZngPrintUtil.printTotal(MyApplication.getContext(), printUtils, reportBean);
    }

    private void smPrintReport(final IWoyouService woyouService, final ReportBean.DataEntity reportBean) {
        final ICallback callback = new ICallback.Stub() {

            @Override
            public void onRunResult(boolean success) throws RemoteException {
            }

            @Override
            public void onReturnString(final String value) throws RemoteException {
            }

            @Override
            public void onRaiseException(int code, final String msg) throws RemoteException {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });

            }
        };

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    PrintBillSuccess.pintSum(reportBean, woyouService, callback, null);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ldPrintReport(ReportBean.DataEntity reportBean) {
        /*printerSample.printSum(reportBean);
        printerSample.startPrint();*/
        printerSummary = new PrinterSummary(MyApplication.getContext(), reportBean) {

            @Override
            protected void onDeviceServiceCrash() {
                mActivity.bindDeviceService();
            }

            @Override
            protected void displayPrinterInfo(String info) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton(true, mButton);
                    }
                });
            }

            @Override
            protected void onStartPrint() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton(false, mButton);
                    }
                });
            }
        };
        printerSummary.startPrint();
    }


    private void wposPrintReport(Printer printer, ReportBean.DataEntity reportBean) {
        if (printer == null) {
            Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        printer.setOnEventListener(new IPrint.OnEventListener() {

            @Override
            public void onEvent(final int what, String in) {
                // TODO Auto-generated method stub
                final String info = in;
                // 回调函数中不能做UI操作，所以可以使用runOnUiThread函数来包装一下代码块
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        setButton(true, mButton);
                        final String message = PrintText
                                .getPrintErrorInfo(what, info);
                        if (message == null || message.length() < 1) {
                            return;
                        }
                        showResultInfo("打印", "打印结果信息", message);
                    }
                });
            }
        });
        setButton(false, mButton);
        PrintText.printTotal(mActivity, printer, reportBean);
    }

    public void print(Printer printer, IWoyouService woyouService, PrintUtils printUtils, AidlPrinter printerDev, AidlPrint aidlPrint, Intent intentService, com.chinapnr.aidl.printer.AidlPrinter printerModule, final TradeDetailBean billdata,boolean isAutoPrint) {
        billdata.setAutoPrint(isAutoPrint);
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case "wpos":
                    if (printer == null) {
                        Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    wposPrint(printer, billdata);
                    break;
                case Constants.LIANDI:
                case Constants.MIBAGPAY_LIANDI:
                case Constants.ZYTPAY_LIANDI:
                    mActivity.bindDeviceService();
                    liandiPrint(billdata);
                    break;
                case "shangmi":
                    smPrint(woyouService, billdata);
                    break;
                case "lepos":
                    leposPrint(printUtils, billdata);
                    break;
                case "lkl":
                    lklPrint(printerDev, billdata);
                    break;
                case "ybx":
                    ybxPrint(intentService, billdata);
                    break;
                case "a920":
                    a920Print(billdata);
                    break;
                case "yipos":
                    yiPrint(aidlPrint, billdata);
                    break;
                case "hdy"://海迪云
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            hdyPrint(billdata);
                        }
                    }).start();
                    break;
                case "xdl"://新大陆
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            xdlPrint(billdata);
                        }
                    }).start();
                    break;
                case Constants.HUIFU://汇付
                    huifuPrint(printerModule, billdata);
                    break;
                case Constants.WJY_WIZARPOS:
                case Constants.WIZARPOS:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            wizarPrint(billdata);
                        }
                    }).start();
                    break;
                case Constants.WJY_YINSHANG://万科银商
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            yinshangPrint(billdata);
                        }
                    }).start();
                    break;
                default:
                    if (isAutoPrint) {
                        bluetoothPrint(billdata, isAutoPrint);
                    } else {
                        CloudPrintUtil.cloudPrint(mActivity, new CloudPrintUtil.OnGetCloudDevicesListCallBack() {
                            @Override
                            public void startCloudPrint(List<DeviceBean> cloudDevices) {
                                CloudPrintUtil.startCloudPrint(mActivity, cloudDevices, billdata);
                            }

                            @Override
                            public void startBluetoothPrint() {
                                bluetoothPrint(billdata, isAutoPrint);
                            }
                        });
                    }
                    break;
            }
        }
    }

    private void liandiPrint(TradeDetailBean orderModel) {
        if (orderModel.getApiProvider() == 10) {
            easyPayPrint(orderModel);
        } else {
            ldPrint(orderModel);
        }
    }

    private void wizarPrint(TradeDetailBean orderModel) {
        if (orderModel.getApiProvider() == 10) {
            easyPayRePrint(orderModel);
//            rePrint(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY), orderModel);
        } else if (orderModel.getApiProvider() == 11) {
            wizardFukaPayRePrint(orderModel);
        } else {
            //其他支付类型打印
            if (Constants.LANDI_APOS_A8.equals(ConfigUtil.getModel())) {
                yinshangTextPrint(orderModel);
            } else {
                wizarPrintText(orderModel);
            }
        }
    }

    private void yinshangPrint(TradeDetailBean orderModel) {
        if (orderModel.getApiProvider() == 10 || (orderModel.getApiProvider() == 11)) {
            yinshangRePrint(orderModel);
        } else {
            //其他支付类型打印
            if (Constants.WIZARPOS_Q2.equals(ConfigUtil.getModel())) {
                wizarPrintText(orderModel);
            } else {
                yinshangTextPrint(orderModel);
            }
        }
    }

    private void yinshangTextPrint(TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        YingShangPrinterUtil.printOrder(orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO, true)) {
            try {
                Thread.currentThread().sleep(3500);//毫秒
                //打印第二次
                orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                YingShangPrinterUtil.printOrder(orderModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void wizarPrintText(final TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        WizarPrinterUtil.with().printText(orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO, true)) {
            try {
                Thread.currentThread().sleep(3500);//毫秒
                //打印第二次
                orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                WizarPrinterUtil.with().printText(orderModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 银商刷卡补打小票
     *
     * @param appMetaData
     * @param orderModel
     */
    /**
     * 银商pos刷卡支付
     */
    private void yinshangRePrint(TradeDetailBean orderModel) {
        String APPID = Constants.WJYS_APPID;
        String transAppName = "公共资源";
        String transAppId = "交易明细";
        JSONObject transData = new JSONObject();
        try {
            transData.put("appId", APPID);//appId
            transData.put("isNeedPrintReceipt", false);//交易结束后自动打单
            transData.put("isShowDetailPage", false);//不显示详情
            transData.put("traceNo", orderModel.getVoucherNo());
            transData.put("extOrderNo", orderModel.getOrderNo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IOnTransEndListener listener = new IOnTransEndListener() {
            @Override
            public void onEnd(String reslutmsg) {
                //交易结束后处理数据，此处略
                LogUtil.d("Jeremy-" + reslutmsg);
            }
        };
        com.ums.AppHelper.callTrans(mActivity, transAppName, transAppId, transData, listener);
    }

    /**
     * 易生刷卡补打小票
     *
     * @param appMetaData
     * @param orderModel
     */
    /**
     * 唤起慧银刷卡支付
     */
    private void easyPayRePrint(TradeDetailBean orderModel) {
        /*Map<String, String> map = new HashMap<>();
        map.put("option", "reprintTicket");
        map.put("trace", orderModel.getReqOrderNo());*/
        mActivity.unbindDeviceService();
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("net.worthtech.worthcasher", "net.worthtech.worthcasher.activity.PayService"));
        intent.putExtra("option", "reprintTicket");
        intent.putExtra("trace", orderModel.getVoucherNo());
        LogUtil.e("REQUEST_BCARD_PAY_print--" + orderModel.getVoucherNo());
        mActivity.startActivityForResult(intent, Constants.REQUEST_BCARD_REPRINT);
    }

    /**
     * 慧银福卡补打小票
     *
     * @param appMetaData
     * @param orderModel
     */

    /**
     * 唤起慧银刷卡支付
     */
    private void wizardFukaPayRePrint(TradeDetailBean orderModel) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("AppID", Constants.WJYS_APPID);
            map.put("AppName", "WJYS");
            map.put("ItemID", "WJY02");
            map.put("CustomerOID", orderModel.getOrderNo());
            map.put("trxId", orderModel.getOrderNo());
            map.put("TransType", 152);//查单、重打印
            map.put("TransAmount", StringUtils.formatFukaMoney(orderModel.getMoney() + ""));
            if (!TextUtils.isEmpty(orderModel.getOrderNo())) {
                if (orderModel.getOrderNo().length() > 16) {
                    map.put("TransIndexCode", orderModel.getOrderNo().substring(orderModel.getOrderNo().length() - 16));
                } else {
                    map.put("TransIndexCode", orderModel.getOrderNo());
                }
            }
            map.put("ReqTransDate", DateUtil.formatYYMD(System.currentTimeMillis()));
            map.put("ReqTransTime", DateUtil.formatDateToHHmmss(DateUtil.formatTime(System.currentTimeMillis())));
            map.put("OriTraceNo", orderModel.getVoucherNo());
            map.put("NoPrintReceipt", 0);
            MainActivity.instance.getCupService().transact(new Gson().toJson(map));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void easyPayPrint(TradeDetailBean orderModel) {
        EasyPrintUtils.easyPayPrint(mActivity, orderModel, 0);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            try {
                Thread.currentThread().sleep(3000);//毫秒
                //打印第二次
                EasyPrintUtils.easyPayPrint(mActivity, orderModel, 1);
            } catch (Exception e) {

            }
        }
    }

    private void huifuPrint(final com.chinapnr.aidl.printer.AidlPrinter printerModule, final TradeDetailBean orderModel) {
        if (orderModel.getApiProvider() == 10) {
            rePrint(AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY), orderModel);
        } else {
            //其他支付类型打印
            huifuPrintText(printerModule, orderModel);
        }
    }

    public void huifuPrintText(final com.chinapnr.aidl.printer.AidlPrinter printerModule, final TradeDetailBean orderModel) {
        orderModel.setPartner(UIUtils.getString(R.string.tv_pay_mch_stub));
        PrintHuifuUtil.with().print(mActivity, printerModule, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    orderModel.setPartner(UIUtils.getString(R.string.tv_pay_user_stub));
                    PrintHuifuUtil.with().print(mActivity, printerModule, orderModel);
                }
            }, 4000);
        }
    }

    /**
     * 汇付补打小票
     *
     * @param appMetaData
     * @param orderModel
     */
    private void rePrint(String appMetaData, TradeDetailBean orderModel) {
        StringBuilder builderUri = new StringBuilder();
        switch (appMetaData) {
            case Constants.HUIFU:
                builderUri.append("payment://com.pnr.pospp/rePrint?");
                builderUri.append("memberId=").append(MyApplication.getPosMemberId());
                if (!TextUtils.isEmpty(MyApplication.getPosEmpAccount())) {
                    builderUri.append("&merOperId=").append(MyApplication.getPosEmpAccount());
                }
                builderUri.append("&merOperIdPrinted=").append("0");
                if (orderModel.isPay()) {
                    builderUri.append("&selfOrdId=").append(orderModel.getReqOrderNo());
                    builderUri.append("&isSale=1");
                } else {
                    builderUri.append("&selfOrdId=").append(orderModel.getReqRefundNo());
                    builderUri.append("&isSale=0");
                }
                LogUtil.d("builderUri====rePrint=" + builderUri.toString());
                startActivityPrint(builderUri.toString());
                break;
        }
    }

    /**
     * 唤起pos打印
     *
     * @param uriStr
     */
    private void startActivityPrint(String uriStr) {
        Uri uri = Uri.parse(uriStr);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        mActivity.startActivityForResult(intent, Constants.REQUEST_BCARD_REPRINT);
    }

    private void xdlPrint(final TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        PrinterXdlUtil.with().print(mActivity, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            try {
                Thread.currentThread().sleep(3000);//毫秒
                //打印第二次
                orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                PrinterXdlUtil.with().print(mActivity, orderModel);
            } catch (Exception e) {

            }
        }
    }

    private void hdyPrint(final TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        PrinterHdyUtil.with().print(mActivity, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            try {
                Thread.currentThread().sleep(3500);//毫秒
                //打印第二次
                orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                PrinterHdyUtil.with().print(mActivity, orderModel);
            } catch (Exception e) {

            }
        }
    }

    private void yiPrint(final AidlPrint aidlPrint, final TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        YiposPrintText.with().doPrint(aidlPrint, YiposPrintText.with().printJson(orderModel), null);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //打印第二次
                    orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                    YiposPrintText.with().doPrint(aidlPrint, YiposPrintText.with().printJson(orderModel), null);
                }
            }, 4000);
        }

    }


    public void a920Print(final TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        PrinterTester.getInstance().init();
        A920Printer.print(mActivity, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //打印第二次
                    orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                    PrinterTester.getInstance().init();
                    A920Printer.print(mActivity, orderModel);
                }
            }, 2000);
        }
    }

    public void ybxPrint(final Intent intentService, final TradeDetailBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        intentService.putExtra("SPRT", orderModel);
        mActivity.startService(intentService);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //打印第二次
                    orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                    intentService.putExtra("SPRT", orderModel);
                    mActivity.startService(intentService);
                }
            }, 5000);
        }
    }

    public void lklPrint(final AidlPrinter printerDev, final TradeDetailBean orderModel) {
        orderModel.setPartner(UIUtils.getString(R.string.tv_pay_mch_stub));
        PrintLKL.printText(printerDev, orderModel, MyApplication.getContext());

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    orderModel.setPartner(UIUtils.getString(R.string.tv_pay_user_stub));

                    PrintLKL.printText(printerDev, orderModel, MyApplication.getContext());

                }
            }, 3000);
        }
    }

    public void leposPrint(final PrintUtils printUtils, final TradeDetailBean billdata) {
        if (printUtils == null) {
            Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
            return;
        }
        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        ZngPrintUtil.print(mActivity, printUtils, billdata);

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setButton(false, mButton);
                        }
                    });
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    ZngPrintUtil.print(mActivity, printUtils, billdata);

                }
            }, 1000);
        }
    }

    public void wposPrint(final Printer printer, final TradeDetailBean billdata) {
        if (printer == null) {
            Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
            return;
        }

        printer.setOnEventListener(new IPrint.OnEventListener() {

            @Override
            public void onEvent(final int what, String in) {
                // TODO Auto-generated method stub
                final String info = in;
                // 回调函数中不能做UI操作，所以可以使用runOnUiThread函数来包装一下代码块
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        setButton(true, mButton);
                        final String message = PrintText.getPrintErrorInfo(what, info);
                        if (message == null || message.length() < 1) {
                            return;
                        }
                        showResultInfo("打印", "打印结果信息", message);
                    }
                });
            }
        });

        setButton(false, mButton);
        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        PrintText.print(mActivity, printer, billdata);

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setButton(false, mButton);
                        }
                    });
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    PrintText.print(mActivity, printer, billdata);

                }
            }, 4000);
        }

    }


    public void ldPrint(final TradeDetailBean billdata) {
        printerSample = new PrinterApos1(mActivity, billdata) {
            @Override
            protected void onDeviceServiceCrash() {
                mActivity.bindDeviceService();
            }

            @Override
            protected void displayPrinterInfo(String info) {
                Log.i("zhouwei", "打印完成" + info);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton(true, mButton);
                    }
                });
            }

            @Override
            protected void onStartPrint() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton(false, mButton);
                    }
                });
            }
        };

        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        printerSample.startPrint();

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    printerSample.startPrint();
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, 4000);
        }
    }

    private void setButton(boolean enable, Button button) {
        button.setEnabled(enable);
    }

    private void showResultInfo(String operInfo, String titleHeader, String info) {
        if (mWposDialog == null) {
            mWposDialog = new CommonNoticeDialog(mActivity, operInfo,
                    titleHeader + ":" + info, getString(R.string.tv_enter));
            DialogHelper.resize(mActivity, mWposDialog);
        }
        if (mActivity.hasWindowFocus()) {
            mWposDialog.show();
        }
    }

    public void smPrint(final IWoyouService woyouService, final TradeDetailBean billdata) {
        final ICallback callback = new ICallback.Stub() {

            @Override
            public void onRunResult(boolean success) throws RemoteException {
            }

            @Override
            public void onReturnString(final String value) throws RemoteException {
            }

            @Override
            public void onRaiseException(int code, final String msg) throws RemoteException {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
            }
        };

        mActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {
                    WindowManager wm = mActivity.getWindowManager();
                    int width = wm.getDefaultDisplay().getWidth();
                    final int w = (int) (width * 0.65);

                    billdata.setPartner(getString(R.string.tv_pay_mch_stub));
                    PrintBillSuccess.print(billdata, woyouService, callback);

                    if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                try {
                                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                                    PrintBillSuccess.print(billdata, woyouService, callback);
                                } catch (Exception e) {
                                }
                            }
                        };
                        Timer timer = new Timer();
                        timer.schedule(task, 4000);
                    }
                } catch (Exception e) {
                    Log.e("hehui", "blue_print--" + e);
                }

            }
        });
    }

    /**
     * 蓝牙打印收款退款交易详情信息
     *
     * @param isAutoPrint 自动打印
     */
    public void bluetoothPrint(final TradeDetailBean billdata, boolean isAutoPrint) {
        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            mActivity.showPrintDialog(1);
            return;
        }
        /*if (MyApplication.bluetoothSocket != null && MyApplication.bluetoothSocket.isConnected()) {
            //打印第二联
            MtaUtils.mtaId(mActivity, "C004");
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        BluePrintUtil.print(billdata);
                    }
                }).start();
                if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
                    Thread.sleep(4000);
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
//                    BluePrintUtil.print(billdata);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            BluePrintUtil.print(billdata);
                        }
                    }).start();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {*/
        if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
            MtaUtils.mtaId(mActivity, "C004");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    boolean isSucc = BluePrintUtil.blueConnent(1, mActivity);
                    if (isSucc) {
                        try {
                            BluePrintUtil.print(billdata, isAutoPrint);
                            if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
                                try {
                                    Thread.sleep(4000);
                                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                                    BluePrintUtil.print(billdata, isAutoPrint);
                                } catch (InterruptedException e) {
                                    Thread.currentThread().interrupt();
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                                    mActivity.showPrintDialog(1);
                                } else {
                                    //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                                    LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                                }
                            }
                        });
                    }

                }
            }.start();

        } else {
            mActivity.showPrintDialog(1);
        }
//        }
    }

    /**
     * 打印预授权小票(押金收款交易详情)
     *
     * @param printer
     * @param woyouService
     * @param printUtils
     * @param printerDev
     * @param aidlPrint
     * @param intentService
     * @param printerModule
     * @param pledgePayBean
     * @param isAutoPrint   是否是自动打印（不需要手动点击页面按钮）
     */
    public void pledgePrint(Printer printer, IWoyouService woyouService, PrintUtils printUtils, AidlPrinter printerDev, AidlPrint aidlPrint, Intent intentService, com.chinapnr.aidl.printer.AidlPrinter printerModule, final PledgePayBean.DataBean pledgePayBean,boolean isAutoPrint) {
        pledgePayBean.setAutoPrint(isAutoPrint);
        if ("2".equals(pledgePayBean.getOptStatus())) {
            pledgePayBean.setToPay(true);
        } else {
            pledgePayBean.setToPay(false);
        }
        if (AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY) != null) {
            String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
            switch (installChannel) {
                case "wpos":
                    if (printer == null) {
                        Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    wposPledgePrint(printer, pledgePayBean);
                    break;
                case Constants.ZYTPAY_LIANDI:
                case Constants.LIANDI:
                case Constants.MIBAGPAY_LIANDI:
                    mActivity.bindDeviceService();
                    ldPledgePrint(pledgePayBean);
                    break;
                case "shangmi":
                    smPledgePrint(woyouService, pledgePayBean);
                    break;
                case "lepos":
                    leposPledgePrint(printUtils, pledgePayBean);
                    break;
                case "lkl":
                    lklPledgePrint(printerDev, pledgePayBean);
                    break;
                case "ybx":
                    ybxPledgePrint(intentService, pledgePayBean);
                    break;
                case "a920":
                    a920PledgePrint(pledgePayBean);
                    break;
                case "yipos":
                    yiPledgePrint(aidlPrint, pledgePayBean);
                    break;
                case "hdy"://海迪云
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            hdyPledgePrint(pledgePayBean);
                        }
                    }).start();
                    break;
                case "xdl"://新大陆
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            xdlPledgePrint(pledgePayBean);
                        }
                    }).start();
                    break;
                case Constants.HUIFU://汇付
                    huifuPledgePrint(printerModule, pledgePayBean);
                    break;
                case Constants.WIZARPOS://慧银
                    wizarposPledgePrint(pledgePayBean);
                    break;
                case Constants.WJY_WIZARPOS://万科慧银
                case Constants.WJY_YINSHANG://万科银商
                    //TODO 银商pos打印方法
                    break;
                default:
                    if (isAutoPrint) {
                        bluetoothPledgePrint(pledgePayBean, isAutoPrint);
                    } else {
                        CloudPrintUtil.cloudPrint(mActivity, new CloudPrintUtil.OnGetCloudDevicesListCallBack() {
                            @Override
                            public void startCloudPrint(List<DeviceBean> cloudDevices) {
                                CloudPrintUtil.startCloudPrint(mActivity, cloudDevices, pledgePayBean);
                            }

                            @Override
                            public void startBluetoothPrint() {
                                bluetoothPledgePrint(pledgePayBean, isAutoPrint);
                            }
                        });
                    }
                    break;
            }
        }
    }

    public void wizarposPledgePrint(final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(UIUtils.getString(R.string.tv_pay_mch_stub));
        WizarPrinterUtil.with().pledgePrint(orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    orderModel.setPartner(UIUtils.getString(R.string.tv_pay_user_stub));
                    WizarPrinterUtil.with().pledgePrint(orderModel);
                }
            }, 4000);
        }
    }

    public void huifuPledgePrint(final com.chinapnr.aidl.printer.AidlPrinter printerModule, final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(UIUtils.getString(R.string.tv_pay_mch_stub));
        PrintHuifuUtil.with().pledgePrint(mActivity, printerModule, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    orderModel.setPartner(UIUtils.getString(R.string.tv_pay_user_stub));
                    PrintHuifuUtil.with().pledgePrint(mActivity, printerModule, orderModel);
                }
            }, 4000);
        }
    }

    private void xdlPledgePrint(final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        PrinterXdlUtil.with().pledgePrint(mActivity, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            try {
                Thread.currentThread().sleep(3000);//毫秒
                //打印第二次
                orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                PrinterXdlUtil.with().pledgePrint(mActivity, orderModel);
            } catch (Exception e) {

            }
        }
    }

    private void hdyPledgePrint(final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        PrinterHdyUtil.with().pledgePrint(mActivity, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            try {
                Thread.currentThread().sleep(3500);//毫秒
                //打印第二次
                orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                PrinterHdyUtil.with().pledgePrint(mActivity, orderModel);
            } catch (Exception e) {

            }
        }
    }

    private void yiPledgePrint(final AidlPrint aidlPrint, final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        YiposPrintText.with().doPrint(aidlPrint, YiposPrintText.with().pledgePrintJson(orderModel), null);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //打印第二次
                    orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                    YiposPrintText.with().doPrint(aidlPrint, YiposPrintText.with().pledgePrintJson(orderModel), null);
                }
            }, 4000);
        }

    }


    public void a920PledgePrint(final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        PrinterTester.getInstance().init();
        A920Printer.pledgePrint(mActivity, orderModel);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //打印第二次
                    orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                    PrinterTester.getInstance().init();
                    A920Printer.pledgePrint(mActivity, orderModel);
                }
            }, 2000);
        }
    }

    public void ybxPledgePrint(final Intent intentService, final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
        intentService.putExtra("SPRT", orderModel);
        mActivity.startService(intentService);
        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //打印第二次
                    orderModel.setPartner(getString(R.string.tv_pay_user_stub));
                    intentService.putExtra("SPRT", orderModel);
                    mActivity.startService(intentService);
                }
            }, 5000);
        }
    }

    public void lklPledgePrint(final AidlPrinter printerDev, final PledgePayBean.DataBean orderModel) {
        orderModel.setPartner(UIUtils.getString(R.string.tv_pay_mch_stub));
        PrintLKL.printPledgeText(printerDev, orderModel, MyApplication.getContext());

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    orderModel.setPartner(UIUtils.getString(R.string.tv_pay_user_stub));
                    PrintLKL.printPledgeText(printerDev, orderModel, MyApplication.getContext());

                }
            }, 3000);
        }
    }

    public void leposPledgePrint(final PrintUtils printUtils, final PledgePayBean.DataBean billdata) {
        if (printUtils == null) {
            Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
            return;
        }
        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        ZngPrintUtil.pledgerint(mActivity, printUtils, billdata);

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setButton(false, mButton);
                        }
                    });
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    ZngPrintUtil.pledgerint(mActivity, printUtils, billdata);

                }
            }, 1000);
        }
    }

    public void wposPledgePrint(final Printer printer, final PledgePayBean.DataBean billdata) {
        if (printer == null) {
            Toast.makeText(mActivity, "尚未初始化打印sdk，请稍后再试", Toast.LENGTH_SHORT).show();
            return;
        }

        printer.setOnEventListener(new IPrint.OnEventListener() {

            @Override
            public void onEvent(final int what, String in) {
                // TODO Auto-generated method stub
                final String info = in;
                // 回调函数中不能做UI操作，所以可以使用runOnUiThread函数来包装一下代码块
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        setButton(true, mButton);
                        final String message = PrintText.getPrintErrorInfo(what, info);
                        if (message == null || message.length() < 1) {
                            return;
                        }
                        showResultInfo("打印", "打印结果信息", message);
                    }
                });
            }
        });

        setButton(false, mButton);
        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        PrintText.pledgePrint(mActivity, printer, billdata);

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setButton(false, mButton);
                        }
                    });
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    PrintText.pledgePrint(mActivity, printer, billdata);

                }
            }, 4000);
        }

    }


    public void ldPledgePrint(final PledgePayBean.DataBean billdata) {
        printerPledgeSample = new PrinterPledgeApos(mActivity, billdata) {
            @Override
            protected void onDeviceServiceCrash() {
                mActivity.bindDeviceService();
            }

            @Override
            protected void displayPrinterInfo(String info) {
                Log.i("zhouwei", "打印完成" + info);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton(true, mButton);
                    }
                });
            }

            @Override
            protected void onStartPrint() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton(false, mButton);
                    }
                });
            }
        };

        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        printerPledgeSample.startPrint();

        if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    printerPledgeSample.startPrint();
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, 4000);
        }
    }

    public void smPledgePrint(final IWoyouService woyouService, final PledgePayBean.DataBean billdata) {
        final ICallback callback = new ICallback.Stub() {

            @Override
            public void onRunResult(boolean success) throws RemoteException {
            }

            @Override
            public void onReturnString(final String value) throws RemoteException {
            }

            @Override
            public void onRaiseException(int code, final String msg) throws RemoteException {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
            }
        };

        mActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {
                    WindowManager wm = mActivity.getWindowManager();
                    int width = wm.getDefaultDisplay().getWidth();
                    final int w = (int) (width * 0.65);

                    billdata.setPartner(getString(R.string.tv_pay_mch_stub));
                    PrintBillSuccess.pledgePrint(billdata, woyouService, callback,
                            CreateOneDiCodeUtil.createCode(billdata.getAuthNo(), w, 180));

                    if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                try {
                                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                                    PrintBillSuccess.pledgePrint(billdata, woyouService, callback,
                                            CreateOneDiCodeUtil.createCode(billdata.getAuthNo(), w, 180));
                                } catch (Exception e) {
                                }
                            }
                        };
                        Timer timer = new Timer();
                        timer.schedule(task, 4000);
                    }
                } catch (Exception e) {
                    Log.e("hehui", "blue_print--" + e);
                }

            }
        });
    }

    /***
     * 蓝牙打印押金收款，押金解冻的交易详情信息
     * */
    public void bluetoothPledgePrint(PledgePayBean.DataBean billdata, boolean isAutoPrint) {
        billdata.setPartner(getString(R.string.tv_pay_mch_stub));
        if (!MyApplication.getDeviceEnable(1)) {//关闭蓝牙打印
            mActivity.showPrintDialog(1);
            return;
        }
        /*if (MyApplication.bluetoothSocket != null && MyApplication.bluetoothSocket.isConnected()) {
            //打印第二联
            MtaUtils.mtaId(mActivity, "C004");
            try {
                BluePrintUtil.pledgePrint(billdata);
                if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
                    Thread.sleep(4000);
                    billdata.setPartner(getString(R.string.tv_pay_user_stub));
                    BluePrintUtil.pledgePrint(billdata);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {*/
        if (!StringUtils.isEmptyOrNull(MyApplication.getBluetoothAddress(1))) {
            MtaUtils.mtaId(mActivity, "C004");
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    boolean isSucc = BluePrintUtil.blueConnent(1, mActivity);
                    if (isSucc) {
                        try {
                            BluePrintUtil.pledgePrint(billdata, isAutoPrint);
                            if (SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_TYPE_TWO)) {
                                Thread.sleep(4000);
                                billdata.setPartner(getString(R.string.tv_pay_user_stub));
                                BluePrintUtil.pledgePrint(billdata, isAutoPrint);
                            }
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            e.printStackTrace();
                        }

                    } else {
                        if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mActivity.showPrintDialog(1);
                                }
                            });

                        } else {
                            //蓝牙未打开,BluePrintUtil里有对蓝牙未打开的处理
                            LogUtil.d(UIUtils.getString(R.string.tx_blue_no_open));
                        }
                    }
                }
            }.start();

        } else {
            mActivity.showPrintDialog(1);
        }
//        }
    }
}
