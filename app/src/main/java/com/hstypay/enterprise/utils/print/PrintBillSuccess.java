package com.hstypay.enterprise.utils.print;

import android.graphics.Bitmap;
import android.os.RemoteException;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import woyou.aidlservice.jiuiv5.ICallback;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * 打印小票 <一句话功能简述> <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2016-4-20]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PrintBillSuccess {

    public static void pintSum(ReportBean.DataEntity info, IWoyouService woyouService,
                               ICallback callback, Bitmap bitmap) throws RemoteException {

        woyouService.setAlignment(1, callback);
        if (info.getType() == 1) {
            woyouService.printText(UIUtils.getString(R.string.print_pay_report_title) + "\n", callback);
        } else if (info.getType() == 2) {
            woyouService.printText(UIUtils.getString(R.string.print_consume_report_title) + "\n", callback);
        }else if (info.getType() == 3){
            woyouService.printText(UIUtils.getString(R.string.print_fk_consume_report_title) + "\n", callback);
        }
        woyouService.printText(" \n", callback);
        woyouService.setAlignment(0, callback);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (MyApplication.getMerchantName().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(MyApplication.getMerchantName() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", callback);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            woyouService.printText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n",
                    callback);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": 全部门店\n", callback);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": \n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (info.getStoreName().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": \n", callback);
                woyouService.printText(info.getStoreName() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n",
                        callback);
            }
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            woyouService.printText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n", callback);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(cashierTitle + ": \n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (info.getCashierName().length() > 12) {
                woyouService.printText(cashierTitle + ": \n", callback);
                woyouService.printText(info.getCashierName() + "\n", callback);
            } else {
                woyouService.printText(cashierTitle + ": " + info.getCashierName() + "\n", callback);
            }
        }
        if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
            if (info.getCashierPointName().length() > 16) {
                StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                String one = sbf.substring(0, 16);
                String two = sbf.substring(16, info.getCashierPointName().length());
                woyouService.printText("收银点: " + "\n", callback);
                woyouService.printText(one + "\n", callback);
                woyouService.printText(two + "\n", callback);
            } else if (info.getCashierPointName().length() > 12) {
                woyouService.printText("收银点: " + "\n", callback);
                woyouService.printText(info.getCashierPointName() + "\n", callback);
            } else {
                woyouService.printText("收银点: " + info.getCashierPointName() + "\n", callback);
            }
        }

        woyouService.printText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\n", callback);
        // 结束时间
        woyouService.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n", callback);

        woyouService.printText("\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_report_total_title) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);

        // 总金额
        if (info.getType() == 1) {
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔\n"), callback);
        } else if (info.getType() == 2) {
           /* woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：",DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔\n"), callback);*/

            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔\n"), callback);

           /* woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔\n"), callback);*/
        }else if (info.getType() == 3){
            //饭卡消费
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+""), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元\n"), callback);

            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔\n"), callback);
            woyouService.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔\n"), callback);
        }

        if (info.getType() == 1) {
            List<ReportBean.DataEntity.ListEntity> list = info.getList();
            if (list != null && list.size() > 0) {
                woyouService.printText("\n", callback);
                woyouService.printText(UIUtils.getString(R.string.print_trade_type_list_title) + ": \n", callback);
                woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);

                for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                    woyouService.printText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元\n"), callback);
                    woyouService.printText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔\n"), callback);
                }
            }
        }

        woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n", callback);
        woyouService.printText("\n\n\n\n", callback);
    }

    public static void pintPledgeSum(PledgeReportBean.DataBean info, IWoyouService woyouService,
                                     ICallback callback, Bitmap bitmap) throws RemoteException {

        woyouService.setAlignment(1, callback);
        woyouService.printText(UIUtils.getString(R.string.print_title_pledge_report) + "\n", callback);

        woyouService.printText(" \n", callback);
        woyouService.setAlignment(0, callback);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (MyApplication.getMerchantName().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(MyApplication.getMerchantName() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", callback);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            woyouService.printText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n",
                    callback);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": 全部门店\n", callback);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": \n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (info.getStoreName().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": \n", callback);
                woyouService.printText(info.getStoreName() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n", callback);
            }
        }

        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            woyouService.printText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n", callback);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(cashierTitle + ": \n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (info.getCashierName().length() > 12) {
                woyouService.printText(cashierTitle + ": \n", callback);
                woyouService.printText(info.getCashierName() + "\n", callback);
            } else {
                woyouService.printText(cashierTitle + ": " + info.getCashierName() + "\n", callback);
            }
        }

        woyouService.printText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\n", callback);
        // 结束时间
        woyouService.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);

        woyouService.printText(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_pledge_refund_money) + ": " + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_pledge_refund_count) + ": " + info.getCntFreeMoney() + "笔\n", callback);
//        woyouService.printText(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元\n", callback);

        woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n", callback);
        woyouService.printText("\n\n\n\n", callback);
    }

    public static void print(final TradeDetailBean order, final IWoyouService woyouService, final ICallback callback) throws RemoteException {

        woyouService.setAlignment(1, callback);

        if (order.getTradeType() == 1) {
            woyouService.printText(UIUtils.getString(R.string.print_recharge_title) + "\n", callback);
        } else if (order.getTradeType() == 2) {
            woyouService.printText(UIUtils.getString(R.string.print_verification_title) + "\n", callback);
        } else {
            if (!order.isPay()) {
                woyouService.printText(UIUtils.getString(R.string.print_refund_title) + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.print_pay_title) + "\n", callback);
            }
        }

        woyouService.printText("\n" + order.getPartner() + "             请妥善保存", callback);
        woyouService.printText("\n" + UIUtils.getString(R.string.print_single_horizontal), callback);
        woyouService.setAlignment(0, callback);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (MyApplication.getMerchantName().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(MyApplication.getMerchantName() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", callback);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            woyouService.printText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", callback);
        }

        String storeNameTitle;
        if (order.isPay()) {
            storeNameTitle = UIUtils.getString(R.string.trade_store_name);
        } else {
            storeNameTitle = UIUtils.getString(R.string.refund_store_name);
        }
        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(storeNameTitle + "：" + "\n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                woyouService.printText(storeNameTitle + "：" + "\n", callback);
                woyouService.printText(order.getStoreMerchantIdCnt() + "\n", callback);
            } else {
                woyouService.printText(storeNameTitle + "："
                        + order.getStoreMerchantIdCnt() + "\n", callback);

            }
        }
        if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
            if (order.getCashPointName().length() > 16) {
                String str = order.getCashPointName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText("收银点: ", callback);
                woyouService.printText(one, callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two, callback);
            } else if (order.getCashPointName().length() > 11) {
                woyouService.printText("收银点: ", callback);
                woyouService.printText(order.getCashPointName(), callback);
            } else {
                woyouService.printText("收银点: " + order.getCashPointName(), callback);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
            woyouService.printText(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()) + "\n", callback);
        }
        if (!order.isPay()) {
            woyouService.printText(UIUtils.getString(R.string.print_refund_no_title) + ":\n", callback);
            woyouService.printText(order.getRefundNo() + "\n", callback);
            woyouService.printText(UIUtils.getString(R.string.print_refund_time_title) + ": \n", callback);
            woyouService.printText(order.getRefundTime() + "\n", callback);

            String refundUser = UIUtils.getString(R.string.print_refund_user_title);
            if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                if (order.getRefundUser().length() > 16) {
                    woyouService.printText(refundUser + ": \n", callback);
                    StringBuffer sbf = new StringBuffer(order.getRefundUser());
                    String user = sbf.substring(0, 16);
                    woyouService.printText(user + "\r\n", callback);
                    String printuser = sbf.substring(16, order.getRefundUser().length());
                    woyouService.printText(printuser + "\r\n", callback);
                } else if (order.getRefundUser().length() > 12) {
                    woyouService.printText(refundUser + ": \n", callback);
                    woyouService.printText(order.getRefundUser() + "\n", callback);
                } else {
                    woyouService.printText(refundUser + ": " + order.getRefundUser() + "\n", callback);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                if (order.getRefundUserRealName().length() > 16) {
                    woyouService.printText(refundUser + ": \n", callback);
                    StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                    String user = sbf.substring(0, 16);
                    woyouService.printText(user + "\r\n", callback);
                    String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                    woyouService.printText(printuser + "\r\n", callback);
                } else if (order.getRefundUserRealName().length() > 12) {
                    woyouService.printText(refundUser + ": \n", callback);
                    woyouService.printText(order.getRefundUserRealName() + "\n", callback);
                } else {
                    woyouService.printText(refundUser + ": " + order.getRefundUserRealName() + "\n", callback);
                }
            }
            woyouService.printText(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()) + "\n", callback);
            if (order.getRefundMoney() > 0) {
                woyouService.printText(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元" + "\n", callback);
            }
            woyouService.printText(UIUtils.getString(R.string.print_refund_instruction) + "\n", callback);
        } else {
            if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                woyouService.printText(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": \n", callback);
                woyouService.printText(order.getTransactionId() + "\n", callback);
            }
            if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                woyouService.printText(UIUtils.getString(R.string.tv_print_order_no) + ": \n", callback);
                woyouService.printText(order.getOrderNo() + "\n", callback);
            }
            woyouService.printText(UIUtils.getString(R.string.print_trade_status_title) + ": " + OrderStringUtil.getTradeStateString(order.getTradeState()) + "\n", callback);
            woyouService.printText(UIUtils.getString(R.string.print_trade_type_title) + ": " + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n", callback);
            try {
                woyouService.printText(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime() + "\n", callback);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    woyouService.printText(cashierTitle + ": \n", callback);
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String user = sbf.substring(0, 16);
                    woyouService.printText(user + "\n", callback);
                    String printuser = sbf.substring(16, order.getCashierName().length());
                    woyouService.printText(printuser + "\n", callback);
                } else if (order.getCashierName().length() > 12) {
                    woyouService.printText(cashierTitle + ": \n", callback);
                    woyouService.printText(order.getCashierName() + "\n", callback);
                } else {
                    woyouService.printText(cashierTitle + ": " + order.getCashierName() + "\n", callback);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                if (order.getOpUserRealName().length() > 16) {
                    woyouService.printText(cashierTitle + ": \n", callback);
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String user = sbf.substring(0, 16);
                    woyouService.printText(user + "\n", callback);
                    String printuser = sbf.substring(16, order.getOpUserRealName().length());
                    woyouService.printText(printuser + "\n", callback);
                } else if (order.getOpUserRealName().length() > 12) {
                    woyouService.printText(cashierTitle + ": \n", callback);
                    woyouService.printText(order.getOpUserRealName() + "\n", callback);
                } else {
                    woyouService.printText(cashierTitle + ": " + order.getOpUserRealName() + "\n", callback);
                }
            }
            woyouService.printText(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元" + "\n", callback);
            woyouService.printText(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元" + "\n", callback);
            woyouService.printText(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元" + "\n", callback);
        }

        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            woyouService.printText(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach() + "\n", callback);
        }

        woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", callback);
        //顾客签名
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
            woyouService.printText(UIUtils.getString(R.string.print_client_sign_title) + "：\n", callback);
        }

        woyouService.printText(" \n", callback);
        woyouService.setAlignment(1, callback);
        if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
            String printCode;
            if (ConfigUtil.printCodeBillEnable()) {
                printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            } else {
                printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            }
            woyouService.printText(ConfigUtil.getPrintCodeTitle() + "\n\n", callback);
            woyouService.printQRCode(printCode, 7, 2, callback);
        }
        if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                woyouService.printText("\n" + UIUtils.getString(R.string.print_single_horizontal), callback);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                if (!TextUtils.isEmpty(title)) {
                    if (title.length() > 16) {
                        StringBuffer sbf = new StringBuffer(title);
                        String one = sbf.substring(0, 16);
                        woyouService.printText(one + "\n", callback);
                        String two = sbf.substring(16, title.length());
                        woyouService.printText(two + "\n", callback);
                    } else {
                        woyouService.printText(title + "\n", callback);
                    }
                }
                woyouService.printText(" \n", callback);
                woyouService.printQRCode(activeUrl, 7, 2, callback);
            }
        }
        woyouService.setAlignment(1, callback);
        woyouService.printText("\n" + (order.isAutoPrint() ? "自动打印" : "手动打印")+ "\n", callback);
        woyouService.printText(" \n\n\n", callback);
    }

    public static void pledgePrint(final PledgePayBean.DataBean order, final IWoyouService woyouService,
                                   final ICallback callback, Bitmap bitmap) throws RemoteException {
        woyouService.setAlignment(1, callback);
        if (order.isToPay()) {
            woyouService.printText(UIUtils.getString(R.string.print_title_pledge_pay) + "\n", callback);
        } else {
            woyouService.printText(UIUtils.getString(R.string.print_title_pledge) + "\n", callback);
        }

        woyouService.printText("\n" + order.getPartner() + "             请妥善保存", callback);
        woyouService.printText("\n" + UIUtils.getString(R.string.print_single_horizontal), callback);
        woyouService.setAlignment(0, callback);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (MyApplication.getMerchantName().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + "\n", callback);
                woyouService.printText(MyApplication.getMerchantName() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", callback);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            woyouService.printText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", callback);
        }

        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                woyouService.printText(UIUtils.getString(R.string.trade_store_name) + "：" + "\n", callback);
                woyouService.printText(one + "\n", callback);
                String two = sbf.substring(16, str.length());
                woyouService.printText(two + "\r\n", callback);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                woyouService.printText(UIUtils.getString(R.string.trade_store_name) + "：" + "\n", callback);
                woyouService.printText(order.getStoreMerchantIdCnt() + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.trade_store_name) + "："
                        + order.getStoreMerchantIdCnt() + "\n", callback);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
            woyouService.printText(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()) + "\n", callback);
        }
        try {
            woyouService.printText(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime() + "\n", callback);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
            woyouService.printText(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：" + "\n", callback);
            woyouService.printText(order.getOutTransactionId() + "\n", callback);
        }
        if (!TextUtils.isEmpty(order.getAuthNo())) {
            woyouService.printText(UIUtils.getString(R.string.tv_pledge_order_no) + ": " + "\n", callback);
            woyouService.printText(order.getAuthNo() + "\n", callback);
        }
        woyouService.printText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()) + "\n", callback);

        if (!order.isToPay()) {
            if (!TextUtils.isEmpty(order.getMoney())) {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元" + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元" + "\n", callback);
            }
        }
        if (order.isToPay()) {
            if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元" + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元" + "\n", callback);
            }
            if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元" + "\n", callback);
            } else {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元" + "\n", callback);
            }
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    woyouService.printText(cashierTitle + ": " + "\n", callback);
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String oneUser = sbf.substring(0, 16);
                    woyouService.printText(oneUser + "\n", callback);
                    String twoUser = sbf.substring(16, order.getCashierName().length());
                    woyouService.printText(twoUser + "\n", callback);
                }
                if (order.getCashierName().length() > 12) {
                    woyouService.printText(cashierTitle + ": " + "\n", callback);
                    woyouService.printText(order.getCashierName() + "\n", callback);
                } else {
                    woyouService.printText(cashierTitle + ": " + order.getCashierName() + "\n", callback);
                }
            }
        } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
            String opUserTitle;
            if (order.isToPay()) {
                opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
            } else {
                opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
            }
            if (order.getOpUserRealName().length() > 16) {
                woyouService.printText(opUserTitle + "\n", callback);
                StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                String oneUser = sbf.substring(0, 16);
                woyouService.printText(oneUser + "\n", callback);
                String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                woyouService.printText(twoUser + "\n", callback);
            }
            if (order.getOpUserRealName().length() > 12) {
                woyouService.printText(opUserTitle + "\n", callback);
                woyouService.printText(order.getOpUserRealName() + "\n", callback);
            } else {
                woyouService.printText(opUserTitle + order.getOpUserRealName() + "\n", callback);
            }
        }
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            woyouService.printText(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach()
                    + "\n", callback);
        }

        woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);
        woyouService.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", callback);
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
            woyouService.printText(UIUtils.getString(R.string.print_client_sign_title) + ": \n", callback);
        }

        woyouService.printText(" \n", callback);
        woyouService.setAlignment(1, callback);
        if (!TextUtils.isEmpty(order.getAuthNo())) {
            if (!order.isToPay()) {
                woyouService.printText(UIUtils.getString(R.string.print_pledge_scan_title) + "\n\n", callback);
            }
            if (ConfigUtil.printCodeBillEnable()) {
                woyouService.printQRCode(Constants.URL_PRINT_QRCODE + order.getAuthNo(), 7, 2, callback);
            } else {
                woyouService.printQRCode(order.getAuthNo(), 7, 2, callback);
            }
        }
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                woyouService.printText("\n" + UIUtils.getString(R.string.print_single_horizontal), callback);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                if (!TextUtils.isEmpty(title)) {
                    if (title.length() > 16) {
                        StringBuffer sbf = new StringBuffer(title);
                        String one = sbf.substring(0, 16);
                        woyouService.printText(one + "\n", callback);
                        String two = sbf.substring(16, title.length());
                        woyouService.printText(two + "\n", callback);
                    } else {
                        woyouService.printText(title + "\n", callback);
                    }
                }
                woyouService.printText(" \n", callback);
                woyouService.printQRCode(activeUrl, 7, 2, callback);
            }
        }
        if (!order.isToPay()) {
            woyouService.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n", callback);
            woyouService.printText(UIUtils.getString(R.string.print_pledge_notice) + "\n", callback);
        }
        woyouService.setAlignment(1, callback);
        woyouService.printText("\n" + (order.isAutoPrint() ? "自动打印" : "手动打印")+ "\n", callback);
        woyouService.printText(" \n\n\n", callback);
    }
}
