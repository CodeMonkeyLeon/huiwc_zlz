package com.hstypay.enterprise.utils.print;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;

import com.chinapnr.aidl.printer.AidlPrinter;
import com.chinapnr.aidl.printer.AidlPrinterListener;
import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print
 * @创建者: Jeremy
 * @创建时间: 2019/3/18 20:09
 * @描述: ${TODO}
 */
public class PrintHuifuUtil {

    private static PrintHuifuUtil singleton;

    public static PrintHuifuUtil with() {
        if (singleton == null) {
            synchronized (PrintHuifuUtil.class) {
                if (singleton == null) {
                    singleton = new PrintHuifuUtil();
                }
            }
        }
        return singleton;
    }

    public void print(final Activity activity, final AidlPrinter printerModule, TradeDetailBean order) {
        try {
            Bundle format = new Bundle();
            format.putString("font", "large");// 小号字体（small,normal,large）
            format.putString("align", "center");// 左边对齐（left,center,right）
            if (order.getTradeType() == 1) {
                printerModule.addText(format, UIUtils.getString(R.string.print_recharge_title));
            } else if (order.getTradeType() == 2) {
                printerModule.addText(format, UIUtils.getString(R.string.print_verification_title));
            } else {
                if (!order.isPay()) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_refund_title));
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pay_title));
                }
            }
            format.putString("font", "normal");// 小号字体（small,normal,large）
            format.putString("align", "left");// 左边对齐（left,center,right）
            printerModule.addText(format, order.getPartner() + "              " + "请妥善保存");
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + ": ");
                    printerModule.addText(format, MyApplication.getMerchantName());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printerModule.addText(format, UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            String storeNameTitle;
            if (order.isPay()) {
                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
            } else {
                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, storeNameTitle + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    printerModule.addText(format, storeNameTitle + ": ");
                    printerModule.addText(format, order.getStoreMerchantIdCnt());
                } else {
                    printerModule.addText(format, storeNameTitle + ": " + order.getStoreMerchantIdCnt());
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                if (order.getCashPointName().length() > 16) {
                    String str = order.getCashPointName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, "收银点: ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (order.getCashPointName().length() > 11) {
                    printerModule.addText(format, "收银点: ");
                    printerModule.addText(format, order.getCashPointName());
                } else {
                    printerModule.addText(format, "收银点: " + order.getCashPointName());
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                printerModule.addText(format, ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()));
            }
            if (!order.isPay()) {
                printerModule.addText(format, UIUtils.getString(R.string.print_refund_no_title) + ": ");
                printerModule.addText(format, order.getRefundNo());
                printerModule.addText(format, UIUtils.getString(R.string.print_refund_time_title) + ": ");
                printerModule.addText(format, order.getRefundTime());

                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        printerModule.addText(format, refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        printerModule.addText(format, user);
                        String printuser = sbf.substring(16, order.getRefundUser().length());
                        printerModule.addText(format, printuser);
                    } else if (order.getRefundUser().length() > 12) {
                        printerModule.addText(format, refundUser + ": ");
                        printerModule.addText(format, order.getRefundUser());
                    } else {
                        printerModule.addText(format, refundUser + ": " + order.getRefundUser());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        printerModule.addText(format, refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        printerModule.addText(format, user);
                        String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                        printerModule.addText(format, printuser);
                    } else if (order.getRefundUserRealName().length() > 12) {
                        printerModule.addText(format, refundUser + ": ");
                        printerModule.addText(format, order.getRefundUserRealName());
                    } else {
                        printerModule.addText(format, refundUser + ": " + order.getRefundUserRealName());
                    }
                }
                printerModule.addText(format, UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()));
                if (order.getRefundMoney() > 0) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元");
                }
                printerModule.addText(format, UIUtils.getString(R.string.print_refund_instruction));
            } else {
                if (!TextUtils.isEmpty(order.getTransactionId())) {
                    printerModule.addText(format, OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": ");
                    printerModule.addText(format, order.getTransactionId());
                }
                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    printerModule.addText(format, UIUtils.getString(R.string.tv_print_order_no) + ": ");
                    printerModule.addText(format, order.getOrderNo());
                }
                printerModule.addText(format, UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()));
                printerModule.addText(format, UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()));
                try {
                    printerModule.addText(format, UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        printerModule.addText(format, cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String oneUser = sbf.substring(0, 16);
                        printerModule.addText(format, oneUser);
                        String twoUser = sbf.substring(16, order.getCashierName().length());
                        printerModule.addText(format, twoUser);
                    }
                    if (order.getCashierName().length() > 12) {
                        printerModule.addText(format, cashierTitle + ": ");
                        printerModule.addText(format, order.getCashierName());
                    } else {
                        printerModule.addText(format, cashierTitle + ": " + order.getCashierName());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        printerModule.addText(format, cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneUser = sbf.substring(0, 16);
                        printerModule.addText(format, oneUser);
                        String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                        printerModule.addText(format, twoUser);
                    }
                    if (order.getOpUserRealName().length() > 12) {
                        printerModule.addText(format, cashierTitle + ": ");
                        printerModule.addText(format, order.getOpUserRealName());
                    } else {
                        printerModule.addText(format, cashierTitle + ": " + order.getOpUserRealName());
                    }
                }
                printerModule.addText(format, UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元");
                printerModule.addText(format, UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元");
                printerModule.addText(format, UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元");
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                printerModule.addText(format, UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach());
            }
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            printerModule.addText(format, UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                printerModule.addText(format, UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }
            printerModule.addText(format, "");
            printerModule.addText(format, "");
            format.putString("align", "center"); // align(left,center,right)
            if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                } else {
                    printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                }
                printerModule.addText(format, ConfigUtil.getPrintCodeTitle());
                Bitmap bitmap = MaxCardManager.getInstance().create2DCode(printCode,
                            DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                            DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                format.putInt("height", 240);
                format.putInt("width", 240);
                format.putInt("offset", 60);
                printerModule.addPicture(format, bitmap);
            }
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        printerModule.addText(format, title);
                    }
                    if (!TextUtils.isEmpty(activeUrl)) {
                        Bitmap scanBitmap = MaxCardManager.getInstance().create2DCode(activeUrl,
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                        format.putInt("height", 240);
                        format.putInt("width", 240);
                        format.putInt("offset", 70);
                        printerModule.addPicture(format, scanBitmap);
                    }
                }
            }
            printerModule.addText(format, "");
            printerModule.addText(format, order.isAutoPrint() ? "自动打印" : "手动打印");
            printerModule.startPrinter(new AidlPrinterListener.Stub() {
                @Override
                public void onFinish() throws RemoteException {
                    printerModule.paperSkip(2);
                }

                @Override
                public void onError(int errCode, String errMsg) throws RemoteException {
                    if (!TextUtils.isEmpty(errMsg)) {
                        MyToast.showToastShort(errMsg);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pledgePrint(final Activity activity, final AidlPrinter printerModule, PledgePayBean.DataBean order) {
        try {
            Bundle format = new Bundle();
            format.putString("font", "large");// 小号字体（small,normal,large）
            format.putString("align", "center");// 左边对齐（left,center,right）

            if (order.isToPay()) {
                printerModule.addText(format, UIUtils.getString(R.string.print_title_pledge_pay));
            } else {
                printerModule.addText(format, UIUtils.getString(R.string.print_title_pledge));
            }

            format.putString("font", "normal");// 小号字体（small,normal,large）
            format.putString("align", "left");// 左边对齐（left,center,right）
            printerModule.addText(format, order.getPartner() + "              " + "请妥善保存");
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + ": ");
                    printerModule.addText(format, MyApplication.getMerchantName());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printerModule.addText(format, UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.trade_store_name) + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.trade_store_name) + ": ");
                    printerModule.addText(format, order.getStoreMerchantIdCnt());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt());
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                printerModule.addText(format,UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()));
            }
            try {
                printerModule.addText(format, UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                printerModule.addText(format, OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：");
                printerModule.addText(format, order.getOutTransactionId());
            }
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                printerModule.addText(format, UIUtils.getString(R.string.tv_pledge_order_no) + ": ");
                printerModule.addText(format, order.getAuthNo());
            }

            printerModule.addText(format, UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()));

            if (!order.isToPay()) {
                if (!TextUtils.isEmpty(order.getMoney())) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元");
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元");
                }
            }
            if (order.isToPay()) {
                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元");
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元");
                }
                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元");
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元");
                }
            }

            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    printerModule.addText(format, cashierTitle + ": ");
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String oneUser = sbf.substring(0, 16);
                    printerModule.addText(format, oneUser);
                    String twoUser = sbf.substring(16, order.getCashierName().length());
                    printerModule.addText(format, twoUser);
                }
                if (order.getCashierName().length() > 12) {
                    printerModule.addText(format, cashierTitle + ": ");
                    printerModule.addText(format, order.getCashierName());
                } else {
                    printerModule.addText(format, cashierTitle + ": " + order.getCashierName());
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                String opUserTitle;
                if (order.isToPay()) {
                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                } else {
                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                }
                if (order.getOpUserRealName().length() > 16) {
                    printerModule.addText(format, opUserTitle);
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String oneUser = sbf.substring(0, 16);
                    printerModule.addText(format, oneUser);
                    String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                    printerModule.addText(format, twoUser);
                }
                if (order.getOpUserRealName().length() > 12) {
                    printerModule.addText(format, opUserTitle);
                    printerModule.addText(format, order.getOpUserRealName());
                } else {
                    printerModule.addText(format, opUserTitle + order.getOpUserRealName());
                }
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                printerModule.addText(format, UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach());
            }
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            printerModule.addText(format, UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                printerModule.addText(format, UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }
            printerModule.addText(format, "");
            format.putString("align", "center"); // align(left,center,right)
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                if (!order.isToPay()) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_pledge_scan_title));
                }
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
                } else {
                    printCode = order.getAuthNo();
                }
                Bitmap bitmap = MaxCardManager.getInstance().create2DCode(printCode,
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                format.putInt("height", 240);
                format.putInt("width", 240);
                format.putInt("offset", 60);
                printerModule.addPicture(format, bitmap);
            }
            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        printerModule.addText(format, title);
                    }
                    if (!TextUtils.isEmpty(activeUrl)) {
                        Bitmap scanBitmap = MaxCardManager.getInstance().create2DCode(activeUrl,
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                        format.putInt("height", 240);
                        format.putInt("width", 240);
                        format.putInt("offset", 70);
                        printerModule.addPicture(format, scanBitmap);
                    }
                }
            }
            if (!order.isToPay()) {
                printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
                printerModule.addText(format, UIUtils.getString(R.string.print_pledge_notice));
            }
            format.putString("align", "center");
            printerModule.addText(format, "");
            printerModule.addText(format, order.isAutoPrint() ? "自动打印" : "手动打印");
            printerModule.startPrinter(new AidlPrinterListener.Stub() {
                @Override
                public void onFinish() throws RemoteException {
                    printerModule.paperSkip(2);
                }

                @Override
                public void onError(int errCode, String errMsg) throws RemoteException {
                    if (!TextUtils.isEmpty(errMsg)) {
                        MyToast.showToastShort(errMsg);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printTotal(final Activity activity, final AidlPrinter printerModule, ReportBean.DataEntity info) {
        try {
            Bundle format = new Bundle();
            format.putString("font", "large");// 小号字体（small,normal,large）
            format.putString("align", "center");// 左边对齐（left,center,right）
            if (info.getType() == 1) {
                printerModule.addText(format, UIUtils.getString(R.string.print_pay_report_title));
            } else if (info.getType() == 2) {
                printerModule.addText(format, UIUtils.getString(R.string.print_consume_report_title));
            }else if (info.getType() == 3){
                printerModule.addText(format, UIUtils.getString(R.string.print_fk_consume_report_title));
            }

            format.putString("font", "normal");// 小号字体（small,normal,large）
            format.putString("align", "left");// 左边对齐（left,center,right）
            printerModule.addText(format, "");
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：");
                    printerModule.addText(format, MyApplication.getMerchantName());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printerModule.addText(format, UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            // 门店
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": " + "全部门店");
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (info.getStoreName().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": ");
                    printerModule.addText(format, info.getStoreName());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName());
                }
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                printerModule.addText(format, cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, cashierTitle + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (info.getStoreName().length() > 12) {
                    printerModule.addText(format, cashierTitle + ": ");
                    printerModule.addText(format, info.getCashierName());
                } else {
                    String str = info.getCashierName();
                    printerModule.addText(format, cashierTitle + ": " + str);
                }
            }
            if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                if (info.getCashierPointName().length() > 16) {
                    StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                    String one = sbf.substring(0, 16);
                    String two = sbf.substring(16, info.getCashierPointName().length());
                    printerModule.addText(format, "收银点: ");
                    printerModule.addText(format, one);
                    printerModule.addText(format, two);
                } else if (info.getCashierPointName().length() > 12) {
                    printerModule.addText(format, "收银点: ");
                    printerModule.addText(format, info.getCashierPointName());
                } else {
                    printerModule.addText(format, "收银点: " + info.getCashierPointName());
                }
            }

            //开始时间
            printerModule.addText(format, "开始时间: " + info.getStartTime());
            // 结束时间
            printerModule.addText(format, UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime());
            printerModule.addText(format, "");
            // 结算总计
            printerModule.addText(format, UIUtils.getString(R.string.print_report_total_title));
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            if (info.getType() == 1) {
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));
            } else if (info.getType() == 2) {
                /*printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));*/

                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔"));

                /*printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔"));*/
            }else if (info.getType() == 3){
                //饭卡消费
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+""));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元"));

                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                printerModule.addText(format, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));

            }
            if (info.getType() == 1) {
                List<ReportBean.DataEntity.ListEntity> list = info.getList();
                if (list != null && list.size() > 0) {
                    printerModule.addText(format, "");
                    printerModule.addText(format, UIUtils.getString(R.string.print_trade_type_list_title) + ": ");
                    printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));

                    for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                        printerModule.addText(format, PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元"));
                        printerModule.addText(format, PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔"));
                    }
                }
            }
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            printerModule.addText(format, UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            printerModule.addText(format, UIUtils.getString(R.string.print_client_sign_title) + "：");

            printerModule.startPrinter(new AidlPrinterListener.Stub() {
                @Override
                public void onFinish() throws RemoteException {
                    printerModule.paperSkip(2);
                }

                @Override
                public void onError(int errCode, String errMsg) throws RemoteException {
                    if (!TextUtils.isEmpty(errMsg)) {
                        MyToast.showToastShort(errMsg);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printPledgeTotal(final Activity activity, final AidlPrinter printerModule, PledgeReportBean.DataBean info) {
        try {
            Bundle format = new Bundle();
            format.putString("font", "large");// 小号字体（small,normal,large）
            format.putString("align", "center");// 左边对齐（left,center,right）
            printerModule.addText(format, UIUtils.getString(R.string.print_title_pledge_report));

            format.putString("font", "normal");// 小号字体（small,normal,large）
            format.putString("align", "left");// 左边对齐（left,center,right）
            printerModule.addText(format, "");
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：");
                    printerModule.addText(format, MyApplication.getMerchantName());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printerModule.addText(format, UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            // 门店
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": " + "全部门店");
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (info.getStoreName().length() > 11) {
                    printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": ");
                    printerModule.addText(format, info.getStoreName());
                } else {
                    printerModule.addText(format, UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName());
                }
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                printerModule.addText(format, cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printerModule.addText(format, cashierTitle + ": ");
                    printerModule.addText(format, one);
                    String two = sbf.substring(16, str.length());
                    printerModule.addText(format, two);
                } else if (info.getStoreName().length() > 12) {
                    printerModule.addText(format, cashierTitle + ": ");
                    printerModule.addText(format, info.getCashierName());
                } else {
                    String str = info.getCashierName();
                    printerModule.addText(format, cashierTitle + ": " + str);
                }
            }
            //开始时间
            printerModule.addText(format, UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime());
            // 结束时间
            printerModule.addText(format, UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime());
            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));

            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元");
            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔");
            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元");
            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔");
            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元");
            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_refund_count) + "：" + info.getCntFreeMoney() + "笔");
//            printerModule.addText(format, UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元");

            printerModule.addText(format, UIUtils.getString(R.string.print_single_horizontal));
            printerModule.addText(format, UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            printerModule.addText(format, UIUtils.getString(R.string.print_client_sign_title) + "：");

            printerModule.addText(format, "");
            printerModule.startPrinter(new AidlPrinterListener.Stub() {
                @Override
                public void onFinish() throws RemoteException {
                    printerModule.paperSkip(2);
                }

                @Override
                public void onError(int errCode, String errMsg) throws RemoteException {
                    if (!TextUtils.isEmpty(errMsg)) {
                        MyToast.showToastShort(errMsg);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
