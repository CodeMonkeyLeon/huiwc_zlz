package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.lkl.cloudpos.aidl.printer.AidlPrinter;
import com.lkl.cloudpos.aidl.printer.AidlPrinterListener;
import com.lkl.cloudpos.aidl.printer.PrintItemObj;
import com.lkl.cloudpos.aidl.printer.PrintItemObj.ALIGN;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 拉卡拉打印
 *
 * @author he_hui
 * @version [版本号, 2016-1-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PrintLKL {
    public static class PrintStateChangeListener extends AidlPrinterListener.Stub {

        @Override
        public void onError(int arg0)
                throws RemoteException {
            //            showMessage("数据打印失败,错误码" + arg0);
            Log.i("hehui", "");
        }

        @Override
        public void onPrintFinish()
                throws RemoteException {
            //            showMessage("数据打印成功");
            Log.i("hehui", "");

        }

    }

    public static void printText(AidlPrinter printerDev, final TradeDetailBean order, Context c) {
        try {
            printerDev.printText(
                    new ArrayList<PrintItemObj>() {
                        {
                            if (order.getTradeType() == 1) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_recharge_title), 16, true, ALIGN.CENTER));
                            } else if (order.getTradeType() == 2) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_verification_title), 16, true, ALIGN.CENTER));
                            } else {
                                if (!order.isPay()) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_refund_title), 16, true, ALIGN.CENTER));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pay_title), 16, true, ALIGN.CENTER));
                                }
                            }

                            add(new PrintItemObj(order.getPartner() + "              请妥善保管", 20, false, ALIGN.LEFT));
                            add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.CENTER));

                            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                                if (MyApplication.getMerchantName().length() > 16) {
                                    String str = MyApplication.getMerchantName();
                                    StringBuffer sbf = new StringBuffer(str);
                                    String one = sbf.substring(0, 16);
                                    add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(one, 16, false, ALIGN.LEFT));
                                    String two = sbf.substring(15, str.length());
                                    add(new PrintItemObj(two, 16, false, ALIGN.LEFT));
                                } else if (MyApplication.getMerchantName().length() > 11) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(MyApplication.getMerchantName(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 12, false, ALIGN.LEFT));
                                }
                            }


                            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 12, false, ALIGN.LEFT));
                            }
                            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                                add(new PrintItemObj(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()), 12, false, ALIGN.LEFT));
                            }
                            String storeNameTitle;
                            if (order.isPay()) {
                                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
                            } else {
                                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
                            }
                            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                                if (order.getStoreMerchantIdCnt().length() > 16) {
                                    String str = order.getStoreMerchantIdCnt();
                                    StringBuffer sbf = new StringBuffer(str);
                                    String one = sbf.substring(0, 16);
                                    add(new PrintItemObj(storeNameTitle + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(one, 12, false, ALIGN.LEFT));
                                    String two = sbf.substring(16, str.length());
                                    add(new PrintItemObj(two, 12, false, ALIGN.LEFT));
                                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                                    add(new PrintItemObj(storeNameTitle + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getStoreMerchantIdCnt(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(storeNameTitle + "：" + order.getStoreMerchantIdCnt(), 12, false, ALIGN.LEFT));
                                }
                            }
                            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                                if (order.getCashPointName().length() > 16) {
                                    String str = order.getCashPointName();
                                    StringBuffer sbf = new StringBuffer(str);
                                    String one = sbf.substring(0, 16);
                                    add(new PrintItemObj("收银点: ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(one, 12, false, ALIGN.LEFT));
                                    String two = sbf.substring(16, str.length());
                                    add(new PrintItemObj(two, 12, false, ALIGN.LEFT));
                                } else if (order.getCashPointName().length() > 11) {
                                    add(new PrintItemObj("收银点: ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getCashPointName(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj("收银点: " + order.getCashPointName(), 12, false, ALIGN.LEFT));
                                }
                            }
                            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                                add(new PrintItemObj(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()), 12, false, ALIGN.LEFT));
                            }
                            if (!order.isPay()) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_refund_no_title), 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(order.getRefundNo(), 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(UIUtils.getString(R.string.print_refund_time_title), 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(order.getRefundTime(), 12, false, ALIGN.LEFT));

                                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                                    if (order.getRefundUser().length() > 16) {
                                        add(new PrintItemObj(refundUser + ": ", 12, false, ALIGN.LEFT));
                                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                                        String user = sbf.substring(0, 16);
                                        add(new PrintItemObj(user, 12, false, ALIGN.LEFT));
                                        String printuser = sbf.substring(16, order.getRefundUser().length());
                                        add(new PrintItemObj(printuser, 12, false, ALIGN.LEFT));
                                    } else if (order.getRefundUser().length() > 12) {
                                        add(new PrintItemObj(refundUser + ": ", 12, false, ALIGN.LEFT));
                                        add(new PrintItemObj(order.getRefundUser(), 12, false, ALIGN.LEFT));
                                    } else {
                                        add(new PrintItemObj(refundUser + ": " + order.getRefundUser(), 12, false, ALIGN.LEFT));
                                    }
                                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                                    if (order.getRefundUserRealName().length() > 16) {
                                        add(new PrintItemObj(refundUser + ": ", 12, false, ALIGN.LEFT));
                                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                                        String user = sbf.substring(0, 16);
                                        add(new PrintItemObj(user, 12, false, ALIGN.LEFT));
                                        String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                                        add(new PrintItemObj(printuser, 12, false, ALIGN.LEFT));
                                    } else if (order.getRefundUserRealName().length() > 12) {
                                        add(new PrintItemObj(refundUser + ": ", 12, false, ALIGN.LEFT));
                                        add(new PrintItemObj(order.getRefundUserRealName(), 12, false, ALIGN.LEFT));
                                    } else {
                                        add(new PrintItemObj(refundUser + ": " + order.getRefundUserRealName(), 12, false, ALIGN.LEFT));
                                    }
                                }
                                add(new PrintItemObj(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()), 12, false, ALIGN.LEFT));
                                if (order.getRefundMoney() > 0) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元", 12, false, ALIGN.LEFT));
                                }
                                add(new PrintItemObj(UIUtils.getString(R.string.print_refund_instruction), 12, false, ALIGN.LEFT));
                            } else {
                                if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                                    add(new PrintItemObj(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + "：", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getTransactionId(), 12, false, ALIGN.LEFT));
                                }
                                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.tv_print_order_no) + "：", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getOrderNo(), 12, false, ALIGN.LEFT));
                                }
                                add(new PrintItemObj(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()), 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()), 12, false, ALIGN.LEFT));
                                try {
                                    if (!StringUtils.isEmptyOrNull(order.getTradeTime())) {
                                        add(new PrintItemObj(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime(), 12, false, ALIGN.LEFT));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //收银员
                                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                                    if (order.getCashierName().length() > 16) {
                                        add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT));
                                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                                        String user = sbf.substring(0, 16);
                                        add(new PrintItemObj(user, 12, false, ALIGN.LEFT));
                                        String printUser = sbf.substring(16, order.getCashierName().length());
                                        add(new PrintItemObj(printUser, 12, false, ALIGN.LEFT));
                                    } else if (order.getCashierName().length() > 12) {
                                        add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT));
                                        add(new PrintItemObj(order.getCashierName(), 12, false, ALIGN.LEFT));
                                    } else {
                                        add(new PrintItemObj(cashierTitle + ": " + order.getCashierName(), 12, false, ALIGN.LEFT));
                                    }
                                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                                    if (order.getOpUserRealName().length() > 16) {
                                        add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT));
                                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                                        String user = sbf.substring(0, 16);
                                        add(new PrintItemObj(user, 12, false, ALIGN.LEFT));
                                        String printUser = sbf.substring(16, order.getOpUserRealName().length());
                                        add(new PrintItemObj(printUser, 12, false, ALIGN.LEFT));
                                    } else if (order.getOpUserRealName().length() > 12) {
                                        add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT));
                                        add(new PrintItemObj(order.getOpUserRealName(), 12, false, ALIGN.LEFT));
                                    } else {
                                        add(new PrintItemObj(cashierTitle + ": " + order.getOpUserRealName(), 12, false, ALIGN.LEFT));
                                    }
                                }
                                add(new PrintItemObj(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元", 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元", 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元", 12, false, ALIGN.LEFT));
                            }

                            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach(), 12, false, ALIGN.LEFT));
                            }

                            add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.LEFT));
                            add(new PrintItemObj(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 12, false, ALIGN.LEFT));

                            //顾客签名
                            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_client_sign_title) + "：", 12, false, ALIGN.LEFT));
                            }
                            add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                            add(new PrintItemObj(ConfigUtil.getPrintCodeTitle(), 12, false, ALIGN.CENTER));
                        }
                    },
                    new AidlPrinterListener.Stub() {

                        @Override
                        public void onPrintFinish()
                                throws RemoteException {
                            //                    showMessage("打印完成");
                        }

                        @Override
                        public void onError(int arg0)
                                throws RemoteException {
                            //                    showMessage("打印出错，错误码为：" + arg0);
                        }
                    });
            if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                } else {
                    printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                }
                Bitmap code = CreateTwoDiCodeUtil.createCode(printCode,
                        DisplayUtil.dip2Px(MyApplication.getContext(), 180),
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                printerDev.printBmp(0, code.getWidth(), code.getHeight(), code, new AidlPrinterListener.Stub() {

                    @Override
                    public void onPrintFinish()
                            throws RemoteException {
                        //                    showMessage("打印位图成功");
                    }

                    @Override
                    public void onError(int arg0)
                            throws RemoteException {
                        //                    showMessage("打印位图失败，错误码" + arg0);
                    }
                });
            }


            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printerDev.printText(new ArrayList<PrintItemObj>() {
                        {
                            String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                            add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.CENTER));
                            if (title.length() > 15) {
                                StringBuffer sbf = new StringBuffer(title);
                                String one = sbf.substring(0, 15);
                                add(new PrintItemObj(one, 12, false, ALIGN.LEFT, false, false));
                                String two = sbf.substring(15, title.length());
                                add(new PrintItemObj(two, 12, false, ALIGN.LEFT, false, false));
                            } else {
                                add(new PrintItemObj(title, 12, false, ALIGN.LEFT, false, false));
                            }
                            //add(new PrintItemObj("   \n", 22, false, ALIGN.LEFT, false, false));
                        }
                    }, new AidlPrinterListener.Stub() {
                        @Override
                        public void onPrintFinish()
                                throws RemoteException {
                            //                    showMessage("打印完成");
                        }

                        @Override
                        public void onError(int arg0)
                                throws RemoteException {
                            //                    showMessage("打印出错，错误码为：" + arg0);
                        }
                    });
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    Bitmap activeCode = CreateTwoDiCodeUtil.createCode(activeUrl,
                            DisplayUtil.dip2Px(MyApplication.getContext(), 180),
                            DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                    printerDev.printBmp(0, activeCode.getWidth(), activeCode.getHeight(), activeCode, new AidlPrinterListener.Stub() {

                        @Override
                        public void onPrintFinish()
                                throws RemoteException {
                            //                    showMessage("打印位图成功");
                        }

                        @Override
                        public void onError(int arg0)
                                throws RemoteException {
                            //                    showMessage("打印位图失败，错误码" + arg0);
                        }
                    });
                }
            }
            printerDev.printText(new ArrayList<PrintItemObj>() {
                {
                    add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                    add(new PrintItemObj(order.isAutoPrint() ? "自动打印" : "手动打印", 12, false, ALIGN.CENTER));
                    add(new PrintItemObj("   \n", 22, false, ALIGN.LEFT, false, false));
                }
            }, new AidlPrinterListener.Stub() {
                @Override
                public void onPrintFinish()
                        throws RemoteException {
                }

                @Override
                public void onError(int arg0)
                        throws RemoteException {
                }
            });
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void printPledgeText(AidlPrinter printerDev, final PledgePayBean.DataBean order, Context c) {
        try {
            printerDev.printText(
                    new ArrayList<PrintItemObj>() {
                        {
                            if (order.isToPay()) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_title_pledge_pay), 16, true, ALIGN.CENTER));
                            } else {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_title_pledge), 16, true, ALIGN.CENTER));
                            }

                            add(new PrintItemObj(order.getPartner() + "              请妥善保管", 20, false, ALIGN.LEFT));
                            add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.CENTER));

                            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                                if (MyApplication.getMerchantName().length() > 16) {
                                    String str = MyApplication.getMerchantName();
                                    StringBuffer sbf = new StringBuffer(str);
                                    String one = sbf.substring(0, 16);
                                    add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(one, 16, false, ALIGN.LEFT));
                                    String two = sbf.substring(15, str.length());
                                    add(new PrintItemObj(two, 16, false, ALIGN.LEFT));
                                } else if (MyApplication.getMerchantName().length() > 11) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(MyApplication.getMerchantName(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 12, false, ALIGN.LEFT));
                                }
                            }


                            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 12, false, ALIGN.LEFT));
                            }

                            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                                if (order.getStoreMerchantIdCnt().length() > 16) {
                                    String str = order.getStoreMerchantIdCnt();
                                    StringBuffer sbf = new StringBuffer(str);
                                    String one = sbf.substring(0, 16);
                                    add(new PrintItemObj(UIUtils.getString(R.string.trade_store_name) + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(one, 12, false, ALIGN.LEFT));
                                    String two = sbf.substring(16, str.length());
                                    add(new PrintItemObj(two, 12, false, ALIGN.LEFT));
                                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.trade_store_name) + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getStoreMerchantIdCnt(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.trade_store_name) + "：" + order.getStoreMerchantIdCnt(), 12, false, ALIGN.LEFT));
                                }
                            }
                            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()), 12, false, ALIGN.LEFT));
                            }
                            try {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime(), 12, false, ALIGN.LEFT));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                                add(new PrintItemObj(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：", 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(order.getOutTransactionId(), 12, false, ALIGN.LEFT));
                            }
                            if (!TextUtils.isEmpty(order.getAuthNo())) {
                                add(new PrintItemObj(UIUtils.getString(R.string.tv_pledge_order_no) + ": ", 12, false, ALIGN.LEFT));
                                add(new PrintItemObj(order.getAuthNo(), 12, false, ALIGN.LEFT));
                            }

                            add(new PrintItemObj(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()), 12, false, ALIGN.LEFT));

                            if (!order.isToPay()) {
                                if (!TextUtils.isEmpty(order.getMoney())) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元", 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元", 12, false, ALIGN.LEFT));
                                }
                            }
                            if (order.isToPay()) {
                                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元", 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元", 12, false, ALIGN.LEFT));
                                }
                                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元", 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元", 12, false, ALIGN.LEFT));
                                }
                            }

                            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                                if (order.getCashierName().length() > 12) {
                                    add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT));
                                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                                    String oneUser = sbf.substring(0, 12);
                                    add(new PrintItemObj(oneUser, 12, false, ALIGN.LEFT));
                                    String twoUser = sbf.substring(12, order.getCashierName().length());
                                    add(new PrintItemObj(twoUser, 12, false, ALIGN.LEFT));
                                }
                                if (order.getCashierName().length() > 12) {
                                    add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getCashierName(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(cashierTitle + ": " + order.getCashierName(), 12, false, ALIGN.LEFT));
                                }
                            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                                String opUserTitle;
                                if (order.isToPay()) {
                                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                                } else {
                                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                                }
                                if (order.getOpUserRealName().length() > 16) {
                                    add(new PrintItemObj(opUserTitle, 12, false, ALIGN.LEFT));
                                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                                    String oneUser = sbf.substring(0, 16);
                                    add(new PrintItemObj(oneUser, 12, false, ALIGN.LEFT));
                                    String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                                    add(new PrintItemObj(twoUser, 12, false, ALIGN.LEFT));
                                }
                                if (order.getOpUserRealName().length() > 12) {
                                    add(new PrintItemObj(opUserTitle, 12, false, ALIGN.LEFT));
                                    add(new PrintItemObj(order.getOpUserRealName(), 12, false, ALIGN.LEFT));
                                } else {
                                    add(new PrintItemObj(opUserTitle + order.getOpUserRealName(), 12, false, ALIGN.LEFT));
                                }
                            }
                            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach(), 12, false, ALIGN.LEFT));
                            }
                            add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.LEFT));
                            add(new PrintItemObj(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 12, false, ALIGN.LEFT));

                            //顾客签名
                            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_client_sign_title) + ": ", 12, false, ALIGN.LEFT));
                            }
                            add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                            if (!TextUtils.isEmpty(order.getAuthNo()) && !order.isToPay()) {
                                add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_scan_title), 12, false, ALIGN.CENTER));
                            }
                        }
                    },
                    new AidlPrinterListener.Stub() {

                        @Override
                        public void onPrintFinish()
                                throws RemoteException {
                            //                    showMessage("打印完成");
                        }

                        @Override
                        public void onError(int arg0)
                                throws RemoteException {
                            //                    showMessage("打印出错，错误码为：" + arg0);
                        }
                    });

            if (!TextUtils.isEmpty(order.getAuthNo())) {
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
                } else {
                    printCode = order.getAuthNo();
                }
                Bitmap code = CreateTwoDiCodeUtil.createCode(printCode,
                        DisplayUtil.dip2Px(MyApplication.getContext(), 180),
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                printerDev.printBmp(0, code.getWidth(), code.getHeight(), code, new AidlPrinterListener.Stub() {

                    @Override
                    public void onPrintFinish()
                            throws RemoteException {
                        //                    showMessage("打印位图成功");
                    }

                    @Override
                    public void onError(int arg0)
                            throws RemoteException {
                        //                    showMessage("打印位图失败，错误码" + arg0);
                    }
                });

                if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
                    if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                            && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                        printerDev.printText(new ArrayList<PrintItemObj>() {
                            {
                                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                                add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.CENTER));
                                if (title.length() > 15) {
                                    StringBuffer sbf = new StringBuffer(title);
                                    String one = sbf.substring(0, 15);
                                    add(new PrintItemObj(one, 12, false, ALIGN.LEFT, false, false));
                                    String two = sbf.substring(15, title.length());
                                    add(new PrintItemObj(two, 12, false, ALIGN.LEFT, false, false));
                                } else {
                                    add(new PrintItemObj(title, 12, false, ALIGN.LEFT, false, false));
                                }
                                //add(new PrintItemObj("   \n", 22, false, ALIGN.LEFT, false, false));
                            }
                        }, new AidlPrinterListener.Stub() {
                            @Override
                            public void onPrintFinish()
                                    throws RemoteException {
                                //                    showMessage("打印完成");
                            }

                            @Override
                            public void onError(int arg0)
                                    throws RemoteException {
                                //                    showMessage("打印出错，错误码为：" + arg0);
                            }
                        });
                        String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                        Bitmap activeCode = CreateTwoDiCodeUtil.createCode(activeUrl,
                                DisplayUtil.dip2Px(MyApplication.getContext(), 180),
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                        printerDev.printBmp(0, code.getWidth(), code.getHeight(), activeCode, new AidlPrinterListener.Stub() {

                            @Override
                            public void onPrintFinish()
                                    throws RemoteException {
                                //                    showMessage("打印位图成功");
                            }

                            @Override
                            public void onError(int arg0)
                                    throws RemoteException {
                                //                    showMessage("打印位图失败，错误码" + arg0);
                            }
                        });
                    }
                }
            }
            printerDev.printText(new ArrayList<PrintItemObj>() {
                {
                    if (!order.isToPay()) {
                        add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.LEFT, false, false));
                        add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_notice), 12, false, ALIGN.LEFT, false, false));
                    }
                    add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                    add(new PrintItemObj(order.isAutoPrint() ? "自动打印" : "手动打印", 12, false, ALIGN.CENTER));
                    add(new PrintItemObj("   \n", 22, false, ALIGN.LEFT, false, false));
                }
            }, new AidlPrinterListener.Stub() {
                @Override
                public void onPrintFinish()
                        throws RemoteException {
                }

                @Override
                public void onError(int arg0)
                        throws RemoteException {
                }
            });
        } catch (
                RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void printSummary(AidlPrinter printerDev, final ReportBean.DataEntity info, Context c) {
        try {
            printerDev.printText(new ArrayList<PrintItemObj>() {
                                     {
                                         try {

                                             if (info.getType() == 1) {
                                                 add(new PrintItemObj(UIUtils.getString(R.string.print_pay_report_title), 16, true, ALIGN.CENTER));
                                             } else if (info.getType() == 2) {
                                                 add(new PrintItemObj(UIUtils.getString(R.string.print_consume_report_title), 16, true, ALIGN.CENTER));
                                             }else if (info.getType() == 3){
                                                 add(new PrintItemObj(UIUtils.getString(R.string.print_fk_consume_report_title), 16, true, ALIGN.CENTER));
                                             }
                                             add(new PrintItemObj(" ", 22, false, ALIGN.CENTER));

                                             if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                                                 if (MyApplication.getMerchantName().length() > 16) {
                                                     String str = MyApplication.getMerchantName();
                                                     StringBuffer sbf = new StringBuffer(str);
                                                     String one = sbf.substring(0, 16);
                                                     add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT, false, false));
                                                     String two = sbf.substring(16, str.length());
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT, false, false));
                                                 } else if (MyApplication.getMerchantName().length() > 11) {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(MyApplication.getMerchantName(), 12, false, ALIGN.LEFT, false, false));
                                                 } else {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 12, false, ALIGN.LEFT, false, false));
                                                 }
                                             }

                                             if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                                                 add(new PrintItemObj(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 12, false, ALIGN.LEFT));
                                             }

                                             // 门店
                                             if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                                                 add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": 全部门店", 12, false, ALIGN.LEFT));
                                             } else {
                                                 if (info.getStoreName().length() > 16) {
                                                     String str = info.getStoreName();
                                                     StringBuffer sbf = new StringBuffer(str);
                                                     String one = sbf.substring(0, 16);
                                                     add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": ", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT));
                                                     String two = sbf.substring(16, str.length());
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT));
                                                 } else if (info.getStoreName().length() > 11) {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": ", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(info.getStoreName(), 12, false, ALIGN.LEFT));
                                                 } else {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName(), 12, false, ALIGN.LEFT));
                                                 }
                                             }
                                             //收银员
                                             String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                                             if (!StringUtils.isEmptyOrNull(info.getCashierName())) {
                                                 if (info.getCashierName().length() > 16) {
                                                     String str = info.getCashierName();
                                                     StringBuffer sbf = new StringBuffer(str);
                                                     String one = sbf.substring(0, 16);
                                                     add(new PrintItemObj(cashierTitle + "：", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT, false, false));
                                                     String two = sbf.substring(16, str.length());
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT, false, false));
                                                 } else if (info.getCashierName().length() > 12) {
                                                     add(new PrintItemObj(cashierTitle + "：", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(info.getCashierName(), 12, false, ALIGN.LEFT, false, false));
                                                 } else {
                                                     add(new PrintItemObj(cashierTitle + "：" + info.getCashierName(), 12, false, ALIGN.LEFT, false, false));
                                                 }
                                             } else {
                                                 add(new PrintItemObj(cashierTitle + "：" + UIUtils.getString(R.string.tv_all_user), 12, false, ALIGN.LEFT));
                                             }
                                             if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                                                 if (info.getCashierPointName().length() > 16) {
                                                     StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                                                     String one = sbf.substring(0, 16);
                                                     String two = sbf.substring(16, info.getCashierPointName().length());
                                                     add(new PrintItemObj("收银点: ", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT));
                                                 } else if (info.getCashierPointName().length() > 12) {
                                                     add(new PrintItemObj("收银点: ", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(info.getCashierPointName(), 12, false, ALIGN.LEFT));
                                                 } else {
                                                     add(new PrintItemObj("收银点: " + info.getCashierPointName(), 12, false, ALIGN.LEFT));
                                                 }
                                             }

                                             //开始时间
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime(), 12, false, ALIGN.LEFT));

                                             //结束时间
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime(), 12,
                                                     false, ALIGN.LEFT));
                                             add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_report_total_title), 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.CENTER));
                                             //add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                             if (info.getType() == 1) {
                                                 add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), 12, false, ALIGN.LEFT));
                                             } else if (info.getType() == 2) {
                                                 /*add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"),12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), 12, false, ALIGN.LEFT));*/

                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔"), 12, false, ALIGN.LEFT));

                                                 /*add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔"), 12, false, ALIGN.LEFT));*/

                                             }else if (info.getType() == 3){
                                                 //饭卡消费
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+""), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 12, false, ALIGN.LEFT));

                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), 12, false, ALIGN.LEFT));
                                                 add(new PrintItemObj(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), 12, false, ALIGN.LEFT));

                                             }
                                             if (info.getType() == 1) {
                                                 List<ReportBean.DataEntity.ListEntity> list = info.getList();
                                                 if (list != null && list.size() > 0) {
                                                     add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(UIUtils.getString(R.string.print_trade_type_list_title) + ": ", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.LEFT));

                                                     for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                                                         add(new PrintItemObj(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元"), 12, false, ALIGN.LEFT));
                                                         add(new PrintItemObj(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔"), 12, false, ALIGN.LEFT));
                                                     }
                                                 }
                                             }

                                             add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_client_sign_title) + "：", 12, false, ALIGN.LEFT));

                                             add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj("", 12, false, ALIGN.LEFT));
                                         } catch (Exception e) {

                                         }

                                     }
                                 },
                    new AidlPrinterListener.Stub() {
                        @Override
                        public void onPrintFinish()
                                throws RemoteException {
                            //                    showMessage("打印完成");
                        }

                        @Override
                        public void onError(int arg0)
                                throws RemoteException {
                            //                    showMessage("打印出错，错误码为：" + arg0);
                        }
                    });

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static void printPledgeSummary(AidlPrinter printerDev, final PledgeReportBean.DataBean info, Context c) {
        try {
            printerDev.printText(new ArrayList<PrintItemObj>() {
                                     {
                                         try {
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_title_pledge_report), 16, true, ALIGN.CENTER));
                                             add(new PrintItemObj(" ", 22, false, ALIGN.CENTER));

                                             if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                                                 if (MyApplication.getMerchantName().length() > 16) {
                                                     String str = MyApplication.getMerchantName();
                                                     StringBuffer sbf = new StringBuffer(str);
                                                     String one = sbf.substring(0, 16);
                                                     add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT, false, false));
                                                     String two = sbf.substring(16, str.length());
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT, false, false));
                                                 } else if (MyApplication.getMerchantName().length() > 11) {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(MyApplication.getMerchantName(), 12, false, ALIGN.LEFT, false, false));
                                                 } else {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 12, false, ALIGN.LEFT, false, false));
                                                 }
                                             }

                                             if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                                                 add(new PrintItemObj(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 12, false, ALIGN.LEFT));
                                             }

                                             // 门店
                                             if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                                                 add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": 全部门店", 12, false, ALIGN.LEFT));
                                             } else {
                                                 if (info.getStoreName().length() > 16) {
                                                     String str = info.getStoreName();
                                                     StringBuffer sbf = new StringBuffer(str);
                                                     String one = sbf.substring(0, 16);
                                                     add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": ", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT));
                                                     String two = sbf.substring(16, str.length());
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT));
                                                 } else if (info.getStoreName().length() > 11) {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ":", 12, false, ALIGN.LEFT));
                                                     add(new PrintItemObj(info.getStoreName(), 12, false, ALIGN.LEFT));
                                                 } else {
                                                     add(new PrintItemObj(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName(), 12, false, ALIGN.LEFT));
                                                 }
                                             }
                                             //收银员
                                             String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                                             if (!StringUtils.isEmptyOrNull(info.getCashierName())) {
                                                 if (info.getCashierName().length() > 16) {
                                                     String str = info.getCashierName();
                                                     StringBuffer sbf = new StringBuffer(str);
                                                     String one = sbf.substring(0, 16);
                                                     add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(one, 12, false, ALIGN.LEFT, false, false));
                                                     String two = sbf.substring(16, str.length());
                                                     add(new PrintItemObj(two, 12, false, ALIGN.LEFT, false, false));
                                                 } else if (info.getCashierName().length() > 12) {
                                                     add(new PrintItemObj(cashierTitle + ": ", 12, false, ALIGN.LEFT, false, false));
                                                     add(new PrintItemObj(info.getCashierName(), 12, false, ALIGN.LEFT, false, false));
                                                 } else {
                                                     add(new PrintItemObj(cashierTitle + ": " + info.getCashierName(), 12, false, ALIGN.LEFT, false, false));
                                                 }
                                             } else {
                                                 add(new PrintItemObj(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user), 12, false, ALIGN.LEFT));
                                             }
                                             //开始时间
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime(), 12, false, ALIGN.LEFT));

                                             //结束时间
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime(), 12,
                                                     false, ALIGN.LEFT));

                                             //结算总计
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.CENTER));

                                             add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_refund_money) + ": " + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_pledge_refund_count) + ": " + info.getCntFreeMoney() + "笔", 12, false, ALIGN.LEFT));
//                                             add(new PrintItemObj("未解冻金额: " + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元", 12, false, ALIGN.LEFT));

                                             add(new PrintItemObj(UIUtils.getString(R.string.print_single_horizontal), 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(UIUtils.getString(R.string.print_client_sign_title) + "：", 12, false, ALIGN.LEFT));

                                             add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                             add(new PrintItemObj(" ", 12, false, ALIGN.LEFT));
                                         } catch (Exception e) {

                                         }

                                     }
                                 },
                    new AidlPrinterListener.Stub() {
                        @Override
                        public void onPrintFinish()
                                throws RemoteException {
                            //                    showMessage("打印完成");
                        }

                        @Override
                        public void onError(int arg0)
                                throws RemoteException {
                            //                    showMessage("打印出错，错误码为：" + arg0);
                        }
                    });

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
