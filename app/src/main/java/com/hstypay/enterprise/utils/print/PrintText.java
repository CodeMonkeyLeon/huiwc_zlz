package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cn.weipass.pos.sdk.IPrint;
import cn.weipass.pos.sdk.IPrint.Gravity;
import cn.weipass.pos.sdk.Printer;
import cn.weipass.pos.sdk.Printer.FontFamily;
import cn.weipass.pos.sdk.Printer.FontSize;
import cn.weipass.pos.sdk.Printer.FontStyle;

public class PrintText {

    public static String getPrintErrorInfo(int what, String info) {
        String message = "";
        switch (what) {
            case IPrint.EVENT_CONNECT_FAILD:
                message = "连接打印机失败";
                break;
            case IPrint.EVENT_CONNECTED:
                // Log.e("subscribe_msg", "连接打印机成功");
                break;
            case IPrint.EVENT_PAPER_JAM:
                message = "打印机卡纸";
                break;
            case IPrint.EVENT_UNKNOW:
                message = "打印机未知错误";
                break;
            case IPrint.EVENT_STATE_OK:
                // 打印机状态正常
                break;
            case IPrint.EVENT_OK://
                // 回调函数中不能做UI操作，所以可以使用runOnUiThread函数来包装一下代码块
                // 打印完成结束
                break;
            case IPrint.EVENT_NO_PAPER:
                message = "打印机缺纸";
                break;
            case IPrint.EVENT_HIGH_TEMP:
                message = "打印机高温";
                break;
            case IPrint.EVENT_PRINT_FAILD:
                message = "打印失败";
                break;
        }

        return message;
    }


    //打印汇总小票
    public static void printTotal(Context context, Printer printer, ReportBean.DataEntity info) {

        if (info.getType() == 1) {
            printer.printText(UIUtils.getString(R.string.print_pay_report_title), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        } else if (info.getType() == 2) {
            printer.printText(UIUtils.getString(R.string.print_consume_report_title), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        } else if (info.getType() == 3) {
            printer.printText(UIUtils.getString(R.string.print_fk_consume_report_title), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        }
        printer.printText(" ", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (MyApplication.getMerchantName().length() > 11) {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(MyApplication.getMerchantName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName()
                        , FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            printer.printText(ToastHelper.toStr(R.string.store_name_title) + ": " + "全部门店",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(ToastHelper.toStr(R.string.store_name_title) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (info.getStoreName().length() > 11) {
                printer.printText(ToastHelper.toStr(R.string.store_name_title) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(info.getStoreName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(ToastHelper.toStr(R.string.store_name_title) + ": " + info.getStoreName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            printer.printText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(cashierTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (info.getCashierName().length() > 12) {
                printer.printText(cashierTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
                printer.printText(info.getCashierName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
            } else {
                printer.printText(cashierTitle + ": " + info.getCashierName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
            }
        }
        if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
            if (info.getCashierPointName().length() > 16) {
                printer.printText("收银点",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                String one = sbf.substring(0, 16);
                printer.printText(one,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, info.getCashierPointName().length());
                printer.printText(two,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (info.getCashierPointName().length() > 12) {
                printer.printText("收银点",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(info.getCashierPointName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText("收银点: " + info.getCashierPointName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }
        printer.printText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime(),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        // 结束时间
        printer.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime(),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        printer.printText(" ", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);

        printer.printText(UIUtils.getString(R.string.print_report_total_title), FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);

        printer.printText("--------------------------------",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        if (info.getType() == 1) {
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
        } else if (info.getType() == 2) {
            /*printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：",DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：",DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：",info.getSuccessCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：",DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：",DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：",DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：",info.getRefundCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);*/

            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);

            /*printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：",info.getNumberPaymentCount() + "次"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：",DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：",info.getRechargeNumber() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);*/
        } else if (info.getType() == 3) {
            //饭卡消费
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "" : info.getSettleRate() + ""), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                    FontStyle.NORMAL, Gravity.LEFT);
        }

        if (info.getType() == 1) {
            List<ReportBean.DataEntity.ListEntity> list = info.getList();
            if (list != null && list.size() > 0) {
                printer.printText("", FontFamily.SONG, FontSize.MEDIUM,
                        FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(UIUtils.getString(R.string.print_trade_type_list_title) + ": ", FontFamily.SONG, FontSize.MEDIUM,
                        FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(UIUtils.getString(R.string.print_single_horizontal), FontFamily.SONG, FontSize.MEDIUM,
                        FontStyle.NORMAL, Gravity.LEFT);
                for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                    printer.printText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元"), FontFamily.SONG, FontSize.MEDIUM,
                            FontStyle.NORMAL, Gravity.LEFT);
                    printer.printText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔"), FontFamily.SONG, FontSize.MEDIUM,
                            FontStyle.NORMAL, Gravity.LEFT);
                }
            }
        }
        printer.printText(UIUtils.getString(R.string.print_single_horizontal), FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：", FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);
        printer.printText("\n\n\n", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);
    }

    public static void printPledgeTotal(Context context, Printer printer, PledgeReportBean.DataBean info) {
        printer.printText(UIUtils.getString(R.string.print_title_pledge_report), FontFamily.SONG,
                FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);

        printer.printText(" ", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (MyApplication.getMerchantName().length() > 11) {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(MyApplication.getMerchantName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName()
                        , FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                    Gravity.LEFT);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + "全部门店",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                    Gravity.LEFT);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(UIUtils.getString(R.string.store_name_title) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (info.getStoreName().length() > 11) {
                printer.printText(UIUtils.getString(R.string.store_name_title) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
                printer.printText(info.getStoreName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
            } else {
                printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
            }
        }

        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            printer.printText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                    Gravity.LEFT);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(cashierTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (info.getCashierName().length() > 12) {
                printer.printText(cashierTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
                printer.printText(info.getCashierName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
            } else {
                printer.printText(cashierTitle + ": " + info.getCashierName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
            }
        }

        printer.printText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime(),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                Gravity.LEFT);


        // 结束时间
        printer.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime(),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        printer.printText(" ", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);
        printer.printText(UIUtils.getString(R.string.print_single_horizontal) + ": ",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        printer.printText(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_pledge_refund_count) + ":" + info.getCntFreeMoney() + "笔",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
//        printer.printText( UIUtils.getString(R.string.print_pledge_refund_money) + "：", + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元",
//                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        printer.printText(UIUtils.getString(R.string.print_single_horizontal), FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：", FontFamily.SONG, FontSize.MEDIUM,
                FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "\n\n\n", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);
    }


    public static void print(Context context, Printer printer, TradeDetailBean order) {
        if (order.getTradeType() == 1) {
            printer.printText(UIUtils.getString(R.string.print_recharge_title), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        } else if (order.getTradeType() == 2) {
            printer.printText(UIUtils.getString(R.string.print_verification_title), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        } else {
            if (!order.isPay()) {
                printer.printText(UIUtils.getString(R.string.print_refund_title), FontFamily.SONG,
                        FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
            } else {
                printer.printText(UIUtils.getString(R.string.print_pay_title), FontFamily.SONG,
                        FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
            }
        }

        printer.printText(" ", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);

        printer.printText(order.getPartner() + "              " + "请妥善保存",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        printer.printText(UIUtils.getString(R.string.print_single_horizontal),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (MyApplication.getMerchantName().length() > 11) {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(MyApplication.getMerchantName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName()
                        , FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }

        String storeNameTitle;
        if (order.isPay()) {
            storeNameTitle = ToastHelper.toStr(R.string.trade_store_name);
        } else {
            storeNameTitle = ToastHelper.toStr(R.string.refund_store_name);
        }
        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(storeNameTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                printer.printText(storeNameTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getStoreMerchantIdCnt(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(storeNameTitle + ": " + order.getStoreMerchantIdCnt(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }
        if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
            if (order.getCashPointName().length() > 16) {
                String str = order.getCashPointName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText("收银点: ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (order.getCashPointName().length() > 11) {
                printer.printText("收银点: ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getCashPointName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText("收银点: " + order.getCashPointName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
            printer.printText(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }

        if (!order.isPay()) {
            printer.printText(UIUtils.getString(R.string.print_refund_no_title) + ": ",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(order.getRefundNo(), FontFamily.SONG,
                    FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

            printer.printText(UIUtils.getString(R.string.print_refund_time_title) + ": ",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(order.getRefundTime(), FontFamily.SONG,
                    FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

            String refundUser = UIUtils.getString(R.string.print_refund_user_title);
            if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                if (order.getRefundUser().length() > 16) {
                    printer.printText(refundUser + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    StringBuffer sbf = new StringBuffer(order.getRefundUser());
                    String user = sbf.substring(0, 16);
                    printer.printText(user, FontFamily.SONG,
                            FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    String printuser = sbf.substring(16, order.getRefundUser().length());
                    printer.printText(printuser, FontFamily.SONG,
                            FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else if (order.getRefundUser().length() > 12) {
                    printer.printText(refundUser + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    printer.printText(order.getRefundUser(), FontFamily.SONG,
                            FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else {
                    printer.printText(refundUser + ": " + order.getRefundUser(),
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                if (order.getRefundUserRealName().length() > 16) {
                    printer.printText(refundUser + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                    String user = sbf.substring(0, 16);
                    printer.printText(user, FontFamily.SONG,
                            FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                    printer.printText(printuser, FontFamily.SONG,
                            FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else if (order.getRefundUserRealName().length() > 12) {
                    printer.printText(refundUser + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    printer.printText(order.getRefundUserRealName(), FontFamily.SONG,
                            FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else {
                    printer.printText(refundUser + ": " + order.getRefundUserRealName(),
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                }
            }
            printer.printText(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            if (order.getRefundMoney() > 0) {
                printer.printText(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
            printer.printText(UIUtils.getString(R.string.print_refund_instruction),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        } else {
            if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                printer.printText(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + "：",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getTransactionId(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
            if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                printer.printText(UIUtils.getString(R.string.tv_print_order_no) + "：",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getOrderNo(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
            printer.printText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            try {
                printer.printText(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeTime(), FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    printer.printText(cashierTitle + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String user = sbf.substring(0, 16);
                    printer.printText(user,
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    String printuser = sbf.substring(16, order.getCashierName().length());
                    printer.printText(printuser,
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else if (order.getCashierName().length() > 12) {
                    printer.printText(cashierTitle + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    printer.printText(order.getCashierName(),
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else {
                    printer.printText(cashierTitle + ": " + order.getCashierName(),
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                if (order.getOpUserRealName().length() > 16) {
                    printer.printText(cashierTitle + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String user = sbf.substring(0, 16);
                    printer.printText(user,
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    String printuser = sbf.substring(16, order.getOpUserRealName().length());
                    printer.printText(printuser,
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else if (order.getOpUserRealName().length() > 12) {
                    printer.printText(cashierTitle + ": ",
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                    printer.printText(order.getOpUserRealName(),
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                } else {
                    printer.printText(cashierTitle + ": " + order.getOpUserRealName(),
                            FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                }
            }
            printer.printText(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }

        //备注
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            printer.printText(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }
        printer.printText(UIUtils.getString(R.string.print_single_horizontal),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        //顾客签名 
        if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
            printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }

        printer.printText("", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.LEFT);
        if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
            String printCode;
            if (ConfigUtil.printCodeBillEnable()) {
                printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            } else {
                printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            }
            printer.printText(ConfigUtil.getPrintCodeTitle(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.CENTER);
            printer.printQrCode(printCode, 300, Gravity.CENTER);
        }

        if (order.getPartner().equals(
                ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                printer.printText(UIUtils.getString(R.string.print_single_horizontal),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                        Gravity.LEFT);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                if (!TextUtils.isEmpty(title)) {
                    printer.printText(title, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                }
                if (!TextUtils.isEmpty(activeUrl)) {
                    printer.printQrCode(activeUrl, 300, Gravity.CENTER);
                }
            }
        }

        printer.printText("\n" + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n", FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.CENTER);
        printer.printText(" \n\n\n", FontFamily.SONG, FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);

    }

    public static void pledgePrint(Context context, Printer printer, PledgePayBean.DataBean order) {
        if (order.isToPay()) {
            printer.printText(UIUtils.getString(R.string.print_title_pledge_pay), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        } else {
            printer.printText(UIUtils.getString(R.string.print_title_pledge), FontFamily.SONG,
                    FontSize.LARGE, FontStyle.NORMAL, Gravity.CENTER);
        }

        printer.printText(" ", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);
        printer.printText(order.getPartner() + "              " + "请妥善保存",
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        printer.printText(UIUtils.getString(R.string.print_single_horizontal),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (MyApplication.getMerchantName().length() > 11) {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(MyApplication.getMerchantName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName()
                        , FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL,
                    Gravity.LEFT);
        }

        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                printer.printText(ToastHelper.toStr(R.string.trade_store_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(one, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String two = sbf.substring(16, str.length());
                printer.printText(two, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                printer.printText(ToastHelper.toStr(R.string.trade_store_name) + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getStoreMerchantIdCnt(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(ToastHelper.toStr(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
            printer.printText(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }
        try {
            printer.printText(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
            printer.printText(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(order.getOutTransactionId(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }
        if (!TextUtils.isEmpty(order.getAuthNo())) {
            printer.printText(UIUtils.getString(R.string.tv_pledge_order_no) + ": ",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(order.getAuthNo(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }
        printer.printText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);

        if (!order.isToPay()) {
            if (!TextUtils.isEmpty(order.getMoney())) {
                printer.printText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }
        if (order.isToPay()) {
            if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                printer.printText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
            if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                printer.printText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
            if (order.getCashierName().length() > 12) {
                printer.printText(cashierTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                StringBuffer sbf = new StringBuffer(order.getCashierName());
                String oneUser = sbf.substring(0, 12);
                printer.printText(oneUser,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String twoUser = sbf.substring(12, order.getCashierName().length());
                printer.printText(twoUser,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
            if (order.getCashierName().length() > 12) {
                printer.printText(cashierTitle + ": ",
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getCashierName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(cashierTitle + ": " + order.getCashierName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
            String opUserTitle;
            if (order.isToPay()) {
                opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
            } else {
                opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
            }
            if (order.getOpUserRealName().length() > 12) {
                printer.printText(opUserTitle,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                String oneUser = sbf.substring(0, 12);
                printer.printText(oneUser,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String twoUser = sbf.substring(12, order.getOpUserRealName().length());
                printer.printText(twoUser,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
            if (order.getOpUserRealName().length() > 12) {
                printer.printText(opUserTitle,
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                printer.printText(order.getOpUserRealName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            } else {
                printer.printText(opUserTitle + order.getOpUserRealName(),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
            }
        }

        //备注
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            printer.printText(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach(),
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }
        printer.printText(UIUtils.getString(R.string.print_single_horizontal),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        printer.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()),
                FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        //顾客签名
        if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
            printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：",
                    FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
        }
        printer.printText("", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.LEFT);

        if (!TextUtils.isEmpty(order.getAuthNo())) {
            if (!order.isToPay()) {
                printer.printText(UIUtils.getString(R.string.print_pledge_scan_title),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.CENTER);
            }
            if (ConfigUtil.printCodeBillEnable()) {
                printer.printQrCode(Constants.URL_PRINT_QRCODE + order.getAuthNo(), 300, Gravity.CENTER);
            } else {
                printer.printQrCode(order.getAuthNo(), 300, Gravity.CENTER);
            }
        }

        if (order.getPartner().equals(
                ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                printer.printText(UIUtils.getString(R.string.print_single_horizontal),
                        FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                if (!TextUtils.isEmpty(title)) {
                    printer.printText(title, FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.LEFT);
                }
                if (!TextUtils.isEmpty(activeUrl)) {
                    printer.printQrCode(activeUrl, 300, Gravity.CENTER);
                }
            }
        }
        if (!order.isToPay()) {
            printer.printText(UIUtils.getString(R.string.print_single_horizontal), FontFamily.SONG, FontSize.LARGE,
                    FontStyle.NORMAL, Gravity.LEFT);
            printer.printText(ToastHelper.toStr(R.string.print_pledge_notice), FontFamily.SONG, FontSize.LARGE,
                    FontStyle.NORMAL, Gravity.LEFT);
        }

        printer.printText("\n" + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n", FontFamily.SONG, FontSize.MEDIUM, FontStyle.NORMAL, Gravity.CENTER);
        printer.printText(" \n\n\n", FontFamily.SONG, FontSize.LARGE,
                FontStyle.NORMAL, Gravity.CENTER);

    }
}
