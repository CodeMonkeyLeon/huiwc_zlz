package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Alignment;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;

/**
 * 联迪A8
 *
 * @author Jeremy
 * @version [版本号, 2019-09-20]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class PrinterApos1 {

    private Context context;
    private StaticLayout mLayout = null;
    private TextPaint mPaint;
    private Canvas mCanvas;
    private String content = null;
    private Printer.Progress progress = new Printer.Progress() {

        @Override
        public void doPrint(Printer arg0) throws Exception {
            onStartPrint();
        }

        @Override
        public void onFinish(int code) {
            if (code == Printer.ERROR_NONE) {
                displayPrinterInfo("PRINT SUCCESS END ");
            }
            /**
             * Has some error. Here is display it, but you may want to hanle the
             * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
             * again in the right time later.
             */
            else {
                String error;
                if (code == Printer.ERROR_PAPERENDED) {
                    error = "打印机缺纸";
                    MyToast.showToastShort(error);
                }
                displayPrinterInfo("PRINT ERR - " + (code));
            }
        }

        @Override
        public void onCrash() {
            onDeviceServiceCrash();
        }

    };

    /**
     * Search card and show all track info
     */
    public void startPrint() {
        try {
            progress.start();
            // DeviceService.logout();
        } catch (RequestException e) {
            e.printStackTrace();
            onDeviceServiceCrash();
        }
    }

    protected abstract void displayPrinterInfo(String info);

    protected abstract void onStartPrint();

    protected abstract void onDeviceServiceCrash();

    public PrinterApos1(final Context context, final TradeDetailBean order) {
        this.context = context;
        progress.addStep(new Printer.Step() {

            @Override
            public void doPrint(Printer printer)
                    throws Exception {
                printer.setAutoTrunc(true);

                // Default mode is real mode, now set it to virtual mode.
                printer.setMode(Printer.MODE_VIRTUAL);
                Format format = new Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);
                if (order.getTradeType() == 1) {
                    printer.printText("           " + UIUtils.getString(R.string.print_recharge_title) + "\n");
                } else if (order.getTradeType() == 2) {
                    printer.printText("           " + UIUtils.getString(R.string.print_verification_title) + "\n");
                } else {
                    if (!order.isPay()) {
                        printer.printText("           " + UIUtils.getString(R.string.print_refund_title) + "\n");
                    } else {
                        printer.printText("           " + UIUtils.getString(R.string.print_pay_title) + "\n");
                    }
                }

                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x1);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x1);
                printer.setFormat(format);
                printer.printText(order.getPartner() + "              请妥善保存\n");
                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(MyApplication.getMerchantName() + "\n");
                    } else {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }

                //商户号
                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
                }
                String storeNameTitle;
                if (order.isPay()) {
                    storeNameTitle = ToastHelper.toStr(R.string.trade_store_name);
                } else {
                    storeNameTitle = ToastHelper.toStr(R.string.refund_store_name);
                }
                if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                    if (order.getStoreMerchantIdCnt().length() > 16) {
                        String str = order.getStoreMerchantIdCnt();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(storeNameTitle + "：" + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (order.getStoreMerchantIdCnt().length() > 11) {
                        printer.printText(storeNameTitle + "：" + "\n");
                        printer.printText(order.getStoreMerchantIdCnt() + "\n");
                    } else {
                        printer.printText(storeNameTitle + "：" + order.getStoreMerchantIdCnt() + "\n");
                    }
                }
                if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                    if (order.getCashPointName().length() > 16) {
                        String str = order.getCashPointName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText("收银点: " + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (order.getCashPointName().length() > 11) {
                        printer.printText("收银点: " + "\n");
                        printer.printText(order.getCashPointName() + "\n");
                    } else {
                        printer.printText("收银点: " + order.getCashPointName() + "\n");
                    }
                }
                if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                    printer.printText(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()) + "\n");
                }
                if (!order.isPay()) {
                    printer.printText(UIUtils.getString(R.string.print_refund_no_title) + ": \n");
                    printer.printText(order.getRefundNo() + "\n");

                    printer.printText(UIUtils.getString(R.string.print_refund_time_title) + ": \n");
                    printer.printText(order.getRefundTime() + "\n");

                    String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                    if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                        if (order.getRefundUser().length() > 16) {
                            printer.printText(refundUser + ": \n");
                            StringBuffer sbf = new StringBuffer(order.getRefundUser());
                            String user = sbf.substring(0, 16);
                            printer.printText(user + "\r\n");
                            String printuser = sbf.substring(16, order.getRefundUser().length());
                            printer.printText(printuser + "\r\n");
                        } else if (order.getRefundUser().length() > 12) {
                            printer.printText(refundUser + ": \n");
                            printer.printText(order.getRefundUser() + "\n");
                        } else {
                            printer.printText(refundUser + ":" + order.getRefundUser() + "\n");
                        }
                    } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                        if (order.getRefundUserRealName().length() > 16) {
                            printer.printText(refundUser + ": \n");
                            StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                            String user = sbf.substring(0, 16);
                            printer.printText(user + "\r\n");
                            String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                            printer.printText(printuser + "\r\n");
                        } else if (order.getRefundUserRealName().length() > 12) {
                            printer.printText(refundUser + ": \n");
                            printer.printText(order.getRefundUserRealName() + "\n");
                        } else {
                            printer.printText(refundUser + ": " + order.getRefundUserRealName() + "\n");
                        }
                    }

                    printer.printText(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()) + "\n");
                    if (order.getRefundMoney() > 0) {
                        printer.printText(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元\n");
                    }
                    printer.printText(UIUtils.getString(R.string.print_refund_instruction) + "\n");
                } else {
                    if (!TextUtils.isEmpty(order.getTransactionId())) {
                        printer.printText(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": \n");
                        printer.printText(order.getTransactionId() + "\n");
                    }
                    if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                        printer.printText(UIUtils.getString(R.string.tv_print_order_no) + "：\n");
                        printer.printText(order.getOrderNo() + "\n");
                    }
                    printer.printText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()) + "\n");
                    printer.printText(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n");
                    try {
                        printer.printText(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeTime() + "\n");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                    if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                        if (order.getCashierName().length() > 16) {
                            printer.printText(cashierTitle + ": \n");
                            StringBuffer sbf = new StringBuffer(order.getCashierName());
                            String oneuser = sbf.substring(0, 16);
                            printer.printText(oneuser + "\n");
                            String twouser = sbf.substring(16, order.getCashierName().length());
                            printer.printText(twouser + "\n");
                        } else if (order.getCashierName().length() > 12) {
                            printer.printText(cashierTitle + ": \n");
                            printer.printText(order.getCashierName() + "\n");
                        } else {
                            printer.printText(cashierTitle + ": " + order.getCashierName() + "\n");
                        }

                    } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                        if (order.getOpUserRealName().length() > 16) {
                            printer.printText(cashierTitle + ": \n");
                            StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                            String oneuser = sbf.substring(0, 16);
                            printer.printText(oneuser + "\n");
                            String twouser = sbf.substring(16, order.getOpUserRealName().length());
                            printer.printText(twouser + "\n");
                        } else if (order.getOpUserRealName().length() > 12) {
                            printer.printText(cashierTitle + ": \n");
                            printer.printText(order.getOpUserRealName() + "\n");
                        } else {
                            printer.printText(cashierTitle + ": " + order.getOpUserRealName() + "\n");
                        }
                    }
                    printer.printText(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元\n");
                    printer.printText(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元\n");
                    printer.printText(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元\n");
                }

                //备注
                if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                    if (order.getAttach().length() > 25) {
                        printer.printText(UIUtils.getString(R.string.print_trade_attach_title) + "：\r\n");
                        StringBuffer sbf = new StringBuffer(order.getAttach());
                        String one = sbf.substring(0, 25);
                        Format format1 = new Format();
                        format1.setHzSize(Format.HZ_DOT16x16);
                        format1.setHzScale(Format.HZ_SC1x2);
                        format1.setAscSize(Format.ASC_DOT24x12);
                        format1.setAscScale(Format.ASC_SC1x1);
                        printer.setFormat(format1);
                        printer.printText(one + "\r\n");
                        //printer.printText("\r\n");
                        Format format2 = new Format();
                        format2.setHzSize(Format.HZ_DOT16x16);
                        format2.setHzScale(Format.HZ_SC1x2);
                        format2.setAscSize(Format.ASC_DOT24x12);
                        format2.setAscScale(Format.ASC_SC1x1);
                        printer.setFormat(format2);
                        String two = sbf.substring(25, sbf.length() - 1);
                        printer.printText(two + "\r\n");

                    } else {
                        printer.printText(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach() + "\r\n");
                    }
                }

                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                printer.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
                //顾客签名
                if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
                    printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：\n");
                }
                printer.printText("\n");

                if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                    String printCode;
                    if (ConfigUtil.printCodeBillEnable()) {
                        printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                    } else {
                        printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                    }
                    printer.printText(Alignment.CENTER, ConfigUtil.getPrintCodeTitle() + "\n");
                    printer.printQrCode(Alignment.CENTER, new QrCode(printCode, QrCode.ECLEVEL_M), 240);
                }
                if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
                    if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                            && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                        printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                        String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                        if (!TextUtils.isEmpty(title)) {
                            if (title.length() > 15) {
                                StringBuffer sbf = new StringBuffer(title);
                                String one = sbf.substring(0, 15);
                                printer.printText(one + "\n");
                                String two = sbf.substring(15, title.length());
                                printer.printText(two + "\r\n");
                            } else {
                                printer.printText(title + "\n");
                            }
                        }
                        printer.printQrCode(Alignment.CENTER,
                                new QrCode(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL), QrCode.ECLEVEL_M), 240);
                    }
                }
                printer.printText(Alignment.CENTER, "\n" + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
            }
        });
    }
}
