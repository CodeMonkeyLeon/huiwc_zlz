package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.printer.PrintTools;
import android.printerstatment.PrinterStat;
import android.text.TextUtils;
import android.util.Log;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print
 * @创建者: Jeremy
 * @创建时间: 2018/12/3 13:39
 * @描述: ${TODO}
 */
public class PrinterHdyUtil {

    private byte mPrintLanguage;
    private static PrinterHdyUtil singleton;

    public static PrinterHdyUtil with() {
        if (singleton == null) {
            synchronized (PrinterHdyUtil.class) {
                if (singleton == null) {
                    singleton = new PrinterHdyUtil();
                }
            }
        }
        return singleton;
    }

    public void print(Context context, TradeDetailBean order) {

        /**
         * 检测打印机状态
         */
        try {
            boolean printer_stat = PrintTools.OpenPrinter("/dev/ttyS4", 115200);
            if (printer_stat == false) {
                MyToast.showToastShort("open printer fail!!!");
                return;
            }
            int ret = PrintTools.CheckPrinterStatus();

            if (PrinterStat.PRINT_OUT_POWER == ret) {
                Log.i("lxm", "打印机未上电");
                MyToast.showToastShort("打印机未上电");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_PAPER == ret) {
                Log.i("lxm", "打印机缺纸");
                MyToast.showToastShort("打印机缺纸");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_TEMP == ret) {
                Log.i("lxm", "打印机过温");
                MyToast.showToastShort("打印机过温");
                //Toast.makeText(prinkActivity.this, getResources().getString(R.string.printer_tempfail), Toast.LENGTH_SHORT).show();
                PrintTools.ClosePrinter();
                return;
            }
            PrintTools.InitPrinter();
            PrintTools.OutCnGbk();

            Locale currentLocale = MyApplication.getContext().getResources().getConfiguration().locale;
            Log.d("wsq", "currentLocale ===== " + currentLocale);

            if ("pt".equals(currentLocale.getLanguage())) {
                Log.d("wsq", "currentLocale.getLanguage =====pt ");
                PrintTools.SetLangPage(PrinterStat.LANG_PT);
                mPrintLanguage = PrinterStat.LANG_PT;
            } else {
                PrintTools.EnterCnGbk();
                mPrintLanguage = PrinterStat.LANG_CN;
            }
            PrintTools.SetAligned(1);
            PrintTools.PrintCodePage("\n", mPrintLanguage);
            if (order.getTradeType() == 1) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_recharge_title) + "\n", mPrintLanguage);
            } else if (order.getTradeType() == 2) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_verification_title) + "\n", mPrintLanguage);
            } else {
                if (!order.isPay()) {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.print_refund_title) + "\n", mPrintLanguage);
                } else {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pay_title) + "\n", mPrintLanguage);
                }
            }
            StringBuilder builder = new StringBuilder();
            // 空格
            builder.append("\n");
            builder.append(order.getPartner() + "              " + "请妥善保存" + "\n");
            builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    builder.append(UIUtils.getString(R.string.shop_name) + ": " + "\n");
                    builder.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    builder.append(two + "\n");
                } else if (MyApplication.getMerchantName().length() > 11) {
                    builder.append(UIUtils.getString(R.string.shop_name) + ": " + "\n");
                    builder.append(MyApplication.getMerchantName() + "\n");
                } else {
                    builder.append(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                builder.append(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
            }

            String storeNameTitle;
            if (order.isPay()) {
                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
            } else {
                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    builder.append(storeNameTitle + ": " + "\n");
                    builder.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    builder.append(two + "\n");
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    builder.append(storeNameTitle + ": " + "\n");
                    builder.append(order.getStoreMerchantIdCnt() + "\n");
                } else {
                    builder.append(storeNameTitle + ": " + order.getStoreMerchantIdCnt() + "\n");
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                if (order.getCashPointName().length() > 16) {
                    String str = order.getCashPointName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    builder.append("收银点: " + "\n");
                    builder.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    builder.append(two + "\n");
                } else if (order.getCashPointName().length() > 11) {
                    builder.append("收银点: " + "\n");
                    builder.append(order.getCashPointName() + "\n");
                } else {
                    builder.append("收银点: " + order.getCashPointName() + "\n");
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                builder.append(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()) + "\n");
            }
            if (!order.isPay()) {
                builder.append(UIUtils.getString(R.string.print_refund_no_title) + ": " + "\n");
                builder.append(order.getRefundNo() + "\n");
                builder.append(UIUtils.getString(R.string.print_refund_time_title) + ": " + "\n");
                builder.append(order.getRefundTime() + "\n");

                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        builder.append(refundUser + ": " + "\n");
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        builder.append(user + "\n");
                        String printUser = sbf.substring(16, order.getRefundUser().length());
                        builder.append(printUser + "\n");
                    } else if (order.getRefundUser().length() > 12) {
                        builder.append(refundUser + ": " + "\n");
                        builder.append(order.getRefundUser() + "\n");
                    } else {
                        builder.append(refundUser + ": " + order.getRefundUser() + "\n");
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        builder.append(refundUser + ": " + "\n");
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        builder.append(user + "\n");
                        String printUser = sbf.substring(16, order.getRefundUserRealName().length());
                        builder.append(printUser + "\n");
                    } else if (order.getRefundUserRealName().length() > 12) {
                        builder.append(refundUser + ": " + "\n");
                        builder.append(order.getRefundUserRealName() + "\n");
                    } else {
                        builder.append(refundUser + ": " + order.getRefundUserRealName() + "\n");
                    }
                }
                builder.append(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()) + "\n");
                if (order.getRefundMoney() > 0) {
                    builder.append(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元" + "\n");
                }
                builder.append(UIUtils.getString(R.string.print_refund_instruction) + "\n");
            } else {
                if (!TextUtils.isEmpty(order.getTransactionId())) {
                    builder.append(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": " + "\n");
                    builder.append(order.getTransactionId() + "\n");
                }

                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    builder.append(UIUtils.getString(R.string.tv_print_order_no) + ": " + "\n");
                    builder.append(order.getOrderNo() + "\n");
                }
                builder.append(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()) + "\n");
                builder.append(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n");
                try {
                    builder.append(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime() + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        builder.append(cashierTitle + ": " + "\n");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String user = sbf.substring(0, 16);
                        builder.append(user + "\n");
                        String printUser = sbf.substring(16, order.getCashierName().length());
                        builder.append(printUser + "\n");
                    } else if (order.getCashierName().length() > 12) {
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(order.getCashierName() + "\n");
                    } else {
                        builder.append(cashierTitle + ": " + order.getCashierName() + "\n");
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        builder.append(cashierTitle + ": " + "\n");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String user = sbf.substring(0, 16);
                        builder.append(user + "\n");
                        String printUser = sbf.substring(16, order.getOpUserRealName().length());
                        builder.append(printUser + "\n");
                    } else if (order.getOpUserRealName().length() > 12) {
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(order.getOpUserRealName() + "\n");
                    } else {
                        builder.append(cashierTitle + ": " + order.getOpUserRealName() + "\n");
                    }
                }
                builder.append(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元" + "\n");
                builder.append(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元" + "\n");
                builder.append(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元" + "\n");
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                builder.append(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach() + "\n");
            }

            builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
            builder.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                builder.append(UIUtils.getString(R.string.print_client_sign_title) + ": " + "\n");
            }

            builder.append("\n");
            PrintTools.SetAligned(0);
            PrintTools.PrintCodePage(builder.toString(), mPrintLanguage);
            PrintTools.SetAligned(1);

            if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                PrintTools.PrintCodePage(ConfigUtil.getPrintCodeTitle(), mPrintLanguage);
                PrintTools.PrintFeed(1);
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                } else {
                    printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                }
                Bitmap bitmap = MaxCardManager.getInstance().create2DCode(printCode,
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                PrintTools.SetImageLeft(90);
                PrintTools.PrintImage(bitmap);//打印二维码
            }

            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    StringBuilder scanBuilder = new StringBuilder();
                    scanBuilder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        scanBuilder.append(title + "\n");
                    }
                    PrintTools.SetAligned(0);
                    PrintTools.PrintCodePage(scanBuilder.toString(), mPrintLanguage);
                    if (!TextUtils.isEmpty(activeUrl)) {
                        Bitmap scanBitmap = MaxCardManager.getInstance().create2DCode(activeUrl,
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                        PrintTools.SetImageLeft(90);
                        PrintTools.PrintImage(scanBitmap);//打印二维码
                    }
                }
            }
            PrintTools.PrintFeed(4);

            /**
             * 关闭打印
             */
            PrintTools.ClosePrinter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pledgePrint(Context context, PledgePayBean.DataBean order) {

        /**
         * 检测打印机状态
         */
        try {
            boolean printer_stat = PrintTools.OpenPrinter("/dev/ttyS4", 115200);
            if (printer_stat == false) {
                MyToast.showToastShort("open printer fail!!!");
                return;
            }
            int ret = PrintTools.CheckPrinterStatus();

            if (PrinterStat.PRINT_OUT_POWER == ret) {
                Log.i("lxm", "打印机未上电");
                MyToast.showToastShort("打印机未上电");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_PAPER == ret) {
                Log.i("lxm", "打印机缺纸");
                MyToast.showToastShort("打印机缺纸");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_TEMP == ret) {
                Log.i("lxm", "打印机过温");
                MyToast.showToastShort("打印机过温");
                PrintTools.ClosePrinter();
                return;
            }
            PrintTools.InitPrinter();
            PrintTools.OutCnGbk();

            Locale currentLocale = MyApplication.getContext().getResources().getConfiguration().locale;
            Log.d("wsq", "currentLocale ===== " + currentLocale);

            if ("pt".equals(currentLocale.getLanguage())) {
                Log.d("wsq", "currentLocale.getLanguage =====pt ");
                PrintTools.SetLangPage(PrinterStat.LANG_PT);
                mPrintLanguage = PrinterStat.LANG_PT;
            } else {
                PrintTools.EnterCnGbk();
                mPrintLanguage = PrinterStat.LANG_CN;
            }
            PrintTools.SetAligned(1);
            PrintTools.PrintCodePage("\n", mPrintLanguage);

            if (order.isToPay()) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_title_pledge_pay) + "\n", mPrintLanguage);
            } else {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_title_pledge) + "\n", mPrintLanguage);
            }
            StringBuilder builder = new StringBuilder();
            // 空格
            builder.append("\n");
            builder.append(order.getPartner() + "              " + "请妥善保存" + "\n");
            builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    builder.append(UIUtils.getString(R.string.shop_name) + ": " + "\n");
                    builder.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    builder.append(two + "\n");
                } else if (MyApplication.getMerchantName().length() > 11) {
                    builder.append(UIUtils.getString(R.string.shop_name) + ": " + "\n");
                    builder.append(MyApplication.getMerchantName() + "\n");
                } else {
                    builder.append(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                builder.append(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
            }

            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    builder.append(UIUtils.getString(R.string.trade_store_name) + ": " + "\n");
                    builder.append(one + "\n");
                    String two = sbf.substring(16, str.length());
                    builder.append(two + "\n");
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    builder.append(UIUtils.getString(R.string.trade_store_name) + ": " + "\n");
                    builder.append(order.getStoreMerchantIdCnt() + "\n");
                } else {
                    builder.append(UIUtils.getString(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt() + "\n");
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                builder.append(UIUtils.getString(R.string.print_title_device_info) + ": " + StringUtils.getDeviceNo(order.getTermNo()) + "\n");
            }
            try {
                builder.append(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime() + "\n");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                builder.append(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：" + "\n");
                builder.append(order.getOutTransactionId());
            }
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                builder.append(UIUtils.getString(R.string.tv_pledge_order_no) + ": " + "\n");
                builder.append(order.getAuthNo());
            }

            builder.append(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()) + "\n");

            if (!order.isToPay()) {
                if (!TextUtils.isEmpty(order.getMoney())) {
                    builder.append(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元" + "\n");
                } else {
                    builder.append(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元" + "\n");
                }
            }
            if (order.isToPay()) {
                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                    builder.append(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元" + "\n");
                } else {
                    builder.append(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元" + "\n");
                }
                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                    builder.append(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元" + "\n");
                } else {
                    builder.append(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元" + "\n");
                }
            }

            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 12) {
                        builder.append(cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String oneUser = sbf.substring(0, 12);
                        builder.append(oneUser);
                        String twoUser = sbf.substring(12, order.getCashierName().length());
                        builder.append(twoUser);
                    }
                    if (order.getCashierName().length() > 12) {
                        builder.append(cashierTitle + ": ");
                        builder.append(order.getCashierName());
                    } else {
                        builder.append(cashierTitle + ": " + order.getCashierName());
                    }
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                String opUserTitle;
                if (order.isToPay()) {
                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                } else {
                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                }
                if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 12) {
                        builder.append(opUserTitle);
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneUser = sbf.substring(0, 12);
                        builder.append(oneUser);
                        String twoUser = sbf.substring(12, order.getOpUserRealName().length());
                        builder.append(twoUser);
                    }
                    if (order.getOpUserRealName().length() > 12) {
                        builder.append(opUserTitle);
                        builder.append(order.getOpUserRealName());
                    } else {
                        builder.append(opUserTitle + order.getOpUserRealName());
                    }
                }
            }
            //付款备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                builder.append(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach() + "\n");
            }
            builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
            builder.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");

            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                builder.append(UIUtils.getString(R.string.print_client_sign_title) + ": " + "\n");
            }

            builder.append("\n");
            if (!order.isToPay() && !TextUtils.isEmpty(order.getAuthNo())) {
                builder.append("      " + UIUtils.getString(R.string.print_pledge_scan_title));
            }
            PrintTools.SetAligned(0);
            PrintTools.PrintCodePage(builder.toString(), mPrintLanguage);
            PrintTools.PrintFeed(1);

            if (!TextUtils.isEmpty(order.getAuthNo())) {
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
                } else {
                    printCode = order.getAuthNo();
                }
                Bitmap bitmap = MaxCardManager.getInstance().create2DCode(printCode,
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                        DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                //Bitmap bitmap = BitmapFactory.decodeResource(MyApplication.getContext().getResources(),R.drawable.zedpublic_erweim_danse);
                PrintTools.SetImageLeft(90);
                PrintTools.PrintImage(bitmap);//打印二维码
            }

            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    StringBuilder scanBuilder = new StringBuilder();
                    scanBuilder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        scanBuilder.append(title + "\n");
                    }
                    PrintTools.SetAligned(0);
                    PrintTools.PrintCodePage(scanBuilder.toString(), mPrintLanguage);
                    if (!TextUtils.isEmpty(activeUrl)) {
                        Bitmap scanBitmap = MaxCardManager.getInstance().create2DCode(activeUrl,
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                        PrintTools.SetImageLeft(90);
                        PrintTools.PrintImage(scanBitmap);//打印二维码
                    }
                }
            }
            PrintTools.SetAligned(1);
            if (!order.isToPay()) {
                StringBuilder noticeBuilder = new StringBuilder();
                noticeBuilder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                noticeBuilder.append(UIUtils.getString(R.string.print_pledge_notice) + "\n");
                noticeBuilder.append("\n" + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n");
                PrintTools.PrintCodePage(noticeBuilder.toString(), mPrintLanguage);
            }
            PrintTools.PrintFeed(4);

            /**
             * 关闭打印
             */
            PrintTools.ClosePrinter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printTotal(ReportBean.DataEntity info) {

        /**
         * 检测打印机状态
         */
        try {
            boolean printer_stat = PrintTools.OpenPrinter("/dev/ttyS4", 115200);
            if (printer_stat == false) {
                MyToast.showToastShort("open printer fail!!!");
                return;
            }
            int ret = PrintTools.CheckPrinterStatus();

            if (PrinterStat.PRINT_OUT_POWER == ret) {
                Log.i("lxm", "打印机未上电");
                MyToast.showToastShort("打印机未上电");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_PAPER == ret) {
                Log.i("lxm", "打印机缺纸");
                MyToast.showToastShort("打印机缺纸");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_TEMP == ret) {
                Log.i("lxm", "打印机过温");
                MyToast.showToastShort("打印机过温");
                //Toast.makeText(prinkActivity.this, getResources().getString(R.string.printer_tempfail), Toast.LENGTH_SHORT).show();
                PrintTools.ClosePrinter();
                return;
            }
            PrintTools.InitPrinter();
            PrintTools.OutCnGbk();

            Locale currentLocale = MyApplication.getContext().getResources().getConfiguration().locale;
            Log.d("wsq", "currentLocale ===== " + currentLocale);

            if ("pt".equals(currentLocale.getLanguage())) {
                Log.d("wsq", "currentLocale.getLanguage =====pt ");
                PrintTools.SetLangPage(PrinterStat.LANG_PT);
                mPrintLanguage = PrinterStat.LANG_PT;
            } else {
                PrintTools.EnterCnGbk();
                mPrintLanguage = PrinterStat.LANG_CN;
            }
            PrintTools.SetAligned(1);
            PrintTools.PrintCodePage("\n", mPrintLanguage);
            if (info.getType() == 1) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pay_report_title) + "\n", mPrintLanguage);
            } else if (info.getType() == 2) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_consume_report_title) + "\n", mPrintLanguage);
            } else if (info.getType() == 3) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_fk_consume_report_title) + "\n", mPrintLanguage);
            }

            PrintTools.SetAligned(0);
            PrintTools.PrintCodePage("\n", mPrintLanguage);
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.shop_name) + "：" + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(one + "\n", mPrintLanguage);
                    String two = sbf.substring(16, str.length());
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.shop_name) + "：" + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(MyApplication.getMerchantName() + "\n", mPrintLanguage);
                } else {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", mPrintLanguage);
                }
            }
            // 商户号
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", mPrintLanguage);
            }

            // 门店
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": 全部门店\n", mPrintLanguage);
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": \n", mPrintLanguage);
                    PrintTools.PrintCodePage(one + "\n", mPrintLanguage);
                    String two = sbf.substring(16, str.length());
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (info.getStoreName().length() > 11) {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": \n", mPrintLanguage);
                    PrintTools.PrintCodePage(info.getStoreName() + "\n", mPrintLanguage);
                } else {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n", mPrintLanguage);
                }
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                PrintTools.PrintCodePage(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n", mPrintLanguage);
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    PrintTools.PrintCodePage(cashierTitle + ": " + one + "\n", mPrintLanguage);
                    String two = sbf.substring(16, str.length());
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (info.getStoreName().length() > 12) {
                    PrintTools.PrintCodePage(cashierTitle + ": \n", mPrintLanguage);
                    PrintTools.PrintCodePage(info.getCashierName() + "\n", mPrintLanguage);
                } else {
                    String str = info.getCashierName();
                    PrintTools.PrintCodePage(cashierTitle + ": " + str + "\n", mPrintLanguage);
                }
            }
            if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                if (info.getCashierPointName().length() > 16) {
                    StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                    String one = sbf.substring(0, 16);
                    String two = sbf.substring(16, info.getCashierPointName().length());
                    PrintTools.PrintCodePage("收银点: " + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(one + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (info.getCashierPointName().length() > 12) {
                    PrintTools.PrintCodePage("收银点: " + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(info.getCashierPointName() + "\n", mPrintLanguage);
                } else {
                    PrintTools.PrintCodePage("收银点: " + info.getCashierPointName() + "\n", mPrintLanguage);
                }
            }

            //开始时间
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\n", mPrintLanguage);
            // 结束时间
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n", mPrintLanguage);

            PrintTools.PrintCodePage("\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_report_total_title) + "\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_single_horizontal) + "\n", mPrintLanguage);

            if (info.getType() == 1) {
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n", mPrintLanguage);
            } else if (info.getType() == 2) {
                /*PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n", mPrintLanguage);*/

                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔") + "\n", mPrintLanguage);

                /*PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔") + "\n", mPrintLanguage);*/
            } else if (info.getType() == 3) {
                //饭卡消费
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "" : info.getSettleRate() + "") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元") + "\n", mPrintLanguage);

                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n", mPrintLanguage);
                PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n", mPrintLanguage);
            }
            if (info.getType() == 1) {
                List<ReportBean.DataEntity.ListEntity> list = info.getList();
                if (list != null && list.size() > 0) {
                    PrintTools.PrintCodePage("\n", mPrintLanguage);
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.print_trade_type_list_title) + ": " + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.print_single_horizontal) + "\n", mPrintLanguage);

                    for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                        PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元") + "\n", mPrintLanguage);
                        PrintTools.PrintCodePage(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔") + "\n", mPrintLanguage);
                    }
                }
            }
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_single_horizontal) + "\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n", mPrintLanguage);

            PrintTools.PrintFeed(5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printPledgeTotal(PledgeReportBean.DataBean info) {

        /**
         * 检测打印机状态
         */
        try {
            boolean printer_stat = PrintTools.OpenPrinter("/dev/ttyS4", 115200);
            if (printer_stat == false) {
                MyToast.showToastShort("open printer fail!!!");
                return;
            }
            int ret = PrintTools.CheckPrinterStatus();

            if (PrinterStat.PRINT_OUT_POWER == ret) {
                Log.i("lxm", "打印机未上电");
                MyToast.showToastShort("打印机未上电");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_PAPER == ret) {
                Log.i("lxm", "打印机缺纸");
                MyToast.showToastShort("打印机缺纸");
                PrintTools.ClosePrinter();
                return;
            }
            if (PrinterStat.PRINT_OUT_TEMP == ret) {
                Log.i("lxm", "打印机过温");
                MyToast.showToastShort("打印机过温");
                //Toast.makeText(prinkActivity.this, getResources().getString(R.string.printer_tempfail), Toast.LENGTH_SHORT).show();
                PrintTools.ClosePrinter();
                return;
            }
            PrintTools.InitPrinter();
            PrintTools.OutCnGbk();

            Locale currentLocale = MyApplication.getContext().getResources().getConfiguration().locale;
            Log.d("wsq", "currentLocale ===== " + currentLocale);

            if ("pt".equals(currentLocale.getLanguage())) {
                Log.d("wsq", "currentLocale.getLanguage =====pt ");
                PrintTools.SetLangPage(PrinterStat.LANG_PT);
                mPrintLanguage = PrinterStat.LANG_PT;
            } else {
                PrintTools.EnterCnGbk();
                mPrintLanguage = PrinterStat.LANG_CN;
            }
            PrintTools.SetAligned(1);
            PrintTools.PrintCodePage("\n", mPrintLanguage);

            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_title_pledge_report) + "\n", mPrintLanguage);
            PrintTools.SetAligned(0);
            PrintTools.PrintCodePage("\n", mPrintLanguage);
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.shop_name) + "：" + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(one + "\n", mPrintLanguage);
                    String two = sbf.substring(16, str.length());
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.shop_name) + "：" + "\n", mPrintLanguage);
                    PrintTools.PrintCodePage(MyApplication.getMerchantName() + "\n", mPrintLanguage);
                } else {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", mPrintLanguage);
                }
            }
            // 商户号
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", mPrintLanguage);
            }

            // 门店
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": 全部门店\n", mPrintLanguage);
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": \n", mPrintLanguage);
                    PrintTools.PrintCodePage(one + "\n", mPrintLanguage);
                    String two = sbf.substring(16, str.length());
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (info.getStoreName().length() > 11) {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": \n", mPrintLanguage);
                    PrintTools.PrintCodePage(info.getStoreName() + "\n", mPrintLanguage);
                } else {
                    PrintTools.PrintCodePage(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n", mPrintLanguage);
                }
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                PrintTools.PrintCodePage(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n", mPrintLanguage);
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    PrintTools.PrintCodePage(cashierTitle + ": " + one + "\n", mPrintLanguage);
                    String two = sbf.substring(16, str.length());
                    PrintTools.PrintCodePage(two + "\n", mPrintLanguage);
                } else if (info.getStoreName().length() > 12) {
                    PrintTools.PrintCodePage(cashierTitle + ": \n", mPrintLanguage);
                    PrintTools.PrintCodePage(info.getCashierName() + "\n", mPrintLanguage);
                } else {
                    String str = info.getCashierName();
                    PrintTools.PrintCodePage(cashierTitle + ": " + str + "\n", mPrintLanguage);
                }
            }

            //开始时间
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\n", mPrintLanguage);
            // 结束时间
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n", mPrintLanguage);

            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_single_horizontal) + "\n", mPrintLanguage);

            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_refund_count) + "：" + info.getCntFreeMoney() + "笔", mPrintLanguage);
//            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_single_horizontal) + "\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", mPrintLanguage);
            PrintTools.PrintCodePage(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n", mPrintLanguage);

            PrintTools.PrintFeed(5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

