package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Alignment;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;

/**
 * 新大陆 N900打印 <一句话功能简述> <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2016-2-4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class PrinterPledgeApos {

    private Context context;
    private StaticLayout mLayout = null;
    private TextPaint mPaint;
    private Canvas mCanvas;
    private String content = null;
    private Printer.Progress progress = new Printer.Progress() {

        @Override
        public void doPrint(Printer arg0) throws Exception {
            onStartPrint();
        }

        @Override
        public void onFinish(int code) {
            if (code == Printer.ERROR_NONE) {
                displayPrinterInfo("PRINT SUCCESS END ");
            }
            /**
             * Has some error. Here is display it, but you may want to hanle the
             * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
             * again in the right time later.
             */
            else {
                displayPrinterInfo("PRINT ERR - " + (code));
            }
        }

        @Override
        public void onCrash() {
            onDeviceServiceCrash();
        }

    };

    /**
     * Search card and show all track info
     */
    public void startPrint() {
        try {
            progress.start();
            // DeviceService.logout();
        } catch (RequestException e) {
            e.printStackTrace();
            onDeviceServiceCrash();
        }
    }

    protected abstract void displayPrinterInfo(String info);

    protected abstract void onStartPrint();

    protected abstract void onDeviceServiceCrash();

    public PrinterPledgeApos(final Context context, final PledgePayBean.DataBean order) {
        this.context = context;
        progress.addStep(new Printer.Step() {

            @Override
            public void doPrint(Printer printer)
                    throws Exception {
                printer.setAutoTrunc(true);

                // Default mode is real mode, now set it to virtual mode.
                printer.setMode(Printer.MODE_VIRTUAL);
                Format format = new Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);
                if (order.isToPay()) {
                    printer.printText("           " + UIUtils.getString(R.string.print_title_pledge_pay) + "\n");
                } else {
                    printer.printText("           " + UIUtils.getString(R.string.print_title_pledge) + "\n");
                }

                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x1);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x1);
                printer.setFormat(format);

                printer.printText(order.getPartner() + "              请妥善保存\n");
                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(MyApplication.getMerchantName() + "\n");
                    } else {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }


                //商户号

                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
                }
                if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                    if (order.getStoreMerchantIdCnt().length() > 16) {
                        String str = order.getStoreMerchantIdCnt();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(ToastHelper.toStr(R.string.trade_store_name) + "：" + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (order.getStoreMerchantIdCnt().length() > 11) {
                        printer.printText(ToastHelper.toStr(R.string.trade_store_name) + "：" + "\n");
                        printer.printText(order.getStoreMerchantIdCnt() + "\n");
                    } else {
                        printer.printText(ToastHelper.toStr(R.string.trade_store_name) + "：" + order.getStoreMerchantIdCnt() + "\n");
                    }
                }
                if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                    printer.printText(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()) + "\n");
                }
                try {
                    printer.printText(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime() + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                    printer.printText(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：" + "\n");
                    printer.printText(order.getOutTransactionId() + "\n");
                }
                if (!TextUtils.isEmpty(order.getAuthNo())) {
                    printer.printText(UIUtils.getString(R.string.tv_pledge_order_no) + ": " + "\n");
                    printer.printText(order.getAuthNo() + "\n");
                }
                printer.printText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()) + "\n");

                if (!order.isToPay()) {
                    if (!TextUtils.isEmpty(order.getMoney())) {
                        printer.printText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元" + "\n");
                    } else {
                        printer.printText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元" + "\n");
                    }
                }
                if (order.isToPay()) {
                    if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                        printer.printText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元" + "\n");
                    } else {
                        printer.printText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元" + "\n");
                    }
                    if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                        printer.printText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元" + "\n");
                    } else {
                        printer.printText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元" + "\n");
                    }
                }
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                        if (order.getCashierName().length() > 16) {
                            printer.printText(cashierTitle + ": " + "\n");
                            StringBuffer sbf = new StringBuffer(order.getCashierName());
                            String oneUser = sbf.substring(0, 16);
                            printer.printText(oneUser + "\n");
                            String twoUser = sbf.substring(16, order.getCashierName().length());
                            printer.printText(twoUser + "\n");
                        }
                        if (order.getCashierName().length() > 12) {
                            printer.printText(cashierTitle + ": " + "\n");
                            printer.printText(order.getCashierName() + "\n");
                        } else {
                            printer.printText(cashierTitle + ": " + order.getCashierName() + "\n");
                        }
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    String opUserTitle;
                    if (order.isToPay()) {
                        opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                    } else {
                        opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                    }
                    if (order.getOpUserRealName().length() > 16) {
                        printer.printText(opUserTitle + "\n");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneUser = sbf.substring(0, 16);
                        printer.printText(oneUser + "\n");
                        String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                        printer.printText(twoUser + "\n");
                    }
                    if (order.getOpUserRealName().length() > 12) {
                        printer.printText(opUserTitle + "\n");
                        printer.printText(order.getOpUserRealName() + "\n");
                    } else {
                        printer.printText(opUserTitle + order.getOpUserRealName() + "\n");
                    }
                }

                //备注
                if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                    if (order.getAttach().length() > 25) {
                        printer.printText(UIUtils.getString(R.string.print_pledge_attach_title) + ": \r\n");
                        StringBuffer sbf = new StringBuffer(order.getAttach());
                        String one = sbf.substring(0, 25);
                        Format format1 = new Format();
                        format1.setHzSize(Format.HZ_DOT16x16);
                        format1.setHzScale(Format.HZ_SC1x2);
                        format1.setAscSize(Format.ASC_DOT24x12);
                        format1.setAscScale(Format.ASC_SC1x1);
                        printer.setFormat(format1);
                        printer.printText(one + "\r\n");
                        //printer.printText("\r\n");
                        Format format2 = new Format();
                        format2.setHzSize(Format.HZ_DOT16x16);
                        format2.setHzScale(Format.HZ_SC1x2);
                        format2.setAscSize(Format.ASC_DOT24x12);
                        format2.setAscScale(Format.ASC_SC1x1);
                        printer.setFormat(format2);
                        String two = sbf.substring(25, sbf.length() - 1);
                        printer.printText(two + "\r\n");

                    } else {
                        printer.printText(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach() + "\r\n");
                    }
                }

                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                printer.printText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
                if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
                    printer.printText(UIUtils.getString(R.string.print_client_sign_title) + ":\n");
                }

                printer.printText("\n");

                if (!TextUtils.isEmpty(order.getAuthNo())) {
                    if (!order.isToPay()) {
                        printer.printText(Alignment.CENTER, UIUtils.getString(R.string.print_pledge_scan_title) + "\n");
                    }
                    if (ConfigUtil.printCodeBillEnable()) {
                        printer.printQrCode(Alignment.CENTER,
                                new QrCode(Constants.URL_PRINT_QRCODE + order.getAuthNo(), QrCode.ECLEVEL_Q), 184);
                    } else {
                        printer.printQrCode(Alignment.CENTER,
                                new QrCode(order.getAuthNo(), QrCode.ECLEVEL_Q), 184);
                    }
                }

                if (order.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
                    if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                            && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                        printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                        String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                        if (!TextUtils.isEmpty(title)) {
                            if (title.length() > 16) {
                                StringBuffer sbf = new StringBuffer(title);
                                String one = sbf.substring(0, 16);
                                printer.printText(one + "\n");
                                String two = sbf.substring(16, title.length());
                                printer.printText(two + "\r\n");
                            } else {
                                printer.printText(title + "\n");
                            }
                        }
                        printer.printQrCode(Alignment.CENTER,
                                new QrCode(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL), QrCode.ECLEVEL_Q),
                                184);
                    }
                }
                if (!order.isToPay()) {
                    printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                    String titlePledge = UIUtils.getString(R.string.print_pledge_notice);
                    if (titlePledge.length() > 16) {
                        StringBuffer sbf = new StringBuffer(titlePledge);
                        String one = sbf.substring(0, 16);
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, titlePledge.length());
                        printer.printText(two + "\r\n");
                    } else {
                        printer.printText(UIUtils.getString(R.string.print_pledge_notice) + "\n");
                    }
                }
                printer.printText(Alignment.CENTER, "\n" + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
            }
        });
    }
}
