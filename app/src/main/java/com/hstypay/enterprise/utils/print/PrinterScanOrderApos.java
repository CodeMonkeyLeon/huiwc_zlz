package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PrintContentBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * 联迪A8
 *
 * @author Jeremy
 * @version [版本号, 2019-09-20]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class PrinterScanOrderApos {

    private Context context;
    private StaticLayout mLayout = null;
    private TextPaint mPaint;
    private Canvas mCanvas;
    private String content = null;
    private Printer.Progress progress = new Printer.Progress() {

        @Override
        public void doPrint(Printer arg0) throws Exception {
            onStartPrint();
        }

        @Override
        public void onFinish(int code) {
            if (code == Printer.ERROR_NONE) {
                displayPrinterInfo("PRINT SUCCESS END ");
            }
            /**
             * Has some error. Here is display it, but you may want to hanle the
             * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
             * again in the right time later.
             */
            else {
                displayPrinterInfo("PRINT ERR - " + (code));
            }
        }

        @Override
        public void onCrash() {
            onDeviceServiceCrash();
        }

    };

    /**
     * Search card and show all track info
     */
    public void startPrint() {
        try {
            progress.start();
            // DeviceService.logout();
        } catch (RequestException e) {
            e.printStackTrace();
            onDeviceServiceCrash();
        }
    }

    protected abstract void displayPrinterInfo(String info);

    protected abstract void onStartPrint();

    protected abstract void onDeviceServiceCrash();

    public PrinterScanOrderApos(final Context context,final PrintContentBean bean, boolean isClient) {
        this.context = context;
        bean.setStoreName("姚餐厅");
        progress.addStep(new Printer.Step() {

            @Override
            public void doPrint(Printer printer)
                    throws Exception {
                printer.setAutoTrunc(true);
                printer.setMode(Printer.MODE_VIRTUAL);
                Printer.Format format = new Printer.Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);
                printer.printText(PrintFormatUtils.printCenterData(bean.getStoreName()) + "\n");
                printer.printText("\r\n");

                format.setAscSize(Printer.Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x1);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x1);
                printer.setFormat(format);
                printer.printText(UIUtils.getString(R.string.print_scan_order_title) + "\n");
                printer.printText("\r\n");
                printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_desk_no_title), bean.getTableCode() + "\n"));
                printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_trade_time_title), bean.getPayTime() + "\n"));
                printer.printText(UIUtils.getString(R.string.print_order_trade_no_title) + "\n");
                printer.printText(bean.getOrderId() + "\n");

                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                printer.printText("\r\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
                printer.printText("\r\n");
            }
        });
    }

    public static void payOrde(PrintContentBean bean, boolean isClient) {
        if (MyApplication.bluetoothSocket2 == null) {
            return;
        }
        try {
            DataOutputStream os = new DataOutputStream(MyApplication.bluetoothSocket2.getOutputStream());
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(1));
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(2));
            PrintFormatUtils.printText(os, bean.getStoreName() + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontSizeCmd(0));
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_scan_order_title) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(0));
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_desk_no_title), bean.getTableCode() + "\n", 10));
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_trade_time_title), bean.getPayTime() + "\n", 10));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_trade_no_title) + "\n");
            PrintFormatUtils.printText(os, bean.getOrderId() + "\n");

            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
            PrintFormatUtils.printText(os, PrintFormatUtils.printThreeData(
                    UIUtils.getString(R.string.print_order_dish_name_title),
                    UIUtils.getString(R.string.print_order_dish_count_title),
                    UIUtils.getString(R.string.print_order_dish_name_title) + "\n"));
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
            if (bean.getOrderGoods() != null && bean.getOrderGoods().size() > 0) {
                for (int i = 0; i < bean.getOrderGoods().size(); i++) {
                    PrintContentBean.OrderGoodsListBean dish = bean.getOrderGoods().get(i);
                    if (!TextUtils.isEmpty(dish.getGoodsName())) {
                        PrintFormatUtils.printText(os, PrintFormatUtils.printThreeData(dish.getGoodsName(), dish.getGoodsCount() + "", DateUtil.formatMoneyByLong(dish.getGoodsPrice()) + "元\n"));
                    }
                }
            }

            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_discount_title), DateUtil.formatMoneyByLong(bean.getDiscountAmt()) + "元\n"));
            if (bean.getEatSite() == 1) {
                PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_mode_title), UIUtils.getString(R.string.print_order_mode_here) + "\n"));
            } else if (bean.getEatSite() == 2) {
                PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_mode_title), UIUtils.getString(R.string.print_order_mode_go) + "\n"));
            }
            PrintFormatUtils.printText(os, PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_order_trade_type_title), bean.getPayType() + "\n"));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(2));
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_pay_money_title));
            PrintFormatUtils.printText(os, DateUtil.formatMoneyByLong(bean.getOrderAmt()) + "元\n");
            PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_real_money_title));
            PrintFormatUtils.printText(os, DateUtil.formatMoneyByLong(bean.getRealAmt()) + "元\n");

            PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(0));
            if (!TextUtils.isEmpty(bean.getRemark())) {
                PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
                PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(0));
                PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_order_remark_title) + "\n");
                PrintFormatUtils.selectCommand(os, PrintFormatUtils.getFontBoldCmd(1));
                PrintFormatUtils.printText(os, bean.getRemark() + "\n");
            }

            if (isClient) {
                PrintFormatUtils.printText(os, UIUtils.getString(R.string.print_single_horizontal) + "\n");
                PrintFormatUtils.printText(os, "\n");
                PrintFormatUtils.selectCommand(os, PrintFormatUtils.getAlignCmd(1));
                PrintFormatUtils.printText(os, bean.getStoreName() + UIUtils.getString(R.string.print_order_welcome) + "\n");
            }
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.printText(os, "\n");
            PrintFormatUtils.printText(os, "\n");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
