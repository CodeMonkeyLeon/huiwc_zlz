package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
* 联迪A8
*
* @author Jeremy
* @version [版本号, 2019-09-20]
* @see [相关类/方法]
* @since [产品/模块版本]
*/
public abstract class PrinterSummary {

    private Context context;

    private Printer.Progress progress = new Printer.Progress() {

        @Override
        public void doPrint(Printer arg0) throws Exception {
            onStartPrint();

        }

        @Override
        public void onFinish(int code) {
            if (code == Printer.ERROR_NONE) {
                displayPrinterInfo("PRINT SUCCESS END ");
            }
            /**
             * Has some error. Here is display it, but you may want to hanle the
             * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
             * again in the right time later.
             */
            else {
                displayPrinterInfo("PRINT ERR - " + (code));
            }
        }

        @Override
        public void onCrash() {
            onDeviceServiceCrash();
        }

    };

    /**
     * Search card and show all track info
     */
    public void startPrint() {
        try {
            progress.start();
            // DeviceService.logout();
        } catch (RequestException e) {
            e.printStackTrace();
            onDeviceServiceCrash();
        }
    }

    protected abstract void displayPrinterInfo(String info);

    protected abstract void onStartPrint();

    protected abstract void onDeviceServiceCrash();

    public PrinterSummary(Context context, final ReportBean.DataEntity info) {
        this.context = context;

        progress.addStep(new Printer.Step() {

            @Override
            public void doPrint(Printer printer) throws Exception {
                printer.setAutoTrunc(true);

                printer.setMode(Printer.MODE_VIRTUAL);
                Format format = new Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);

                // 标题
                if (info.getType() == 1) {
                    printer.printText("            " + UIUtils.getString(R.string.print_pay_report_title) + "\n");
                } else if (info.getType() == 2) {
                    printer.printText("          " + UIUtils.getString(R.string.print_consume_report_title) + "\n");
                }else if (info.getType() == 3){
                    printer.printText("          " + UIUtils.getString(R.string.print_fk_consume_report_title) + "\n");
                }
                printer.printText("\r\n");

                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x1);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x1);
                printer.setFormat(format);

                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(MyApplication.getMerchantName() + "\n");
                    } else {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "："
                                + MyApplication.getMerchantName() + "\n");
                    }
                }
                // 商户号
                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
                }
                // 门店
                if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                    printer.printText(UIUtils.getString(R.string.store_name_title) + ": 全部门店\n");
                } else {
                    if (info.getStoreName().length() > 16) {
                        String str = info.getStoreName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\n");
                    } else if (info.getStoreName().length() > 11) {
                        printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        printer.printText(info.getStoreName() + "\n");
                    } else {
                        printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n");
                    }
                }

                // 收银员
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                    printer.printText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n");
                } else {
                    if (info.getCashierName().length() > 16) {
                        String str = info.getCashierName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(cashierTitle + ": " + one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\n");
                    } else if (info.getCashierName().length() > 12) {
                        printer.printText(cashierTitle + ": " + "\n");
                        printer.printText(info.getCashierName() + "\n");
                    } else {
                        String str = info.getCashierName();
                        printer.printText(cashierTitle + ": " + str + "\n");
                    }
                }
                if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                    if (info.getCashierPointName().length() > 16) {
                        StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                        String one = sbf.substring(0, 16);
                        String two = sbf.substring(16, info.getCashierPointName().length());
                        printer.printText("收银点: " + "\n");
                        printer.printText(one + "\n");
                        printer.printText(two + "\n");
                    } else if (info.getCashierPointName().length() > 12) {
                        printer.printText("收银点: " + "\n");
                        printer.printText(info.getCashierPointName() + "\n");
                    } else {
                        printer.printText("收银点: " + info.getCashierPointName() + "\n");
                    }
                }
                //开始时间
                printer.printText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\r\n");
//                printer.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\r\n");
                // 结束时间
                printer.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\r\n");
                printer.printText("\r\n");

                // 结算总计
                printer.printText(UIUtils.getString(R.string.print_report_total_title) + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                if (info.getType() == 1) {
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：",DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔\n"));
                } else if (info.getType() == 2) {
                   /* printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()-info.getRefundFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔\n"));*/

                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔\n"));

                    /*printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元\n"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔\n"));*/
                }else if (info.getType() == 3){
                    //饭卡消费
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+""));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                    printer.printText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));
                }

                if (info.getType() == 1) {
                    List<ReportBean.DataEntity.ListEntity> list = info.getList();
                    if (list != null && list.size() > 0) {
                        printer.printText("\n");
                        printer.printText(UIUtils.getString(R.string.print_trade_type_list_title) + ": \n");
                        printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                        for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                            printer.printText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：",DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元\n"));
                            printer.printText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：",itemInfo.getSuccessCount() + "笔\n"));
                        }
                    }
                }
                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                printer.printText(UIUtils.getString(R.string.print_time_title) + ": " + DateUtil.formatTime(System.currentTimeMillis()) + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n");
                printer.printText("\n");
                printer.printText("\n");
                printer.printText("\n");
                printer.printText("\n");
                printer.printText("\n");
            }
        });
    }

    public PrinterSummary(Context context, final PledgeReportBean.DataBean info) {
        this.context = context;

        progress.addStep(new Printer.Step() {

            @Override
            public void doPrint(Printer printer) throws Exception {
                printer.setAutoTrunc(true);

                printer.setMode(Printer.MODE_VIRTUAL);
                Format format = new Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);

                printer.printText("           " + UIUtils.getString(R.string.print_title_pledge_report) + "\n");
                printer.printText("\r\n");

                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x1);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x1);
                printer.setFormat(format);

                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\r\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        printer.printText(MyApplication.getMerchantName() + "\n");
                    } else {
                        printer.printText(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }
                // 商户号

                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    printer.printText(ToastHelper.toStr(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
                }

                // 门店
                if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                    printer.printText(UIUtils.getString(R.string.store_name_title) + ": 全部门店\n");
                } else {
                    if (info.getStoreName().length() > 16) {
                        String str = info.getStoreName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        printer.printText(one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\n");
                    } else if (info.getStoreName().length() > 11) {
                        printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        printer.printText(info.getStoreName() + "\n");
                    } else {
                        printer.printText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n");
                    }
                }

                // 收银员
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                    printer.printText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n");
                } else {
                    if (info.getCashierName().length() > 16) {
                        String str = info.getCashierName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        printer.printText(cashierTitle + ": " + one + "\n");
                        String two = sbf.substring(16, str.length());
                        printer.printText(two + "\n");
                    } else if (info.getCashierName().length() > 12) {
                        printer.printText(cashierTitle + ": " + "\n");
                        printer.printText(info.getCashierName() + "\n");
                    } else {
                        String str = info.getCashierName();
                        printer.printText(cashierTitle + ": " + str + "\n");
                    }
                }

                //开始时间
                printer.printText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\r\n");
                // 结束时间
                printer.printText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\r\n");

                printer.printText("\r\n");
                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                printer.printText(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元" + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔" + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元" + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔" + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元" + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_pledge_refund_count) + "：" + info.getCntFreeMoney() + "笔" + "\r\n");
//                printer.printText(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元" + "\r\n");

                printer.printText(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                printer.printText(UIUtils.getString(R.string.print_time_title) + ": " + DateUtil.formatTime(System.currentTimeMillis()) + "\r\n");
                printer.printText(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n");
                printer.printText("\n");
                printer.printText("\n");
                printer.printText("\n");
                printer.printText("\n");
                printer.printText("\n");

            }
        });
    }
}
