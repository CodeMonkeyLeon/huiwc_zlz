package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.DisplayUtil;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.utils.MoneyUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.unionpay.cloudpos.DeviceException;
import com.unionpay.cloudpos.POSTerminal;
import com.unionpay.cloudpos.printer.Format;
import com.unionpay.cloudpos.printer.PrinterDevice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名: WankePay
 * @包名: com.hsty.wankepay.utils
 * @创建者: Jeremy
 * @创建时间: 2019/1/12 16:49
 * @描述: ${TODO}
 */
public class WizarPrinterUtil {
    private PrinterDevice device = null;
    private Context mContext = MyApplication.getContext();
    private static WizarPrinterUtil singleton;

    public static WizarPrinterUtil with() {
        if (singleton == null) {
            synchronized (WizarPrinterUtil.class) {
                if (singleton == null) {
                    singleton = new WizarPrinterUtil();
                }
            }
        }
        return singleton;
    }

    public void open() {
        if (device == null) {
            device = (PrinterDevice) POSTerminal.getInstance(mContext)
                    .getDevice("cloudpos.device.printer");
        }
        try {
            device.open();
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            device.close();
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }

    public void printText(TradeDetailBean order) {
        open();
        try {
            int status = queryStatus();
            if (status == device.STATUS_PAPER_EXIST) {
                Format format = new Format();
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_LARGE);

                if (order.getTradeType() == 1) {
                    device.printText(format, UIUtils.getString(R.string.print_recharge_title));
                } else if (order.getTradeType() == 2) {
                    device.printText(format, UIUtils.getString(R.string.print_verification_title));
                } else {
                    if (!order.isPay()) {
                        device.printText(format, UIUtils.getString(R.string.print_refund_title));
                    } else {
                        device.printText(format, UIUtils.getString(R.string.print_pay_title));
                    }
                }
                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_MEDIUM);
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                StringBuilder builder = new StringBuilder();
                // 空格
                builder.append("\n");
                builder.append(order.getPartner() + "              " + "请妥善保存" + "\n");
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        builder.append(ToastHelper.toStr(R.string.shop_name) + ": " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(16, str.length());
                        builder.append(two + "\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + ": " + "\n");
                        builder.append(MyApplication.getMerchantName() + "\n");
                    } else {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }

                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    builder.append(UIUtils.getString(R.string.print_title_merchant_id) + ": " + MyApplication.getMechantId() + "\n");
                }

                String storeNameTitle;
                if (order.isPay()) {
                    storeNameTitle = UIUtils.getString(R.string.trade_store_name);
                } else {
                    storeNameTitle = UIUtils.getString(R.string.refund_store_name);
                }
                if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                    if (order.getStoreMerchantIdCnt().length() > 16) {
                        String str = order.getStoreMerchantIdCnt();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        builder.append(storeNameTitle + ": " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(16, str.length());
                        builder.append(two + "\n");
                    } else if (order.getStoreMerchantIdCnt().length() > 11) {
                        builder.append(storeNameTitle + ": " + "\n");
                        builder.append(order.getStoreMerchantIdCnt() + "\n");
                    } else {
                        builder.append(storeNameTitle + ": " + order.getStoreMerchantIdCnt() + "\n");
                    }
                }

                if (!StringUtils.isEmptyOrNull(order.getThirdMerchantId())) {
                    if (order.getThirdMerchantId().length() > 32) {
                        String str = order.getThirdMerchantId();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 32);
                        builder.append("收单机构商户编号: " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(32, str.length());
                        builder.append(two + "\n");
                    } else if (order.getThirdMerchantId().length() > 14) {
                        builder.append("收单机构商户编号: " + "\n");
                        builder.append(order.getThirdMerchantId() + "\n");
                    } else {
                        builder.append("收单机构商户编号: " + order.getThirdMerchantId() + "\n");
                    }
                }
                if (!StringUtils.isEmptyOrNull(order.getTradeCode())) {
                    if (order.getTradeCode().length() > 32) {
                        String str = order.getTradeCode();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 32);
                        builder.append("终端编号: " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(32, str.length());
                        builder.append(two + "\n");
                    } else if (order.getTradeCode().length() > 22) {
                        builder.append("终端编号: " + "\n");
                        builder.append(order.getTradeCode() + "\n");
                    } else {
                        builder.append("终端编号: " + order.getTradeCode() + "\n");
                    }
                }
                if (!StringUtils.isEmptyOrNull(order.getThirdOrderNo())) {
                    if (order.getThirdOrderNo().length() > 32) {
                        String str = order.getThirdOrderNo();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 32);
                        builder.append("收单机构商户订单号: " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(32, str.length());
                        builder.append(two + "\n");
                    } else if (order.getThirdOrderNo().length() > 12) {
                        builder.append("收单机构商户订单号: " + "\n");
                        builder.append(order.getThirdOrderNo() + "\n");
                    } else {
                        builder.append("收单机构商户订单号: " + order.getThirdOrderNo() + "\n");
                    }
                }
                if (!order.isPay()) {
                    builder.append(UIUtils.getString(R.string.print_refund_no_title) + ": " + "\n");
                    builder.append(order.getRefundNo() + "\n");
                    builder.append(UIUtils.getString(R.string.print_refund_time_title) + ": " + "\n");
                    builder.append(order.getRefundTime() + "\n");

                    String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                    if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                        if (order.getRefundUser().length() > 16) {
                            StringBuffer sbf = new StringBuffer(order.getRefundUser());
                            String one = sbf.substring(0, 16);
                            String two = sbf.substring(16, order.getRefundUser().length());
                            builder.append(refundUser + "\n");
                            builder.append(one + "\n");
                            builder.append(two + "\n");
                        } else if (order.getRefundUser().length() > 12) {
                            builder.append(refundUser + ": " + "\n");
                            builder.append(order.getRefundUser() + "\n");
                        } else {
                            builder.append(refundUser + ": " + order.getRefundUser() + "\n");
                        }
                    } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                        if (order.getRefundUserRealName().length() > 16) {
                            StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                            String one = sbf.substring(0, 16);
                            String two = sbf.substring(16, order.getRefundUserRealName().length());
                            builder.append(refundUser + ": " + "\n");
                            builder.append(one + "\n");
                            builder.append(two + "\n");
                        } else if (order.getRefundUserRealName().length() > 12) {
                            builder.append(refundUser + ": " + "\n");
                            builder.append(order.getRefundUserRealName() + "\n");
                        } else {
                            builder.append(refundUser + ": " + order.getRefundUserRealName() + "\n");
                        }
                    }
                    builder.append(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()) + "\n");
                    if (order.getRefundMoney() > 0) {
                        builder.append(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元" + "\n");
                    }
                    builder.append(UIUtils.getString(R.string.print_refund_instruction) + "\n");
                } else {
                    if (!TextUtils.isEmpty(order.getTransactionId())) {
                        builder.append(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": " + "\n");
                        builder.append(order.getTransactionId() + "\n");
                    }
                    if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                        builder.append(UIUtils.getString(R.string.tv_print_order_no) + ": " + "\n");
                        builder.append(order.getOrderNo() + "\n");
                    }
                    builder.append(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()) + "\n");
                    builder.append(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n");
                    if (order.getCouponInfoData() != null && !order.getCouponInfoData().isEmpty()) {
                        for (int i = 0; i < order.getCouponInfoData().size(); i++) {
                            CouponInfoData coupon = order.getCouponInfoData().get(i);
                            StringBuilder sb = new StringBuilder("优惠券信息：");
                            if (coupon.getCouponType() == 1) {//代金券
                                if (coupon.getDeductible() > 0) {//有门槛的券
                                    sb.append("满");
                                    sb.append(MoneyUtils.changeF2Y(coupon.getDeductible()));
                                    sb.append("减");
                                }
                                sb.append(MoneyUtils.changeF2Y(coupon.getReduceMoney()));
                                sb.append("元-代金券");
                            } else if (coupon.getCouponType() == 2) {//折扣券
                                sb.append(MoneyUtils.changeF2Y(coupon.getDiscountAmount()));
                                sb.append("折-折扣券");
                            } else if (coupon.getCouponType() == 3) {//兑换券
                                sb.append("兑换券");
                            }
                            builder.append(sb.toString() + "\n");
                            builder.append("优惠券编码：" + coupon.getCouponCode() + "\n");
                        }
                    }
                    if (order.getCouponGroupInfoData() != null && !order.getCouponGroupInfoData().isEmpty()) {
                        for (int i = 0; i < order.getCouponGroupInfoData().size(); i++) {
                            CouponInfoData coupon = order.getCouponGroupInfoData().get(i);
                            StringBuilder sb = new StringBuilder("优惠券信息：");
                            if (coupon.getCouponType() == 6) {
                                sb.append("团购券");
                                sb.append(MoneyUtils.changeF2Y(coupon.getCouponMoney()));
                                sb.append("元");
                                builder.append(sb.toString() + "\n");
                                for (int i1 = 0; i1 < coupon.getCouponCodeList().size(); i1++) {
                                    if (i1 == 0) {
                                        builder.append("优惠券编码：" + coupon.getCouponCodeList().get(i1) + "\n");
                                    } else {
                                        builder.append("            " + coupon.getCouponCodeList().get(i1) + "\n");
                                    }
                                }
                            }
                        }
                    }
                    try {
                        builder.append(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime() + "\n");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                    if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                        if (order.getCashierName().length() > 16) {
                            StringBuffer sbf = new StringBuffer(order.getCashierName());
                            String oneUser = sbf.substring(0, 16);
                            String twoUser = sbf.substring(16, order.getCashierName().length());
                            builder.append(cashierTitle + ": " + "\n");
                            builder.append(oneUser + "\n");
                            builder.append(twoUser + "\n");
                        } else if (order.getCashierName().length() > 12) {
                            builder.append(cashierTitle + ": " + "\n");
                            builder.append(order.getCashierName() + "\n");
                        } else {
                            builder.append(cashierTitle + ": " + order.getCashierName() + "\n");
                        }
                    } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                        if (order.getOpUserRealName().length() > 16) {
                            StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                            String oneUser = sbf.substring(0, 16);
                            String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                            builder.append(cashierTitle + ": " + "\n");
                            builder.append(oneUser + "\n");
                            builder.append(twoUser + "\n");
                        }
                        if (order.getOpUserRealName().length() > 12) {
                            builder.append(cashierTitle + ": " + "\n");
                            builder.append(order.getOpUserRealName() + "\n");
                        } else {
                            builder.append(cashierTitle + ": " + order.getOpUserRealName() + "\n");
                        }
                    }
                    builder.append(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元" + "\n");
                    builder.append(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元" + "\n");
                    builder.append(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元" + "\n");
                }

                //备注
                if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                    builder.append(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach() + "\n");
                }
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                builder.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
                //顾客签名
                if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                    builder.append(UIUtils.getString(R.string.print_client_sign_title) + ": " + "\n");
                }
                builder.append("\n");
                device.printText(format, builder.toString());
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                    String printCode;
                    if (ConfigUtil.printCodeBillEnable()) {
                        printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                    } else {
                        printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                    }
                    device.printText(format, ConfigUtil.getPrintCodeTitle());
                    Bitmap bitmap = MaxCardManager.getInstance().create2DCode(printCode,
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                    format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                    device.printBitmap(format, bitmap);
                }
                if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
                    if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                            && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                        StringBuilder scanBuilder = new StringBuilder();
                        scanBuilder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                        String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                        String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                        if (!TextUtils.isEmpty(title)) {
                            scanBuilder.append(title + "\n");
                        }
                        device.printText(format, scanBuilder.toString());
                        if (!TextUtils.isEmpty(activeUrl)) {
                            Bitmap scanBitmap = MaxCardManager.getInstance().create2DCode(activeUrl,
                                    DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                    DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                            format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                            device.printBitmap(format, scanBitmap);
                        }
                    }
                }
                device.printText(format, "\n\n\n\n");
            } else if (status == device.STATUS_OUT_OF_PAPER) {
                if (ToastHelper.toStr(R.string.tv_pay_mch_stub).equals(order.getPartner())) {
                    ToastHelper.showInfo("打印机缺纸！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void pledgePrint(PledgePayBean.DataBean order) {
        open();
        try {
            int status = queryStatus();
            if (status == device.STATUS_PAPER_EXIST) {
                Format format = new Format();
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_LARGE);
                if (order.isToPay()) {
                    device.printText(format, UIUtils.getString(R.string.print_title_pledge_pay) + "\n");
                } else {
                    device.printText(format, UIUtils.getString(R.string.print_title_pledge) + "\n");
                }
                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_MEDIUM);
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                StringBuilder builder = new StringBuilder();
                // 空格
                builder.append("\n");
                builder.append(order.getPartner() + "              " + "请妥善保存" + "\n");
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        builder.append(ToastHelper.toStr(R.string.shop_name) + ": " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(16, str.length());
                        builder.append(two + "\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + ": " + "\n");
                        builder.append(MyApplication.getMerchantName() + "\n");
                    } else {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }

                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    builder.append(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMechantId() + "\n");
                }

                if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                    if (order.getStoreMerchantIdCnt().length() > 16) {
                        String str = order.getStoreMerchantIdCnt();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        builder.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(16, str.length());
                        builder.append(two + "\n");
                    } else if (order.getStoreMerchantIdCnt().length() > 11) {
                        builder.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + "\n");
                        builder.append(order.getStoreMerchantIdCnt() + "\n");
                    } else {
                        builder.append(ToastHelper.toStr(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt() + "\n");
                    }
                }

                try {
                    builder.append(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime() + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                    builder.append(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：" + "\n");
                    builder.append(order.getOutTransactionId() + "\n");
                }
                if (!TextUtils.isEmpty(order.getAuthNo())) {
                    builder.append(UIUtils.getString(R.string.tv_pledge_order_no) + "：" + "\n");
                    builder.append(order.getAuthNo() + "\n");
                }
                builder.append(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()) + "\n");

                if (!order.isToPay()) {
                    if (!TextUtils.isEmpty(order.getMoney())) {
                        builder.append(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元" + "\n");
                    } else {
                        builder.append(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元" + "\n");
                    }
                }
                if (order.isToPay()) {
                    if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                        builder.append(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元" + "\n");
                    } else {
                        builder.append(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元" + "\n");
                    }
                    if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                        builder.append(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元" + "\n");
                    } else {
                        builder.append(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元" + "\n");
                    }
                }

                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String oneUser = sbf.substring(0, 16);
                        String twoUser = sbf.substring(16, order.getCashierName().length());
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(oneUser + "\n");
                        builder.append(twoUser + "\n");
                    }
                    if (order.getCashierName().length() > 12) {
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(order.getCashierName() + "\n");
                    } else {
                        builder.append(cashierTitle + ": " + order.getCashierName() + "\n");
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    String opUserTitle;
                    if (order.isToPay()) {
                        opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                    } else {
                        opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                    }
                    if (order.getOpUserRealName().length() > 16) {
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneUser = sbf.substring(0, 16);
                        String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                        builder.append(opUserTitle + "\n");
                        builder.append(oneUser + "\n");
                        builder.append(twoUser + "\n");
                    }
                    if (order.getOpUserRealName().length() > 12) {
                        builder.append(opUserTitle + "\n");
                        builder.append(order.getOpUserRealName() + "\n");
                    } else {
                        builder.append(opUserTitle + order.getOpUserRealName() + "\n");
                    }
                }
                //备注
                if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                    builder.append(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach() + "\n");
                }
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                builder.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
                //顾客签名
                if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                    builder.append(UIUtils.getString(R.string.print_client_sign_title) + ": " + "\n");
                }
                builder.append("\n");
                device.printText(format, builder.toString());
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);

                if (!TextUtils.isEmpty(order.getAuthNo())) {
                    if (!order.isToPay()) {
                        device.printText(format, UIUtils.getString(R.string.print_pledge_scan_title));
                    }
                    String printCode;
                    if (ConfigUtil.printCodeBillEnable()) {
                        printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
                    } else {
                        printCode = order.getAuthNo();
                    }
                    Bitmap bitmap = MaxCardManager.getInstance().create2DCode(printCode,
                            DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                            DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                    format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                    device.printBitmap(format, bitmap);
                }
                if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
                    if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                            && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                        device.printText(format, UIUtils.getString(R.string.print_single_horizontal));
                        String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                        String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                        if (!TextUtils.isEmpty(title)) {
                            device.printText(format, title);
                        }
                        if (!TextUtils.isEmpty(activeUrl)) {
                            Bitmap scanBitmap = MaxCardManager.getInstance().create2DCode(activeUrl,
                                    DisplayUtil.dip2Px(MyApplication.getContext(), 100),
                                    DisplayUtil.dip2Px(MyApplication.getContext(), 100));
                            format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                            device.printBitmap(format, scanBitmap);
                        }
                    }
                }
                if (!order.isToPay()) {
                    device.printText(format, UIUtils.getString(R.string.print_single_horizontal));
                    device.printText(format, UIUtils.getString(R.string.print_pledge_notice));
                }
                device.printText(format, "\n\n\n\n");
            } else if (status == device.STATUS_OUT_OF_PAPER) {
                if (ToastHelper.toStr(R.string.tv_pay_mch_stub).equals(order.getPartner())) {
                    ToastHelper.showInfo("打印机缺纸！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public static byte[] setAlignMode(int n) {
        return new byte[]{(byte) 0x1B, (byte) 0x61, (byte) n};
    }

    public void printTotal(ReportBean.DataEntity info) {
        open();
        try {
            int status = queryStatus();
            if (status == device.STATUS_PAPER_EXIST) {
                Format format = new Format();
                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_LARGE);
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);

                if (info.getType() == 1) {
                    device.printText(format, UIUtils.getString(R.string.print_pay_report_title));
                } else if (info.getType() == 2) {
                    device.printText(format, UIUtils.getString(R.string.print_consume_report_title));
                }else if (info.getType() == 3){
                    device.printText(format, UIUtils.getString(R.string.print_fk_consume_report_title));
                }

                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_MEDIUM);
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                StringBuilder builder = new StringBuilder();
                builder.append("\n");
                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(16, str.length());
                        builder.append(two + "\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        builder.append(MyApplication.getMerchantName() + "\n");
                    } else {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }
                // 商户号
                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    builder.append(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
                }

                if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                    builder.append(UIUtils.getString(R.string.store_name_title) + ": " + "全部门店" + "\n");
                } else {
                    if (info.getStoreName().length() > 16) {
                        String str = info.getStoreName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        String two = sbf.substring(16, str.length());
                        builder.append(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        builder.append(one + "\n");
                        builder.append(two + "\n");
                    } else if (info.getStoreName().length() > 11) {
                        builder.append(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        builder.append(info.getStoreName() + "\n");
                    } else {
                        builder.append(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n");
                    }
                }

                // 收银员
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                    builder.append(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user) + "\n");
                } else {
                    String str = info.getCashierName();
                    if (info.getCashierName().length() > 16) {
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        String two = sbf.substring(16, str.length());
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(one + "\n");
                        builder.append(two + "\n");
                    } else if (info.getStoreName().length() > 12) {
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(info.getCashierName() + "\n");
                    } else {
                        builder.append(cashierTitle + ": " + str + "\n");
                    }
                }

                //开始时间
                builder.append("开始时间: " + info.getStartTime() + "\n");

                // 结束时间
                builder.append(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n");
                builder.append("\n");
                // 结算总计
                builder.append(UIUtils.getString(R.string.print_report_total_title) + "\n");
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");

                if (info.getType() == 1) {
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n");
                } else if (info.getType() == 2) {
                /*builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n");*/
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔") + "\n");
                /*builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次") + "\n");
                builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元") + "\n");
                builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔") + "\n");*/
                }else if (info.getType() == 3){
                    //饭卡消费
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+"") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元") + "\n");

                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getSuccessCount() + "笔") + "\n");
                    builder.append(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n");
                }
                if (info.getType() == 1) {
                    List<ReportBean.DataEntity.ListEntity> list = info.getList();
                    if (list != null && list.size() > 0) {
                        builder.append("\n");
                        builder.append(UIUtils.getString(R.string.print_trade_type_list_title) + ": " + "\n");
                        builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                        for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                            builder.append(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元") + "\n");
                            builder.append(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔") + "\n");
                        }
                    }
                }
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                builder.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
                builder.append(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n");
                device.printText(format, builder.toString());
                device.printText(format, "\n\n\n\n\n");
            } else if (status == device.STATUS_OUT_OF_PAPER) {
                ToastHelper.showError("打印机缺纸！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void printPledgeTotal(PledgeReportBean.DataBean info) {
        open();
        try {
            int status = queryStatus();
            if (status == device.STATUS_PAPER_EXIST) {
                Format format = new Format();
                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_LARGE);
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_CENTER);
                device.printText(format, UIUtils.getString(R.string.print_title_pledge_report));

                format.setParameter(Format.FORMAT_FONT_SIZE, Format.FORMAT_FONT_SIZE_MEDIUM);
                format.setParameter(Format.FORMAT_ALIGN, Format.FORMAT_ALIGN_LEFT);
                StringBuilder builder = new StringBuilder();
                builder.append("\n");
                if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                    if (MyApplication.getMerchantName().length() > 16) {
                        String str = MyApplication.getMerchantName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        builder.append(one + "\n");
                        String two = sbf.substring(16, str.length());
                        builder.append(two + "\n");
                    } else if (MyApplication.getMerchantName().length() > 11) {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n");
                        builder.append(MyApplication.getMerchantName() + "\n");
                    } else {
                        builder.append(ToastHelper.toStr(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n");
                    }
                }
                // 商户号
                if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                    builder.append(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n");
                }

                if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                    builder.append(UIUtils.getString(R.string.store_name_title) + ": " + "全部门店" + "\n");
                } else {
                    if (info.getStoreName().length() > 16) {
                        String str = info.getStoreName();
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        String two = sbf.substring(16, str.length());
                        builder.append(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        builder.append(one + "\n");
                        builder.append(two + "\n");
                    } else if (info.getStoreName().length() > 11) {
                        builder.append(UIUtils.getString(R.string.store_name_title) + ": " + "\n");
                        builder.append(info.getStoreName() + "\n");
                    } else {
                        builder.append(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n");
                    }
                }

                // 收银员
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                    builder.append(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+"\n");
                } else {
                    String str = info.getCashierName();
                    if (info.getCashierName().length() > 16) {
                        StringBuffer sbf = new StringBuffer(str);
                        String one = sbf.substring(0, 16);
                        String two = sbf.substring(16, str.length());
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(one + "\n");
                        builder.append(two + "\n");
                    } else if (info.getStoreName().length() > 12) {
                        builder.append(cashierTitle + ": " + "\n");
                        builder.append(info.getCashierName() + "\n");
                    } else {
                        builder.append(cashierTitle + ": " + str + "\n");
                    }
                }

                //开始时间
                builder.append("开始时间: " + info.getStartTime() + "\n");

                // 结束时间
                builder.append(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n");
                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                builder.append(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元" + "\n");
                builder.append(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔" + "\n");
                builder.append(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元" + "\n");
                builder.append(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔" + "\n");
                builder.append(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元" + "\n");
                builder.append(UIUtils.getString(R.string.print_pledge_refund_count) + "：" + info.getCntFreeMoney() + "笔" + "\n");
//            builder.append(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元" + "\n");

                builder.append(UIUtils.getString(R.string.print_single_horizontal) + "\n");
                builder.append(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
                builder.append(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n");
                device.printText(format, builder.toString());
                device.printText(format, "\n\n\n\n\n");
            } else if (status == device.STATUS_OUT_OF_PAPER) {
                ToastHelper.showError("打印机缺纸！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void sendESCCommand() {
        byte[] command = new byte[]{
                (byte) 0x12, (byte) 0x54
        };
        try {
            device.sendESCCommand(command);
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }

    public int queryStatus() {
        try {
            int status = device.queryStatus();
            return status;
        } catch (DeviceException e) {
            e.printStackTrace();
            return -123456;
        }
    }

    public void cutPaper() {
        try {
            device.cutPaper();
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }

    public void printBarcode() {
        try {
            Format format = new Format();
            format.setParameter("HRI-location", "DOWN");
            device.printBarcode(format, PrinterDevice.BARCODE_CODE128, "01234567896");
            device.printText("\n\n\n");
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }

    public void cancelRequest() {
        try {
            device.cancelRequest();
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }
}
