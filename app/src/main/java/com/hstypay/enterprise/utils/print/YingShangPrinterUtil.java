package com.hstypay.enterprise.utils.print;


import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Base64;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.MaxCardManager;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.bean.vanke.CouponInfoData;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.MoneyUtils;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.ums.upos.sdk.printer.OnPrintResultListener;
import com.ums.upos.sdk.printer.PrinterManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class YingShangPrinterUtil {

    private static YingShangPrinterUtil singleton;
    private static String title;
    private String title1;
    private static String storeName;
    private static String endTime;

    public static YingShangPrinterUtil with() {
        if (singleton == null) {
            synchronized (YingShangPrinterUtil.class) {
                if (singleton == null) {
                    singleton = new YingShangPrinterUtil();
                }
            }
        }
        return singleton;
    }

    public static void printTotal(ReportBean.DataEntity info) {

        try {
            PrinterManager printer = new PrinterManager();

            String text = totalText(info);
            try {
                printer.initPrinter();
                printer.setPrnScript(text, "384");
                printer.startPrint(new OnPrintResultListener() {
                    @Override
                    public void onPrintResult(int resCode) {//arg0具体可参考常量类ServiceResult

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    //订单详情打印
    public static void printOrder(TradeDetailBean order) {
        try {
            PrinterManager printer = new PrinterManager();
            PrintScriptUtil print = new PrintScriptUtil();
//            if (printer.getStatus())
            printer.initPrinter();
            print.setNextFormat(ScriptConstant.LARGE, ScriptConstant.LARGE);
            if (order.getTradeType() == 1) {
                print.addText(ScriptConstant.CENTER, UIUtils.getString(R.string.print_recharge_title));
            } else if (order.getTradeType() == 2) {
                print.addText(ScriptConstant.CENTER, UIUtils.getString(R.string.print_verification_title));
            } else {
                if (order.getCouponInfoData() != null && !order.getCouponInfoData().isEmpty()) {
                    print.addText(ScriptConstant.CENTER, "优惠券核销凭证");
                } else {
                    if (!order.isPay()) {
                        print.addText(ScriptConstant.CENTER, UIUtils.getString(R.string.print_refund_title));
                    } else {
                        print.addText(ScriptConstant.CENTER, UIUtils.getString(R.string.print_pay_title));
                    }
                }

            }
            print.setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL);
            // 空格
            print.addFeedline("1");
            print.addText(ScriptConstant.LEFT, order.getPartner() + "              " + "请妥善保存");
            print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_double_horizontal));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.shop_name) + ": ");
                    print.addText(ScriptConstant.LEFT, one);
                    String two = sbf.substring(16, str.length());
                    print.addText(ScriptConstant.LEFT, two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.shop_name) + ": ");
                    print.addText(ScriptConstant.LEFT, MyApplication.getMerchantName());
                } else {
                    print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            String storeNameTitle;
            if (order.isPay()) {
                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
            } else {
                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.addText(ScriptConstant.LEFT, storeNameTitle + ": ");
                    print.addText(ScriptConstant.LEFT, one);
                    String two = sbf.substring(16, str.length());
                    print.addText(ScriptConstant.LEFT, two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    print.addText(ScriptConstant.LEFT, storeNameTitle + ": ");
                    print.addText(ScriptConstant.LEFT, order.getStoreMerchantIdCnt());
                } else {
                    print.addText(ScriptConstant.LEFT, storeNameTitle + ": " + order.getStoreMerchantIdCnt());
                }
            }

            if (!StringUtils.isEmptyOrNull(order.getThirdMerchantId())) {
                if (order.getThirdMerchantId().length() > 32) {
                    String str = order.getThirdMerchantId();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 32);
                    print.addText(ScriptConstant.LEFT, "收单机构商户编号: " + "\n");
                    print.addText(ScriptConstant.LEFT, one + "\n");
                    String two = sbf.substring(32, str.length());
                    print.addText(ScriptConstant.LEFT, two + "\n");
                } else if (order.getThirdMerchantId().length() > 14) {
                    print.addText(ScriptConstant.LEFT, "收单机构商户编号: " + "\n");
                    print.addText(ScriptConstant.LEFT, order.getThirdMerchantId() + "\n");
                } else {
                    print.addText(ScriptConstant.LEFT, "收单机构商户编号: " + order.getThirdMerchantId() + "\n");
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getTradeCode())) {
                print.addText(ScriptConstant.LEFT, "终端编号: " + order.getTradeCode());
            }
            if (!StringUtils.isEmptyOrNull(order.getThirdOrderNo())) {
                if (order.getThirdOrderNo().length() > 32) {
                    String str = order.getThirdOrderNo();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 32);
                    print.addText(ScriptConstant.LEFT, "收单机构商户订单号: " + "\n");
                    print.addText(ScriptConstant.LEFT, one + "\n");
                    String two = sbf.substring(32, str.length());
                    print.addText(ScriptConstant.LEFT, two + "\n");
                } else if (order.getThirdOrderNo().length() > 12) {
                    print.addText(ScriptConstant.LEFT, "收单机构商户订单号: " + "\n");
                    print.addText(ScriptConstant.LEFT, order.getThirdOrderNo() + "\n");
                } else {
                    print.addText(ScriptConstant.LEFT, "收单机构商户订单号: "  + order.getThirdOrderNo() + "\n");
                }
            }
            if (!order.isPay()) {
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_no_title) + ": ");
                print.addText(ScriptConstant.LEFT, order.getRefundNo());
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_time_title) + ": ");
                print.addText(ScriptConstant.LEFT, order.getRefundTime());

                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        print.addText(ScriptConstant.LEFT, refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        print.addText(ScriptConstant.LEFT, user);
                        String printUser = sbf.substring(16, order.getRefundUser().length());
                        print.addText(ScriptConstant.LEFT, printUser);
                    } else if (order.getRefundUser().length() > 12) {
                        print.addText(ScriptConstant.LEFT, refundUser + ": ");
                        print.addText(ScriptConstant.LEFT, order.getRefundUser());
                    } else {
                        print.addText(ScriptConstant.LEFT, refundUser + ": " + order.getRefundUser());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        print.addText(ScriptConstant.LEFT, refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        print.addText(ScriptConstant.LEFT, user);
                        String printUser = sbf.substring(16, order.getRefundUserRealName().length());
                        print.addText(ScriptConstant.LEFT, printUser);
                    } else if (order.getRefundUserRealName().length() > 12) {
                        print.addText(ScriptConstant.LEFT, refundUser + ": ");
                        print.addText(ScriptConstant.LEFT, order.getRefundUserRealName());
                    } else {
                        print.addText(ScriptConstant.LEFT, refundUser + ": " + order.getRefundUserRealName());
                    }
                }
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()));
                if (order.getRefundMoney() > 0) {
                    print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元");
                }
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_instruction));
            } else {
                if (!TextUtils.isEmpty(order.getTransactionId())) {
                    print.addText(ScriptConstant.LEFT, OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": ");
                    print.addText(ScriptConstant.LEFT, order.getTransactionId());
                }
                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.tv_print_order_no) + ": ");
                    print.addText(ScriptConstant.LEFT, order.getOrderNo());
                }
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()));
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.tradeTypeFromString(order.getApiProvider(), order.getApiCode()));

                if (order.getCouponInfoData() != null && !order.getCouponInfoData().isEmpty()) {
                    for (int i = 0; i < order.getCouponInfoData().size(); i++) {
                        CouponInfoData coupon = order.getCouponInfoData().get(i);
                        StringBuilder sb = new StringBuilder("优惠券信息：");
                        if (coupon.getCouponType() == 1) {
                            if (coupon.getDeductible() > 0) {//有门槛的券
                                sb.append("满");
                                sb.append(MoneyUtils.changeF2Y(coupon.getDeductible()));
                                sb.append("减");
                            }
                            sb.append(MoneyUtils.changeF2Y(coupon.getReduceMoney()));
                            sb.append("元-代金券");
                        } else if (coupon.getCouponType() == 2) {
                            sb.append(MoneyUtils.changeF2Y(coupon.getDiscountAmount()));
                            sb.append("折-折扣券");
                        } else if (coupon.getCouponType() == 3) {//兑换券
                            sb.append("兑换券");
                        }
                        print.addText(ScriptConstant.LEFT, sb.toString());
                        print.addText(ScriptConstant.LEFT, "优惠券编码：" + coupon.getCouponCode());
                    }
                }
                if (order.getCouponGroupInfoData() != null && !order.getCouponGroupInfoData().isEmpty()) {
                    for (int i = 0; i < order.getCouponGroupInfoData().size(); i++) {
                        CouponInfoData coupon = order.getCouponGroupInfoData().get(i);
                        StringBuilder sb = new StringBuilder("优惠券信息：");
                        if (coupon.getCouponType() == 6) {
                            sb.append("团购券");
                            sb.append(MoneyUtils.changeF2Y(coupon.getCouponMoney()));
                            sb.append("元");
                            print.addText(ScriptConstant.LEFT, sb.toString());
                            for (int i1 = 0; i1 < coupon.getCouponCodeList().size(); i1++) {
                                if (i1 == 0) {
                                    print.addText(ScriptConstant.LEFT, "优惠券编码：" + coupon.getCouponCodeList().get(i1));
                                } else {
                                    print.addText(ScriptConstant.LEFT, "            " + coupon.getCouponCodeList().get(i1));
                                }
                            }
                        }
                    }
                }

                try {
                    print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        print.addText(ScriptConstant.LEFT, cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String user = sbf.substring(0, 16);
                        print.addText(ScriptConstant.LEFT, user);
                        String printUser = sbf.substring(16, order.getCashierName().length());
                        print.addText(ScriptConstant.LEFT, printUser);
                    } else if (order.getCashierName().length() > 12) {
                        print.addText(ScriptConstant.LEFT, cashierTitle + ": ");
                        print.addText(ScriptConstant.LEFT, order.getCashierName());
                    } else {
                        print.addText(ScriptConstant.LEFT, cashierTitle + ": " + order.getCashierName());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        print.addText(ScriptConstant.LEFT, cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String user = sbf.substring(0, 16);
                        print.addText(ScriptConstant.LEFT, user);
                        String printUser = sbf.substring(16, order.getOpUserRealName().length());
                        print.addText(ScriptConstant.LEFT, printUser);
                    } else if (order.getOpUserRealName().length() > 12) {
                        print.addText(ScriptConstant.LEFT, cashierTitle + ": ");
                        print.addText(ScriptConstant.LEFT, order.getOpUserRealName());
                    } else {
                        print.addText(ScriptConstant.LEFT, cashierTitle + ": " + order.getOpUserRealName());
                    }
                }
                /*print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_single_horizontal));*/
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元");
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元");
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元");
            }
            print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_single_horizontal));
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach());
            }

            print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                print.addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }
            /*print.addFeedline("1");
            printer.setPrnScript(print.getString(), "384");
            print.addFeedline("1");
            if (!TextUtils.isEmpty(order.getOutTradeNo())) {
                print.addFeedline("1");
                print.addText(ScriptConstant.CENTER, ConfigUtil.getPrintCodeTitle());
                printer.setPrnScript(print.getString(), "384");
                print.addFeedline("1");
                if (ConfigUtil.printCodeBillEnable()) {
                    Bitmap bitmap = MaxCardManager.getInstance().create2DCode(Constants.URL_PRINT_QRCODE + order.getOutTradeNo(), 370, 240);
                    printer.setBitmap(bitmap);
//                    print.addQrcode(ScriptConstant.CENTER, ).setQrqodeSize(300);
                } else {
                    Bitmap bitmap = MaxCardManager.getInstance().create2DCode(order.getOutTradeNo(), 370, 240);
                    printer.setBitmap(bitmap);
                }
            }*/
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub)) &&
                    SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                print.addFeedline("1");
                if (!TextUtils.isEmpty(title)) {
                    print.addText(ScriptConstant.CENTER, title);
                }
                printer.setPrnScript(print.getString(), "384");
                if (!TextUtils.isEmpty(activeUrl)) {
                    if (ConfigUtil.printCodeBillEnable()) {
                        Bitmap bitmap = MaxCardManager.getInstance().create2DCode(activeUrl, 370, 240);
                        printer.setBitmap(bitmap);
                    } else {
                        Bitmap bitmap = MaxCardManager.getInstance().create2DCode(activeUrl, 370, 240);
                        printer.setBitmap(bitmap);
                    }
                }
            } else {
                print.addFeedline("1");
                printer.setPrnScript(print.getString(), "384");
                print.addFeedline("1");
            }
            printer.startPrint(new OnPrintResultListener() {
                @Override
                public void onPrintResult(int resCode) {//arg0具体可参考常量类ServiceResult

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d("eeeeee=="+e.toString());
        }
    }

    private static String totalText(ReportBean.DataEntity info) {

        PrintScriptUtil psu = new PrintScriptUtil();

        if (info.getType() == 1) {
            title = UIUtils.getString(R.string.print_pay_report_title);
        } else if (info.getType() == 2) {
            title = UIUtils.getString(R.string.print_consume_report_title);
        }else if (info.getType() == 3){
            title = UIUtils.getString(R.string.print_fk_consume_report_title);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            storeName = UIUtils.getString(R.string.store_name_title) + ": " + "全部门店";

        } else {
            storeName = UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName();
        }

        endTime = UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime();

        psu.setNextFormat(ScriptConstant.LARGE, ScriptConstant.LARGE)
                .addText(ScriptConstant.CENTER, title)
                .addText(ScriptConstant.CENTER, " ")
                .setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL)
                .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName())
                .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId())
                .addText(ScriptConstant.LEFT, storeName)
                .addText(ScriptConstant.LEFT, "开始时间: " + info.getStartTime())
                .addText(ScriptConstant.LEFT, endTime)
                .addText(ScriptConstant.LEFT, " ")
                .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_report_total_title))
                .addText(ScriptConstant.CENTER, UIUtils.getString(R.string.print_single_horizontal));
        PrintScriptUtil pr = new PrintScriptUtil();
        if (info.getType() == 1) {
            pr.setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL)
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_net_income_title) + "：" + DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_count_title) + "：" + info.getSuccessCount() + "笔")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(info.getPayFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_merchant_coupon_title) + "：" + DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_money_title) + "：" + DateUtil.formatMoneyUtils(info.getRefundFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_count_title) + "：" + info.getRefundCount() + "笔");

            List<ReportBean.DataEntity.ListEntity> list = info.getList();
            if (list != null && list.size() > 0) {


                pr.setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL)
                        .addText(ScriptConstant.LEFT, " ")
                        .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_type_list_title) + ": ")
                        .addText(ScriptConstant.CENTER, UIUtils.getString(R.string.print_single_horizontal));

                for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                    pr.addText(ScriptConstant.LEFT, OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：" + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元")
                            .addText(ScriptConstant.LEFT, OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：" + itemInfo.getSuccessCount() + "笔");
                }
            }

        } else if (info.getType() == 2) {

            pr.setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL)
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_consume_money_title) + "：" + DateUtil.formatMoneyUtils(info.getPayFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_consume_count_title) + "：" + info.getSuccessCount() + "笔");
        }else if (info.getType() == 3){
            //饭卡消费
            pr.setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL)
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_money_title) + "：" + DateUtil.formatMoneyUtils(info.getRefundFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：" + (TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+""))
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_fk_settlement_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_trade_count_title) + "：" + info.getSuccessCount() + "笔")
                    .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_refund_count_title) + "：" + info.getRefundCount() + "笔");

        }
        PrintScriptUtil p = new PrintScriptUtil();
        p.setNextFormat(ScriptConstant.NORMAL, ScriptConstant.NORMAL)
                .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_single_horizontal))
                .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()))
                .addText(ScriptConstant.LEFT, UIUtils.getString(R.string.print_client_sign_title))
                .addText(ScriptConstant.LEFT, " ");

        String s = psu.getString() + pr.getString() + p.getString();

        return s;
    }


    /**
     * bitmap转为base64
     *
     * @param bitmap
     * @return
     */
    public static String bitmapToBase64(Bitmap bitmap) {

        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;

    }
}
