package com.hstypay.enterprise.utils.print;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.zng.common.PrintUtils;
import com.zng.common.utils.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print
 * @创建者: Jeremy
 * @创建时间: 2018/4/2 10:43
 * @描述: ${TODO}
 */

public class ZngPrintUtil {
    private static void printStatus(Context context, int ret) {
        switch (ret) {
            case 0:
                Toast.makeText(context, "打印机正常", Toast.LENGTH_SHORT).show();
                break;
            case -1:
                Toast.makeText(context, "底层库没有进行初始化", Toast.LENGTH_SHORT).show();
                break;
            case -2:
                Toast.makeText(context, "串口通信失败", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(context, "打印机有故障", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(context, "打印机缺纸", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(context, "打印机过热", Toast.LENGTH_SHORT).show();
                break;
            case 4:
                Toast.makeText(context, "请稍后重试!", Toast.LENGTH_SHORT).show();
                Logger.d("lss", "打印机未就绪");
                break;
            case 5:
                Toast.makeText(context, "通讯异常!", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, "未知  ret = " + ret, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public static void printTotal(Context context, PrintUtils printUtils, ReportBean.DataEntity info) {
        int det = printUtils.detectionPrinter();
        if (det == 0) {
            printUtils.printerSetLineAlign(1);
            printUtils.printerSetOverstriking(1);
            printUtils.setCharDoubleHeight();
            printUtils.setCharDoubleWidth();
            if (info.getType() == 1) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pay_report_title));
            } else if (info.getType() == 2) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_consume_report_title));
            } else if (info.getType() == 3) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_fk_consume_report_title));
            }
            printUtils.printNewLine();

            printUtils.printerSetLineAlign(0);
            printUtils.printerSetOverstriking(0);
            printUtils.setCharFormatNormal();
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(MyApplication.getMerchantName());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": 全部门店");
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (info.getStoreName().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": ");
                    printUtils.printTextAndNewLine(info.getStoreName());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName());
                }
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                printUtils.printTextAndNewLine(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(cashierTitle + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (info.getCashierName().length() > 12) {
                    printUtils.printTextAndNewLine(cashierTitle + ": ");
                    printUtils.printTextAndNewLine(info.getCashierName());
                } else {
                    printUtils.printTextAndNewLine(cashierTitle + ": " + info.getCashierName());
                }
            }
            if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                if (info.getCashierPointName().length() > 16) {
                    StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                    String one = sbf.substring(0, 16);
                    String two = sbf.substring(16, info.getCashierPointName().length());
                    printUtils.printTextAndNewLine("收银点: ");
                    printUtils.printTextAndNewLine(one);
                    printUtils.printTextAndNewLine(two);
                } else if (info.getCashierPointName().length() > 12) {
                    printUtils.printTextAndNewLine("收银点: ");
                    printUtils.printTextAndNewLine(info.getCashierPointName());
                } else {
                    printUtils.printTextAndNewLine("收银点: " + info.getCashierPointName());
                }
            }

            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime());
            // 结束时间
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime());

            printUtils.printNewLine();
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_report_total_title));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));

            if (info.getType() == 1) {
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));
            } else if (info.getType() == 2) {
               /* printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));*/

                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔"));

                /*printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔"));*/
            } else if (info.getType() == 3) {
                //饭卡消费
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元\n"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元\n"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "" : info.getSettleRate() + ""));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元\n"));

                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔\n"));
                printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔\n"));
            }

            if (info.getType() == 1) {
                List<ReportBean.DataEntity.ListEntity> list = info.getList();
                if (list != null && list.size() > 0) {
                    printUtils.printNewLine();
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_type_list_title) + ": ");
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));

                    for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                        printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元"));
                        printUtils.printTextAndNewLine(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔"));
                    }
                }
            }

            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_client_sign_title) + "：");

            printUtils.printNewLine();
            printUtils.printNewLine();
            printUtils.printNewLine();
            printUtils.printNewLine();
        } else {
            printStatus(context, det);
        }
    }

    public static void printPledgeTotal(Context context, PrintUtils printUtils, PledgeReportBean.DataBean info) {
        int det = printUtils.detectionPrinter();
        if (det == 0) {
            printUtils.printerSetLineAlign(1);
            printUtils.printerSetOverstriking(1);
            printUtils.setCharDoubleHeight();
            printUtils.setCharDoubleWidth();
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_pledge_report));
            printUtils.printNewLine();

            printUtils.printerSetLineAlign(0);
            printUtils.printerSetOverstriking(0);
            printUtils.setCharFormatNormal();
            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(MyApplication.getMerchantName());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": 全部门店");
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (info.getStoreName().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": ");
                    printUtils.printTextAndNewLine(info.getStoreName());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName());
                }
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                printUtils.printTextAndNewLine(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(cashierTitle + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (info.getCashierName().length() > 12) {
                    printUtils.printTextAndNewLine(cashierTitle + ": ");
                    printUtils.printTextAndNewLine(info.getCashierName());
                } else {
                    printUtils.printTextAndNewLine(cashierTitle + ": " + info.getCashierName());
                }
            }

            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime());
            // 结束时间
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime());
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));

            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_refund_count) + "：" + info.getCntFreeMoney() + "笔");
//            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元");

            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_client_sign_title) + "：");
            printUtils.printNewLine();
            printUtils.printNewLine();
            printUtils.printNewLine();
            printUtils.printNewLine();
        } else {
            printStatus(context, det);
        }
    }

    public static void print(Context context, PrintUtils printUtils, TradeDetailBean order) {
        int det = printUtils.detectionPrinter();
        if (det == 0) {
            printUtils.printerSetLineAlign(1);
            printUtils.printerSetOverstriking(1);
            printUtils.setCharDoubleHeight();
            printUtils.setCharDoubleWidth();
            if (order.getTradeType() == 1) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_recharge_title));
            } else if (order.getTradeType() == 2) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_verification_title));
            } else {
                if (!order.isPay()) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_refund_title));
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pay_title));
                }
            }
            // 空格
            printUtils.printerSetLineAlign(0);
            printUtils.printerSetOverstriking(0);
            printUtils.setCharFormatNormal();
            printUtils.printNewLine();
            printUtils.printTextAndNewLine(order.getPartner() + "              " + "请妥善保存");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(MyApplication.getMerchantName());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            String storeNameTitle;
            if (order.isPay()) {
                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
            } else {
                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(storeNameTitle + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    printUtils.printTextAndNewLine(storeNameTitle + ": ");
                    printUtils.printTextAndNewLine(order.getStoreMerchantIdCnt());
                } else {
                    printUtils.printTextAndNewLine(storeNameTitle + ": " + order.getStoreMerchantIdCnt());
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                if (order.getCashPointName().length() > 16) {
                    String str = order.getCashPointName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine("收银点: ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (order.getCashPointName().length() > 11) {
                    printUtils.printTextAndNewLine("收银点: ");
                    printUtils.printTextAndNewLine(order.getCashPointName());
                } else {
                    printUtils.printTextAndNewLine("收银点: " + order.getCashPointName());
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                printUtils.printTextAndNewLine(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()));
            }
            if (!order.isPay()) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_refund_no_title));
                printUtils.printTextAndNewLine(order.getRefundNo());
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_refund_time_title));
                printUtils.printTextAndNewLine(order.getRefundTime());

                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        printUtils.printTextAndNewLine(refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        printUtils.printTextAndNewLine(user);
                        String printuser = sbf.substring(16, order.getRefundUser().length());
                        printUtils.printTextAndNewLine(printuser);
                    } else if (order.getRefundUser().length() > 12) {
                        printUtils.printTextAndNewLine(refundUser + ": ");
                        printUtils.printTextAndNewLine(order.getRefundUser());
                    } else {
                        printUtils.printTextAndNewLine(refundUser + ": " + order.getRefundUser());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        printUtils.printTextAndNewLine(refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        printUtils.printTextAndNewLine(user);
                        String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                        printUtils.printTextAndNewLine(printuser);
                    } else if (order.getRefundUserRealName().length() > 12) {
                        printUtils.printTextAndNewLine(refundUser + ": ");
                        printUtils.printTextAndNewLine(order.getRefundUserRealName());
                    } else {
                        printUtils.printTextAndNewLine(refundUser + ": " + order.getRefundUserRealName());
                    }
                }
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()));
                if (order.getRefundMoney() > 0) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元");
                }
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_refund_instruction));
            } else {
                if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                    printUtils.printTextAndNewLine(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": ");
                    printUtils.printTextAndNewLine(order.getTransactionId());
                }
                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.tv_print_order_no) + "：");
                    printUtils.printTextAndNewLine(order.getOrderNo());
                }
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()));
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()));
                try {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        printUtils.printTextAndNewLine(cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String user = sbf.substring(0, 16);
                        printUtils.printTextAndNewLine(user);
                        String printUser = sbf.substring(16, order.getCashierName().length());
                        printUtils.printTextAndNewLine(printUser);
                    } else if (order.getCashierName().length() > 12) {
                        printUtils.printTextAndNewLine(cashierTitle + ": ");
                        printUtils.printTextAndNewLine(order.getCashierName());
                    } else {
                        printUtils.printTextAndNewLine(cashierTitle + ": " + order.getCashierName());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        printUtils.printTextAndNewLine(cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String user = sbf.substring(0, 16);
                        printUtils.printTextAndNewLine(user);
                        String printUser = sbf.substring(16, order.getOpUserRealName().length());
                        printUtils.printTextAndNewLine(printUser);
                    } else if (order.getOpUserRealName().length() > 12) {
                        printUtils.printTextAndNewLine(cashierTitle + ": ");
                        printUtils.printTextAndNewLine(order.getOpUserRealName());
                    } else {
                        printUtils.printTextAndNewLine(cashierTitle + ": " + order.getOpUserRealName());
                    }
                }
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元");
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元");
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元");
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach());
            }
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }

            printUtils.printNewLine();
            printUtils.printerSetLineAlign(1);
            if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                } else {
                    printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                }
                printUtils.printTextAndNewLine(ConfigUtil.getPrintCodeTitle());
                printUtils.printTwoDemChar(printCode);
            }

            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        printUtils.printTextAndNewLine(title);
                    }
                    printUtils.printTwoDemChar(activeUrl);
                }
            }
            printUtils.printNewLine();
            printUtils.printerSetLineAlign(1);
            printUtils.printTextAndNewLine(order.isAutoPrint() ? "自动打印" : "手动打印");
            printUtils.printNewLine();
            printUtils.printNewLine();
            printUtils.printNewLine();
        } else {
            printStatus(context, det);
        }
    }

    public static void pledgerint(Context context, PrintUtils printUtils, PledgePayBean.DataBean order) {
        int det = printUtils.detectionPrinter();
        if (det == 0) {
            printUtils.printerSetLineAlign(1);
            printUtils.printerSetOverstriking(1);
            printUtils.setCharDoubleHeight();
            printUtils.setCharDoubleWidth();
            if (order.isToPay()) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_pledge_pay));
            } else {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_pledge));
            }
            // 空格
            printUtils.printerSetLineAlign(0);
            printUtils.printerSetOverstriking(0);
            printUtils.setCharFormatNormal();
            printUtils.printNewLine();
            printUtils.printTextAndNewLine(order.getPartner() + "              " + "请妥善保存");
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + ": ");
                    printUtils.printTextAndNewLine(MyApplication.getMerchantName());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.trade_store_name) + ": ");
                    printUtils.printTextAndNewLine(one);
                    String two = sbf.substring(16, str.length());
                    printUtils.printTextAndNewLine(two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.trade_store_name) + ": ");
                    printUtils.printTextAndNewLine(order.getStoreMerchantIdCnt());
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt());
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()));
            }
            try {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                printUtils.printTextAndNewLine(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：");
                printUtils.printTextAndNewLine(order.getOutTransactionId());
            }
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.tv_pledge_order_no) + ": ");
                printUtils.printTextAndNewLine(order.getAuthNo());
            }

            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()));

            if (!order.isToPay()) {
                if (!TextUtils.isEmpty(order.getMoney())) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元");
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元");
                }
            }
            if (order.isToPay()) {
                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元");
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元");
                }
                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元");
                } else {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元");
                }
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    printUtils.printTextAndNewLine(cashierTitle + ": ");
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String oneUser = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(oneUser);
                    String twoUser = sbf.substring(16, order.getCashierName().length());
                    printUtils.printTextAndNewLine(twoUser);
                }
                if (order.getCashierName().length() > 12) {
                    printUtils.printTextAndNewLine(cashierTitle + ": ");
                    printUtils.printTextAndNewLine(order.getCashierName());
                } else {
                    printUtils.printTextAndNewLine(cashierTitle + ": " + order.getCashierName());
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                String opUserTitle;
                if (order.isToPay()) {
                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                } else {
                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                }
                if (order.getOpUserRealName().length() > 16) {
                    printUtils.printTextAndNewLine(opUserTitle);
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String oneUser = sbf.substring(0, 16);
                    printUtils.printTextAndNewLine(oneUser);
                    String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                    printUtils.printTextAndNewLine(twoUser);
                }
                if (order.getOpUserRealName().length() > 12) {
                    printUtils.printTextAndNewLine(opUserTitle);
                    printUtils.printTextAndNewLine(order.getOpUserRealName());
                } else {
                    printUtils.printTextAndNewLine(opUserTitle + order.getOpUserRealName());
                }
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach());
            }
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
            printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }

            printUtils.printNewLine();
            printUtils.printerSetLineAlign(1);
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                if (!order.isToPay()) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_scan_title));
                }
                if (ConfigUtil.printCodeBillEnable()) {
                    printUtils.printTwoDemChar(Constants.URL_PRINT_QRCODE + order.getAuthNo());
                } else {
                    printUtils.printTwoDemChar(order.getAuthNo());
                }
            }

            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        printUtils.printTextAndNewLine(title);
                    }
                    printUtils.printTwoDemChar(activeUrl);
                }
            }
            if (!order.isToPay()) {
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_single_horizontal));
                printUtils.printTextAndNewLine(UIUtils.getString(R.string.print_pledge_notice));
            }
            printUtils.printNewLine();
            printUtils.printerSetLineAlign(1);
            printUtils.printTextAndNewLine(order.isAutoPrint() ? "自动打印" : "手动打印");
            printUtils.printNewLine();
            printUtils.printNewLine();
            printUtils.printNewLine();
        } else {
            printStatus(context, det);
        }
    }
}
