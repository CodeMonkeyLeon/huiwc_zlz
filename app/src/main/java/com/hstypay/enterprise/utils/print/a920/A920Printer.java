package com.hstypay.enterprise.utils.print.a920;

import android.content.Context;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.pax.dal.entity.EFontTypeAscii;
import com.pax.dal.entity.EFontTypeExtCode;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print.a920
 * @创建者: Jeremy
 * @创建时间: 2018/8/13 15:03
 * @描述: ${TODO}
 */
public class A920Printer {
    public static void print(Context context, TradeDetailBean order) {
        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_32, EFontTypeExtCode.FONT_32_32);
        if (order.getTradeType() == 1) {
            PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_recharge_title) + "\n", null);
        } else if (order.getTradeType() == 2) {
            PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_verification_title) + "\n", null);
        } else {
            if (!order.isPay()) {
                PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_refund_title) + "\n", null);
            } else {
                PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_pay_title) + "\n", null);
            }
        }

        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_24, EFontTypeExtCode.FONT_24_24);
        PrinterTester.getInstance().printStr("\n", null);
        PrinterTester.getInstance().printStr(order.getPartner() + "              " + "请妥善保存\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (MyApplication.getMerchantName().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(MyApplication.getMerchantName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", null);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", null);
        }

        String storeNameTitle;
        if (order.isPay()) {
            storeNameTitle = UIUtils.getString(R.string.trade_store_name);
        } else {
            storeNameTitle = UIUtils.getString(R.string.refund_store_name);
        }
        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(storeNameTitle + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                PrinterTester.getInstance().printStr(storeNameTitle + ": \n", null);
                PrinterTester.getInstance().printStr(order.getStoreMerchantIdCnt() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(storeNameTitle + ": " + order.getStoreMerchantIdCnt() + "\n", null);
            }
        }
        if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
            if (order.getCashPointName().length() > 16) {
                String str = order.getCashPointName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr("收银点: " + "\n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (order.getCashPointName().length() > 11) {
                PrinterTester.getInstance().printStr("收银点: " + "\n", null);
                PrinterTester.getInstance().printStr(order.getCashPointName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr("收银点: " + order.getCashPointName() + "\n", null);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
            PrinterTester.getInstance().printStr(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()) + "\n", null);
        }
        if (!order.isPay()) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_refund_no_title) + ": \n", null);
            PrinterTester.getInstance().printStr(order.getRefundNo() + "\n", null);
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_refund_time_title) + ": \n", null);
            PrinterTester.getInstance().printStr(order.getRefundTime() + "\n", null);

            String refundUser = UIUtils.getString(R.string.print_refund_user_title);
            if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                if (order.getRefundUser().length() > 16) {
                    PrinterTester.getInstance().printStr(refundUser + ": \n", null);
                    StringBuffer sbf = new StringBuffer(order.getRefundUser());
                    String user = sbf.substring(0, 16);
                    PrinterTester.getInstance().printStr(user + "\n", null);
                    String printUser = sbf.substring(16, order.getRefundUser().length());
                    PrinterTester.getInstance().printStr(printUser + "\n", null);
                } else if (order.getRefundUser().length() > 12) {
                    PrinterTester.getInstance().printStr(refundUser + ": \n", null);
                    PrinterTester.getInstance().printStr(order.getRefundUser() + "\n", null);
                } else {
                    PrinterTester.getInstance().printStr(refundUser + ": " + order.getRefundUser() + "\n", null);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                if (order.getRefundUserRealName().length() > 16) {
                    PrinterTester.getInstance().printStr(refundUser + ": \n", null);
                    StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                    String user = sbf.substring(0, 16);
                    PrinterTester.getInstance().printStr(user + "\n", null);
                    String printUser = sbf.substring(16, order.getRefundUserRealName().length());
                    PrinterTester.getInstance().printStr(printUser + "\n", null);
                } else if (order.getRefundUserRealName().length() > 12) {
                    PrinterTester.getInstance().printStr(refundUser + ": \n", null);
                    PrinterTester.getInstance().printStr(order.getRefundUserRealName() + "\n", null);
                } else {
                    PrinterTester.getInstance().printStr(refundUser + ": " + order.getRefundUserRealName() + "\n", null);
                }
            }
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()) + "\n", null);
            if (order.getRefundMoney() > 0) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元" + "\n", null);
            }
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_refund_instruction) + "\n", null);
        } else {
            if (!TextUtils.isEmpty(order.getTransactionId())) {
                PrinterTester.getInstance().printStr(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": \n", null);
                PrinterTester.getInstance().printStr(order.getTransactionId() + "\n", null);
            }

            if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.tv_print_order_no) + ": \n", null);
                PrinterTester.getInstance().printStr(order.getOrderNo() + "\n", null);
            }
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()) + "\n", null);
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()) + "\n", null);
            try {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime() + "\n", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String user = sbf.substring(0, 16);
                    PrinterTester.getInstance().printStr(user + "\n", null);
                    String printUser = sbf.substring(16, order.getCashierName().length());
                    PrinterTester.getInstance().printStr(printUser + "\n", null);
                } else if (order.getCashierName().length() > 12) {
                    PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                    PrinterTester.getInstance().printStr(order.getCashierName() + "\n", null);
                } else {
                    PrinterTester.getInstance().printStr(cashierTitle + ": " + order.getCashierName() + "\n", null);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                if (order.getOpUserRealName().length() > 16) {
                    PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String user = sbf.substring(0, 16);
                    PrinterTester.getInstance().printStr(user + "\n", null);
                    String printUser = sbf.substring(16, order.getOpUserRealName().length());
                    PrinterTester.getInstance().printStr(printUser + "\n", null);
                } else if (order.getOpUserRealName().length() > 12) {
                    PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                    PrinterTester.getInstance().printStr(order.getOpUserRealName() + "\n", null);
                } else {
                    PrinterTester.getInstance().printStr(cashierTitle + ": " + order.getOpUserRealName() + "\n", null);
                }
            }
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元" + "\n", null);
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元" + "\n", null);
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元" + "\n", null);
        }

        //备注
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach() + "\n", null);
        }

        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", null);
        //顾客签名
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_client_sign_title) + ": \n", null);
        }
        PrinterTester.getInstance().printStr("\n", null);
        if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
            String printCode;
            if (ConfigUtil.printCodeBillEnable()) {
                printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            } else {
                printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            }
            PrinterTester.getInstance().printQrcode(PrintFormatUtils.printCenterData(ConfigUtil.getPrintCodeTitle()));
            PrinterTester.getInstance().printQrcode(printCode);
        }

        if (order.getPartner().equals(
                UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                if (!TextUtils.isEmpty(title)) {
                    PrinterTester.getInstance().printStr(title + "\n", null);
                }
                PrinterTester.getInstance().printQrcode(activeUrl);
            }
        }
        PrinterTester.getInstance().printStr(" \n", null);
        PrinterTester.getInstance().printStr("        " + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n", null);
        PrinterTester.getInstance().printStr("\n\n\n\n", null);

        PrinterTester.getInstance().start();
        //ToastUtil.showToastShort(status);
    }

    public static void pledgePrint(Context context, PledgePayBean.DataBean order) {
        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_32, EFontTypeExtCode.FONT_32_32);
        if (order.isToPay()) {
            PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_title_pledge_pay) + "\n", null);
        } else {
            PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_title_pledge) + "\n", null);
        }

        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_24, EFontTypeExtCode.FONT_24_24);
        PrinterTester.getInstance().printStr("\n", null);
        PrinterTester.getInstance().printStr(order.getPartner() + "              " + "请妥善保存\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (MyApplication.getMerchantName().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(MyApplication.getMerchantName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", null);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", null);
        }

        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.trade_store_name) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.trade_store_name) + ": \n", null);
                PrinterTester.getInstance().printStr(order.getStoreMerchantIdCnt() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt() + "\n", null);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()) + "\n", null);
        }
        try {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime() + "\n", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
            PrinterTester.getInstance().printStr(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：" + "\n", null);
            PrinterTester.getInstance().printStr(order.getOutTransactionId() + "：" + "\n", null);
        }
        if (!TextUtils.isEmpty(order.getAuthNo())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.tv_pledge_order_no) + ": " + "\n", null);
            PrinterTester.getInstance().printStr(order.getAuthNo() + "\n", null);
        }
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()) + "\n", null);

        if (!order.isToPay()) {
            if (!TextUtils.isEmpty(order.getMoney())) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元" + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元" + "\n", null);
            }
        }
        if (order.isToPay()) {
            if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元" + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元" + "\n", null);
            }
            if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元" + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元" + "\n", null);
            }
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 12) {
                    PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String oneUser = sbf.substring(0, 12);
                    PrinterTester.getInstance().printStr(oneUser + "\n", null);
                    String twoUser = sbf.substring(12, order.getCashierName().length());
                    PrinterTester.getInstance().printStr(twoUser + "\n", null);
                }
                if (order.getCashierName().length() > 12) {
                    PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                    PrinterTester.getInstance().printStr(order.getCashierName() + "\n", null);
                } else {
                    PrinterTester.getInstance().printStr(cashierTitle + ": " + order.getCashierName() + "\n", null);
                }
            }
        } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
            String opUserTitle;
            if (order.isToPay()) {
                opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
            } else {
                opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
            }
            if (order.getOpUserRealName().length() > 12) {
                PrinterTester.getInstance().printStr(opUserTitle + "\n", null);
                StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                String oneUser = sbf.substring(0, 12);
                PrinterTester.getInstance().printStr(oneUser + "\n", null);
                String twoUser = sbf.substring(12, order.getOpUserRealName().length());
                PrinterTester.getInstance().printStr(twoUser + "\n", null);
            }
            if (order.getOpUserRealName().length() > 12) {
                PrinterTester.getInstance().printStr(opUserTitle + "\n", null);
                PrinterTester.getInstance().printStr(order.getOpUserRealName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(opUserTitle + order.getOpUserRealName() + "\n", null);
            }
        }
        //备注
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach() + "\n", null);
        }

        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", null);

        //顾客签名
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_client_sign_title) + ": \n", null);
        }
        PrinterTester.getInstance().printStr("\n", null);
        if (!TextUtils.isEmpty(order.getAuthNo())) {
            if (!order.isToPay()) {
                PrinterTester.getInstance().printStr("    " + UIUtils.getString(R.string.print_pledge_scan_title) + "\n", null);
            }
            String printCode;
            if (ConfigUtil.printCodeBillEnable()) {
                printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
            } else {
                printCode = order.getAuthNo();
            }
            PrinterTester.getInstance().printQrcode(printCode);
        }

        if (order.getPartner().equals(
                UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                if (!TextUtils.isEmpty(title)) {
                    PrinterTester.getInstance().printStr(title + "\n", null);
                }
                PrinterTester.getInstance().printQrcode(activeUrl);
            }
        }
        if (!order.isToPay()) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_notice) + "\n", null);
        }
        PrinterTester.getInstance().printStr(" \n", null);
        PrinterTester.getInstance().printStr("        " + (order.isAutoPrint() ? "自动打印" : "手动打印") + "\n", null);
        PrinterTester.getInstance().printStr("\n\n\n\n\n", null);

        PrinterTester.getInstance().start();
        //ToastUtil.showToastShort(status);
    }

    //打印汇总小票
    public static void printTotal(Context context, ReportBean.DataEntity info) {
        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_32, EFontTypeExtCode.FONT_32_32);
        if (info.getType() == 1) {
            PrinterTester.getInstance().printStr("        " + UIUtils.getString(R.string.print_pay_report_title) + "\n", null);
        } else if (info.getType() == 2) {
            PrinterTester.getInstance().printStr("      " + UIUtils.getString(R.string.print_consume_report_title) + "\n", null);
        } else if (info.getType() == 3) {
            PrinterTester.getInstance().printStr("      " + UIUtils.getString(R.string.print_fk_consume_report_title) + "\n", null);
        }

        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_24, EFontTypeExtCode.FONT_24_24);
        PrinterTester.getInstance().printStr("\n", null);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (MyApplication.getMerchantName().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(MyApplication.getMerchantName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", null);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", null);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": " + "全部门店\n", null);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (info.getStoreName().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": \n", null);
                PrinterTester.getInstance().printStr(info.getStoreName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n", null);
            }
        }

        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            PrinterTester.getInstance().printStr(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+ "\n", null);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (info.getCashierName().length() > 12) {
                PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                PrinterTester.getInstance().printStr(info.getCashierName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(cashierTitle + ": " + info.getCashierName() + "\n", null);
            }
        }
        if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
            if (info.getCashierPointName().length() > 16) {
                StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                String one = sbf.substring(0, 16);
                String two = sbf.substring(16, info.getCashierPointName().length());
                PrinterTester.getInstance().printStr("收银点: " + "\n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (info.getCashierPointName().length() > 12) {
                PrinterTester.getInstance().printStr("收银点: " + "\n", null);
                PrinterTester.getInstance().printStr(info.getCashierPointName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr("收银点: " + info.getCashierPointName() + "\n", null);
            }
        }

        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\n", null);
        // 结束时间
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n", null);

        PrinterTester.getInstance().printStr("\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_report_total_title) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);

        if (info.getType() == 1) {
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n", null);
        } else if (info.getType() == 2) {
            /*PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n", null);*/

            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔") + "\n", null);

            /*PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔") + "\n", null);*/

        } else if (info.getType() == 3) {
            //饭卡消费
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "" : info.getSettleRate() + "") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元") + "\n", null);

            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔") + "\n", null);
            PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔") + "\n", null);
        }
        if (info.getType() == 1) {
            List<ReportBean.DataEntity.ListEntity> list = info.getList();
            if (list != null && list.size() > 0) {
                PrinterTester.getInstance().printStr("\n", null);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_trade_type_list_title) + ": " + "\n", null);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);

                for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                    PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元") + "\n", null);
                    PrinterTester.getInstance().printStr(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔") + "\n", null);
                }
            }
        }
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n", null);

        PrinterTester.getInstance().printStr("\n\n\n\n\n\n", null);
        PrinterTester.getInstance().start();
    }

    public static void printPledgeTotal(Context context, PledgeReportBean.DataBean info) {
        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_32, EFontTypeExtCode.FONT_32_32);
        PrinterTester.getInstance().printStr("       " + UIUtils.getString(R.string.print_title_pledge_report) + "\n", null);

        PrinterTester.getInstance().fontSet(EFontTypeAscii.FONT_16_24, EFontTypeExtCode.FONT_24_24);
        PrinterTester.getInstance().printStr("\n", null);

        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (MyApplication.getMerchantName().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + ": \n", null);
                PrinterTester.getInstance().printStr(MyApplication.getMerchantName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName() + "\n", null);
            }
        }

        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId() + "\n", null);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": " + "全部门店\n", null);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (info.getStoreName().length() > 11) {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": \n", null);
                PrinterTester.getInstance().printStr(info.getStoreName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName() + "\n", null);
            }
        }
        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            PrinterTester.getInstance().printStr(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)+ "\n", null);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                PrinterTester.getInstance().printStr(cashierTitle + ": \n", null);
                PrinterTester.getInstance().printStr(one + "\n", null);
                String two = sbf.substring(16, str.length());
                PrinterTester.getInstance().printStr(two + "\n", null);
            } else if (info.getCashierName().length() > 12) {
                PrinterTester.getInstance().printStr(cashierTitle + ":\n", null);
                PrinterTester.getInstance().printStr(info.getCashierName() + "\n", null);
            } else {
                PrinterTester.getInstance().printStr(cashierTitle + ": " + info.getCashierName() + "\n", null);
            }
        }

        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime() + "\n", null);
        // 结束时间
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime() + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);

        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_refund_money) + ": " + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_refund_count) + ":" + info.getCntFreeMoney() + "笔\n", null);
//        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_single_horizontal) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", null);
        PrinterTester.getInstance().printStr(UIUtils.getString(R.string.print_client_sign_title) + "：" + "\n", null);

        PrinterTester.getInstance().printStr("\n\n\n\n\n\n\n", null);

        PrinterTester.getInstance().start();
    }
}
