package com.hstypay.enterprise.utils.print.xdl;

import android.newland.os.NlBuild;
import android.os.Build;
import android.os.Handler;

import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.utils.LogUtil;
import com.newland.me.ConnUtils;
import com.newland.me.DeviceManager;
import com.newland.mtype.ConnectionCloseEvent;
import com.newland.mtype.Device;
import com.newland.mtype.ExModuleType;
import com.newland.mtype.ModuleType;
import com.newland.mtype.conn.DeviceConnParams;
import com.newland.mtype.event.DeviceEventListener;
import com.newland.mtype.module.common.cardreader.K21CardReader;
import com.newland.mtype.module.common.emv.EmvModule;
import com.newland.mtype.module.common.externalPin.ExternalPinInput;
import com.newland.mtype.module.common.externalScan.ExternalScan;
import com.newland.mtype.module.common.externalrfcard.ExternalRFCard;
import com.newland.mtype.module.common.externalsignature.ExternalSignature;
import com.newland.mtype.module.common.iccard.ICCardModule;
import com.newland.mtype.module.common.light.IndicatorLight;
import com.newland.mtype.module.common.pin.K21Pininput;
import com.newland.mtype.module.common.printer.Printer;
import com.newland.mtype.module.common.rfcard.K21RFCardModule;
import com.newland.mtype.module.common.scanner.BarcodeScanner;
import com.newland.mtype.module.common.scanner.BarcodeScannerManager;
import com.newland.mtype.module.common.security.K21SecurityModule;
import com.newland.mtype.module.common.serialport.SerialModule;
import com.newland.mtype.module.common.sm.SmModule;
import com.newland.mtype.module.common.storage.Storage;
import com.newland.mtype.module.common.swiper.K21Swiper;
import com.newland.mtypex.nseries.NSConnV100ConnParams;
import com.newland.mtypex.nseries3.NS3ConnParams;

import java.lang.reflect.Method;

//import com.newland.mtype.ExModuleType;
//import com.newland.mtype.module.common.serialport.SerialModule;

public class N900Device extends AbstractDevice {
	private static final String K21_DRIVER_NAME = "com.newland.me.K21Driver";
//	private static final String K21_DRIVER_NAME = "com.newland.me.MT90Driver";
private DeviceConnParams deviceConnParams = null;

	private static BaseActivity baseActivity;
	private static N900Device n900Device=null;
	private static DeviceManager deviceManager ;

	private N900Device(BaseActivity baseactivity) {
		N900Device.baseActivity = baseactivity;
	}

	public static N900Device getInstance(BaseActivity baseactivity) {
		if (n900Device == null) {
			synchronized (N900Device.class) {
				if (n900Device == null) {
					n900Device = new N900Device(baseactivity);
				}
			}
		}                                                 
		N900Device.baseActivity = baseactivity;
		return n900Device;
	}

	@Override
	public void connectDevice() {
		try {
			deviceManager = ConnUtils.getDeviceManager();
			initConnParams();

			deviceManager.init(baseActivity, K21_DRIVER_NAME, deviceConnParams, new DeviceEventListener<ConnectionCloseEvent>() {

				@Override
				public void onEvent(ConnectionCloseEvent event, Handler handler) {
					if (event.isSuccess()) {
						MyToast.showToastShort("设备被客户主动断开！");
					}
					if (event.isFailed()) {
						MyToast.showToastShort("设备链接异常断开！");
					}
				}

				@Override
				public Handler getUIHandler() {
					return null;
				}
			});

			deviceManager.connect();
			//MyToast.showToastShort("设备连接成功,连接对象"+deviceConnParams.getConnectType());
			//baseActivity.btnStateToWaitingConn();

		} catch (Exception e1) {
			e1.printStackTrace();
			MyToast.showToastShort("链接异常,请检查设备或重新连接..."+e1);
		}
	}

	@Override
	public void disconnect() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (deviceManager != null) {
						deviceManager.disconnect();				
						deviceManager = null;
						//baseActivity.btnStateToWaitingInit();
					}
				} catch (Exception e) {
					LogUtil.e("设备断开异常:" + e);
				}
			}
		}).start();
	}
	/**
	 * 获取新大陆SDK版本信息。
	 *
	 * @return
	 *      SDK2.0      -- 非事件机制版本
	 *      SDK3.0      -- 事件机制版本
	 */
	public static String SDKVersion() {
		String version = "unkonwn";
		/**
		 * ro.build.newland_sdk 后续固件版本增加的属性值
		 *
		 */
		version = getProperties("ro.build.newland_sdk");
		if ("unkonwn".equals(version)) {
			version = getProperties("ro.build.customer_id");
			if ("unknown".equals(version)) {
				// 根据MTMS之前的规则判断
				//20180719，SDK 2.0： SDK 2.0分支、银商专用、阿里千牛，其他的都是SDK 3.0。
				return  version;
			} else if ("ChinaUms".equals(version) || "SDK_2.0".equals(version) || "AliQianNiu".equals(version)) {
				return "SDK2.0";
			} else {
				return "SDK3.0";
			}
		} else {
			return version;
		}


	}


	/**
	 * 获取系统属性值
	 *
	 * @param key
	 * @return
	 *      返回值 unknown  表示属性值不存在。
	 *      其他返回具体的属性值
	 */

	private static String getProperties(String key) {
		String defaultValue = "unkonwn";
		String value = defaultValue;
		try {
			Class<?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class, String.class);
			value = (String) (get.invoke(c, key, defaultValue));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	private void initConnParams(){
		String current_driver_version = NlBuild.VERSION.NL_FIRMWARE;
		if(SDKVersion()=="SDK2.0"){
			deviceConnParams = new NSConnV100ConnParams();
		}else if(SDKVersion()=="SDK3.0"){
			deviceConnParams = new NS3ConnParams();

		}else{			//unkonwn
			if("SA1".equals(NlBuild.VERSION.NL_HARDWARE_ID) && Build.MODEL.equals("N900")){ //900 3G设备只支持2.0
				deviceConnParams = new NSConnV100ConnParams();
				return;
			}
			if(Build.MODEL.equals("N900")){
				if(current_driver_version.equals("V2.0.28")||current_driver_version.equals("V2.1.03")||current_driver_version.equals("V2.1.05")||current_driver_version.equals("V2.1.09")||current_driver_version.equals("V2.1.18")||current_driver_version.equals("V2.1.27")||current_driver_version.equals("V2.1.37")||current_driver_version.equals("V2.1.49")||current_driver_version.equals("V2.1.53")||current_driver_version.equals("V2.1.58")
						||current_driver_version.equals("V2.1.62")||current_driver_version.equals("V2.0.16")||current_driver_version.equals("V2.1.17")||current_driver_version.equals("V2.1.21")||current_driver_version.equals("V2.1.23")||current_driver_version.equals("V2.1.24")||current_driver_version.equals("V2.1.29")||current_driver_version.equals("V2.1.31")||current_driver_version.equals("V2.1.32")||current_driver_version.equals("V2.1.40")||current_driver_version.equals("V2.1.41")
						||current_driver_version.equals("V2.1.44")||current_driver_version.equals("V2.0.45")||current_driver_version.equals("V2.1.46")||current_driver_version.equals("V2.1.48")||current_driver_version.equals("V2.1.51")||current_driver_version.equals("V2.1.55")||current_driver_version.equals("V2.1.56")||current_driver_version.equals("V2.1.60")){
					deviceConnParams = new NS3ConnParams();
				}else{
					deviceConnParams = new NSConnV100ConnParams(); //默认2.0连接
				}

			}else if(Build.MODEL.equals("N910")){
				if(current_driver_version.equals("V2.0.28")||current_driver_version.equals("V2.1.03")||current_driver_version.equals("V2.1.05")||current_driver_version.equals("V2.1.09")||current_driver_version.equals("V2.1.18")||current_driver_version.equals("V2.1.27")||current_driver_version.equals("V2.1.35")||current_driver_version.equals("V2.1.40")||current_driver_version.equals("V2.1.52")||current_driver_version.equals("V2.1.54")
						||current_driver_version.equals("V2.1.64")||current_driver_version.equals("V2.0.16")||current_driver_version.equals("V2.1.13")||current_driver_version.equals("V2.1.21")||current_driver_version.equals("V2.1.23")||current_driver_version.equals("V2.1.24")||current_driver_version.equals("V2.1.29")||current_driver_version.equals("V2.1.30")||current_driver_version.equals("V2.1.32")||current_driver_version.equals("V2.1.43")||current_driver_version.equals("V2.0.33")
						||current_driver_version.equals("V2.1.44")||current_driver_version.equals("V2.0.45")||current_driver_version.equals("V2.1.37")||current_driver_version.equals("V2.0.36")||current_driver_version.equals("V2.1.66")||current_driver_version.equals("V2.1.67")||current_driver_version.equals("V2.1.68")||current_driver_version.equals("V2.1.71")||current_driver_version.equals("V2.1.72")||current_driver_version.equals("V2.3.00")||current_driver_version.equals("V2.1.57")||current_driver_version.equals("V2.1.26")){
					deviceConnParams = new NS3ConnParams();
				}else{
					deviceConnParams = new NSConnV100ConnParams(); //默认2.0连接
				}
			}else {
				deviceConnParams = new NS3ConnParams();
			}

		}


	}
	@Override
	public boolean isDeviceAlive() {
		boolean ifConnected = ( deviceManager== null ? false : deviceManager.getDevice().isAlive());
        return ifConnected;
	}

	@Override
	public K21CardReader getCardReaderModuleType() {
		K21CardReader cardReader=(K21CardReader) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_CARDREADER);
		return cardReader;
	}

	@Override
	public EmvModule getEmvModuleType() {
		EmvModule emvModule=(EmvModule) deviceManager.getDevice().getExModule("EMV_INNERLEVEL2");
		return emvModule;
	}

	@Override
	public ICCardModule getICCardModule() {
		ICCardModule iCCardModule=(ICCardModule) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_ICCARDREADER);
		return iCCardModule;
	}

	@Override
	public IndicatorLight getIndicatorLight() {
		IndicatorLight indicatorLight=(IndicatorLight) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_INDICATOR_LIGHT);
		return indicatorLight;
	}

	@Override
	public K21Pininput getK21Pininput() {
		K21Pininput k21Pininput=(K21Pininput) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_PININPUT);
		return k21Pininput;
	}

	@Override
	public Printer getPrinter() {
		Printer printer=(Printer) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_PRINTER);
		printer.init();
		return printer;
	}

	@Override
	public K21RFCardModule getRFCardModule() {
		K21RFCardModule rFCardModule=(K21RFCardModule) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_RFCARDREADER);
		return rFCardModule;
	}

	@Override
	public BarcodeScanner getBarcodeScanner() {
		BarcodeScannerManager barcodeScannerManager=(BarcodeScannerManager) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_BARCODESCANNER);
		BarcodeScanner scanner = barcodeScannerManager.getDefault();
		return scanner;
	}

	@Override
	public K21SecurityModule getSecurityModule() {
		K21SecurityModule securityModule=(K21SecurityModule) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_SECURITY);
		return securityModule;
	}

	@Override
	public Storage getStorage() {
		Storage storage=(Storage) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_STORAGE);
		return storage;
	}

	@Override
	public K21Swiper getK21Swiper() {
		K21Swiper k21Swiper=(K21Swiper) deviceManager.getDevice().getStandardModule(ModuleType.COMMON_SWIPER);
		return k21Swiper;
	}

	@Override
	public Device getDevice() {
		return deviceManager.getDevice();
	}
	
	@Override
	public SerialModule getUsbSerial() {
		SerialModule k21Serial=(SerialModule) deviceManager.getDevice().getExModule(ExModuleType.USBSERIAL);
		return k21Serial;
	}

	@Override
	public SmModule getSmModule() {
		SmModule smModule = (SmModule)deviceManager.getDevice().getStandardModule(ModuleType.COMMON_SM);
		return smModule;
	}
	@Override
	public String getModel() {
		return Build.MODEL;
	}

	@Override
	public ExternalPinInput getExternalPinInput() {
		return  (ExternalPinInput) deviceManager.getDevice().getStandardModule(ModuleType.EXTERNAL_PININPUT);
	}
	@Override
	public ExternalRFCard getExternalRfCard() {
		return (ExternalRFCard) deviceManager.getDevice().getExModule(ExModuleType.RFCARD);
	}

	@Override
	public ExternalSignature getExternalSignature() {
		return  (ExternalSignature) deviceManager.getDevice().getExModule(ExModuleType.SIGNATURE);
	}

	@Override
	public ExternalScan getExternalScan() {
		return  (ExternalScan) deviceManager.getDevice().getExModule(ExModuleType.SCAN);
	}

}
