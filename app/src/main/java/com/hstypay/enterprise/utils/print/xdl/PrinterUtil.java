package com.hstypay.enterprise.utils.print.xdl;

import android.graphics.Bitmap;

import com.newland.mtype.module.common.printer.PrintContext;
import com.newland.mtype.module.common.printer.Printer;
import com.newland.mtype.module.common.printer.PrinterResult;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PrinterUtil {

    private static PrinterUtil instance;

    private StringBuffer mBuffer = new StringBuffer();
    private Map<String, Bitmap> map = new HashMap<String, Bitmap>();

    private PrinterUtil() {}

    public static PrinterUtil getInstance() {
//        if (instance == null) {
            instance = new PrinterUtil();
//        }
        return instance;
    }

    // 开始打印，所有数据组装完成后调此方法开始打印
    public PrinterResult startPrint(Printer printer) throws Exception {
        PrinterResult result;
        if (!map.isEmpty()) {
            result = printer.printByScript(PrintContext.defaultContext(), mBuffer.toString().getBytes("GBK"), map, 60, TimeUnit.SECONDS);
        } else {
            result = printer.printByScript(PrintContext.defaultContext(), mBuffer.toString().getBytes("GBK"), 60, TimeUnit.SECONDS);
        }
        return result;
    }

    // 设置字体大小和行间距
    public void setPrintFormat(int font, int lineSpace) {
        if (font == Font.SMALL) {
            mBuffer.append("!hz s\n !asc s\n !gray 5\n");
        } else if (font == Font.LARGE) {
            mBuffer.append("!hz l\n !asc n\n !gray 5\n");
        } else {
            mBuffer.append("!hz n\n !asc n\n !gray 5\n");
        }
        mBuffer.append("!yspace " + lineSpace + "\n");
    }

    // 打印文本
    public void setPrintText(String text, int align) {
        if (align == Align.CENTER) {
            mBuffer.append("*text c " + text + "\n");
        } else if (align == Align.RIGHT) {
            mBuffer.append("*text r " + text + "\n");
        } else {
            mBuffer.append("*text l " + text + "\n");
        }
    }

    public void setPrintText(String text) {
        mBuffer.append("*text l " + text + "\n");
    }

    /**
     * 打印位图
     * @param key
     * @param bitmap
     * @param align
     * @param height
     * @param width
     */
    public void printBitmap(String key, Bitmap bitmap, int align, int width, int height) {
        map.put(key, bitmap);
        if (align == Align.LEFT) {
            mBuffer.append("*image l " + width + "*" + height + " path:" + key + "\n");
        } else if (align == Align.RIGHT) {
            mBuffer.append("*image r " + width + "*" + height + " path:" + key + "\n");
        } else {
            mBuffer.append("*image c " + width + "*" + height + " path:" + key + "\n");
        }
    }

    /**
     * 打印条码
     * @param barCode 条码数据
     * @param width   传入数据为1-8 默认2
     * @param height  传入数据为1-320 默认64
     */
    public void printBarCode(String barCode, int width, int height) {
        mBuffer.append("!barcode " + width + " " + height + "\n" + " *barcode c " + barCode + "\n");
    }

    // 打印二维码
    public void printQrCode(String qrCode, int height) {
        mBuffer.append("!qrcode " + height + " 2\n" + "*qrcode c " + qrCode + "\n");
    }

    // 打印下画线
    public void printUnderLine(String text, int align) {
        if (align == Align.CENTER) {
            mBuffer.append("*underline c " + text + "\n");
        } else if (align == Align.RIGHT) {
            mBuffer.append("*underline r " + text + "\n");
        } else {
            mBuffer.append("*underline l " + text + "\n");
        }
    }

    // 打印虚线   ---------------------------
    public void printLine() {
        mBuffer.append("*line" + "\n");
    }

    /**
     * 走纸
     * @param line 走纸行数
     */
    public void feedLine(int line) {
        mBuffer.append("*feedline " + line + "\n");
    }

    // 字体大小
    public static class Font {
        public static final int SMALL = 0;
        public static final int MIDDLE = 1;
        public static final int LARGE = 2;
    }

    // 字体对齐方式
    public static class Align {
        public static final int LEFT = 0;
        public static final int CENTER = 1;
        public static final int RIGHT = 2;
    }
}
