package com.hstypay.enterprise.utils.print.xdl;

import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.base.BaseActivity;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.newland.mtype.module.common.printer.Printer;
import com.newland.mtype.module.common.printer.PrinterResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print.xdl
 * @创建者: Jeremy
 * @创建时间: 2018/12/4 16:56
 * @描述: ${TODO}
 */
public class PrinterXdlUtil {
    private static PrinterXdlUtil singleton;

    public static PrinterXdlUtil with() {
        if (singleton == null) {
            synchronized (PrinterXdlUtil.class) {
                if (singleton == null) {
                    singleton = new PrinterXdlUtil();
                }
            }
        }
        return singleton;
    }

    public void print(BaseActivity activity, TradeDetailBean order) {
        try {
            N900Device n900Device = N900Device.getInstance(activity);
            Printer printer = n900Device.getPrinter();
            printer.init();

            PrinterUtil print = PrinterUtil.getInstance();
            print.setPrintFormat(PrinterUtil.Font.LARGE, 5);
            if (order.getTradeType() == 1) {
                print.setPrintText(UIUtils.getString(R.string.print_recharge_title), PrinterUtil.Align.CENTER);
            } else if (order.getTradeType() == 2) {
                print.setPrintText(UIUtils.getString(R.string.print_verification_title), PrinterUtil.Align.CENTER);
            } else {
                if (!order.isPay()) {
                    print.setPrintText(UIUtils.getString(R.string.print_refund_title), PrinterUtil.Align.CENTER);
                } else {
                    print.setPrintText(UIUtils.getString(R.string.print_pay_title), PrinterUtil.Align.CENTER);
                }
            }
            print.setPrintFormat(PrinterUtil.Font.MIDDLE, 8);
            // 空格
            print.feedLine(1);
            print.setPrintText(order.getPartner() + "              " + "请妥善保存");
            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + ": ");
                    print.setPrintText(MyApplication.getMerchantName());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                print.setPrintText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            String storeNameTitle;
            if (order.isPay()) {
                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
            } else {
                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(storeNameTitle + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    print.setPrintText(storeNameTitle + ": ");
                    print.setPrintText(order.getStoreMerchantIdCnt());
                } else {
                    print.setPrintText(storeNameTitle + ": " + order.getStoreMerchantIdCnt());
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                if (order.getCashPointName().length() > 16) {
                    String str = order.getCashPointName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText("收银点: ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (order.getCashPointName().length() > 11) {
                    print.setPrintText("收银点: ");
                    print.setPrintText(order.getCashPointName());
                } else {
                    print.setPrintText("收银点: " + order.getCashPointName());
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                print.setPrintText(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()));
            }
            if (!order.isPay()) {
                print.setPrintText(UIUtils.getString(R.string.print_refund_no_title) + ": ");
                print.setPrintText(order.getRefundNo());
                print.setPrintText(UIUtils.getString(R.string.print_refund_time_title) + ": ");
                print.setPrintText(order.getRefundTime());

                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        print.setPrintText(refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        print.setPrintText(user);
                        String printUser = sbf.substring(16, order.getRefundUser().length());
                        print.setPrintText(printUser);
                    } else if (order.getRefundUser().length() > 12) {
                        print.setPrintText(refundUser + ": ");
                        print.setPrintText(order.getRefundUser());
                    } else {
                        print.setPrintText(refundUser + ": " + order.getRefundUser());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        print.setPrintText(refundUser + ": ");
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        print.setPrintText(user);
                        String printUser = sbf.substring(16, order.getRefundUserRealName().length());
                        print.setPrintText(printUser);
                    } else if (order.getRefundUserRealName().length() > 12) {
                        print.setPrintText(refundUser + ": ");
                        print.setPrintText(order.getRefundUserRealName());
                    } else {
                        print.setPrintText(refundUser + ": " + order.getRefundUserRealName());
                    }
                }
                print.setPrintText(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()));
                if (order.getRefundMoney() > 0) {
                    print.setPrintText(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元");
                }
                print.setPrintText(UIUtils.getString(R.string.print_refund_instruction));
            } else {
                if (!TextUtils.isEmpty(order.getTransactionId())) {
                    print.setPrintText(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": ");
                    print.setPrintText(order.getTransactionId());
                }
                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    print.setPrintText(UIUtils.getString(R.string.tv_print_order_no) + ": ");
                    print.setPrintText(order.getOrderNo());
                }
                print.setPrintText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()));
                print.setPrintText(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()));
                try {
                    print.setPrintText(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        print.setPrintText(cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String user = sbf.substring(0, 16);
                        print.setPrintText(user);
                        String printUser = sbf.substring(16, order.getCashierName().length());
                        print.setPrintText(printUser);
                    } else if (order.getCashierName().length() > 12) {
                        print.setPrintText(cashierTitle + ": ");
                        print.setPrintText(order.getCashierName());
                    } else {
                        print.setPrintText(cashierTitle + ": " + order.getCashierName());
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        print.setPrintText(cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String user = sbf.substring(0, 16);
                        print.setPrintText(user);
                        String printUser = sbf.substring(16, order.getOpUserRealName().length());
                        print.setPrintText(printUser);
                    } else if (order.getOpUserRealName().length() > 12) {
                        print.setPrintText(cashierTitle + ": ");
                        print.setPrintText(order.getOpUserRealName());
                    } else {
                        print.setPrintText(cashierTitle + ": " + order.getOpUserRealName());
                    }
                }
                print.setPrintText(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元");
                print.setPrintText(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元");
                print.setPrintText(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元");
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                print.setPrintText(UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach());
            }

            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
            print.setPrintText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                print.setPrintText(UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }

            if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                } else {
                    printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                }
                print.feedLine(1);
                print.setPrintText(ConfigUtil.getPrintCodeTitle(), PrinterUtil.Align.CENTER);
                print.feedLine(1);
                print.printQrCode(printCode, 200);
            }
            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {

                        Pattern p = Pattern.compile("\t|\r|\n");
                        Matcher m = p.matcher(title);
                        title = m.replaceAll("");
                        title = title.replaceAll(" +", " ");
                        print.setPrintText(title);
                    }
                    if (!TextUtils.isEmpty(activeUrl)) {
                        print.printQrCode(activeUrl, 200);
                    }
                }
            }
            print.feedLine(1);
            print.setPrintText(order.isAutoPrint() ? "自动打印" : "手动打印", PrinterUtil.Align.CENTER);
            print.feedLine(4);
            PrinterResult printerResult = print.startPrint(printer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pledgePrint(BaseActivity activity, PledgePayBean.DataBean order) {
        try {
            N900Device n900Device = N900Device.getInstance(activity);
            Printer printer = n900Device.getPrinter();
            printer.init();

            PrinterUtil print = PrinterUtil.getInstance();
            print.setPrintFormat(PrinterUtil.Font.LARGE, 5);

            if (order.isToPay()) {
                print.setPrintText(UIUtils.getString(R.string.print_title_pledge_pay), PrinterUtil.Align.CENTER);
            } else {
                print.setPrintText(UIUtils.getString(R.string.print_title_pledge), PrinterUtil.Align.CENTER);
            }
            print.setPrintFormat(PrinterUtil.Font.MIDDLE, 8);
            // 空格
            print.feedLine(1);
            print.setPrintText(order.getPartner() + "              " + "请妥善保存");
            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + ": ");
                    print.setPrintText(MyApplication.getMerchantName());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                print.setPrintText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.trade_store_name) + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.trade_store_name) + ": ");
                    print.setPrintText(order.getStoreMerchantIdCnt());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt());
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                print.setPrintText(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()));
            }
            try {
                print.setPrintText(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                print.setPrintText(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：");
                print.setPrintText(order.getOutTransactionId());
            }
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                print.setPrintText(UIUtils.getString(R.string.tv_pledge_order_no) + ": ");
                print.setPrintText(order.getAuthNo());
            }

            print.setPrintText(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()));

            if (!order.isToPay()) {
                if (!TextUtils.isEmpty(order.getMoney())) {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元");
                } else {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元");
                }
            }
            if (order.isToPay()) {
                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元");
                } else {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元");
                }
                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元");
                } else {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元");
                }
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 12) {
                        print.setPrintText(cashierTitle + ": ");
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String oneUser = sbf.substring(0, 12);
                        print.setPrintText(oneUser);
                        String twoUser = sbf.substring(12, order.getCashierName().length());
                        print.setPrintText(twoUser);
                    }
                    if (order.getCashierName().length() > 12) {
                        print.setPrintText(cashierTitle + ": ");
                        print.setPrintText(order.getCashierName());
                    } else {
                        print.setPrintText(cashierTitle + ": " + order.getCashierName());
                    }
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                String opUserTitle;
                if (order.isToPay()) {
                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                } else {
                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                }
                if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 12) {
                        print.setPrintText(opUserTitle);
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneUser = sbf.substring(0, 12);
                        print.setPrintText(oneUser);
                        String twoUser = sbf.substring(12, order.getOpUserRealName().length());
                        print.setPrintText(twoUser);
                    }
                    if (order.getOpUserRealName().length() > 12) {
                        print.setPrintText(opUserTitle);
                        print.setPrintText(order.getOpUserRealName());
                    } else {
                        print.setPrintText(opUserTitle + order.getOpUserRealName());
                    }
                }
            }
            //付款备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                print.setPrintText(UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach());
            }
            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
            print.setPrintText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                print.setPrintText(UIUtils.getString(R.string.print_client_sign_title) + ": ");
            }

            if (!TextUtils.isEmpty(order.getAuthNo())) {
                print.feedLine(1);
                if (!order.isToPay()) {
                    print.setPrintText(UIUtils.getString(R.string.print_pledge_scan_title), PrinterUtil.Align.CENTER);
                }
                print.feedLine(1);
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
                } else {
                    printCode = order.getAuthNo();
                }
                print.printQrCode(printCode, 200);
            }
            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {

                        Pattern p = Pattern.compile("\t|\r|\n");
                        Matcher m = p.matcher(title);
                        title = m.replaceAll("");
                        title = title.replaceAll(" +", " ");
                        LogUtil.e("print------" + title);
                        print.setPrintText(title);
                    }
                    if (!TextUtils.isEmpty(activeUrl)) {
                        print.printQrCode(activeUrl, 200);
                    }
                }
            }
            if (!order.isToPay()) {
                print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
                print.setPrintText(UIUtils.getString(R.string.print_pledge_notice));
            }
            print.feedLine(1);
            print.setPrintText(order.isAutoPrint() ? "自动打印" : "手动打印", PrinterUtil.Align.CENTER);
            print.feedLine(4);
            PrinterResult printerResult = print.startPrint(printer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printPledgeTotal(BaseActivity activity, PledgeReportBean.DataBean info) {

        /**
         * 检测打印机状态
         */
        try {
            N900Device n900Device = N900Device.getInstance(activity);
            Printer printer = n900Device.getPrinter();
            printer.init();

            PrinterUtil print = PrinterUtil.getInstance();
            print.setPrintFormat(PrinterUtil.Font.LARGE, 5);
            print.setPrintText(UIUtils.getString(R.string.print_title_pledge_report), PrinterUtil.Align.CENTER);
            print.setPrintFormat(PrinterUtil.Font.MIDDLE, 8);
            // 空格
            print.feedLine(1);

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：");
                    print.setPrintText(MyApplication.getMerchantName());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }
            // 商户号

            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                print.setPrintText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            // 门店
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": 全部门店");
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (info.getStoreName().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": ");
                    print.setPrintText(info.getStoreName());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName());
                }
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                print.setPrintText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(cashierTitle + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (info.getCashierName().length() > 12) {
                    print.setPrintText(cashierTitle + ": ");
                    print.setPrintText(info.getCashierName());
                } else {
                    print.setPrintText(cashierTitle + ": " + info.getCashierName());
                }
            }

            //开始时间
            print.setPrintText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime());
            // 结束时间
            print.setPrintText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime());
            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));

            print.setPrintText(UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元");
            print.setPrintText(UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔");
            print.setPrintText(UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元");
            print.setPrintText(UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔");
            print.setPrintText(UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元");
            print.setPrintText(UIUtils.getString(R.string.print_pledge_refund_count) + "：" + info.getCntFreeMoney() + "笔");
//            print.setPrintText(UIUtils.getString(R.string.print_pledge_refund_money) + "： + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元");

            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
            print.setPrintText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            print.setPrintText(UIUtils.getString(R.string.print_client_sign_title) + "：");

            print.feedLine(6);
            PrinterResult printerResult = print.startPrint(printer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printTotal(BaseActivity activity, ReportBean.DataEntity info) {

        /**
         * 检测打印机状态
         */
        try {
            N900Device n900Device = N900Device.getInstance(activity);
            Printer printer = n900Device.getPrinter();
            printer.init();

            PrinterUtil print = PrinterUtil.getInstance();
            print.setPrintFormat(PrinterUtil.Font.LARGE, 5);
            if (info.getType() == 1) {
                print.setPrintText(UIUtils.getString(R.string.print_pay_report_title), PrinterUtil.Align.CENTER);
            } else if (info.getType() == 2) {
                print.setPrintText(UIUtils.getString(R.string.print_consume_report_title), PrinterUtil.Align.CENTER);
            } else if (info.getType() == 3) {
                print.setPrintText(UIUtils.getString(R.string.print_fk_consume_report_title), PrinterUtil.Align.CENTER);
            }
            print.setPrintFormat(PrinterUtil.Font.MIDDLE, 8);
            // 空格
            print.feedLine(1);

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (MyApplication.getMerchantName().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：");
                    print.setPrintText(MyApplication.getMerchantName());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName());
                }
            }
            // 商户号
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                print.setPrintText(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId());
            }

            // 门店
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": 全部门店");
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (info.getStoreName().length() > 11) {
                    print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": ");
                    print.setPrintText(info.getStoreName());
                } else {
                    print.setPrintText(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName());
                }
            }

            // 收银员
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                print.setPrintText(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    print.setPrintText(cashierTitle + ": ");
                    print.setPrintText(one);
                    String two = sbf.substring(16, str.length());
                    print.setPrintText(two);
                } else if (info.getCashierName().length() > 12) {
                    print.setPrintText(cashierTitle + ": ");
                    print.setPrintText(info.getCashierName());
                } else {
                    print.setPrintText(cashierTitle + ": " + info.getCashierName());
                }
            }
            if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                if (info.getCashierPointName().length() > 16) {
                    StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                    String one = sbf.substring(0, 16);
                    String two = sbf.substring(16, info.getCashierPointName().length());
                    print.setPrintText("收银点: ");
                    print.setPrintText(one);
                    print.setPrintText(two);
                } else if (info.getCashierPointName().length() > 12) {
                    print.setPrintText("收银点: ");
                    print.setPrintText(info.getCashierPointName());
                } else {
                    print.setPrintText("收银点: " + info.getCashierPointName());
                }
            }

            //开始时间
            print.setPrintText(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime());
            // 结束时间
            print.setPrintText(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime());

            print.feedLine(1);
            // 结算总计
            print.setPrintText(UIUtils.getString(R.string.print_report_total_title));
            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));

            if (info.getType() == 1) {
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));
            } else if (info.getType() == 2) {
               /* print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));*/

                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔"));

                /*print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔"));*/
            } else if (info.getType() == 3) {
                //饭卡消费
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "" : info.getSettleRate() + ""));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"));
                print.setPrintText(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"));

            }
            if (info.getType() == 1) {
                List<ReportBean.DataEntity.ListEntity> list = info.getList();
                if (list != null && list.size() > 0) {
                    print.feedLine(1);
                    print.setPrintText(UIUtils.getString(R.string.print_trade_type_list_title) + ": ");
                    print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));

                    for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                        print.setPrintText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元"));
                        print.setPrintText(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔"));
                    }
                }
            }
            print.setPrintText(UIUtils.getString(R.string.print_single_horizontal));
            print.setPrintText(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
            print.setPrintText(UIUtils.getString(R.string.print_client_sign_title) + "：");

            print.feedLine(5);
            PrinterResult printerResult = print.startPrint(printer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
