package com.hstypay.enterprise.utils.print.ybx;

import android.app.IntentService;
import android.content.Intent;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.hstypay.enterprise.utils.print.CreateTwoDiCodeUtil;


public class PrintPledgeYbxService extends IntentService {

    private PrinterManager printer;

    public PrintPledgeYbxService() {
        super("bill");
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        printer = new PrinterManager();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    int ret = 0;

    @Override
    protected void onHandleIntent(Intent intent) {
        PledgePayBean.DataBean order = (PledgePayBean.DataBean) intent.getSerializableExtra("SPRT");

        if (order == null)
            return;
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                printer.setupPage(384, 1450);
            }
        } else {
            printer.setupPage(384, 1150);
        }
        if (order.isToPay()) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_title_pledge_pay), 115, 0, -1, -1, "arial", 30, 0, 000000, 1);
        } else {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_title_pledge), 115, 0, -1, -1, "arial", 30, 0, 000000, 1);
        }
        LogUtil.d("ret====" + ret);
        ret += printer.drawTextEx(order.getPartner() + "                          请妥善保存", 5, 50, -1, -1, "arial", 24, 0, 000000, 1);
        LogUtil.d("ret====" + ret);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, MyApplication.getMerchantName().toString().length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (MyApplication.getMerchantName().length() > 11) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(MyApplication.getMerchantName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }
        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(UIUtils.getString(R.string.trade_store_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.trade_store_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getStoreMerchantIdCnt(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.trade_store_name) + "：" + order.getStoreMerchantIdCnt(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }
        //交易单号
        try {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
            ret += printer.drawTextEx(OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(order.getOutTransactionId(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }
        if (!TextUtils.isEmpty(order.getAuthNo())) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.tv_pledge_order_no) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(order.getAuthNo(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }

        ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

        if (!order.isToPay()) {
            if (!TextUtils.isEmpty(order.getMoney())) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (order.isToPay()) {
            if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
            if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }

        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
            if (order.getCashierName().length() > 12) {
                ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                StringBuffer sbf = new StringBuffer(order.getCashierName());
                String oneUser = sbf.substring(0, 12);
                ret += printer.drawTextEx(oneUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String twoUser = sbf.substring(12, order.getCashierName().length());
                ret += printer.drawTextEx(twoUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
            if (order.getCashierName().length() > 12) {
                ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getCashierName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(cashierTitle + ": " + order.getCashierName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
            String opUserTitle;
            if (order.isToPay()) {
                opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
            } else {
                opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
            }
            if (order.getOpUserRealName().length() > 16) {
                ret += printer.drawTextEx(opUserTitle, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                String oneUser = sbf.substring(0, 16);
                ret += printer.drawTextEx(oneUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                ret += printer.drawTextEx(twoUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
            if (order.getOpUserRealName().length() > 12) {
                ret += printer.drawTextEx(opUserTitle, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getOpUserRealName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(opUserTitle + order.getOpUserRealName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }

        //备注
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            String str = order.getAttach();
            if (str.length() > 40) {
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 20);
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_attach_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                String two = sbf.substring(20, 40);
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                String three = sbf.substring(40, str.length());
                ret += printer.drawTextEx(three, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
            } else if (str.getBytes().length > 20) {
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 20);
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_attach_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                String two = sbf.substring(20, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_attach_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                ret += printer.drawTextEx(str, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
            }
        }

        ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
        //顾客签名
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_client_sign_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
        }
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);

        if (!TextUtils.isEmpty(order.getAuthNo())) {
            if (!order.isToPay()) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_scan_title), 80, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
            String printCode;
            if (ConfigUtil.printCodeBillEnable()) {
                printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
            } else {
                printCode = order.getAuthNo();
            }
            Bitmap code = CreateTwoDiCodeUtil.createCode(printCode, 280, 280);
            ret += printer.drawBitmap(code, 50, ret) + 280;
        }
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                if (!TextUtils.isEmpty(title)) {
                    if (title.length() > 15) {
                        StringBuffer sbf = new StringBuffer(title);
                        String one = sbf.substring(0, 15);
                        ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                        String two = sbf.substring(15, title.length());
                        ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                    } else {
                        ret += printer.drawTextEx(title, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                    }
                }
                Bitmap activeCode = CreateTwoDiCodeUtil.createCode(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL), 280, 280);
                ret += printer.drawBitmap(activeCode, 50, ret);
            }
        }
        if (!order.isToPay()) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_pledge_notice), 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        }
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(order.isAutoPrint() ? "自动打印" : "手动打印", 115, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret = printer.printPage(0);
        Intent i = new Intent("android.prnt.message");
        i.putExtra("ret", ret);
        this.sendBroadcast(i);
    }

    private void sleep() {
        //延时1秒
        try {
            Thread.currentThread();
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}