/*
 * 文 件 名:  PrintSum.java
 * 描    述:  <描述>
 * 修 改 人:  admin
 * 修改时间:  2017-3-9
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.enterprise.utils.print.ybx;

import android.app.IntentService;
import android.content.Intent;
import android.device.PrinterManager;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.UIUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <一句话功能简述>
 * 打印汇总
 *
 * @author admin
 * @version [版本号, 2017-3-9]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PrintSum extends IntentService {

    private PrinterManager printer;

    public PrintSum() {
        super("bill");
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        printer = new PrinterManager();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    int ret = 0;

    @Override
    protected void onHandleIntent(Intent intent) {
        ReportBean.DataEntity info = (ReportBean.DataEntity) intent.getSerializableExtra("SPRT");

        if (info == null)
            return;

        if (info.getType() == 1) {
            if (info.getList() != null && info.getList().size() > 0) {
                if (info.getList().size() == 1) {
                    printer.setupPage(384, 850);
                } else if (info.getList().size() == 2) {
                    printer.setupPage(384, 920);
                } else if (info.getList().size() == 3) {
                    printer.setupPage(384, 1000);
                } else if (info.getList().size() == 4) {
                    printer.setupPage(384, 1050);
                } else if (info.getList().size() == 5) {
                    printer.setupPage(384, 1150);
                }
            } else {
                printer.setupPage(384, 750);
            }
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_pay_report_title), 135, 0, -1, -1, "arial", 30, 0, 000000, 1);
        } else if (info.getType() == 2) {
            printer.setupPage(384, 600);
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_consume_report_title), 100, 0, -1, -1, "arial", 30, 0, 000000, 1);
        }
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        //商户名称
        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (MyApplication.getMerchantName().length() > 11) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(MyApplication.getMerchantName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        //商户号
        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }

        if (StringUtils.isEmptyOrNull(info.getStoreName())) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.store_name_title) + ": 全部门店", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        } else {
            if (info.getStoreName().length() > 16) {
                String str = info.getStoreName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(UIUtils.getString(R.string.store_name_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (info.getStoreName().length() > 11) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.store_name_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(info.getStoreName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }

        String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
        if (StringUtils.isEmptyOrNull(info.getCashierName())) {
            ret += printer.drawTextEx(cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        } else {
            if (info.getCashierName().length() > 16) {
                String str = info.getCashierName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (info.getCashierName().length() > 12) {
                ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(info.getCashierName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(cashierTitle + ": " + info.getCashierName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
            if (info.getCashierPointName().length() > 16) {
                StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                String one = sbf.substring(0, 16);
                String two = sbf.substring(16, info.getCashierPointName().length());
                ret += printer.drawTextEx("收银点: ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (info.getCashierPointName().length() > 12) {
                ret += printer.drawTextEx("收银点: ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(info.getCashierPointName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx("收银点: " + info.getCashierPointName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }

        ret += printer.drawTextEx(UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        // 结束时间
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_report_total_title), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

        //收款总计
        if (info.getType() == 1) {
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        } else if (info.getType() == 2) {
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }else if (info.getType() == 3){
            //饭卡消费
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate())?"":info.getSettleRate()+""), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }
        if (info.getType() == 1) {
            if (info.getSuccessCount() > 0) {
                ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_type_list_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

                List<ReportBean.DataEntity.ListEntity> list = info.getList();
                if (list != null && list.size() > 0) {
                    for (ReportBean.DataEntity.ListEntity itemInfo : list) {
                        ret += printer.drawTextEx(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                        ret += printer.drawTextEx(PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔"), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    }
                }
            }
        }
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_client_sign_title) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

        ret = printer.printPage(0);
        Intent i = new Intent("android.prnt.message");
        i.putExtra("ret", ret);
        this.sendBroadcast(i);
    }

    private void sleep() {
        //延时1秒
        try {
            Thread.currentThread();
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
