package com.hstypay.enterprise.utils.print.ybx;

import android.app.IntentService;
import android.content.Intent;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.print.CreateTwoDiCodeUtil;


public class PrintYbxService extends IntentService {

    private PrinterManager printer;

    public PrintYbxService() {
        super("bill");
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        printer = new PrinterManager();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    int ret = 0;

    @Override
    protected void onHandleIntent(Intent intent) {
        TradeDetailBean order = (TradeDetailBean) intent.getSerializableExtra("SPRT");

        if (order == null)
            return;
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                if (order.isPay()) {
                    if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                        printer.setupPage(384, 1450);
                    } else {
                        printer.setupPage(384, 1350);
                    }
                } else {
                    printer.setupPage(384, 1450);
                }
            }
        } else {
            if (order.isPay()) {
                if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                    printer.setupPage(384, 1050);
                } else {
                    printer.setupPage(384, 1000);
                }
            } else {
                printer.setupPage(384, 1150);
            }
        }
        LogUtil.d("otherType===" + order.isPay());
        if (order.getTradeType() == 1) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_recharge_title), 115, 0, -1, -1, "arial", 30, 0, 000000, 1);
        } else if (order.getTradeType() == 2) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_verification_title), 115, 0, -1, -1, "arial", 30, 0, 000000, 1);
        } else {
            if (!order.isPay()) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_refund_title), 115, 0, -1, -1, "arial", 30, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_pay_title), 115, 0, -1, -1, "arial", 30, 0, 000000, 1);
            }
        }
        LogUtil.d("ret====" + ret);
        ret += printer.drawTextEx(order.getPartner() + "                          请妥善保存", 5, 50, -1, -1, "arial", 24, 0, 000000, 1);
        LogUtil.d("ret====" + ret);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
            if (MyApplication.getMerchantName().length() > 16) {
                String str = MyApplication.getMerchantName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, MyApplication.getMerchantName().toString().length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (MyApplication.getMerchantName().length() > 11) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(MyApplication.getMerchantName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }

        String storeNameTitle;
        if (order.isPay()) {
            storeNameTitle = UIUtils.getString(R.string.trade_store_name);
        } else {
            storeNameTitle = UIUtils.getString(R.string.refund_store_name);
        }
        if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
            if (order.getStoreMerchantIdCnt().length() > 16) {
                String str = order.getStoreMerchantIdCnt();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx(storeNameTitle + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (order.getStoreMerchantIdCnt().length() > 11) {
                ret += printer.drawTextEx(storeNameTitle + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getStoreMerchantIdCnt(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx(storeNameTitle + "：" + order.getStoreMerchantIdCnt(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
            if (order.getCashPointName().length() > 16) {
                String str = order.getCashPointName();
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 16);
                ret += printer.drawTextEx("收银点: ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String two = sbf.substring(16, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else if (order.getCashPointName().length() > 11) {
                ret += printer.drawTextEx("收银点: ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getCashPointName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } else {
                ret += printer.drawTextEx("收银点: " + order.getCashPointName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
        }
        if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
            ret += printer.drawTextEx(ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }
        if (!order.isPay()) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_refund_no_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(order.getRefundNo(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_refund_time_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(order.getRefundTime(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);

            String refundUser = UIUtils.getString(R.string.print_refund_user_title);
            if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                if (order.getRefundUser().length() > 16) {
                    ret += printer.drawTextEx(refundUser + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    StringBuffer sbf = new StringBuffer(order.getRefundUser());
                    String user = sbf.substring(0, 16);
                    ret += printer.drawTextEx(user, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    String printUser = sbf.substring(16, order.getRefundUser().length());
                    ret += printer.drawTextEx(printUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                } else if (order.getRefundUser().length() > 12) {
                    ret += printer.drawTextEx(refundUser + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    ret += printer.drawTextEx(order.getRefundUser(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                } else {
                    ret += printer.drawTextEx(refundUser + ": " + order.getRefundUser(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                if (order.getRefundUserRealName().length() > 16) {
                    ret += printer.drawTextEx(refundUser + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                    String user = sbf.substring(0, 16);
                    ret += printer.drawTextEx(user, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                    ret += printer.drawTextEx(printuser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                } else if (order.getRefundUserRealName().length() > 12) {
                    ret += printer.drawTextEx(refundUser + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    ret += printer.drawTextEx(order.getRefundUserRealName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                } else {
                    ret += printer.drawTextEx(refundUser + ": " + order.getRefundUserRealName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                }
            }
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            if (order.getRefundMoney() > 0) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_refund_instruction), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        } else {
            //交易单号
            if (!TextUtils.isEmpty(order.getTransactionId())) {
                ret += printer.drawTextEx(OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getTransactionId(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            }
            if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.tv_print_order_no) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                ret += printer.drawTextEx(order.getOrderNo(), 5, ret, -1, -1, "arial", 22, 0, 000000, 1);
            }
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider()), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            try {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (order.getCashierName().length() > 16) {
                    ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    StringBuffer sbf = new StringBuffer(order.getCashierName());
                    String oneUser = sbf.substring(0, 16);
                    ret += printer.drawTextEx(oneUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    String twoUser = sbf.substring(16, order.getCashierName().length());
                    ret += printer.drawTextEx(twoUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                }
                if (order.getCashierName().length() > 12) {
                    ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    ret += printer.drawTextEx(order.getCashierName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                } else {
                    ret += printer.drawTextEx(cashierTitle + ": " + order.getCashierName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                if (order.getOpUserRealName().length() > 16) {
                    ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                    String oneUser = sbf.substring(0, 16);
                    ret += printer.drawTextEx(oneUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                    ret += printer.drawTextEx(twoUser, 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                }
                if (order.getOpUserRealName().length() > 12) {
                    ret += printer.drawTextEx(cashierTitle + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                    ret += printer.drawTextEx(order.getOpUserRealName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                } else {
                    ret += printer.drawTextEx(cashierTitle + ": " + order.getOpUserRealName(), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                }
            }
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_order_money_title) + "：" + DateUtil.formatMoneyUtils(order.getMoney()) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元", 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
        }

        //备注
        if (!StringUtils.isEmptyOrNull(order.getAttach())) {
            String str = order.getAttach();
            if (str.length() > 40) {
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 20);
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_attach_title) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                String two = sbf.substring(20, 40);
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                String three = sbf.substring(40, str.length());
                ret += printer.drawTextEx(three, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
            } else if (str.getBytes().length > 20) {
                StringBuffer sbf = new StringBuffer(str);
                String one = sbf.substring(0, 20);
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_attach_title) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                String two = sbf.substring(20, str.length());
                ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
            } else {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_trade_attach_title) + "：", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                ret += printer.drawTextEx(str, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
            }
        }

        ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
        ret += printer.drawTextEx(UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
        //顾客签名
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
            ret += printer.drawTextEx(UIUtils.getString(R.string.print_client_sign_title) + ": ", 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
        }
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);

        if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
            String printCode;
            if (ConfigUtil.printCodeBillEnable()) {
                printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            } else {
                printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
            }
            ret += printer.drawTextEx(ConfigUtil.getPrintCodeTitle(), 80, ret, -1, -1, "arial", 24, 0, 000000, 1);
            Bitmap code = CreateTwoDiCodeUtil.createCode(printCode, 280, 280);
            ret += printer.drawBitmap(code, 50, ret) + 280;
        }
        if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_user_stub))) {
            if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                    && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                ret += printer.drawTextEx(UIUtils.getString(R.string.print_ybx_single_horizontal), 5, ret, -1, -1, "arial", 24, 0, 000000, 1);
                String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                if (!TextUtils.isEmpty(title)) {
                    if (title.length() > 15) {
                        StringBuffer sbf = new StringBuffer(title);
                        String one = sbf.substring(0, 15);
                        ret += printer.drawTextEx(one, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                        String two = sbf.substring(15, title.length());
                        ret += printer.drawTextEx(two, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                    } else {
                        ret += printer.drawTextEx(title, 5, ret, -1, -1, "arial", 24, 0, 000000, 0);
                    }
                }
                Bitmap activeCode = CreateTwoDiCodeUtil.createCode(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL), 280, 280);
                ret += printer.drawBitmap(activeCode, 50, ret);
            }
        }
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(order.isAutoPrint() ? "自动打印" : "手动打印", 115, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret += printer.drawTextEx(" ", 5, ret, -1, -1, "arial", 20, 0, 000000, 1);
        ret = printer.printPage(0);
        Intent i = new Intent("android.prnt.message");
        i.putExtra("ret", ret);
        this.sendBroadcast(i);
    }

    private void sleep() {
        //延时1秒
        try {
            Thread.currentThread();
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}