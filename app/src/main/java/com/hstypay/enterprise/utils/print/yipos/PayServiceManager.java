package com.hstypay.enterprise.utils.print.yipos;

import android.os.IBinder;

import com.huiyi.nypos.pay.thirdpay.aidl.AidlPayService;


public class PayServiceManager {
	
	private static AidlPayService mServiceImp = null;
	
	public static void init(IBinder binder){
		mServiceImp = AidlPayService.Stub.asInterface(binder);
	}
	
	public static AidlPayService getInstance(){
		return mServiceImp;
	}
}
