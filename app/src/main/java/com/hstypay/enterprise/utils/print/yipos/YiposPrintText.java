package com.hstypay.enterprise.utils.print.yipos;

import android.graphics.Bitmap;
import android.os.RemoteException;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.Widget.MyToast;
import com.hstypay.enterprise.app.MyApplication;
import com.hstypay.enterprise.bean.PledgePayBean;
import com.hstypay.enterprise.bean.PledgeReportBean;
import com.hstypay.enterprise.bean.ReportBean;
import com.hstypay.enterprise.bean.TradeDetailBean;
import com.hstypay.enterprise.utils.ConfigUtil;
import com.hstypay.enterprise.utils.Constants;
import com.hstypay.enterprise.utils.DateUtil;
import com.hstypay.enterprise.utils.OrderStringUtil;
import com.hstypay.enterprise.utils.PrintFormatUtils;
import com.hstypay.enterprise.utils.SpUtil;
import com.hstypay.enterprise.utils.StringUtils;
import com.hstypay.enterprise.utils.ToastHelper;
import com.hstypay.enterprise.utils.UIUtils;
import com.hstypay.enterprise.utils.Utils;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlPrint;
import com.huiyi.nypos.pay.thirdpay.aidl.AidlThirdListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.utils.print.yipos
 * @创建者: Jeremy
 * @创建时间: 2018/11/21 14:44
 * @描述: ${TODO}
 */
public class YiposPrintText {

    private static final String POSITION_CENTER = "center";
    private static final String POSITION_LEFT = "left";
    private static final String POSITION_RIGHT = "right";
    private static final String TYPE_TEXT = "txt";//文本
    private static final String TYPE_ONECODE = "one-dimension";//一维码
    private static final String TYPE_QRCODE = "two-dimension";//二维码
    private static YiposPrintText singleton;
    private int count = 1;

    public static YiposPrintText with() {
        if (singleton == null) {
            synchronized (YiposPrintText.class) {
                if (singleton == null) {
                    singleton = new YiposPrintText();
                }
            }
        }
        return singleton;
    }

    private JSONObject printFormat(String size, String content) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("content-type", TYPE_TEXT); // 打印字符串
        item.put("size", size); // 打印文字大小
        item.put("position", POSITION_LEFT); // 打印位置 left center right
        item.put("content", content); // 打印内容
        return item;
    }

    private JSONObject printFormat(String contentType, String size, String position, int bold, String content) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("content-type", contentType); // 打印字符串
        item.put("size", size); // 打印文字大小
        item.put("position", position); // 打印位置 left center right
        if (bold != 0) {
            item.put("bold", bold);
        }
        item.put("content", content); // 打印内容
        return item;
    }

    /**
     * 自定义打印小票
     *
     * @return
     */
    public String printJson(TradeDetailBean order) {
        JSONObject json = new JSONObject();
        JSONArray list = new JSONArray();
        try {

            if (order.getTradeType() == 1) {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_recharge_title)));
            } else if (order.getTradeType() == 2) {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_verification_title)));
            } else {
                if (!order.isPay()) {
                    list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_refund_title)));
                } else {
                    list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_pay_title)));
                }
            }

            list.put(printFormat("2", ""));
            list.put(printFormat("2", order.getPartner() + "              " + "请妥善保存"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 15) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (MyApplication.getMerchantName().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", MyApplication.getMerchantName()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName()));
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId()));
            }
            String storeNameTitle;
            if (order.isPay()) {
                storeNameTitle = UIUtils.getString(R.string.trade_store_name);
            } else {
                storeNameTitle = UIUtils.getString(R.string.refund_store_name);
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", storeNameTitle + "："));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    list.put(printFormat("2", storeNameTitle + "："));
                    list.put(printFormat("2", order.getStoreMerchantIdCnt()));
                } else {
                    list.put(printFormat("2", storeNameTitle + "：" + order.getStoreMerchantIdCnt()));
                }
            }
            if (!StringUtils.isEmptyOrNull(order.getCashPointName())) {
                if (order.getCashPointName().length() > 16) {
                    String str = order.getCashPointName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", "收银点: "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (order.getCashPointName().length() > 11) {
                    list.put(printFormat("2", "收银点: "));
                    list.put(printFormat("2", order.getCashPointName()));
                } else {
                    list.put(printFormat("2", "收银点: " + order.getCashPointName()));
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getDeviceSn()))) {
                list.put(printFormat("2", ToastHelper.toStr(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getDeviceSn())));
            }
            if (!order.isPay()) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_refund_no_title) + ": "));
                list.put(printFormat("2", order.getRefundNo()));
                list.put(printFormat("2", UIUtils.getString(R.string.print_refund_time_title) + ": "));
                list.put(printFormat("2", order.getRefundTime()));

                String refundUser = UIUtils.getString(R.string.print_refund_user_title);
                if (!StringUtils.isEmptyOrNull(order.getRefundUser())) {
                    if (order.getRefundUser().length() > 16) {
                        list.put(printFormat("2", refundUser + ": "));
                        StringBuffer sbf = new StringBuffer(order.getRefundUser());
                        String user = sbf.substring(0, 16);
                        list.put(printFormat("2", user));
                        String printuser = sbf.substring(16, order.getRefundUser().length());
                        list.put(printFormat("2", printuser));
                    } else if (order.getRefundUser().length() > 12) {
                        list.put(printFormat("2", refundUser + ": "));
                        list.put(printFormat("2", order.getRefundUser()));
                    } else {
                        list.put(printFormat("2", refundUser + ": " + order.getRefundUser()));
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getRefundUserRealName())) {
                    if (order.getRefundUserRealName().length() > 16) {
                        list.put(printFormat("2", refundUser + ": "));
                        StringBuffer sbf = new StringBuffer(order.getRefundUserRealName());
                        String user = sbf.substring(0, 16);
                        list.put(printFormat("2", user));
                        String printuser = sbf.substring(16, order.getRefundUserRealName().length());
                        list.put(printFormat("2", printuser));
                    } else if (order.getRefundUserRealName().length() > 12) {
                        list.put(printFormat("2", refundUser + ": "));
                        list.put(printFormat("2", order.getRefundUserRealName()));
                    } else {
                        list.put(printFormat("2", refundUser + ": " + order.getRefundUserRealName()));
                    }
                }
                list.put(printFormat("2", UIUtils.getString(R.string.print_refund_status_title) + ": " + OrderStringUtil.getRefundStateString(order.getRefundStatus())));
                if (order.getRefundMoney() > 0) {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_refund_money_title) + ": " + DateUtil.formatMoneyUtils(order.getRefundMoney()) + "元"));
                }
                list.put(printFormat("2", UIUtils.getString(R.string.print_refund_instruction)));
            } else {
                if (!StringUtils.isEmptyOrNull(order.getTransactionId())) {
                    list.put(printFormat("2", OrderStringUtil.getTradeTypeTitleString(order.getApiProvider()) + ": "));
                    list.put(printFormat("2", order.getTransactionId()));
                }
                if (!StringUtils.isEmptyOrNull(order.getOrderNo())) {
                    list.put(printFormat("2", UIUtils.getString(R.string.tv_print_order_no) + ": "));
                    list.put(printFormat("2", order.getOrderNo()));
                }
                list.put(printFormat("2", UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getTradeStateString(order.getTradeState())));
                list.put(printFormat("2", UIUtils.getString(R.string.print_trade_type_title) + "：" + OrderStringUtil.getTradeTypeString(order.getApiProvider())));
                try {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_trade_time_title) + ": " + order.getTradeTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        list.put(printFormat("2", cashierTitle + ": "));
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String user = sbf.substring(0, 16);
                        list.put(printFormat("2", user));
                        String printUser = sbf.substring(16, order.getCashierName().length());
                        list.put(printFormat("2", printUser));
                    } else if (order.getCashierName().length() > 12) {
                        list.put(printFormat("2", cashierTitle + ": "));
                        list.put(printFormat("2", order.getCashierName()));
                    } else {
                        list.put(printFormat("2", cashierTitle + ": " + order.getCashierName()));
                    }
                } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        list.put(printFormat("2", cashierTitle + ": "));
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String user = sbf.substring(0, 16);
                        list.put(printFormat("2", user));
                        String printUser = sbf.substring(16, order.getOpUserRealName().length());
                        list.put(printFormat("2", printUser));
                    } else if (order.getOpUserRealName().length() > 12) {
                        list.put(printFormat("2", cashierTitle + ": "));
                        list.put(printFormat("2", order.getOpUserRealName()));
                    } else {
                        list.put(printFormat("2", cashierTitle + ": " + order.getOpUserRealName()));
                    }
                    list.put(printFormat("2", cashierTitle + ": " + order.getOpUserRealName()));
                }
                list.put(printFormat("2", UIUtils.getString(R.string.print_order_money_title) + ": " + DateUtil.formatMoneyUtils(order.getMoney()) + "元"));
                list.put(printFormat("2", UIUtils.getString(R.string.print_coupon_money_title) + "：-" + DateUtil.formatMoneyUtils(order.getCouponFee()) + "元"));
                list.put(printFormat("2", UIUtils.getString(R.string.print_real_money_title) + "：" + DateUtil.formatMoneyUtils(order.getRealMoney()) + "元"));
            }

            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_trade_attach_title) + "：" + order.getAttach()));
            }
            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
            list.put(printFormat("2", UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis())));
            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_client_sign_title) + "："));
            }
            list.put(printFormat("2", ""));
            if (!TextUtils.isEmpty(order.getOrderNo()) || !TextUtils.isEmpty(order.getRefundNo())) {
                list.put(printFormat("2", PrintFormatUtils.printCenterData(ConfigUtil.getPrintCodeTitle())));
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                } else {
                    printCode = (order.isPay() ? order.getOrderNo() : order.getRefundNo());
                }
                list.put(printFormat(TYPE_QRCODE, "260", POSITION_CENTER, 0, printCode));
            }

            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        list.put(printFormat("2", title));
                    }
                    list.put(printFormat(TYPE_QRCODE, "260", POSITION_CENTER, 0, activeUrl));
                }
            }
            list.put(printFormat("2", ""));
            list.put(printFormat(TYPE_TEXT, "2", POSITION_CENTER, 0, (order.isAutoPrint() ? "自动打印" : "手动打印")));
            json.put("spos", list);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * 自定义打印小票
     *
     * @return
     */
    public String pledgePrintJson(PledgePayBean.DataBean order) {
        JSONObject json = new JSONObject();
        JSONArray list = new JSONArray();
        try {
            if (order.isToPay()) {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_title_pledge_pay)));
            } else {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_title_pledge)));
            }

            list.put(printFormat("2", ""));
            list.put(printFormat("2", order.getPartner() + "              " + "请妥善保存"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 15) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (MyApplication.getMerchantName().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", MyApplication.getMerchantName()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName()));
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId()));
            }
            if (!StringUtils.isEmptyOrNull(order.getStoreMerchantIdCnt())) {
                if (order.getStoreMerchantIdCnt().length() > 16) {
                    String str = order.getStoreMerchantIdCnt();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.trade_store_name) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (order.getStoreMerchantIdCnt().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.trade_store_name) + ": "));
                    list.put(printFormat("2", order.getStoreMerchantIdCnt()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.trade_store_name) + ": " + order.getStoreMerchantIdCnt()));
                }
            }
            if (!StringUtils.isEmptyOrNull(StringUtils.getDeviceNo(order.getTermNo()))) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_title_device_info) + "：" + StringUtils.getDeviceNo(order.getTermNo())));
            }
            try {
                list.put(printFormat("2", UIUtils.getString(R.string.print_trade_time_title) + "：" + order.getTradeFinishTime()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!order.isToPay() && !TextUtils.isEmpty(order.getOutTransactionId())) {
                list.put(printFormat("2", OrderStringUtil.getPledgeTypeTitleString(order.getApiProvider()) + "："));
                list.put(printFormat("2", order.getOutTransactionId()));
            }
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                list.put(printFormat("2", UIUtils.getString(R.string.tv_pledge_order_no) + ": "));
                list.put(printFormat("2", order.getAuthNo()));
            }

            list.put(printFormat("2", UIUtils.getString(R.string.print_trade_status_title) + "：" + OrderStringUtil.getPledgeStateString(order.getTradeStatus())));

            if (!order.isToPay()) {
                if (!TextUtils.isEmpty(order.getMoney())) {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getMoney(), 0)) + "元"));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_freeze_money_title) + ": 0.00元"));
                }
            }
            if (order.isToPay()) {
                if (!TextUtils.isEmpty(order.getSumPayMoney())) {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_trade_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumPayMoney(), 0)) + "元"));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_trade_money_title) + ": 0.00元"));
                }
                if (!TextUtils.isEmpty(order.getSumFreeMoney())) {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_refund_money_title) + ": " + DateUtil.formatMoneyUtils(Utils.Long.tryParse(order.getSumFreeMoney(), 0)) + "元"));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_refund_money_title) + ": 0.00元"));
                }
            }

            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                if (!StringUtils.isEmptyOrNull(order.getCashierName())) {
                    if (order.getCashierName().length() > 16) {
                        list.put(printFormat("2", cashierTitle + ": "));
                        StringBuffer sbf = new StringBuffer(order.getCashierName());
                        String oneUser = sbf.substring(0, 16);
                        list.put(printFormat("2", oneUser));
                        String twoUser = sbf.substring(16, order.getCashierName().length());
                        list.put(printFormat("2", twoUser));
                    }
                    if (order.getCashierName().length() > 12) {
                        list.put(printFormat("2", cashierTitle + ": "));
                        list.put(printFormat("2", order.getCashierName()));
                    } else {
                        list.put(printFormat("2", cashierTitle + ": " + order.getCashierName()));
                    }
                }
            } else if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                String opUserTitle;
                if (order.isToPay()) {
                    opUserTitle = UIUtils.getString(R.string.print_pledge_operator_title) + ": ";
                } else {
                    opUserTitle = UIUtils.getString(R.string.print_cashier_title) + ": ";
                }
                if (!StringUtils.isEmptyOrNull(order.getOpUserRealName())) {
                    if (order.getOpUserRealName().length() > 16) {
                        list.put(printFormat("2", opUserTitle));
                        StringBuffer sbf = new StringBuffer(order.getOpUserRealName());
                        String oneUser = sbf.substring(0, 16);
                        list.put(printFormat("2", oneUser));
                        String twoUser = sbf.substring(16, order.getOpUserRealName().length());
                        list.put(printFormat("2", twoUser));
                    }
                    if (order.getOpUserRealName().length() > 12) {
                        list.put(printFormat("2", opUserTitle));
                        list.put(printFormat("2", order.getOpUserRealName()));
                    } else {
                        list.put(printFormat("2", opUserTitle + order.getOpUserRealName()));
                    }
                }
            }
            //备注
            if (!StringUtils.isEmptyOrNull(order.getAttach())) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_attach_title) + ": " + order.getAttach()));
            }
            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
            list.put(printFormat("2", UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis())));

            //顾客签名
            if (order.getPartner().equals(UIUtils.getString(R.string.tv_pay_mch_stub))) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_client_sign_title) + ": "));
            }
            list.put(printFormat("2", ""));
            if (!TextUtils.isEmpty(order.getAuthNo())) {
                if (!order.isToPay()) {
                    list.put(printFormat("2", "       " + UIUtils.getString(R.string.print_pledge_scan_title)));
                }
                String printCode;
                if (ConfigUtil.printCodeBillEnable()) {
                    printCode = Constants.URL_PRINT_QRCODE + order.getAuthNo();
                } else {
                    printCode = order.getAuthNo();
                }
                list.put(printFormat(TYPE_QRCODE, "260", POSITION_CENTER, 0, printCode));
            }

            if (order.getPartner().equals(
                    UIUtils.getString(R.string.tv_pay_user_stub))) {
                if (SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_ENABLE)
                        && !TextUtils.isEmpty(SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL))) {
                    list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
                    String title = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_TITLE);
                    String activeUrl = SpUtil.getString(MyApplication.getContext(), Constants.SP_PRINT_ACTIVE_URL);
                    if (!TextUtils.isEmpty(title)) {
                        list.put(printFormat("2", title));
                    }
                    list.put(printFormat(TYPE_QRCODE, "260", POSITION_CENTER, 0, activeUrl));
                }
            }
            if (!order.isToPay()) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
                list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_notice)));
            }
            list.put(printFormat("2", ""));
            list.put(printFormat(TYPE_TEXT, "2", POSITION_CENTER, 0, (order.isAutoPrint() ? "自动打印" : "手动打印")));
            json.put("spos", list);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public String printJsonSum(ReportBean.DataEntity info) {
        JSONObject json = new JSONObject();
        JSONArray list = new JSONArray();
        try {
            if (info.getType() == 1) {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_pay_report_title)));
            } else if (info.getType() == 2) {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_consume_report_title)));
            } else if (info.getType() == 3) {
                list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_fk_consume_report_title)));
            }
            list.put(printFormat("2", ""));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (MyApplication.getMerchantName().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", MyApplication.getMerchantName()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName()));
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId()));
            }
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": " + "全部门店"));
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (info.getStoreName().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": "));
                    list.put(printFormat("2", info.getStoreName()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName()));
                }
            }
            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                list.put(printFormat("2", cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", cashierTitle + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (info.getCashierName().length() > 12) {
                    list.put(printFormat("2", cashierTitle + ": "));
                    list.put(printFormat("2", info.getCashierName()));
                } else {
                    list.put(printFormat("2", cashierTitle + ": " + info.getCashierName()));
                }
            }
            if (!StringUtils.isEmptyOrNull(info.getCashierPointName())) {
                if (info.getCashierPointName().length() > 16) {
                    StringBuffer sbf = new StringBuffer(info.getCashierPointName());
                    String one = sbf.substring(0, 16);
                    String two = sbf.substring(16, info.getCashierPointName().length());
                    list.put(printFormat("2", "收银点: "));
                    list.put(printFormat("2", one));
                    list.put(printFormat("2", two));
                } else if (info.getCashierPointName().length() > 12) {
                    list.put(printFormat("2", "收银点: "));
                    list.put(printFormat("2", info.getCashierPointName()));
                } else {
                    list.put(printFormat("2", "收银点: " + info.getCashierPointName()));
                }
            }

            list.put(printFormat("2", UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime()));

            // 结束时间
            list.put(printFormat("2", UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime()));

            list.put(printFormat("2", ""));
            list.put(printFormat("2", UIUtils.getString(R.string.print_report_total_title)));
            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));

            if (info.getType() == 1) {
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_net_income_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_real_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔")));
            } else if (info.getType() == 2) {
                /*list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_total_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee() - info.getRefundFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_merchant_coupon_title) + "：", DateUtil.formatMoneyUtils(info.getMchDiscountsFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔")));*/

                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_money_title) + "：", DateUtil.formatMoneyUtils(info.getPayFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_count_title) + "：", info.getSuccessCount() + "笔")));

                /*list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_consume_card_count_title) + "：", info.getNumberPaymentCount() + "次")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_money_title) + "：", DateUtil.formatMoneyUtils(info.getRechargeAmount()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_recharge_count_title) + "：", info.getRechargeNumber() + "笔")));*/
            } else if (info.getType() == 3) {
                //饭卡消费
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_money_title) + "：", DateUtil.formatMoneyUtils(info.getSuccessFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_money_title) + "：", DateUtil.formatMoneyUtils(info.getRefundFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_rate_title) + "：", TextUtils.isEmpty(info.getSettleRate()) ? "" : info.getSettleRate() + "")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_fk_settlement_money_title) + "：", DateUtil.formatMoneyUtils(info.getSettlementFee()) + "元")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_trade_count_title) + "：", info.getSuccessCount() + "笔")));
                list.put(printFormat("2", PrintFormatUtils.printTwoData(UIUtils.getString(R.string.print_refund_count_title) + "：", info.getRefundCount() + "笔")));

            }
            if (info.getType() == 1) {
                List<ReportBean.DataEntity.ListEntity> listInfo = info.getList();
                if (listInfo != null && listInfo.size() > 0) {
                    list.put(printFormat("2", ""));
                    list.put(printFormat("2", UIUtils.getString(R.string.print_trade_type_list_title) + ": "));
                    list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));

                    for (ReportBean.DataEntity.ListEntity itemInfo : listInfo) {
                        list.put(printFormat("2", PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "金额：", DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "元")));
                        list.put(printFormat("2", PrintFormatUtils.printTwoData(OrderStringUtil.getTradeTypeString(itemInfo.getApiProvider()) + "笔数：", itemInfo.getSuccessCount() + "笔")));
                    }
                }
            }
            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
            list.put(printFormat("2", UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis())));
            list.put(printFormat("2", UIUtils.getString(R.string.print_client_sign_title) + "："));
            json.put("spos", list);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public String printJsonPledgeSum(PledgeReportBean.DataBean info) {
        JSONObject json = new JSONObject();
        JSONArray list = new JSONArray();
        try {
            list.put(printFormat(TYPE_TEXT, "3", POSITION_CENTER, 1, UIUtils.getString(R.string.print_title_pledge_report)));
            list.put(printFormat("2", ""));

            if (!StringUtils.isEmptyOrNull(MyApplication.getMerchantName())) {
                if (MyApplication.getMerchantName().length() > 16) {
                    String str = MyApplication.getMerchantName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (MyApplication.getMerchantName().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + ": "));
                    list.put(printFormat("2", MyApplication.getMerchantName()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.shop_name) + "：" + MyApplication.getMerchantName()));
                }
            }
            if (!StringUtils.isEmptyOrNull(MyApplication.getMechantId())) {
                list.put(printFormat("2", UIUtils.getString(R.string.print_title_merchant_id) + "：" + MyApplication.getMechantId()));
            }
            if (StringUtils.isEmptyOrNull(info.getStoreName())) {
                list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": 全部门店"));
            } else {
                if (info.getStoreName().length() > 16) {
                    String str = info.getStoreName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (info.getStoreName().length() > 11) {
                    list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": "));
                    list.put(printFormat("2", info.getStoreName()));
                } else {
                    list.put(printFormat("2", UIUtils.getString(R.string.store_name_title) + ": " + info.getStoreName()));
                }
            }

            String cashierTitle = UIUtils.getString(R.string.print_cashier_title);
            if (StringUtils.isEmptyOrNull(info.getCashierName())) {
                list.put(printFormat("2", cashierTitle + ": "+UIUtils.getString(R.string.tv_all_user)));
            } else {
                if (info.getCashierName().length() > 16) {
                    String str = info.getCashierName();
                    StringBuffer sbf = new StringBuffer(str);
                    String one = sbf.substring(0, 16);
                    list.put(printFormat("2", cashierTitle + ": "));
                    list.put(printFormat("2", one));
                    String two = sbf.substring(16, str.length());
                    list.put(printFormat("2", two));
                } else if (info.getCashierName().length() > 12) {
                    list.put(printFormat("2", cashierTitle + ": "));
                    list.put(printFormat("2", info.getCashierName()));
                } else {
                    list.put(printFormat("2", cashierTitle + ": " + info.getCashierName()));
                }
            }
            list.put(printFormat("2", UIUtils.getString(R.string.print_start_time_title) + ": " + info.getStartTime()));

            // 结束时间
            list.put(printFormat("2", UIUtils.getString(R.string.print_end_time_title) + ": " + info.getEndTime()));

            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));

            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPreMoney()) + "元"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_count_title) + "：" + info.getCntPreMoney() + "笔"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_pay_money_title) + "：" + DateUtil.formatMoneyUtils(info.getSumPayMoney()) + "元"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_pay_count_title) + "：" + info.getCntPayMoney() + "笔"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumFreeMoney()) + "元"));
            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_refund_count) + ":" + info.getCntFreeMoney() + "笔"));
//            list.put(printFormat("2", UIUtils.getString(R.string.print_pledge_refund_money) + "：" + DateUtil.formatMoneyUtils(info.getSumNotFreeMoney()) + "元"));

            list.put(printFormat("2", UIUtils.getString(R.string.print_single_horizontal)));
            list.put(printFormat("2", UIUtils.getString(R.string.print_time_title) + "：" + DateUtil.formatTime(System.currentTimeMillis())));
            list.put(printFormat("2", UIUtils.getString(R.string.print_client_sign_title) + "："));
            list.put(printFormat("2", ""));
            json.put("spos", list);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public void doPrint(AidlPrint aidlPrint, String jsonStr, Bitmap[] bitmaps) {
        try {
            aidlPrint.print(jsonStr, bitmaps, new AidlThirdListener.Stub() {
                @Override
                public void thirdSuccess(final String arg0) throws RemoteException {
                    MyToast.showToastShort(arg0);
                }

                @Override
                public void thirdFailed(String arg0, String arg1)
                        throws RemoteException {
                    MyToast.showToastShort(arg0 + "--" + arg1);
                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
