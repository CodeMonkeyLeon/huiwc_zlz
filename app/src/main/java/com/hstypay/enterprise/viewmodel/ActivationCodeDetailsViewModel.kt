package com.hstypay.enterprise.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.hstypay.enterprise.bean.ActivationCode
import com.hstypay.enterprise.bean.CashierItem
import com.hstypay.enterprise.bean.LinkEmployeeBean
import com.hstypay.enterprise.commonlib.base.BaseViewModel
import com.hstypay.enterprise.commonlib.http.ApiManager

/**
 * @Author dean.zeng
 * @Description PC收银激活码详情
 * @Date 2020-07-17 19:15
 **/
class ActivationCodeDetailsViewModel : BaseViewModel() {

    val activationCode = MutableLiveData<ActivationCode>()
    val cashierList = MutableLiveData<ArrayList<CashierItem>>()
    val upData= MutableLiveData<String>()
    /**
     * 获取详情
     */
    fun getActivationCodeDetails(id: Long) {
        requestSync({ ApiManager.get().getService().getActivationCodeDetails(id) }, {
            activationCode.postValue(it)
        })
    }

    /**
     * 修改门店信息
     */
    fun activationCodeUpdate(id: Long, storeMerchantShortName: String) {
        requestSync({ ApiManager.get().getService().activationCodeUpdate(id, storeMerchantShortName) }, {
            upData.postValue(storeMerchantShortName)
        })
    }

    /**
     * 获取收银员
     */
    fun getCashierList(storeMerchantId: String) {
        requestSync({ ApiManager.get().getService().getCashierList(storeMerchantId) }, {
            var list = arrayListOf<LinkEmployeeBean.DataEntity>()
            cashierList.postValue(ArrayList(it))
        })
    }

}