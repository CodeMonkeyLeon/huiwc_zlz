package com.hstypay.enterprise.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.hstypay.enterprise.commonlib.base.BaseViewModel
import com.hstypay.enterprise.commonlib.http.ApiManager

/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2020-07-17 15:52
 **/
class CreateActivationCodeViewMode : BaseViewModel() {
    val successLiveData = MutableLiveData<Any>()

    fun addActivationCode(storeMerchantId: String, storeMerchantShortName: String) {
        requestSync({ ApiManager.get().getService().addActivationCode(storeMerchantId, storeMerchantShortName) }, {
            successLiveData.postValue(it)
        })
    }

}