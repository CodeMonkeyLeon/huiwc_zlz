package com.hstypay.enterprise.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.hstypay.enterprise.bean.ActivationCode
import com.hstypay.enterprise.bean.StoreList
import com.hstypay.enterprise.commonlib.base.BaseViewModel
import com.hstypay.enterprise.commonlib.http.ApiManager
import com.hstypay.enterprise.commonlib.http.HttpResponse
import retrofit2.http.Field

/**
 * @Author dean.zeng
 * @Description PC收银激活码管理
 * @Date 2020-07-16 16:57
 **/
class PCCashierSnManageViewModel : BaseViewModel() {
    val storeList = MutableLiveData<List<StoreList>>()
    val activationCodeList = MutableLiveData<List<ActivationCode>>()

    /*
    门店列表
     */
    fun findStores() {
        requestSync({ ApiManager.get().getService().findStores("10000", "1", "1") },
                {
                    storeList.postValue(it)
                })
    }

    /*
    获取激活码列表
     */
    fun getActivationData(pageSize: String, currentPage: String,
                          activationStatus: String,
                          storeMerchantId: String) {
        requestSync({
            ApiManager.get().getService().activationCodeList(pageSize, currentPage,
                    activationStatus, storeMerchantId)
        }, {
            if (it!=null)
            activationCodeList.postValue(it.data)
        },{
            activationCodeList.postValue(null)
        })
    }


}