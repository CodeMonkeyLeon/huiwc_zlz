package com.hstypay.enterprise.voice;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;

import com.hstypay.enterprise.R;
import com.hstypay.enterprise.utils.AppHelper;
import com.hstypay.enterprise.utils.LogUtil;
import com.hstypay.enterprise.utils.Utils;

import java.io.FileNotFoundException;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/7/28.
 */

public class VoiceUtils {
    private static volatile VoiceUtils singleton = null;
    public boolean IsPlaying;
    private Context mContext;
    private SoundPool soundPool;
    private int soundID;
    private AudioManager am;
    private int streamVolume;

    public VoiceUtils(Context context) {
        this.mContext = context.getApplicationContext();
    }

    /**
     * 单例
     *
     * @param context
     * @return
     */
    public static VoiceUtils with(Context context) {
        if (singleton == null) {
            synchronized (VoiceUtils.class) {
                if (singleton == null) {
                    singleton = new VoiceUtils(context);
                }
            }
        }
        return singleton;
    }

    public void setIsPlay(boolean IsPlaying) {

        this.IsPlaying = IsPlaying;
    }

    public boolean getIsPlay() {
        return IsPlaying;
    }

    private String str = "";

    public void playReceiveOrder() {
        if (soundPool == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundPool = new SoundPool.Builder()
                        .setMaxStreams(10)
                        .build();
            } else {
                soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 100);
            }
        }
        singleton.setIsPlay(true);
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        streamVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);

        soundID = soundPool.load(mContext, R.raw.receive_order, Integer.MAX_VALUE);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                int play = soundPool.play(soundID, 1f, 1f, 1, 0, 1);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SystemClock.sleep(4000);
                        singleton.setIsPlay(false);
                        am.setStreamVolume(AudioManager.STREAM_MUSIC, streamVolume, AudioManager.FLAG_PLAY_SOUND);
                    }
                }).start();
            }
        });
    }

    public void play(String stramount, boolean strsuccess) {

        //如果是TRUE  就播放“收款成功”这句话
        LogUtil.d("金额的-s-" + stramount);
        if (!TextUtils.isEmpty(stramount) && Utils.Double.isDouble(stramount)) {
            if (strsuccess) {
                str = "$" + PlaySound.capitalValueOf(new BigDecimal(stramount).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            } else {
                str = PlaySound.capitalValueOf(Double.valueOf(String.format("%.2f", Double.parseDouble(stramount))));
            }
        } else {
            str = "$";
        }
        LogUtil.d("金额的长度 " + str);
        String temp = "";

        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }*/
        if (!TextUtils.isEmpty(str)) {
            am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            streamVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
            LogUtil.d("voice--" + streamVolume + "/" + (am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)));
            am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
            LogUtil.d("voice-1-" + streamVolume + "/" + (am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)));
            singleton.setIsPlay(true);
            SystemClock.sleep(1000);
            playSoundpool(1, str, str.length());
        }
        //playSoundpool(1, str, str.length());
    }

    public void playFile(String fileName, final long ms) {
        if (soundPool == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundPool = new SoundPool.Builder()
                        .setMaxStreams(10)
                        .build();
            } else {
                soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 100);
            }
        }
        singleton.setIsPlay(true);
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        streamVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);

        int id = mContext.getResources().getIdentifier(fileName, "raw", AppHelper.getAppPackageName(mContext));
        if (id != 0) {
            soundID = soundPool.load(mContext, id, Integer.MAX_VALUE);
            soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    int play = soundPool.play(soundID, 1f, 1f, 1, 0, 1);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SystemClock.sleep(ms);
                            singleton.setIsPlay(false);
                            am.setStreamVolume(AudioManager.STREAM_MUSIC, streamVolume, AudioManager.FLAG_PLAY_SOUND);
                        }
                    }).start();
                }
            });
        }
    }

    //TODO 修改并发
    private void playSoundpool(final int soundindex, final String soundString, final int soundcount) {
        createSoundPool(soundindex, soundString);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                int play = soundPool.play(soundID, 1f, 1f, 1, 0, 1);
                LogUtil.d("ww===" + play);
                /*int newsoundindex = soundindex;
                if (newsoundindex == 1) {
                    SystemClock.sleep(1800);
                } else {
                    SystemClock.sleep(400);
                }*/
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int newsoundindex = soundindex;
                        if (newsoundindex == 1) {
                            if (AppHelper.getApkType() == 2) {
                                SystemClock.sleep(1000);
                            } else {
                                SystemClock.sleep(1500);
                            }
                        } else {
                            SystemClock.sleep(400);
                        }
                        if (soundindex < soundcount) {
                            newsoundindex = newsoundindex + 1;
                            playSoundpool(newsoundindex, soundString, soundcount);
                        } else {
                            singleton.setIsPlay(false);
                            LogUtil.d("voice-2-" + streamVolume + "/" + (am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)));
                            am.setStreamVolume(AudioManager.STREAM_MUSIC, streamVolume, AudioManager.FLAG_PLAY_SOUND);
                            LogUtil.d("voice-3-" + streamVolume + "/" + (am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)));
                        }
                    }
                }).start();
            }
        });
    }

    public void createSoundPool(int soundIndex, String soundString) {
        LogUtil.d("ww====" + "pool--" + soundIndex);
        if (soundPool == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                soundPool = new SoundPool.Builder()
                        .setMaxStreams(10)
                        .build();
            } else {
                soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 100);
            }
        }


        String soundChar = soundString.substring(soundIndex - 1, soundIndex);
        switch (soundChar) {
            case "零":
                soundID = soundPool.load(mContext, R.raw.tts_0, Integer.MAX_VALUE);
                break;
            case "壹":
                soundID = soundPool.load(mContext, R.raw.tts_1, Integer.MAX_VALUE);
                break;
            case "贰":
                soundID = soundPool.load(mContext, R.raw.tts_2, Integer.MAX_VALUE);
                break;
            case "叁":
                soundID = soundPool.load(mContext, R.raw.tts_3, Integer.MAX_VALUE);
                break;
            case "肆":
                soundID = soundPool.load(mContext, R.raw.tts_4, Integer.MAX_VALUE);
                break;
            case "伍":
                soundID = soundPool.load(mContext, R.raw.tts_5, Integer.MAX_VALUE);
                break;
            case "陆":
                soundID = soundPool.load(mContext, R.raw.tts_6, Integer.MAX_VALUE);
                break;
            case "柒":
                soundID = soundPool.load(mContext, R.raw.tts_7, Integer.MAX_VALUE);
                break;
            case "捌":
                soundID = soundPool.load(mContext, R.raw.tts_8, Integer.MAX_VALUE);
                break;
            case "玖":
                soundID = soundPool.load(mContext, R.raw.tts_9, Integer.MAX_VALUE);
                break;
            case "拾":
                soundID = soundPool.load(mContext, R.raw.tts_ten, Integer.MAX_VALUE);
                break;
            case "佰":
                soundID = soundPool.load(mContext, R.raw.tts_hundred, Integer.MAX_VALUE);
                break;
            case "仟":
                soundID = soundPool.load(mContext, R.raw.tts_thousand, Integer.MAX_VALUE);
                break;
            case "元":
                soundID = soundPool.load(mContext, R.raw.tts_yuan, Integer.MAX_VALUE);
                break;
            case "万":
                soundID = soundPool.load(mContext, R.raw.tts_ten_thousand, Integer.MAX_VALUE);
                break;
            case "点":
                soundID = soundPool.load(mContext, R.raw.tts_dot, Integer.MAX_VALUE);
                break;
            case "$":
                soundID = soundPool.load(mContext, R.raw.receive_money, Integer.MAX_VALUE);
                break;

        }
    }

}
