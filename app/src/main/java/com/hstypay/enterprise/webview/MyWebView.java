package com.hstypay.enterprise.webview;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.webview
 * @创建者: Jeremy
 * @创建时间: 2018/9/11 17:37
 * @描述: ${TODO}
 */
public class MyWebView extends WebView {
    public OnScrollInterface mScrollInterface;

    public MyWebView(Context context) {
        super(context);
    }

    public MyWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {

        super.onScrollChanged(l, t, oldl, oldt);

        mScrollInterface.onSChanged(l, t, oldl, oldt);

    }

    public void setOnCustomScrollChangeListener(OnScrollInterface scrollInterface) {

        this.mScrollInterface = scrollInterface;

    }

    public interface OnScrollInterface {

        public void onSChanged(int l, int t, int oldl, int oldt);

    }
}
