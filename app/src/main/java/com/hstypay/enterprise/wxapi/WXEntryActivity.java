package com.hstypay.enterprise.wxapi;

import android.os.Bundle;

import com.hstypay.enterprise.utils.LogUtil;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.umeng.weixin.callback.WXCallbackActivity;

//import com.umeng.socialize.weixin.view.WXCallbackActivity;

public class WXEntryActivity extends WXCallbackActivity implements IWXAPIEventHandler {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LogUtil.d("11111111111112222222");
        finish();
    }

    @Override
    public void onReq(BaseReq baseReq) {
        LogUtil.d("1111111111111333333333");
    }

    @Override
    public void onResp(BaseResp resp) {
        LogUtil.d("111111111111111111111");
        if (resp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {
            WXLaunchMiniProgram.Resp launchMiniProResp = (WXLaunchMiniProgram.Resp) resp;
            String extraData = launchMiniProResp.extMsg; //对应小程序组件 <button open-type="launchApp"> 中的 app-parameter 属性
            LogUtil.d("小程序==" + extraData);
            finish();
        }
    }
}
