package com.hstypay.enterprise;

import android.text.TextUtils;


import com.hstypay.enterprise.utils.DateUtil;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test1(){
        String code = "https://hpay.hstypay.com/app/crestv/qrcode?qrcode=30c0c6644c484c30b8ff6fe68838a16d&time=1458254";
        String[] split = code.split("&");//防止多参数链接
        String qrcodeId = "";
        if (split != null && split.length > 0) {
            for (int i = 0; i < split.length; i++) {
                if (split[i].contains("qrcode=")) {
                    String[] mchIds = split[i].split("qrcode=");
                    for (int i1 = 0; i1 < mchIds.length; i1++) {
                        qrcodeId = mchIds[1];
                    }
                }
            }
        }
        if (qrcodeId==null||qrcodeId.length()==0) {
            qrcodeId = code;
        }

        System.out.println(">>>>>>>>>>>"+qrcodeId);
    }

    @Test
    public void test2(){
        String todayDateOfMonth = DateUtil.getPreSevenDayDate();
        System.out.println(">>>>>>>>>>>"+todayDateOfMonth);

    }
}